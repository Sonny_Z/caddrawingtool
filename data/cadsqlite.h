#ifndef CADSQLITE_H
#define CADSQLITE_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QVariant>
#include "fas.h"
#include "fas_entity.h"
#include "fas_entitycontainer.h"
#include "fasutils.h"

class CadSqlite
{
public:
    CadSqlite();
    ~CadSqlite();
    // init database, create db file if not exist.
    void initDatabase();
    bool isExistingTable(const QString tableName);

    bool createDataFile(const QString& strFileName);
    bool openDataBase(const QString& strFileName);
    void closeDataBase();

    void createDeviceTable();
    void addDevice(const QString& deviceId,const QString& X, const QString& Y, const QString& deviceCode, const QString& deviceType, const QString& note, const QString& systemType);
    void removeDevice(const QString& deviceId);
    int readDeviceNum();
    void dropDevice();

    void createDetectTable();
    void addDetectRule(int id, DetectionRule rule);

    int readDetectNum();
    void dropDetect();

public:
    QSqlDatabase m_db;

};

#endif // DBMANAGER_H
