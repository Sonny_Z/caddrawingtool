#ifndef CADEXCEL_H
#define CADEXCEL_H

#include <QList>
#include <QString>

class CadExcel
{
public:
    CadExcel(const QString &fileName);

    void savePairInformation(QList<QString> name, QList<QString> count);

private:
    QString excelName;
};

#endif // CADEXCEL_H
