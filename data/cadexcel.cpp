#include "cadexcel.h"

#ifdef Q_OS_WIN
#include <QAxObject>
#endif

#include <QDir>
#include <QFile>

#include "commondef.h"

CadExcel::CadExcel(const QString &fileName)
{
    QDir fileDir = QFileInfo(fileName).absoluteDir();
    QString strFileDir = QFileInfo(fileName).absolutePath();
    excelName = strFileDir + "/FASDeviceCountResult.xlsx";
    if(!fileDir.exists())
    {
        fileDir.mkpath(strFileDir);
    }
    else
    {
        bool removed = fileDir.remove(excelName);
    }
}

void CadExcel::savePairInformation(QList<QString> nameList, QList<QString> countList)
{
    if(nameList.isEmpty() || nameList.count() != countList.count())
        return;

#ifdef Q_OS_WIN
    QAxObject* pApplication = new QAxObject();
    pApplication->setControl("Excel.Application");//connect to Excel control widget
    pApplication->dynamicCall("SetVisible(bool)", false);//false don't show from
    pApplication->setProperty("DisplayAlerts", false);//Don't show any warning information。
    QAxObject* pWorkBooks = pApplication->querySubObject("Workbooks");
    if(pWorkBooks == nullptr)
    {
        delete pApplication;
        return;
    }
    QAxObject* pWorkBook = nullptr;

    QFile file(excelName);
    if (file.exists())
    {
        pWorkBook = pWorkBooks->querySubObject("Open(const QString &)", excelName);
    }
    else
    {
        pWorkBooks->dynamicCall("Add");
        pWorkBook = pApplication->querySubObject("ActiveWorkBook");
    }
    if(pWorkBook != nullptr)
    {
        QAxObject* pSheets = pWorkBook->querySubObject("Sheets");
        if(pSheets != nullptr)
        {
            QAxObject* pSheet = pSheets->querySubObject("Item(int)", 1);
            for(int ii = 0; ii < nameList.count() && pSheet != nullptr; ii++)
            {
                QAxObject *pRange1 = pSheet->querySubObject("Cells(int,int)", ii+1, 1);
                if(pRange1 != nullptr)
                    pRange1->dynamicCall("Value", nameList.at(ii));
                QAxObject *pRange2 = pSheet->querySubObject("Cells(int,int)", ii+1, 2);
                if(pRange2 != nullptr)
                    pRange2->dynamicCall("Value", countList.at(ii));
            }
        }
        pWorkBook->dynamicCall("SaveAs(const QString &)", QDir::toNativeSeparators(excelName));
    }
    pApplication->dynamicCall("Quit()");
    delete pApplication;
    pApplication = nullptr;
#endif
}

