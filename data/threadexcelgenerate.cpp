#include "threadexcelgenerate.h"
#include "cadexcel.h"
#include "commondef.h"

void ThreadExcelGenerate::updateDetectType(QList<DeviceInfo*> typeList, QList<int> countList)
{
    detectType = typeList;
    typeCount = countList;
}

void ThreadExcelGenerate::run()
{
#ifdef Q_OS_WIN
    CoInitialize(nullptr);
    CadExcel myExcel(COMMONDEF->getExcelName());
    QList<int> countList;
    QList<QString> deviceStrList = COMMONDEF->getDeviceTypeList();

    for(int ii = 0; ii < deviceStrList.size(); ii++)
    {
        countList.append(0);
    }
    for (int ii = 0; ii < detectType.size(); ii++)
    {
        int index = deviceStrList.indexOf(detectType.at(ii)->getName());
        if(index >= 0)
            countList.replace(index, countList.at(index) + typeCount.at(ii));
    }
    //filter devices whose count is not zero.
    QList<QString> savedCountList;
    QList<QString> savedDeviceList;
    savedDeviceList.append(tr("DeviceName"));
    savedCountList.append(tr("Count"));
    for(int ii = 0; ii < deviceStrList.size(); ii++)
    {
        if(countList.at(ii) > 0)
        {
            savedDeviceList.append(COMMONDEF->translateToLocal(deviceStrList.at(ii)));
            savedCountList.append(QString::number(countList.at(ii)));
        }
    }
    myExcel.savePairInformation(savedDeviceList, savedCountList);
#endif
    this->exit();
}

