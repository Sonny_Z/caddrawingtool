#include "cadsqlite.h"
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QDebug>
#include "commondef.h"
#include "fasutils.h"

CadSqlite::CadSqlite() {}

CadSqlite::~CadSqlite() {}

void CadSqlite::initDatabase()
{
    QString strDbFile = COMMONDEF->getDatabaseName();
    this->createDataFile(strDbFile);
}

bool CadSqlite::createDataFile(const QString &strFileName)
{
    if(!QFile::exists(strFileName))
    {
        QDir fileDir = QFileInfo(strFileName).absoluteDir();
        QString strFileDir = QFileInfo(strFileName).absolutePath();
        if(!fileDir.exists())
        {
            fileDir.mkpath(strFileDir);
        }
        QFile dbFile(strFileName);
        if(!dbFile.open(QIODevice::WriteOnly))
        {
            dbFile.close();
            return false;
        }
        dbFile.close();
    }
    return true;
}

bool CadSqlite::isExistingTable(const QString tableName)
{
    QSqlQuery query;
    QString strSql = QString("SELECT 1 FROM sqlite_master where type = 'table' and  name = '%1'").arg(tableName);
    query.exec(strSql);
    if(query.next())
    {
        int nResult = query.value(0).toInt();
        if(nResult)
        {
            return true;
        }
    }
    return false;
}

bool CadSqlite::openDataBase(const QString& strFileName)
{
    if(QSqlDatabase::contains("qt_sql_default_connection"))
      m_db = QSqlDatabase::database("qt_sql_default_connection");
    else
      m_db = QSqlDatabase::addDatabase("QSQLITE");

    m_db.setDatabaseName(strFileName);
    if(m_db.open())
    {
        return true;
    }
    return false;
}

void CadSqlite::closeDataBase()
{
    m_db.close();
}

void CadSqlite::createDeviceTable()
{
    QString strSql = "create table DEVICE(Id VARCHAR(16) PRIMARY KEY, X VARCHAR(32), Y VARCHAR(32), deviceCode VARCHAR(32), deviceType VARCHAR(32), description VARCHAR(32), systemType VARCHAR(32))";
    QSqlQuery query(m_db);
    query.exec(strSql);

    QString strIndexSql = "CREATE INDEX device_index1 on DEVICE(Id);";
    query.exec(strIndexSql);
}

void CadSqlite::addDevice(const QString& deviceId,const QString& X, const QString& Y, const QString& deviceCode, const QString& deviceType, const QString& note, const QString& systemType)
{
    QString strSql;
    QSqlQuery query(m_db);
    strSql.clear();
    strSql.append("INSERT INTO DEVICE(Id, X, Y, deviceCode, deviceType, description, systemType) VALUES (:id, :x, :y, :code, :type, :note, :systemType)");
    query.prepare(strSql);
    query.bindValue(":id", deviceId);
    query.bindValue(":x", X);
    query.bindValue(":y", Y);
    query.bindValue(":code", deviceCode);
    query.bindValue(":type", deviceType);
    query.bindValue(":note", note);
    query.bindValue(":systemType", systemType);

    query.exec();
}

void CadSqlite::removeDevice(const QString& deviceId)
{
    QString strSql = QString("DELETE FROM DEVICE WHERE Id = '%1'").arg(deviceId);
    QSqlQuery query(m_db);
    bool bResult = query.exec(strSql);
//    // qDebug() << "removeDevice, result=" << bResult;
}

void CadSqlite::dropDevice()
{
    QString strSql = QString("DROP TABLE DEVICE");
    QSqlQuery query(m_db);
    bool bResult = query.exec(strSql);
    // qDebug() << "dropDevice, result=" << bResult;
}

void CadSqlite::dropDetect()
{
    QString strSql = QString("DROP TABLE DETECT");
    QSqlQuery query(m_db);
    bool bResult = query.exec(strSql);

    // qDebug() << "dropDetect, result=" << bResult;
}

void CadSqlite::createDetectTable()
{
    QString strSql = QString("create table DETECT(Id VARCHAR(16) PRIMARY KEY, CIRCLES VARCHAR(16), LINES VARCHAR(16), ARCS VARCHAR(16), HATCHS VARCHAR(16), SOLIDS VARCHAR(16), SIZE VARCHAR(16), TEXT VARCHAR(32), TYPE VARCHAR(32), SYSTEM VARCHAR(32))");
    QSqlQuery query(m_db);
    bool bResult = query.exec(strSql);
    // qDebug() << "DETECT table has been built"<< bResult;
}

void CadSqlite::addDetectRule(int id, DetectionRule rule)
{
    QString strSql;
    QSqlQuery query(m_db);

    strSql.clear();
    strSql.append("INSERT INTO DETECT(ID,CIRCLES,LINES,ARCS,HATCHS,SOLIDS,SIZE,TEXT,TYPE,SYSTEM) VALUES (:id, :circles, :lines, :arcs, :hatchs, :solids, :size, :text, :type, :system)");
    query.prepare(strSql);
    query.bindValue(":id", id);
    query.bindValue(":circles", rule.feature.circles);
    query.bindValue(":lines", rule.feature.lines);
    query.bindValue(":arcs", rule.feature.arcs);
    query.bindValue(":hatchs", rule.feature.hatchs);
    query.bindValue(":solids", rule.feature.solids);
    query.bindValue(":size", QString::number(rule.feature.size, 'g', 6));
    query.bindValue(":text", rule.feature.text);
    query.bindValue(":type", rule.deviceType);
    query.bindValue(":system", rule.systemType);
    bool bResult = query.exec();
    // qDebug() << "add one detect rule"<< bResult;
}



int CadSqlite::readDetectNum()
{
    QString strSql;
    QSqlQuery query(m_db);
    int count = 0;
    strSql.clear();
    strSql.append("select * from DETECT");
    query.prepare(strSql);
    query.exec();
    while(query.next()) count++;
    return count;
}

int CadSqlite::readDeviceNum()
{
    int id = 0;
    QString strSql;
    QSqlQuery query(m_db);
    strSql.clear();
    strSql.append("select * from DEVICE");
    query.prepare(strSql);
    query.exec();
    while(query.next()) id = query.value(0).toInt();
    return id;
}

