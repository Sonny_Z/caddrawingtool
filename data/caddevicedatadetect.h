#ifndef CADDEVICEDATADETECT_H
#define CADDEVICEDATADETECT_H

#include <QList>
#include <QString>
#include <QDir>
#include <QFile>

#include "commondef.h"
#include "fas_block.h"
#include "fas_insert.h"

class CadDeviceDataDetect
{
public:
    CadDeviceDataDetect(FAS_Block* &b, QString image, QString deviceType, int deviceCount);

    ~CadDeviceDataDetect();
    void setImageSrc(QString&);
    void setType(QString&);
    void setCount(int&);

    FAS_Block* getBlock();
    QString getImageSrc();
    QString getType();
    int getCount();
    bool getRecognizedStatus();

private:
    QString deviceImageSrc;
    QString deviceType;
    int deviceCount;
    bool isRecognized;
    FAS_Block* myBlock;
};

#endif // CADDEVICEDATADETECT_H
