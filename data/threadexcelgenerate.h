#ifndef THREADEXCELGENERATE_H
#define THREADEXCELGENERATE_H

#include <QThread>
#include "qt_windows.h"
#include "deviceinfo.h"

class ThreadExcelGenerate : public QThread
{
    Q_OBJECT

public:
    void run();

public slots:
    void updateDetectType(QList<DeviceInfo*> typeList, QList<int> countList);

private:
    QList<DeviceInfo*> detectType;
    QList<int> typeCount;
};

#endif // THREADEXCELGENERATE_H
