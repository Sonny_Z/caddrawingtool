#include "caddevicedatadetect.h"

#ifdef Q_OS_WIN
#include <QAxObject>
#endif

CadDeviceDataDetect::CadDeviceDataDetect(FAS_Block* &b, QString imagePath, QString deviceType, int deviceCount){
    this->deviceImageSrc = imagePath;
    this->deviceType = deviceType;
    this->deviceCount = deviceCount;
    this->isRecognized = true;
    this->myBlock = b;
}

CadDeviceDataDetect::~CadDeviceDataDetect(){

}
void CadDeviceDataDetect::setImageSrc(QString& imagePath){
    this->deviceImageSrc = imagePath;
}
void CadDeviceDataDetect::setType(QString& deviceType){
    this->deviceType = deviceType;
}
void CadDeviceDataDetect::setCount(int& deviceCount){
    this->deviceCount = deviceCount;
}

QString CadDeviceDataDetect::getImageSrc(){
    return this->deviceImageSrc;
}
QString CadDeviceDataDetect::getType(){
    return this->deviceType;
}
int CadDeviceDataDetect::getCount(){
    return this->deviceCount;
}
bool CadDeviceDataDetect::getRecognizedStatus(){
    return this->isRecognized;
}

FAS_Block* CadDeviceDataDetect::getBlock(){
    return this->myBlock;
}
