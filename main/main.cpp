//sort with alphabetical order
#include <QApplication>
#include <QTranslator>
#include <QFileInfo>
#include <QMessageBox>

#include "commondef.h"
#include "fas_system.h"
#include "mainwindow.h"
#include "logindialog.h"
#include "fas_fontlist.h"
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName("GST");
    QCoreApplication::setApplicationName("SolutionClient");
    QCoreApplication::setApplicationVersion(COMMONDEF->getAppVersion());

    QFileInfo prgInfo( QFile::decodeName(argv[0]) );
    QString prgDir(prgInfo.absolutePath());
    FAS_SYSTEM->init(app.applicationName(), app.applicationVersion(), "SolutionClient", prgDir);
    FAS_FONTLIST->init();
    // qDebug() << "app.applicationName():" << app.applicationName();
    MainWindow w;
    //检查程序是否 已经启动过
    QSharedMemory singleton(app.applicationName());
    if(!singleton.create(1))  {    //已经存在的
        QMessageBox *messageBox = new QMessageBox(QObject::tr("Warning"),
                                                 QObject::tr("Application is running."),
                                                 QMessageBox::Warning,
                                                 QMessageBox::Ok | QMessageBox::Default,
                                                 QMessageBox::Cancel | QMessageBox::Escape,
                                                 0);
        messageBox->exec();
        return -1;
    }

    int mainExitCode = 0;

    QHostInfo info;
    QString domainName = info.localDomainName();
    if(!COMMONDEF->isValidDomain(domainName)){
        QMessageBox *messageBox = new QMessageBox(QObject::tr("Warning"),
                                                 QObject::tr("Only Carrier(GST) assets are supported! Application will be closed."),
                                                 QMessageBox::Warning,
                                                 QMessageBox::Ok | QMessageBox::Default,
                                                 QMessageBox::Cancel | QMessageBox::Escape,
                                                 0);
//        messageBox->setTextInteractionFlags(Qt::TextSelectableByMouse);
        auto mExitCode = messageBox->exec();
        exit(mExitCode);
//        return -1;
    }

    QString latestOnlineAppVersion = COMMONDEF->getLatestOnlineAppVersion();
    auto latestOnlineAppVersionTemp = latestOnlineAppVersion;
    QString localAppVersion = app.applicationVersion();
    if(!latestOnlineAppVersion.isNull() && !latestOnlineAppVersion.isEmpty() && !localAppVersion.contains(latestOnlineAppVersion,Qt::CaseInsensitive)){
        auto localAppVersionInt = localAppVersion.replace(".","").remove(0,1).toInt();
        auto latestOnlineAppVersionInt = latestOnlineAppVersionTemp.replace(".","").remove(0,1).toInt();
        if(latestOnlineAppVersionInt > localAppVersionInt){
            QString message = QString::fromUtf8("%1 %2 %3")
                    .arg(QObject::tr("New version"))
                    .arg(latestOnlineAppVersion)
                    .arg(QObject::tr("is available! Please download the latest version online."));
            QMessageBox *messageBox = new QMessageBox(QObject::tr("Information"),
                                                     QObject::tr(message.toUtf8()),
                                                     QMessageBox::Information,
                                                     QMessageBox::Ok | QMessageBox::Default,
                                                     QMessageBox::Cancel | QMessageBox::Escape,
                                                     0);
            messageBox->exec();
        }
    }

    QString keyStr = "userName";
    auto localUserName = COMMONDEF->getAppSetting(keyStr);
    if(localUserName.isNull() || localUserName.isEmpty()){
        LoginDialog* login = new LoginDialog();
        login->setWindowFlags(login->windowFlags() | Qt::WindowStaysOnTopHint);
        SetWindowPos(HWND(login->winId()),HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
        auto e = login->exec();
        if(e != QDialog::Accepted){
            return -1;
        }
        delete login;
    }/*else{
        COMMONDEF->initializationValidation();
    }*/


    w.show();
    mainExitCode = app.exec();

    if(0 == mainExitCode){
        auto tmpDir = COMMONDEF->tempDirPath();
        QDir tmpObj(tmpDir);
        tmpObj.removeRecursively();/**< 如果目标目录存在，则进行清理 */
    }
    return mainExitCode;
}
