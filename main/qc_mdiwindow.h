/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef QC_MDIWINDOW_H
#define QC_MDIWINDOW_H

#include <QMdiSubWindow>

class QCloseEvent;

// MDI document window. Contains a document and a view (window).
class QC_MDIWindow: public QMdiSubWindow
{
    Q_OBJECT

public:
    QC_MDIWindow(QWidget* parent,
                 Qt::WindowFlags wflags=0);
    ~QC_MDIWindow();

public:

    bool closeMDI(bool force);

signals:
    void signalClosing(QC_MDIWindow*);
    void signalShow();

protected:
    void closeEvent(QCloseEvent*);
    void showEvent(QShowEvent *showEvent);

};

#endif

