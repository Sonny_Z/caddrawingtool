/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "qc_mdiwindow.h"

//sort with alphabetical order
#include <iostream>
#include <QApplication>
#include <QCloseEvent>

QC_MDIWindow::QC_MDIWindow(QWidget* parent, Qt::WindowFlags wflags)
                            : QMdiSubWindow(parent, wflags)
{
    setWindowFlag(Qt::WindowMaximizeButtonHint,false);
    setWindowFlag(Qt::WindowMinimizeButtonHint,false);
    setWindowFlag(Qt::WindowCloseButtonHint,false);
}

QC_MDIWindow::~QC_MDIWindow()
{
}

// closes this MDI window.
bool QC_MDIWindow::closeMDI(bool force)
{
//    emit(signalClosing(this));
    return true;
}

// Called by Qt when the user closes this MDI window.
void QC_MDIWindow::closeEvent(QCloseEvent* ce)
{
    if (closeMDI(true))
    {
        ce->accept();
    }
    else
    {
        ce->ignore();
    }
//    this->close();
}
void QC_MDIWindow::showEvent(QShowEvent *showEvent)
{
    emit signalShow();
}


