#-------------------------------------------------
#
# Project created by QtCreator 2017-04-01T15:59:15
#
#-------------------------------------------------

QT += core gui sql svg
QT += network
win32{ QT += axcontainer}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = SolutionClient
TEMPLATE = app
include(./boost.pri)

CONFIG += c++11

CONFIG  += debug_and_release
CONFIG(debug,debug|release){
    DESTDIR = $${PWD}/../bin
    #LIBS    += -L$${PWD}/lib/log4Qt -lliblog
}else{
    DESTDIR = $${PWD}/../bin
    #LIBS    += -L$${PWD}/lib/log4Qt -lliblog
}
INCLUDEPATH += $${PWD}/lib/log4Qt/

QMAKE_CXXFLAGS += -std=c++0x


INCLUDEPATH += lib/quazip/include
INCLUDEPATH += lib/zlib/include

LIBS += -L$${PWD}/lib/quazip/lib -lquazip -L$${PWD}/lib/zlib/lib -lzdll
INCLUDEPATH += \
    #/home/haojinming/Documents/boost_1_53_0 \
    lib/actions \
    lib/engine \
    lib/dxfparser \
    lib/dxfparser/dxfcodec \
    lib/fasdxfclient \
    lib/gui \
    lib/information \
    lib/math \
    lib/modification \
    main\
    ui \
    fasutils \
    data \
    ../res

SOURCES += \
    data/caddevicedatadetect.cpp \
    fasutils/grabthread.cpp \
    fasutils/longtimeprocess.cpp \
    fasutils/threaddraw.cpp \
    fasutils/threaddxfprocess.cpp \
    lib/actions/fas_actioninterface.cpp \
    lib/actions/fas_preview.cpp \
    lib/actions/fas_previewactioninterface.cpp \
    lib/actions/fas_snapper.cpp \
    lib/engine/fas_attdef.cpp \
    lib/engine/fas_font.cpp \
    lib/engine/fas_fontlist.cpp \
    lib/engine/fas_funlib.cpp \
    lib/engine/lc_rect.cpp \
    lib/engine/lc_splinepoints.cpp \
    lib/engine/fas_arc.cpp \
    lib/engine/fas_atomicentity.cpp \
    lib/engine/fas_block.cpp \
    lib/engine/fas_blocklist.cpp \
    lib/engine/fas_circle.cpp \
    lib/engine/fas_color.cpp \
    lib/engine/fas_constructionline.cpp \
    lib/engine/fas_dimaligned.cpp \
    lib/engine/fas_dimangular.cpp \
    lib/engine/fas_dimdiametric.cpp \
    lib/engine/fas_dimension.cpp \
    lib/engine/fas_dimlinear.cpp \
    lib/engine/fas_dimradial.cpp \
    lib/engine/fas_document.cpp \
    lib/engine/fas_ellipse.cpp \
    lib/engine/fas_entity.cpp \
    lib/engine/fas_entitycontainer.cpp \
    lib/engine/fas_flags.cpp \
    lib/engine/fas_graphic.cpp \
    lib/engine/fas_hatch.cpp \
    lib/engine/fas_image.cpp \
    lib/engine/fas_insert.cpp \
    lib/engine/fas_layer.cpp \
    lib/engine/fas_layerlist.cpp \
    lib/engine/fas_leader.cpp \
    lib/engine/fas_line.cpp \
    lib/engine/fas_mtext.cpp \
    lib/engine/fas_overlaybox.cpp \
    lib/engine/fas_overlayline.cpp \
    lib/engine/fas_pattern.cpp \
    lib/engine/fas_patternlist.cpp \
    lib/engine/fas_pen.cpp \
    lib/engine/fas_point.cpp \
    lib/engine/fas_polyline.cpp \
    lib/engine/fas_solid.cpp \
    lib/engine/fas_spline.cpp \
    lib/engine/fas_system.cpp \
    lib/engine/fas_text.cpp \
    lib/engine/fas_undo.cpp \
    lib/engine/fas_undoable.cpp \
    lib/engine/fas_undocycle.cpp \
    lib/engine/fas_units.cpp \
    lib/engine/fas_variabledict.cpp \
    lib/engine/fas_vector.cpp \
    lib/gui/fas_eventhandler.cpp \
    lib/gui/fas_graphicview.cpp \
    lib/gui/fas_grid.cpp \
    lib/gui/fas_linetypepattern.cpp \
    lib/gui/fas_painter.cpp \
    lib/gui/fas_painterqt.cpp \
    lib/gui/fas_staticgraphicview.cpp \
    lib/information/fas_information.cpp \
    lib/math/lc_quadratic.cpp \
    lib/math/fas_math.cpp \
    lib/modification/fas_modification.cpp \
    lib/modification/fas_selection.cpp \
    lib/dxfparser/dxf_entities.cpp \
    lib/dxfparser/dxf_objects.cpp \
    lib/dxfparser/dxfcodec/dxf_textcodec.cpp \
    lib/dxfparser/dxfcodec/dxfwriter.cpp \
    data/cadexcel.cpp \
    data/cadsqlite.cpp \
    main/main.cpp \
    main/qc_mdiwindow.cpp \
    ui/addcustomizeddevicedialog.cpp \
    ui/deviceRecordform.cpp \
    ui/entitypropdialog.cpp \
    ui/loadingprocess.cpp \
    ui/logindialog.cpp \
    ui/mainwindow.cpp \
    ui/blockform.cpp \
    ui/finddevice.cpp \
    ui/informationdialog.cpp \
    ui/entityqggv.cpp \
    ui/lccentralwidget.cpp \
    fasutils/commondef.cpp \
    ui/myprogressdialog.cpp \
    ui/newviewdialog.cpp \
    ui/popupmessagedialog.cpp \
    ui/showhidedialog.cpp \
    fasutils/fasutils.cpp\
    data/threadexcelgenerate.cpp \
    ui/autocodedialog.cpp \
    fasutils/deviceinfo.cpp \
    ui/adddevicetypedialog.cpp \
    ui/multiselectdialog.cpp \
    ui/adddevicedialog.cpp \
    ui/drawnotes.cpp \
    ui/settingdialog.cpp \
    ui/qg_graphicview.cpp \
    lib/actions/fas_actiondefault.cpp \
    lib/actions/fas_actioneditundo.cpp \
    lib/actions/fas_actionzoomin.cpp \
    lib/actions/fas_actionzoompan.cpp \
    lib/actions/fas_actionzoomwindow.cpp \
    lib/actions/fas_actionmodifydelete.cpp \
    lib/dxfparser/dxfcodec/dxfreader.cpp \
    lib/dxfparser/libdxf_parser.cpp \
    lib/dxfparser/libdxf_writer.cpp \
    lib/fasdxfclient/fas_dxfprocessor.cpp \
    ui/calculatewirelengthdialog.cpp \
    ui/nblockform.cpp \
    lib/engine/fas_devicecontainer.cpp \
    fasutils/Ifilter.cpp \
    lib/log4qt/log4qt/helpers/classlogger.cpp \
    lib/log4qt/log4qt/helpers/configuratorhelper.cpp \
    lib/log4qt/log4qt/helpers/datetime.cpp \
    lib/log4qt/log4qt/helpers/factory.cpp \
    lib/log4qt/log4qt/helpers/initialisationhelper.cpp \
    lib/log4qt/log4qt/helpers/logerror.cpp \
    lib/log4qt/log4qt/helpers/logobject.cpp \
    lib/log4qt/log4qt/helpers/logobjectptr.cpp \
    lib/log4qt/log4qt/helpers/optionconverter.cpp \
    lib/log4qt/log4qt/helpers/patternformatter.cpp \
    lib/log4qt/log4qt/helpers/properties.cpp \
    lib/log4qt/log4qt/spi/filter.cpp \
    lib/log4qt/log4qt/varia/debugappender.cpp \
    lib/log4qt/log4qt/varia/denyallfilter.cpp \
    lib/log4qt/log4qt/varia/levelmatchfilter.cpp \
    lib/log4qt/log4qt/varia/levelrangefilter.cpp \
    lib/log4qt/log4qt/varia/listappender.cpp \
    lib/log4qt/log4qt/varia/nullappender.cpp \
    lib/log4qt/log4qt/varia/stringmatchfilter.cpp \
    lib/log4qt/log4qt/appenderskeleton.cpp \
    lib/log4qt/log4qt/basicconfigurator.cpp \
    lib/log4qt/log4qt/consoleappender.cpp \
    lib/log4qt/log4qt/dailyrollingfileappender.cpp \
    lib/log4qt/log4qt/fileappender.cpp \
    lib/log4qt/log4qt/hierarchy.cpp \
    lib/log4qt/log4qt/layout.cpp \
    lib/log4qt/log4qt/level.cpp \
    lib/log4qt/log4qt/log4qt.cpp \
    lib/log4qt/log4qt/logger.cpp \
    lib/log4qt/log4qt/loggerrepository.cpp \
    lib/log4qt/log4qt/loggingevent.cpp \
    lib/log4qt/log4qt/logmanager.cpp \
    lib/log4qt/log4qt/mdc.cpp \
    lib/log4qt/log4qt/ndc.cpp \
    lib/log4qt/log4qt/patternlayout.cpp \
    lib/log4qt/log4qt/propertyconfigurator.cpp \
    lib/log4qt/log4qt/rollingfileappender.cpp \
    lib/log4qt/log4qt/simplelayout.cpp \
    lib/log4qt/log4qt/ttcclayout.cpp \
    lib/log4qt/log4qt/writerappender.cpp \
    lib/log4qt/qblog4qt.cpp \
    ui/waitdrawprogressdialog.cpp \
    ui/waitlongtimedialog.cpp \
    ui/waitopendialog.cpp \
    ui/waitopenprogressdialog.cpp \
    ui/zoomwidget.cpp

HEADERS  += \
    data/caddevicedatadetect.h \
    fasutils/grabthread.h \
    fasutils/longtimeprocess.h \
    fasutils/threaddraw.h \
    fasutils/threaddxfprocess.h \
    lib/actions/fas_actioninterface.h \
    lib/actions/fas_preview.h \
    lib/actions/fas_previewactioninterface.h \
    lib/actions/fas_snapper.h \
    lib/engine/fas_attdef.h \
    lib/engine/fas_font.h \
    lib/engine/fas_fontchar.h \
    lib/engine/fas_fontlist.h \
    lib/engine/fas_funlib.h \
    lib/engine/lc_rect.h \
    lib/engine/lc_splinepoints.h \
    lib/engine/fas_arc.h \
    lib/engine/fas_atomicentity.h \
    lib/engine/fas_block.h \
    lib/engine/fas_blocklist.h \
    lib/engine/fas_circle.h \
    lib/engine/fas_color.h \
    lib/engine/fas_constructionline.h \
    lib/engine/fas_dimaligned.h \
    lib/engine/fas_dimangular.h \
    lib/engine/fas_dimdiametric.h \
    lib/engine/fas_dimension.h \
    lib/engine/fas_dimlinear.h \
    lib/engine/fas_dimradial.h \
    lib/engine/fas_document.h \
    lib/engine/fas_ellipse.h \
    lib/engine/fas_entity.h \
    lib/engine/fas_entitycontainer.h \
    lib/engine/fas_flags.h \
    lib/engine/fas_graphic.h \
    lib/engine/fas_hatch.h \
    lib/engine/fas_image.h \
    lib/engine/fas_insert.h \
    lib/engine/fas_layer.h \
    lib/engine/fas_layerlist.h \
    lib/engine/fas_layerlistlistener.h \
    lib/engine/fas_leader.h \
    lib/engine/fas_line.h \
    lib/engine/fas_mtext.h \
    lib/engine/fas_overlaybox.h \
    lib/engine/fas_overlayline.h \
    lib/engine/fas_pattern.h \
    lib/engine/fas_patternlist.h \
    lib/engine/fas_pen.h \
    lib/engine/fas_point.h \
    lib/engine/fas_polyline.h \
    lib/engine/fas_solid.h \
    lib/engine/fas_spline.h \
    lib/engine/fas_system.h \
    lib/engine/fas_text.h \
    lib/engine/fas_undo.h \
    lib/engine/fas_undoable.h \
    lib/engine/fas_undocycle.h \
    lib/engine/fas_units.h \
    lib/engine/fas_variable.h \
    lib/engine/fas_variabledict.h \
    lib/engine/fas_vector.h \
    lib/gui/fas_eventhandler.h \
    lib/gui/fas_graphicview.h \
    lib/gui/fas_grid.h \
    lib/gui/fas_linetypepattern.h \
    lib/gui/fas_painter.h \
    lib/gui/fas_painterqt.h \
    lib/gui/fas_staticgraphicview.h \
    lib/information/fas_information.h \
    lib/math/lc_quadratic.h \
    lib/math/fas_math.h \
    lib/modification/fas_modification.h \
    lib/modification/fas_selection.h \
    lib/dxfparser/dxf_base.h \
    lib/dxfparser/dxf_entities.h \
    lib/dxfparser/dxf_interface.h \
    lib/dxfparser/dxf_objects.h \
    lib/dxfparser/dxfcodec/dxf_cptables.h \
    lib/dxfparser/dxfcodec/dxf_textcodec.h \
    lib/dxfparser/dxfcodec/dxfreader.h \
    lib/dxfparser/dxfcodec/dxfwriter.h \
    ui/addcustomizeddevicedialog.h \
    ui/deviceRecordform.h \
    ui/entitypropdialog.h \
    ui/loadingprocess.h \
    ui/logindialog.h \
    ui/myprogressdialog.h \
    ui/newviewdialog.h \
    ui/popupmessagedialog.h \
    ui/qg_scrollbar.h \
    data/cadexcel.h \
    data/cadsqlite.h \
    ui/mainwindow.h \
    ui/blockform.h \
    ui/entityqggv.h \
    ui/finddevice.h \
    ui/informationdialog.h \
    ui/lc_centralwidget.h \
    ui/qg_graphicview.h \
    fasutils/commondef.h \
    ui/showhidedialog.h \
    fasutils/fasutils.h \
    data/threadexcelgenerate.h \
    data/caddevicedatadetect.h \
    ui/autocodedialog.h \
    fasutils/deviceinfo.h \
    ui/adddevicetypedialog.h \
    ui/multiselectdialog.h \
    main/qc_mdiwindow.h \
    ui/adddevicedialog.h \
    ui/drawnotes.h \
    ui/settingdialog.h \
    lib/engine/fas.h \
    lib/actions/fas_actiondefault.h \
    lib/actions/fas_actioneditundo.h \
    lib/actions/fas_actionzoomin.h \
    lib/actions/fas_actionzoompan.h \
    lib/actions/fas_actionzoomwindow.h \
    lib/actions/fas_actionmodifydelete.h \
    lib/dxfparser/libdxf_parser.h \
    lib/dxfparser/libdxf_writer.h \
    lib/fasdxfclient/fas_dxfprocessor.h \
    ui/calculatewirelengthdialog.h \
    ui/nblockform.h \
    lib/engine/fas_devicecontainer.h \
    fasutils/Ifilter.h \
    lib/log4qt/log4qt/helpers/classlogger.h \
    lib/log4qt/log4qt/helpers/configuratorhelper.h \
    lib/log4qt/log4qt/helpers/datetime.h \
    lib/log4qt/log4qt/helpers/factory.h \
    lib/log4qt/log4qt/helpers/initialisationhelper.h \
    lib/log4qt/log4qt/helpers/logerror.h \
    lib/log4qt/log4qt/helpers/logobject.h \
    lib/log4qt/log4qt/helpers/logobjectptr.h \
    lib/log4qt/log4qt/helpers/optionconverter.h \
    lib/log4qt/log4qt/helpers/patternformatter.h \
    lib/log4qt/log4qt/helpers/properties.h \
    lib/log4qt/log4qt/spi/filter.h \
    lib/log4qt/log4qt/varia/debugappender.h \
    lib/log4qt/log4qt/varia/denyallfilter.h \
    lib/log4qt/log4qt/varia/levelmatchfilter.h \
    lib/log4qt/log4qt/varia/levelrangefilter.h \
    lib/log4qt/log4qt/varia/listappender.h \
    lib/log4qt/log4qt/varia/nullappender.h \
    lib/log4qt/log4qt/varia/stringmatchfilter.h \
    lib/log4qt/log4qt/appender.h \
    lib/log4qt/log4qt/appenderskeleton.h \
    lib/log4qt/log4qt/basicconfigurator.h \
    lib/log4qt/log4qt/consoleappender.h \
    lib/log4qt/log4qt/dailyrollingfileappender.h \
    lib/log4qt/log4qt/fileappender.h \
    lib/log4qt/log4qt/hierarchy.h \
    lib/log4qt/log4qt/layout.h \
    lib/log4qt/log4qt/level.h \
    lib/log4qt/log4qt/log4qt.h \
    lib/log4qt/log4qt/logger.h \
    lib/log4qt/log4qt/loggerrepository.h \
    lib/log4qt/log4qt/loggingevent.h \
    lib/log4qt/log4qt/logmanager.h \
    lib/log4qt/log4qt/mdc.h \
    lib/log4qt/log4qt/ndc.h \
    lib/log4qt/log4qt/patternlayout.h \
    lib/log4qt/log4qt/propertyconfigurator.h \
    lib/log4qt/log4qt/rollingfileappender.h \
    lib/log4qt/log4qt/simplelayout.h \
    lib/log4qt/log4qt/ttcclayout.h \
    lib/log4qt/log4qt/writerappender.h \
    lib/log4qt/log4qt_global.h \
    lib/log4qt/qblog4qt.h \
    ui/waitdrawprogressdialog.h \
    ui/waitlongtimedialog.h \
    ui/waitopendialog.h \
    ui/waitopenprogressdialog.h \
    ui/zoomwidget.h


FORMS += \
    forms/addcustomizeddevicedialog.ui \
    forms/blockform.ui \
    forms/deviceRecordform.ui \
    forms/entitypropdialog.ui \
    forms/finddevice.ui \
    forms/informationdialog.ui \
    forms/loadingprocess.ui \
    forms/logindialog.ui \
    forms/mainwindow.ui \
    forms/autocodedialog.ui \
    forms/newviewdialog.ui \
    forms/popupmessagedialog.ui \
    forms/showhidedialog.ui \
    forms/adddevicetypedialog.ui \
    forms/multiselectdialog.ui \
    forms/adddevicedialog.ui \
    forms/drawnotes.ui \
    forms/settingdialog.ui \
    forms/calculatewirelengthdialog.ui \
    forms/nblockform.ui \
    ui/myprogressdialog.ui \
    ui/waitlongtimedialog.ui \
    ui/waitopendialog.ui \
    ui/zoomwidget.ui

RESOURCES += \
    resource.qrc

DISTFILES +=

TRANSLATIONS += resource/Language_zh.ts

#RC_ICONS = resource/image/cadr.ico

#RC_FILE = proj.rc

#OTHER_FILES += proj.rc

#RC_FILE += proj.rc


