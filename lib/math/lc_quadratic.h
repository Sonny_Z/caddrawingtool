/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/


#ifndef LC_QUADRATIC_H
#define LC_QUADRATIC_H

#include "fas_vector.h"
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

class FAS_VectorSolutions;
class FAS_AtomicEntity;

// Class for generic linear and quadratic equation supports translation and rotation of an equation
class LC_Quadratic {
public:
    explicit LC_Quadratic();
    LC_Quadratic(const LC_Quadratic& lc0);
    LC_Quadratic& operator = (const LC_Quadratic& lc0);
    LC_Quadratic(const FAS_AtomicEntity* circle, const FAS_Vector& point);
    LC_Quadratic(const FAS_AtomicEntity* circle0,const FAS_AtomicEntity* circle1,
                 bool mirror = false);
    LC_Quadratic(const FAS_Vector& point0, const FAS_Vector& point1);

    LC_Quadratic(std::vector<double> ce);
    std::vector<double> getCoefficients() const;
    LC_Quadratic move(const FAS_Vector& v);
    LC_Quadratic rotate(const double& a);
    LC_Quadratic rotate(const FAS_Vector& center, const double& a);
    bool isQuadratic() const;
    explicit operator bool() const;
    bool isValid() const;
    void setValid(bool value);
    bool operator == (bool valid) const;
    bool operator != (bool valid) const;

    boost::numeric::ublas::vector<double>& getLinear();
    const boost::numeric::ublas::vector<double>& getLinear() const;
    boost::numeric::ublas::matrix<double>& getQuad();
    const boost::numeric::ublas::matrix<double>& getQuad() const;
    double const& constTerm()const;
    double& constTerm();

    // switch x,y coordinates
    LC_Quadratic flipXY(void) const;
    // the matrix of rotation by angle
    static boost::numeric::ublas::matrix<double> rotationMatrix(const double& angle);
    static FAS_VectorSolutions getIntersection(const LC_Quadratic& l1, const LC_Quadratic& l2);
    friend std::ostream& operator << (std::ostream& os, const LC_Quadratic& l);
private:
    // the equation form: {x, y}.m_mQuad.{{x},{y}} + m_vLinear.{{x},{y}}+m_dConst=0
    boost::numeric::ublas::matrix<double> m_mQuad;
    boost::numeric::ublas::vector<double> m_vLinear;
    double m_dConst;
    bool m_bIsQuadratic;
    // whether this quadratic form is valid
    bool m_bValid;
};



#endif
//EOF
