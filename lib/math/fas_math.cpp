/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_math.h"

//sort with alphabetical order
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/math/special_functions/ellint_2.hpp>

#include <cmath>
#include <QString>
#include <QDebug>

#include "fas_vector.h"

namespace {
constexpr double m_piX2 = M_PI*2; //2*PI
}

// Rounds the given double to the closest int.
int FAS_Math::round(double v) {
    return (int) lrint(v);
}
// Save pow function
double FAS_Math::pow(double x, double y)
{
    errno = 0;
    double ret = ::pow(x, y);
    if (errno==EDOM) {
        ret = 0.0;
    }
    else if (errno==ERANGE) {
        ret = 0.0;
    }
    return ret;
}

// pow of vector components
FAS_Vector FAS_Math::pow(FAS_Vector vp, double y)
{
    return FAS_Vector(pow(vp.x,y),pow(vp.y,y));
}

//Converts radians to degrees.
double FAS_Math::rad2deg(double a)
{
    return 180./M_PI*a;
}

// Converts degrees to radians.
double FAS_Math::deg2rad(double a)
{
    return M_PI/180.0*a;
}

//Converts radians to gradians.
double FAS_Math::rad2gra(double a)
{
    return 200./M_PI*a;
}

double FAS_Math::gra2rad(double a)
{
    return M_PI/200.*a;
}

// Finds greatest common divider using Euclid's algorithm.
unsigned FAS_Math::findGCD(unsigned a, unsigned b)
{

    while (b)
    {
        unsigned rem = a % b;
        a = b;
        b = rem;
    }

    return a;
}

// Tests if angle a is between a1 and a2. a, a1 and a2 must be in the range between 0 and 2*PI.
bool FAS_Math::isAngleBetween(double a, double a1, double a2, bool reversed)
{
    if (reversed) std::swap(a1,a2);
    if(getAngleDifferenceU(a2, a1 ) < FAS_TOLERANCE_ANGLE) return true;
    const double tol=0.5*FAS_TOLERANCE_ANGLE;
    const double diff0=correctAngle(a2 -a1) + tol;
    return diff0 >= correctAngle(a - a1) || diff0 >= correctAngle(a2 - a);
}

// Corrects the given angle to the range of 0-2*Pi.
double FAS_Math::correctAngle(double a)
{
    return M_PI + remainder(a - M_PI, m_piX2);
}

double FAS_Math::correctAngleU(double a)
{
    return fabs(remainder(a, m_piX2));
}


// The angle that needs to be added to a1 to reach a2. Always positive and less than 2*pi.
double FAS_Math::getAngleDifference(double a1, double a2, bool reversed)
{
    if(reversed) std::swap(a1, a2);
    return correctAngle(a2 - a1);
}

double FAS_Math::getAngleDifferenceU(double a1, double a2)
{
    return correctAngleU(a1 - a2);
}


// Makes a text constructed with the given angle readable. Used for dimension texts and for mirroring texts.
double FAS_Math::makeAngleReadable(double angle, bool readable, bool* corrected)
{
    double ret=correctAngle(angle);
    bool cor = isAngleReadable(ret) ^ readable;
    // quadrant 1 & 4
    if (cor)
    {
        ret = correctAngle(angle+M_PI);
    }
    if (corrected)
    {
        *corrected = cor;
    }
    return ret;
}

//return true: if the given angle is in a range that is readable for texts created with that angle.
bool FAS_Math::isAngleReadable(double angle)
{
    const double tolerance=0.001;
    if (angle>M_PI_2)
        return fabs(remainder(angle, m_piX2)) < (M_PI_2 - tolerance);
    else
        return fabs(remainder(angle, m_piX2)) < (M_PI_2 + tolerance);
}

bool FAS_Math::isSameDirection(double dir1, double dir2, double tol)
{
    return getAngleDifferenceU(dir1, dir2) < tol;
}

//Converts a double into a string which is as short as possible
QString FAS_Math::doubleToString(double value, double prec)
{
    if (prec< FAS_TOLERANCE )
    {
        return QString().setNum(value, prec);
    }

    double const num = FAS_Math::round(value / prec)*prec;

    QString exaStr = FAS_Math::doubleToString(1./prec, 10);
    int const dotPos = exaStr.indexOf('.');

    if (dotPos==-1)
    {
        return QString().setNum(FAS_Math::round(num));
    }
    else
    {
        int digits = dotPos - 1;
        return FAS_Math::doubleToString(num, digits);
    }
}

// Converts a double into a string which is as short as possible.
QString FAS_Math::doubleToString(double value, int prec)
{
    QString valStr;
    valStr.setNum(value, 'f', prec);
    if(valStr.contains('.'))
    {
        if(valStr.at(valStr.length()-1)=='.')
        {
            valStr.truncate(valStr.length()-1);
        }
    }
    return valStr;
}

//Equation solvers

// quadratic, cubic, and quartic equation solver
// @ ce[] contains coefficent of the cubic equation:
// @ returns a vector contains real roots
//
// solvers assume arguments are valid, and there's no attempt to verify validity of the argument pointers
std::vector<double> FAS_Math::quadraticSolver(const std::vector<double>& ce)
//quadratic solver for
// x^2 + ce[0] x + ce[1] =0
{
    std::vector<double> ans(0,0.);
    if (ce.size() != 2) return ans;
    using LDouble = long double;
    LDouble const b = -0.5L * ce[0];
    LDouble const c = ce[1];
    // x^2 -2 b x + c=0
    // (x - b)^2 = b^2 - c
    // b^2 >= fabs(c)
    // x = b \pm b sqrt(1. - c/(b^2))
    LDouble const b2= b * b;
    LDouble const discriminant= b2 - c;
    LDouble const fc = std::abs(c);

    //TODO, fine tune to tolerance level
    LDouble const TOL = 1e-24L;

    if (discriminant < 0.L)
    {
        //negative discriminant, no real root
        //return ans;
    }

    //find the radical
    LDouble r;

    // given |p| >= |q|
    // sqrt(p^2 \pm q^2) = p sqrt(1 \pm q^2/p^2)
    if (b2 >= fc)
        r = std::abs(b) * std::sqrt(1.L - c/b2);
    else
        // c is negative, because b2 - c is non-negative
        r = std::sqrt(fc) * std::sqrt(1.L + b2/fc);

    if (r >= TOL*std::abs(b))
    {
        //two roots
        if (b >= 0.L)
            //since both (b,r)>=0, avoid (b - r) loss of significance
            ans.push_back(b + r);
        else
            //since b<0, r>=0, avoid (b + r) loss of significance
            ans.push_back(b - r);

        //Vieta's formulas for the second root
        ans.push_back(c/ans.front());
    }
    else
        //multiple roots
        ans.push_back(b);
    return ans;
}


std::vector<double> FAS_Math::cubicSolver(const std::vector<double>& ce)
//cubic equation solver
// x^3 + ce[0] x^2 + ce[1] x + ce[2] = 0
{
    std::vector<double> ans(0,0.);
    if (ce.size() != 3) return ans;
    double shift=(1./3)*ce[0];
    double p=ce[1] -shift*ce[0];
    double q=ce[0]*( (2./27)*ce[0]*ce[0]-(1./3)*ce[1])+ce[2];
    double discriminant= (1./27)*p*p*p+(1./4)*q*q;
    if ( fabs(p)< 1.0e-75)
    {
        ans.push_back((q>0)?-pow(q,(1./3)):pow(-q,(1./3)));
        ans[0] -= shift;
        return ans;
    }
    if(discriminant>0)
    {
        std::vector<double> ce2(2,0.);
        ce2[0]=q;
        ce2[1]=-1./27*p*p*p;
        auto r=quadraticSolver(ce2);
        if ( r.size()==0 )
        { //should not happen
            std::cerr<<__FILE__<<" : "<<__func__<<" : line"<<__LINE__<<" :cubicSolver()::Error cubicSolver("<<ce[0]<<' '<<ce[1]<<' '<<ce[2]<<")\n";
            return ans;
        }
        double u,v;
        u= (q<=0) ? pow(r[0], 1./3): -pow(-r[1],1./3);
        v=(-1./3)*p/u;
        ans.push_back(u+v - shift);
    }
    else
    {
        std::complex<double> u(q,0),rt[3];
        u=std::pow(-0.5*u-sqrt(0.25*u*u+p*p*p/27),1./3);
        rt[0]=u-p/(3.*u)-shift;
        std::complex<double> w(-0.5,sqrt(3.)/2);
        rt[1]=u*w-p/(3.*u*w)-shift;
        rt[2]=u/w-p*w/(3.*u)-shift;
        ans.push_back(rt[0].real());
        ans.push_back(rt[1].real());
        ans.push_back(rt[2].real());
    }
    // newton-raphson
    for(double& x0: ans)
    {
        double dx=0.;
        for(size_t i=0; i<20; ++i)
        {
            double f=( (x0 + ce[0])*x0 + ce[1])*x0 +ce[2];
            double df=(3.*x0+2.*ce[0])*x0 +ce[1];
            if(fabs(df)>fabs(f)+FAS_TOLERANCE)
            {
                dx=f/df;
                x0 -= dx;
            }
            else
                break;
        }
    }

    return ans;
}

/** quartic solver
* x^4 + ce[0] x^3 + ce[1] x^2 + ce[2] x + ce[3] = 0
@ce, a vector of size 4 contains the coefficient in order
@return, a vector contains real roots
**/
std::vector<double> FAS_Math::quarticSolver(const std::vector<double>& ce)
{
    std::vector<double> ans(0,0.);
    if(ce.size() != 4)
        return ans;

    double shift=0.25*ce[0];
    double shift2=shift*shift;
    double a2=ce[0]*ce[0];
    double p= ce[1] - (3./8)*a2;
    double q= ce[2] + ce[0]*((1./8)*a2 - 0.5*ce[1]);
    double r= ce[3] - shift*ce[2] + (ce[1] - 3.*shift2)*shift2;
    if (q*q <= 1.e-4*FAS_TOLERANCE*fabs(p*r))
    {
        // Biquadratic equations
        double discriminant= 0.25*p*p -r;
        if (discriminant < -1.e3*FAS_TOLERANCE)
        {
            return ans;
        }
        double t2[2];
        t2[0]=-0.5*p-sqrt(fabs(discriminant));
        t2[1]= -p - t2[0];
        if ( t2[1] >= 0.) { // two real roots
            ans.push_back(sqrt(t2[1])-shift);
            ans.push_back(-sqrt(t2[1])-shift);
        }
        if ( t2[0] >= 0. ) {// four real roots
            ans.push_back(sqrt(t2[0])-shift);
            ans.push_back(-sqrt(t2[0])-shift);
        }
        return ans;
    }
    if ( fabs(r)< 1.0e-75 ) {
        std::vector<double> cubic(3,0.);
        cubic[1]=p;
        cubic[2]=q;
        ans.push_back(0.);
        auto r=cubicSolver(cubic);
        std::copy(r.begin(),r.end(), std::back_inserter(ans));
        for(size_t i=0; i<ans.size(); i++) ans[i] -= shift;
        return ans;
    }
    std::vector<double> cubic(3,0.);
    cubic[0]=2.*p;
    cubic[1]=p*p-4.*r;
    cubic[2]=-q*q;
    auto r3= cubicSolver(cubic);
    //newton-raphson
    if (r3.size()==1)
    {
        //one real root from cubic
        if (r3[0]< 0.)
        {
            return ans;
        }
        double sqrtz0=sqrt(r3[0]);
        std::vector<double> ce2(2,0.);
        ce2[0]=	-sqrtz0;
        ce2[1]=0.5*(p+r3[0])+0.5*q/sqrtz0;
        auto r1=quadraticSolver(ce2);
        if (r1.size()==0 )
        {
            ce2[0]=	sqrtz0;
            ce2[1]=0.5*(p+r3[0])-0.5*q/sqrtz0;
            r1=quadraticSolver(ce2);
        }
        for(auto& x: r1)
        {
            x -= shift;
        }
        return r1;
    }
    if ( r3[0]> 0. && r3[1] > 0. )
    {
        double sqrtz0=sqrt(r3[0]);
        std::vector<double> ce2(2,0.);
        ce2[0]=	-sqrtz0;
        ce2[1]=0.5*(p+r3[0])+0.5*q/sqrtz0;
        ans=quadraticSolver(ce2);
        ce2[0]=	sqrtz0;
        ce2[1]=0.5*(p+r3[0])-0.5*q/sqrtz0;
        auto r1=quadraticSolver(ce2);
        std::copy(r1.begin(),r1.end(),std::back_inserter(ans));
        for(auto& x: ans)
        {
            x -= shift;
        }
    }
    // newton-raphson
    for(double& x0: ans)
    {
        double dx=0.;
        for(size_t i=0; i<20; ++i)
        {
            double f=(( (x0 + ce[0])*x0 + ce[1])*x0 +ce[2])*x0 + ce[3] ;
            double df=((4.*x0+3.*ce[0])*x0 +2.*ce[1])*x0+ce[2];
            if(fabs(df)>FAS_TOLERANCE2)
            {
                dx=f/df;
                x0 -= dx;
            }
            else
                break;
        }
    }
    return ans;
}

/** quartic solver
* ce[4] x^4 + ce[3] x^3 + ce[2] x^2 + ce[1] x + ce[0] = 0
@ce, a vector of size 5 contains the coefficient in order
@return, a vector contains real roots
*ToDo, need a robust algorithm to locate zero terms, better handling of tolerances
**/
std::vector<double> FAS_Math::quarticSolverFull(const std::vector<double>& ce)
{
    std::vector<double> roots(0,0.);
    if(ce.size()!=5) return roots;
    std::vector<double> ce2(4,0.);

    if ( fabs(ce[4]) < 1.0e-14)
    { // this should not happen
        if ( fabs(ce[3]) < 1.0e-14)
        { // this should not happen
            if ( fabs(ce[2]) < 1.0e-14)
            { // this should not happen
                if( fabs(ce[1]) > 1.0e-14)
                {
                    roots.push_back(-ce[0]/ce[1]);
                }
                else
                {
                    return roots;
                }
            }
            else
            {
                ce2.resize(2);
                ce2[0]=ce[1]/ce[2];
                ce2[1]=ce[0]/ce[2];
                roots=FAS_Math::quadraticSolver(ce2);
            }
        }
        else
        {
            ce2.resize(3);
            ce2[0]=ce[2]/ce[3];
            ce2[1]=ce[1]/ce[3];
            ce2[2]=ce[0]/ce[3];
            roots=FAS_Math::cubicSolver(ce2);
        }
    }
    else
    {
        ce2[0]=ce[3]/ce[4];
        ce2[1]=ce[2]/ce[4];
        ce2[2]=ce[1]/ce[4];
        ce2[3]=ce[0]/ce[4];
        if(fabs(ce2[3])<= FAS_TOLERANCE15)
        {
            //constant term is zero, factor 0 out, solve a cubic equation
            ce2.resize(3);
            roots=FAS_Math::cubicSolver(ce2);
            roots.push_back(0.);
        }
        else
            roots=FAS_Math::quarticSolver(ce2);
    }
    return roots;
}

//linear Equation solver by Gauss-Jordan
bool FAS_Math::linearSolver(const std::vector<std::vector<double> >& mt, std::vector<double>& sn)
{
    //verify the matrix size
    size_t mSize(mt.size()); //rows
    size_t aSize(mSize+1); //columns of augmented matrix
    if(std::any_of(mt.begin(), mt.end(), [&aSize](const std::vector<double>& v)->bool
    {
                   return v.size() != aSize;
}))
        return false;
    sn.resize(mSize);//to hold the solution
    // solve the linear equation by Gauss-Jordan elimination
    std::vector<std::vector<double> > mt0(mt); //copy the matrix;
    for(size_t i=0;i<mSize;++i)
    {
        size_t imax(i);
        double cmax(fabs(mt0[i][i]));
        for(size_t j=i+1;j<mSize;++j)
        {
            if(fabs(mt0[j][i]) > cmax )
            {
                imax=j;
                cmax=fabs(mt0[j][i]);
            }
        }
        if(cmax<FAS_TOLERANCE2) return false; //singular matrix
        if(imax != i)
        {//move the line with largest absolute value at column i to row i, to avoid division by zero
            std::swap(mt0[i],mt0[imax]);
        }
        for(size_t k=i+1;k<=mSize;++k)
        { //normalize the i-th row
            mt0[i][k] /= mt0[i][i];
        }
        mt0[i][i]=1.;
        for(size_t j=0;j<mSize;++j)
        {
            //Gauss-Jordan
            if(j != i )
            {
                double& a = mt0[j][i];
                for(size_t k=i+1;k<=mSize;++k)
                {
                    mt0[j][k] -= mt0[i][k]*a;
                }
                a=0.;
            }
        }
    }
    for(size_t i=0;i<mSize;++i)
    {
        sn[i]=mt0[i][mSize];
    }
    return true;
}

// wrapper of elliptic integral of the second type, Legendre form
double FAS_Math::ellipticIntegral_2(const double& k, const double& phi)
{
    double a= remainder(phi-M_PI_2,M_PI);
    if(a>0.)
    {
        return boost::math::ellint_2<double,double>(k,a);
    }
    else
    {
        return - boost::math::ellint_2<double,double>(k,fabs(a));
    }
}

/** solver quadratic simultaneous equations of set two **/
FAS_VectorSolutions FAS_Math::simultaneousQuadraticSolver(const std::vector<double>& m)
{
    FAS_VectorSolutions ret(0);
    if(m.size() != 8 ) return ret; // valid m should contain exact 8 elements
    std::vector< double> c1(0,0.);
    std::vector< std::vector<double> > m1(0,c1);
    c1.resize(6);
    c1[0]=m[0];
    c1[1]=0.;
    c1[2]=m[1];
    c1[3]=0.;
    c1[4]=0.;
    c1[5]=-1.;
    m1.push_back(c1);
    c1[0]=m[2];
    c1[1]=2.*m[3];
    c1[2]=m[4];
    c1[3]=m[5];
    c1[4]=m[6];
    c1[5]=m[7];
    m1.push_back(c1);
    return simultaneousQuadraticSolverFull(m1);
}

/** solver quadratic simultaneous equations of a set of two **/
FAS_VectorSolutions FAS_Math::simultaneousQuadraticSolverFull(const std::vector<std::vector<double> >& m)
{
    FAS_VectorSolutions ret;
    if(m.size()!=2)  return ret;
    if( m[0].size() ==3 || m[1].size()==3 )
    {
        return simultaneousQuadraticSolverMixed(m);
    }
    if(m[0].size()!=6 || m[1].size()!=6) return ret;
    /** eliminate x, quartic equation of y **/
    auto& a=m[0][0];
    auto& b=m[0][1];
    auto& c=m[0][2];
    auto& d=m[0][3];
    auto& e=m[0][4];
    auto& f=m[0][5];

    auto& g=m[1][0];
    auto& h=m[1][1];
    auto& i=m[1][2];
    auto& j=m[1][3];
    auto& k=m[1][4];
    auto& l=m[1][5];
    double a2=a*a;
    double b2=b*b;
    double c2=c*c;
    double d2=d*d;
    double e2=e*e;
    double f2=f*f;

    double g2=g*g;
    double  h2=h*h;
    double  i2=i*i;
    double  j2=j*j;
    double  k2=k*k;
    double  l2=l*l;
    std::vector<double> qy(5,0.);
    //y^4
    qy[4]=-c2*g2 + b*c*g*h - a*c*h2 - b2*g*i + 2.*a*c*g*i + a*b*h*i - a2*i2;
    //y^3
    qy[3]=-2.*c*e*g2 + c*d*g*h + b*e*g*h - a*e*h2 - 2.*b*d*g*i + 2.*a*e*g*i + a*d*h*i +
            b*c*g*j - 2.*a*c*h*j + a*b*i*j - b2*g*k + 2.*a*c*g*k + a*b*h*k - 2.*a2*i*k;
    //y^2
    qy[2]=(-e2*g2 + d*e*g*h - d2*g*i + c*d*g*j + b*e*g*j - 2.*a*e*h*j + a*d*i*j - a*c*j2 -
           2.*b*d*g*k + 2.*a*e*g*k + a*d*h*k + a*b*j*k - a2*k2 - b2*g*l + 2.*a*c*g*l + a*b*h*l - 2.*a2*i*l)
            - (2.*c*f*g2 - b*f*g*h + a*f*h2 - 2.*a*f*g*i);
    //y
    qy[1]=(d*e*g*j - a*e*j2 - d2*g*k + a*d*j*k - 2.*b*d*g*l + 2.*a*e*g*l + a*d*h*l + a*b*j*l - 2.*a2*k*l)
            -(2.*e*f*g2 - d*f*g*h - b*f*g*j + 2.*a*f*h*j - 2.*a*f*g*k);
    //y^0
    qy[0]=-d2*g*l + a*d*j*l - a2*l2
            - ( f2*g2 - d*f*g*j + a*f*j2 - 2.*a*f*g*l);

    //quarticSolver
    auto roots=quarticSolverFull(qy);
    if (roots.size()==0 ) { // no intersection found
        return ret;
    }
    std::vector<double> ce(0,0.);

    for(size_t i0=0;i0<roots.size();i0++)
    {
        ce.resize(3);
        ce[0]=a;
        ce[1]=b*roots[i0]+d;
        ce[2]=c*roots[i0]*roots[i0]+e*roots[i0]+f;
        if(fabs(ce[0])<1e-75 && fabs(ce[1])<1e-75)
        {
            ce[0]=g;
            ce[1]=h*roots[i0]+j;
            ce[2]=i*roots[i0]*roots[i0]+k*roots[i0]+f;
        }
        if(fabs(ce[0])<1e-75 && fabs(ce[1])<1e-75)
            continue;

        if(fabs(a)>1e-75)
        {
            std::vector<double> ce2(2,0.);
            ce2[0]=ce[1]/ce[0];
            ce2[1]=ce[2]/ce[0];
            auto xRoots=quadraticSolver(ce2);
            for(size_t j0=0;j0<xRoots.size();j0++)
            {
                FAS_Vector vp(xRoots[j0],roots[i0]);
                if(simultaneousQuadraticVerify(m,vp)) ret.push_back(vp);
            }
            continue;
        }
        FAS_Vector vp(-ce[2]/ce[1],roots[i0]);
        if(simultaneousQuadraticVerify(m,vp)) ret.push_back(vp);
    }
    return ret;
}

FAS_VectorSolutions FAS_Math::simultaneousQuadraticSolverMixed(const std::vector<std::vector<double> >& m)
{
    FAS_VectorSolutions ret;
    auto p0=& (m[0]);
    auto p1=& (m[1]);
    if(p1->size()==3)
    {
        std::swap(p0,p1);
    }
    if(p1->size()==3)
    {
        //linear
        std::vector<double> sn(2,0.);
        std::vector<std::vector<double> > ce;
        ce.push_back(m[0]);
        ce.push_back(m[1]);
        ce[0][2]=-ce[0][2];
        ce[1][2]=-ce[1][2];
        if( FAS_Math::linearSolver(ce,sn)) ret.push_back(FAS_Vector(sn[0],sn[1]));
        return ret;
    }
    const double& a=p0->at(0);
    const double& b=p0->at(1);
    const double& c=p0->at(2);
    const double& d=p1->at(0);
    const double& e=p1->at(1);
    const double& f=p1->at(2);
    const double& g=p1->at(3);
    const double& h=p1->at(4);
    const double& i=p1->at(5);

    std::vector<double> ce(3,0.);
    const double& a2=a*a;
    const double& b2=b*b;
    const double& c2=c*c;
    ce[0]= -f*a2+a*b*e-b2*d;
    ce[1]=a*b*g-a2*h- (2*b*c*d-a*c*e);
    ce[2]=a*c*g-c2*d-a2*i;
    std::vector<double> roots(0,0.);
    if( fabs(ce[1])>FAS_TOLERANCE15 && fabs(ce[0]/ce[1])<FAS_TOLERANCE15)
    {
        roots.push_back( - ce[2]/ce[1]);
    }
    else
    {
        std::vector<double> ce2(2,0.);
        ce2[0]=ce[1]/ce[0];
        ce2[1]=ce[2]/ce[0];
        roots=quadraticSolver(ce2);
    }

    if(roots.size()==0)
    {
        return FAS_VectorSolutions();
    }
    for(size_t i=0;i<roots.size();i++)
    {
        ret.push_back(FAS_Vector(-(b*roots.at(i)+c)/a,roots.at(i)));
    }

    return ret;

}

// verify a solution for simultaneousQuadratic
bool FAS_Math::simultaneousQuadraticVerify(const std::vector<std::vector<double> >& m, FAS_Vector& v)
{
    FAS_Vector v0=v;
    auto& a=m[0][0];
    auto& b=m[0][1];
    auto& c=m[0][2];
    auto& d=m[0][3];
    auto& e=m[0][4];
    auto& f=m[0][5];

    auto& g=m[1][0];
    auto& h=m[1][1];
    auto& i=m[1][2];
    auto& j=m[1][3];
    auto& k=m[1][4];
    auto& l=m[1][5];
    // verifying the equations to floating point tolerance by terms
    double sum0=0., sum1=0.;
    double f00=0.,f01=0.;
    double amax0, amax1;
    for(size_t i0=0; i0<20; ++i0)
    {
        double& x=v.x;
        double& y=v.y;
        double x2=x*x;
        double y2=y*y;
        double const terms0[12]={ a*x2, b*x*y, c*y2, d*x, e*y, f, g*x2, h*x*y, i*y2, j*x, k*y, l};
        amax0=fabs(terms0[0]), amax1=fabs(terms0[6]);
        double px=2.*a*x+b*y+d;
        double py=b*x+2.*c*y+e;
        sum0=0.;
        for(int i=0; i<6; i++)
        {
            if(amax0<fabs(terms0[i])) amax0=fabs(terms0[i]);
            sum0 += terms0[i];
        }
        std::vector<std::vector<double>> nrCe;
        nrCe.push_back(std::vector<double>{px, py, sum0});
        px=2.*g*x+h*y+j;
        py=h*x+2.*i*y+k;
        sum1=0.;
        for(int i=6; i<12; i++)
        {
            if(amax1<fabs(terms0[i])) amax1=fabs(terms0[i]);
            sum1 += terms0[i];
        }
        nrCe.push_back(std::vector<double>{px, py, sum1});
        std::vector<double> dn;
        bool ret=linearSolver(nrCe, dn);
        if(!i0)
        {
            f00=sum0;
            f01=sum1;
        }
        if(!ret) break;
        v -= FAS_Vector(dn[0], dn[1]);
    }
    if( fabs(sum0)> fabs(f00) && fabs(sum1)>fabs(f01))
    {
        v=v0;
        sum0=f00;
        sum1=f01;
    }
    const double tols=2.*sqrt(6.)*sqrt(DBL_EPSILON); //experimental tolerances to verify simultaneous quadratic
    return (amax0<=tols || fabs(sum0)/amax0<tols) &&  (amax1<=tols || fabs(sum1)/amax1<tols);
}
//EOF
