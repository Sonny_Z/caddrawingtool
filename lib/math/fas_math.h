/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_MATH_H
#define FAS_MATH_H

#include <vector>

class FAS_Vector;
class FAS_VectorSolutions;
class QString;

#ifndef M_PI
#define M_PI       3.141592653589793238462643
#endif
#ifndef M_PI_2
#define M_PI_2       1.57079632679489661923
#endif
#ifndef M_PI
#define M_PIx2      6.283185307179586 // 2*PI
#endif

class FAS_Math
{
private:
    FAS_Math() = delete;
public:
    static int round(double v);
    static double pow(double x, double y);
    static FAS_Vector pow(FAS_Vector x, double y);

    static double rad2deg(double a);
    static double deg2rad(double a);
    static double rad2gra(double a);
    static double gra2rad(double a);
    static unsigned findGCD(unsigned a, unsigned b);
    static bool isAngleBetween(double a,
                               double a1, double a2,
                               bool reversed = false);
    // correct angle to be within [0, 2 Pi)
    static double correctAngle(double a);
    // correct angle to be undirectional [0, Pi)
    static double correctAngleU(double a);

    // angular difference
    static double getAngleDifference(double a1, double a2, bool reversed = false);
    //return the minimum of angular difference a1-a2 and a2-a1
    static double getAngleDifferenceU(double a1, double a2);
    static double makeAngleReadable(double angle, bool readable=true, bool* corrected=nullptr);
    static bool isAngleReadable(double angle);
    static bool isSameDirection(double dir1, double dir2, double tol);

    static std::vector<double> quadraticSolver(const std::vector<double>& ce);
    static std::vector<double> cubicSolver(const std::vector<double>& ce);
    /* quartic solver
    * x^4 + ce[0] x^3 + ce[1] x^2 + ce[2] x + ce[3] = 0
    ce, a vector of size 4 contains the coefficient in order
    return, a vector contains real roots
    **/
    static std::vector<double> quarticSolver(const std::vector<double>& ce);
    /* quartic solver
* ce[4] x^4 + ce[3] x^3 + ce[2] x^2 + ce[1] x + ce[0] = 0
    ce, a vector of size 5 contains the coefficient in order
    return, a vector contains real roots
    **/
    static std::vector<double> quarticSolverFull(const std::vector<double>& ce);
    //solver for linear equation set
    /*
      * Solve linear equation set
      *param mt holds the augmented matrix
      *param sn holds the solution
      *param return true, if the equation set has a unique solution, return false otherwise
      */
    static bool linearSolver(const std::vector<std::vector<double> >& m, std::vector<double>& sn);
    /* solver quadratic simultaneous equations of a set of two **/
    /* solve the following quadratic simultaneous equations,
      * ma000 x^2 + ma011 y^2 - 1 =0
      * ma100 x^2 + 2 ma101 xy + ma111 y^2 + mb10 x + mb11 y +mc1 =0
      *
      *m, a vector of size 8 contains coefficients in the strict order of:
      ma000 ma011 ma100 ma101 ma111 mb10 mb11 mc1
      *return a FAS_VectorSolutions contains real roots (x,y)
      */
    static FAS_VectorSolutions simultaneousQuadraticSolver(const std::vector<double>& m);

    /* solver quadratic simultaneous equations of a set of two **/
    /* solve the following quadratic simultaneous equations,
      * ma000 x^2 + ma001 xy + ma011 y^2 + mb00 x + mb01 y + mc0 =0
      * ma100 x^2 + ma101 xy + ma111 y^2 + mb10 x + mb11 y + mc1 =0
      *
  *@param m a vector of size 2 each contains a vector of size 6 coefficients in the strict order of:
  ma000 ma001 ma011 mb00 mb01 mc0
  ma100 ma101 ma111 mb10 mb11 mc1
      *@return a FAS_VectorSolutions contains real roots (x,y)
      */
    static FAS_VectorSolutions simultaneousQuadraticSolverFull(const std::vector<std::vector<double> >& m);
    static FAS_VectorSolutions simultaneousQuadraticSolverMixed(const std::vector<std::vector<double> >& m);

    //brief verify simultaneousQuadraticVerify a solution for simultaneousQuadratic
    static bool simultaneousQuadraticVerify(const std::vector<std::vector<double> >& m, FAS_Vector& v);
    // wrapper for elliptic integral **/
    static double ellipticIntegral_2(const double& k, const double& phi);
    static QString doubleToString(double value, double prec);
    static QString doubleToString(double value, int prec);
};

#endif
