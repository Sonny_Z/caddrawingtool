#-------------------------------------------------
#
# Project created by QtCreator 2018-08-14T19:32:53
#
#-------------------------------------------------

QT       += widgets
CONFIG   += c++11

TARGET = log4Qt
TEMPLATE = lib

DEFINES += LOG4QT_LIBRARY
include(log4qt/log4qt.pri)

CONFIG  += debug_and_release
CONFIG(debug,debug|release){
    TARGET  = liblog
    DESTDIR = $${PWD}
}else{
    TARGET  = log4Qt
    DESTDIR = $${PWD}
}

SOURCES += \
    qblog4qt.cpp

HEADERS +=\
        log4qt_global.h \
    qblog4qt.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
