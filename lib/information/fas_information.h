/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_INFORMATION_H
#define FAS_INFORMATION_H

#include "fas.h"

class FAS_Ellipse;
class FAS_Entity;
class FAS_EntityContainer;
class FAS_Vector;
class FAS_VectorSolutions;
class FAS_Arc;
class FAS_Circle;
class FAS_Line;

/*
 * Class for getting information about entities. This includes
 * also things like the end point of an element which is
 * nearest to a given coordinate.
 * There's no interaction handled in this class.
 * This class is bound to an entity container.
 */
class FAS_Information {
public:
    FAS_Information(FAS_EntityContainer& entityContainer);

    static bool isDimension(FAS2::EntityType type);
    static bool isTrimmable(FAS_Entity* e);
    static bool isTrimmable(FAS_Entity* e1, FAS_Entity* e2);

    FAS_Vector getNearestEndpoint(const FAS_Vector& point, double* dist = nullptr) const;
    FAS_Vector getNearestPointOnEntity(const FAS_Vector& point,
                                       bool onEntity=true,
                                       double* dist = nullptr,
                                       FAS_Entity** entity=nullptr) const;
    FAS_Entity* getNearestEntity(const FAS_Vector& point,
                                 double* dist = nullptr,
                                 FAS2::ResolveLevel level=FAS2::ResolveAll) const;

    static FAS_VectorSolutions getIntersection(FAS_Entity const* e1,
                                               FAS_Entity const* e2,
                                               bool onEntities = false);
    static FAS_VectorSolutions getIntersectionLineLine(FAS_Line* e1,
                                                       FAS_Line* e2);
    static FAS_VectorSolutions getIntersectionLineArc(FAS_Line* line,
                                                      FAS_Arc* arc);
    static FAS_VectorSolutions getIntersectionArcArc(FAS_Entity const* e1,
                                                     FAS_Entity const* e2);
    static FAS_VectorSolutions getIntersectionEllipseEllipse(
            FAS_Ellipse const* e1,
            FAS_Ellipse const* e2);
    static FAS_VectorSolutions getIntersectionArcEllipse(FAS_Arc* e1,
                                                         FAS_Ellipse* e2);
    static FAS_VectorSolutions getIntersectionCircleEllipse(FAS_Circle* e1,
                                                            FAS_Ellipse* e2);
    static FAS_VectorSolutions getIntersectionEllipseLine(FAS_Line* line,
                                                          FAS_Ellipse* ellipse);
    // createQuadrilateral form quadrilateral from 4 straight lines
    static FAS_VectorSolutions createQuadrilateral(const FAS_EntityContainer& container);
    static bool isPointInsideContour(const FAS_Vector& point,
                                     FAS_EntityContainer* contour,
                                     bool* onContour=nullptr);
private:
    FAS_EntityContainer* container;
};

#endif
