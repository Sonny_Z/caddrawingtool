/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_information.h"

//sort with alphabetical order
#include <vector>

#include "fas_arc.h"
#include "fas_circle.h"
#include "fas_ellipse.h"
#include "fas_entitycontainer.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_polyline.h"
#include "fas_vector.h"
#include "lc_quadratic.h"
#include "lc_rect.h"
#include "lc_splinepoints.h"

FAS_Information::FAS_Information(FAS_EntityContainer& container):
    container(&container)
{
}

//return true: if the entity is a dimensioning enity.
bool FAS_Information::isDimension(FAS2::EntityType type)
{
    switch(type)
    {
    case FAS2::EntityDimAligned:
    case FAS2::EntityDimLinear:
    case FAS2::EntityDimRadial:
    case FAS2::EntityDimDiametric:
    case FAS2::EntityDimAngular:
        return true;
    default:
        return false;
    }
}

// true the entity can be trimmed. i.e. it is in a graphic or in a polyline.
bool FAS_Information::isTrimmable(FAS_Entity* e)
{
    if (e)
    {
        if (e->getParent())
        {
            switch(e->getParent()->rtti())
            {
            case FAS2::EntityPolyline:
            case FAS2::EntityContainer:
            case FAS2::EntityGraphic:
            case FAS2::EntityBlock:
                return true;
            default:
                return false;
            }
        }
    }
    return false;
}


// true the two entities can be trimmed to each other; i.e. they are in a graphic or in the same polyline.
bool FAS_Information::isTrimmable(FAS_Entity* e1, FAS_Entity* e2)
{
    if (e1 && e2)
    {
        if (e1->getParent() && e2->getParent())
        {
            if (e1->getParent()->rtti()==FAS2::EntityPolyline &&
                    e2->getParent()->rtti()==FAS2::EntityPolyline &&
                    e1->getParent()==e2->getParent())
            {

                // in the same polyline
                FAS_Polyline* pl = static_cast<FAS_Polyline *>(e1->getParent());
                int idx1 = pl->findEntity(e1);
                int idx2 = pl->findEntity(e2);
                if (abs(idx1-idx2)==1 ||
                        (pl->isClosed() && abs(idx1-idx2)==int(pl->count()-1)))
                {
                    // directly following entities
                    return true;
                }
                else
                {
                    // not directly following entities
                    return false;
                }
            }
            else if ((e1->getParent()->rtti()==FAS2::EntityContainer ||
                      e1->getParent()->rtti()==FAS2::EntityGraphic ||
                      e1->getParent()->rtti()==FAS2::EntityBlock) &&
                     (e2->getParent()->rtti()==FAS2::EntityContainer ||
                      e2->getParent()->rtti()==FAS2::EntityGraphic ||
                      e2->getParent()->rtti()==FAS2::EntityBlock))
            {
                return true;
            }
        }
        else
        {
            return (e1->getParent()==e2->getParent());
        }
    }
    return false;
}


// Gets the nearest end point to the given coordinate.
FAS_Vector FAS_Information::getNearestEndpoint(const FAS_Vector& coord, double* dist) const
{
    return container->getNearestEndpoint(coord, dist);
}

// Gets the nearest point to the given coordinate which is on an entity.
FAS_Vector FAS_Information::getNearestPointOnEntity(const FAS_Vector& coord,
                                                    bool onEntity,
                                                    double* dist,
                                                    FAS_Entity** entity) const
{
    return container->getNearestPointOnEntity(coord, onEntity, dist, entity);
}


// Gets the nearest entity to the given coordinate.t all in this graphics container.

FAS_Entity* FAS_Information::getNearestEntity(const FAS_Vector& coord,
                                              double* dist,
                                              FAS2::ResolveLevel level) const
{
    return container->getNearestEntity(coord, dist, level);
}



// Calculates the intersection point(s) between two entities.
FAS_VectorSolutions FAS_Information::getIntersection(FAS_Entity const* e1,
                                                     FAS_Entity const* e2, bool onEntities)
{

    FAS_VectorSolutions ret;
    const double tol = 1.0e-4;

    if (!(e1 && e2) )
    {
        return ret;
    }
    if (e1->getId() == e2->getId())
    {
        return ret;
    }

    // unsupported entities / entity combinations:
    if (e1->rtti()==FAS2::EntityMText || e2->rtti()==FAS2::EntityMText ||
            e1->rtti()==FAS2::EntityText || e2->rtti()==FAS2::EntityText ||
            isDimension(e1->rtti()) || isDimension(e2->rtti())) {
        return ret;
    }

    if (onEntities && !(e1->isConstruction() || e2->isConstruction()))
    {
        LC_Rect const rect1{e1->getMin(), e1->getMax()};
        LC_Rect const rect2{e2->getMin(), e2->getMax()};

        if (onEntities && !rect1.intersects(rect2, FAS_TOLERANCE))
        {
            return ret;
        }
    }

    if ( e1->getParent() && e1->getParent() == e2->getParent())
    {
        if ( e1->getParent()->rtti()==FAS2::EntitySpline ) {
            //do not calculate intersections from neighboring lines of a spline
            if ( abs(e1->getParent()->findEntity(e1) - e1->getParent()->findEntity(e2)) <= 1 )
            {
                return ret;
            }
        }
    }

    if(e1->rtti() == FAS2::EntitySplinePoints || e2->rtti() == FAS2::EntitySplinePoints)
    {
        ret = LC_SplinePoints::getIntersection(e1, e2);
    }
    else
    {
        auto isArc = [](FAS_Entity const* e)
        {
            auto type = e->rtti();
            return type == FAS2::EntityCircle || type == FAS2::EntityArc;
        };

        if(isArc(e1) && isArc(e2))
        {
            ret=getIntersectionArcArc(e1, e2);
        }
        else
        {
            const auto qf1=e1->getQuadratic();
            const auto qf2=e2->getQuadratic();
            ret=LC_Quadratic::getIntersection(qf1,qf2);
        }
    }
    FAS_VectorSolutions ret2;
    for(const FAS_Vector& vp: ret)
    {
        if (!vp.valid) continue;
        if (onEntities)
        {
            //ignore intersections not on entity
            if (!((e1->isConstruction(true) || e1->isPointOnEntity(vp, tol)) &&
                  (e2->isConstruction(true) || e2->isPointOnEntity(vp, tol))))
            {
                continue;
            }
        }
        // need to test whether the intersection is tangential
        FAS_Vector direction1=e1->getTangentDirection(vp);
        FAS_Vector direction2=e2->getTangentDirection(vp);
        if( direction1.valid && direction2.valid && fabs(fabs(direction1.dotP(direction2)) - sqrt(direction1.squared()*direction2.squared())) < sqrt(tol)*tol )
            ret2.setTangent(true);
        ret2.push_back(vp);
    }

    return ret2;
}


//return Intersection between two lines.
FAS_VectorSolutions FAS_Information::getIntersectionLineLine(FAS_Line* e1, FAS_Line* e2)
{
    FAS_VectorSolutions ret;
    if (!(e1 && e2))
    {
        return ret;
    }
    FAS_Vector p1 = e1->getStartpoint();
    FAS_Vector p2 = e1->getEndpoint();
    FAS_Vector p3 = e2->getStartpoint();
    FAS_Vector p4 = e2->getEndpoint();

    double num = ((p4.x-p3.x)*(p1.y-p3.y) - (p4.y-p3.y)*(p1.x-p3.x));
    double div = ((p4.y-p3.y)*(p2.x-p1.x) - (p4.x-p3.x)*(p2.y-p1.y));

    if (fabs(div)>FAS_TOLERANCE &&
            fabs(remainder(e1->getAngle1()-e2->getAngle1(), M_PI))>=FAS_TOLERANCE*10.)
    {
        double u = num / div;
        double xs = p1.x + u * (p2.x-p1.x);
        double ys = p1.y + u * (p2.y-p1.y);
        ret = FAS_VectorSolutions({FAS_Vector(xs, ys)});
    }
    // lines are parallel
    else
    {
        ret = FAS_VectorSolutions();
    }

    return ret;
}



//return One or two intersection points between given entities.
FAS_VectorSolutions FAS_Information::getIntersectionLineArc(FAS_Line* line, FAS_Arc* arc)
{
    FAS_VectorSolutions ret;
    if (!(line && arc)) return ret;

    double dist=0.0;
    FAS_Vector nearest;
    nearest = line->getNearestPointOnEntity(arc->getCenter(), false, &dist);

    // special case: arc touches line (tangent):
    if (nearest.valid && fabs(dist - arc->getRadius()) < 1.0e-4)
    {
        ret = FAS_VectorSolutions({nearest});
        ret.setTangent(true);
        return ret;
    }

    FAS_Vector p = line->getStartpoint();
    FAS_Vector d = line->getEndpoint() - line->getStartpoint();
    double d2=d.squared();
    FAS_Vector c = arc->getCenter();
    double r = arc->getRadius();
    FAS_Vector delta = p - c;
    if (d2<FAS_TOLERANCE2)
    {
        //line too short, still check the whether the line touches the arc
        if ( fabs(delta.squared() - r*r) < 2.*FAS_TOLERANCE*r )
        {
            return FAS_VectorSolutions({line->getMiddlePoint()});
        }
        return ret;
    }


    //intersection
    // solution = p + t d;
    //| p -c+ t d|^2 = r^2
    // |d|^2 t^2 + 2 (p-c).d t + |p-c|^2 -r^2 = 0
    double a1 = FAS_Vector::dotP(delta,d);
    double term1 = a1*a1 - d2*(delta.squared()-r*r);
    if( term1 < - FAS_TOLERANCE)
    {
        return ret;
    }
    else
    {
        term1=fabs(term1);
        if( term1 < FAS_TOLERANCE * d2 )
        {
            ret=FAS_VectorSolutions({line->getNearestPointOnEntity(c, false)});
            ret.setTangent(true);
            return ret;
        }
        double t = sqrt(fabs(term1));
        //two intersections
        return FAS_VectorSolutions({ p + d*(t-a1)/d2, p -d*(t+a1)/d2});
    }
}

//return One or two intersection points between given entities.
FAS_VectorSolutions FAS_Information::getIntersectionArcArc(FAS_Entity const* e1, FAS_Entity const* e2)
{
    FAS_VectorSolutions ret;
    if (!(e1 && e2))
        return ret;

    if(e1->rtti() != FAS2::EntityArc && e1->rtti() != FAS2::EntityCircle)
        return ret;
    if(e2->rtti() != FAS2::EntityArc && e2->rtti() != FAS2::EntityCircle)
        return ret;

    FAS_Vector c1 = e1->getCenter();
    FAS_Vector c2 = e2->getCenter();
    double r1 = e1->getRadius();
    double r2 = e2->getRadius();

    FAS_Vector u = c2 - c1;
    // concentric
    if (u.magnitude()<1.0e-6)
    {
        return ret;
    }
    FAS_Vector v = FAS_Vector(u.y, -u.x);
    double s, t1, t2, term;
    s = 1.0/2.0 * ((r1*r1 - r2*r2)/(FAS_Math::pow(u.magnitude(), 2.0)) + 1.0);
    term = (r1*r1)/(FAS_Math::pow(u.magnitude(), 2.0)) - s*s;
    // no intersection:
    if (term<0.0)
    {
        ret = FAS_VectorSolutions();
    }
    // one or two intersections:
    else
    {
        t1 = sqrt(term);
        t2 = -sqrt(term);
        bool tangent = false;
        FAS_Vector sol1 = c1 + u*s + v*t1;
        FAS_Vector sol2 = c1 + u*s + v*t2;

        if (sol1.distanceTo(sol2)<1.0e-4)
        {
            sol2 = FAS_Vector(false);
            ret = FAS_VectorSolutions({sol1});
            tangent = true;
        }
        else
        {
            ret = FAS_VectorSolutions({sol1, sol2});
        }
        ret.setTangent(tangent);
    }
    return ret;
}

FAS_VectorSolutions FAS_Information::getIntersectionEllipseEllipse(
        FAS_Ellipse const* e1, FAS_Ellipse const* e2)
{
    FAS_VectorSolutions ret;
    if (!(e1 && e2) )
    {
        return ret;
    }
    if ((e1->getCenter() - e2 ->getCenter()).squared() < FAS_TOLERANCE2 &&
            (e1->getMajorP() - e2 ->getMajorP()).squared() < FAS_TOLERANCE2 &&
            fabs(e1->getRatio() - e2 ->getRatio()) < FAS_TOLERANCE)
    {
        // overlapped ellipses, do not do overlap
        return ret;
    }
    FAS_Ellipse ellipse01(nullptr,e1->getData());
    FAS_Ellipse *e01= & ellipse01;
    if( e01->getMajorRadius() < e01->getMinorRadius() ) e01->switchMajorMinor();
    FAS_Ellipse ellipse02(nullptr,e2->getData());
    FAS_Ellipse *e02= &ellipse02;
    if( e02->getMajorRadius() < e02->getMinorRadius() ) e02->switchMajorMinor();
    //transform ellipse2 to ellipse1's coordinates
    FAS_Vector shiftc1=- e01->getCenter();
    double shifta1=-e01->getAngle();
    e02->move(shiftc1);
    e02->rotate(shifta1);
    double a1=e01->getMajorRadius();
    double b1=e01->getMinorRadius();
    double x2=e02->getCenter().x, y2=e02->getCenter().y;
    double a2=e02->getMajorRadius();
    double b2=e02->getMinorRadius();

    if( e01->getMinorRadius() < FAS_TOLERANCE || e01 -> getRatio()< FAS_TOLERANCE)
    {
        // treate e01 as a line
        FAS_Line line{e1->getParent(), {{-a1,0.}, {a1,0.}}};
        ret= getIntersectionEllipseLine(&line, e02);
        ret.rotate(-shifta1);
        ret.move(-shiftc1);
        return ret;
    }
    if( e02->getMinorRadius() < FAS_TOLERANCE || e02 -> getRatio()< FAS_TOLERANCE)
    {
        // treate e02 as a line
        FAS_Line line{e1->getParent(), {{-a2,0.}, {a2,0.}}};
        line.rotate({0.,0.}, e02->getAngle());
        line.move(e02->getCenter());
        ret = getIntersectionEllipseLine(&line, e01);
        ret.rotate(-shifta1);
        ret.move(-shiftc1);
        return ret;
    }

    //ellipse01 equation:
    // x^2/(a1^2) + y^2/(b1^2) - 1 =0
    double t2= - e02->getAngle();
    double cs=cos(t2),si=sin(t2);
    double ucs=x2*cs,usi=x2*si, vcs=y2*cs,vsi=y2*si;
    double cs2=cs*cs,si2=1-cs2;
    double tcssi=2.*cs*si;
    double ia2=1./(a2*a2),ib2=1./(b2*b2);
    std::vector<double> m(0,0.);
    m.push_back( 1./(a1*a1)); //ma000
    m.push_back( 1./(b1*b1)); //ma011
    m.push_back(cs2*ia2 + si2*ib2); //ma100
    m.push_back(cs*si*(ib2 - ia2)); //ma101
    m.push_back(si2*ia2 + cs2*ib2); //ma111
    m.push_back(( y2*tcssi - 2.*x2*cs2)*ia2 - ( y2*tcssi+2*x2*si2)*ib2); //mb10
    m.push_back( ( x2*tcssi - 2.*y2*si2)*ia2 - ( x2*tcssi+2*y2*cs2)*ib2); //mb11
    m.push_back((ucs - vsi)*(ucs-vsi)*ia2+(usi+vcs)*(usi+vcs)*ib2 -1.); //mc1
    auto vs0=FAS_Math::simultaneousQuadraticSolver(m);
    shifta1 = - shifta1;
    shiftc1 = - shiftc1;
    for(FAS_Vector vp: vs0)
    {
        vp.rotate(shifta1);
        vp.move(shiftc1);
        ret.push_back(vp);
    }
    return ret;
}

//wrapper to do Circle-Ellipse and Arc-Ellipse using Ellipse-Ellipse intersection
FAS_VectorSolutions FAS_Information::getIntersectionCircleEllipse(FAS_Circle* c1, FAS_Ellipse* e1)
{
    FAS_VectorSolutions ret;
    if (!(c1 && e1))
        return ret;

    FAS_Ellipse const e2{c1->getParent(),
        {c1->getCenter(), {c1->getRadius(),0.}, 1.0, 0., 2.*M_PI, false}};
    return getIntersectionEllipseEllipse(e1, &e2);
}

FAS_VectorSolutions FAS_Information::getIntersectionArcEllipse(FAS_Arc * a1, FAS_Ellipse* e1)
{
    FAS_VectorSolutions ret;
    if (!(a1 && e1))
    {
        return ret;
    }
    FAS_Ellipse const e2{a1->getParent(),
        {a1->getCenter(),
            {a1->getRadius(), 0.},
            1.0,
            a1->getAngle1(), a1->getAngle2(),
                    a1->isReversed()}};
    return getIntersectionEllipseEllipse(e1, &e2);
}

//return One or two intersection points between given entities.
FAS_VectorSolutions FAS_Information::getIntersectionEllipseLine(FAS_Line* line, FAS_Ellipse* ellipse)
{
    FAS_VectorSolutions ret;
    if (!(line && ellipse))
        return ret;

    // rotate into normal position:
    double rx = ellipse->getMajorRadius();
    if(rx<FAS_TOLERANCE)
    {
        //zero radius ellipse
        FAS_Vector vp(line->getNearestPointOnEntity(ellipse->getCenter(), true));
        if((vp-ellipse->getCenter()).squared() <FAS_TOLERANCE2)
        {
            //center on line
            ret.push_back(vp);
        }
        return ret;
    }
    FAS_Vector angleVector = ellipse->getMajorP().scale(FAS_Vector(1./rx,-1./rx));
    double ry = rx*ellipse->getRatio();
    FAS_Vector center = ellipse->getCenter();
    FAS_Vector a1 = line->getStartpoint().rotate(center, angleVector);
    FAS_Vector a2 = line->getEndpoint().rotate(center, angleVector);
    //    FAS_Vector origin = a1;
    FAS_Vector dir = a2-a1;
    FAS_Vector diff = a1 - center;
    FAS_Vector mDir = FAS_Vector(dir.x/(rx*rx), dir.y/(ry*ry));
    FAS_Vector mDiff = FAS_Vector(diff.x/(rx*rx), diff.y/(ry*ry));

    double a = FAS_Vector::dotP(dir, mDir);
    double b = FAS_Vector::dotP(dir, mDiff);
    double c = FAS_Vector::dotP(diff, mDiff) - 1.0;
    double d = b*b - a*c;

    if (d < - 1.e3*FAS_TOLERANCE*sqrt(FAS_TOLERANCE))
    {
        return ret;
    }
    if( d < 0. ) d=0.;
    double root = sqrt(d);
    double t_a = -b/a;
    double t_b = root/a;

    ret.push_back(a1.lerp(a2,t_a+t_b));
    FAS_Vector vp(a1.lerp(a2,t_a-t_b));
    if ( (ret.get(0)-vp).squared()>FAS_TOLERANCE2)
    {
        ret.push_back(vp);
    }
    angleVector.y *= -1.;
    ret.rotate(center, angleVector);
    return ret;
}

// Checks if the given coordinate is inside the given contour.
bool FAS_Information::isPointInsideContour(const FAS_Vector& point,
                                           FAS_EntityContainer* contour, bool* onContour)
{
    if (!contour)
    {
        return false;
    }

    if (point.x < contour->getMin().x || point.x > contour->getMax().x ||
            point.y < contour->getMin().y || point.y > contour->getMax().y)
    {
        return false;
    }

    double width = contour->getSize().x+1.0;
    bool sure;
    int counter;
    int tries = 0;
    double rayAngle = 0.0;
    do {
        sure = true;
        // create ray:
        FAS_Vector v = FAS_Vector::polar(width*10.0, rayAngle);
        FAS_Line ray{point, point+v};
        counter = 0;
        FAS_VectorSolutions sol;

        if (onContour)
        {
            *onContour = false;
        }

        for (FAS_Entity* e = contour->firstEntity(FAS2::ResolveAll); e; e = contour->nextEntity(FAS2::ResolveAll))
        {
            sol = FAS_Information::getIntersection(&ray, e, true);
            for (int i=0; i<=1; ++i)
            {
                FAS_Vector p = sol.get(i);
                if (p.valid)
                {
                    // point is on the contour itself
                    if (p.distanceTo(point)<1.0e-5)
                    {
                        if (onContour)
                        {
                            *onContour = true;
                        }
                    }
                    else
                    {
                        if (e->rtti()==FAS2::EntityLine)
                        {
                            FAS_Line* line = (FAS_Line*)e;
                            // ray goes through startpoint of line:
                            if (p.distanceTo(line->getStartpoint())<1.0e-4)
                            {
                                if (FAS_Math::correctAngle(line->getAngle1())<M_PI)
                                {
                                    sure = false;
                                }
                            }
                            // ray goes through endpoint of line:
                            else if (p.distanceTo(line->getEndpoint())<1.0e-4)
                            {
                                if (FAS_Math::correctAngle(line->getAngle2())<M_PI)
                                {
                                    sure = false;
                                }
                            }
                            counter++;
                            
                        }
                        else if (e->rtti()==FAS2::EntityArc)
                        {
                            FAS_Arc* arc = (FAS_Arc*)e;
                            if (p.distanceTo(arc->getStartpoint())<1.0e-4)
                            {
                                double dir = arc->getDirection1();
                                if ((dir<M_PI && dir>=1.0e-5) ||
                                        ((dir>2*M_PI-1.0e-5 || dir<1.0e-5) &&
                                         arc->getCenter().y>p.y))
                                {
                                    counter++;
                                    sure = false;
                                }
                            }
                            else if (p.distanceTo(arc->getEndpoint())<1.0e-4)
                            {
                                double dir = arc->getDirection2();
                                if ((dir<M_PI && dir>=1.0e-5) ||
                                        ((dir>2*M_PI-1.0e-5 || dir<1.0e-5) &&
                                         arc->getCenter().y>p.y))
                                {
                                    counter++;
                                    sure = false;
                                }
                            } else
                            {
                                counter++;
                            }
                        } else if (e->rtti()==FAS2::EntityCircle)
                        {
                            // tangent:
                            if (i==0 && sol.get(1).valid==false)
                            {
                                if (!sol.isTangent())
                                {
                                    counter++;
                                }
                                else
                                {
                                    sure = false;
                                }
                            }
                            else if (i==1 || sol.get(1).valid==true)
                            {
                                counter++;
                            }
                        }
                        else if (e->rtti()==FAS2::EntityEllipse)
                        {
                            FAS_Ellipse* ellipse=static_cast<FAS_Ellipse*>(e);
                            if(ellipse->isArc())
                            {
                                if (p.distanceTo(ellipse->getStartpoint())<1.0e-4)
                                {
                                    double dir = ellipse->getDirection1();
                                    if ((dir<M_PI && dir>=1.0e-5) ||
                                            ((dir>2*M_PI-1.0e-5 || dir<1.0e-5) &&
                                             ellipse->getCenter().y>p.y))
                                    {
                                        counter++;
                                        sure = false;
                                    }
                                }
                                else if (p.distanceTo(ellipse->getEndpoint())<1.0e-4)
                                {
                                    double dir = ellipse->getDirection2();
                                    if ((dir<M_PI && dir>=1.0e-5) ||
                                            ((dir>2*M_PI-1.0e-5 || dir<1.0e-5) &&
                                             ellipse->getCenter().y>p.y))
                                    {
                                        counter++;
                                        sure = false;
                                    }
                                }
                                else
                                {
                                    counter++;
                                }
                            }
                            else
                            {
                                // tangent:
                                if (i==0 && sol.get(1).valid==false)
                                {
                                    if (!sol.isTangent())
                                    {
                                        counter++;
                                    }
                                    else
                                    {
                                        sure = false;
                                    }
                                } else if (i==1 || sol.get(1).valid==true)
                                {
                                    counter++;
                                }
                            }
                        }
                    }
                }
            }
        }

        rayAngle+=0.02;
        tries++;
    } while (!sure && rayAngle<2*M_PI && tries<6);
    return ((counter%2)==1);
}


FAS_VectorSolutions FAS_Information::createQuadrilateral(const FAS_EntityContainer& container)
{
    FAS_VectorSolutions ret;
    if(container.count()!=4)
        return ret;

    FAS_EntityContainer c(container);
    std::vector<FAS_Line*> lines;
    for(auto e: c)
    {
        if(e->rtti()!=FAS2::EntityLine)
            return ret;
        lines.push_back(static_cast<FAS_Line*>(e));
    }
    if(lines.size()!=4)
        return ret;

    //find intersections
    std::vector<FAS_Vector> vertices;
    for(auto it=lines.begin()+1; it != lines.end(); ++it)
    {
        for(auto jt=lines.begin(); jt != it; ++jt)
        {
            FAS_VectorSolutions const& sol=FAS_Information::getIntersectionLineLine(*it, *jt);
            if(sol.size()){
                vertices.push_back(sol.at(0));
            }
        }
    }

    switch (vertices.size())
    {
    default:
        return ret;
    case 4:
        break;
    case 5:
    case 6:
        for(FAS_Line* pl: lines)
        {
            const double a0=pl->getDirection1();
            std::vector<std::vector<FAS_Vector>::iterator> left;
            std::vector<std::vector<FAS_Vector>::iterator> right;
            for(auto it=vertices.begin(); it != vertices.end(); ++it)
            {
                FAS_Vector const& dir=*it - pl->getNearestPointOnEntity(*it, false);
                if(dir.squared()<FAS_TOLERANCE15)
                    continue;
                if(remainder(dir.angle() - a0, 2.*M_PI) > 0.)
                    left.push_back(it);
                else
                    right.push_back(it);

                if(left.size()==2 && right.size()==1)
                {
                    vertices.erase(right[0]);
                    break;
                }
                else if(left.size()==1 && right.size()==2)
                {
                    vertices.erase(left[0]);
                    break;
                }
            }
            if(vertices.size()==4)
                break;
        }
        break;
    }

    //order vertices
    FAS_Vector center{0., 0.};
    for(const FAS_Vector& vp: vertices)
        center += vp;
    center *= 0.25;
    std::sort(vertices.begin(), vertices.end(), [&center](const FAS_Vector& a,
              const FAS_Vector&b)->bool
    {
        return center.angleTo(a)<center.angleTo(b);
    }
    );
    for(const FAS_Vector& vp: vertices)
    {
        ret.push_back(vp);
    }
    return ret;
}
