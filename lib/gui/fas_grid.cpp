/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_grid.h"

//sort with alphabetical order
#include <cmath>
#include <QString>

#include "fas_graphic.h"
#include "fas_graphicview.h"
#include "fas_units.h"
#include "fas_math.h"
#include "lc_rect.h"

namespace
{
//maximum number grid points to draw, for performance consideration
const int maxGridPoints=10000;
//minimum grid width to consider
const double minimumGridWidth=1.0e-8;
}

FAS_Grid::FAS_Grid(FAS_GraphicView* graphicView)
    :graphicView(graphicView)
    ,baseGrid(false)
{}

// Updates the grid point array.
void FAS_Grid::updatePointArray()
{
    if (!graphicView->isGridOn())
        return;

    FAS_Graphic* graphic = graphicView->getGraphic();
    // auto scale grid?
    bool scaleGrid = true;
    // get grid setting
    FAS_Vector userGrid;
    if (graphic)
    {
        //$ISOMETRICGRID == $SNAPSTYLE
        isometric = static_cast<bool>(graphic->getVariableInt("$SNAPSTYLE",0));
        crosshairType=graphic->getCrosshairType();
        userGrid = graphic->getVariableVector("$GRIDUNIT",
                                              FAS_Vector(-1.0, -1.0));
    }
    else
    {
        isometric = false;
        crosshairType= FAS2::LeftCrosshair;
        userGrid.x = -1.0;
        userGrid.y = -1.0;
    }
    pt.clear();
    metaX.clear();
    metaY.clear();
    // find out unit:
    FAS2::Unit unit = FAS2::None;
    FAS2::LinearFormat format = FAS2::Decimal;
    if (graphic) {
        unit = graphic->getUnit();
        format = graphic->getLinearFormat();
    }
    FAS_Vector gridWidth;
    int minGridSpacing = 10;
    if (FAS_Units::isMetric(unit) || unit==FAS2::None ||
            format==FAS2::Decimal || format==FAS2::Engineering)
    {
        //metric grid
        gridWidth = getMetricGridWidth(userGrid, scaleGrid, minGridSpacing);
    }
    else
    {
        // imperial grid:
        gridWidth = getImperialGridWidth(userGrid, scaleGrid, minGridSpacing);
    }
    // for grid info:
    spacing = gridWidth.x;
    metaSpacing = metaGridWidth.x;
    if (gridWidth.x>minimumGridWidth && gridWidth.y>minimumGridWidth &&
            graphicView->toGuiDX(gridWidth.x)>2 &&
            graphicView->toGuiDY(gridWidth.y)>2)
    {
        // find grid boundaries
        double left = (int)(graphicView->toGraphX(0) / gridWidth.x) * gridWidth.x;
        double right = (int)(graphicView->toGraphX(graphicView->getWidth()) / gridWidth.x) * gridWidth.x;
        double top = (int)(graphicView->toGraphY(0) / gridWidth.y) * gridWidth.y;
        double bottom = (int)(graphicView->toGraphY(graphicView->getHeight()) / gridWidth.y) * gridWidth.y;

        left -= gridWidth.x;
        right += gridWidth.x;
        top += gridWidth.y;
        bottom -= gridWidth.y;

        //top/bottom is reversed with RectF definition
        LC_Rect const rect{{left, bottom}, {right, top}};
        // populate grid points and metaGrid line positions: pts, metaX, metaY
        if(isometric)
        {
            createIsometricGrid(rect, gridWidth);
        }
        else
        {
            createOrthogonalGrid(rect, gridWidth);
        }
    }
}

FAS_Vector FAS_Grid::getMetricGridWidth(FAS_Vector const& userGrid, bool scaleGrid, int minGridSpacing)
{
    FAS_Vector gridWidth;
    if (userGrid.x>0.0)
    {
        gridWidth.x = userGrid.x;
    }
    else
    {
        gridWidth.x = minimumGridWidth;
    }

    if (userGrid.y>0.0)
    {
        gridWidth.y = userGrid.y;
    }
    else
    {
        gridWidth.y = minimumGridWidth;
    }
    if (scaleGrid|| userGrid.x<=minimumGridWidth || userGrid.y<=minimumGridWidth)
    {
        if(scaleGrid || userGrid.x<=minimumGridWidth)
        {
            while (graphicView->toGuiDX(gridWidth.x)<minGridSpacing)
            {
                gridWidth.x*=10;
            }
        }
        if(scaleGrid || userGrid.y<=minimumGridWidth)
        {
            while (graphicView->toGuiDY(gridWidth.y)<minGridSpacing)
            {
                gridWidth.y*=10;
            }
        }
    }
    metaGridWidth.x = gridWidth.x*10;
    metaGridWidth.y = gridWidth.y*10;
    return gridWidth;
}

FAS_Vector FAS_Grid::getImperialGridWidth(FAS_Vector const& userGrid, bool scaleGrid, int minGridSpacing)
{
    FAS_Vector gridWidth;
    if (userGrid.x>0.0)
    {
        gridWidth.x = userGrid.x;
    }
    else
    {
        gridWidth.x = 1.0/1024.0;
    }

    if (userGrid.y>0.0)
    {
        gridWidth.y = userGrid.y;
    }
    else
    {
        gridWidth.y = 1.0/1024.0;
    }

    FAS2::Unit unit = FAS2::None;
    FAS_Graphic* graphic = graphicView->getGraphic();

    if (graphic)
    {
        unit = graphic->getUnit();
    }

    if (unit==FAS2::Inch)
    {
        // auto scale grid
        if (scaleGrid|| userGrid.x<=minimumGridWidth || userGrid.y<=minimumGridWidth)
        {
            if(scaleGrid || userGrid.x<=minimumGridWidth)
            {
                while (graphicView->toGuiDX(gridWidth.x)<minGridSpacing)
                {
                    if (FAS_Math::round(gridWidth.x)>=36) {
                        gridWidth.x*=2;
                    }
                    else if (FAS_Math::round(gridWidth.x)>=12)
                    {
                        gridWidth.x*=3;
                    }
                    else if (FAS_Math::round(gridWidth.x)>=4)
                    {
                        gridWidth.x*=3;
                    }
                    else if (FAS_Math::round(gridWidth.x)>=1)
                    {
                        gridWidth.x*=2;
                    }
                    else {
                        gridWidth.x*=2;
                    }
                }
            }
            if(scaleGrid || userGrid.y<=minimumGridWidth)
            {
                while (graphicView->toGuiDY(gridWidth.y)<minGridSpacing)
                {
                    if (FAS_Math::round(gridWidth.y)>=36)
                    {
                        gridWidth.y*=2;
                    }
                    else if (FAS_Math::round(gridWidth.y)>=12)
                    {
                        gridWidth.y*=3;
                    }
                    else if (FAS_Math::round(gridWidth.y)>=4)
                    {
                        gridWidth.y*=3;
                    }
                    else if (FAS_Math::round(gridWidth.y)>=1)
                    {
                        gridWidth.y*=2;
                    }
                    else {
                        gridWidth.y*=2;
                    }
                }
            }
        }
        // metagrid X shows inches..
        metaGridWidth.x = 1.0;

        if (graphicView->toGuiDX(metaGridWidth.x)<minGridSpacing*2)
        {
            metaGridWidth.x = 12.0;

            // .. or yards
            if (graphicView->toGuiDX(metaGridWidth.x)<minGridSpacing*2)
            {
                metaGridWidth.x = 36.0;
                if (graphicView->toGuiDX(metaGridWidth.x)<minGridSpacing*2)
                {
                    metaGridWidth.x = -1.0;
                }

            }
        }
        // metagrid Y shows inches..
        metaGridWidth.y = 1.0;

        if (graphicView->toGuiDY(metaGridWidth.y)<minGridSpacing*2)
        {
            // .. or feet
            metaGridWidth.y = 12.0;
            // .. or yards
            if (graphicView->toGuiDY(metaGridWidth.y)<minGridSpacing*2)
            {
                metaGridWidth.y = 36.0;
                // .. or nothing
                if (graphicView->toGuiDY(metaGridWidth.y)<minGridSpacing*2) {
                    metaGridWidth.y = -1.0;
                }
            }
        }
    } else {
        if (scaleGrid) {
            while (graphicView->toGuiDX(gridWidth.x)<minGridSpacing)
            {
                gridWidth.x*=2;
            }
            metaGridWidth.x = -1.0;

            while (graphicView->toGuiDY(gridWidth.y)<minGridSpacing)
            {
                gridWidth.y*=2;
            }
            metaGridWidth.y = -1.0;
        }
    }
    return gridWidth;
}


void FAS_Grid::createOrthogonalGrid(LC_Rect const& rect, FAS_Vector const& gridWidth)
{
    double const left=rect.minP().x;
    double const right=rect.maxP().x;
    //top/bottom reversed
    double const top=rect.maxP().y;
    double const bottom=rect.minP().y;

    cellV.set(fabs(gridWidth.x),fabs(gridWidth.y));
    int numberX = (FAS_Math::round((right-left) / gridWidth.x) + 1);
    int numberY = (FAS_Math::round((top-bottom) / gridWidth.y) + 1);
    int number = numberX*numberY;
    //todo, fix baseGrid for orthogonal grid
    baseGrid.set(left,bottom);
    // create grid array:
    if (number<=0 || number>maxGridPoints)
        return;
    pt.resize(number);
    int i=0;
    FAS_Vector bp0(baseGrid);
    for (int y=0; y<numberY; ++y)
    {
        FAS_Vector bp1(bp0);
        for (int x=0; x<numberX; ++x)
        {
            pt[i++] = bp1;
            bp1.x += gridWidth.x;
        }
        bp0.y += gridWidth.y;
    }
    // find meta grid boundaries
    if (metaGridWidth.x>minimumGridWidth && metaGridWidth.y>minimumGridWidth &&
            graphicView->toGuiDX(metaGridWidth.x)>2 &&
            graphicView->toGuiDY(metaGridWidth.y)>2)
    {

        double mleft = (int)(graphicView->toGraphX(0) / metaGridWidth.x) * metaGridWidth.x;
        double mright = (int)(graphicView->toGraphX(graphicView->getWidth()) / metaGridWidth.x) * metaGridWidth.x;
        double mtop = (int)(graphicView->toGraphY(0) / metaGridWidth.y) * metaGridWidth.y;
        double mbottom = (int)(graphicView->toGraphY(graphicView->getHeight()) / metaGridWidth.y) * metaGridWidth.y;

        mleft -= metaGridWidth.x;
        mright += metaGridWidth.x;
        mtop += metaGridWidth.y;
        mbottom -= metaGridWidth.y;

        // calculate number of visible meta grid lines:
        int numMetaX = (FAS_Math::round((mright-mleft) / metaGridWidth.x) + 1);
        int numMetaY = (FAS_Math::round((mtop-mbottom) / metaGridWidth.y) + 1);

        if (numMetaX<=0 || numMetaY<=0) return;
        // create meta grid arrays:
        metaX.resize(numMetaX);
        metaY.resize(numMetaY);

        int i=0;
        for (int x=0; x<numMetaX; ++x)
        {
            metaX[i++] = mleft+x*metaGridWidth.x;
        }
        i=0;
        for (int y=0; y<numMetaY; ++y)
        {
            metaY[i++] = mbottom+y*metaGridWidth.y;
        }
    }
}

void FAS_Grid::createIsometricGrid(LC_Rect const& rect, FAS_Vector const& gridWidth)
{
    double const left=rect.minP().x;
    double const right=rect.maxP().x;
    //top/bottom reversed
    double const top=rect.maxP().y;
    double const bottom=rect.minP().y;
    int numberY = (FAS_Math::round((top-bottom) / gridWidth.y) + 1);
    double dx=sqrt(3.)*gridWidth.y;
    cellV.set(fabs(dx),fabs(gridWidth.y));
    double hdx=0.5*dx;
    double hdy=0.5*gridWidth.y;
    int numberX = (FAS_Math::round((right-left) / dx) + 1);
    int number = 2*numberX*numberY;
    baseGrid.set(left+remainder(-left,dx),bottom+remainder(-bottom,fabs(gridWidth.y)));

    if (number<=0 || number>maxGridPoints) return;

    pt.resize(number);

    int i=0;
    FAS_Vector bp0(baseGrid),dbp1(hdx,hdy);
    for (int y=0; y<numberY; ++y)
    {
        FAS_Vector bp1(bp0);
        for (int x=0; x<numberX; ++x)
        {
            pt[i++] = bp1;
            pt[i++] = bp1+dbp1;
            bp1.x += dx;
        }
        bp0.y += gridWidth.y;
    }
    //find metaGrid
    if (metaGridWidth.y>minimumGridWidth && graphicView->toGuiDY(metaGridWidth.y)>2)
    {
        metaGridWidth.x=(metaGridWidth.x<0.)?-sqrt(3.)*fabs(metaGridWidth.y):sqrt(3.)*fabs(metaGridWidth.y);
        FAS_Vector baseMetaGrid(left+remainder(-left,metaGridWidth.x)-fabs(metaGridWidth.x),bottom+remainder(-bottom,metaGridWidth.y)-fabs(metaGridWidth.y));

        // calculate number of visible meta grid lines:
        int numMetaX = (FAS_Math::round((right-left) / metaGridWidth.x) + 1);
        int numMetaY = (FAS_Math::round((top-bottom) / metaGridWidth.y) + 1);

        if (numMetaX<=0 || numMetaY<=0) return;
        // create meta grid arrays:
        metaX.resize(numMetaX);
        metaY.resize(numMetaY);

        double x0(baseMetaGrid.x);
        for (int i=0; i<numMetaX; x0 += metaGridWidth.x)
        {
            metaX[i++] = x0;
        }
        x0=baseMetaGrid.y;
        for (int i=0; i<numMetaY; x0 += metaGridWidth.y)
        {
            metaY[i++] = x0;
        }
    }
}

QString FAS_Grid::getInfo() const
{
    return QString("%1 / %2").arg(spacing).arg(metaSpacing);
}

std::vector<FAS_Vector> const& FAS_Grid::getPoints() const
{
    return pt;
}

std::vector<double> const& FAS_Grid::getMetaX() const
{
    return metaX;
}

std::vector<double> const& FAS_Grid::getMetaY() const
{
    return metaY;
}

bool FAS_Grid::isIsometric() const
{
    return isometric;
}

void FAS_Grid::setIsometric(bool b)
{
    isometric=b;
}

FAS_Vector FAS_Grid::getMetaGridWidth() const
{
    return metaGridWidth;
}

FAS_Vector const& FAS_Grid::getCellVector() const
{
    return cellV;
}

void FAS_Grid::setCrosshairType(FAS2::CrosshairType chType)
{
    crosshairType=chType;
}
FAS2::CrosshairType FAS_Grid::getCrosshairType() const
{
    return crosshairType;
}

// EOF
