/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_STATICGRAPHICVIEW_H
#define FAS_STATICGRAPHICVIEW_H

#include "fas_graphicview.h"

/*
 * This is an implementation of a graphic viewer with a fixed size
 * for drawing onto fixed devices (such as bitmaps).
 */
class FAS_StaticGraphicView: public FAS_GraphicView
{
public:
    FAS_StaticGraphicView(int w, int h, FAS_Painter* p, QSize const* pb = nullptr);

    int getWidth() const override;
    int getHeight() const override;
    void redraw(FAS2::RedrawMethod) override{}
    void adjustOffsetControls() override{}
    void adjustZoomControls() override{}
    void setMouseCursor(FAS2::CursorType ) {}

    void updateGridStatusWidget(const QString& ) {}
    FAS_Vector getMousePosition() const override;
    void paint();

private:
    int width;
    int height;
};

#endif
