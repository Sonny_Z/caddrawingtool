/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_GRAPHICVIEW_H
#define FAS_GRAPHICVIEW_H

#include <QWidget>
#include <QDateTime>
#include <QMap>
#include <tuple>

#include "fas_entitycontainer.h"
#include "fas_grid.h"
#include "fas_snapper.h"
#include "lc_rect.h"

class QMouseEvent;
class QKeyEvent;
class FAS_ActionInterface;
class FAS_EventHandler;
class FAS_CommandEvent;
struct FAS_LineTypePattern;


//common GUI interface for the graphic viewer widget which has to be implementet by real GUI classes such as the Qt graphical view.
class FAS_GraphicView : public QWidget
{
    Q_OBJECT

public:
    FAS_GraphicView(QWidget * parent = 0, Qt::WindowFlags f = 0);
    virtual ~FAS_GraphicView();

    void cleanUp();
    FAS_Graphic* getGraphic() const;
    void setDrawingMode(FAS2::DrawingMode m)
    {
        drawingMode = m;
    }
    FAS2::DrawingMode getDrawingMode() const
    {
        return drawingMode;
    }

    // Activates or deactivates the delete mode.
    void setDeleteMode(bool m)
    {
        deleteMode = m;
    }
    bool getDeleteMode() const
    {
        return deleteMode;
    }
    virtual int getWidth() const= 0;
    virtual int getHeight() const= 0;
    virtual void redraw(FAS2::RedrawMethod method=FAS2::RedrawAll) = 0;
    virtual void adjustOffsetControls() = 0;
    virtual void adjustZoomControls() = 0;

    // Sets the background color.
    virtual void setBackground(const FAS_Color& bg);
    FAS_Color getBackground() const
    {
        return background;
    }

    //Current foreground color.
    FAS_Color getForeground() const
    {
        return foreground;
    }

    //Sets the grid color.
    void setGridColor(const FAS_Color& c)
    {
        gridColor = c;
    }

    // Sets the meta grid color.
    void setMetaGridColor(const FAS_Color& c)
    {
        metaGridColor = c;
    }

    //Sets the selection color.
    void setSelectedColor(const FAS_Color& c)
    {
        selectedColor = c;
    }

    // Sets the highlight color.
    void setHighlightedColor(const FAS_Color& c)
    {
        highlightedColor = c;
    }

    // Sets the color for the first handle (start vertex)
    void setStartHandleColor(const FAS_Color& c)
    {
        startHandleColor = c;
    }

    //Sets the color for handles, that are neither start nor end vertices
    void setHandleColor(const FAS_Color& c)
    {
        handleColor = c;
    }

    // Sets the color for the last handle (end vertex)
    void setEndHandleColor(const FAS_Color& c) {
        endHandleColor = c;
    }

    // This virtual method can be overwritten to set the mouse cursor to the given type.
    virtual void setMouseCursor(FAS2::CursorType /*c*/)=0;

    void setContainer(FAS_EntityContainer* container);
    FAS_EntityContainer* getContainer() const;
    void setFactor(double f);
    void setFactorX(double f);
    void setFactorY(double f);
    FAS_Vector getFactor() const;
    virtual void setOffset(int ox, int oy);
    void setOffsetX(int ox);
    void setOffsetY(int oy);
    int getOffsetX() const;
    int getOffsetY() const;
    void centerOffsetX();
    void centerOffsetX(FAS_Entity* entity);
    void centerOffsetY();
    void centerOffsetY(FAS_Entity* entity);
    void centerX(double x);
    void centerY(double y);
    void setBorders(int left, int top, int right, int bottom);
    int getBorderLeft() const;
    int getBorderTop() const;
    int getBorderRight() const;
    int getBorderBottom() const;

    void freezeZoom(bool freeze);
    bool isZoomFrozen() const;

    void setDefaultAction(FAS_ActionInterface* action);
    FAS_ActionInterface*  getDefaultAction();
    void setCurrentAction(FAS_ActionInterface* action);
    FAS_ActionInterface* getCurrentAction();

    void killSelectActions();
    void killAllActions();

    void back();
    void enter();

    void enableCoordinateInput();
    void disableCoordinateInput();

    virtual void zoomIn(double f=1.5, const FAS_Vector& center=FAS_Vector(false));
    virtual void zoomInX(double f=1.5);
    virtual void zoomInY(double f=1.5);
    virtual void zoomOut(double f=1.5, const FAS_Vector& center=FAS_Vector(false));
    virtual void zoomOutX(double f=1.5);
    virtual void zoomOutY(double f=1.5);
    virtual void zoomAuto(bool axis=true, bool keepAspectRatio=true);
    virtual void zoomAutoBlock(bool axis=true, bool keepAspectRatio=true);
    virtual void zoomAutoY(bool axis=true);
    virtual void zoomPrevious();
    virtual void saveView();
    virtual void restoreView();
    virtual void zoomWindow(FAS_Vector v1, FAS_Vector v2,
                            bool keepAspectRatio=true);
    virtual void zoomPan(int dx, int dy);
    virtual void zoomScroll(FAS2::Direction direction);
    virtual void zoomPage();
    virtual void drawLayer1(FAS_Painter *painter);
    virtual void drawLayer2(FAS_Painter *painter);
    virtual void drawLayer3(FAS_Painter *painter);
    virtual void deleteEntity(FAS_Entity* e);
    virtual void drawEntity(FAS_Painter *painter, FAS_Entity* e, double& patternOffset);
    virtual void drawEntity(FAS_Painter *painter, FAS_Entity* e);
    virtual void drawEntity(FAS_Painter *painter, FAS_Entity* e,FAS_InsertData& data);
    virtual void drawEntity(FAS_Painter *painter, FAS_Entity* e, double& patternOffset,FAS_InsertData& data);
    virtual void drawEntity(FAS_Entity* e, double& patternOffset);
    virtual void drawEntity(FAS_Entity* e);

    bool filterEntityDisplay(FAS_Entity* e);

    virtual void drawEntityPlain(FAS_Painter *painter, FAS_Entity* e);
    virtual void drawEntityPlain(FAS_Painter *painter, FAS_Entity* e, double& patternOffset);
    virtual void drawEntityPlain(FAS_Painter *painter, FAS_Entity* e,FAS_InsertData& data);
    virtual void drawEntityPlain(FAS_Painter *painter, FAS_Entity* e, double& patternOffset,FAS_InsertData& data);
    virtual void setPenForEntity(FAS_Painter *painter, FAS_Entity* e );
    virtual FAS_Vector getMousePosition() const = 0;
    virtual const FAS_LineTypePattern* getPattern(FAS2::LineType t);
    virtual void drawAbsoluteZero(FAS_Painter *painter);
    virtual void drawRelativeZero(FAS_Painter *painter);
    virtual void drawPaper(FAS_Painter *painter);
    virtual void drawMetaGrid(FAS_Painter *painter);
    virtual void drawOverlay(FAS_Painter *painter);

    FAS_Grid* getGrid() const;
    virtual void updateGridStatusWidget(const QString& /*text*/) = 0;
    void setDefaultSnapMode(FAS_SnapMode sm);
    FAS_SnapMode getDefaultSnapMode() const;
    void setSnapRestriction(FAS2::SnapRestriction sr);
    FAS2::SnapRestriction getSnapRestriction() const;

    bool isGridOn() const;
    bool isGridIsometric() const;
    void setCrosshairType(FAS2::CrosshairType chType);
    FAS2::CrosshairType getCrosshairType() const;

    FAS_Vector toGui(FAS_Vector v) const;
    double toGuiX(double x) const;
    double toGuiY(double y) const;
    double toGuiDX(double d) const;
    double toGuiDY(double d) const;

    FAS_Vector toGraph(FAS_Vector v) const;
    FAS_Vector toGraph(int x, int y) const;
    double toGraphX(int x) const;
    double toGraphY(int y) const;
    double toGraphDX(int d) const;
    double toGraphDY(int d) const;

    //(Un-)Locks the position of the relative zero.
    void lockRelativeZero(bool lock);
    bool isRelativeZeroLocked() const;

    //return Relative zero coordinate.
    FAS_Vector const& getRelativeZero() const;
    void setRelativeZero(const FAS_Vector& pos);
    void moveRelativeZero(const FAS_Vector& pos);

    FAS_EventHandler* getEventHandler() const;

    void setPrintPreview(bool pv);
    bool isPrintPreview() const;
    void setPrinting(bool p);
    bool isPrinting() const;
    bool isDraftMode() const;
    void setDraftMode(bool dm);
    bool isCleanUp(void) const;
    virtual FAS_EntityContainer* getOverlayContainer(FAS2::OverlayGraphics position);
    const LC_Rect& getViewRect()
    {
        return view_rect;
    }
    bool isPanning() const;
    void setPanning(bool state);

protected:

    FAS_EntityContainer* container{nullptr}; // Holds a pointer to all the enties
    FAS_EventHandler* eventHandler;

    // background color (any color)
    FAS_Color background;
    // foreground color (black or white)
    FAS_Color foreground;
    // grid color
    FAS_Color gridColor;
    // meta grid color
    FAS_Color metaGridColor;
    // selected color
    FAS_Color selectedColor;
    // highlighted color
    FAS_Color highlightedColor;
    // Start handle color
    FAS_Color startHandleColor;
    // Intermediate (not start/end vertex) handle color
    FAS_Color handleColor;
    // End handle color
    FAS_Color endHandleColor;
    // Grid
    std::unique_ptr<FAS_Grid> grid;
    // Current default snap mode for this graphic view.
    FAS_SnapMode defaultSnapMode;
    // Current default snap restriction for this graphic view.
    FAS2::SnapRestriction defaultSnapRes;
    FAS2::DrawingMode drawingMode;
    // Delete mode. If true, all drawing actions will delete in background color instead.
    bool deleteMode=false;

    LC_Rect view_rect;

private:
    bool zoomFrozen=false;
    bool draftMode=false;
    FAS_Vector factor = FAS_Vector(1.,1.);
    int offsetX=0;
    int offsetY=0;
    //circular buffer for saved views
    std::vector<std::tuple<int, int, FAS_Vector> > savedViews;
    unsigned short savedViewIndex=0;
    unsigned short savedViewCount=0;
    QDateTime previousViewTime;

    int borderLeft=0;
    int borderTop=0;
    int borderRight=0;
    int borderBottom=0;

    FAS_Vector relativeZero{false};
    bool relativeZeroLocked=false;
    // Print preview flag
    bool printPreview=false;
    // Active when printing only:
    bool printing=false;

    // Map that will be used for overlaying additional items on top of the main CAD drawing
    QMap<int, FAS_EntityContainer *> overlayEntities;
    // if true, graphicView is under cleanup
    bool m_bIsCleanUp=false;
    bool panning;
signals:
    void relative_zero_changed(const FAS_Vector&);
    void previous_zoom_state(bool);
};

#endif
