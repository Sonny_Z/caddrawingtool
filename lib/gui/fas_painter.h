/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_PAINTER_H
#define FAS_PAINTER_H

#include "fas_vector.h"

class FAS_Color;
class FAS_Pen;
class QPainterPath;
class QRectF;
class QPolygon;
class QPolygonF;
class QImage;
class QBrush;
class QFont;

/*
 * This class is a common interface for a painter class. Such
 * a class will in it's implementation be responsible to paint
 * lines, arcs, ... in widgets. All angles in rad.
 */
class FAS_Painter
{
public:
    FAS_Painter(): offset{0.0,0.0}
    {
        drawingMode = FAS2::ModeFull;
        drawSelectedEntities=false;
    }

    //Sets the drawing mode.
    void setDrawingMode(FAS2::DrawingMode m)
    {
        drawingMode = m;
    }

    // When set to true, only entities that are selected will be drawn
    void setDrawSelectedOnly(bool dso)
    {
        drawSelectedEntities=dso;
    }

    // When true, only selected items will be draw
    bool shouldDrawSelected()
    {
        return drawSelectedEntities;
    }

    //return Current drawing mode.
    FAS2::DrawingMode getDrawingMode()
    {
        return drawingMode;
    }

    virtual void moveTo(int x, int y) = 0;
    virtual void lineTo(int x, int y) = 0;

    virtual void drawGridPoint(const FAS_Vector& p) = 0;
    virtual void drawPoint(const FAS_Vector& p) = 0;
    virtual void drawLine(const FAS_Vector& p1, const FAS_Vector& p2) = 0;
    virtual void drawRect(const FAS_Vector& p1, const FAS_Vector& p2);
    virtual void drawArc(const FAS_Vector& cp, double radius,
                         double a1, double a2,
                         const FAS_Vector& p1, const FAS_Vector& p2,
                         bool reversed) = 0;
    virtual void drawArc(const FAS_Vector& cp, double radius,
                         double a1, double a2,
                         bool reversed) = 0;
    void createArc(QPolygon& pa,
                   const FAS_Vector& cp, double radius,
                   double a1, double a2,
                   bool reversed);
    void createEllipse(QPolygon& pa,
                       const FAS_Vector& cp,
                       double radius1, double radius2,
                       double angle,
                       double angle1, double angle2,
                       bool reversed);
    virtual void drawCircle(const FAS_Vector& cp, double radius) = 0;
    virtual void drawEllipse(const FAS_Vector& cp,
                             double radius1, double radius2,
                             double angle,
                             double angle1, double angle2,
                             bool reversed) = 0;
    virtual void drawImg(QImage& img, const FAS_Vector& pos,
                         double angle, const FAS_Vector& factor) = 0;

    virtual void drawTextH(int x1, int y1, int x2, int y2,
                           const QString& text) = 0;
    virtual void fillRect(int x1, int y1, int w, int h, const FAS_Color& col) = 0;
    virtual void fillRect ( const QRectF & rectangle, const FAS_Color & color ) = 0;
    virtual void fillRect ( const QRectF & rectangle, const QBrush & brush ) = 0;

    virtual void fillTriangle(const FAS_Vector& p1,
                              const FAS_Vector& p2,
                              const FAS_Vector& p3) = 0;

    virtual void drawPath ( const QPainterPath & path ) = 0;
    virtual void drawHandle(const FAS_Vector& p, const FAS_Color& c, int size=-1);

    virtual FAS_Pen getPen() const = 0;
    virtual void setPen(const FAS_Pen& pen) = 0;
    virtual void setPen(const FAS_Color& color) = 0;
    virtual void setPen(int r, int g, int b) = 0;
    virtual void disablePen() = 0;
    virtual void setFont(const QFont& font) = 0;
    virtual const QBrush& brush() const = 0;
    virtual void setBrush(const FAS_Color& color) = 0;
    virtual void setBrush(const QBrush& color) = 0;
    virtual void drawPolygon(const QPolygon& a, Qt::FillRule rule=Qt::WindingFill) = 0;
    virtual void erase() = 0;
    virtual int getWidth() const= 0;
    virtual int getHeight() const= 0;
    virtual double getDpmm() const= 0;

    virtual void setOffset(const FAS_Vector& o)
    {
        offset = o;
    }

    virtual void setClipRect(int x, int y, int w, int h) = 0;
    virtual void resetClipping() = 0;
    int toScreenX(double x) const;
    int toScreenY(double y) const;

protected:
    // Current drawing mode.
    FAS2::DrawingMode drawingMode;
    // A fixed offset added to all entities drawn (useful for previews).
    FAS_Vector offset;
    // When set to true, only selected entities should be drawn
    bool drawSelectedEntities;
};

#endif
