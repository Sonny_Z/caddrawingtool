/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_eventhandler.h"

//sort with alphabetical order
#include <QAction>
#include <QMouseEvent>
#include <QRegExp>

#include "fas_actioninterface.h"
#include "fas_math.h"

FAS_EventHandler::FAS_EventHandler(QObject* parent) : QObject(parent)
{
    connect(parent, SIGNAL(relative_zero_changed(const FAS_Vector&)),
            this, SLOT(setRelativeZero(const FAS_Vector&)));
}

FAS_EventHandler::~FAS_EventHandler()
{
    delete defaultAction;
    defaultAction = nullptr;

    for(auto a: currentActions)
    {
        delete a;
    }
    currentActions.clear();
}

void FAS_EventHandler::back()
{
    QMouseEvent e(QEvent::MouseButtonRelease, QPoint(0,0), Qt::RightButton, Qt::RightButton,Qt::NoModifier);
    mouseReleaseEvent(&e);
    if (!hasAction() && q_action)
    {
        q_action->setChecked(false);
        q_action = nullptr;
    }
}


// Go enter pressed event for current action.
void FAS_EventHandler::enter()
{
    QKeyEvent e(QEvent::KeyPress, Qt::Key_Enter, 0);
    keyPressEvent(&e);
}


// Called by QG_GraphicView
void FAS_EventHandler::mousePressEvent(QMouseEvent* e)
{
    if(hasAction())
    {
        currentActions.last()->mousePressEvent(e);
        e->accept();
    }
    else
    {
        if (defaultAction)
        {
            defaultAction->mousePressEvent(e);
            e->accept();
        }
        else
        {
            e->ignore();
        }
    }
}

//Called by QG_GraphicView
void FAS_EventHandler::mouseReleaseEvent(QMouseEvent* e)
{
    if(hasAction())
    {
        currentActions.last()->mouseReleaseEvent(e);
        cleanUp();
        e->accept();
    }
    else if(e->button()==Qt::RightButton)
    {
        en=defaultAction->catchEntity(e);
    }
    else
    {
        if (defaultAction)
        {
            defaultAction->mouseReleaseEvent(e);
        }
        else
        {
            e->ignore();
        }
    }
}

// Called by QG_GraphicView
void FAS_EventHandler::mouseMoveEvent(QMouseEvent* e)
{
    if(hasAction())
        currentActions.last()->mouseMoveEvent(e);
    else if (defaultAction)
        defaultAction->mouseMoveEvent(e);
}

// Called by QG_GraphicView
void FAS_EventHandler::mouseLeaveEvent()
{
    if(hasAction())
    {
        currentActions.last()->suspend();
    }
    else
    {
        if (defaultAction)
        {
            defaultAction->suspend();
        }
    }
}

// Called by QG_GraphicView
void FAS_EventHandler::mouseEnterEvent()
{
    if(hasAction())
    {
        currentActions.last()->resume();
    }
    else
    {
        if (defaultAction)
        {
            defaultAction->resume();
        }
    }
}

// Called by QG_GraphicView
void FAS_EventHandler::keyPressEvent(QKeyEvent* e)
{
    if(hasAction())
    {
        currentActions.last()->keyPressEvent(e);
    }
    else
    {
        if (defaultAction)
        {
            defaultAction->keyPressEvent(e);
        }
        else
        {
            e->ignore();
        }
    }
}

// Called by QG_GraphicView
void FAS_EventHandler::keyReleaseEvent(QKeyEvent* e)
{
    if(hasAction())
    {
        currentActions.last()->keyReleaseEvent(e);
    }
    else
    {
        if (defaultAction)
        {
            defaultAction->keyReleaseEvent(e);
        }
        else
        {
            e->ignore();
        }
    }
}

// Enables coordinate input in the command line.
void FAS_EventHandler::enableCoordinateInput()
{
    coordinateInputEnabled = true;
}

void FAS_EventHandler::disableCoordinateInput()
{
    coordinateInputEnabled = false;
}

// Current action.
FAS_ActionInterface* FAS_EventHandler::getCurrentAction()
{
    if(hasAction())
    {
        return currentActions.last();
    }
    else
    {
        return defaultAction;
    }
}

// The current default action.
FAS_ActionInterface* FAS_EventHandler::getDefaultAction() const
{
    return defaultAction;
}

// Sets the default action.
void FAS_EventHandler::setDefaultAction(FAS_ActionInterface* action)
{
    if (defaultAction)
    {
        defaultAction->finish();
        delete defaultAction;
    }
    defaultAction = action;
}

// Sets the current action.
void FAS_EventHandler::setCurrentAction(FAS_ActionInterface* action)
{
    if (action==nullptr)
    {
        return;
    }
    // Predecessor of the new action or nullptr:
    FAS_ActionInterface* predecessor = nullptr;
    // Suspend current action:
    if(hasAction())
    {
        predecessor = currentActions.last();
        predecessor->suspend();
    }
    else
    {
        if (defaultAction)
        {
            predecessor = defaultAction;
            predecessor->suspend();
        }
    }

    // Set current action:
    currentActions.push_back(action);
    action->init();
    if (action->isFinished()==false)
    {
        action->setPredecessor(predecessor);
    }

    cleanUp();
    if (q_action)
        q_action->setChecked(true);
}

// Kills all running selection actions. Called when a selection actionis launched to reduce confusion.
void FAS_EventHandler::killSelectActions()
{
    for (auto it=currentActions.begin();it != currentActions.end();)
    {
        if ((*it)->rtti()==FAS2::ActionSelectSingle ||
                (*it)->rtti()==FAS2::ActionSelectWindow)
        {
            if( ! (*it)->isFinished())
            {
                (*it)->finish();
            }
            delete *it;
            it= currentActions.erase(it);
        }
        else
        {
            it++;
        }
    }
}

// Kills all running actions. Called when a window is closed.
void FAS_EventHandler::killAllActions()
{
    if (q_action)
    {
        q_action->setChecked(false);
        q_action = nullptr;
    }

    for(auto p: currentActions)
    {
        if (!p->isFinished())
        {
            p->finish();
        }
    }
    defaultAction->init(0);
}

//return true if the action is within currentActions
bool FAS_EventHandler::isValid(FAS_ActionInterface* action) const
{
    return currentActions.indexOf(action) >= 0;
}

//return true if there is at least one action in the action stack.
bool FAS_EventHandler::hasAction()
{
    foreach (FAS_ActionInterface* a, currentActions)
    {
        if(!a->isFinished())
            return true;
    }
    return false;
}

// Garbage collector for actions.
void FAS_EventHandler::cleanUp()
{
    for (auto it=currentActions.begin(); it != currentActions.end();)
    {
        if( (*it)->isFinished())
        {
            delete *it;
            it= currentActions.erase(it);
        }else{
            ++it;
        }
    }
    if(hasAction())
    {
        currentActions.last()->resume();
    }
    else
    {
        if (defaultAction)
        {
            defaultAction->resume();
        }
    }
}

// Sets the snap mode for all currently active actions.
void FAS_EventHandler::setSnapMode(FAS_SnapMode sm)
{
    for(auto a: currentActions)
    {
        if( ! a->isFinished())
        {
            a->setSnapMode(sm);
        }
    }

    if (defaultAction)
    {
        defaultAction->setSnapMode(sm);
    }
}

// Sets the snap restriction for all currently active actions.
void FAS_EventHandler::setSnapRestriction(FAS2::SnapRestriction sr)
{
    for(auto a: currentActions)
    {
        if( ! a->isFinished())
        {
            a->setSnapRestriction(sr);
        }
    }
    if (defaultAction)
    {
        defaultAction->setSnapRestriction(sr);
    }
}

void FAS_EventHandler::setQAction(QAction* action)
{
    if (q_action)
    {
        q_action->setChecked(false);
        killAllActions();
    }
    q_action = action;
}

void FAS_EventHandler::setRelativeZero(const FAS_Vector& point)
{
    relative_zero = point;
}

bool FAS_EventHandler::inSelectionMode()
{
    switch (getCurrentAction()->rtti())
    {
    case FAS2::ActionDefault:
    case FAS2::ActionSelectSingle:
    case FAS2::ActionSelectWindow:
    case FAS2::ActionDeselectWindow:
        return true;
    default:
        return false;
    }
}

// EOF
