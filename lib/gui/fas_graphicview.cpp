﻿/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_graphicview.h"

//sort with alphabetical order
#include <climits>
#include <cmath>
#include <iostream>
#include <QApplication>
#include <QDesktopWidget>
#include <QAction>
#include <QMouseEvent>
#include <QtAlgorithms>

#include "fas_eventhandler.h"
#include "fas_line.h"
#include "fas_linetypepattern.h"
#include "fas_graphic.h"
#include "fas_grid.h"
#include "fas_insert.h"
#include "fas_layer.h"
#include "fas_math.h"
#include "fas_mtext.h"
#include "fas_painter.h"
#include "fas_painterqt.h"
#include "fas_text.h"
#include "fas_units.h"
#include "fasutils.h"
#include "qdebug.h"

FAS_GraphicView::FAS_GraphicView(QWidget* parent, Qt::WindowFlags f)
    :QWidget(parent, f)
    ,eventHandler{new FAS_EventHandler{this}}
    ,gridColor(Qt::gray)
    ,metaGridColor{64, 64, 64}
    ,grid{new FAS_Grid{this}}
    ,drawingMode(FAS2::ModeFull)
    ,savedViews(16)
    ,previousViewTime(QDateTime::currentDateTime())
    ,panning(false)
{
    setBackground(QColor(Qt::black));
    setGridColor(QColor(Qt::gray));
    setMetaGridColor(QColor(Qt::darkGray));
    setSelectedColor(QColor(Qt::red));
    setHighlightedColor(QColor(Qt::darkGreen));
    setStartHandleColor(QColor(Qt::cyan));
    setHandleColor(QColor(Qt::blue));
    setEndHandleColor(QColor(Qt::blue));
}

FAS_GraphicView::~FAS_GraphicView()
{
    qDeleteAll(overlayEntities);
    delete eventHandler;
}

// Must be called by any derived class in the destructor.
void FAS_GraphicView::cleanUp()
{
    m_bIsCleanUp=true;
}

// Sets the pointer to the graphic which contains the entities which are visualized by this widget.
void FAS_GraphicView::setContainer(FAS_EntityContainer* container)
{
    this->container = container;
}

/**
 * Sets the zoom factor in X for this visualization of the graphic.
 */
void FAS_GraphicView::setFactorX(double f)
{
    if (!zoomFrozen)
    {
        factor.x = fabs(f);
    }
}

// Sets the zoom factor in Y for this visualization of the graphic.
void FAS_GraphicView::setFactorY(double f)
{
    if (!zoomFrozen)
    {
        factor.y = fabs(f);
    }
}

void FAS_GraphicView::setOffset(int ox, int oy)
{
    setOffsetX(ox);
    setOffsetY(oy);
}

//return true if the grid is switched on.
bool FAS_GraphicView::isGridOn() const
{
    if (container)
    {
        FAS_Graphic* g = container->getGraphic();
        if (g)
        {
            g->setGridOn(1);
            return g->isGridOn();
        }
    }
    return true;
}

//return true if the grid is isometric
bool FAS_GraphicView::isGridIsometric() const
{
    return grid->isIsometric();
}


void FAS_GraphicView::setCrosshairType(FAS2::CrosshairType chType)
{
    grid->setCrosshairType(chType);
}

FAS2::CrosshairType FAS_GraphicView::getCrosshairType() const
{
    return grid->getCrosshairType();
}

// Centers the drawing in x-direction.
void FAS_GraphicView::centerOffsetX()
{
    if (container && !zoomFrozen)
    {
        offsetX = (int)(((getWidth()-borderLeft-borderRight)
                         - (container->getSize().x*factor.x))/2.0
                        - (container->getMin().x*factor.x)) + borderLeft;
    }
}

void FAS_GraphicView::centerOffsetX(FAS_Entity* entity)
{
    FAS_Entity* container = entity;
    if (container && !zoomFrozen)
    {
        offsetX = (int)(((getWidth()-borderLeft-borderRight)
                         - (container->getSize().x*factor.x))/2.0
                        - (container->getMin().x*factor.x)) + borderLeft;
    }
}

// Centers the drawing in y-direction.
void FAS_GraphicView::centerOffsetY()
{
    if (container && !zoomFrozen)
    {
        offsetY = (int)((getHeight()-borderTop-borderBottom
                         - (container->getSize().y*factor.y))/2.0
                        - (container->getMin().y*factor.y)) + borderBottom;
    }
}
void FAS_GraphicView::centerOffsetY(FAS_Entity* entity)
{
    FAS_Entity* container = entity;
    if (container && !zoomFrozen)
    {
        offsetY = (int)((getHeight()-borderTop-borderBottom
                         - (container->getSize().y*factor.y))/2.0
                        - (container->getMin().y*factor.y)) + borderBottom;
    }
}

// Centers the given coordinate in the view in x-direction.
void FAS_GraphicView::centerX(double v)
{
    if (!zoomFrozen)
    {
        offsetX = (int)((v*factor.x)
                        - (double)(getWidth()-borderLeft-borderRight)/2.0);
    }
}
//Centers the given coordinate in the view in y-direction.
void FAS_GraphicView::centerY(double v)
{
    if (!zoomFrozen)
    {
        offsetY = (int)((v*factor.y) - (double)(getHeight()-borderTop-borderBottom)/2.0);
    }
}

//return Current action or nullptr.
FAS_ActionInterface* FAS_GraphicView::getDefaultAction()
{
    if (eventHandler)
    {
        return eventHandler->getDefaultAction();
    }
    else
    {
        return nullptr;
    }
}

// Sets the default action of the event handler.
void FAS_GraphicView::setDefaultAction(FAS_ActionInterface* action)
{
    if (eventHandler)
    {
        eventHandler->setDefaultAction(action);
    }
}

//return Current action or nullptr.
FAS_ActionInterface* FAS_GraphicView::getCurrentAction()
{
    if (eventHandler)
    {
        return eventHandler->getCurrentAction();
    }
    else
    {
        return nullptr;
    }
}

// Sets the current action of the event handler.
void FAS_GraphicView::setCurrentAction(FAS_ActionInterface* action)
{
    if (eventHandler)
    {
        eventHandler->setCurrentAction(action);
    }
}

// Kills all running selection actions. Called when a selection action is launched to reduce confusion.
void FAS_GraphicView::killSelectActions()
{
    if (eventHandler) {
        eventHandler->killSelectActions();
    }
}

// Kills all running actions.
void FAS_GraphicView::killAllActions()
{
    if (eventHandler)
    {
        eventHandler->killAllActions();
    }
}

// Go back in menu or current action.
void FAS_GraphicView::back()
{
    if (eventHandler && eventHandler->hasAction())
    {
        eventHandler->back();
    }
}

// Go forward with the current action.
void FAS_GraphicView::enter()
{
    if (eventHandler && eventHandler->hasAction())
    {
        eventHandler->enter();
    }
}

// Enables coordinate input in the command line.
void FAS_GraphicView::enableCoordinateInput()
{
    if (eventHandler)
    {
        eventHandler->enableCoordinateInput();
    }
}

// Disables coordinate input in the command line.
void FAS_GraphicView::disableCoordinateInput()
{
    if (eventHandler)
    {
        eventHandler->disableCoordinateInput();
    }
}

// zooms in by factor f
void FAS_GraphicView::zoomIn(double f, const FAS_Vector& center)
{
    if (f<1.0e-6)
    {
        return;
    }

    FAS_Vector c = center;
    if (!c.valid)
    {
        //find mouse position
        c= getMousePosition();
    }

    zoomWindow(toGraph(0, 0).scale(c, FAS_Vector(1.0/f,1.0/f)),
               toGraph(getWidth(), getHeight()).scale(c, FAS_Vector(1.0/f,1.0/f)));
    if(COMMONDEF->wheelZoomShowWidgetNum&&COMMONDEF->isWheelEvent)
    {

    }
    else
    {
        redraw();
    }

}

// zooms in by factor f in x
void FAS_GraphicView::zoomInX(double f)
{
    factor.x*=f;
    offsetX=(int)((offsetX-getWidth()/2)*f)+getWidth()/2;
    adjustOffsetControls();
    adjustZoomControls();
    redraw();
}

// zooms in by factor f in y
void FAS_GraphicView::zoomInY(double f)
{
    factor.y*=f;
    offsetY=(int)((offsetY-getHeight()/2)*f)+getHeight()/2;
    adjustOffsetControls();
    adjustZoomControls();
    redraw();
}

// zooms out by factor f
void FAS_GraphicView::zoomOut(double f, const FAS_Vector& center)
{
    if (f<1.0e-6)
    {
        return;
    }
    zoomIn(1/f, center);
}

// zooms out by factor f in x
void FAS_GraphicView::zoomOutX(double f)
{
    if (f<1.0e-6)
    {
        return;
    }
    factor.x/=f;
    offsetX=(int)(offsetX/f);
    adjustOffsetControls();
    adjustZoomControls();
    redraw();
}

void FAS_GraphicView::zoomOutY(double f)
{
    if (f<1.0e-6)
    {
        return;
    }
    factor.y/=f;
    offsetY=(int)(offsetY/f);
    adjustOffsetControls();
    adjustZoomControls();
    redraw();
}

void FAS_GraphicView::zoomAuto(bool axis, bool keepAspectRatio)
{

    if (container)
    {
        int widthNew=getWidth();
        int heightNew=getHeight();

        container->calculateBorders();
        double sx, sy;
        if (axis)
        {
            auto const dV = container->getMax() - container->getMin();
            sx = std::max(dV.x, 0.);
            sy = std::max(dV.y, 0.);
        }
        else
        {
            sx = container->getSize().x;
            sy = container->getSize().y;
        }

        double fx=1., fy=1.;
        unsigned short fFlags=0;

        if (sx>FAS_TOLERANCE)
        {
            fx = (widthNew-borderLeft-borderRight) / sx;
        }
        else
        {
            fFlags += 1; //invalid x factor
        }

        if (sy>FAS_TOLERANCE)
        {
            fy = (heightNew-borderTop-borderBottom) / sy;
        }
        else
        {
            fFlags += 2; //invalid y factor
        }

        switch(fFlags)
        {
        case 1:
            fx=fy;
            break;
        case 2:
            fy=fx;
            break;
        case 3:
            return; //do not do anything, invalid factors
        default:
            if (keepAspectRatio)
            {
                fx = fy = std::min(fx, fy);
            }
            break;
        }

        //exclude invalid factors
        fFlags=0;
        if (fx<FAS_TOLERANCE||fx>FAS_MAXDOUBLE)
        {
            fx=1.0;
            fFlags += 1;
        }
        if (fy<FAS_TOLERANCE||fy>FAS_MAXDOUBLE)
        {
            fy=1.0;
            fFlags += 2;
        }
        if(fFlags == 3 ) return;
        saveView();
        setFactorX(fx);
        setFactorY(fy);
        adjustZoomControls();
        centerOffsetX();
        centerOffsetY();
        adjustOffsetControls();
        /*if(COMMONDEF->firstOpen)
        {
            COMMONDEF->firstOpen=false;

        }*/

//        redraw();
    }
}

void FAS_GraphicView::zoomAutoBlock(bool axis, bool keepAspectRatio)
{
    if (container)
    {
        int widthNew=getWidth();
        int heightNew=getHeight();
        double sx, sy;
        if (axis)
        {
            auto const dV = container->getMax() - container->getMin();
            sx = std::max(dV.x, 0.);
            sy = std::max(dV.y, 0.);
        }
        else
        {
            sx = container->getSize().x;
            sy = container->getSize().y;
        }

        double fx=1., fy=1.;
        unsigned short fFlags=0;

        if (sx>FAS_TOLERANCE)
        {
            fx = (widthNew-borderLeft-borderRight) / sx;
        }
        else
        {
            fFlags += 1; //invalid x factor
        }

        if (sy>FAS_TOLERANCE)
        {
            fy = (heightNew-borderTop-borderBottom) / sy;
        }
        else
        {
            fFlags += 2; //invalid y factor
        }

        switch(fFlags)
        {
        case 1:
            fx=fy;
            break;
        case 2:
            fy=fx;
            break;
        case 3:
            return; //do not do anything, invalid factors
        default:
            if (keepAspectRatio)
            {
                fx = fy = std::min(fx, fy);
            }
            break;
        }

        //exclude invalid factors
        fFlags=0;
        if (fx<FAS_TOLERANCE||fx>FAS_MAXDOUBLE)
        {
            fx=1.0;
            fFlags += 1;
        }
        if (fy<FAS_TOLERANCE||fy>FAS_MAXDOUBLE)
        {
            fy=1.0;
            fFlags += 2;
        }
        if(fFlags == 3 ) return;
        saveView();
        setFactorX(fx);
        setFactorY(fy);
        adjustZoomControls();
        centerOffsetX();
        centerOffsetY();
        adjustOffsetControls();
        redraw();
    }
}

/**
 * Shows previous view.
 */
void FAS_GraphicView::zoomPrevious()
{
    if (container)
    {
        restoreView();
    }
}

/**
 * Saves the current view as previous view to which we can
 * switch back later with @see restoreView().
 */
void FAS_GraphicView::saveView()
{
    if(getGraphic()) getGraphic()->setModified(true);
    QDateTime noUpdateWindow=QDateTime::currentDateTime().addMSecs(-500);
    //do not update view within 500 milliseconds
    if(previousViewTime > noUpdateWindow) return;
    previousViewTime = QDateTime::currentDateTime();
    savedViews[savedViewIndex]=std::make_tuple(offsetX,offsetY,factor);
    savedViewIndex = (savedViewIndex+1)%savedViews.size();
    if(savedViewCount<savedViews.size()) savedViewCount++;

    if(savedViewCount==1)
    {
        emit previous_zoom_state(true);
    }
}

//Restores the view previously saved with
void FAS_GraphicView::restoreView()
{
    if(savedViewCount == 0) return;
    savedViewCount --;
    if(savedViewCount==0)
    {
        emit previous_zoom_state(false);
    }
    savedViewIndex = (savedViewIndex + savedViews.size() - 1)%savedViews.size();

    offsetX = std::get<0>(savedViews[savedViewIndex]);
    offsetY = std::get<1>(savedViews[savedViewIndex]);
    factor = std::get<2>(savedViews[savedViewIndex]);

    adjustOffsetControls();
    adjustZoomControls();
    redraw();
}

//Performs autozoom in Y axis only.
void FAS_GraphicView::zoomAutoY(bool axis)
{
    if (container) {
        double visibleHeight = 0.0;
        double minY = FAS_MAXDOUBLE;
        double maxY = FAS_MINDOUBLE;
        bool noChange = false;

        for(auto e: *container)
        {
            if (e->rtti()==FAS2::EntityLine)
            {
                FAS_Line* l = (FAS_Line*)e;
                double x1, x2;
                x1 = toGuiX(l->getStartpoint().x);
                x2 = toGuiX(l->getEndpoint().x);

                if (((x1 > 0.0) && (x1 < (double) getWidth())) ||
                        ((x2 > 0.0) && (x2 < (double) getWidth())))
                {
                    minY = std::min(minY, l->getStartpoint().y);
                    minY = std::min(minY, l->getEndpoint().y);
                    maxY = std::max(maxY, l->getStartpoint().y);
                    maxY = std::max(maxY, l->getEndpoint().y);
                }
            }
        }

        if (axis)
        {
            visibleHeight = std::max(maxY, 0.0) - std::min(minY, 0.0);
        }
        else
        {
            visibleHeight = maxY-minY;
        }

        if (visibleHeight<1.0)
        {
            noChange = true;
        }

        double fy = 1.0;
        if (visibleHeight>1.0e-6)
        {
            fy = (getHeight()-borderTop-borderBottom)
                    / visibleHeight;
            if (factor.y<0.000001)
            {
                noChange = true;
            }
        }

        if (noChange==false)
        {
            setFactorY(fy);
            //centerOffsetY();
            offsetY = (int)((getHeight()-borderTop-borderBottom
                             - (visibleHeight*factor.y))/2.0
                            - (minY*factor.y)) + borderBottom;
            adjustOffsetControls();
            adjustZoomControls();
        }
    }
}

// Zooms the area given by v1 and v2.
void FAS_GraphicView::zoomWindow(FAS_Vector v1, FAS_Vector v2,
                                 bool keepAspectRatio)
{
    double zoomX=480.0;    // Zoom for X-Axis
    double zoomY=640.0;    // Zoom for Y-Axis   (Set smaller one)
    int zoomBorder = 0;

    // Switch left/right and top/bottom is necessary:
    if(v1.x>v2.x)
    {
        std::swap(v1.x,v2.x);
    }
    if(v1.y>v2.y)
    {
        std::swap(v1.y,v2.y);
    }

    // Get zoom in X and zoom in Y:
    if(v2.x-v1.x>1.0e-6)
    {
        zoomX = getWidth() / (v2.x-v1.x);
    }
    if(v2.y-v1.y>1.0e-6)
    {
        zoomY = getHeight() / (v2.y-v1.y);
    }

    // Take smaller zoom:
    if (keepAspectRatio)
    {
        if(zoomX<zoomY)
        {
            if(getWidth()!=0)
            {
                zoomX = zoomY = ((double)(getWidth()-2*zoomBorder)) /
                        (double)getWidth()*zoomX;
            }
        }
        else
        {
            if(getHeight()!=0)
            {
                zoomX = zoomY = ((double)(getHeight()-2*zoomBorder)) /
                        (double)getHeight()*zoomY;
            }
        }
    }

    zoomX=fabs(zoomX);
    zoomY=fabs(zoomY);

    // Borders in pixel after zoom
    int pixLeft  =(int)(v1.x*zoomX);
    int pixTop   =(int)(v2.y*zoomY);
    int pixRight =(int)(v2.x*zoomX);
    int pixBottom=(int)(v1.y*zoomY);
    if(  pixLeft == INT_MIN || pixLeft== INT_MAX ||
         pixRight == INT_MIN || pixRight== INT_MAX ||
         pixTop == INT_MIN || pixTop== INT_MAX ||
         pixBottom == INT_MIN || pixBottom== INT_MAX )
    {
        return;
    }
    saveView();

    // Set new offset for zero point:
    offsetX = - pixLeft + (getWidth() -pixRight +pixLeft)/2;
    offsetY = - pixTop + (getHeight() -pixBottom +pixTop)/2;
    factor.x = zoomX;
    factor.y = zoomY;

    adjustOffsetControls();
    adjustZoomControls();
    if(COMMONDEF->wheelZoomShowWidgetNum&&COMMONDEF->isWheelEvent)
    {

    }
    else
    {
       redraw();
    }

}

// Centers the point v1.
void FAS_GraphicView::zoomPan(int dx, int dy)
{
    offsetX += dx;
    offsetY -= dy;
    adjustOffsetControls();
    redraw();
}

// Scrolls in the given direction.
void FAS_GraphicView::zoomScroll(FAS2::Direction direction)
{
    switch (direction) {
    case FAS2::Up:
        offsetY-=50;
        break;
    case FAS2::Down:
        offsetY+=50;
        break;
    case FAS2::Right:
        offsetX+=50;
        break;
    case FAS2::Left:
        offsetX-=50;
        break;
    }
    adjustOffsetControls();
    adjustZoomControls();
    redraw();
}
// Zooms to page extends.
void FAS_GraphicView::zoomPage()
{
    if (!container)
    {
        return;
    }

    FAS_Graphic* graphic = container->getGraphic();
    if (!graphic)
    {
        return;
    }
    FAS_Vector s = graphic->getPaperSize()/graphic->getPaperScale();
    double fx, fy;
    if (s.x>FAS_TOLERANCE)
    {
        fx = (getWidth()-borderLeft-borderRight) / s.x;
    }
    else
    {
        fx = 1.0;
    }

    if (s.y>FAS_TOLERANCE)
    {
        fy = (getHeight()-borderTop-borderBottom) / s.y;
    } else {
        fy = 1.0;
    }
    fx = fy = std::min(fx, fy);

    if (fx<FAS_TOLERANCE)
    {
        fx=fy=1.0;
    }

    setFactorX(fx);
    setFactorY(fy);
    centerOffsetX();
    centerOffsetY();
    adjustOffsetControls();
    adjustZoomControls();
    redraw();
}

// Draws the entities.
void FAS_GraphicView::drawLayer1(FAS_Painter *painter)
{
    if (isPrintPreview())
    {
        drawPaper(painter);
    }
    else
    {
        int dpiX = qApp->desktop()->logicalDpiX();
        const FAS_Pen penSaved=painter->getPen();
        if(dpiX>96) {
            FAS_Pen pen=penSaved;
            pen.setWidth(FAS2::Width01);
            painter->setPen(pen);
        }

        drawMetaGrid(painter);

        if(dpiX>96)
            painter->setPen(penSaved);
    }
}

void FAS_GraphicView::drawLayer2(FAS_Painter *painter)
{
    COMMONDEF->drawEntityCount=0;

    drawEntity(painter, container);
    if (!isPrintPreview())
        drawAbsoluteZero(painter);
}

void FAS_GraphicView::drawLayer3(FAS_Painter *painter)
{
    if (!isPrintPreview())
    {
        //drawRelativeZero(painter);
        drawOverlay(painter);
    }
}

void FAS_GraphicView::setPenForEntity(FAS_Painter *painter,FAS_Entity *e)
{
    if (draftMode)
    {
        painter->setPen(FAS_Pen(foreground,
                                FAS2::Width00, FAS2::SolidLine));
    }

    // Getting pen from entity (or layer)
    FAS_Pen pen = e->getPen(true);
    int w = pen.getWidth();
    if (w<0)
    {
        w = 0;
    }

    // - Scale pen width.
    // - Notes: pen width is not scaled on print and print preview.
    //   This is the standard (AutoCAD like) behaviour.
    // ------------------------------------------------------------
    if (!draftMode)
    {
        double uf = 1.0; // Unit factor.
        double wf = 1.0; // Width factor.

        FAS_Graphic* graphic = container->getGraphic();

        if (graphic)
        {
            uf = FAS_Units::convert(1.0, FAS2::Millimeter, graphic->getUnit());

            if ((isPrinting() || isPrintPreview()) &&
                    graphic->getPaperScale() > FAS_TOLERANCE )
            {
                wf = 1.0 / graphic->getPaperScale();
            }
        }

        pen.setScreenWidth(toGuiDX(w / 100.0 * uf * wf));
    }
    else
    {
        pen.setScreenWidth(0);
    }
    // prevent drawing with 1-width which is slow:
    if (FAS_Math::round(pen.getScreenWidth())==1) {
        pen.setScreenWidth(0.0);
    }

    // prevent background color on background drawing:
    if (pen.getColor().stripFlags()==background.stripFlags()) {
        pen.setColor(foreground);
    }

    // this entity is selected:
    if (e->isSelected()) {
        pen.setLineType(FAS2::SolidLine); // DotLine modified 0725 ltree
        pen.setColor(selectedColor);
    }

    // this entity is highlighted:
    if (e->isHighlighted()) {
        pen.setColor(highlightedColor);
    }

    // deleting not drawing:
    if (getDeleteMode()) {
        pen.setColor(background);
    }
    if(COMMONDEF->convertDeviceToImage)
    {
        pen.setColor(Qt::black);
    }
    painter->setPen(pen);
}
void FAS_GraphicView::drawEntity(FAS_Entity* /*e*/, double& /*patternOffset*/)
{
    redraw(FAS2::RedrawDrawing);
}
void FAS_GraphicView::drawEntity(FAS_Entity* /*e*/ /*patternOffset*/)
{
    redraw(FAS2::RedrawDrawing);
}
void FAS_GraphicView::drawEntity(FAS_Painter *painter, FAS_Entity* e)
{
    double offset(0.);
    drawEntity(painter,e,offset);
}
void FAS_GraphicView::drawEntity(FAS_Painter *painter, FAS_Entity* e,FAS_InsertData& data)
{
    double offset(0.);
    drawEntity(painter,e,offset,data);
}
void FAS_GraphicView::drawEntity(FAS_Painter *painter, FAS_Entity* e, double& patternOffset,FAS_InsertData& insertData)
{
    // given entity is nullptr:
    if (!e)
    {
        return;
    }

    // entity is not visible:
    if (!e->isVisible())
    {
        return;
    }
    e->resetBorders();
    if (fabs(insertData.scaleFactor.x)>1.0e-6 && fabs(insertData.scaleFactor.y)>1.0e-6)
    {

        e->resetBasicData(insertData.insertionPoint,insertData.insertionPoint, insertData.angle,insertData.scaleFactor);

        e->moveEntityBoarder(insertData.insertionPoint,insertData.insertionPoint, insertData.angle,insertData.scaleFactor);
  }else
    {
         e->resetBasicData(insertData.insertionPoint,insertData.insertionPoint, insertData.angle,insertData.scaleFactor);
         e->moveEntityBoarder(insertData.insertionPoint,insertData.insertionPoint, insertData.angle,insertData.scaleFactor);
    }

    e->calculateBorders();

    if( isPrintPreview() || isPrinting() )
    {
        // do not draw construction layer on print preview or print
        if(e->isConstruction())
            return;
    }

    // test if the entity is in the viewport

    if (!isPrinting()
            && e->rtti() != FAS2::EntityOverlayLine
            && e->rtti() != FAS2::EntityOverlayBox
            && e->rtti() != FAS2::EntityGraphic
            && e->rtti() != FAS2::EntityPreview&&
            //&& e->rtti() != FAS2::EntityLine &&
            (toGuiX(e->getMax().x)<0 || toGuiX(e->getMin().x)>getWidth() ||
             toGuiY(e->getMin().y)<0 || toGuiY(e->getMax().y)>getHeight())) {

        return;
    }

//    qDebug() <<"Insert Name:" << insertData.name;
//    qDebug() << "toGuiX Size " << toGuiX(max.x) << toGuiY(max.y) << toGuiX(min.x) <<toGuiY(min.y);
//    qDebug() << "Insert Size " << max.x << max.y << min.x <<min.y;

//    If the entity is a container and its size is much smaller that view size, just paint a rectangle.

    //qDebug()<<"COMMONDEF->drawEntityCount:"<<COMMONDEF->drawEntityCount;
    if(e->isContainer() && !isPrinting()&& e->rtti() != FAS2::EntityGraphic)
    {
        FAS_Vector entitySize = toGui(e->getMax()) - toGui(e->getMin());
        int displayThd = COMMONDEF->displayThd;
        if(qAbs(qRound(entitySize.x))+qAbs(qRound(entitySize.y))>0)
        {
        bool isVisibleClear = (getWidth() + getHeight()) / (fabs(entitySize.x) + fabs(entitySize.y)) < displayThd;
        if(!isVisibleClear)
        {
            COMMONDEF->drawEntityCount++;
            painter->drawRect(toGui(e->getMin()), toGui(e->getMax()));
            return;
        }
        }
    }

    // set pen (color):
    setPenForEntity(painter, e );
    if (isDraftMode())
    {
        switch(e->rtti())
        {
        case FAS2::EntityMText:
        case FAS2::EntityText:
            if (toGuiDX(((FAS_MText*)e)->getHeight())<4 || e->countDeep()>100)
            {
                // large or tiny texts as rectangles:
                COMMONDEF->drawEntityCount++;
                painter->drawRect(toGui(e->getMin()), toGui(e->getMax()));
            }
            else
            {
                drawEntityPlain(painter, e, patternOffset,insertData);
            }
            break;
        case FAS2::EntityImage:
            // all images as rectangles:
            COMMONDEF->drawEntityCount++;
            painter->drawRect(toGui(e->getMin()), toGui(e->getMax()));
            break;
        case FAS2::EntityHatch:
            //skip hatches
            break;
        default:
            drawEntityPlain(painter, e, patternOffset,insertData);
        }
    }
    else
    {
        drawEntityPlain(painter, e, patternOffset,insertData);
    }

    // draw reference points:
    if (e->isSelected())
    {
        if (!e->isParentSelected())
        {
            FAS_VectorSolutions const& s = e->getRefPoints();

            for (size_t i=0; i<s.getNumber(); ++i)
            {
                int sz = -1;
                FAS_Color col = handleColor;
                if (i == 0)
                {
                    col = startHandleColor;
                }
                else if (i == s.getNumber() - 1)
                {
                    col = endHandleColor;
                }
                if (getDeleteMode())
                {
                    painter->drawHandle(toGui(s.get(i)), background, sz);
                }
                else
                {
                    painter->drawHandle(toGui(s.get(i)), col, sz);
                }
            }
        }
    }
}
void FAS_GraphicView::drawEntity(FAS_Painter *painter, FAS_Entity* e, double& patternOffset)
{
    // given entity is nullptr:
    if (!e)
    {
        return;
    }

    // entity is not visible:
    if (!e->isVisible())
    {
        return;
    }

    if( isPrintPreview() || isPrinting() )
    {
        // do not draw construction layer on print preview or print
        if(e->isConstruction())
            return;
    }

    // test if the entity is in the viewport
    auto maxX=e->getMax().x;
    auto maxY=e->getMax().y;
    auto minX=e->getMin().x;
    auto minY=e->getMin().y;
    auto xMax=toGuiX(e->getMax().x);//<0
    auto xMin=toGuiX(e->getMin().x);//>getWidth()
    auto yMax=toGuiY(e->getMax().y);//>getHeight()
    auto yMin=toGuiY(e->getMin().y);//<0
    if (!isPrinting()
            && e->rtti() != FAS2::EntityOverlayLine
            && e->rtti() != FAS2::EntityOverlayBox
            && e->rtti() != FAS2::EntityGraphic
            && e->rtti() != FAS2::EntityPreview &&
            //&& e->rtti() != FAS2::EntityLine &&
            (toGuiX(e->getMax().x)<0 || toGuiX(e->getMin().x)>getWidth() ||
             toGuiY(e->getMin().y)<0 || toGuiY(e->getMax().y)>getHeight())) {
//        qDebug() << "!isPrinting()" <<!isPrinting();
//        qDebug() << "oGuiX(e->getMax().x)<0" << toGuiX(e->getMax().x);
//        qDebug() << "toGuiX(e->getMin().x)" << toGuiX(e->getMin().x);
//        qDebug() << "getWidth()" << getWidth();
//        qDebug() << "toGuiY(e->getMin().y)" <<toGuiY(e->getMin().y);
//        qDebug() << "toGuiY(e->getMax().y)" <<toGuiY(e->getMax().y);
//        qDebug() << "getHeight()" <<getHeight();
        return;
    }

    //If the entity is a container and its size is much smaller that view size, just paint a rectangle.
    if(e->isContainer() && !isPrinting()&& e->rtti() != FAS2::EntityGraphic)
    {
        FAS_Vector entitySize = toGui(e->getMax()) - toGui(e->getMin());
        if(qAbs(qRound(entitySize.x))+qAbs(qRound(entitySize.y))>0)
        {
        int displayThd = COMMONDEF->displayThd;
        bool isVisibleClear = (getWidth() + getHeight()) / (fabs(entitySize.x) + fabs(entitySize.y)) < displayThd;
        if(!isVisibleClear)
        {
            COMMONDEF->drawEntityCount++;
            painter->drawRect(toGui(e->getMin()), toGui(e->getMax()));
            return;
        }
        }
    }

    // set pen (color):
//    auto ePex = e->getPen();
//    qDebug() << "Entity PenEx: " << ePex.getColor().toQColor().toRgb();
//    auto temPex = painter->getPen();
//    qDebug() << "painter PenEx: " << temPex.getColor().toQColor().toRgb();
    setPenForEntity(painter, e);
//    auto ePcr = e->getPen();
//    qDebug() << "Entity PenCr: " << ePcr.getColor().toQColor().toRgb();
//    auto temPcr = painter->getPen();
//    qDebug() << "painter PenCr: " << temPcr.getColor().toQColor().toRgb();
    if (isDraftMode())
    {
        switch(e->rtti())
        {
        case FAS2::EntityMText:
        case FAS2::EntityText:
            if (toGuiDX(((FAS_MText*)e)->getHeight())<4 || e->countDeep()>100)
            {
                // large or tiny texts as rectangles:
                COMMONDEF->drawEntityCount++;
                painter->drawRect(toGui(e->getMin()), toGui(e->getMax()));
            }
            else
            {
                drawEntityPlain(painter, e, patternOffset);
            }
            break;
        case FAS2::EntityImage:
            // all images as rectangles:
            COMMONDEF->drawEntityCount++;
            painter->drawRect(toGui(e->getMin()), toGui(e->getMax()));
            break;
        case FAS2::EntityHatch:
            //skip hatches
            break;
        default:
            drawEntityPlain(painter, e, patternOffset);
        }
    }
    else
    {
        drawEntityPlain(painter, e, patternOffset);
    }

    // draw reference points:
    if (e->isSelected())
    {
        if (!e->isParentSelected())
        {
            FAS_VectorSolutions const& s = e->getRefPoints();

            for (size_t i=0; i<s.getNumber(); ++i)
            {
                int sz = -1;
                FAS_Color col = handleColor;
                if (i == 0)
                {
                    col = startHandleColor;
                }
                else if (i == s.getNumber() - 1)
                {
                    col = endHandleColor;
                }
                if (getDeleteMode())
                {
                    painter->drawHandle(toGui(s.get(i)), background, sz);
                }
                else
                {
                    painter->drawHandle(toGui(s.get(i)), col, sz);
                }
            }
        }
    }
}

// The painter must be initialized and all the attributes (pen) must be set.
void FAS_GraphicView::drawEntityPlain(FAS_Painter *painter, FAS_Entity* e, double& patternOffset)
{
    if (!e) {
        return;
    }
    if (!e->isContainer() && (!e->isSelected())&&painter->shouldDrawSelected())
    //if (!e->isContainer() && (e->isSelected()!=painter->shouldDrawSelected()))
    {
        return;
    }

    e->draw(painter, this, patternOffset);

}
void FAS_GraphicView::drawEntityPlain(FAS_Painter *painter, FAS_Entity* e)
{
    if (!e)
    {
        return;
    }
    if (!e->isContainer() && (!e->isSelected())&&painter->shouldDrawSelected())
    //if (!e->isContainer() && (e->isSelected()!=painter->shouldDrawSelected()))
    {
        return;
    }
    double patternOffset(0.);
    e->draw(painter, this, patternOffset);
}
// The painter must be initialized and all the attributes (pen) must be set.
void FAS_GraphicView::drawEntityPlain(FAS_Painter *painter, FAS_Entity* e, double& patternOffset,FAS_InsertData& data)
{
    if (!e) {
        return;
    }
    if (!e->isContainer() && (!e->isSelected())&&painter->shouldDrawSelected())
    //if (!e->isContainer() && (e->isSelected()!=painter->shouldDrawSelected()))
    {
        return;
    }

    e->drawInsertEntity(painter, this, patternOffset,data);

}
void FAS_GraphicView::drawEntityPlain(FAS_Painter *painter, FAS_Entity* e,FAS_InsertData& data)
{
    if (!e)
    {
        return;
    }
    if (!e->isContainer() && (!e->isSelected())&&painter->shouldDrawSelected())
    //if (!e->isContainer() && (e->isSelected()!=painter->shouldDrawSelected()))
    {
        return;
    }
    double patternOffset(0.);
    e->drawInsertEntity(painter, this, patternOffset,data);
}
// Deletes an entity with the background color. Might be recusively called e.g. for polylines.
void FAS_GraphicView::deleteEntity(FAS_Entity* e)
{
    setDeleteMode(true);
    drawEntity(e);
    setDeleteMode(false);
    redraw(FAS2::RedrawDrawing);
}

/*
 * return Pointer to the static pattern struct that belongs to the
 * given pattern type or nullptr.
 */
const FAS_LineTypePattern* FAS_GraphicView::getPattern(FAS2::LineType t)
{
    switch (t)
    {
    case FAS2::SolidLine:
        return &FAS_LineTypePattern::patternSolidLine;
        break;

    case FAS2::DotLine:
        return &FAS_LineTypePattern::patternDotLine;
        break;
    case FAS2::DotLineTiny:
        return &FAS_LineTypePattern::patternDotLineTiny;
        break;
    case FAS2::DotLine2:
        return &FAS_LineTypePattern::patternDotLine2;
        break;
    case FAS2::DotLineX2:
        return &FAS_LineTypePattern::patternDotLineX2;
        break;

    case FAS2::DashLine:
        return &FAS_LineTypePattern::patternDashLine;
        break;
    case FAS2::DashLineTiny:
        return &FAS_LineTypePattern::patternDashLineTiny;
        break;
    case FAS2::DashLine2:
        return &FAS_LineTypePattern::patternDashLine2;
        break;
    case FAS2::DashLineX2:
        return &FAS_LineTypePattern::patternDashLineX2;
        break;

    case FAS2::DashDotLine:
        return &FAS_LineTypePattern::patternDashDotLine;
        break;
    case FAS2::DashDotLineTiny:
        return &FAS_LineTypePattern::patternDashDotLineTiny;
        break;
    case FAS2::DashDotLine2:
        return &FAS_LineTypePattern::patternDashDotLine2;
        break;
    case FAS2::DashDotLineX2:
        return &FAS_LineTypePattern::patternDashDotLineX2;
        break;

    case FAS2::DivideLine:
        return &FAS_LineTypePattern::patternDivideLine;
        break;
    case FAS2::DivideLineTiny:
        return &FAS_LineTypePattern::patternDivideLineTiny;
        break;
    case FAS2::DivideLine2:
        return &FAS_LineTypePattern::patternDivideLine2;
        break;
    case FAS2::DivideLineX2:
        return &FAS_LineTypePattern::patternDivideLineX2;
        break;

    case FAS2::CenterLine:
        return &FAS_LineTypePattern::patternCenterLine;
        break;
    case FAS2::CenterLineTiny:
        return &FAS_LineTypePattern::patternCenterLineTiny;
        break;
    case FAS2::CenterLine2:
        return &FAS_LineTypePattern::patternCenterLine2;
        break;
    case FAS2::CenterLineX2:
        return &FAS_LineTypePattern::patternCenterLineX2;
        break;

    case FAS2::BorderLine:
        return &FAS_LineTypePattern::patternBorderLine;
        break;
    case FAS2::BorderLineTiny:
        return &FAS_LineTypePattern::patternBorderLineTiny;
        break;
    case FAS2::BorderLine2:
        return &FAS_LineTypePattern::patternBorderLine2;
        break;
    case FAS2::BorderLineX2:
        return &FAS_LineTypePattern::patternBorderLineX2;
        break;

    case FAS2::LineByLayer:
        return &FAS_LineTypePattern::patternBlockLine;
        break;
    case FAS2::LineByBlock:
        return &FAS_LineTypePattern::patternBlockLine;
        break;
    default:
        break;
    }
    return nullptr;
}

/*
 * This virtual method can be overwritten to draw the absolute
 * zero. It's called from within drawIt(). The default implemetation
 * draws a simple red cross on the zero of thge sheet
 * THis function can ONLY be called from within a paintEvent because it will
 * use the painter
 */
void FAS_GraphicView::drawAbsoluteZero(FAS_Painter *painter)
{
    int const zr = 20;
    FAS_Pen p(FAS_Color(255,0,0), FAS2::Width00, FAS2::SolidLine);
    p.setScreenWidth(0);
    painter->setPen(p);
    auto vp=toGui(FAS_Vector(0,0));
    if( vp.x +zr<0 || vp.x-zr>getWidth()) return;
    if( vp.y +zr<0 || vp.y-zr>getHeight()) return;

    painter->drawLine(FAS_Vector(vp.x-zr, vp.y),
                      FAS_Vector(vp.x+zr, vp.y)
                      );
    painter->drawLine(FAS_Vector(vp.x, vp.y-zr),
                      FAS_Vector(vp.x, vp.y+zr)
                      );
}

/*
 * This virtual method can be overwritten to draw the relative
 * zero point. It's called from within drawIt(). The default implemetation
 * draws a simple red round zero point. This is the point that was last created by the user, end of a line for example
 * THis function can ONLY be called from within a paintEvent because it will
 * use the painter
 */
void FAS_GraphicView::drawRelativeZero(FAS_Painter *painter)
{
    if (!relativeZero.valid)
    {
        return;
    }

    FAS_Pen p(FAS_Color(255,0,0), FAS2::Width00, FAS2::SolidLine);
    p.setScreenWidth(0);
    painter->setPen(p);

    int const zr=5;
    auto vp=toGui(relativeZero);
    if( vp.x +zr<0 || vp.x-zr>getWidth()) return;
    if( vp.y +zr<0 || vp.y-zr>getHeight()) return;

    painter->drawLine(FAS_Vector(vp.x-zr, vp.y),
                      FAS_Vector(vp.x+zr, vp.y)
                      );
    painter->drawLine(FAS_Vector(vp.x, vp.y-zr),
                      FAS_Vector(vp.x, vp.y+zr)
                      );

    painter->drawCircle(vp, 5);
}

// Draws the paper border (for print previews).
void FAS_GraphicView::drawPaper(FAS_Painter *painter)
{
    if (!container)
    {
        return;
    }

    FAS_Graphic* graphic = container->getGraphic();
    if (graphic->getPaperScale()<1.0e-6)
    {
        return;
    }

    // draw paper:
    painter->setPen(QColor(Qt::gray));

    FAS_Vector pinsbase = graphic->getPaperInsertionBase();
    FAS_Vector size = graphic->getPaperSize();
    double scale = graphic->getPaperScale();

    FAS_Vector v1 = toGui((FAS_Vector(0,0)-pinsbase)/scale);
    FAS_Vector v2 = toGui((size-pinsbase)/scale);

    // gray background:
    painter->fillRect(0,0, getWidth(), getHeight(),
                      FAS_Color(200,200,200));

    // shadow
    painter->fillRect(
                (int)(v1.x)+6, (int)(v1.y)+6,
                (int)((v2.x-v1.x)), (int)((v2.y-v1.y)),
                FAS_Color(64,64,64));

    // border:
    painter->fillRect(
                (int)(v1.x), (int)(v1.y),
                (int)((v2.x-v1.x)), (int)((v2.y-v1.y)),
                FAS_Color(64,64,64));

    // paper
    painter->fillRect(
                (int)(v1.x)+1, (int)(v1.y)-1,
                (int)((v2.x-v1.x))-2, (int)((v2.y-v1.y))+2,
                FAS_Color(255,255,255));
}

// Draws the meta grid.
void FAS_GraphicView::drawMetaGrid(FAS_Painter *painter)
{
    if (!(grid && isGridOn()))
    {
        return;
    }

    grid->updatePointArray();
    FAS_Pen pen(metaGridColor,
                FAS2::Width00,
                FAS2::DotLine);
    painter->setPen(pen);

    FAS_Vector dv=grid->getMetaGridWidth().scale(factor);
    double dx=fabs(dv.x);
    double dy=fabs(dv.y); //potential bug, need to recover metaGrid.width
    // draw meta grid:
    auto mx = grid->getMetaX();
    for(auto const& x: mx){
        painter->drawLine(FAS_Vector(toGuiX(x), 0),
                          FAS_Vector(toGuiX(x), getHeight()));
        if(grid->isIsometric()){
            painter->drawLine(FAS_Vector(toGuiX(x)+0.5*dx, 0),
                              FAS_Vector(toGuiX(x)+0.5*dx, getHeight()));
        }
    }
    auto my = grid->getMetaY();
    if(grid->isIsometric()){//isometric metaGrid
        dx=fabs(dx);
        dy=fabs(dy);
        if(!my.size()|| dx<1||dy<1) return;
        FAS_Vector baseMeta(toGui(FAS_Vector(mx[0],my[0])));
        // x-x0=k*dx, x-remainder(x-x0,dx)
        FAS_Vector vp0(-remainder(-baseMeta.x,dx)-dx,getHeight()-remainder(getHeight()-baseMeta.y,dy)+dy);
        FAS_Vector vp1(vp0);
        FAS_Vector vp2(getWidth()-remainder(getWidth()-baseMeta.x,dx)+dx,vp0.y);
        FAS_Vector vp3(vp2);
        int cmx = round((vp2.x - vp0.x)/dx);
        int cmy = round((vp0.y +remainder(-baseMeta.y,dy)+dy)/dy);
        for(int i=cmx+cmy+2;i>=0;i--){
            if ( i <= cmx ) {
                vp0.x += dx;
                vp2.y -= dy;
            }else{
                vp0.y -= dy;
                vp2.x -= dx;
            }
            if ( i <= cmy ) {
                vp1.y -= dy;
                vp3.x -= dx;
            }else{
                vp1.x += dx;
                vp3.y -= dy;
            }
            painter->drawLine(vp0,vp1);
            painter->drawLine(vp2,vp3);
        }

    }
    else
    {//orthogonal
        for(auto const& y: my)
        {
            painter->drawLine(FAS_Vector(0, toGuiY(y)),
                              FAS_Vector(getWidth(), toGuiY(y)));
        }
    }
}

void FAS_GraphicView::drawOverlay(FAS_Painter *painter)
{
    QList<int> const& keys=overlayEntities.keys();
    for (int i = 0; i < keys.size(); ++i)
    {
        if (overlayEntities[i])
        {
            setPenForEntity(painter, overlayEntities[i] );
            drawEntityPlain(painter, overlayEntities[i]);
        }
    }
}

FAS2::SnapRestriction FAS_GraphicView::getSnapRestriction() const
{
    return defaultSnapRes;
}

FAS_SnapMode FAS_GraphicView::getDefaultSnapMode() const
{
    return defaultSnapMode;
}

// Sets the default snap mode used by newly created actions.
void FAS_GraphicView::setDefaultSnapMode(FAS_SnapMode sm)
{
    defaultSnapMode = sm;
    if (eventHandler) {
        eventHandler->setSnapMode(sm);
    }
}

// Sets a snap restriction (e.g. orthogonal).
void FAS_GraphicView::setSnapRestriction(FAS2::SnapRestriction sr)
{
    defaultSnapRes = sr;

    if (eventHandler)
    {
        eventHandler->setSnapRestriction(sr);
    }
}

// Translates a vector in real coordinates to a vector in screen coordinates.
FAS_Vector FAS_GraphicView::toGui(FAS_Vector v) const
{
    return FAS_Vector(toGuiX(v.x), toGuiY(v.y));
}

// Translates a real coordinate in X to a screen coordinate X.
double FAS_GraphicView::toGuiX(double x) const
{
    return x*factor.x + offsetX;
}

// Translates a real coordinate in Y to a screen coordinate Y.
double FAS_GraphicView::toGuiY(double y) const
{
    return -y*factor.y + getHeight() - offsetY;
}

// Translates a real coordinate distance to a screen coordinate distance.
double FAS_GraphicView::toGuiDX(double d) const
{
    return d*factor.x;
}
// Translates a real coordinate distance to a screen coordinate distance.
double FAS_GraphicView::toGuiDY(double d) const
{
    return d*factor.y;
}

// Translates a vector in screen coordinates to a vector in real coordinates.
FAS_Vector FAS_GraphicView::toGraph(FAS_Vector v) const
{
    return FAS_Vector(toGraphX(FAS_Math::round(v.x)),
                      toGraphY(FAS_Math::round(v.y)));
}

//Translates two screen coordinates to a vector in real coordinates.
FAS_Vector FAS_GraphicView::toGraph(int x, int y) const
{
    return FAS_Vector(toGraphX(x), toGraphY(y));
}


// Translates a screen coordinate in X to a real coordinate X.
double FAS_GraphicView::toGraphX(int x) const
{
    return (x - offsetX)/factor.x;
}

// Translates a screen coordinate in Y to a real coordinate Y.
double FAS_GraphicView::toGraphY(int y) const
{
    return -(y - getHeight() + offsetY)/factor.y;
}

// Translates a screen coordinate distance to a real coordinate distance.
double FAS_GraphicView::toGraphDX(int d) const
{
    return d/factor.x;
}

/**
 * Translates a screen coordinate distance to a real coordinate distance.
 */
double FAS_GraphicView::toGraphDY(int d) const
{
    return d/factor.y;
}

/**
 * Sets the relative zero coordinate (if not locked)
 * without deleting / drawing the point.
 */
void FAS_GraphicView::setRelativeZero(const FAS_Vector& pos)
{
    if (relativeZeroLocked==false)
    {
        relativeZero = pos;
        emit relative_zero_changed(pos);
    }
}

// Sets the relative zero coordinate, deletes the old positionon the screen and draws the new one.
void FAS_GraphicView::moveRelativeZero(const FAS_Vector& pos)
{
    setRelativeZero(pos);
    redraw(FAS2::RedrawGrid);
}

// Gets the specified overlay container.
FAS_EntityContainer* FAS_GraphicView::getOverlayContainer(FAS2::OverlayGraphics position)
{
    if (overlayEntities[position])
    {
        return overlayEntities[position];
    }
    overlayEntities[position]=new FAS_EntityContainer(nullptr);

    return overlayEntities[position];

}

FAS_Grid* FAS_GraphicView::getGrid() const
{
    return grid.get();
}

FAS_EventHandler* FAS_GraphicView::getEventHandler() const
{
    return eventHandler;
}

void FAS_GraphicView::setBackground(const FAS_Color& bg)
{
    background = bg;

    // bright background:
    if (bg.red()+bg.green()+bg.blue()>380) {
        foreground = FAS_Color(0,0,0);
    } else {
        foreground = FAS_Color(255,255,255);
    }
}

void FAS_GraphicView::setBorders(int left, int top, int right, int bottom)
{
    borderLeft = left;
    borderTop = top;
    borderRight = right;
    borderBottom = bottom;
}

FAS_Graphic* FAS_GraphicView::getGraphic() const{
    if (container && container->rtti()==FAS2::EntityGraphic)
    {
        return static_cast<FAS_Graphic *>(container);
    } else {
        return nullptr;
    }
}

FAS_EntityContainer* FAS_GraphicView::getContainer() const
{
    return container;
}

void FAS_GraphicView::setFactor(double f)
{
    setFactorX(f);
    setFactorY(f);
}

FAS_Vector FAS_GraphicView::getFactor() const
{
    return factor;
}
int FAS_GraphicView::getBorderLeft() const
{
    return borderLeft;
}
int FAS_GraphicView::getBorderTop() const
{
    return borderTop;
}
int FAS_GraphicView::getBorderRight() const
{
    return borderRight;
}
int FAS_GraphicView::getBorderBottom() const
{
    return borderBottom;
}

void FAS_GraphicView::freezeZoom(bool freeze)
{
    zoomFrozen=freeze;
}
bool FAS_GraphicView::isZoomFrozen() const
{
    return zoomFrozen;
}
void FAS_GraphicView::setOffsetX(int ox)
{
    offsetX = ox;
}
void FAS_GraphicView::setOffsetY(int oy)
{
    offsetY = oy;
}
int FAS_GraphicView::getOffsetX() const
{
    return offsetX;
}
int FAS_GraphicView::getOffsetY() const
{
    return offsetY;
}
void FAS_GraphicView::lockRelativeZero(bool lock)
{
    relativeZeroLocked=lock;
}

bool FAS_GraphicView::isRelativeZeroLocked() const
{
    return relativeZeroLocked;
}

FAS_Vector const& FAS_GraphicView::getRelativeZero() const
{
    return relativeZero;
}

void FAS_GraphicView::setPrintPreview(bool pv)
{
    printPreview = pv;
}

bool FAS_GraphicView::isPrintPreview() const
{
    return printPreview;
}

void FAS_GraphicView::setPrinting(bool p)
{
    printing = p;
}

bool FAS_GraphicView::isPrinting() const
{
    return printing;
}

bool FAS_GraphicView::isDraftMode() const
{
    return draftMode;
}

void FAS_GraphicView::setDraftMode(bool dm)
{
    draftMode=dm;
}

bool FAS_GraphicView::isCleanUp(void) const
{
    return m_bIsCleanUp;
}

bool FAS_GraphicView::isPanning() const
{
    return panning;
}

void FAS_GraphicView::setPanning(bool state)
{
    panning = state;
}

