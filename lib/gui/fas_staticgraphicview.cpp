/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_staticgraphicview.h"

//sort with alphabetical order
#include <QSize>

FAS_StaticGraphicView::FAS_StaticGraphicView(int w, int h, FAS_Painter* /*p*/, QSize const* pb):
    width(w)
  ,height(h)
{
    setBackground({255,255,255});
    QSize b{5, 5};
    if (pb) b = *pb;
    setBorders(b.width(), b.height(), b.width(), b.height());
}

int FAS_StaticGraphicView::getWidth() const
{
    return width;
}
int FAS_StaticGraphicView::getHeight() const
{
    return height;
}

FAS_Vector FAS_StaticGraphicView::getMousePosition() const
{
    return FAS_Vector(false);
}

void FAS_StaticGraphicView::paint()
{
    redraw(FAS2::RedrawAll);
}

