/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

//sort with alphabetical order
#include <cmath>

#include "fas_painterqt.h"
#include "fas_math.h"
#include "qdebug.h"
#include "commondef.h"
namespace {
Qt::PenStyle rsToQtLineType(FAS2::LineType t)
{
    switch (t)
    {
    case FAS2::NoPen:
        return Qt::NoPen;
    case FAS2::SolidLine:
        return Qt::SolidLine;
    case FAS2::DotLine:
    case FAS2::DotLineTiny:
    case FAS2::DotLine2:
    case FAS2::DotLineX2:
        return Qt::DotLine;
    case FAS2::DashLine:
    case FAS2::DashLineTiny:
    case FAS2::DashLine2:
    case FAS2::DashLineX2:
        return Qt::DashLine;
    case FAS2::DashDotLine:
    case FAS2::DashDotLineTiny:
    case FAS2::DashDotLine2:
    case FAS2::DashDotLineX2:
        return Qt::DashDotLine;
    case FAS2::DivideLine:
    case FAS2::DivideLineTiny:
    case FAS2::DivideLine2:
    case FAS2::DivideLineX2:
        return Qt::DashDotDotLine;
    case FAS2::CenterLine:
    case FAS2::CenterLineTiny:
    case FAS2::CenterLine2:
    case FAS2::CenterLineX2:
        return Qt::DashDotLine;
    case FAS2::BorderLine:
    case FAS2::BorderLineTiny:
    case FAS2::BorderLine2:
    case FAS2::BorderLineX2:
        return Qt::DashDotLine;
    case FAS2::LineByLayer:
    case FAS2::LineByBlock:
    default:
        return Qt::SolidLine;
    }
    return Qt::SolidLine;
}
}
FAS_PainterQt::FAS_PainterQt( QPaintDevice* pd)
    : QPainter(pd), FAS_Painter() {}
FAS_PainterQt::~FAS_PainterQt(){}

void FAS_PainterQt::moveTo(int x, int y)
{
    rememberX=x;
    rememberY=y;
}


void FAS_PainterQt::lineTo(int x, int y)
{
    setPenForConvertDeviceToImage();
    QPainterPath path;
    path.moveTo(rememberX,rememberY);
    path.lineTo(x,y);
    QPainter::drawPath(path);
    rememberX=x;
    rememberY=y;
}

//Draws a grid point at (x1, y1).
void FAS_PainterQt::drawGridPoint(const FAS_Vector& p)
{
    QPainter::drawPoint(toScreenX(p.x), toScreenY(p.y));
}

// Draws a point at (x1, y1).
void FAS_PainterQt::drawPoint(const FAS_Vector& p)
{
    setPenForConvertDeviceToImage();
    QPainter::drawLine(toScreenX(p.x-1), toScreenY(p.y),
                       toScreenX(p.x+1), toScreenY(p.y));
    QPainter::drawLine(toScreenX(p.x), toScreenY(p.y-1),
                       toScreenX(p.x), toScreenY(p.y+1));
}

// Draws a line from (x1, y1) to (x2, y2).
void FAS_PainterQt::drawLine(const FAS_Vector& p1, const FAS_Vector& p2)
{
    setPenForConvertDeviceToImage();
    QPainter::drawLine(toScreenX(p1.x), toScreenY(p1.y),
                       toScreenX(p2.x), toScreenY(p2.y));
}

// Draws an arc which starts / ends exactly at the given coordinates.
void FAS_PainterQt::drawArc(const FAS_Vector& cp, double radius,
                            double a1, double a2,
                            const FAS_Vector& p1, const FAS_Vector& p2,
                            bool reversed)
{
    setPenForConvertDeviceToImage();
    if(radius<=0.5)
    {
        drawGridPoint(cp);
    }
    else
    {
        int   cix;            // Next point on circle
        int   ciy;            //
        double aStep;         // Angle Step (rad)
        double a;             // Current Angle (rad)
        double linStep;       // linear step (pixels)

        if (drawingMode==FAS2::ModePreview)
        {
            linStep = 20.0;
        }
        else
        {
            linStep = 6.0;
        }

        if (fabs(linStep/radius)<=1.0)
        {
            aStep=asin(linStep/radius);
        }
        else
        {
            aStep=1.0;
        }

        if (aStep<0.05)
        {
            aStep = 0.05;
        }

        if(!reversed)
        {
            // Arc Counterclockwise:
            if(a1>a2-1.0e-10)
            {
                a2+=2*M_PI;
            }
            //moveTo(toScreenX(p1.x), toScreenY(p1.y));
            QPolygon pa;
            int i=0;
            pa.resize(i+1);
            pa.setPoint(i++, toScreenX(p1.x), toScreenY(p1.y));
            for(a=a1+aStep; a<=a2; a+=aStep)
            {
                cix = toScreenX(cp.x+cos(a)*radius);
                ciy = toScreenY(cp.y-sin(a)*radius);
                //lineTo(cix, ciy);
                pa.resize(i+1);
                pa.setPoint(i++, cix, ciy);
            }
            //lineTo(toScreenX(p2.x), toScreenY(p2.y));
            pa.resize(i+1);
            pa.setPoint(i++, toScreenX(p2.x), toScreenY(p2.y));
            drawPolyline(pa);
        }
        else
        {
            // Arc Clockwise:
            if(a1<a2+1.0e-10)
            {
                a2-=2*M_PI;
            }
            QPolygon pa;
            int i=0;
            pa.resize(i+1);
            pa.setPoint(i++, toScreenX(p1.x), toScreenY(p1.y));
            for(a=a1-aStep; a>=a2; a-=aStep)
            {
                cix = toScreenX(cp.x+cos(a)*radius);
                ciy = toScreenY(cp.y-sin(a)*radius);
                pa.resize(i+1);
                pa.setPoint(i++, cix, ciy);
            }
            pa.resize(i+1);
            pa.setPoint(i++, toScreenX(p2.x), toScreenY(p2.y));
            drawPolyline(pa);
        }
    }
}

// Draws an arc.
void FAS_PainterQt::drawArc(const FAS_Vector& cp, double radius,
                            double a1, double a2,
                            bool reversed)
{
    setPenForConvertDeviceToImage();
    if(radius<=0.5)
    {
        drawGridPoint(cp);
    }
    else
    {
        QPolygon pa;
        createArc(pa, cp, radius, a1, a2, reversed);
        drawPolyline(pa);
    }
}

// Draws a circle.
void FAS_PainterQt::drawCircle(const FAS_Vector& cp, double radius)
{
    setPenForConvertDeviceToImage();
    if (radius<500)
    {
        // This is _very_ slow for large arcs:
        QPainter::drawEllipse(toScreenX(cp.x-radius),
                              toScreenY(cp.y-radius),
                              FAS_Math::round(2.0*radius),
                              FAS_Math::round(2.0*radius));
    }
    else
    {
        drawArc(cp,
                radius,
                0.0, 2*M_PI,
                cp + FAS_Vector(radius, 0.0),
                cp + FAS_Vector(radius, 0.0),
                false);
    }
}

// Draws a rotated ellipse arc.
void FAS_PainterQt::drawEllipse(const FAS_Vector& cp,
                                double radius1, double radius2,
                                double angle,
                                double a1, double a2,
                                bool reversed)
{
    QPolygon pa;
    createEllipse(pa, cp, radius1, radius2, angle, a1, a2, reversed);
    setPenForConvertDeviceToImage();
    drawPolyline(pa);
}

// Draws image.
void FAS_PainterQt::drawImg(QImage& img, const FAS_Vector& pos,
                            double angle, const FAS_Vector& factor)
{
    save();
    if (factor.x < 1 || factor.y < 1)
    {
        FAS_PainterQt::setRenderHint(SmoothPixmapTransform , true);
    }
    else
    {
        FAS_PainterQt::setRenderHint(SmoothPixmapTransform);
    }

    QMatrix wm;
    wm.translate(pos.x, pos.y);
    wm.rotate(FAS_Math::rad2deg(-angle));
    wm.scale(factor.x, factor.y);
    setWorldMatrix(wm);
    drawImage(0,-img.height(), img);
    restore();
}

void FAS_PainterQt::drawTextH(int x1, int y1,
                              int x2, int y2,
                              const QString& text)
{
    drawText(x1, y1, x2, y2,
             Qt::AlignLeft|Qt::AlignVCenter,
             text);
}

void FAS_PainterQt::fillRect(int x1, int y1, int w, int h,
                             const FAS_Color& col)
{
    QPainter::fillRect(x1, y1, w, h, col);
}


void FAS_PainterQt::fillTriangle(const FAS_Vector& p1,
                                 const FAS_Vector& p2,
                                 const FAS_Vector& p3)
{

    QPolygon arr(3);
    QBrush brushSaved=brush();
    arr.putPoints(0, 3,
                  toScreenX(p1.x),toScreenY(p1.y),
                  toScreenX(p2.x),toScreenY(p2.y),
                  toScreenX(p3.x),toScreenY(p3.y));
    setBrush(FAS_Color(pen().color()));
    drawPolygon(arr);
    setBrush(brushSaved);
}


void FAS_PainterQt::erase()
{
    QPainter::eraseRect(0,0,getWidth(),getHeight());
}


int FAS_PainterQt::getWidth() const
{
    return device()->width();
}

// get Density per millimeter on screen/print device
double FAS_PainterQt::getDpmm() const
{
    int mm(device()->widthMM());
    if(mm==0)
        mm=400;
    return double(device()->width())/mm;
}


int FAS_PainterQt::getHeight() const
{
    return device()->height();
}

const QBrush& FAS_PainterQt::brush() const
{
    return QPainter::brush();
}

FAS_Pen FAS_PainterQt::getPen() const
{
    return lpen;
}

void FAS_PainterQt::setPen(const FAS_Pen& pen)
{
    lpen = pen;
    if (drawingMode==FAS2::ModeBW)
    {
        lpen.setColor(FAS_Color(0,0,0));
    }
    QPen p(lpen.getColor(), FAS_Math::round(lpen.getScreenWidth()),
           rsToQtLineType(lpen.getLineType()));
    p.setJoinStyle(Qt::RoundJoin);
    p.setCapStyle(Qt::RoundCap);
    QPainter::setPen(p);
}

void FAS_PainterQt::setPen(const FAS_Color& color)
{
    if (drawingMode==FAS2::ModeBW)
    {
        lpen.setColor(FAS_Color(0,0,0));
        QPainter::setPen(FAS_Color(0,0,0));
    }
    else
    {
        lpen.setColor(color);
        QPainter::setPen(color);
    }
}

void FAS_PainterQt::setPen(int r, int g, int b)
{
    if (drawingMode==FAS2::ModeBW)
    {
        setPen(QColor(0, 0, 0));
    }
    else
    {
        setPen(QColor(r, g, b));
    }
}

void FAS_PainterQt::disablePen()
{
    lpen = FAS_Pen(FAS2::FlagInvalid);
    QPainter::setPen(Qt::NoPen);
}

void FAS_PainterQt::setFont(const QFont& font)
{
    QPainter::setFont(font);
}

void FAS_PainterQt::setBrush(const FAS_Color& color)
{
    if (drawingMode==FAS2::ModeBW)
    {
        QPainter::setBrush(QColor(0, 0, 0));
    }
    else
    {
        QPainter::setBrush(color);
    }
}

void FAS_PainterQt::setBrush(const QBrush& color)
{
    QPainter::setBrush(color);
}

void FAS_PainterQt::drawPolygon(const QPolygon& a, Qt::FillRule rule)
{
    setPenForConvertDeviceToImage();
    QPainter::drawPolygon(a,rule);
}

void FAS_PainterQt::drawPath ( const QPainterPath & path )
{
    setPenForConvertDeviceToImage();
    QPainter::drawPath(path);
}


void FAS_PainterQt::setClipRect(int x, int y, int w, int h)
{
    QPainter::setClipRect(x, y, w, h);
    setClipping(true);
}

void FAS_PainterQt::resetClipping()
{
    setClipping(false);
}

void FAS_PainterQt::fillRect ( const QRectF & rectangle, const FAS_Color & color )
{
    double x1=rectangle.left();
    double x2=rectangle.right();
    double y1=rectangle.top();
    double y2=rectangle.bottom();
    QPainter::fillRect(toScreenX(x1),toScreenY(y1),toScreenX(x2)-toScreenX(x1),toScreenY(y2)-toScreenX(y1), color);
}
void FAS_PainterQt::fillRect ( const QRectF & rectangle, const QBrush & brush )
{
    double x1=rectangle.left();
    double x2=rectangle.right();
    double y1=rectangle.top();
    double y2=rectangle.bottom();
    QPainter::fillRect(toScreenX(x1),toScreenY(y1),toScreenX(x2),toScreenY(y2), brush);
}
void FAS_PainterQt::setPenForConvertDeviceToImage()
{
    //myl
    if(COMMONDEF->convertDeviceToImage)
    {
//        int penwidth=this->getPen().getWidth();
            QPen pen;
            pen.setColor(Qt::black);
            pen.setWidth(2);
            QPainter::setPen(pen);
    }
}
