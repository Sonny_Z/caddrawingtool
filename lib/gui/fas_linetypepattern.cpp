/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_linetypepattern.h"

//sort with alphabetical order
#include<cmath>

FAS_LineTypePattern::FAS_LineTypePattern(std::initializer_list<double> const& pattern):
    pattern(pattern)
{
    totalLength=0.;
    num = pattern.size();
    for(double const& l: this->pattern){
        totalLength += fabs(l);
    }
}

//define all line patterns in pixels
const FAS_LineTypePattern FAS_LineTypePattern::patternSolidLine={10.0};

const FAS_LineTypePattern FAS_LineTypePattern::patternDotLineTiny{{0.15, -1.}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDotLine{{0.2, -6.2}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDotLine2{{0.2, -3.1}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDotLineX2{{0.2, -12.4}};

const FAS_LineTypePattern FAS_LineTypePattern::patternDashLineTiny{{2., -1.}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDashLine{{12.0, -6.0}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDashLine2{{6.0, -3.0}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDashLineX2{{24.0, -12.0}};

const FAS_LineTypePattern FAS_LineTypePattern::patternDashDotLineTiny{{2., -0.7, 0.15, -2.}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDashDotLine{{12.0, -5., 0.2, -5.95}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDashDotLine2{{6.0, -2., 0.2, -2.}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDashDotLineX2{{24.0, -8., 0.2, -8.}};

const FAS_LineTypePattern FAS_LineTypePattern::patternDivideLineTiny{{2., -0.7, 0.15, -0.7, 0.15, -0.7}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDivideLine{{12.0, -4.9, 0.2, -4.9, 0.2, -4.9}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDivideLine2{{6.0, -1.9, 0.2, -1.9, 0.2, -1.9}};
const FAS_LineTypePattern FAS_LineTypePattern::patternDivideLineX2{{24.0, -8., 0.2, -8., 0.2, -8.}};

const FAS_LineTypePattern FAS_LineTypePattern::patternCenterLineTiny{{5., -1., 1., -1.}};
const FAS_LineTypePattern FAS_LineTypePattern::patternCenterLine{{32.0, -6.0, 6.0, -6.0}};
const FAS_LineTypePattern FAS_LineTypePattern::patternCenterLine2{{16.0, -3.0, 3.0, -3.0}};
const FAS_LineTypePattern FAS_LineTypePattern::patternCenterLineX2{{64.0, -12.0, 12.0, -12.0}};

const FAS_LineTypePattern FAS_LineTypePattern::patternBorderLineTiny{{2., -1., 2., -1., 0.15, -1.}};
const FAS_LineTypePattern FAS_LineTypePattern::patternBorderLine{{12.0, -4.0, 12.0, -4., 0.2, -4.}};
const FAS_LineTypePattern FAS_LineTypePattern::patternBorderLine2{{6.0, -3.0, 6.0, -3., 0.2, -3.}};
const FAS_LineTypePattern FAS_LineTypePattern::patternBorderLineX2{{24.0, -8.0, 24.0, -8., 0.2, -8.}};

const FAS_LineTypePattern FAS_LineTypePattern::patternBlockLine{{0.5, -0.5}};
const FAS_LineTypePattern FAS_LineTypePattern::patternSelected{{1.0, -3.0}};
