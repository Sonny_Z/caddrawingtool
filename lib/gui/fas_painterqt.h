/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_PAINTERQT_H
#define FAS_PAINTERQT_H

#include <QPainter>

#include "fas_painter.h"
#include "fas_pen.h"

/*
 * The Qt implementation of a painter. It can draw objects such as
 * lines or arcs in a widget. All coordinates are screen coordinates
 * and have nothing to do with the graphic view.
 */
class FAS_PainterQt: public QPainter, public FAS_Painter
{

public:
    FAS_PainterQt( QPaintDevice* pd);
    ~FAS_PainterQt();
    virtual void moveTo(int x, int y);
    virtual void lineTo(int x, int y);
    virtual void drawGridPoint(const FAS_Vector& p);
    virtual void drawPoint(const FAS_Vector& p);
    virtual void drawLine(const FAS_Vector& p1, const FAS_Vector& p2);
    //virtual void drawRect(const FAS_Vector& p1, const FAS_Vector& p2);
    virtual void fillRect ( const QRectF & rectangle, const FAS_Color & color );
    virtual void fillRect ( const QRectF & rectangle, const QBrush & brush );
    virtual void drawArc(const FAS_Vector& cp, double radius,
                         double a1, double a2,
                         const FAS_Vector& p1, const FAS_Vector& p2,
                         bool reversed);

    virtual void drawArc(const FAS_Vector& cp, double radius,
                         double a1, double a2,
                         bool reversed);
    virtual void drawCircle(const FAS_Vector&, double radius);
    virtual void drawEllipse(const FAS_Vector& cp,
                             double radius1, double radius2,
                             double angle,
                             double a1, double a2,
                             bool reversed);
        virtual void drawImg(QImage& img, const FAS_Vector& pos,
            double angle, const FAS_Vector& factor);
    virtual void drawTextH(int x1, int y1, int x2, int y2,
                           const QString& text);
    virtual void fillRect(int x1, int y1, int w, int h,
                          const FAS_Color& col);

    virtual void fillTriangle(const FAS_Vector& p1,
                              const FAS_Vector& p2,
                              const FAS_Vector& p3);

    virtual void drawPolygon(const QPolygon& a,Qt::FillRule rule=Qt::WindingFill);
    virtual void drawPath ( const QPainterPath & path );
    virtual void erase();
    virtual int getWidth() const;
    virtual double getDpmm() const;
    virtual int getHeight() const;
    virtual FAS_Pen getPen() const;
    virtual void setPen(const FAS_Pen& pen);
    virtual void setPen(const FAS_Color& color);
    virtual void setPen(int r, int g, int b);
    virtual void disablePen();
    virtual void setFont(const QFont& font);
    //virtual void setColor(const QColor& color);
    virtual const QBrush& brush() const;
    virtual void setBrush(const FAS_Color& color);
    virtual void setBrush(const QBrush& color);

    virtual void setClipRect(int x, int y, int w, int h);
    virtual void resetClipping();
private:
    void setPenForConvertDeviceToImage();
protected:
    FAS_Pen lpen;
    long rememberX; // Used for the moment because QPainter doesn't support moveTo anymore, thus we need to remember ourselve the moveTo positions
    long rememberY;
};

#endif

