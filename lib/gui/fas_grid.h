/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_GRID_H
#define FAS_GRID_H

#include "fas_vector.h"

class FAS_GraphicView;
class QString;
namespace lc {
namespace geo {
class Area;
}
}

using LC_Rect = lc::geo::Area;

//Grids can be drawn on graphic views
class FAS_Grid
{
public:
    FAS_Grid(FAS_GraphicView* graphicView);
    void updatePointArray();
    // Array of all visible grid points.
    std::vector<FAS_Vector> const& getPoints() const;
    //return Number of visible grid points.
    int count() const;
    void setCrosshairType(FAS2::CrosshairType chType);
    FAS2::CrosshairType getCrosshairType() const;
    //return Grid info for status widget.
    QString getInfo() const;
    //return a vector of Meta grid positions in X.
    std::vector<double> const& getMetaX() const;
    //return a vector of Meta grid positions in Y.
    std::vector<double> const& getMetaY() const;

    bool isIsometric() const;
    void setIsometric(bool b);
    FAS_Vector getMetaGridWidth() const;
    FAS_Vector const& getCellVector() const;

private:
    // copy ctor disabled
    FAS_Grid(FAS_Grid const&) = delete;
    FAS_Grid& operator = (FAS_Grid const&) = delete;
    void createOrthogonalGrid(LC_Rect const& rect, FAS_Vector const& gridWidth);
    void createIsometricGrid(LC_Rect const& rect, FAS_Vector const& gridWidth);
    FAS_Vector getMetricGridWidth(FAS_Vector const& userGrid, bool scaleGrid, int minGridSpacing);
    FAS_Vector getImperialGridWidth(FAS_Vector const& userGrid, bool scaleGrid, int minGridSpacing);
    // Graphic view this grid is connected to.
    FAS_GraphicView* graphicView;
    // Current grid spacing
    double spacing;
    // Current meta grid spacing
    double metaSpacing;
    // Pointer to array of grid points
    std::vector<FAS_Vector> pt;
    FAS_Vector baseGrid; // the left-bottom grid point
    FAS_Vector cellV;// (dx,dy)
    FAS_Vector metaGridWidth;
    // Meta grid positions in X
    std::vector<double> metaX;
    // Meta grid positions in Y
    std::vector<double> metaY;
    bool isometric;
    FAS2::CrosshairType crosshairType;
};

#endif
