/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

//sort with alphabetical order
#include <cmath>
#include <QPolygon>

#include "fas_color.h"
#include "fas_math.h"
#include "fas_pen.h"
#include "fas_painter.h"

void FAS_Painter::createArc(QPolygon& pa,
                            const FAS_Vector& cp, double radius,
                            double a1, double a2,
                            bool reversed)
{
    if (radius<1.0e-6)
    {
        return;
    }

    double aStep=fabs(2.0/radius);         // Angle Step (rad)
    if(aStep>=0.5) aStep=0.5;
    if(reversed)
    {
        if(a1<=a2+FAS_TOLERANCE) a1+=2.*M_PI;
        aStep *= -1;
    }
    else
    {
        if(a2<=a1+FAS_TOLERANCE) a2+=2.*M_PI;
    }
    double a;             // Current Angle (rad)
    //QPointArray pa;
    pa.clear();
    double da=fabs(a2-a1);
    for(a=a1; fabs(a-a1)<da; a+=aStep)
    {
        pa<<QPoint(toScreenX(cp.x+cos(a)*radius), toScreenY(cp.y-sin(a)*radius));
    }

    QPoint pt2(toScreenX(cp.x+cos(a2)*radius), toScreenY(cp.y-sin(a2)*radius));
    if(pa.size()>0 && pa.last() != pt2)
        pa<<pt2;
}

void FAS_Painter::createEllipse(QPolygon& pa,
                                const FAS_Vector& cp,
                                double radius1, double radius2,
                                double angle,
                                double angle1, double angle2,
                                bool reversed)
{
    const FAS_Vector vr(radius1,radius2);
    const FAS_Vector rvp(radius2,radius1);
    const double ab=radius1*radius2;
    double ea1=angle1;
    double ea2;
    double dA=FAS_Math::getAngleDifference(angle1, angle2, reversed);
    if(dA <= FAS_TOLERANCE_ANGLE)
    {
        dA=2.*M_PI;
        ea2 =ea1 + dA;
    }
    else
        ea2 = ea1 +(reversed?-dA:dA);
    const FAS_Vector angleVector(-angle);
    FAS_Vector vp(-ea1);
    vp.scale(vr);
    vp.rotate(angleVector);
    vp.move(cp);
    pa.clear();
    const double minDea=fabs(ea2-ea1)/2048.;
    // Arc Counterclockwise:
    do
    {
        FAS_Vector va(-ea1);
        vp=va;
        double r2=va.scale(rvp).squared();
        if( r2<FAS_TOLERANCE15) r2=FAS_TOLERANCE15;
        double aStep=ab/(r2*sqrt(r2));
        if(aStep < minDea) aStep=minDea;
        if(aStep > M_PI/4.) aStep=M_PI/4.;
        ea1 += reversed?-aStep:aStep;
        vp.scale(vr);
        vp.rotate(angleVector);
        vp.move(cp);
        pa<<QPoint(toScreenX(vp.x),
                   toScreenY(vp.y));
    } while(fabs(angle1-ea1)<dA);

    vp.set(cos(ea2)*radius1, -sin(ea2)*radius2);
    vp.rotate(angleVector);
    vp.move(cp);
    pa<<QPoint(toScreenX(vp.x), toScreenY(vp.y));
}

void FAS_Painter::drawRect(const FAS_Vector& p1, const FAS_Vector& p2)
{
    drawPolygon(QRect(int(p1.x+0.5), int(p1.y+0.5), int(p2.x - p1.x+0.5), int(p2.y - p1.y+0.5)));
}

void FAS_Painter::drawHandle(const FAS_Vector& p, const FAS_Color& c, int size)
{
    if (size<0)
    {
        size = 2;
    }
    fillRect((int)(p.x-size), (int)(p.y-size), 2*size, 2*size, c);
}

int FAS_Painter::toScreenX(double x) const
{
    return FAS_Math::round(offset.x + x);
}

int FAS_Painter::toScreenY(double y) const
{
    return FAS_Math::round(offset.y + y);
}

