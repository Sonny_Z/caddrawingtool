/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/


#ifndef FAS_EVENTHANDLER_H
#define FAS_EVENTHANDLER_H

#include <QObject>

#include "fas_vector.h"
#include "fas_entity.h"

class FAS_ActionInterface;
class QAction;
class QMouseEvent;
class QKeyEvent;
class FAS_CommandEvent;
class FAS_Vector;

struct FAS_SnapMode;

/*
 * The event handler owns and manages all actions that are currently
 * active. All events going from the view to the actions come over
 * this class.
 */
class FAS_EventHandler : public QObject
{
    Q_OBJECT

public:
    FAS_EventHandler(QObject* parent = 0);
    ~FAS_EventHandler();

    void setQAction(QAction* action);

    void back();
    void enter();

    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseLeaveEvent();
    void mouseEnterEvent();

    void keyPressEvent(QKeyEvent* e);
    void keyReleaseEvent(QKeyEvent* e);

    void enableCoordinateInput();
    void disableCoordinateInput();

    void setDefaultAction(FAS_ActionInterface* action);
    FAS_ActionInterface* getDefaultAction() const;

    void setCurrentAction(FAS_ActionInterface* action);
    FAS_ActionInterface* getCurrentAction();
    bool isValid(FAS_ActionInterface* action) const;

    void killSelectActions();
    void killAllActions();

    bool hasAction();
    void cleanUp();
    void setSnapMode(FAS_SnapMode sm);
    void setSnapRestriction(FAS2::SnapRestriction sr);

    // return true if the current action is for selecting
    bool inSelectionMode();

private:
    QAction* q_action{nullptr};
    FAS_ActionInterface* defaultAction{nullptr};
    QList<FAS_ActionInterface*> currentActions;
    bool coordinateInputEnabled{true};
    FAS_Vector relative_zero;
public:
    FAS_Entity* en{nullptr};

public slots:
    void setRelativeZero(const FAS_Vector&);
};

#endif
