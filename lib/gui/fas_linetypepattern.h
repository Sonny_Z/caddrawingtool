/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_LINETYPEPATTERN_H
#define FAS_LINETYPEPATTERN_H

#include <vector>
#include <cstddef>

// Stores a line type pattern.
struct FAS_LineTypePattern {
    FAS_LineTypePattern()=delete;
    FAS_LineTypePattern(std::initializer_list<double> const& pattern);

    std::vector<double> pattern;
    double totalLength;
    size_t num;
    //define all line patterns in pixels
    static const FAS_LineTypePattern patternSolidLine;

    static const FAS_LineTypePattern patternDotLineTiny;
    static const FAS_LineTypePattern patternDotLine;
    static const FAS_LineTypePattern patternDotLine2;
    static const FAS_LineTypePattern patternDotLineX2;

    static const FAS_LineTypePattern patternDashLineTiny;
    static const FAS_LineTypePattern patternDashLine;
    static const FAS_LineTypePattern patternDashLine2;
    static const FAS_LineTypePattern patternDashLineX2;

    static const FAS_LineTypePattern patternDashDotLineTiny;
    static const FAS_LineTypePattern patternDashDotLine;
    static const FAS_LineTypePattern patternDashDotLine2;
    static const FAS_LineTypePattern patternDashDotLineX2;

    static const FAS_LineTypePattern patternDivideLineTiny;
    static const FAS_LineTypePattern patternDivideLine;
    static const FAS_LineTypePattern patternDivideLine2;
    static const FAS_LineTypePattern patternDivideLineX2;

    static const FAS_LineTypePattern patternCenterLineTiny;
    static const FAS_LineTypePattern patternCenterLine;
    static const FAS_LineTypePattern patternCenterLine2;
    static const FAS_LineTypePattern patternCenterLineX2;

    static const FAS_LineTypePattern patternBorderLineTiny;
    static const FAS_LineTypePattern patternBorderLine;
    static const FAS_LineTypePattern patternBorderLine2;
    static const FAS_LineTypePattern patternBorderLineX2;

    static const FAS_LineTypePattern patternBlockLine;
    static const FAS_LineTypePattern patternSelected;
};

#endif
