/**************************************************************************
* This file is part of the FAS Managementt tool project.*
**********************************************************************/

#ifndef FAS_H
#define FAS_H

#define FAS_MAXDOUBLE 1.0E+10
#define FAS_MINDOUBLE -1.0E+10
//tolerance
#define FAS_TOLERANCE 1.0e-10
//squared tolerance
#define FAS_TOLERANCE15 1.5e-15
#define FAS_TOLERANCE2 1.0e-20
#define FAS_TOLERANCE_ANGLE 1.0e-8
#define MYL 1
class FAS2
{
public:
    // Flags.
    enum Flags
    {
        // Flag for Undoables.
        FlagUndone      = 1<<0,
        // Entity Visibility.
        FlagVisible     = 1<<1,
        // Entity attribute (e.g. color) is defined by layer.
        FlagByLayer     = 1<<2,
        // Entity attribute (e.g. color) defined by block.
        FlagByBlock     = 1<<3,
        // Layer frozen.
        FlagFrozen      = 1<<4,
        // Layer frozen by default.
        FlagDefFrozen   = 1<<5,
        // Layer locked.
        FlagLocked      = 1<<6,
        // Used for invalid pens.
        FlagInvalid     = 1<<7,
        // Entity in current selection.
        FlagSelected    = 1<<8,
        // Polyline closed?
        FlagClosed      = 1<<9,
        // Flag for temporary entities (e.g. hatch)
        FlagTemp        = 1<<10,
        // Flag for processed entities (optcontour)
        FlagProcessed   = 1<<11,
        // Startpoint selected
        FlagSelected1   = 1<<12,
        // Endpoint selected
        FlagSelected2   = 1<<13,
        // Entity is highlighted temporarily (as a user action feedback)
        FlagHighlighted = 1<<14
    };

    // Variable types used by FAS_VariableDict and FAS_Variable.
    enum VariableType
    {
        VariableString,
        VariableInt,
        VariableDouble,
        VariableVector,
        VariableVoid
    };

    // File types. Used by file dialogs.
    enum FormatType
    {
        FormatUnknown,         //Unknown / unsupported format.
        FormatDXF,             //DXF format.
        FormatDWG              //DWG format. / unsupported format.
    };

    // Entity types returned by the rtti() method
    enum EntityType
    {
        EntityUnknown,      //Unknown
        EntityContainer,    //Container
        EntityBlock,        //Block (Group definition)
        EntityFontChar,     //Font character
        EntityInsert,       //Insert (Group instance)
        EntityGraphic,      //Graphic with layers
        EntityPoint,        //Point
        EntityLine,         //Line
        EntityPolyline,     //Polyline
        EntityVertex,       //Vertex (part of a polyline)
        EntityArc,          //Arc
        EntityCircle,       //Circle
        EntityEllipse,      //Ellipse
        EntitySolid,        //Solid
        EntityConstructionLine, //Construction line
        EntityAttdef,
        EntityAttrib,
        EntityMText,         //Multi-line Text
        EntityText,         //Single-line Text
        EntityDimAligned,   //Aligned Dimension
        EntityDimLinear,    //Linear Dimension
        EntityDimRadial,    //Radial Dimension
        EntityDimDiametric, //Diametric Dimension
        EntityDimAngular,   //Angular Dimension
        EntityDimLeader,    //Leader Dimension
        EntityHatch,        //Hatch
        EntityImage,        //Image
        EntitySpline,       //Spline
        EntitySplinePoints,       //SplinePoints
        EntityOverlayBox,    //OverlayBox
        EntityPreview,    //Preview Container
        EntityPattern,
        EntityOverlayLine
    };

    // Action types used by action factories.
    enum ActionType
    {
        ActionNone,        //Invalid action id.
        ActionDefault,

        ActionEditKillAllActions,
        ActionEditUndo,
        ActionEditRedo,

        ActionSelect,
        ActionSelectSingle,
        ActionSelectWindow,
        ActionDeselectWindow,

        ActionModifyDelete
    };

    // Entity ending. Used for returning which end of an entity is ment.
    enum Ending
    {
        EndingStart,    //Start point.
        EndingEnd,      //End point.
        EndingNone      //Neither.
    };

    // Update mode for non-atomic entities that need to be updated when they change.
    enum UpdateMode
    {
        NoUpdate,       //No automatic updates.
        Update,         //Always update automatically when modified.
    };

    // Drawing mode.
    enum DrawingMode
    {
        ModeFull,       //Draw everything always detailed (default)
        ModeAuto,       //Draw details when reasonable
        ModePreview,    //Draw only in black/white without styles
        ModeBW          //Black/white. Can be used for printing.
    };

    // Undoable rtti.
    enum UndoableType
    {
        UndoableUnknown,    //Unknown undoable
        UndoableEntity,     //Entity
        UndoableLayer       //Layer
    };

    // Units.
    enum Unit
    {
        None = 0,               //No unit (unit from parent)
        Inch = 1,               //Inch
        Foot = 2,               //Foot: 12 Inches
        Mile = 3,               //Mile: 1760 Yards = 1609 m
        Millimeter = 4,         //Millimeter: 0.001m
        Centimeter = 5,         //Centimeter: 0.01m
        Meter = 6,              //Meter
        Kilometer = 7,          //Kilometer: 1000m
        Microinch = 8,          //Microinch: 0.000001
        Mil = 9,                //Mil = 0.001 Inch
        Yard = 10,              //Yard: 3 Feet
        Angstrom = 11,          //Angstrom: 10^-10m
        Nanometer = 12,         //Nanometer: 10^-9m
        Micron = 13,            //Micron: 10^-6m
        Decimeter = 14,         //Decimeter: 0.1m
        Decameter = 15,         //Decameter: 10m
        Hectometer = 16,        //Hectometer: 100m
        Gigameter = 17,         //Gigameter: 1000000m
        Astro = 18,             //Astro: 149.6 x 10^9m
        Lightyear = 19,         //Lightyear: 9460731798 x 10^6m
        Parsec = 20,            //Parsec: 30857 x 10^12

        LastUnit = 21           //Used to iterate through units
    };

    // Format for length values.
    enum LinearFormat
    {
        // Scientific (e.g. 2.5E+05)
        Scientific,
        // Decimal (e.g. 9.5)*/
        Decimal,
        // Engineering (e.g. 7' 11.5")*/
        Engineering,
        // Architectural (e.g. 7'-9 1/8")*/
        Architectural,
        // Fractional (e.g. 7 9 1/8)
        Fractional
    };

    // Angle Units.
    enum AngleUnit
    {
        Deg,               // Degrees
        Rad,               // Radians
        Gra                // Gradians
    };

    // Display formats for angles.
    enum AngleFormat
    {
        // Degrees with decimal point
        DegreesDecimal,
        // Degrees, Minutes and Seconds
        DegreesMinutesSeconds,
        // Gradians with decimal point
        Gradians,
        // Radians with decimal point
        Radians,
        // Surveyor's units
        Surveyors
    };

    // Enum of levels of resolving when iterating through an entity tree.
    enum ResolveLevel
    {
        // Groups are not resolved
        ResolveNone,
        // Resolve all but not Inserts.
        ResolveAllButInserts,
        // Resolve all but not Text or MText.
        ResolveAllButTexts,
        // Resolve no text or images,
        ResolveAllButTextImage,
        //all Entity Containers are resolved
        ResolveAll
    };

    // Direction used for scrolling actions.
    enum Direction
    {
        Up, Left, Right, Down
    };

    // Direction for zooming actions.
    enum ZoomDirection
    {
        In, Out
    };

    // Axis specification for zooming actions.
    enum Axis
    {
        OnlyX,
        OnlyY,
        Both
    };

    // Crosshair type
    enum CrosshairType
    {
        LeftCrosshair,        // Left type isometric Crosshair
        TopCrosshair,         // Top type isometric Crosshair
        RightCrosshair,       // Right type isometric Crosshair
        OrthogonalCrosshair   // Orthogonal Crosshair
    };

    // Snapping modes
    enum SnapMode
    {
        SnapFree,               // Free positioning
        SnapGrid,               // Snap to grid points
        SnapEndpoint,           // Snap to endpoints
        SnapMiddle,             // Snap to middle points
        SnapCenter,             // Snap to centers
        SnapOnEntity,           // Snap to the next point on an entity
        SnapDist,               // Snap to points with a distance to an endpoint
        SnapIntersection,       // Snap to intersection
        SnapIntersectionManual  // Snap to intersection manually
    };

    // Snap restrictions
    enum SnapRestriction
    {
        RestrictNothing,        // No restriction to snap mode
        RestrictHorizontal,     // Restrict to 0,180 degrees
        RestrictVertical,       // Restrict to 90,270 degrees
        RestrictOrthogonal      // Restrict to 90,180,270,0 degrees
    };

    // Enum of line styles:
    enum LineType
    {
        LineByBlock = -2,     // Line type defined by block not entity
        LineByLayer = -1,     // Line type defined by layer not entity
        NoPen = 0,            // No line at all.
        SolidLine = 1,        // Normal line.

        DotLine = 2,          // Dotted line.
        DotLineTiny = 3,      // Dotted line tiny
        DotLine2 = 4,         // Dotted line small.
        DotLineX2 = 5,        // Dotted line large.

        DashLine = 6,         // Dashed line.
        DashLineTiny=7,       // Dashed line tiny
        DashLine2 = 8,        // Dashed line small.
        DashLineX2 = 9,       // Dashed line large.

        DashDotLine = 10,     // Alternate dots and dashes
        DashDotLineTiny = 11, // Alternate dots and dashes tiny
        DashDotLine2 = 12,    // Alternate dots and dashes small.
        DashDotLineX2 = 13,   // Alternate dots and dashes large.

        DivideLine = 14,      // dash, dot, dot.
        DivideLineTiny = 15,  // dash, dot, dot, tiny
        DivideLine2 = 16,     // dash, dot, dot small.
        DivideLineX2 = 17,    // dash, dot, dot large.

        CenterLine = 18,      // dash, small dash.
        CenterLineTiny = 19,  // dash, small dash tiny
        CenterLine2 = 20,     // dash, small dash small.
        CenterLineX2 = 21,    // dash, small dash large.

        BorderLine = 22,      // dash, dash, dot.
        BorderLineTiny = 23,      // dash, dash, dot tiny
        BorderLine2 = 24,     // dash, dash, dot small.
        BorderLineX2 = 25,    // dash, dash, dot large.

        LineTypeUnchanged=26  // Line type defined by block not entity
    };

    // Enum of line widths:
    enum LineWidth
    {
        Width00 = 0,       //Width 1.  (0.00mm)
        Width01 = 5,       //Width 2.  (0.05mm)
        Width02 = 9,       //Width 3.  (0.09mm)
        Width03 = 13,      //Width 4.  (0.13mm)
        Width04 = 15,      //Width 5.  (0.15mm)
        Width05 = 18,      //Width 6.  (0.18mm)
        Width06 = 20,      //Width 7.  (0.20mm)
        Width07 = 25,      //Width 8.  (0.25mm)
        Width08 = 30,      //Width 9.  (0.30mm)
        Width09 = 35,      //Width 10. (0.35mm)
        Width10 = 40,      //Width 11. (0.40mm)
        Width11 = 50,      //Width 12. (0.50mm)
        Width12 = 53,      //Width 13. (0.53mm)
        Width13 = 60,      //Width 14. (0.60mm)
        Width14 = 70,      //Width 15. (0.70mm)
        Width15 = 80,      //Width 16. (0.80mm)
        Width16 = 90,      //Width 17. (0.90mm)
        Width17 = 100,     //Width 18. (1.00mm)
        Width18 = 106,     //Width 19. (1.06mm)
        Width19 = 120,     //Width 20. (1.20mm)
        Width20 = 140,     //Width 21. (1.40mm)
        Width21 = 158,     //Width 22. (1.58mm)
        Width22 = 200,     //Width 23. (2.00mm)
        Width23 = 211,     //Width 24. (2.11mm)
        WidthForImage = 666,     //Width 24. (6.66mm)
        WidthByLayer = -1, //Line width defined by layer not entity.
        WidthByBlock = -2, //Line width defined by block not entity.
        WidthDefault = -3  //Line width defaults to the predefined line width.
    };

    // Enum of cursor types.

    enum CursorType
    {
        ArrowCursor,          //ArrowCursor - standard arrow cursor.
        UpArrowCursor,        //UpArrowCursor - upwards arrow.
        CrossCursor,          //CrossCursor - crosshair.
        WaitCursor,           //WaitCursor - hourglass/watch.
        IbeamCursor,          //IbeamCursor - ibeam/text entry.
        SizeVerCursor,        //SizeVerCursor - vertical resize.
        SizeHorCursor,        //SizeHorCursor - horizontal resize.
        SizeBDiagCursor,      //SizeBDiagCursor - diagonal resize (/).
        SizeFDiagCursor,      //SizeFDiagCursor - diagonal resize (\).
        SizeAllCursor,        //SizeAllCursor - all directions resize.
        BlankCursor,          //BlankCursor - blank/invisible cursor.
        SplitVCursor,         //SplitVCursor - vertical splitting.
        SplitHCursor,         //SplitHCursor - horziontal splitting.
        PointingHandCursor,   //PointingHandCursor - a pointing hand.
        ForbiddenCursor,      //ForbiddenCursor - a slashed circle.
        WhatsThisCursor,      //WhatsThisCursor - an arrow with a ?.
        OpenHandCursor,       //Qt OpenHandCursor
        ClosedHandCursor,       //Qt ClosedHandCursor
        CadCursor,            //CadCursor - a bigger cross.
        DelCursor,            //DelCursor - cursor for choosing entities
        SelectCursor,         //SelectCursor - for selecting single entities
        MagnifierCursor,      //MagnifierCursor - a magnifying glass.
        MovingHandCursor      //Moving hand - a little flat hand.
    };

    // Paper formats.
    enum PaperFormat
    {
        Custom,
        Letter,
        Legal,
        Executive,
        A0,
        A1,
        A2,
        A3,
        A4,
        A5,
        A6,
        A7,
        A8,
        A9,
        B0,
        B1,
        B2,
        B3,
        B4,
        B5,
        B6,
        B7,
        B8,
        B9,
        B10,
        C5E,
        Comm10E,
        DLE,
        Folio,
        Ledger,
        Tabloid,
        Arch_A,
        Arch_B,
        Arch_C,
        Arch_D,
        Arch_E,
        Arch_E1,
        Arch_E2,
        Arch_E3,

        NPageSize
    };

    // Items that can be put on a overlay, the items are rendered in this order.
    enum OverlayGraphics
    {
        ActionPreviewEntity, // Action Entities
        Snapper // Snapper
    };

    // Different re-draw methods to speed up rendering of the screen
    enum RedrawMethod
    {
        RedrawNone = 0,
        RedrawGrid = 1,
        RedrawOverlay = 2,
        RedrawDrawing = 4,
        RedrawAll = 0xffff
    };

    enum Language
    {
        English,    /*English*/
        Chinese     /*Chinese*/
    };

    enum SystemType
    {
        D_And_Asystem,    /*Common*/
        IESsystem        /*IES*/
    };
    /**
     * Text drawing direction.
     */
    enum TextDrawingDirection {
        LeftToRight,     /**< Left to right */
        TopToBottom,     /**< Top to bottom */
        ByStyle          /**< Inherited from associated text style */
    };

    /**
     * Line spacing style for texts.
     */
    enum TextLineSpacingStyle {
        AtLeast,        /**< Taller characters will override */
        Exact           /**< Taller characters will not override */
    };
};

#endif
