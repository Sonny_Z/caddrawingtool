/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_POLYLINE_H
#define FAS_POLYLINE_H

#include "fas_entitycontainer.h"

// Holds the data that defines a polyline.
struct FAS_PolylineData : public FAS_Flags
{
    FAS_PolylineData();
    FAS_PolylineData(const FAS_Vector& startpoint,
                     const FAS_Vector& endpoint,
                     bool closed);

    FAS_Vector startpoint;
    FAS_Vector endpoint;
    FAS_Vector bkstartpoint;
    FAS_Vector bkendpoint;
};

std::ostream& operator << (std::ostream& os, const FAS_PolylineData& pd);

class FAS_Polyline : public FAS_EntityContainer
{
public:
    FAS_Polyline(FAS_EntityContainer* parent=nullptr);
    FAS_Polyline(FAS_EntityContainer* parent,
                 const FAS_PolylineData& d);
    FAS_Entity* clone() const override;
    FAS2::EntityType rtti() const  override
    {
        return FAS2::EntityPolyline;
    }
    /** @return Copy of data that defines the polyline. */
    FAS_PolylineData getData() const
    {
        return data;
    }
    void setStartpoint(FAS_Vector const& v);
    FAS_Vector getStartpoint() const override;
    void setEndpoint(FAS_Vector const& v);
    FAS_Vector getEndpoint() const override;
    double getClosingBulge() const;
    void updateEndpoints();
    bool isClosed() const;
    void setClosed(bool cl);
    void setClosed(bool cl, double bulge);// rewrite this:
    FAS_VectorSolutions getRefPoints() const override;
    FAS_Vector getMiddlePoint(void)const override
    {
        return FAS_Vector(false);
    }
    FAS_Vector getNearestRef(const FAS_Vector& coord, double* dist = nullptr) const override;
    FAS_Vector getNearestSelectedRef( const FAS_Vector& coord, double* dist = nullptr) const override;
    FAS_Entity* addVertex(const FAS_Vector& v, double bulge=0.0, bool prepend=false);

    void appendVertexs(const std::vector< std::pair<FAS_Vector, double> >& vl);
    void setNextBulge(double bulge)
    {
        nextBulge = bulge;
    }
    void addEntity(FAS_Entity* entity) override;
    void removeLastVertex();
    void endPolyline();
    bool offset(const FAS_Vector& coord, const double& distance) override;
    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void stretch(const FAS_Vector& firstCorner,
                 const FAS_Vector& secondCorner,
                 const FAS_Vector& offset) override;
    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;
    void revertDirection() override;

    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
    void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;

    friend std::ostream& operator << (std::ostream& os, const FAS_Polyline& l);
protected:
    FAS_Entity* createVertex(const FAS_Vector& v,
                             double bulge=0.0, bool prepend=false);
protected:
    FAS_PolylineData data;
    FAS_Entity* closingEntity;
    double nextBulge;
};

#endif
