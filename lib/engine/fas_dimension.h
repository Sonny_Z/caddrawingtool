/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_DIMENSION_H
#define FAS_DIMENSION_H

#include "fas_entitycontainer.h"
#include "fas_mtext.h"

// Holds the data that is common to all dimension entities.
struct FAS_DimensionData : public FAS_Flags
{
    FAS_DimensionData();
    FAS_DimensionData(const FAS_Vector& definitionPoint,
                      const FAS_Vector& middleOfText,
                      FAS_MTextData::VAlign valign,
                      FAS_MTextData::HAlign halign,
                      FAS_MTextData::MTextLineSpacingStyle lineSpacingStyle,
                      double lineSpacingFactor,
                      QString text,
                      QString style,
                      double angle);

    //  Definition point
    FAS_Vector definitionPoint;
    //  Middle point of dimension text
    FAS_Vector middleOfText;
    //  Vertical alignment
    FAS_MTextData::VAlign valign;
    //  Horizontal alignment
    FAS_MTextData::HAlign halign;
    //  Line spacing style
    FAS_MTextData::MTextLineSpacingStyle lineSpacingStyle;
    //  Line spacing factor
    double lineSpacingFactor;
    // Text string entered explicitly by user or null
    QString text;
    //  Dimension style name
    QString style;
    //  Rotation angle of dimension text away from default orientation
    double angle;
};

std::ostream& operator << (std::ostream& os,
                           const FAS_DimensionData& dd);

class FAS_Dimension : public FAS_EntityContainer
{
public:
    FAS_Dimension(FAS_EntityContainer* parent,
                  const FAS_DimensionData& d);

    FAS_Vector getNearestRef( const FAS_Vector& coord, double* dist = nullptr) const override;
    FAS_Vector getNearestSelectedRef( const FAS_Vector& coord, double* dist = nullptr) const override;

    FAS_DimensionData getData() const
    {
        return data;
    }

    QString getLabel(bool resolve=true);
    void setLabel(const QString& l);

     /* Needs to be implemented by the dimension class to return the
     *  measurement of the dimension (e.g. 10.5 or 15'14").
     */
    virtual QString getMeasuredLabel() = 0;


     /* Must be overwritten by implementing dimension entity class
     *  to update the subentities which make up the dimension entity.
     */
    void update() override
    {
        updateDim();
    }

    virtual void updateDim(bool autoText=false) = 0;

    void updateCreateDimensionLine(const FAS_Vector& p1, const FAS_Vector& p2,
                                   bool arrow1=true, bool arrow2=true, bool autoText=false);

    FAS_Vector getDefinitionPoint()
    {
        return data.definitionPoint;
    }

    FAS_Vector getMiddleOfText()
    {
        return data.middleOfText;
    }

    FAS_MTextData::VAlign getVAlign()
    {
        return data.valign;
    }

    FAS_MTextData::HAlign getHAlign()
    {
        return data.halign;
    }

    FAS_MTextData::MTextLineSpacingStyle getLineSpacingStyle()
    {
        return data.lineSpacingStyle;
    }

    double getLineSpacingFactor()
    {
        return data.lineSpacingFactor;
    }

    QString getText()
    {
        return data.text;
    }

    QString getStyle()
    {
        return data.style;
    }

    double getAngle()
    {
        return data.angle;
    }

    double getGeneralFactor();
    double getGeneralScale();
    double getArrowSize();
    double getTickSize();
    double getExtensionLineExtension();
    double getExtensionLineOffset();
    double getDimensionLineGap();
    double getTextHeight();
    bool getAlignText();
    bool getFixedLengthOn();
    double getFixedLength();
    FAS2::LineWidth getExtensionLineWidth();
    FAS2::LineWidth getDimensionLineWidth();
    FAS_Color getDimensionLineColor();
    FAS_Color getExtensionLineColor();
    FAS_Color getTextColor();
    QString getTextStyle();

    double getGraphicVariable(const QString& key, double defMM, int code);
    static QString stripZerosAngle(QString angle, int zeros=0);
    static QString stripZerosLinear(QString linear, int zeros=1);

    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;

protected:
    // Data common to all dimension entities.
    FAS_DimensionData data;
};

#endif
