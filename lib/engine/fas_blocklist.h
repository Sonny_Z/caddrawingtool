/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_BLOCKLIST_H
#define FAS_BLOCKLIST_H


#include <QList>
#include <QString>
#include <QHash>

#include "fas_block.h"

class FAS_BlockList {
public:
    FAS_BlockList(){}
    FAS_BlockList(bool owner=false);
    virtual ~FAS_BlockList() {}

    void clear();
    // return Number of blocks available.
    int count() const;

    // return Block at given position or nullptr if i is out of range.
    FAS_Block* at(int i);
    FAS_Block* at(int i) const;
    QList<FAS_Block*>::iterator begin();
    QList<FAS_Block*>::iterator end();
    QList<FAS_Block*>::const_iterator begin()const;
    QList<FAS_Block*>::const_iterator end()const;
    QHash<QString,FAS_Block*> getClonedHashBlocks();


    virtual bool add(FAS_Block* block, bool notify=true);
    virtual bool addClone(FAS_Block* block);
    virtual void remove(FAS_Block* block);
    FAS_Block* find(const QString& name);
    FAS_Block* first();
    FAS_Block* last();
    FAS_Block* getBlockInHash(QString Name);
    void toggle(const QString& name);
    void toggle(FAS_Block* block);
    void freezeAll(bool freeze);

    // Sets the layer lists modified status to 'm'.
    void setModified(bool m);

    // retval true The layer list has been modified.
    bool isModified() const;

    friend std::ostream& operator << (std::ostream& os, FAS_BlockList& b);

private:
    // Is the list owning the blocks?
    bool owner;
    // Blocks in the graphic
    QList<FAS_Block*> blocks;
    QHash<QString,FAS_Block*> blocksHash;
    // Flag set if the layer list was modified and not yet saved.
    bool modified;
};

#endif
