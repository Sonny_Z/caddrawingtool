/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_DIMALIGNED_H
#define FAS_DIMALIGNED_H

#include "fas_dimension.h"

// Holds the data that defines an aligned dimension entity.
struct FAS_DimAlignedData {
    FAS_DimAlignedData();
    FAS_DimAlignedData(const FAS_Vector& extensionPoint1,
                       const FAS_Vector& extensionPoint2);

    // Definition point. Startpoint of the first extension line.
    FAS_Vector extensionPoint1;
    // Definition point. Startpoint of the second extension line.
    FAS_Vector extensionPoint2;
};

std::ostream& operator << (std::ostream& os, const FAS_DimAlignedData& dd);

// Class for aligned dimension entities.
class FAS_DimAligned : public FAS_Dimension {
public:
    FAS_DimAligned(FAS_EntityContainer* parent,
                   const FAS_DimensionData& d,
                   const FAS_DimAlignedData& ed);

    FAS_Entity* clone() const override;

    // return FAS2::EntityDimAligned */
    FAS2::EntityType rtti() const override{
        return FAS2::EntityDimAligned;
    }

    //return Copy of data that defines the aligned dimension.
    FAS_DimAlignedData const& getEData() const;

    FAS_VectorSolutions getRefPoints() const override;

    QString getMeasuredLabel() override;

    void updateDim(bool autoText=false) override;

    FAS_Vector const& getExtensionPoint1() const;

    FAS_Vector const& getExtensionPoint2() const;

    // Recalculate the original Dimension Point to remove Dim oblique angle.
    void updateDimPoint();

    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    bool hasEndpointsWithinWindow(const FAS_Vector& v1, const FAS_Vector& v2) override;
    void stretch(const FAS_Vector& firstCorner,
                 const FAS_Vector& secondCorner,
                 const FAS_Vector& offset) override;
    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;

    friend std::ostream& operator << (std::ostream& os,
                                      const FAS_DimAligned& d);

protected:
    // Extended data.
    FAS_DimAlignedData edata;
};

#endif
