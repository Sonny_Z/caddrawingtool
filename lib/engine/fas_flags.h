/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_FLAGS_H
#define FAS_FLAGS_H

//Base class for objects which have flags.
struct FAS_Flags {
    FAS_Flags(unsigned f = 0);

    virtual ~FAS_Flags(){}
    unsigned getFlags() const;
    void resetFlags();
    void setFlags(unsigned f);
    void setFlag(unsigned f);
    void delFlag(unsigned f);
    void toggleFlag(unsigned f);
    bool getFlag(unsigned f) const;

private:
    unsigned flags = 0;
};

#endif
