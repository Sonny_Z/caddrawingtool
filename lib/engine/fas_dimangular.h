/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_DIMANGULAR_H
#define FAS_DIMANGULAR_H

#include "fas_dimension.h"

// Holds the data that defines a angular dimension entity.
struct FAS_DimAngularData {
    FAS_DimAngularData();
    FAS_DimAngularData(const FAS_Vector& definitionPoint1,
                       const FAS_Vector& definitionPoint2,
                       const FAS_Vector& definitionPoint3,
                       const FAS_Vector& definitionPoint4);

    /** Definition point 1. */
    FAS_Vector definitionPoint1;
    /** Definition point 2. */
    FAS_Vector definitionPoint2;
    /** Definition point 3. */
    FAS_Vector definitionPoint3;
    /** Definition point 4. */
    FAS_Vector definitionPoint4;
};

std::ostream& operator << (std::ostream& os,
                           const FAS_DimAngularData& dd);

class FAS_DimAngular : public FAS_Dimension {
public:
    FAS_DimAngular(FAS_EntityContainer* parent,
                   const FAS_DimensionData& d,
                   const FAS_DimAngularData& ed);

    FAS_Entity* clone() const override;

    // return FAS2::EntityDimAngular.
    FAS2::EntityType rtti() const override{
        return FAS2::EntityDimAngular;
    }

    //return Copy of data that defines the angular dimension.
    FAS_DimAngularData getEData() const {
        return edata;
    }

    QString getMeasuredLabel() override;
    double getAngle();
    FAS_Vector getCenter() const override;
    bool getAngles(double& ang1, double& ang2, bool& reversed,
                   FAS_Vector& p1, FAS_Vector& p2);

    void updateDim(bool autoText=false) override;

    FAS_Vector getDefinitionPoint1() {
        return edata.definitionPoint1;
    }
    FAS_Vector getDefinitionPoint2() {
        return edata.definitionPoint2;
    }
    FAS_Vector getDefinitionPoint3() {
        return edata.definitionPoint3;
    }
    FAS_Vector getDefinitionPoint4() {
        return edata.definitionPoint4;
    }
    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;

    friend std::ostream& operator << (std::ostream& os,
                                      const FAS_DimAngular& d);

protected:
    // Extended data.
    FAS_DimAngularData edata;
};

#endif
