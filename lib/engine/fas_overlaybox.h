/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/


#ifndef FAS_OVERLAYBOX_H
#define FAS_OVERLAYBOX_H

#include "fas_atomicentity.h"

//Holds the data that defines a line.
class FAS_OverlayBoxData
{
public:
    FAS_OverlayBoxData() {}
    FAS_OverlayBoxData(const FAS_Vector& corner1, const FAS_Vector& corner2)
        : corner1(corner1), corner2(corner2) {}

    friend class FAS_OverlayBox;

    friend std::ostream& operator << (std::ostream& os, const FAS_OverlayBoxData& ld);

public:
    FAS_Vector corner1;
    FAS_Vector corner2;
};

class FAS_OverlayBox : public FAS_AtomicEntity
{
public:
    FAS_OverlayBox(FAS_EntityContainer* parent, const FAS_OverlayBoxData& d);
    FAS_Entity* clone() const override;

    FAS2::EntityType rtti() const override
    {
        return FAS2::EntityOverlayBox;
    }
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;

    // return Start point of the entity
    FAS_Vector getCorner1() const
    {
        return data.corner1;
    }
    // return End point of the entity
    FAS_Vector getCorner2() const
    {
        return data.corner2;
    }
    // return Copy of data that defines the line.
    FAS_OverlayBoxData getData() const
    {
        return data;
    }

    /** We should make a seperate drawing meganism for overlays and not use entities */
    void move(const FAS_Vector& /*offset*/)override{}
    void rotate(const FAS_Vector& /*center*/, const double& /*angle*/)override{}
    void rotate(const FAS_Vector& /*center*/, const FAS_Vector& /*angleVector*/)override{}
    void scale(const FAS_Vector& /*center*/, const FAS_Vector& /*factor*/)override{}
    void mirror(const FAS_Vector& /*axisPoint1*/, const FAS_Vector& /*axisPoint2*/)override{}
    void calculateBorders() override{}
    FAS_Vector getNearestEndpoint(const FAS_Vector&, double*)const override{return {};}
    FAS_Vector getNearestPointOnEntity(const FAS_Vector&, bool, double*, FAS_Entity**)const override{return {};}
    FAS_Vector getNearestCenter(const FAS_Vector&, double*)const override{return {};}
    FAS_Vector getNearestMiddle(const FAS_Vector&, double*,int)const override{return {};}
    FAS_Vector getNearestDist(double, const FAS_Vector&, double*)const override{return {};}
    double getDistanceToPoint(const FAS_Vector&, FAS_Entity**, FAS2::ResolveLevel, double)const override{return -1;}//is -1 right here

protected:
    FAS_OverlayBoxData data;
}
;

#endif
