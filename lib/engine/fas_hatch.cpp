/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_hatch.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>
#include <memory>
#include <QBrush>
#include <QPainterPath>
#include <QString>

#include "fas_arc.h"
#include "fas_circle.h"
#include "fas_ellipse.h"
#include "fas_graphicview.h"
#include "fas_information.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_painter.h"
#include "fas_patternlist.h"

FAS_HatchData::FAS_HatchData(bool _solid,
                             double _scale,
                             double _angle,
                             const QString& _pattern):
    solid(_solid)
  ,scale(_scale)
  ,angle(_angle)
  ,pattern(_pattern)
{
}

std::ostream& operator << (std::ostream& os, const FAS_HatchData& td) {
    os << "(" << td.pattern.toLatin1().data() << ")";
    return os;
}

FAS_Hatch::FAS_Hatch(FAS_EntityContainer* parent,
                     const FAS_HatchData& d)
    : FAS_EntityContainer(parent), data(d)
{
    hatch = nullptr;
    updateRunning = false;
    needOptimization = true;
    updateError = HATCH_UNDEFINED;
}

/**
 * Validates the hatch.
 */
bool FAS_Hatch::validate()
{
    bool ret = true;
    // loops:
    for(auto l: entities)
    {
        if (l->rtti()==FAS2::EntityContainer)
        {
            FAS_EntityContainer* loop = (FAS_EntityContainer*)l;
            ret = loop->optimizeContours() && ret;
        }
    }
    return ret;
}

FAS_Entity* FAS_Hatch::clone() const
{
    FAS_Hatch* t = new FAS_Hatch(*this);
    t->setOwner(isOwner());
    t->initId();
    t->detach();
    t->hatch = nullptr;
    return t;
}

//return Number of loops.
int FAS_Hatch::countLoops() const
{
    if (data.solid)
    {
        return count();
    }
    else
    {
        return count() - 1;
    }
}

bool FAS_Hatch::isContainer() const
{
    return !isSolid();
}

/**
 * Recalculates the borders of this hatch.
 */
void FAS_Hatch::calculateBorders()
{
    activateContour(true);
    FAS_EntityContainer::calculateBorders();
    activateContour(false);
}

/**
 * Updates the Hatch. Called when the
 * hatch or it's data, position, alignment, .. changes.
 */
void FAS_Hatch::update()
{
    updateError = HATCH_OK;
    if (updateRunning)
    {
        return;
    }

    if (updateEnabled==false)
    {
        return;
    }

    if (data.solid==true)
    {
        calculateBorders();
        return;
    }

    updateRunning = true;

    // delete old hatch:
    if (hatch)
    {
        removeEntity(hatch);
        hatch = nullptr;
    }

    if (isUndone())
    {
        updateRunning = false;
        return;
    }

    if (!validate())
    {
        updateRunning = false;
        updateError = HATCH_INVALID_CONTOUR;
        return;
    }

    // search pattern:
    FAS_Pattern* pat = FAS_PATTERNLIST->requestPattern(data.pattern);
    if (!pat) {
        updateRunning = false;
        updateError = HATCH_PATTERN_NOT_FOUND;
        return;
    }
    pat = (FAS_Pattern*)pat->clone();

    // scale pattern
    pat->scale(FAS_Vector(0.0,0.0), FAS_Vector(data.scale, data.scale));
    pat->calculateBorders();
    forcedCalculateBorders();

    // find out how many pattern-instances we need in x/y:
    int px1, py1, px2, py2;
    double f;
    FAS_Hatch* copy = (FAS_Hatch*)this->clone();
    copy->rotate(FAS_Vector(0.0,0.0), -data.angle);
    copy->forcedCalculateBorders();

    // create a pattern over the whole contour.
    FAS_Vector pSize = pat->getSize();
    FAS_Vector rot_center=pat->getMin();
    FAS_Vector cSize = getSize();

    if (cSize.x<1.0e-6 || cSize.y<1.0e-6 ||
            pSize.x<1.0e-6 || pSize.y<1.0e-6 ||
            cSize.x>FAS_MAXDOUBLE-1 || cSize.y>FAS_MAXDOUBLE-1 ||
            pSize.x>FAS_MAXDOUBLE-1 || pSize.y>FAS_MAXDOUBLE-1) {
        delete pat;
        delete copy;
        updateRunning = false;
        updateError = HATCH_TOO_SMALL;
        return;
    }

    // avoid huge memory consumption:
    else if ( cSize.x* cSize.y/(pSize.x*pSize.y)>1e4)
    {
        delete pat;
        delete copy;
        updateError = HATCH_AREA_TOO_BIG;
        return;
    }

    f = copy->getMin().x/pSize.x;
    px1 = (int)floor(f);
    f = copy->getMin().y/pSize.y;
    py1 = (int)floor(f);
    f = copy->getMax().x/pSize.x;
    px2 = (int)ceil(f);
    f = copy->getMax().y/pSize.y;
    py2 = (int)ceil(f);
    FAS_Vector dvx=FAS_Vector(data.angle)*pSize.x;
    FAS_Vector dvy=FAS_Vector(data.angle+M_PI*0.5)*pSize.y;
    pat->rotate(rot_center, data.angle);
    pat->move(-rot_center);


    FAS_EntityContainer tmp;   // container for untrimmed lines
    for (int px=px1; px<px2; px++)
    {
        for (int py=py1; py<py2; py++)
        {
            for(auto e: *pat)
            {
                FAS_Entity* te=e->clone();
                te->move(dvx*px + dvy*py);
                tmp.addEntity(te);
            }
        }
    }

    delete pat;
    pat = nullptr;
    delete copy;
    copy = nullptr;
    // cut pattern to contour shape:
    FAS_EntityContainer tmp2;   // container for small cut lines
    FAS_Line* line = nullptr;
    FAS_Arc* arc = nullptr;
    FAS_Circle* circle = nullptr;
    FAS_Ellipse* ellipse = nullptr;
    for(auto e: tmp)
    {
        line = nullptr;
        arc = nullptr;
        circle = nullptr;
        ellipse = nullptr;
        FAS_Vector startPoint;
        FAS_Vector endPoint;
        FAS_Vector center = FAS_Vector(false);
        bool reversed=false;
        switch(e->rtti())
        {
        case FAS2::EntityLine:
            line=static_cast<FAS_Line*>(e);
            startPoint = line->getStartpoint();
            endPoint = line->getEndpoint();
            break;
        case FAS2::EntityArc:
            arc=static_cast<FAS_Arc*>(e);
            startPoint = arc->getStartpoint();
            endPoint = arc->getEndpoint();
            center = arc->getCenter();
            reversed = arc->isReversed();
            break;
        case FAS2::EntityCircle:
            circle=static_cast<FAS_Circle*>(e);
            startPoint = circle->getCenter()
                    + FAS_Vector(circle->getRadius(), 0.0);
            endPoint = startPoint;
            center = circle->getCenter();
            break;
        case FAS2::EntityEllipse:
            ellipse = static_cast<FAS_Ellipse*>(e);
            startPoint = ellipse->getStartpoint();
            endPoint = ellipse->getEndpoint();
            center = ellipse->getCenter();
            reversed = ellipse->isReversed();
            break;
        default:
            continue;
        }

        // getting all intersections of this pattern line with the contour:
        QList<FAS_Vector> is;

        for(auto loop: entities)
        {
            if (loop->isContainer())
            {
                for(auto p: * static_cast<FAS_EntityContainer*>(loop))
                {
                    FAS_VectorSolutions sol =
                            FAS_Information::getIntersection(e, p, true);
                    for (const FAS_Vector& vp: sol){
                        if (vp.valid) {
                            is.append(vp);
                        }
                    }
                }
            }
        }


        QList<FAS_Vector> is2;//to be filled with sorted intersections
        is2.append(startPoint);

        // sort the intersection points into is2 (only if there are intersections):
        if(is.size() == 1)
        {//only one intersection
            is2.append(is.first());
        }
        else if(is.size() > 1)
        {
            FAS_Vector sp = startPoint;
            double sa = center.angleTo(sp);
            if(ellipse ) sa=ellipse->getEllipseAngle(sp);
            bool done;
            double minDist;
            double dist = 0.0;
            FAS_Vector av;
            FAS_Vector v;
            FAS_Vector last{};
            do {
                done = true;
                minDist = FAS_MAXDOUBLE;
                av.valid = false;
                for (int i = 0; i < is.size(); ++i)
                {
                    v = is.at(i);
                    double a;
                    switch(e->rtti())
                    {
                    case FAS2::EntityLine:
                        dist = sp.distanceTo(v);
                        break;
                    case FAS2::EntityArc:
                    case FAS2::EntityCircle:
                        a = center.angleTo(v);
                        dist = reversed?
                                    fmod(sa - a + 2.*M_PI,2.*M_PI):
                                    fmod(a - sa + 2.*M_PI,2.*M_PI);
                        break;
                    case FAS2::EntityEllipse:
                        a = ellipse->getEllipseAngle(v);
                        dist = reversed?
                                    fmod(sa - a + 2.*M_PI,2.*M_PI):
                                    fmod(a - sa + 2.*M_PI,2.*M_PI);
                        break;
                    default:
                        break;

                    }

                    if (dist<minDist)
                    {
                        minDist = dist;
                        done = false;
                        av = v;
                    }
                }

                // copy to sorted list, removing double points
                if (!done && av)
                {
                    if (last.valid==false || last.distanceTo(av)>FAS_TOLERANCE)
                    {
                        is2.append(av);
                        last = av;
                    }
                    is.removeOne(av);
                    av.valid = false;
                }
            } while(!done);
        }

        is2.append(endPoint);

        // add small cut lines / arcs to tmp2:
        for (int i = 1; i < is2.size(); ++i)
        {
            auto v1 = is2.at(i-1);
            auto v2 = is2.at(i);


            if (line)
            {
                tmp2.addEntity(new FAS_Line{&tmp2, v1, v2});
            }
            else if (arc || circle)
            {
                if(fabs(center.angleTo(v2)-center.angleTo(v1)) > FAS_TOLERANCE_ANGLE)
                {
                    //don't create an arc with a too small angle
                    tmp2.addEntity(new FAS_Arc(&tmp2,
                                               FAS_ArcData(center,
                                                           center.distanceTo(v1),
                                                           center.angleTo(v1),
                                                           center.angleTo(v2),
                                                           reversed)));
                }

            }
        }

    }

    // the hatch pattern entities:
    hatch = new FAS_EntityContainer(this);
    hatch->setPen(FAS_Pen(FAS2::FlagInvalid));
    hatch->setLayer(nullptr);
    hatch->setFlag(FAS2::FlagTemp);
    for(auto e: tmp2)
    {
        FAS_Vector middlePoint;
        FAS_Vector middlePoint2;
        if (e->rtti()==FAS2::EntityLine)
        {
            FAS_Line* line = static_cast<FAS_Line*>(e);
            middlePoint = line->getMiddlePoint();
            middlePoint2 = line->getNearestDist(line->getLength()/2.1,
                                                line->getStartpoint());
        }
        else if (e->rtti()==FAS2::EntityArc)
        {
            FAS_Arc* arc = static_cast<FAS_Arc*>(e);
            middlePoint = arc->getMiddlePoint();
            middlePoint2 = arc->getNearestDist(arc->getLength()/2.1,
                                               arc->getStartpoint());
        }
        else
        {
            middlePoint = FAS_Vector{false};
            middlePoint2 = FAS_Vector{false};
        }

        if (middlePoint.valid)
        {
            bool onContour=false;
            if (FAS_Information::isPointInsideContour( middlePoint, this, &onContour) ||
                    FAS_Information::isPointInsideContour(middlePoint2, this))
            {
                FAS_Entity* te = e->clone();
                te->setPen(FAS2::FlagInvalid);
                te->setLayer(nullptr);
                te->reparent(hatch);
                hatch->addEntity(te);
            }
        }
    }

    addEntity(hatch);
    forcedCalculateBorders();
    activateContour(false);
    updateRunning = false;
}


// Activates of deactivates the hatch boundary.
void FAS_Hatch::activateContour(bool on)
{
    for(auto e: entities)
    {
        if (!e->isUndone())
        {
            if (!e->getFlag(FAS2::FlagTemp))
            {
                e->setVisible(on);
            }
        }
    }
}

//#include<QDebug>
/**
 * Overrides drawing of subentities. This is only ever called for solid fills.
 */
void FAS_Hatch::draw(FAS_Painter* painter, FAS_GraphicView* view, double& /*patternOffset*/) {
    COMMONDEF->drawEntityCount++;
    if (!data.solid) {
        foreach (auto se, entities){

            view->drawEntity(painter,se);
        }
        return;
    }

    //area of solid fill. Use polygon approximation, except trivial cases
    QPainterPath path;
    QList<QPolygon> paClosed;
    QPolygon pa;

    // loops:
    if (needOptimization==true) {
        for(auto l: entities){

            if (l->rtti()==FAS2::EntityContainer) {
                FAS_EntityContainer* loop = (FAS_EntityContainer*)l;

                loop->optimizeContours();
            }
        }
        needOptimization = false;
    }

    // loops:
    for(auto l: entities){
        l->setLayer(getLayer());

        if (l->rtti()==FAS2::EntityContainer) {
            FAS_EntityContainer* loop = (FAS_EntityContainer*)l;

            // edges:
            for(auto e: *loop){

                e->setLayer(getLayer());
                switch (e->rtti()) {
                case FAS2::EntityLine: {
                    QPoint pt1(FAS_Math::round(view->toGuiX(e->getStartpoint().x)),
                               FAS_Math::round(view->toGuiY(e->getStartpoint().y)));
                    QPoint pt2(FAS_Math::round(view->toGuiX(e->getEndpoint().x)),
                               FAS_Math::round(view->toGuiY(e->getEndpoint().y)));

                    if(pa.size() && (pa.last()-pt1).manhattanLength()>=1)
                        pa<<pt1;
                    pa<<pt2;
                }
                    break;

                case FAS2::EntityArc: {
                    QPolygon pa2;
                    FAS_Arc* arc=static_cast<FAS_Arc*>(e);

                    painter->createArc(pa2, view->toGui(arc->getCenter()),
                                       view->toGuiDX(arc->getRadius())
                                       ,arc->getAngle1(),arc->getAngle2(),arc->isReversed());
                    if(pa.size() &&pa2.size()&&(pa.last()-pa2.first()).manhattanLength()<1)
                        pa2.remove(0,1);
                    pa<<pa2;

                }
                    break;

                case FAS2::EntityCircle: {
                    FAS_Circle* circle = static_cast<FAS_Circle*>(e);
                    FAS_Vector c=view->toGui(circle->getCenter());
                    double r=view->toGuiDX(circle->getRadius());
#if QT_VERSION >= 0x040400
                    path.addEllipse(QPoint(c.x,c.y),r,r);
#else
                    path.addEllipse(c.x - r, c.y + r, 2.*r, 2.*r);
#endif
                }
                    break;
                case FAS2::EntityEllipse:
                    if(static_cast<FAS_Ellipse*>(e)->isArc()) {
                        QPolygon pa2;
                        auto ellipse=static_cast<FAS_Ellipse*>(e);

                        painter->createEllipse(pa2,
                                               view->toGui(ellipse->getCenter()),
                                               view->toGuiDX(ellipse->getMajorRadius()),
                                               view->toGuiDX(ellipse->getMinorRadius()),
                                               ellipse->getAngle()
                                               ,ellipse->getAngle1(),ellipse->getAngle2(),ellipse->isReversed()
                                               );
                        if(pa.size() && pa2.size()&&(pa.last()-pa2.first()).manhattanLength()<1)
                            pa2.remove(0,1);
                        pa<<pa2;
                    }else{
                        QPolygon pa2;
                        auto ellipse=static_cast<FAS_Ellipse*>(e);
                        painter->createEllipse(pa2,
                                               view->toGui(ellipse->getCenter()),
                                               view->toGuiDX(ellipse->getMajorRadius()),
                                               view->toGuiDX(ellipse->getMinorRadius()),
                                               ellipse->getAngle(),
                                               ellipse->getAngle1(), ellipse->getAngle2(),
                                               ellipse->isReversed()
                                               );
                        path.addPolygon(pa2);
                    }
                    break;
                default:
                    break;
                }
            }
            if( pa.size()>2) {
                pa << pa.first();
                paClosed << pa;
                pa.clear();
            }
        }
    }
    if(pa.size()>2){
        pa << pa.first();
        paClosed<<pa;
    }

    for(auto& p:paClosed){
        path.addPolygon(p);
    }
    const QBrush brush(painter->brush());
    const FAS_Pen pen=painter->getPen();
    painter->setBrush(pen.getColor());
    painter->disablePen();
    painter->drawPath(path);
    painter->setBrush(brush);
    painter->setPen(pen);
}

//must be called after update()
double FAS_Hatch::getTotalArea() {

    double totalArea=0.;

    // loops:
    for(auto l: entities){

        if (l!=hatch && l->rtti()==FAS2::EntityContainer) {
            totalArea += l->areaLineIntegral();
        }
    }
    return totalArea;
}

double FAS_Hatch::getDistanceToPoint(
        const FAS_Vector& coord,
        FAS_Entity** entity,
        FAS2::ResolveLevel level,
        double solidDist) const {

    if (data.solid==true) {
        if (entity) {
            *entity = const_cast<FAS_Hatch*>(this);
        }

        bool onContour;
        if (FAS_Information::isPointInsideContour(
                    coord,
                    const_cast<FAS_Hatch*>(this), &onContour)) {

            // distance is the snap range:
            return solidDist;
        }

        return FAS_MAXDOUBLE;
    } else {
        return FAS_EntityContainer::getDistanceToPoint(coord, entity,
                                                       level, solidDist);
    }
}



void FAS_Hatch::move(const FAS_Vector& offset) {
    FAS_EntityContainer::move(offset);
    update();
}



void FAS_Hatch::rotate(const FAS_Vector& center, const double& angle) {
    FAS_EntityContainer::rotate(center, angle);
    data.angle = FAS_Math::correctAngle(data.angle+angle);
    update();
}


void FAS_Hatch::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    FAS_EntityContainer::rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angleVector.angle());
    update();
}

void FAS_Hatch::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    FAS_EntityContainer::scale(center, factor);
    data.scale *= factor.x;
    update();
}



void FAS_Hatch::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    FAS_EntityContainer::mirror(axisPoint1, axisPoint2);
    double ang = axisPoint1.angleTo(axisPoint2);
    data.angle = FAS_Math::correctAngle(data.angle + ang*2.0);
    update();
}


void FAS_Hatch::stretch(const FAS_Vector& firstCorner,
                        const FAS_Vector& secondCorner,
                        const FAS_Vector& offset) {

    FAS_EntityContainer::stretch(firstCorner, secondCorner, offset);
    update();
}
void FAS_Hatch::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& dataInsert)
{
    if (!(painter && view))
    {
        return;
    }

    FAS_Entity* e=firstEntity(FAS2::ResolveNone);
    if (e)
    {
        FAS_Pen p=this->getPen(true);
        e->setPen(p);
        double patternOffset(0.0);
        //view->drawEntity(painter, e, patternOffset,dataInsert);
         view->drawEntity(painter, e, patternOffset);

        e = nextEntity(FAS2::ResolveNone);
        while(e)
        {

            //view->drawEntity(painter, e, patternOffset,dataInsert);
            view->drawEntity(painter, e, patternOffset);
            e = nextEntity(FAS2::ResolveNone);
        }
    }
}
void FAS_Hatch::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){



    if(!data.bkBasicData.valid){
        data.bkBasicData=FAS_Vector(true);
        data.bkangle=data.angle;

    }else{
        data.angle=data.bkangle;
    }
    FAS_EntityContainer::resetBasicData(offset,center,angle,factor);
}

void FAS_Hatch::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){

    FAS_EntityContainer::moveEntityBoarder(offset,center,angle,factor);
    data.angle = FAS_Math::correctAngle(data.angle+angle);
    data.scale *= factor.x;
    this->update();

}
std::ostream& operator << (std::ostream& os, const FAS_Hatch& p) {
    os << " Hatch: " << p.getData() << "\n";
    return os;
}
