/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_BLOCK_H
#define FAS_BLOCK_H

#include "deviceinfo.h"
#include "fas_document.h"

// Holds the data that defines a block.
struct FAS_BlockData {
    FAS_BlockData() {}
    FAS_BlockData(const QString& name,
               const FAS_Vector& basePoint,
               bool frozen);
    bool isValid() const;
    // Block name. Acts as an id.
    QString name;
    // Base point of the Block.
    FAS_Vector basePoint;
    DeviceInfo* type = nullptr;
    //! Frozen flag
    bool frozen;
    // set flag
    bool set;
};

// blocks are documents and can therefore be handled by graphic views.
class FAS_Block : public FAS_Document
{

    friend class FAS_BlockList;

public:
    FAS_Block(FAS_EntityContainer* parent, const FAS_BlockData& d);
    ~FAS_Block();
    virtual FAS_Entity* clone() const;

    /** @return FAS2::EntityBlock */
    virtual FAS2::EntityType rtti() const
    {
        return FAS2::EntityBlock;
    }

    // set device type
    void setType(DeviceInfo* TYPE)
    {
        this->data.set = true;
        this->data.type = TYPE;
    }
    DeviceInfo* getType()
    {
        return this->data.type;
    }

    // flag that if the device type is set.
    bool is_set()
    {
        return this->data.set;
    }

    // return Name of this block (the name is an Id for this block).
    QString getName() const {
        return data.name;
    }

    // return base point of this block.
    FAS_Vector getBasePoint() const {
        return data.basePoint;
    }

    virtual FAS_LayerList* getLayerList();
    virtual FAS_BlockList* getBlockList();
    ///Does nothing.
    virtual void newDoc() {
        // do nothing
    }

    /**
     * Reimplementation from FAS_Document. Saves the parent graphic document.
     */
    virtual bool save(bool isAutoSave = false);

    /**
     * Reimplementation from FAS_Document. Does nothing.
     */
    virtual bool saveAs(const QString& filename, FAS2::FormatType type, bool force = false);

    // Reimplementation from FAS_Document. Does nothing.
    virtual bool open(const QString& , FAS2::FormatType)
    {
        // do nothing
        return false;
    }
    virtual bool loadTemplate(const QString& , FAS2::FormatType)
    {
        // do nothing
        return false;
    }

    friend std::ostream& operator << (std::ostream& os, const FAS_Block& b);

    // sets a new name for the block. Only called by blocklist to assure that block names stay unique.
    void setName(const QString& n)
    {
        data.name = n;
    }

    // true if this block is frozen (invisible), false if this block isn't frozen (visible)
    bool isFrozen() const
    {
        return data.frozen;
    }

    // Toggles the visibility of this block. Freezes the block if it's not frozen, thaws the block otherwise
    void toggle()
    {
        data.frozen = !data.frozen;
    }

    void freeze(bool freeze) {
        data.frozen = freeze;
    }

    virtual void setModified(bool m);

protected:
    // Block data
    FAS_BlockData data;
};


#endif
