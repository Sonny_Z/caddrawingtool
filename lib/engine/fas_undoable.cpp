/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_undoable.h"
//sort with alphabetical order
#include "fas_undocycle.h"

/*
 * Destructor. Makes sure that this undoable is removed from
 * its undo cycle before it is deleted.
 */
FAS_Undoable::~FAS_Undoable() {
    if (cycle)
        cycle->removeUndoable(this);
}

/*
 * Sets the undo cycle this entity is in. This is necessary to
 * make sure the entity can remove itself from the cycle before
 * being deleted.
 */
void FAS_Undoable::setUndoCycle(FAS_UndoCycle* cycle)
{
    this->cycle = cycle;
}

/*
 * The undoable thing gets activated if it was undone and
 * deactivated otherwise.
 */
void FAS_Undoable::changeUndoState()
{
    toggleFlag(FAS2::FlagUndone);
    undoStateChanged(isUndone());
}

//Undoes or redoes an undoable.
void FAS_Undoable::setUndoState(bool undone)
{
    if (undone)
    {
        setFlag(FAS2::FlagUndone);
    }
    else
    {
        delFlag(FAS2::FlagUndone);
    }
    undoStateChanged(isUndone());
}
// Is this entity in the Undo memory and not active?
bool FAS_Undoable::isUndone() const
{
    return getFlag(FAS2::FlagUndone);
}

