/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_image.h"
//sort with alphabetical order

#include <iostream>
#include <QImage>

#include "fas_graphicview.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_painterqt.h"

FAS_ImageData::FAS_ImageData(int _handle,
                             const FAS_Vector& _insertionPoint,
                             const FAS_Vector& _uVector,
                             const FAS_Vector& _vVector,
                             const FAS_Vector& _size,
                             const QString& _file,
                             int _brightness,
                             int _contrast,
                             int _fade):
    handle(_handle)
  , insertionPoint(_insertionPoint)
  , uVector(_uVector)
  , vVector(_vVector)
  , size(_size)
  , file(_file)
  , brightness(_brightness)
  , contrast(_contrast)
  , fade(_fade)
{
}

std::ostream& operator << (std::ostream& os, const FAS_ImageData& ld)
{
    os << "(" << ld.insertionPoint << ")";
    return os;
}

FAS_Image::FAS_Image(FAS_EntityContainer* parent,
                     const FAS_ImageData& d)
    :FAS_AtomicEntity(parent), data(d)
{
    update();
    calculateBorders();
}

FAS_Image::FAS_Image(const FAS_Image& _image):
    FAS_AtomicEntity(_image.getParent())
  ,data(_image.data)
  ,img(_image.img.get()?new QImage(*_image.img):nullptr)
{
}

FAS_Image& FAS_Image::operator = (const FAS_Image& _image)
{
    data=_image.data;
    if(_image.img.get())
    {
        img.reset(new QImage(*_image.img));
    }
    else
    {
        img.reset();
    }
    return *this;
}

FAS_Image::FAS_Image(FAS_Image&& _image):
    FAS_AtomicEntity(_image.getParent())
  ,data(std::move(_image.data))
  ,img(std::move(_image.img))
{
}

FAS_Image& FAS_Image::operator = (FAS_Image&& _image)
{
    data=_image.data;
    img = std::move(_image.img);
    return *this;
}


FAS_Entity* FAS_Image::clone() const {
    FAS_Image* i = new FAS_Image(*this);
    i->setHandle(getHandle());
    i->initId();
    i->update();
    return i;
}


void FAS_Image::updateData(FAS_Vector size, FAS_Vector Uv, FAS_Vector Vv)
{
    data.size = size;
    data.uVector = Uv;
    data.vVector = Vv;
    update();
    calculateBorders();
}


void FAS_Image::update()
{
    img.reset(new QImage(data.file));
    if (!img->isNull()) {
        data.size = FAS_Vector(img->width(), img->height());
    }
}

void FAS_Image::calculateBorders()
{
    FAS_VectorSolutions sol = getCorners();
    minV =  FAS_Vector::minimum(
                FAS_Vector::minimum(sol.get(0), sol.get(1)),
                FAS_Vector::minimum(sol.get(2), sol.get(3)));
    maxV =  FAS_Vector::maximum(
                FAS_Vector::maximum(sol.get(0), sol.get(1)),
                FAS_Vector::maximum(sol.get(2), sol.get(3)));
}

FAS_VectorSolutions FAS_Image::getCorners() const
{
    FAS_VectorSolutions sol(4);
    sol.set(0, data.insertionPoint);
    sol.set(1, data.insertionPoint + data.uVector*FAS_Math::round(data.size.x));
    sol.set(3, data.insertionPoint + data.vVector*FAS_Math::round(data.size.y));
    sol.set(2, sol.get(3) + data.uVector*FAS_Math::round(data.size.x));
    return sol;
}

// whether a given point is within image region
bool FAS_Image::containsPoint(const FAS_Vector& coord) const
{
    QPolygonF paf;
    FAS_VectorSolutions corners =getCorners();
    for(const FAS_Vector& vp: corners)
    {
        paf.push_back(QPointF(vp.x, vp.y));
    }
    paf.push_back(paf.at(0));
    return paf.containsPoint(QPointF(coord.x,coord.y),Qt::OddEvenFill);
}

FAS_Vector FAS_Image::getNearestEndpoint(const FAS_Vector& coord, double* dist) const
{
    FAS_VectorSolutions corners = getCorners();
    return corners.getClosest(coord, dist);
}

FAS_Vector FAS_Image::getNearestPointOnEntity(const FAS_Vector& coord,
                                              bool onEntity, double* dist, FAS_Entity** entity) const
{
    if (entity)
    {
        *entity = const_cast<FAS_Image*>(this);
    }

    FAS_VectorSolutions const& corners =getCorners();
    //allow selecting image by clicking within images
    if(containsPoint(coord))
    {
        //if coord is within image
        if(dist)
            *dist=0.;
        return coord;
    }
    FAS_VectorSolutions points;
    for (size_t i=0; i < corners.size(); ++i)
    {
        size_t const j = (i+1)%corners.size();
        FAS_Line const l{corners.at(i), corners.at(j)};
        FAS_Vector const vp = l.getNearestPointOnEntity(coord, onEntity);
        points.push_back(vp);
    }
    return points.getClosest(coord, dist);
}

FAS_Vector FAS_Image::getNearestCenter(const FAS_Vector& coord, double* dist) const
{

    FAS_VectorSolutions const& corners{getCorners()};
    FAS_VectorSolutions points;
    for (size_t i=0; i < corners.size(); ++i)
    {
        size_t const j = (i+1)%corners.size();
        points.push_back((corners.get(i) + corners.get(j))*0.5);
    }
    points.push_back((corners.get(0) + corners.get(2))*0.5);
    return points.getClosest(coord, dist);
}

FAS_Vector FAS_Image::getNearestMiddle(const FAS_Vector& coord,
                                       double* dist,
                                       const int /*middlePoints*/) const
{
    return getNearestCenter(coord, dist);
}

FAS_Vector FAS_Image::getNearestDist(double distance,
                                     const FAS_Vector& coord,
                                     double* dist) const
{
    FAS_VectorSolutions const& corners = getCorners();
    FAS_VectorSolutions points;
    for (size_t i = 0; i < corners.size(); ++i)
    {
        size_t const j = (i+1)%corners.size();
        FAS_Line const l{corners.get(i), corners.get(j)};
        FAS_Vector const& vp = l.getNearestDist(distance, coord, dist);
        points.push_back(vp);
    }
    return points.getClosest(coord, dist);
}

double FAS_Image::getDistanceToPoint(const FAS_Vector& coord, FAS_Entity** entity, FAS2::ResolveLevel /*level*/, double /*solidDist*/) const
{
    if (entity)
    {
        *entity = const_cast<FAS_Image*>(this);
    }
    FAS_VectorSolutions corners = getCorners();

    //allow selecting image by clicking within images
    if(containsPoint(coord))
    {
        //if coord is on image
        bool draftMode = false;
        if(!draftMode)
            return double(0.);
    }
    //continue to allow selecting by image edges
    double minDist = FAS_MAXDOUBLE;
    for (size_t i = 0; i < corners.size(); ++i)
    {
        size_t const j = (i+1)%corners.size();
        FAS_Line const l{corners.get(i), corners.get(j)};
        double const dist = l.getDistanceToPoint(coord, nullptr);
        minDist = std::min(minDist, dist);
    }
    return minDist;
}

void FAS_Image::move(const FAS_Vector& offset)
{
    data.insertionPoint.move(offset);
    moveBorders(offset);
}

void FAS_Image::rotate(const FAS_Vector& center, const double& angle)
{
    FAS_Vector angleVector(angle);
    data.insertionPoint.rotate(center, angleVector);
    data.uVector.rotate(angleVector);
    data.vVector.rotate(angleVector);
    calculateBorders();
}

void FAS_Image::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    data.insertionPoint.rotate(center, angleVector);
    data.uVector.rotate(angleVector);
    data.vVector.rotate(angleVector);
    calculateBorders();
}

void FAS_Image::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    data.insertionPoint.scale(center, factor);
    data.uVector.scale(factor);
    data.vVector.scale(factor);
    scaleBorders(center,factor);
}

void FAS_Image::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    data.insertionPoint.mirror(axisPoint1, axisPoint2);
    FAS_Vector vp0(0.,0.);
    FAS_Vector vp1( axisPoint2-axisPoint1 );
    data.uVector.mirror(vp0,vp1);
    data.vVector.mirror(vp0,vp1);
    calculateBorders();
}

void FAS_Image::draw(FAS_Painter* painter, FAS_GraphicView* view, double& /*patternOffset*/)
{

    if (!(painter && view) || !img.get() || img->isNull())
        return;
 COMMONDEF->drawEntityCount++;
    FAS_Vector scale{view->toGuiDX(data.uVector.magnitude()), view->toGuiDY(data.vVector.magnitude())};
    double angle = data.uVector.angle();
    painter->drawImg(*img, view->toGui(data.insertionPoint), angle, scale);

    if (isSelected())
    {
        FAS_VectorSolutions sol = getCorners();
        for (size_t i = 0; i < sol.size(); ++i)
        {
            size_t const j = (i+1)%sol.size();
            painter->drawLine(view->toGui(sol.get(i)), view->toGui(sol.get(j)));
        }
    }
}

//Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_Image& i) {
    os << " Image: " << i.getData() << "\n";
    return os;
}

