/****************************************************************************
**
** This file is part of the LibreCAD project, a 2D CAD program
**
** Copyright (C) 2010 R. van Twisk (librecad@rvt.dds.nl)
** Copyright (C) 2001-2003 RibbonSoft. All rights reserved.
**
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by 
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
** This copyright notice MUST APPEAR in all copies of the script!  
**
**********************************************************************/


#ifndef FAS_FONTLIST_H
#define FAS_FONTLIST_H


#include <QList>
class FAS_Font;

#define FAS_FONTLIST FAS_FontList::instance()

/**
 * The global list of fonts. This is implemented as a singleton.
 * Use FAS_FontList::instance() to get a pointer to the object.
 *
 * @author Andrew Mustun
 */
class FAS_FontList {
protected:
    FAS_FontList();

public:
    /**
     * @return Instance to the unique font list.
     */
    static FAS_FontList* instance() {
        if (uniqueInstance==NULL) {
            uniqueInstance = new FAS_FontList();
        }
        return uniqueInstance;
    }

    virtual ~FAS_FontList() {}

    void init();

    void clearFonts();
    int countFonts() {
        return fonts.count();
    }
    virtual void removeFont(FAS_Font* font);
    FAS_Font* requestFont(const QString& name);

    //! @return a const iterator for the font list.
    QListIterator<FAS_Font *> getIteretor(){
        return QListIterator<FAS_Font *>(fonts);
    }

    friend std::ostream& operator << (std::ostream& os, FAS_FontList& l);

protected:
    static FAS_FontList* uniqueInstance;

private:
    //! fonts in the graphic
    QList<FAS_Font *> fonts;
};

#endif
