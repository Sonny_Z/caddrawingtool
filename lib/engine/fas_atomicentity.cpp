/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_atomicentity.h"

FAS_AtomicEntity::FAS_AtomicEntity(FAS_EntityContainer* parent) : FAS_Entity(parent)
{}

bool FAS_AtomicEntity::isContainer() const {
    return false;
}

//return true because entities made from subclasses are atomic entities.
bool FAS_AtomicEntity::isAtomic() const {
    return true;
}

// return Always 1 for atomic entities.
unsigned int FAS_AtomicEntity::count() const{
    return 1;
}

// return Always 1 for atomic entities.
unsigned int FAS_AtomicEntity::countDeep() const{
    return 1;
}

//return the endpoint of the entity or an invalid vector if the entity has no endpoint.
FAS_Vector FAS_AtomicEntity::getEndpoint() const {
    return FAS_Vector(false);
}

// Implementation must return the startpoint of the entity or an invalid vector if the entity has no startpoint.
FAS_Vector FAS_AtomicEntity::getStartpoint() const {
    return FAS_Vector(false);
}

// Implementation must return the angle in which direction the entity starts.
double FAS_AtomicEntity::getDirection1() const {
    return 0.0;
}

// Implementation must return the angle in which direction the entity starts the opposite way.
double FAS_AtomicEntity::getDirection2() const {
    return 0.0;
}
FAS_Vector FAS_AtomicEntity::getCenter() const {
    return FAS_Vector(false);
}
double FAS_AtomicEntity::getRadius() const {
    return 0.;
}

// return The closest center point.
FAS_Vector FAS_AtomicEntity::getNearestCenter(const FAS_Vector& coord, double* dist)const
{
    return FAS_Vector(false);
}

void FAS_AtomicEntity::setStartpointSelected(bool select)
{
    if (select) {
        setFlag(FAS2::FlagSelected1);
    } else {
        delFlag(FAS2::FlagSelected1);
    }
}

void FAS_AtomicEntity::setEndpointSelected(bool select)
{
    if (select) {
        setFlag(FAS2::FlagSelected2);
    } else {
        delFlag(FAS2::FlagSelected2);
    }
}
bool FAS_AtomicEntity::isTangent(const FAS_CircleData& /* circleData */) const
{
    return false;
}

//return True if the entities startpoint is selected.
bool FAS_AtomicEntity::isStartpointSelected() const
{
    return getFlag(FAS2::FlagSelected1);
}

//return True if the entities endpoint is selected.
bool FAS_AtomicEntity::isEndpointSelected() const
{
    return getFlag(FAS2::FlagSelected2);
}

void FAS_AtomicEntity::revertDirection(){}

// create offset of the entity to the given direction and distance
bool FAS_AtomicEntity::offset(const FAS_Vector& /*position*/, const double& /*distance*/) {return false;}

// move the startpoint of the entity to the given position.
void FAS_AtomicEntity::moveStartpoint(const FAS_Vector& /*pos*/) {}

// move the endpoint of the entity to the given position.
void FAS_AtomicEntity::moveEndpoint(const FAS_Vector& /*pos*/) {}

// trim the startpoint of the entity to the given position.
void FAS_AtomicEntity::trimStartpoint(const FAS_Vector& pos)
{
    moveStartpoint(pos);
}

// trim the endpoint of the entity to the given position.
void FAS_AtomicEntity::trimEndpoint(const FAS_Vector& pos)
{
    moveEndpoint(pos);
}

/*
 * Implementation must return which ending of the entity will
 * be trimmed if 'coord' is the coordinate chosen to indicate the
 * trim entity and 'trimPoint' is the point to which the entity will
 * be trimmed.
 */
FAS2::Ending FAS_AtomicEntity::getTrimPoint(const FAS_Vector& /*coord*/,
                                            const FAS_Vector& /*trimPoint*/)
{
    return FAS2::EndingNone;
}

/*
 * Implementation must trim the entity in the case of multiple
 * intersections and return the trimPoint
 * trimCoord indicts the trigger trim position
 * trimSol contains intersections
 */
FAS_Vector FAS_AtomicEntity::prepareTrim(const FAS_Vector& /*trimCoord*/,
                                         const FAS_VectorSolutions& /*trimSol*/)
{
    return FAS_Vector(false);
}

void FAS_AtomicEntity::reverse() {}

void FAS_AtomicEntity::moveSelectedRef(const FAS_Vector& ref, const FAS_Vector& offset)
{
    if (isSelected()) {
        moveRef(ref, offset);
    }
}
