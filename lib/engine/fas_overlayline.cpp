/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_overlayline.h"

//sort with alphabetical order
#include "fas_painter.h"

FAS_OverlayLine::FAS_OverlayLine(FAS_EntityContainer* parent,
                 const FAS_LineData& d)
        :FAS_Line(parent, d)
{}
void FAS_OverlayLine::draw(FAS_Painter* painter, FAS_GraphicView* view, double& /*patternOffset*/)
{
    if (painter==nullptr || view==nullptr)
    {
        return;
    }
    painter->drawLine(getStartpoint(), getEndpoint());
}
