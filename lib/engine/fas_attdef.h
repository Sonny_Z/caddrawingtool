#ifndef FAS_ATTDEF_H
#define FAS_ATTDEF_H
//#include "fas_mtext.h"
#include "fas_text.h"
struct FAS_AttdefData
{
    FAS_AttdefData(FAS_TextData textdata,
                QString label);
    FAS_TextData textdata;
    QString label;
};
class FAS_ATTDEF : public FAS_Text//FAS_MText
{
public:
    //FAS_ATTDEF(FAS_EntityContainer* parent,
               //const FAS_MTextData& d);
    FAS_ATTDEF(FAS_EntityContainer* parent,
               const FAS_AttdefData& d);
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);
    void calculateBorders();
    virtual FAS2::EntityType rtti() const override
    {
        return FAS2::EntityAttdef;
    }

public:
   FAS_AttdefData data;
};

#endif // FAS_ATTDEF_H
