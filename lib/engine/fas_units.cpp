/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_units.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>
#include <QObject>

#include "fas_math.h"
#include "fas_vector.h"

/**
 * Converts a DXF integer () to a Unit enum.
 */
FAS2::Unit FAS_Units::dxfint2unit(int dxfint)
{
    return (FAS2::Unit)dxfint;
}

// return a short string representing the given unit (e.g. "mm")
QString FAS_Units::unitToSign(FAS2::Unit u)
{
    QString ret = "";

    switch (u) {
    case FAS2::None:
        ret = "";
        break;
    case FAS2::Inch:
        ret = "\"";
        break;
    case FAS2::Foot:
        ret = "'";
        break;
    case FAS2::Mile:
        ret = "mi";
        break;
    case FAS2::Millimeter:
        ret = "mm";
        break;
    case FAS2::Centimeter:
        ret = "cm";
        break;
    case FAS2::Meter:
        ret = "m";
        break;
    case FAS2::Kilometer:
        ret = "km";
        break;
    case FAS2::Microinch:
        ret = "µ\"";
        break;
    case FAS2::Mil:
        ret = "mil";
        break;
    case FAS2::Yard:
        ret = "yd";
        break;
    case FAS2::Angstrom:
        ret = "A";
        break;
    case FAS2::Nanometer:
        ret = "nm";
        break;
    case FAS2::Micron:
        ret = "µm";
        break;
    case FAS2::Decimeter:
        ret = "dm";
        break;
    case FAS2::Decameter:
        ret = "dam";
        break;
    case FAS2::Hectometer:
        ret = "hm";
        break;
    case FAS2::Gigameter:
        ret = "Gm";
        break;
    case FAS2::Astro:
        ret = "astro";
        break;
    case FAS2::Lightyear:
        ret = "ly";
        break;
    case FAS2::Parsec:
        ret = "pc";
        break;

    default:
        ret = "";
        break;
    }

    return ret;
}

//return a string representing the given unit (e.g. "Millimeter").translated if @a t is true (the default).
QString FAS_Units::unitToString(FAS2::Unit u, bool t)
{
    switch (u)
    {
    case FAS2::None:
        return "None";
    case FAS2::Inch:
        return "Inch";
    case FAS2::Foot:
        return "Foot";
    case FAS2::Mile:
        return "Mile";
    case FAS2::Millimeter:
        return "Millimeter";
    case FAS2::Centimeter:
        return "Centimeter";
    case FAS2::Meter:
        return "Meter";
    case FAS2::Kilometer:
        return "Kilometer";
    case FAS2::Microinch:
        return "Microinch";
    case FAS2::Mil:
        return "Mil";
    case FAS2::Yard:
        return "Yard";
    case FAS2::Angstrom:
        return "Angstrom";
    case FAS2::Nanometer:
        return "Nanometer";
    case FAS2::Micron:
        return "Micron";
    case FAS2::Decimeter:
        return "Decimeter";
    case FAS2::Decameter:
        return "Decameter";
    case FAS2::Hectometer:
        return "Hectometer";
    case FAS2::Gigameter:
        return "Gigameter";
    case FAS2::Astro:
        return "Astro";
    case FAS2::Lightyear:
        return "Lightyear";
    case FAS2::Parsec:
        return "Parsec";
    default:
        return "";
    }
}

// Converts a string into a unit enum.
FAS2::Unit FAS_Units::stringToUnit(const QString& u)
{
    FAS2::Unit ret = FAS2::None;

    if (u=="None")
    {
        ret = FAS2::None;
    }
    else if (u=="Inch")
    {
        ret = FAS2::Inch;
    }
    else if (u=="Foot")
    {
        ret = FAS2::Foot;
    }
    else if (u=="Mile")
    {
        ret = FAS2::Mile;
    }
    else if (u=="Millimeter")
    {
        ret = FAS2::Millimeter;
    }
    else if (u=="Centimeter")
    {
        ret = FAS2::Centimeter;
    }
    else if (u=="Meter")
    {
        ret = FAS2::Meter;
    }
    else if (u=="Kilometer")
    {
        ret = FAS2::Kilometer;
    }
    else if (u=="Microinch")
    {
        ret = FAS2::Microinch;
    }
    else if (u=="Mil")
    {
        ret = FAS2::Mil;
    }
    else if (u=="Yard")
    {
        ret = FAS2::Yard;
    }
    else if (u=="Angstrom")
    {
        ret = FAS2::Angstrom;
    }
    else if (u=="Nanometer")
    {
        ret = FAS2::Nanometer;
    }
    else if (u=="Micron")
    {
        ret = FAS2::Micron;
    }
    else if (u=="Decimeter")
    {
        ret = FAS2::Decimeter;
    }
    else if (u=="Decameter")
    {
        ret = FAS2::Decameter;
    }
    else if (u=="Hectometer")
    {
        ret = FAS2::Hectometer;
    }
    else if (u=="Gigameter")
    {
        ret = FAS2::Gigameter;
    }
    else if (u=="Astro")
    {
        ret = FAS2::Astro;
    }
    else if (u=="Lightyear")
    {
        ret = FAS2::Lightyear;
    }
    else if (u=="Parsec")
    {
        ret = FAS2::Parsec;
    }
    return ret;
}

//return true: the unit is metric, false: the unit is imperial.
bool FAS_Units::isMetric(FAS2::Unit u)
{
    switch (u)
    {
    case FAS2::Millimeter:
    case FAS2::Centimeter:
    case FAS2::Meter:
    case FAS2::Kilometer:
    case FAS2::Angstrom:
    case FAS2::Nanometer:
    case FAS2::Micron:
    case FAS2::Decimeter:
    case FAS2::Decameter:
    case FAS2::Hectometer:
    case FAS2::Gigameter:
    case FAS2::Astro:
    case FAS2::Lightyear:
    case FAS2::Parsec:
        return true;
    default:
        return false;
    }
}

// return factor to convert the given unit to Millimeters.
double FAS_Units::getFactorToMM(FAS2::Unit u)
{
    switch (u)
    {
    default:
    case FAS2::None:
    case FAS2::Millimeter:
        return 1.0;
    case FAS2::Inch:
        return 25.4;
    case FAS2::Foot:
        return 304.8;
    case FAS2::Mile:
        return 1.609344e6; //international mile
    case FAS2::Centimeter:
        return 10;
    case FAS2::Meter:
        return 1e3;
    case FAS2::Kilometer:
        return 1e6;
    case FAS2::Microinch:
        return 2.54e-5;
    case FAS2::Mil:
        return 0.0254;
    case FAS2::Yard:
        return 914.4;
    case FAS2::Angstrom:
        return 1e-7;
    case FAS2::Nanometer:
        return 1e-6;
    case FAS2::Micron:
        return 1e-3;
    case FAS2::Decimeter:
        return 100.0;
    case FAS2::Decameter:
        return 1e4;
    case FAS2::Hectometer:
        return 1e5;
    case FAS2::Gigameter:
        return 1e9;
    case FAS2::Astro:
        return 1.495978707e14;
    case FAS2::Lightyear:
        return 9.4607304725808e18;
    case FAS2::Parsec:
        return 3.0856776e19;
    }
}


//Converts the given value 'val' from unit 'src' to unit 'dest'.
double FAS_Units::convert(double val, FAS2::Unit src, FAS2::Unit dest)
{
    if (getFactorToMM(dest)>0.0)
    {
        return (val*getFactorToMM(src))/getFactorToMM(dest);
    }
    else
    {
        return val;
    }
}



//Converts the given vector 'val' from unit 'src' to unit 'dest'.
FAS_Vector FAS_Units::convert(const FAS_Vector& val, FAS2::Unit src, FAS2::Unit dest)
{
    return FAS_Vector(convert(val.x, src, dest),
                      convert(val.y, src, dest),
                      convert(val.z, src, dest)
                      );
}

/*
 * Formats the given length in the given format.
 * param length The length in the current unit of the drawing.
 * param format Format of the string.
 * param prec Precisision of the value (e.g. 0.001 or 1/128 = 0.0078125)
 & param showUnit Append unit to the value.
 */
QString FAS_Units::formatLinear(double length, FAS2::Unit unit,
                                FAS2::LinearFormat format,
                                int prec, bool showUnit)
{
    QString ret;
    switch (format)
    {
    case FAS2::Scientific:
        ret = formatScientific(length, unit, prec, showUnit);
        break;

    case FAS2::Decimal:
        ret = formatDecimal(length, unit, prec, showUnit);
        break;

    case FAS2::Engineering:
        ret = formatEngineering(length, unit, prec, showUnit);
        break;

    case FAS2::Architectural:
        ret = formatArchitectural(length, unit, prec, showUnit);
        break;

    case FAS2::Fractional:
        ret = formatFractional(length, unit, prec, showUnit);
        break;

    default:
        ret = "";
        break;
    }
    return ret;
}

/*
 * Formats the given length in scientific format (e.g. 2.5E7).
 * @param length The length in the current unit of the drawing.
 * @param prec Precisision of the value (e.g. 0.001 or 1/128 = 0.0078125)
 & @param showUnit Append unit to the value.
 */
QString FAS_Units::formatScientific(double length, FAS2::Unit unit,
                                    int prec, bool showUnit)
{

    QString const ret= QString("%1").arg(length,0,'E', prec);
    if(showUnit)
        return ret + unitToSign(unit);
    return ret;
}

/*
 * Formats the given length in decimal (normal) format (e.g. 2.5).
 * @param length The length in the current unit of the drawing.
 * @param prec Precisision of the value (e.g. 0.001)
 & @param showUnit Append unit to the value.
 */
QString FAS_Units::formatDecimal(double length, FAS2::Unit unit,
                                 int prec, bool showUnit)
{
    QString const ret=FAS_Math::doubleToString(length, prec);
    if(showUnit)
        return ret+unitToSign(unit);
    return ret;
}

/*
 * Formats the given length in engineering format (e.g. 5' 4.5").
 * @param length The length in the current unit of the drawing.
 * @param prec Precisision of the value (e.g. 0.001 or 1/128 = 0.0078125)
 & @param showUnit Append unit to the value.
 */
QString FAS_Units::formatEngineering(double length, FAS2::Unit /*unit*/,
                                     int prec, bool /*showUnit*/)
{
    QString ret;
    bool sign = (length<0.0);
    int feet = (int)floor(fabs(length)/12);
    double inches = fabs(length) - feet*12;
    QString sInches = FAS_Math::doubleToString(inches, prec);
    if (sInches=="12")
    {
        feet++;
        sInches="0";
    }
    if (feet)
    {
        ret = QString("%1'-%2\"").arg(feet).arg(sInches);
    }
    else
    {
        ret = QString("%1\"").arg(sInches);
    }

    if (sign)
    {
        ret = "-" + ret;
    }
    return ret;
}

/*
 * Formats the given length in architectural format (e.g. 5' 4 1/2").
 * @param length The length in the current unit of the drawing.
 * @param prec Precisision of the value (e.g. 0.001 or 1/128 = 0.0078125)
 & @param showUnit Append unit to the value.
 */
QString FAS_Units::formatArchitectural(double length, FAS2::Unit /*unit*/,
                                       int prec, bool showUnit)
{
    QString ret;
    bool neg = (length<0.0);
    int feet = (int)floor(fabs(length)/12);
    double inches = fabs(length) - feet*12;
    QString sInches = formatFractional(inches, FAS2::Inch, prec, showUnit);
    if (sInches=="12")
    {
        feet++;
        sInches = "0";
    }

    if (neg)
    {
        ret = QString("-%1'-%2\"").arg(feet).arg(sInches);
    }
    else
    {
        ret = QString("%1'-%2\"").arg(feet).arg(sInches);
    }
    return ret;
}

/*
 * Formats the given length in fractional (barbarian) format (e.g. 5' 3 1/64").
 * @param length The length in the current unit of the drawing.
 * @param unit Should be inches.
 * @param prec Precisision of the value (e.g. 0.001 or 1/128 = 0.0078125)
 & @param showUnit Append unit to the value.
 */
QString FAS_Units::formatFractional(double length, FAS2::Unit /*unit*/,
                                    int prec, bool /*showUnit*/)
{
    QString ret;
    unsigned num;            // number of complete inches (num' 7/128")
    unsigned nominator;      // number of fractions (nominator/128)
    unsigned denominator;    // (4/denominator)
    // sign:
    QString neg = "";
    if(length < 0)
    {
        neg = "-";
        length = fabs(length);
    }
    num = (unsigned)floor(length);
    denominator = 2<<prec;
    nominator = (unsigned) FAS_Math::round((length-num)*denominator);
    // fraction rounds up to 1:
    if (nominator==denominator)
    {
        nominator=0;
        denominator=0;
        ++num;
    }
    // Simplify the fraction
    if (nominator && denominator)
    {
        unsigned gcd = FAS_Math::findGCD(nominator, denominator);
        if (gcd)
        {
            nominator = nominator / gcd;
            denominator = denominator / gcd;
        }
        else
        {
            nominator = 0;
            denominator = 0;
        }
    }

    if( num && nominator )
    {
        ret = QString("%1%2 %3/%4").arg(neg).arg(num).arg(nominator).arg(denominator);
    }
    else if(nominator)
    {
        ret = QString("%1%2/%3").arg(neg).arg(nominator).arg(denominator);
    }
    else if(num)
    {
        ret = QString("%1%2").arg(neg).arg(num);
    }
    else
    {
        ret = "0";
    }
    return ret;
}

/*
 * Formats the given angle with the given format.
 * @param angle The angle (always in rad).
 * @param format Format of the string.
 * @param prec Precisision of the value (e.g. 0.001 or 1/128 = 0.0078125)
 * @ret String with the formatted angle.
 */
QString FAS_Units::formatAngle(double angle, FAS2::AngleFormat format,
                               int prec)
{
    QString ret;
    double value;
    switch (format)
    {
    case FAS2::DegreesDecimal:
    case FAS2::DegreesMinutesSeconds:
        value = FAS_Math::rad2deg(angle);
        break;
    case FAS2::Radians:
        value = angle;
        break;
    case FAS2::Gradians:
        value = FAS_Math::rad2gra(angle);
        break;
    default:
        return "";
        break;
    }

    switch (format)
    {
    case FAS2::DegreesDecimal:
    case FAS2::Radians:
    case FAS2::Gradians:
        ret = FAS_Math::doubleToString(value, prec);
        if (format==FAS2::DegreesDecimal)
            ret+=QChar(0xB0);
        if (format==FAS2::Radians)
            ret+="r";
        if (format==FAS2::Gradians)
            ret+="g";
        break;

    case FAS2::DegreesMinutesSeconds:
    {
        int vDegrees, vMinutes;
        double vSeconds;
        QString degrees, minutes, seconds;

        vDegrees = (int)floor(value);
        vMinutes = (int)floor((value - vDegrees) * 60.0);
        vSeconds = (value - vDegrees - (vMinutes/60.0)) * 3600.0;

        seconds = FAS_Math::doubleToString(vSeconds, (prec>1 ? prec-2 : 0));

        if(seconds=="60")
        {
            seconds="0";
            ++vMinutes;
            if(vMinutes==60)
            {
                vMinutes=0;
                ++vDegrees;
            }
        }

        if (prec==0 && vMinutes>=30.0)
        {
            vDegrees++;
        }
        else if (prec==1 && vSeconds>=30.0)
        {
            vMinutes++;
        }

        degrees.setNum(vDegrees);
        minutes.setNum(vMinutes);

        switch (prec)
        {
        case 0:
            ret = degrees + QChar(0xB0);
            break;
        case 1:
            ret = degrees + QChar(0xB0) + " " + minutes + "'";
            break;
        default:
            ret = degrees + QChar(0xB0) + " " + minutes + "' "
                    + seconds + "\"";
            break;
        }
    }
        break;

    default:
        break;
    }

    return ret;
}

/*
 * Converts the given number from a DXF file into an AngleFormat enum.
 * @param num $DIMAUNIT from DXF (0: decimal deg, 1: deg/min/sec, 2: gradians,
 *                                3: radians, 4: surveyor's units)
 * @ret Matching AngleFormat enum value.
 */
FAS2::AngleFormat FAS_Units::numberToAngleFormat(int num)
{
    FAS2::AngleFormat af;
    switch (num)
    {
    default:
    case 0:
        af = FAS2::DegreesDecimal;
        break;
    case 1:
        af = FAS2::DegreesMinutesSeconds;
        break;
    case 2:
        af = FAS2::Gradians;
        break;
    case 3:
        af = FAS2::Radians;
        break;
    case 4:
        af = FAS2::Surveyors;
        break;
    }
    return af;
}

//return Size of the given paper format.
FAS_Vector FAS_Units::paperFormatToSize(FAS2::PaperFormat p)
{
    FAS_Vector ret(false);
    switch (p)
    {
    case FAS2::Custom:
        ret = FAS_Vector(0.0, 0.0);
        break;
    case FAS2::Letter:
        ret = FAS_Vector(215.9, 279.4);
        break;
    case FAS2::Legal:
        ret = FAS_Vector(215.9, 355.6);
        break;
    case FAS2::Executive:
        ret = FAS_Vector(190.5, 254.0);
        break;
    case FAS2::A0:
        ret = FAS_Vector(841.0, 1189.0);
        break;
    case FAS2::A1:
        ret = FAS_Vector(594.0, 841.0);
        break;
    case FAS2::A2:
        ret = FAS_Vector(420.0, 594.0);
        break;
    case FAS2::A3:
        ret = FAS_Vector(297.0, 420.0);
        break;
    case FAS2::A4:
        ret = FAS_Vector(210.0, 297.0);
        break;
    case FAS2::A5:
        ret = FAS_Vector(148.0, 210.0);
        break;
    case FAS2::A6:
        ret = FAS_Vector(105.0, 148.0);
        break;
    case FAS2::A7:
        ret = FAS_Vector(74.0, 105.0);
        break;
    case FAS2::A8:
        ret = FAS_Vector(52.0, 74.0);
        break;
    case FAS2::A9:
        ret = FAS_Vector(37.0, 52.0);
        break;
    case FAS2::B0:
        ret = FAS_Vector(1000.0, 1414.0);
        break;
    case FAS2::B1:
        ret = FAS_Vector(707.0, 1000.0);
        break;
    case FAS2::B2:
        ret = FAS_Vector(500.0, 707.0);
        break;
    case FAS2::B3:
        ret = FAS_Vector(353.0, 500.0);
        break;
    case FAS2::B4:
        ret = FAS_Vector(250.0, 353.0);
        break;
    case FAS2::B5:
        ret = FAS_Vector(176.0, 250.0);
        break;
    case FAS2::B6:
        ret = FAS_Vector(125.0, 176.0);
        break;
    case FAS2::B7:
        ret = FAS_Vector(88.0, 125.0);
        break;
    case FAS2::B8:
        ret = FAS_Vector(62.0, 88.0);
        break;
    case FAS2::B9:
        ret = FAS_Vector(44.0, 62.0);
        break;
    case FAS2::B10:
        ret = FAS_Vector(31.0, 44.0);
        break;
    case FAS2::C5E:
        ret = FAS_Vector(163.0, 229.0);
        break;
    case FAS2::Comm10E:
        ret = FAS_Vector(105.0, 241.0);
        break;
    case FAS2::DLE:
        ret = FAS_Vector(110.0, 220.0);
        break;
    case FAS2::Folio:
        ret = FAS_Vector(210.0, 330.0);
        break;
    case FAS2::Ledger:
        ret = FAS_Vector(432.0, 279.0);
        break;
    case FAS2::Tabloid:
        ret = FAS_Vector(279.0, 432.0);
        break;
    case FAS2::Arch_A:
        return FAS_Vector(229.,305.);
    case FAS2::Arch_B:
        return FAS_Vector(305.,457.);
    case FAS2::Arch_C:
        return FAS_Vector(457.,610.);
    case FAS2::Arch_D:
        return FAS_Vector(610.,914.);
    case FAS2::Arch_E:
        return FAS_Vector(914.,1219.);
    case FAS2::Arch_E1:
        return FAS_Vector(762.,1067.);
    case FAS2::Arch_E2:
        return FAS_Vector(660.,965.);
    case FAS2::Arch_E3:
        return FAS_Vector(686.,991.);
    case FAS2::NPageSize:
        return FAS_Vector(0.0, 0.0);
        break;
    default:
        break;
    }
    return ret;
}

//Gets the paper format which matches the given size. If no format matches, FAS2::Custom is returned.
FAS2::PaperFormat FAS_Units::paperSizeToFormat(const FAS_Vector& s)
{
    FAS_Vector ts1;
    FAS_Vector ts2;
    for (int i=(int)FAS2::Custom; i<=(int)FAS2::NPageSize; ++i)
    {
        ts1 = FAS_Units::paperFormatToSize((FAS2::PaperFormat)i);
        ts2 = FAS_Vector(ts1.y, ts1.x);
        if (ts1.distanceTo(s)<1.0e-4 || ts2.distanceTo(s)<1.0e-4)
        {
            return (FAS2::PaperFormat)i;
        }
    }
    return FAS2::Custom;
}

/**
 * Converts a paper format to a string (e.g. for a combobox).
 */
QString FAS_Units::paperFormatToString(FAS2::PaperFormat p)
{
    QString ret = "";

    switch (p)
    {
    case FAS2::Custom:
        ret = "Custom";
        break;
    case FAS2::Letter:
        ret = "Letter";
        break;
    case FAS2::Legal:
        ret = "Legal";
        break;
    case FAS2::Executive:
        ret = "Executive";
        break;
    case FAS2::A0:
        ret = "A0";
        break;
    case FAS2::A1:
        ret = "A1";
        break;
    case FAS2::A2:
        ret = "A2";
        break;
    case FAS2::A3:
        ret = "A3";
        break;
    case FAS2::A4:
        ret = "A4";
        break;
    case FAS2::A5:
        ret = "A5";
        break;
    case FAS2::A6:
        ret = "A6";
        break;
    case FAS2::A7:
        ret = "A7";
        break;
    case FAS2::A8:
        ret = "A8";
        break;
    case FAS2::A9:
        ret = "A9";
        break;
    case FAS2::B0:
        ret = "B0";
        break;
    case FAS2::B1:
        ret = "B1";
        break;
    case FAS2::B2:
        ret = "B2";
        break;
    case FAS2::B3:
        ret = "B3";
        break;
    case FAS2::B4:
        ret = "B4";
        break;
    case FAS2::B5:
        ret = "B5";
        break;
    case FAS2::B6:
        ret = "B6";
        break;
    case FAS2::B7:
        ret = "B7";
        break;
    case FAS2::B8:
        ret = "B8";
        break;
    case FAS2::B9:
        ret = "B9";
        break;
    case FAS2::B10:
        ret = "B10";
        break;
    case FAS2::C5E:
        ret = "C5E";
        break;
    case FAS2::Comm10E:
        ret = "Comm10E";
        break;
    case FAS2::DLE:
        ret = "DLE";
        break;
    case FAS2::Folio:
        ret = "Folio";
        break;
    case FAS2::Ledger:
        ret = "Ledger";
        break;
    case FAS2::Tabloid:
        ret = "Tabloid";
        break;
    case FAS2::Arch_A:
        return QString("Arch A");
    case FAS2::Arch_B:
        return QString("Arch B");
    case FAS2::Arch_C:
        return QString("Arch C");
    case FAS2::Arch_D:
        return QString("Arch D");
    case FAS2::Arch_E:
        return QString("Arch E");
    case FAS2::Arch_E1:
        return QString("Arch E1");
    case FAS2::Arch_E2:
        return QString("Arch E2");
    case FAS2::Arch_E3:
        return QString("Arch E3");

    case FAS2::NPageSize:
        ret = "NPageSize";
        break;
    default:
        break;
    }

    return ret;
}
//Converts a string to a paper format.
FAS2::PaperFormat FAS_Units::stringToPaperFormat(const QString& p)
{
    QString ls = p.toLower();
    FAS2::PaperFormat ret = FAS2::Custom;

    if (p=="custom")
    {
        ret = FAS2::Custom;
    } else if (p=="letter") {
        ret = FAS2::Letter;
    } else if (p=="legal") {
        ret = FAS2::Legal;
    } else if (p=="executive") {
        ret = FAS2::Executive;
    } else if (p=="a0") {
        ret = FAS2::A0;
    } else if (p=="a1") {
        ret = FAS2::A1;
    } else if (p=="a2") {
        ret = FAS2::A2;
    } else if (p=="a3") {
        ret = FAS2::A3;
    } else if (p=="a4") {
        ret = FAS2::A4;
    } else if (p=="a5") {
        ret = FAS2::A5;
    } else if (p=="a6") {
        ret = FAS2::A6;
    } else if (p=="a7") {
        ret = FAS2::A7;
    } else if (p=="a8") {
        ret = FAS2::A8;
    } else if (p=="a9") {
        ret = FAS2::A9;
    } else if (p=="b0") {
        ret = FAS2::B0;
    } else if (p=="b1") {
        ret = FAS2::B1;
    } else if (p=="b2") {
        ret = FAS2::B2;
    } else if (p=="b3") {
        ret = FAS2::B3;
    } else if (p=="b4") {
        ret = FAS2::B4;
    } else if (p=="b5") {
        ret = FAS2::B5;
    } else if (p=="b6") {
        ret = FAS2::B6;
    } else if (p=="b7") {
        ret = FAS2::B7;
    } else if (p=="b8") {
        ret = FAS2::B8;
    } else if (p=="b9") {
        ret = FAS2::B9;
    } else if (p=="b10") {
        ret = FAS2::B10;
    }
    else if (p=="c5e") {
        ret = FAS2::C5E;
    } else if (p=="comm10e") {
        ret = FAS2::Comm10E;
    } else if (p=="dle") {
        ret = FAS2::DLE;
    } else if (p=="folio") {
        ret = FAS2::Folio;
    } else if (p=="ledger") {
        ret = FAS2::Ledger;
    } else if (p=="tabloid") {
        ret = FAS2::Tabloid;
    }
    if (p==QString("Arch A")) return FAS2::Arch_A;
    if (p==QString("Arch B")) return FAS2::Arch_B;
    if (p==QString("Arch C")) return FAS2::Arch_C;
    if (p==QString("Arch D")) return FAS2::Arch_D;
    if (p==QString("Arch E")) return FAS2::Arch_E;
    if (p==QString("Arch E1")) return FAS2::Arch_E1;
    if (p==QString("Arch E2")) return FAS2::Arch_E2;
    if (p==QString("Arch E3")) return FAS2::Arch_E3;

    if (p=="npagesize") return FAS2::NPageSize;

    return ret;
}

// Calculates a scaling factor from given dpi and units.
double FAS_Units::dpiToScale(double dpi, FAS2::Unit unit)
{
    double scale = FAS_Units::convert(1.0, FAS2::Inch, unit) / dpi;
    return scale;
}

// Calculates a dpi value from given scaling factor and units.
double FAS_Units::scaleToDpi(double scale, FAS2::Unit unit)
{
    double dpi = FAS_Units::convert(1.0, FAS2::Inch, unit) / scale;
    return dpi;
}

void FAS_Units::test()
{
    QString s;
    double v;
    for (v=11.9999; v<12.0001; v+=0.0000001)
    {
        for (int prec=0; prec<=6; ++prec)
        {
            s = FAS_Units::formatLinear(v, FAS2::Inch, FAS2::Architectural,
                                        prec, true);
            std::cout << "prec: " << prec << " v: " << v << " s: " << s.toLatin1().data() << "\n";
        }
    }
}

