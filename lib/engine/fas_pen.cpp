/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include <iostream>
#include "fas_pen.h"

std::ostream& operator << (std::ostream& os, const FAS_Pen& p) {
    //os << "style: " << p.style << std::endl;
    os << " pen color: " << p.getColor()
    << " pen width: " << p.getWidth()
    << " pen screen width: " << p.getScreenWidth()
    << " pen line type: " << p.getLineType()
    << " flags: " << (p.getFlag(FAS2::FlagInvalid) ? "INVALID" : "")
    << std::endl;
    return os;
}
