/****************************************************************************
**
** This file is part of the LibreCAD project, a 2D CAD program
**
** Copyright (C) 2010 R. van Twisk (librecad@rvt.dds.nl)
** Copyright (C) 2001-2003 RibbonSoft. All rights reserved.
**
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by 
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
** This copyright notice MUST APPEAR in all copies of the script!  
**
**********************************************************************/


#ifndef FAS_FONTCHAR_H
#define FAS_FONTCHAR_H

#include "fas_block.h"

#define FAS_String QString
/**
 * A character in a font is represented by this special block class.
 *
 * @author Andrew Mustun
 */
class FAS_FontChar : public FAS_Block {
public:
    /**
     * @param parent The font this block belongs to.
     * @param name The name of the letter (a unicode char) used as 
     *        an identifier.
     * @param basePoint Base point (offset) of the letter (usually 0/0).
     */
    FAS_FontChar(FAS_EntityContainer* parent,
                const FAS_String& name,
                FAS_Vector basePoint)
            : FAS_Block(parent, FAS_BlockData(name, basePoint, false)) {}

    virtual ~FAS_FontChar() {}

    /** @return FAS2::EntityFontChar */
    virtual FAS2::EntityType rtti() const {
        return FAS2::EntityFontChar;
    }


    /*friend std::ostream& operator << (std::ostream& os, const FAS_FontChar& b) {
       	os << " name: " << b.getName().latin1() << "\n";
        os << " entities: " << (FAS_EntityContainer&)b << "\n";
       	return os;
}*/


protected:
};


#endif
