#include "fas_funlib.h"
#include<QDataStream>
#include <QFile>
bool FAS_FunLib::writeFAS_Graphic(QString fileName,FAS_Graphic& graphic)
{
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
    {
        return false;
    }
    QDataStream out(&file);
    out<<graphic;
    file.close();
    return true;
}
bool FAS_FunLib::readFAS_Graphic(QString fileName,FAS_Graphic& graphic)
{
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
    {
        return false;
    }
    QDataStream in(&file);
    in>>graphic;
    file.close();
    return true;
}
bool FAS_FunLib::writeFAS_Graphic(QString fileName,QVector<FAS_Graphic>& graphics)
{
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
    {
        return false;
    }
    QDataStream out(&file);
    out<<graphics;
    file.close();
    return true;
}
bool FAS_FunLib::readFAS_Graphic(QString fileName,QVector<FAS_Graphic>& graphics)
{
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
    {
        return false;
    }
    QDataStream in(&file);
    in>>graphics;
    file.close();
    return true;
}
