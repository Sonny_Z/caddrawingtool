/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_IMAGE_H
#define FAS_IMAGE_H

#include <memory>
#include "fas_atomicentity.h"

class QImage;

// Holds the data that defines a line.
struct FAS_ImageData
{
    FAS_ImageData() {}
    FAS_ImageData(int handle,
                  const FAS_Vector& insertionPoint,
                  const FAS_Vector& uVector,
                  const FAS_Vector& vVector,
                  const FAS_Vector& size,
                  const QString& file,
                  int brightness,
                  int contrast,
                  int fade);

    // Handle of image definition.
    int handle;
    // Insertion point.
    FAS_Vector insertionPoint;
    // u vector. Points along visual bottom of image.
    FAS_Vector uVector;
    // v vector. Points along visual left of image.
    FAS_Vector vVector;
    // Image size in pixel.
    FAS_Vector size;
    // Path to image file.
    QString file;
    // Brightness (0..100, default: 50).
    int brightness;
    // Contrast (0..100, default: 50).
    int contrast;
    // Fade (0..100, default: 0).
    int fade;
};

// Class for a image entity.
class FAS_Image : public FAS_AtomicEntity
{
public:
    FAS_Image(FAS_EntityContainer* parent,
              const FAS_ImageData& d);
    FAS_Image(const FAS_Image& _image);
    FAS_Image(FAS_Image&& _image);
    FAS_Image& operator = (const FAS_Image& _image);
    FAS_Image& operator = (FAS_Image&& _image);
    FAS_Entity* clone() const override;
    FAS2::EntityType rtti() const override
    {
        return FAS2::EntityImage;
    }
    void update() override;
    FAS_ImageData getData() const
    {
        return data;
    }

    // return Insertion point of the entity
    FAS_Vector getInsertionPoint() const
    {
        return data.insertionPoint;
    }
    // sets the insertion point for the image.
    void setInsertionPoint(FAS_Vector ip)
    {
        data.insertionPoint = ip;
        calculateBorders();
    }
    // Update image data ONLY for plugins.
    void updateData(FAS_Vector size, FAS_Vector Uv, FAS_Vector Vv);
    //return File name of the image./
    QString getFile() const
    {
        return data.file;
    }
    // Sets the file name of the image.
    void setFile(const QString& file)
    {
        data.file = file;
    }

    // return u Vector. Points along bottom, 1 pixel long.
    FAS_Vector getUVector() const
    {
        return data.uVector;
    }
    // return v Vector. Points along left, 1 pixel long.
    FAS_Vector getVVector() const
    {
        return data.vVector;
    }
    // return Width of image in pixels.
    int getWidth() const
    {
        return (int)data.size.x;
    }
    // return Height of image in pixels.
    int getHeight() const
    {
        return (int)data.size.y;
    }
    // return Brightness.
    int getBrightness() const
    {
        return data.brightness;
    }
    // return Contrast.
    int getContrast() const
    {
        return data.contrast;
    }
    // return Fade.
    int getFade() const
    {
        return data.fade;
    }
    // return Image definition handle.
    int getHandle() const
    {
        return data.handle;
    }
    // Sets the image definition handle.
    void setHandle(int h)
    {
        data.handle = h;
    }
    // return The four corners. **/
    FAS_VectorSolutions getCorners() const;

    //return image with in graphic units.
    double getImageWidth()
    {
        return data.size.x * data.uVector.magnitude();
    }

    //return image height in graphic units.
    double getImageHeight()
    {
        return data.size.y * data.vVector.magnitude();
    }

    FAS_Vector getNearestEndpoint(const FAS_Vector& coord, double* dist = nullptr)const override;
    FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                       bool onEntity=true, double* dist = nullptr, FAS_Entity** entity=nullptr)const override;
    FAS_Vector getNearestCenter(const FAS_Vector& coord, double* dist = nullptr)const override;
    FAS_Vector getNearestMiddle(const FAS_Vector& coord, double* dist = nullptr, int middlePoints=1)const override;
    FAS_Vector getNearestDist(double distance, const FAS_Vector& coord, double* dist = nullptr)const override;
    double getDistanceToPoint(const FAS_Vector& coord,
                              FAS_Entity** entity=nullptr,
                              FAS2::ResolveLevel level=FAS2::ResolveNone,
                              double solidDist = FAS_MAXDOUBLE) const override;

    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
    friend std::ostream& operator << (std::ostream& os, const FAS_Image& l);
    void calculateBorders() override;

protected:
    // whether the the point is within image
    bool containsPoint(const FAS_Vector& coord) const;
    FAS_ImageData data;
    std::unique_ptr<QImage> img;
};

#endif
