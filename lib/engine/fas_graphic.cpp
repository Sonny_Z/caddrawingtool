/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_graphic.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>
#include <QDir>

#include "fas_block.h"
#include "fas_dxfprocessor.h"
#include "fas_layer.h"
#include "fas_math.h"
#include "fas_units.h"
/*
 *  QDateTime modifiedTime;
    QString currentFileName; //keep a copy of filename for the modifiedTime

    FAS_LayerList layerList;
    FAS_BlockList blockList;
    FAS_VariableDict variableDict;
    FAS2::CrosshairType crosshairType; //corss hair type used by isometric grid
    //if set to true, will refuse to modify paper scale
    bool paperScaleFixed;

    bool modified;

    FAS_Pen activePen;

    QString filename;

    QString autosaveFilename;

    FAS2::FormatType formatType;
    FAS_GraphicView * gv;//used to read/save current view
 * */
#if 1
QDataStream &operator<<(QDataStream &out,const FAS_Graphic &para)
{
    out<<para.currentFileName<<para.modifiedTime<<para.paperScaleFixed<<para.modified
    <<para.filename<<para.autosaveFilename<<para.formatType<<para.crosshairType;
    //<<para.layerList<<para.blockList<<para.variableDict;
    return out;
}

QDataStream &operator>>(QDataStream &in, FAS_Graphic &para)
{
    in>>para.currentFileName>>para.modifiedTime>>para.paperScaleFixed>>para.modified
      >>para.filename>>para.autosaveFilename>>para.formatType>>para.crosshairType;
    //>>para.layerList>>para.blockList>>para.variableDict;
    return in;
}
#endif

FAS_Graphic::FAS_Graphic(FAS_EntityContainer* parent)
    : FAS_Document(parent),
      layerList(),
      blockList(true),paperScaleFixed(false)
{
    setUnit(FAS_Units::stringToUnit("None"));
    addVariable("$SNAPSTYLE",0,70);
    crosshairType = FAS2::LeftCrosshair;
    FAS2::Unit unit = getUnit();

    if (unit==FAS2::Inch)
    {
        addVariable("$DIMASZ", 0.1, 40);
        addVariable("$DIMEXE", 0.05, 40);
        addVariable("$DIMEXO", 0.025, 40);
        addVariable("$DIMGAP", 0.025, 40);
        addVariable("$DIMTXT", 0.1, 40);
    }
    else
    {
        addVariable("$DIMASZ",
                    FAS_Units::convert(2.5, FAS2::Millimeter, unit), 40);
        addVariable("$DIMEXE",
                    FAS_Units::convert(1.25, FAS2::Millimeter, unit), 40);
        addVariable("$DIMEXO",
                    FAS_Units::convert(0.625, FAS2::Millimeter, unit), 40);
        addVariable("$DIMGAP",
                    FAS_Units::convert(0.625, FAS2::Millimeter, unit), 40);
        addVariable("$DIMTXT",
                    FAS_Units::convert(2.5, FAS2::Millimeter, unit), 40);
    }
    addVariable("$DIMTIH", 0, 70);
    setPaperScale(getPaperScale());
    setPaperInsertionBase(getPaperInsertionBase());
    setModified(false);
}

FAS_Graphic::~FAS_Graphic()
{
    for(auto layer : layerList)
    {
        delete layer;
    }
    layerList.clear();
    for(auto block : blockList)
    {
        delete block;
    }
    blockList.clear();
}

// Counts the entities on the given layer.
unsigned long int FAS_Graphic::countLayerEntities(FAS_Layer* layer)
{
    int c=0;
    if (layer)
    {
        for(auto t: entities)
        {
            if (t->getLayer() &&
                    t->getLayer()->getName()==layer->getName())
            {
                c+=t->countDeep();
            }
        }
    }
    return c;
}

// Removes the given layer and undoes all entities on it.
void FAS_Graphic::removeLayer(FAS_Layer* layer)
{
    if (layer && layer->getName()!="0")
    {
        std::vector<FAS_Entity*> toRemove;
        //find entities on layer
        for(auto e: entities)
        {
            if (e->getLayer() && e->getLayer()->getName()==layer->getName())
            {
                toRemove.push_back(e);
            }
        }
        // remove all entities on that layer:
        if(toRemove.size())
        {
            startUndoCycle();
            for(auto e: toRemove)
            {
                e->setUndoState(true);
                e->setLayer("0");
                addUndoable(e);
            }
            endUndoCycle();
        }

        toRemove.clear();
        // remove all entities in blocks that are on that layer:
        for(FAS_Block* blk: blockList)
        {
            if(!blk) continue;
            for(auto e: *blk)
            {
                if (e->getLayer() &&
                        e->getLayer()->getName()==layer->getName())
                {
                    toRemove.push_back(e);
                }
            }
        }

        for(auto e: toRemove)
        {
            e->setUndoState(true);
            e->setLayer("0");
        }
        layerList.remove(layer);
    }
}


// Clears all layers, blocks and entities of this graphic. A default layer (0) is created.
void FAS_Graphic::newDoc()
{
    clear();
    clearLayers();
    clearBlocks();
    addLayer(new FAS_Layer("0"));
    setModified(false);
}

//Saves this graphic with the current filename and settings.
bool FAS_Graphic::save(bool isAutoSave)
{
    bool ret	= false;
    if (isModified())
    {
        QString actualName;
        FAS2::FormatType	actualType;

        actualType	= formatType;

        if (isAutoSave)
        {
            actualName = autosaveFilename;

            if (formatType == FAS2::FormatUnknown)
                actualType = FAS2::FormatDXF;
        }
        else
        {
            QFileInfo	finfo(filename);
            QDateTime m=finfo.lastModified();
            if ( currentFileName == QString(filename) && modifiedTime.isValid() && m != modifiedTime )
            {
                return false;
            }

            actualName = filename;
        }
        if (!actualName.isEmpty())
        {
            FAS_DXFProcessor dxfWriter;
            ret = dxfWriter.fileExport(*this, actualName, actualType);
            QFileInfo	finfo(actualName);
            modifiedTime=finfo.lastModified();
            currentFileName=actualName;
        }
        if (ret && !isAutoSave)
        {
            QFile qf_file(autosaveFilename);
            // Tell that drawing file is no more modified.
            setModified(false);
            layerList.setModified(false);
            blockList.setModified(false);
            // Remove autosave file, if able to create associated object,and if autosave file exist.
            if (qf_file.exists())
            {
                qf_file.remove();
            }
        }
    }
    else
    {
        ret = true;
    }
    return ret;
}

// Saves this graphic with the given filename and current settings.
bool FAS_Graphic::saveAs(const QString &filename, FAS2::FormatType type, bool force)
{
    // Set to "failed" by default.
    bool ret	= false;

    // Check/memorize if file name we want to use as new file
    // name is the same as the actual file name.
    bool fn_is_same	= filename == this->filename;
    auto const filenameSaved=this->filename;
    auto const autosaveFilenameSaved=this->autosaveFilename;
    auto const formatTypeSaved=this->formatType;

    this->filename = filename;
    this->formatType	= type;

    // QString	const oldAutosaveName = this->autosaveFilename;
    QFileInfo	finfo(filename);

    // Construct new autosave filename by prepending # to the filename
    // part, using the same directory as the destination file.
    this->autosaveFilename = finfo.path() + "/#" + finfo.fileName();

    // When drawing is saved using a different name than the actual
    // drawing file name, think that drawing file
    // has been modified, to make sure the drawing file saved.
    if (!fn_is_same || force)
        setModified(true);

    ret = save();    // Save file.

    if (ret)
    {
        // Save was successful, remove old autosave file.
        QFile	qf_file(autosaveFilenameSaved);

        if (qf_file.exists())
        {
            qf_file.remove();
        }

    }
    else
    {
        //do not modify filenames:
        this->filename=filenameSaved;
        this->autosaveFilename=autosaveFilenameSaved;
        this->formatType=formatTypeSaved;
    }

    return ret;
}

// Loads the given file into this graphic.
bool FAS_Graphic::open(const QString &filename, FAS2::FormatType type)
{
    bool ret = false;

    this->filename = filename;
    QFileInfo finfo(filename);
    this->autosaveFilename = finfo.path() + "/#" + finfo.fileName();

    // clean all:
    newDoc();

    // import file:
    FAS_DXFProcessor dxfReader;
    ret = dxfReader.fileImport(*this, filename, type);

    if( ret)
    {
        setModified(false);
        layerList.setModified(false);
        blockList.setModified(false);
        modifiedTime = finfo.lastModified();
        currentFileName=QString(filename);
    }

    return ret;
}

// @return true if the grid is switched on (visible).
bool FAS_Graphic::isGridOn()
{
    int on = getVariableInt("$GRIDMODE", 1);
    return on!=0;
}

// Enables / disables the grid.
void FAS_Graphic::setGridOn(bool on)
{
    addVariable("$GRIDMODE", (int)on, 70);
}

//return true if the isometric grid is switched on (visible).
bool FAS_Graphic::isIsometricGrid()
{
    int on = getVariableInt("$SNAPSTYLE", 0);
    return on!=0;
}

// Enables / disables isometric grid.
void FAS_Graphic::setIsometricGrid(bool on)
{
    addVariable("$SNAPSTYLE", (int)on, 70);
}

void FAS_Graphic::setCrosshairType(FAS2::CrosshairType chType)
{
    crosshairType=chType;
}

FAS2::CrosshairType FAS_Graphic::getCrosshairType()
{
    return crosshairType;
}

// Sets the unit of this graphic to 'u'
void FAS_Graphic::setUnit(FAS2::Unit u)
{
    setPaperSize(FAS_Units::convert(getPaperSize(), getUnit(), u));
    addVariable("$INSUNITS", (int)u, 70);
}

// Gets the unit of this graphic
FAS2::Unit FAS_Graphic::getUnit()
{
    return (FAS2::Unit)getVariableInt("$INSUNITS", 0);
}

//return The linear format type for this document. This is determined by the variable "$LUNITS".
FAS2::LinearFormat FAS_Graphic::getLinearFormat()
{
    int lunits = getVariableInt("$LUNITS", 2);
    return getLinearFormat(lunits);
}

//return The linear format type used by the variable "$LUNITS" & "$DIMLUNIT".
FAS2::LinearFormat FAS_Graphic::getLinearFormat(int f){
    switch (f) {
    default:
    case 2:
        return FAS2::Decimal;
        break;
    case 1:
        return FAS2::Scientific;
        break;

    case 3:
        return FAS2::Engineering;
        break;

    case 4:
        return FAS2::Architectural;
        break;

    case 5:
        return FAS2::Fractional;
        break;
    }

    return FAS2::Decimal;
}

//return The linear precision for this document. This is determined by the variable "$LUPREC".
int FAS_Graphic::getLinearPrecision()
{
    return getVariableInt("$LUPREC", 4);
}

//return The angle format type for this document. This is determined by the variable "$AUNITS".
FAS2::AngleFormat FAS_Graphic::getAngleFormat()
{
    int aunits = getVariableInt("$AUNITS", 0);

    switch (aunits) {
    default:
    case 0:
        return FAS2::DegreesDecimal;
        break;

    case 1:
        return FAS2::DegreesMinutesSeconds;
        break;

    case 2:
        return FAS2::Gradians;
        break;

    case 3:
        return FAS2::Radians;
        break;

    case 4:
        return FAS2::Surveyors;
        break;
    }

    return FAS2::DegreesDecimal;
}

//return The linear precision for this document. This is determined by the variable "$LUPREC".
int FAS_Graphic::getAnglePrecision()
{
    return getVariableInt("$AUPREC", 4);
}

/*
 * return The insertion point of the drawing into the paper space.
 * This is the distance from the lower left paper edge to the zero
 * point of the drawing. DXF: $PINSBASE.
 */
FAS_Vector FAS_Graphic::getPaperInsertionBase()
{
    return getVariableVector("$PINSBASE", FAS_Vector(0.0,0.0));
}


// Sets the PINSBASE variable.
void FAS_Graphic::setPaperInsertionBase(const FAS_Vector& p)
{
    addVariable("$PINSBASE", p, 10);
}

//return Paper size in graphic units.
FAS_Vector FAS_Graphic::getPaperSize()
{
    FAS_Vector def= FAS_Units::convert(FAS_Vector(210.0,297.0), FAS2::Millimeter, getUnit());

    FAS_Vector v1 = getVariableVector("$PLIMMIN", FAS_Vector(0.0,0.0));
    FAS_Vector v2 = getVariableVector("$PLIMMAX", def);

    return v2-v1;
}

// Sets a new paper size.
void FAS_Graphic::setPaperSize(const FAS_Vector& s)
{
    addVariable("$PLIMMIN", FAS_Vector(0.0,0.0), 10);
    addVariable("$PLIMMAX", s, 10);
}

//return Paper format.
FAS2::PaperFormat FAS_Graphic::getPaperFormat(bool* landscape)
{
    FAS_Vector size = FAS_Units::convert(getPaperSize(),
                                         getUnit(), FAS2::Millimeter);

    if (landscape)
    {
        *landscape = (size.x>size.y);
    }

    return FAS_Units::paperSizeToFormat(size);
}

// Sets the paper format to the given format.
void FAS_Graphic::setPaperFormat(FAS2::PaperFormat f, bool landscape)
{
    FAS_Vector size = FAS_Units::paperFormatToSize(f);

    if (landscape ^ (size.x > size.y))
    {
        std::swap(size.x, size.y);
    }

    setPaperSize(FAS_Units::convert(size, FAS2::Millimeter, getUnit()));
}

// return Paper space scaling (DXF: $PSVPSCALE).
double FAS_Graphic::getPaperScale()
{
    double ret;

    ret = getVariableDouble("$PSVPSCALE", 1.0);

    return ret;
}

// Sets a new scale factor for the paper space.
void FAS_Graphic::setPaperScale(double s)
{
    if(paperScaleFixed==false) addVariable("$PSVPSCALE", s, 40);
}

// Centers drawing on page. Affects DXF variable $PINSBASE.
void FAS_Graphic::centerToPage()
{
    FAS_Vector size = getPaperSize();

    double scale = getPaperScale();
    auto s=getSize();
    auto sMin=getMin();
    if(fabs(s.x)<FAS_TOLERANCE)
    {
        s.x=10.;
        sMin.x=-5.;
    }
    if(fabs(s.y)<FAS_TOLERANCE)
    {
        s.y=10.;
        sMin.y=-5.;
    }

    FAS_Vector pinsbase = (size-s*scale)/2.0 - sMin*scale;

    setPaperInsertionBase(pinsbase);
}

// Fits drawing on page. Affects DXF variable $PINSBASE.
bool FAS_Graphic::fitToPage()
{
    bool ret(true);
    double border = FAS_Units::convert(25.0, FAS2::Millimeter, getUnit());
    FAS_Vector ps = getPaperSize();
    if(ps.x>border && ps.y>border) ps -= FAS_Vector(border, border);
    FAS_Vector s = getSize();
    if(fabs(s.x)<FAS_TOLERANCE) s.x=10.;
    if(fabs(s.y)<FAS_TOLERANCE) s.y=10.;
    double fx = FAS_MAXDOUBLE;
    double fy = FAS_MAXDOUBLE;
    double fxy;
    if (fabs(s.x) > FAS_TOLERANCE) {
        fx = ps.x / s.x;
        ret=false;
    }
    if (fabs(s.y) > FAS_TOLERANCE) {
        fy = ps.y / s.y;
        ret=false;
    }

    fxy = std::min(fx, fy);
    if (fxy >= FAS_MAXDOUBLE || fxy <= 1.0e-10)
    {
        setPaperSize(FAS_Units::convert(FAS_Vector(210.,297.), FAS2::Millimeter, getUnit()));
        ret=false;
    }
    setPaperScale(fxy);
    centerToPage();
    return ret;
}

void FAS_Graphic::addEntity(FAS_Entity* entity)
{
    FAS_EntityContainer::addEntity(entity);
    if( entity->rtti() == FAS2::EntityBlock ||
            entity->rtti() == FAS2::EntityContainer)
    {
        FAS_EntityContainer* e=static_cast<FAS_EntityContainer*>(entity);
        for(auto e1: *e)
        {
            addEntity(e1);
        }
    }
}

// Dumps the entities to stdout.
std::ostream& operator << (std::ostream& os, FAS_Graphic& g)
{
    os << "--- Graphic: \n";
    os << "---" << *g.getLayerList() << "\n";
    os << "---" << *g.getBlockList() << "\n";
    os << "---" << (FAS_Undo&)g << "\n";
    os << "---" << (FAS_EntityContainer&)g << "\n";

    return os;
}

// Removes invalid objects.return how many objects were removed
int FAS_Graphic::clean()
{
    // author: ravas

    int how_many = 0;

    foreach (FAS_Entity* e, entities)
    {
        if    (e->getMin().x > e->getMax().x
               || e->getMin().y > e->getMax().y
               || e->getMin().x > FAS_MAXDOUBLE
               || e->getMax().x > FAS_MAXDOUBLE
               || e->getMin().x < FAS_MINDOUBLE
               || e->getMax().x < FAS_MINDOUBLE
               || e->getMin().y > FAS_MAXDOUBLE
               || e->getMax().y > FAS_MAXDOUBLE
               || e->getMin().y < FAS_MINDOUBLE
               || e->getMax().y < FAS_MINDOUBLE)
        {
            removeEntity(e);
            how_many += 1;
        }
    }
    return how_many;
}
