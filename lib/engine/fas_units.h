/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_UNITS_H
#define FAS_UNITS_H

#include "fas.h"

class FAS_Vector;
class QString;

class FAS_Units
{
public:

    static FAS2::Unit dxfint2unit(int dxfint);

    static QString unitToString(FAS2::Unit u, bool t = true);
    static FAS2::Unit stringToUnit(const QString& u);

    static bool isMetric(FAS2::Unit u);
    static double getFactorToMM(FAS2::Unit u);
    static double convert(double val, FAS2::Unit src, FAS2::Unit dest);
    static FAS_Vector convert(const FAS_Vector& val, FAS2::Unit src, FAS2::Unit dest);

    static QString unitToSign(FAS2::Unit u);

    static QString formatLinear(double length, FAS2::Unit unit,
                                FAS2::LinearFormat format,
                                int prec, bool showUnit=false);
    static QString formatScientific(double length, FAS2::Unit unit,
                                    int prec, bool showUnit=false);
    static QString formatDecimal(double length, FAS2::Unit unit,
                                 int prec, bool showUnit=false);
    static QString formatEngineering(double length, FAS2::Unit unit,
                                     int prec, bool showUnit=false);
    static QString formatArchitectural(double length, FAS2::Unit unit,
                                       int prec, bool showUnit=false);
    static QString formatFractional(double length, FAS2::Unit unit,
                                    int prec, bool showUnit=false);

    static QString formatAngle(double angle, FAS2::AngleFormat format,
                               int prec);
    static FAS2::AngleFormat numberToAngleFormat(int num);

    static FAS_Vector paperFormatToSize(FAS2::PaperFormat p);
    static FAS2::PaperFormat paperSizeToFormat(const FAS_Vector& s);

    static QString paperFormatToString(FAS2::PaperFormat p);
    static FAS2::PaperFormat stringToPaperFormat(const QString& p);

    static void test();
    static double dpiToScale(double dpi, FAS2::Unit unit);
    static double scaleToDpi(double scale, FAS2::Unit unit);

};

#endif
