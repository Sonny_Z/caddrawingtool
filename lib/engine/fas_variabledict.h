/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_VARIABLEDICT_H
#define FAS_VARIABLEDICT_H

#include <QHash>
#include "fas_variable.h"

class FAS_Vector;
class QString;

/**
 * Dictionary of variables. The variables are stored as key / value
 * pairs (string / string).
 */
class FAS_VariableDict
{
public:
    FAS_VariableDict() {}

    void clear();
    //return Number of variables available.
    int count() const
    {
        return variables.count();
    }

    void add(const QString& key, const FAS_Vector& value, int code);
    void add(const QString& key, const QString& value, int code);
    void add(const QString& key, int value, int code);
    void add(const QString& key, double value, int code);

    FAS_Vector getVector(const QString& key, const FAS_Vector& def) const;
    QString getString(const QString& key, const QString& def) const;
    int getInt(const QString& key, int def) const;
    double getDouble(const QString& key, double def) const;

    void remove(const QString& key);

    QHash<QString, FAS_Variable> const& getVariableDict() const
    {
        return variables;
    }
    QHash<QString, FAS_Variable>& getVariableDict()
    {
        return variables;
    }
    friend std::ostream& operator << (std::ostream& os, FAS_VariableDict& v);

private:
    // Variables for the graphic
    QHash<QString, FAS_Variable> variables;
};

#endif
