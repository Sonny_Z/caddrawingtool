/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_solid.h"

//sort with alphabetical order
#include <iostream>
#include <cmath>

#include "fas_graphicview.h"
#include "fas_information.h"
#include "fas_line.h"
#include "fas_painter.h"

FAS_SolidData::FAS_SolidData():
    corner{{FAS_Vector(false), FAS_Vector(false), FAS_Vector(false), FAS_Vector(false)}}
{
}

/**
 * Constructor for a solid with 3 corners.
 */
FAS_SolidData::FAS_SolidData(const FAS_Vector& corner1,
                             const FAS_Vector& corner2,
                             const FAS_Vector& corner3):
    corner{{corner1, corner2, corner3, FAS_Vector(false)}}
{
}

/**
 * Constructor for a solid with 4 corners.
 */
FAS_SolidData::FAS_SolidData(const FAS_Vector& corner1,
                             const FAS_Vector& corner2,
                             const FAS_Vector& corner3,
                             const FAS_Vector& corner4):
    corner{{corner1, corner2, corner3, corner4}}
{
}

std::ostream& operator << (std::ostream& os,
                           const FAS_SolidData& pd) {
    os << "(";
    for (int i=0; i<4; i++) {
        os << pd.corner[i];
    }
    os << ")";
    return os;
}

FAS_Vector corner[4];

/**
 * Default constructor.
 */
FAS_Solid::FAS_Solid(FAS_EntityContainer* parent,
                     const FAS_SolidData& d)
    :FAS_AtomicEntity(parent), data(d) {
    calculateBorders();
}

FAS_Entity* FAS_Solid::clone() const {
    FAS_Solid* s = new FAS_Solid(*this);
    s->initId();
    return s;
}

/**
 * @return Corner number 'num'.
 */
FAS_Vector FAS_Solid::getCorner(int num) const{
    if (num>=0 && num<4) {
        return data.corner[num];
    } else {
        return FAS_Vector(false);
    }
}

/**
 * Shapes this Solid into a standard arrow (used in dimensions).
 *
 * @param point The point the arrow points to.
 * @param angle Direction of the arrow.
 * @param arrowSize Size of arrow (length).
 */
void FAS_Solid::shapeArrow(const FAS_Vector& point,
                           double angle,
                           double arrowSize)
{
    double cosv1, sinv1, cosv2, sinv2;
    double arrowSide = arrowSize/cos(0.165);

    cosv1 = cos(angle+0.165)*arrowSide;
    sinv1 = sin(angle+0.165)*arrowSide;
    cosv2 = cos(angle-0.165)*arrowSide;
    sinv2 = sin(angle-0.165)*arrowSide;

    data.corner[0] = point;
    data.corner[1] = FAS_Vector(point.x - cosv1, point.y - sinv1);
    data.corner[2] = FAS_Vector(point.x - cosv2, point.y - sinv2);
    data.corner[3] = FAS_Vector(false);

    calculateBorders();
}



void FAS_Solid::calculateBorders()
{
    resetBorders();
    for (int i=0; i<4; ++i)
    {
        if (data.corner[i].valid)
        {
            minV = FAS_Vector::minimum(minV, data.corner[i]);
            maxV = FAS_Vector::maximum(maxV, data.corner[i]);
        }
    }
}



FAS_Vector FAS_Solid::getNearestEndpoint(const FAS_Vector& coord, double* dist)const {

    double minDist = FAS_MAXDOUBLE;
    double curDist;
    FAS_Vector ret;

    for (int i=0; i<4; ++i) {
        if (data.corner[i].valid) {
            curDist = data.corner[i].distanceTo(coord);
            if (curDist<minDist) {
                ret = data.corner[i];
                minDist = curDist;
            }
        }
    }

    if (dist) {
        *dist = minDist;
    }

    return ret;
}

bool FAS_Solid::isInCrossWindow(const FAS_Vector& v1,const FAS_Vector& v2)const {
    //    bool sol = false;
    FAS_Vector vBL, vTR;
    FAS_VectorSolutions sol;
    //sort imput vectors to BottomLeft & TopRight
    if (v1.x<v2.x) {
        vBL.x = v1.x;
        vTR.x = v2.x;
    } else {
        vBL.x = v2.x;
        vTR.x = v1.x;
    }
    if (v1.y<v2.y) {
        vBL.y = v1.y;
        vTR.y = v2.y;
    } else {
        vBL.y = v2.y;
        vTR.y = v1.y;
    }
    //Check if is out of window
    if ( getMin().x > vTR.x || getMax().x < vBL.x
         || getMin().y > vTR.y || getMax().y < vBL.y) {
        return false;
    }
    std::vector<FAS_Line> l;
    l.emplace_back(data.corner[0], data.corner[1]);
    l.emplace_back(data.corner[1], data.corner[2]);
    if (data.corner[3].valid) {
        l.emplace_back(data.corner[2], data.corner[3]);
        l.emplace_back(data.corner[3], data.corner[0]);
    } else {
        l.emplace_back(data.corner[2], data.corner[0]);
    }
    //Find crossing edge
    if (getMax().x > vBL.x && getMin().x < vBL.x) {//left
        FAS_Line edge{vBL, {vBL.x, vTR.y}};
        for(auto const& l0: l) {
            sol = FAS_Information::getIntersection(&edge, &l0, true);
            if (sol.hasValid()) {
                return true;
            }
        }
    }
    if (getMax().x > vTR.x && getMin().x < vTR.x) {//right
        FAS_Line edge{{vTR.x, vBL.y}, vTR};
        for(auto const& l0: l) {
            sol = FAS_Information::getIntersection(&edge, &l0, true);
            if (sol.hasValid()) {
                return true;
            }
        }
    }
    if (getMax().y > vBL.y && getMin().y < vBL.y) {//bottom
        FAS_Line edge{vBL, {vTR.x, vBL.y}};
        for(auto const& l0: l) {
            sol = FAS_Information::getIntersection(&edge, &l0, true);
            if (sol.hasValid()) {
                return true;
            }
        }
    }
    if(getMax().y > vTR.y && getMin().y < vTR.y) {//top
        FAS_Line edge{{vBL.x, vTR.y}, vTR};
        for(auto const& l0: l) {
            sol = FAS_Information::getIntersection(&edge, &l0, true);
            if (sol.hasValid()) {
                return true;
            }
        }
    }
    return false;
}

/**
*
* @return true if positive o zero, false if negative.
*/
bool FAS_Solid::sign (const FAS_Vector& v1, const FAS_Vector& v2, const FAS_Vector& v3)const {
    double res = (v1.x-v3.x)*(v2.y-v3.y)-(v2.x-v3.x)*(v1.y-v3.y);
    return (res>=0.0);
}

/**
 * @todo Implement this.
 */
FAS_Vector FAS_Solid::getNearestPointOnEntity(const FAS_Vector& coord,
                                              bool onEntity, double* dist, FAS_Entity** entity)const {
    //first check if point is inside solid
    bool s1 = sign(data.corner[0], data.corner[1], coord);
    bool s2 = sign(data.corner[1], data.corner[2], coord);
    bool s3 = sign(data.corner[2], data.corner[0], coord);
    if ( (s1 == s2) && (s2 == s3) ) {
        if (dist)
            *dist = 0.0;
        return coord;
    }
    if (data.corner[3].valid) {
        s1 = sign(data.corner[0], data.corner[2], coord);
        s2 = sign(data.corner[2], data.corner[3], coord);
        s3 = sign(data.corner[3], data.corner[0], coord);
        if ( (s1 == s2) && (s2 == s3) ) {
            if (dist)
                *dist = 0.0;
            return coord;
        }
    }
    //not inside of solid
    FAS_Vector ret(false);
    double currDist = FAS_MAXDOUBLE;
    double tmpDist;
    if (entity) {
        *entity = const_cast<FAS_Solid*>(this);
    }
    //Find nearest distance from each edge
    int totalV = 3;
    if (data.corner[3].valid)
        totalV = 4;
    for (int i=0; i<=totalV; ++i) {
        int next =i+1;
        //closing edge
        if (next == totalV) next =0;

        FAS_Vector direction = data.corner[next]-data.corner[i];
        FAS_Vector vpc=coord-data.corner[i];
        double a=direction.squared();
        if( a < FAS_TOLERANCE2) {
            //line too short
            vpc=data.corner[i];
        }else{
            //find projection on line
            vpc = data.corner[i] + direction*FAS_Vector::dotP(vpc,direction)/a;
        }
        tmpDist = vpc.distanceTo(coord);
        if (tmpDist < currDist) {
            currDist = tmpDist;
            ret = vpc;
        }
    }
    //verify this part
    if( onEntity && !ret.isInWindowOrdered(minV,maxV) ){
        //projection point not within range, find the nearest endpoint
        ret = getNearestEndpoint(coord,dist);
        currDist = ret.distanceTo(coord);
    }

    if (dist) {
        *dist = currDist;
    }

    return ret;
}



FAS_Vector FAS_Solid::getNearestCenter(const FAS_Vector& /*coord*/,
                                       double* dist) const{

    if (dist) {
        *dist = FAS_MAXDOUBLE;
    }

    return FAS_Vector(false);
}



FAS_Vector FAS_Solid::getNearestMiddle(const FAS_Vector& /*coord*/,
                                       double* dist,
                                       const int /*middlePoints*/)const {
    if (dist) {
        *dist = FAS_MAXDOUBLE;
    }
    return FAS_Vector(false);
}



FAS_Vector FAS_Solid::getNearestDist(double /*distance*/,
                                     const FAS_Vector& /*coord*/,
                                     double* dist) const{
    if (dist) {
        *dist = FAS_MAXDOUBLE;
    }
    return FAS_Vector(false);
}



/**
 * @return Distance from one of the boundry lines of this solid to given point.
 *
 */
double FAS_Solid::getDistanceToPoint(const FAS_Vector& coord,
                                     FAS_Entity** entity,
                                     FAS2::ResolveLevel /*level*/,
                                     double /*solidDist*/)const {
    if (entity) {
        *entity = const_cast<FAS_Solid*>(this);
    }
    double ret;
    getNearestPointOnEntity(coord,true,&ret,entity);
    return ret;
}



void FAS_Solid::move(const FAS_Vector& offset) {
    for (int i=0; i<4; ++i) {
        if(data.corner[i].valid)
            data.corner[i].move(offset);
    }
    calculateBorders();
}



//rotation
void FAS_Solid::rotate(const FAS_Vector& center, const double& angle) {
    FAS_Vector angleVector(angle);
    for (int i=0; i<4; ++i) {
        if(data.corner[i].valid)
            data.corner[i].rotate(center, angleVector);
    }
    calculateBorders();
}

void FAS_Solid::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    for (int i=0; i<4; ++i) {
        if(data.corner[i].valid)
            data.corner[i].rotate(center, angleVector);
    }
    calculateBorders();
}



void FAS_Solid::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    for (int i=0; i<4; ++i) {
        if(data.corner[i].valid)
            data.corner[i].scale(center, factor);
    }
    calculateBorders();
}



void FAS_Solid::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    for (int i=0; i<4; ++i) {
        if(data.corner[i].valid)
            data.corner[i].mirror(axisPoint1, axisPoint2);
    }
    calculateBorders();
}


void FAS_Solid::draw(FAS_Painter* painter, FAS_GraphicView* view,
                     double& /*patternOffset*/) {

    if (!(painter && view)) {
        return;
    }
COMMONDEF->drawEntityCount++;
    //    FAS_SolidData d = getData();
    painter->fillTriangle(view->toGui(getCorner(0)),
                          view->toGui(getCorner(1)),
                          view->toGui(getCorner(2)));
    if (!isTriangle()) {
        painter->fillTriangle(view->toGui(getCorner(1)),
                              view->toGui(getCorner(2)),
                              view->toGui(getCorner(3)));
    }

}
void FAS_Solid::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center1, const double& angle,const FAS_Vector& factor){

    if(!data.bkBasicData.valid){
        data.bkBasicData=FAS_Vector(true);
        data.bkcorner=data.corner;

    }else{
        data.corner=data.bkcorner;
    }
}

void FAS_Solid::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){
    for (int i=0; i<4; ++i) {
        if(data.corner[i].valid)
            data.corner[i].move(offset);
    }
    FAS_Vector angleVector(angle);
    for (int i=0; i<4; ++i) {
        if(data.corner[i].valid)
            data.corner[i].rotate(center, angleVector);
    }
    for (int i=0; i<4; ++i) {
        if(data.corner[i].valid)
            data.corner[i].scale(center, factor);
    }

}
/**
 * Dumps the point's data to stdout.
 */
std::ostream& operator << (std::ostream& os, const FAS_Solid& p) {
    os << " Solid: " << p.getData() << "\n";
    return os;
}

