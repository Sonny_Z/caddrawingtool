/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_POINT_H
#define FAS_POINT_H

#include "fas_atomicentity.h"

// Holds the data that defines a point.
struct FAS_PointData
{
    FAS_PointData(const FAS_Vector& pos): pos(pos) {}
    friend std::ostream& operator << (std::ostream& os, const FAS_PointData& pd);
    FAS_Vector pos;
    FAS_Vector bkpos;
};

class FAS_Point : public FAS_AtomicEntity
{
public:
    FAS_Point(FAS_EntityContainer* parent,
              const FAS_PointData& d);

    FAS_Entity* clone() const override;

    FAS2::EntityType rtti() const override;
    FAS_Vector getStartpoint() const override;
    FAS_Vector getEndpoint() const override;
    void moveStartpoint(const FAS_Vector& pos) override;
    FAS_PointData getData() const;
    FAS_VectorSolutions getRefPoints() const override;
    //return Position of the point
    FAS_Vector getPos() const;
    // Sets a new position for this point.
    void setPos(const FAS_Vector& pos);
    FAS_Vector getCenter() const override;
    double getRadius() const override;
    bool isTangent(const FAS_CircleData& circleData) const override;
    FAS_Vector getMiddlePoint(void)const override;
    FAS_Vector getNearestEndpoint(const FAS_Vector& coord, double* dist = nullptr)const override;
    FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                       bool onEntity = true,
                                       double* dist = nullptr,
                                       FAS_Entity** entity = nullptr)const override;
    FAS_Vector getNearestCenter(const FAS_Vector& coord,
                                double* dist = nullptr)const override;
    FAS_Vector getNearestMiddle(const FAS_Vector& coord,
                                double* dist = nullptr,
                                int middlePoints = 1)const override;
    FAS_Vector getNearestDist(double distance,
                              const FAS_Vector& coord,
                              double* dist = nullptr)const override;
    double getDistanceToPoint(const FAS_Vector& coord,
                              FAS_Entity** entity=nullptr,
                              FAS2::ResolveLevel level=FAS2::ResolveNone,
                              double solidDist = FAS_MAXDOUBLE)const override;

    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;

    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;

    friend std::ostream& operator << (std::ostream& os, const FAS_Point& p);

    /** Recalculates the borders of this entity. */
    void calculateBorders () override;
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;


protected:
    FAS_PointData data;
};
#endif
