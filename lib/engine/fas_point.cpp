/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_point.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>

#include "fas_circle.h"
#include "fas_graphicview.h"
#include "fas_painter.h"

FAS_Point::FAS_Point(FAS_EntityContainer* parent,
                   const FAS_PointData& d)
        :FAS_AtomicEntity(parent), data(d)
{
    calculateBorders ();
}

FAS_Entity* FAS_Point::clone() const
{
	FAS_Point* p = new FAS_Point(*this);
	p->initId();
	return p;
}

FAS2::EntityType FAS_Point::rtti() const
{
    return FAS2::EntityPoint;
}

void FAS_Point::calculateBorders ()
{
    minV = maxV = data.pos;
}

FAS_VectorSolutions FAS_Point::getRefPoints() const
{
	return FAS_VectorSolutions{data.pos};
}

FAS_Vector FAS_Point::getStartpoint() const
{
    return data.pos;
}

FAS_Vector FAS_Point::getEndpoint() const
{
    return data.pos;
}

FAS_PointData FAS_Point::getData() const
{
    return data;
}

FAS_Vector FAS_Point::getPos() const
{
    return data.pos;
}

FAS_Vector FAS_Point::getCenter() const
{
    return data.pos;
}

double FAS_Point::getRadius() const
{
    return 0.;
}

bool FAS_Point::isTangent(const FAS_CircleData& circleData) const
{
    double const dist=data.pos.distanceTo(circleData.center);
    return fabs(dist - fabs(circleData.radius))<50.*FAS_TOLERANCE;
}

void FAS_Point::setPos(const FAS_Vector& pos)
{
    data.pos = pos;
}

FAS_Vector FAS_Point::getNearestEndpoint(const FAS_Vector& coord, double* dist)const
{
    if (dist)
    {
        *dist = data.pos.distanceTo(coord);
    }
    return data.pos;
}

FAS_Vector FAS_Point::getNearestPointOnEntity(const FAS_Vector& coord,
        bool /*onEntity*/, double* dist, FAS_Entity** entity) const
{
    if (dist)
    {
        *dist = data.pos.distanceTo(coord);
    }
    if (entity)
    {
        *entity = const_cast<FAS_Point*>(this);
    }
    return data.pos;
}

FAS_Vector FAS_Point::getNearestCenter(const FAS_Vector& coord, double* dist) const
{
    if (dist)
    {
        *dist = data.pos.distanceTo(coord);
    }
    return data.pos;
}

FAS_Vector FAS_Point::getMiddlePoint()const
{
    return data.pos;
}

FAS_Vector FAS_Point::getNearestMiddle(const FAS_Vector& coord,
                                     double* dist,
                                     const int /*middlePoints*/)const
{
    if (dist)
    {
        *dist = data.pos.distanceTo(coord);
    }
    return data.pos;
}

FAS_Vector FAS_Point::getNearestDist(double /*distance*/,
                                   const FAS_Vector& /*coord*/,
                                   double* dist) const
{
    if (dist)
    {
        *dist = FAS_MAXDOUBLE;
    }
    return FAS_Vector(false);
}

double FAS_Point::getDistanceToPoint(const FAS_Vector& coord,
                                     FAS_Entity** entity,
                                     FAS2::ResolveLevel /*level*/,
                                     double /*solidDist*/)const
{
    if (entity)
    {
        *entity = const_cast<FAS_Point*>(this);
    }
    return data.pos.distanceTo(coord);
}

void FAS_Point::moveStartpoint(const FAS_Vector& pos)
{
    data.pos = pos;
    calculateBorders();
}

void FAS_Point::move(const FAS_Vector& offset) {
    data.pos.move(offset);
    calculateBorders();
}

void FAS_Point::rotate(const FAS_Vector& center, const double& angle)
{
    data.pos.rotate(center, angle);
    calculateBorders();
}

void FAS_Point::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    data.pos.rotate(center, angleVector);
    calculateBorders();
}

void FAS_Point::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    data.pos.scale(center, factor);
    calculateBorders();
}

void FAS_Point::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    data.pos.mirror(axisPoint1, axisPoint2);
    calculateBorders();
}

void FAS_Point::draw(FAS_Painter* painter,FAS_GraphicView* view, double& /*patternOffset*/)
{
    if (painter==nullptr || view==nullptr)
    {
        return;
    }
    COMMONDEF->drawEntityCount++;
    painter->drawPoint(view->toGui(getPos()));
}

// Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_Point& p)
{
    os << " Point: " << p.getData() << "\n";
    return os;
}

std::ostream& operator << (std::ostream& os, const FAS_PointData& pd)
{
    os << "(" << pd.pos << ")";
    return os;
}

void FAS_Point::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) {

    FAS_Vector bkpos1=data.bkpos;
    if(!bkpos1.valid){
        data.bkpos=data.pos;
    }else{
        data.pos=data.bkpos;
    }
}
void FAS_Point::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) {

    data.pos.move(offset);
    data.pos.rotate(center, angle);
    data.pos.scale(center, factor);
}


// EOF
