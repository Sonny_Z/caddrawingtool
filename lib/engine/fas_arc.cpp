/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_arc.h"

//sort with alphabetical order
#include <cmath>
#include "fas_graphicview.h"
#include "fas_information.h"
#include "fas_line.h"
#include "fas_linetypepattern.h"
#include "fas_math.h"
#include "fas_painterqt.h"
#include "lc_quadratic.h"
#include "lc_rect.h"
#include <QDebug>
FAS_ArcData::FAS_ArcData(const FAS_Vector& _center,
                         double _radius,
                         double _angle1, double _angle2,
                         bool _reversed):
    center(_center)
  ,radius(_radius)
  ,angle1(_angle1)
  ,angle2(_angle2)
  ,reversed(_reversed)
{
}

void FAS_ArcData::reset()
{
    center = FAS_Vector(false);
    radius = 0.0;
    angle1 = 0.0;
    angle2 = 0.0;
    reversed = false;
}

bool FAS_ArcData::isValid() const
{
    return (center.valid && radius>FAS_TOLERANCE &&
            fabs(remainder(angle1-angle2, 2.*M_PI))>FAS_TOLERANCE_ANGLE);
}

std::ostream& operator << (std::ostream& os, const FAS_ArcData& ad)
{
    os << "(" << ad.center <<
          "/" << ad.radius <<
          " " << ad.angle1 <<
          "," << ad.angle2 <<
          ")";
    return os;
}
/**
 * Default constructor.
 */
FAS_Arc::FAS_Arc(FAS_EntityContainer* parent,
                 const FAS_ArcData& d)
    : FAS_AtomicEntity(parent), data(d)
{
    calculateBorders();
}

FAS_Entity* FAS_Arc::clone() const
{
    FAS_Arc* a = new FAS_Arc(*this);
    a->initId();
    return a;
}

// Creates this arc from 3 given points which define the arc line.
bool FAS_Arc::createFrom3P(const FAS_Vector& p1, const FAS_Vector& p2,
                           const FAS_Vector& p3)
{
    FAS_Vector vra=p2 - p1;
    FAS_Vector vrb=p3 - p1;
    double ra2=vra.squared()*0.5;
    double rb2=vrb.squared()*0.5;
    double crossp=vra.x * vrb.y - vra.y * vrb.x;
    if (fabs(crossp)< FAS_TOLERANCE2)
    {
        return false;
    }
    crossp=1./crossp;
    data.center.set((ra2*vrb.y - rb2*vra.y)*crossp,(rb2*vra.x - ra2*vrb.x)*crossp);
    data.radius=data.center.magnitude();
    data.center += p1;
    data.angle1=data.center.angleTo(p1);
    data.angle2=data.center.angleTo(p3);
    data.reversed = FAS_Math::isAngleBetween(data.center.angleTo(p2),
                                             data.angle1, data.angle2, true);
    return true;
}


// Creates an arc from its startpoint, endpoint, start direction (angle) and radius.
bool FAS_Arc::createFrom2PDirectionRadius(const FAS_Vector& startPoint,
                                          const FAS_Vector& endPoint,
                                          double direction1, double radius)
{
    FAS_Vector ortho = FAS_Vector::polar(radius, direction1 + M_PI_2);
    FAS_Vector center1 = startPoint + ortho;
    FAS_Vector center2 = startPoint - ortho;

    if (center1.distanceTo(endPoint) < center2.distanceTo(endPoint)) {
        data.center = center1;
    } else {
        data.center = center2;
    }

    data.radius = radius;
    data.angle1 = data.center.angleTo(startPoint);
    data.angle2 = data.center.angleTo(endPoint);
    data.reversed = false;

    double diff = FAS_Math::correctAngle(getDirection1()-direction1);
    if (fabs(diff-M_PI)<1.0e-1)
    {
        data.reversed = true;
    }
    calculateBorders();

    return true;
}

/**
 * Creates an arc from its startpoint, endpoint, start direction (angle)
 * and angle length.
 */
bool FAS_Arc::createFrom2PDirectionAngle(const FAS_Vector& startPoint,
                                         const FAS_Vector& endPoint,
                                         double direction1, double angleLength)
{
    if (angleLength <= FAS_TOLERANCE_ANGLE || angleLength > 2. * M_PI - FAS_TOLERANCE_ANGLE) return false;
    FAS_Line l0 {nullptr, startPoint, startPoint - FAS_Vector{direction1}};
    double const halfA = 0.5 * angleLength;
    l0.rotate(startPoint, halfA);

    double d0;
    FAS_Vector vEnd0 = l0.getNearestPointOnEntity(endPoint, false, &d0);
    FAS_Line l1 = l0;
    l1.rotate(startPoint, -angleLength);
    double d1;
    FAS_Vector vEnd1 = l1.getNearestPointOnEntity(endPoint, false, &d1);
    if (d1 < d0) {
        vEnd0 = vEnd1;
        l0 = l1;
    }

    l0.rotate((startPoint + vEnd0) * 0.5, M_PI_2);

    l1 = FAS_Line{nullptr, startPoint, startPoint + FAS_Vector{direction1 + M_PI_2}};

    auto const sol = FAS_Information::getIntersection(&l0, &l1, false);
    if (sol.size()==0) return false;

    data.center = sol.at(0);

    data.radius = data.center.distanceTo(startPoint);
    data.angle1 = data.center.angleTo(startPoint);
    data.reversed = false;

    double diff = FAS_Math::correctAngle(getDirection1()-direction1);
    if (fabs(diff-M_PI)<1.0e-1) {
        data.angle2 = FAS_Math::correctAngle(data.angle1 -angleLength);
        data.reversed = true;
    }else{
        data.angle2 = FAS_Math::correctAngle(data.angle1 +angleLength);
    }
    calculateBorders();

    return true;
}

// Creates an arc from its startpoint, endpoint and bulge.
bool FAS_Arc::createFrom2PBulge(const FAS_Vector& startPoint, const FAS_Vector& endPoint,
                                double bulge)
{
    data.reversed = (bulge<0.0);
    double alpha = atan(bulge)*4.0;

    FAS_Vector middle = (startPoint+endPoint)/2.0;
    double dist = startPoint.distanceTo(endPoint)/2.0;

    // alpha can't be 0.0 at this point
    data.radius = fabs(dist / sin(alpha/2.0));

    double wu = fabs(FAS_Math::pow(data.radius, 2.0) - FAS_Math::pow(dist, 2.0));
    double h = sqrt(wu);
    double angle = startPoint.angleTo(endPoint);

    if (bulge>0.0) {
        angle+=M_PI_2;
    } else {
        angle-=M_PI_2;
    }

    if (fabs(alpha)>M_PI) {
        h*=-1.0;
    }

    data.center.setPolar(h, angle);
    data.center+=middle;
    data.angle1 = data.center.angleTo(startPoint);
    data.angle2 = data.center.angleTo(endPoint);

    calculateBorders();

    return true;
}

void FAS_Arc::calculateBorders()
{
    FAS_Vector const startpoint = data.center + FAS_Vector::polar(data.radius, data.angle1);
    FAS_Vector const endpoint = data.center + FAS_Vector::polar(data.radius, data.angle2);
    LC_Rect const rect{startpoint, endpoint};

    double minX = rect.lowerLeftCorner().x;
    double minY = rect.lowerLeftCorner().y;
    double maxX = rect.upperRightCorner().x;
    double maxY = rect.upperRightCorner().y;

    double a1 = isReversed() ? data.angle2 : data.angle1;
    double a2 = isReversed() ? data.angle1 : data.angle2;
    if ( FAS_Math::isAngleBetween(0.5*M_PI,a1,a2,false) ) {
        maxY = data.center.y + data.radius;
    }
    if ( FAS_Math::isAngleBetween(1.5*M_PI,a1,a2,false) ) {
        minY = data.center.y - data.radius;
    }
    if ( FAS_Math::isAngleBetween(M_PI,a1,a2,false) ) {
        minX = data.center.x - data.radius;
    }
    if ( FAS_Math::isAngleBetween(0.,a1,a2,false) ) {
        maxX = data.center.x + data.radius;
    }

    minV.set(minX, minY);
    maxV.set(maxX, maxY);
}


FAS_Vector FAS_Arc::getStartpoint() const
{
    return data.center + FAS_Vector::polar(data.radius, data.angle1);
}

// End point of the entity. */
FAS_Vector FAS_Arc::getEndpoint() const
{
    return data.center + FAS_Vector::polar(data.radius, data.angle2);
}

FAS_VectorSolutions FAS_Arc::getRefPoints() const
{
    //order: start, end, center
    return {getStartpoint(), getEndpoint(), data.center};
}

double FAS_Arc::getDirection1() const {
    if (!data.reversed) {
        return FAS_Math::correctAngle(data.angle1+M_PI_2);
    }
    else {
        return FAS_Math::correctAngle(data.angle1-M_PI_2);
    }
}
// @return  the angle at which the arc starts at the endpoint.
double FAS_Arc::getDirection2() const
{
    if (!data.reversed) {
        return FAS_Math::correctAngle(data.angle2-M_PI_2);
    }
    else {
        return FAS_Math::correctAngle(data.angle2+M_PI_2);
    }
}

FAS_Vector FAS_Arc::getNearestEndpoint(const FAS_Vector& coord, double* dist) const
{
    double dist1, dist2;

    auto const startpoint = getStartpoint();
    auto const endpoint = getEndpoint();

    dist1 = coord.squaredTo(startpoint);
    dist2 = coord.squaredTo(endpoint);

    if (dist2<dist1) {
        if (dist)
            *dist = sqrt(dist2);

        return endpoint;
    } else {
        if (dist)
            *dist = sqrt(dist1);

        return startpoint;
    }

}

/**
  *find the tangential points from a given point, i.e., the tangent lines should pass
  * the given point and tangential points
  */
FAS_VectorSolutions FAS_Arc::getTangentPoint(const FAS_Vector& point) const
{
    FAS_VectorSolutions ret;
    double r2(getRadius()*getRadius());
    if(r2<FAS_TOLERANCE2) return ret; //circle too small
    FAS_Vector vp(point-getCenter());
    double c2(vp.squared());
    if(c2<r2-getRadius()*2.*FAS_TOLERANCE) {
        //inside point, no tangential point
        return ret;
    }
    if(c2>r2+getRadius()*2.*FAS_TOLERANCE) {
        //external point
        FAS_Vector vp1(-vp.y,vp.x);
        vp1*=getRadius()*sqrt(c2-r2)/c2;
        vp *= r2/c2;
        vp += getCenter();
        if(vp1.squared()>FAS_TOLERANCE2) {
            ret.push_back(vp+vp1);
            ret.push_back(vp-vp1);
            return ret;
        }
    }
    ret.push_back(point);
    return ret;
}

FAS_Vector FAS_Arc::getTangentDirection(const FAS_Vector& point) const
{
    FAS_Vector vp(point-getCenter());
    return FAS_Vector(-vp.y,vp.x);

}

FAS_Vector FAS_Arc::getNearestPointOnEntity(const FAS_Vector& coord,
                                            bool onEntity, double* dist, FAS_Entity** entity) const
{

    FAS_Vector vec(false);
    if (entity) {
        *entity = const_cast<FAS_Arc*>(this);
    }

    double angle = (coord-data.center).angle();
    if ( ! onEntity || FAS_Math::isAngleBetween(angle,
                                                data.angle1, data.angle2, isReversed())) {
        vec.setPolar(data.radius, angle);
        vec+=data.center;
    } else {
        return vec=getNearestEndpoint(coord, dist);
    }
    if (dist) {
        *dist = vec.distanceTo(coord);
    }

    return vec;
}

FAS_Vector FAS_Arc::getNearestCenter(const FAS_Vector& coord,
                                     double* dist) const{
    if (dist) {
        *dist = coord.distanceTo(data.center);
    }
    return data.center;
}

// get the nearest equidistant middle points
FAS_Vector FAS_Arc::getNearestMiddle(const FAS_Vector& coord, double* dist, int middlePoints                              )const
{
    double amin = getAngle1();
    double amax = getAngle2();
    if( !(std::isnormal(amin) || std::isnormal(amax)))
    {
        //whole circle, no middle point
        if(dist)
        {
            *dist = FAS_MAXDOUBLE;
        }
        return FAS_Vector(false);
    }
    if(isReversed())
    {
        std::swap(amin,amax);
    }
    double da = fmod(amax-amin+2.*M_PI, 2.*M_PI);
    if ( da < FAS_TOLERANCE )
    {
        da= 2.*M_PI; // whole circle
    }
    FAS_Vector vp(getNearestPointOnEntity(coord,true,dist));
    double angle = getCenter().angleTo(vp);
    int counts = middlePoints+1;
    int i( static_cast<int>(fmod(angle-amin+2.*M_PI,2.*M_PI)/da*counts+0.5));
    if(!i) i++; // remove end points
    if(i==counts) i--;
    angle=amin + da*(double(i)/double(counts));
    vp.setPolar(getRadius(), angle);
    vp.move(getCenter());

    if (dist)
    {
        *dist = vp.distanceTo(coord);
    }
    return vp;
}

FAS_Vector FAS_Arc::getNearestDist(double distance, const FAS_Vector& coord, double* dist) const
{
    if (data.radius<FAS_TOLERANCE) {
        if (dist)
            *dist = FAS_MAXDOUBLE;

        return {};
    }

    double aDist = distance / data.radius;
    if (isReversed()) aDist= -aDist;
    double a;

    if(coord.distanceTo(getStartpoint()) < coord.distanceTo(getEndpoint()))
        a=getAngle1() + aDist;
    else
        a=getAngle2() - aDist;

    FAS_Vector ret = FAS_Vector::polar(data.radius, a);
    ret += getCenter();

    return ret;
}

FAS_Vector FAS_Arc::getNearestDist(double distance, bool startp) const
{
    if (data.radius < FAS_TOLERANCE) {
        return {};
    }

    double a;
    double aDist = distance / data.radius;

    if (isReversed()) {
        if (startp) {
            a = data.angle1 - aDist;
        } else {
            a = data.angle2 + aDist;
        }
    } else {
        if (startp) {
            a = data.angle1 + aDist;
        } else {
            a = data.angle2 - aDist;
        }
    }

    FAS_Vector p = FAS_Vector::polar(data.radius, a);
    p += data.center;

    return p;
}


FAS_Vector FAS_Arc::getNearestOrthTan(const FAS_Vector& coord, const FAS_Line& normal, bool onEntity ) const
{
    if ( !coord.valid ) {
        return FAS_Vector(false);
    }
    double angle=normal.getAngle1();
    FAS_Vector vp = FAS_Vector::polar(getRadius(),angle);
    std::vector<FAS_Vector> sol;
    for(int i=0; i <= 1; i++)
    {
        if(!onEntity || FAS_Math::isAngleBetween(angle,getAngle1(),getAngle2(),isReversed()))
        {
            if (i)
                sol.push_back(- vp);
            else
                sol.push_back(vp);
        }
        angle=FAS_Math::correctAngle(angle+M_PI);
    }
    switch(sol.size())
    {
    case 0:
        return FAS_Vector(false);
    case 2:
        if( FAS_Vector::dotP(sol[1],coord-getCenter())>0.)
        {
            vp=sol[1];
            break;
        }
    default:
        vp=sol[0];
    }
    return getCenter() + vp;
}

void FAS_Arc::moveStartpoint(const FAS_Vector& pos) {
    // polyline arcs: move point not angle:
    double bulge = getBulge();
    if(fabs(bulge - M_PI_2) < FAS_TOLERANCE_ANGLE)
        return;

    createFrom2PBulge(pos, getEndpoint(), bulge);
    correctAngles(); // make sure angleLength is no more than 2*M_PI
}

void FAS_Arc::moveEndpoint(const FAS_Vector& pos) {
    double bulge = getBulge();
    createFrom2PBulge(getStartpoint(), pos, bulge);
    correctAngles(); // make sure angleLength is no more than 2*M_PI
}

// this function creates offset
// coord, position indicates the direction of offset
// distance, distance of offset
bool FAS_Arc::offset(const FAS_Vector& coord, const double& distance)
{
    double r0(coord.distanceTo(getCenter()));
    if(r0 > getRadius())
    {
        r0 = getRadius()+ fabs(distance);
    }
    else
    {
        r0 = getRadius()- fabs(distance);
        if(r0<FAS_TOLERANCE)
        {
            return false;
        }
    }
    setRadius(r0);
    calculateBorders();
    return true;
}
std::vector<FAS_Entity* > FAS_Arc::offsetTwoSides(const double& distance) const
{
    std::vector<FAS_Entity*> ret(0,nullptr);
    ret.push_back(new FAS_Arc(nullptr,FAS_ArcData(getCenter(),getRadius()+distance,getAngle1(),getAngle2(),isReversed())));
    if(getRadius()>distance)
        ret.push_back(new FAS_Arc(nullptr,FAS_ArcData(getCenter(),getRadius()-distance,getAngle1(),getAngle2(),isReversed())));
    return ret;
}

// revert the direction of an atomic entity
void FAS_Arc::revertDirection()
{
    std::swap(data.angle1,data.angle2);
    data.reversed = ! data.reversed;
}

// make sure angleLength() is not more than 2*M_PI
void FAS_Arc::correctAngles()
{
    double *pa1= & data.angle1;
    double *pa2= & data.angle2;
    if (isReversed())
        std::swap(pa1, pa2);
    *pa2 = *pa1 + fmod(*pa2 - *pa1, 2.*M_PI);
    if ( fabs(getAngleLength()) < FAS_TOLERANCE_ANGLE )
        *pa2 += 2.*M_PI;
}

void FAS_Arc::trimStartpoint(const FAS_Vector& pos) {
    data.angle1 = data.center.angleTo(pos);
    correctAngles(); // make sure angleLength is no more than 2*M_PI
    calculateBorders();
}

void FAS_Arc::trimEndpoint(const FAS_Vector& pos) {
    data.angle2 = data.center.angleTo(pos);
    correctAngles(); // make sure angleLength is no more than 2*M_PI
    calculateBorders();
}

FAS2::Ending FAS_Arc::getTrimPoint(const FAS_Vector& trimCoord,
                                   const FAS_Vector& /*trimPoint*/)
{

    double angMouse = data.center.angleTo(trimCoord);
    if( fabs(remainder(angMouse-data.angle1, 2.*M_PI))< fabs(remainder(angMouse-data.angle2, 2.*M_PI)))
        return FAS2::EndingStart;
    else
        return FAS2::EndingEnd;
}

FAS_Vector FAS_Arc::prepareTrim(const FAS_Vector& trimCoord, const FAS_VectorSolutions& trimSol)
{
    if( ! trimSol.hasValid() ) return (FAS_Vector(false));
    if( trimSol.getNumber() == 1 ) return (trimSol.get(0));
    double am=getArcAngle(trimCoord);
    std::vector<double> ias;
    double ia(0.),ia2(0.);
    FAS_Vector is,is2;
    for(size_t ii=0; ii<trimSol.getNumber(); ++ii)
    {
        //find closest according ellipse angle
        ias.push_back(getArcAngle(trimSol.get(ii)));
        if( !ii ||  fabs( remainder( ias[ii] - am, 2*M_PI)) < fabs( remainder( ia -am, 2*M_PI)) )
        {
            ia = ias[ii];
            is = trimSol.get(ii);
        }
    }
    std::sort(ias.begin(),ias.end());
    for(size_t ii=0; ii<trimSol.getNumber(); ++ii) { //find segment to enclude trimCoord
        if ( ! FAS_Math::isSameDirection(ia,ias[ii],FAS_TOLERANCE))
            continue;
        if( FAS_Math::isAngleBetween(am,ias[(ii+trimSol.getNumber()-1)% trimSol.getNumber()],ia,false))
        {
            ia2=ias[(ii+trimSol.getNumber()-1)% trimSol.getNumber()];
        }
        else
        {
            ia2=ias[(ii+1)% trimSol.getNumber()];
        }
        break;
    }
    for(const FAS_Vector& vp: trimSol)
    {
        //find segment to enclude trimCoord
        if ( ! FAS_Math::isSameDirection(ia2,getArcAngle(vp),FAS_TOLERANCE))
            continue;
        is2=vp;
        break;
    }
    double dia=fabs(remainder(ia-am,2*M_PI));
    double dia2=fabs(remainder(ia2-am,2*M_PI));
    double ai_min=std::min(dia,dia2);
    double da1=fabs(remainder(getAngle1()-am,2*M_PI));
    double da2=fabs(remainder(getAngle2()-am,2*M_PI));
    double da_min=std::min(da1,da2);
    if( da_min < ai_min )
    {
        //trimming one end of arc
        bool irev= FAS_Math::isAngleBetween(am,ia2,ia, isReversed()) ;
        if ( FAS_Math::isAngleBetween(ia,getAngle1(),getAngle2(), isReversed()) &&
             FAS_Math::isAngleBetween(ia2,getAngle1(),getAngle2(), isReversed()) )
        {
            if(irev)
            {
                setAngle2(ia);
                setAngle1(ia2);
            }
            else
            {
                setAngle1(ia);
                setAngle2(ia2);
            }
            da1=fabs(remainder(getAngle1()-am,2*M_PI));
            da2=fabs(remainder(getAngle2()-am,2*M_PI));
        }
        if( ((da1 < da2) && (FAS_Math::isAngleBetween(ia2,ia,getAngle1(),isReversed()))) ||
                ((da1 > da2) && (FAS_Math::isAngleBetween(ia2,getAngle2(),ia,isReversed())))
                )
        {
            std::swap(is,is2);
        }
    } else {
        //choose intersection as new end
        if( dia > dia2)
        {
            std::swap(is,is2);
            std::swap(ia,ia2);
        }
        if(FAS_Math::isAngleBetween(ia,getAngle1(),getAngle2(),isReversed())) {
            if(FAS_Math::isAngleBetween(am,getAngle1(),ia,isReversed()))
            {
                setAngle2(ia);
            }
            else
            {
                setAngle1(ia);
            }
        }
    }
    return is;
}

void FAS_Arc::reverse()
{
    std::swap(data.angle1,data.angle2);
    data.reversed = !data.reversed;
}


void FAS_Arc::move(const FAS_Vector& offset)
{
    data.center.move(offset);
    moveBorders(offset);
}

void FAS_Arc::rotate(const FAS_Vector& center, const double& angle)
{
    data.center.rotate(center, angle);
    data.angle1 = FAS_Math::correctAngle(data.angle1+angle);
    data.angle2 = FAS_Math::correctAngle(data.angle2+angle);
    calculateBorders();
}

void FAS_Arc::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    data.center.rotate(center, angleVector);
    double angle(angleVector.angle());
    data.angle1 = FAS_Math::correctAngle(data.angle1+angle);
    data.angle2 = FAS_Math::correctAngle(data.angle2+angle);
    calculateBorders();
}

void FAS_Arc::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    // negative scaling: mirroring
    if (factor.x<0.0)
    {
        mirror(data.center, data.center + FAS_Vector(0.0, 1.0));
    }
    if (factor.y<0.0)
    {
        mirror(data.center, data.center + FAS_Vector(1.0, 0.0));
    }

    data.center.scale(center, factor);
    data.radius *= factor.x;
    data.radius = fabs( data.radius );
    calculateBorders();
}

void FAS_Arc::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    data.center.mirror(axisPoint1, axisPoint2);
    setReversed( ! isReversed() );
    double a= (axisPoint2 - axisPoint1).angle()*2;
    setAngle1(FAS_Math::correctAngle(a - getAngle1()));
    setAngle2(FAS_Math::correctAngle(a - getAngle2()));
    correctAngles(); // make sure angleLength is no more than 2*M_PI
    calculateBorders();
}

void FAS_Arc::moveRef(const FAS_Vector& ref, const FAS_Vector& offset)
{
    if (fabs(fabs(getAngleLength()-M_PI)-M_PI) < FAS_TOLERANCE_ANGLE)
    {
        move(offset);
        return;
    }
    auto const refs = getRefPoints();
    double dMin;
    size_t index;
    FAS_Vector const vp = refs.getClosest(ref, &dMin, &index);
    if (dMin >= 1.0e-4)
        return;

    //reference points must be by the order: start, end, center
    switch (index) {
    case 0:
        moveStartpoint(vp + offset);
        return;
    case 1:
        moveEndpoint(vp + offset);
        return;
    default:
        move(offset);
    }

    correctAngles(); // make sure angleLength is no more than 2*M_PI
}


void FAS_Arc::stretch(const FAS_Vector& firstCorner,
                      const FAS_Vector& secondCorner,
                      const FAS_Vector& offset) {

    if (getMin().isInWindow(firstCorner, secondCorner) &&
            getMax().isInWindow(firstCorner, secondCorner))
    {
        move(offset);
    }
    else
    {
        if (getStartpoint().isInWindow(firstCorner,
                                       secondCorner))
        {
            moveStartpoint(getStartpoint() + offset);
        }
        if (getEndpoint().isInWindow(firstCorner,
                                     secondCorner))
        {
            moveEndpoint(getEndpoint() + offset);
        }
    }
    correctAngles(); // make sure angleLength is no more than 2*M_PI
}

// find the visible part of the arc, and call drawVisible() to draw
void FAS_Arc::draw(FAS_Painter* painter, FAS_GraphicView* view,
                   double& patternOffset)
{


    if (!( painter && view))
        return;
COMMONDEF->drawEntityCount++;
    //only draw the visible portion of line
    FAS_Vector vpMin(view->toGraph(0,view->getHeight()));
    FAS_Vector vpMax(view->toGraph(view->getWidth(),0));
    QPolygonF visualBox(QRectF(vpMin.x,vpMin.y,vpMax.x-vpMin.x, vpMax.y-vpMin.y));

    FAS_Vector vpStart(isReversed()?getEndpoint():getStartpoint());
    FAS_Vector vpEnd(isReversed()?getStartpoint():getEndpoint());

    std::vector<FAS_Vector> vertex(0);
    for(unsigned short i=0;i<4;i++)
    {
        const QPointF& vp(visualBox.at(i));
        vertex.push_back(FAS_Vector(vp.x(),vp.y()));
    }
    /** angles at cross points */
    std::vector<double> crossPoints(0);

    double baseAngle=isReversed()? getAngle2():getAngle1();
    for(unsigned short i=0;i<4;i++)
    {
        FAS_Line line{vertex.at(i),vertex.at((i+1)%4)};
        auto vpIts = FAS_Information::getIntersection(static_cast<FAS_Entity*>(this), &line, true);
        if( vpIts.size()==0) continue;
        for(const FAS_Vector& vp: vpIts)
        {
            auto ap1=getTangentDirection(vp).angle();
            auto ap2=line.getTangentDirection(vp).angle();
            //ignore tangent points, because the arc doesn't cross over
            if( fabs( remainder(ap2 - ap1, M_PI) ) < FAS_TOLERANCE_ANGLE)
                continue;
            crossPoints.push_back(
                        FAS_Math::getAngleDifference(baseAngle, getCenter().angleTo(vp))
                        );
        }
    }
    if(vpStart.isInWindowOrdered(vpMin, vpMax)) crossPoints.push_back(0.);
    if(vpEnd.isInWindowOrdered(vpMin, vpMax)) crossPoints.push_back(getAngleLength());

    //sorting
    std::sort(crossPoints.begin(),crossPoints.end());
    //draw visible
    FAS_Arc arc(*this);
    arc.setPen(getPen());
    arc.setSelected(isSelected());
    arc.setReversed(false);
    for(size_t i=1;i<crossPoints.size();i+=2){
        arc.setAngle1(baseAngle+crossPoints[i-1]);
        arc.setAngle2(baseAngle+crossPoints[i]);
        arc.drawVisible(painter,view,patternOffset);
    }

}

// directly draw the arc, assuming the whole arc is within visible window.
void FAS_Arc::drawVisible(FAS_Painter* painter, FAS_GraphicView* view,
                          double& patternOffset) {

    if (!( painter && view)) return;
    //visible in grahic view
    if(isVisibleInWindow(view)==false) return;

    FAS_Vector cp=view->toGui(getCenter());
    double ra=getRadius()*view->getFactor().x;
    double length=getLength()*view->getFactor().x;
    //double styleFactor = getStyleFactor();
    patternOffset -= length;

    if ( !isSelected() && (getPen().getLineType()==FAS2::SolidLine || view->getDrawingMode()==FAS2::ModePreview))
    {
        painter->drawArc(cp,
                         ra,
                         getAngle1(), getAngle2(),
                         isReversed());
        return;
    }

    // Pattern:
    const FAS_LineTypePattern* pat;
    if (isSelected()) {
        pat = &FAS_LineTypePattern::patternSolidLine; //modified
    } else {
        pat = view->getPattern(getPen().getLineType());
    }

    if (!pat || ra<0.5) {//avoid division by zero from small ra
        painter->drawArc(cp, ra,
                         getAngle1(),getAngle2(),
                         isReversed());
        return;
    }

    if (ra<FAS_TOLERANCE_ANGLE){
        return;
    }

    // Pen to draw pattern is always solid:
    FAS_Pen pen = painter->getPen();
    pen.setLineType(FAS2::SolidLine);
    painter->setPen(pen);

    // create scaled pattern:
    if(pat->num <= 0)
    {
        //invalid pattern
        painter->drawArc(cp,
                         ra,
                         getAngle1(), getAngle2(),
                         isReversed());
        return;
    }
    std::vector<double> da(pat->num);
    double patternSegmentLength(pat->totalLength);
    double ira=1./ra;
    double dpmm=static_cast<FAS_PainterQt*>(painter)->getDpmm();
    for (size_t i=0; i<pat->num; i++)
    {
        da[i] =dpmm*(isReversed() ? -fabs(pat->pattern[i]):fabs(pat->pattern[i]));
        if ( fabs(da[i]) < 1.) da[i] = copysign(1., da[i]);
        da[i] *= ira;
    }

    double total=remainder(patternOffset-0.5*patternSegmentLength,patternSegmentLength)-0.5*patternSegmentLength;

    double a1{FAS_Math::correctAngle(getAngle1())};
    double a2{FAS_Math::correctAngle(getAngle2())};

    if(isReversed()) {//always draw from a1 to a2, so, patternOffset is is automatic
        if(a1<a2+FAS_TOLERANCE_ANGLE) a2 -= 2.*M_PI;
        total = a1 - total*ira; //in angle
    }else{
        if(a2<a1+FAS_TOLERANCE_ANGLE) a2 += 2.*M_PI;
        total = a1 + total*ira; //in angle
    }
    double limit(fabs(a1-a2));
    double t2;

    for(int j=0; fabs(total-a1) < limit; j=(j+1)%pat->num)
    {
        t2 = total + da[j];

        if(pat->pattern[j] > 0.0 && fabs(t2-a2) < limit)
        {
            double a11=(fabs(total-a2) < limit)?total:a1;
            double a21=(fabs(t2-a1) < limit)?t2:a2;
            painter->drawArc(cp, ra, a11, a21, isReversed());
        }
        total = t2;
    }
}

// return Middle point of the entity.
FAS_Vector FAS_Arc::getMiddlePoint() const
{
    double a=getAngle1();
    double b=getAngle2();

    if (isReversed())
    {
        a =b+ FAS_Math::correctAngle(a-b)*0.5;
    }
    else
    {
        a += FAS_Math::correctAngle(b-a)*0.5;
    }
    FAS_Vector ret(a);
    return getCenter() + ret*getRadius();
}



// return Angle length in rad.
double FAS_Arc::getAngleLength() const {
    double ret;
    double a=getAngle1();
    double b=getAngle2();

    if (isReversed()) std::swap(a,b);
    ret = FAS_Math::correctAngle(b-a);
    // full circle:
    if (fabs(remainder(ret,2.*M_PI))<FAS_TOLERANCE_ANGLE)
    {
        ret = 2*M_PI;
    }

    return ret;
}

// return Length of the arc.
double FAS_Arc::getLength() const
{
    return getAngleLength()*data.radius;
}

// Gets the arc's bulge (tangens of angle length divided by 4).
double FAS_Arc::getBulge() const
{
    double bulge = tan(fabs(getAngleLength())/4.0);
    if (isReversed()) {
        bulge*=-1;
    }
    return bulge;
}

/* return the equation of the entity
for quadratic,

return a vector contains:
m0 x^2 + m1 xy + m2 y^2 + m3 x + m4 y + m5 =0

for linear:
m0 x + m1 y + m2 =0
*/
LC_Quadratic FAS_Arc::getQuadratic() const
{
    std::vector<double> ce(6,0.);
    ce[0]=1.;
    ce[2]=1.;
    ce[5]=-data.radius*data.radius;
    LC_Quadratic ret(ce);
    ret.move(data.center);
    return ret;
}

double FAS_Arc::areaLineIntegral() const
{
    const double& r=data.radius;
    const double& a0=data.angle1;
    const double& a1=data.angle2;
    const double r2=0.25*r*r;
    const double fStart=data.center.x*r*sin(a0)+r2*sin(a0+a0);
    const double fEnd=data.center.x*r*sin(a1)+r2*sin(a1+a1);
    return (isReversed()?fStart-fEnd:fEnd-fStart) + 2.*r2*getAngleLength();
}
void FAS_Arc::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center1, const double& angle,const FAS_Vector& factor){


    FAS_Vector center=getCenter();
    double radius=getRadius();
    double angle1=getAngle1();
    double angle2=getAngle2();
    FAS_Vector bkcenter=data.bkcenter;
    if(!bkcenter.valid){
        data.bkcenter=center;
        data.bkradius=radius;
        data.bkangle1=angle1;
        data.bkangle2=angle2;
        data.bkreversed=data.reversed;
    }else{
        data.center=data.bkcenter;
        data.radius=data.bkradius;
        data.angle1=data.bkangle1;
        data.angle2=data.bkangle2;
        data.reversed=data.bkreversed;
    }
}

void FAS_Arc::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){
        data.center.move(offset);
        data.center.rotate(center, angle);
        data.angle1 = FAS_Math::correctAngle(data.angle1+angle);
        data.angle2 = FAS_Math::correctAngle(data.angle2+angle);


        if (factor.x<0.0)
        {
            FAS_Vector axisPoint1=data.center;
            FAS_Vector axisPoint2=data.center + FAS_Vector(0.0, 1.0);
            data.center.mirror(axisPoint1, axisPoint2);
            setReversed( ! isReversed() );
            double a= (axisPoint2 - axisPoint1).angle()*2;
            setAngle1(FAS_Math::correctAngle(a - getAngle1()));
            setAngle2(FAS_Math::correctAngle(a - getAngle2()));
            correctAngles(); // make sure angleLength is no more than 2*M_PI
        }
        if (factor.y<0.0)
        {
            FAS_Vector axisPoint1=data.center;
            FAS_Vector axisPoint2=data.center + FAS_Vector(1.0, 0.0);
            data.center.mirror(axisPoint1, axisPoint2);
            setReversed( ! isReversed() );
            double a= (axisPoint2 - axisPoint1).angle()*2;
            setAngle1(FAS_Math::correctAngle(a - getAngle1()));
            setAngle2(FAS_Math::correctAngle(a - getAngle2()));
            correctAngles(); // make sure angleLength is no more than 2*M_PI
        }
        data.center.scale(center, factor);
        data.radius *= factor.x;
        data.radius = fabs( data.radius );

}
// Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_Arc& a) {
    os << " Arc: " << a.data << "\n";
    return os;
}

