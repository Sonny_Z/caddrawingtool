/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_variabledict.h"

//sort with alphabetical order
#include <iostream>
#include <QString>

/**
 * Removes all variables in the blocklist.
 */
void FAS_VariableDict::clear()
{
    variables.clear();
}

/**
 * Adds a variable to the variable dictionary. If a variable with the
 * same name already exists, is will be overwritten.
 */
void FAS_VariableDict::add(const QString& key,
                           const QString& value, int code)
{
    if (key.isEmpty()) {
        return;
    }

    variables.insert(key, FAS_Variable(value, code));
}


/**
 * Adds a variable to the variable dictionary. If a variable with the
 * same name already exists, is will be overwritten.
 */
void FAS_VariableDict::add(const QString& key, int value, int code)
{
    if (key.isEmpty()) {
        return;
    }

    variables.insert(key, FAS_Variable(value, code));
}


/**
 * Adds a variable to the variable dictionary. If a variable with the
 * same name already exists, is will be overwritten.
 */
void FAS_VariableDict::add(const QString& key, double value, int code)
{
    if (key.isEmpty()) {
        return;
    }

    variables.insert(key, FAS_Variable(value, code));
}


/**
 * Adds a variable to the variable dictionary. If a variable with the
 * same name already exists, is will be overwritten.
 */
void FAS_VariableDict::add(const QString& key,
                           const FAS_Vector& value, int code)
{
    if (key.isEmpty()) {
        return;
    }

    variables.insert(key, FAS_Variable(value, code));
}


/**
 * Gets the value for the given variable.
 *
 * @param key Key of the variable.
 * @param def Default value.
 *
 * @return The value for the given variable or the given default value
 * if the variable couldn't be found.
 */
FAS_Vector FAS_VariableDict::getVector(const QString& key, const FAS_Vector& def) const
{
    FAS_Vector ret;

    auto i = variables.find(key);
    if (variables.end() != i && FAS2::VariableVector == i.value().getType()) {
        ret = i.value().getVector();
    } else {
        ret = def;
    }

    return ret;
}


/**
 * Gets the value for the given variable.
 *
 * @param key Key of the variable.
 * @param def Default value.
 *
 * @return The value for the given variable or the given default value
 * if the variable couldn't be found.
 */
QString FAS_VariableDict::getString(const QString& key, const QString& def) const
{
    QString ret;
    auto i = variables.find(key);
    if (variables.end() != i && FAS2::VariableString == i.value().getType()) {
        ret = i.value().getString();
    }
    else {
        ret = def;
    }

    return ret;
}


/**
 * Gets the value as int for the given variable.
 *
 * @param key Key of the variable.
 * @param def Default value.
 *
 * @return The value for the given variable or the given default value
 * if the variable couldn't be found.
 */
int FAS_VariableDict::getInt(const QString& key, int def) const
{
    int ret = 0;

    auto i = variables.find(key);
    if (variables.end() != i && FAS2::VariableInt == i.value().getType()) {
        ret = i.value().getInt();
    } else {
        ret = def;
    }

    return ret;
}


/**
 * Gets the value as double for the given variable.
 *
 * @param key Key of the variable.
 * @param def Default value.
 *
 * @return The value for the given variable or the given default value
 * if the variable couldn't be found.
 */
double FAS_VariableDict::getDouble(const QString& key, double def) const
{
    double ret = 0.0;

    auto i = variables.find(key);
    if (variables.end() != i && FAS2::VariableDouble == i.value().getType()) {
        ret = i.value().getDouble();
    } else {
        ret = def;
    }
    return ret;
}
/*
 * Removes a variable from the list.
 * TODO: Listeners are notified after the block was removed from
 * the list but before it gets deleted.
 */
void FAS_VariableDict::remove(const QString& key)
{
    // here the block is removed from the list but not deleted
    variables.remove(key);
}

std::ostream& operator << (std::ostream& os, FAS_VariableDict& d)
{
    os << "Variables: \n";
    auto it = d.variables.begin();
    while (it != d.variables.end()) {
        os << it.key().toLatin1().data() << ": ";
        switch (it.value().getType()) {
        case FAS2::VariableVoid:
            os << "void\n";
            break;
        case FAS2::VariableInt:
            os << "int " << it.value().getInt() << "\n";
            break;
        case FAS2::VariableDouble:
            os << "double " << it.value().getDouble() << "\n";
            break;
        case FAS2::VariableVector:
            os << "vector " << it.value().getVector() << "\n";
            break;
        case FAS2::VariableString:
            os << "string " << it.value().getString().toLatin1().data() << "\n";
            break;
        }
        ++it;
    }
    os << std::endl;

    return os;
}

