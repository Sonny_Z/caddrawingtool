/****************************************************************************
**
** This file is part of the LibreCAD project, a 2D CAD program
**
** Copyright (C) 2010 R. van Twisk (librecad@rvt.dds.nl)
** Copyright (C) 2001-2003 RibbonSoft. All rights reserved.
**
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by 
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
** 
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
** This copyright notice MUST APPEAR in all copies of the script!  
**
**********************************************************************/


#ifndef FAS_FONT_H
#define FAS_FONT_H

#include <iostream>
#include <QStringList>
#include "fas_blocklist.h"

/**
 * Class for representing a font. This is implemented as a FAS_Graphic
 * with a name (the font name) and several blocks, one for each letter
 * in the font.
 *
 * @author Andrew Mustun
 */
class FAS_Font {
public:
    FAS_Font(const QString& name, bool owner=true);
    //FAS_Font(const char* name);

    /** @return the fileName of this font. */
    QString getFileName() const {
        return fileName;
    }
	
    /** @return the encoding of this font. */
    QString getEncoding() const {
        return encoding;
    }
	
    /** @return the alternative names of this font. */
    const QStringList& getNames() const {
        return names;
    }
	
    /** @return the author(s) of this font. */
    const QStringList& getAuthors() const {
        return authors;
    }

    /** @return Default letter spacing for this font */
    double getLetterSpacing() {
        return letterSpacing;
    }

    /** @return Default word spacing for this font */
    double getWordSpacing() {
        return wordSpacing;
    }

    /** @return Default line spacing factor for this font */
    double getLineSpacingFactor() {
        return lineSpacingFactor;
    }

    bool loadFont();

	// Wrappers for block list (letters) functions
    FAS_BlockList* getLetterList() {
		return &letterList;
	}
    FAS_Block* findLetter(const QString& name) {
		return letterList.find(name);
	}
    uint countLetters() {
        return letterList.count();
    }
    FAS_Block* letterAt(uint i) {
		return letterList.at(i);
	}

    friend std::ostream& operator << (std::ostream& os, const FAS_Font& l);

    friend class FAS_FontList;

private:
	//! block list (letters)
    FAS_BlockList letterList;

    //! Font file name
    QString fileName;
	
    //! Font encoding (see docu for QTextCodec)
    QString encoding;

	//! Font names
        QStringList names;
	
	//! Authors
        QStringList authors;

    //! Is this font currently loaded into memory?
    bool loaded;

    //! Default letter spacing for this font
    double letterSpacing;

    //! Default word spacing for this font
    double wordSpacing;

    //! Default line spacing factor for this font
    double lineSpacingFactor;
};

#endif

