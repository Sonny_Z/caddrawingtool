/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "FAS_DeviceContainer.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>

#include "commondef.h"
#include "fas.h"
#include "fas_arc.h"
#include "fas_blocklist.h"
#include "fas_circle.h"
#include "fas_ellipse.h"
#include "fas_graphic.h"
#include "fas_graphicview.h"
#include "fas_layer.h"
#include "fas_math.h"
#include "fas_painterqt.h"
#include "fasutils.h"

FAS_DeviceContainerData::FAS_DeviceContainerData(const QString& _name,
                               FAS_Vector _insertionPoint,
                               FAS_Vector _scaleFactor,
                               double _angle,
                               int _cols, int _rows, FAS_Vector _spacing,
                               FAS_BlockList* _blockSource ,
                               FAS2::UpdateMode _updateMode ):
    name(_name)
  ,insertionPoint(_insertionPoint)
  ,scaleFactor(_scaleFactor)
  ,angle(_angle)
  ,cols(_cols)
  ,rows(_rows)
  ,spacing(_spacing)
  ,blockSource(_blockSource)
  ,updateMode(_updateMode)
  ,deviceInfo(COMMONDEF->getNewNotDeviceInfo())
{
}

std::ostream& operator << (std::ostream& os, const FAS_DeviceContainerData& d)
{
    os << "(" << d.name.toLatin1().data() << ")";
    return os;
}

FAS_DeviceContainer::FAS_DeviceContainer(FAS_EntityContainer* parent, const FAS_DeviceContainerData& d)
    : FAS_EntityContainer(parent), data(d)
{
    block = nullptr;
    if (data.updateMode!=FAS2::NoUpdate)
    {
        update();
    }
}

FAS_DeviceContainer::~FAS_DeviceContainer()
{
    if(data.deviceInfo != nullptr)
    {
        delete data.deviceInfo;
        data.deviceInfo = nullptr;
    }
}

FAS_Entity* FAS_DeviceContainer::clone() const
{
    return NULL;
}

// Updates the entity buffer of this insert entity.
void FAS_DeviceContainer::update()
{

}

// return Pointer to the block associated with this Insert
FAS_Block* FAS_DeviceContainer::getBlockForInsert() const
{
    return NULL;
}

bool FAS_DeviceContainer::isVisible() const
{
    return false;
}

FAS_VectorSolutions FAS_DeviceContainer::getRefPoints() const
{
    return FAS_VectorSolutions{data.insertionPoint};
}

FAS_Vector FAS_DeviceContainer::getNearestRef(const FAS_Vector& coord, double* dist) const
{
    return getRefPoints().getClosest(coord, dist);
}

void FAS_DeviceContainer::move(const FAS_Vector& offset)
{
    data.insertionPoint.move(offset);
    update();
}

void FAS_DeviceContainer::rotate(const FAS_Vector& center, const double& angle)
{
    data.insertionPoint.rotate(center, angle);
    data.angle = FAS_Math::correctAngle(data.angle+angle);
    update();
}

void FAS_DeviceContainer::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    data.insertionPoint.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angleVector.angle());
    update();
}

void FAS_DeviceContainer::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    data.insertionPoint.scale(center, factor);
    data.scaleFactor.scale(FAS_Vector(0.0, 0.0), factor);
    data.spacing.scale(FAS_Vector(0.0, 0.0), factor);
    update();
}

void FAS_DeviceContainer::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    data.insertionPoint.mirror(axisPoint1, axisPoint2);
    FAS_Vector vec = FAS_Vector::polar(1.0, data.angle);
    vec.mirror(FAS_Vector(0.0,0.0), axisPoint2-axisPoint1);
    data.angle = FAS_Math::correctAngle(vec.angle()-M_PI);
    data.scaleFactor.x *= -1;
    update();
}

void FAS_DeviceContainer::draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset)
{

}

std::ostream& operator << (std::ostream& os, const FAS_DeviceContainer& i)
{
    os << " Insert: " << i.getData() << std::endl;
    return os;
}

void FAS_DeviceContainer::setDeviceInfo(DeviceInfo* info)
{
    data.deviceInfo = info;
}

DeviceInfo* FAS_DeviceContainer::getDeviceInfo() const
{
    return data.deviceInfo;
}

