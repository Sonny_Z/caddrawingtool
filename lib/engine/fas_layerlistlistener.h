/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_LAYERLISTLISTENER_H
#define FAS_LAYERLISTLISTENER_H

#include "fas_layer.h"

/**
 * This class is an interface for classes that are interested in
 * knowing about changes in the layer list.
 */
class FAS_LayerListListener {
public:
    FAS_LayerListListener() {}
    virtual ~FAS_LayerListListener() {}

    /**
     * Called when the active layer changes.
     */
    virtual void layerActivated(FAS_Layer*) {}

    /**
     * Called when a new layer is added to the list.
     */
    virtual void layerAdded(FAS_Layer*) {}

    /**
     * Called when a layer is removed from the list.
     */
    virtual void layerRemoved(FAS_Layer*) {}

    /**
     * Called when a layer's attributes are modified.
     */
    virtual void layerEdited(FAS_Layer*) {}

    /**
     * Called when a layer's visibility is toggled.
     */
    virtual void layerToggled(FAS_Layer*) {}

    /**
     * Called when a layer's lock attribute is toggled.
     */
    virtual void layerToggledLock(FAS_Layer*) {}

    /**
     * Called when a layer's printing attribute is toggled.
     */
    virtual void layerToggledPrint(FAS_Layer*) {}

    /**
     * Called when a layer's construction attribute is toggled.
     */
    virtual void layerToggledConstruction(FAS_Layer*) {}
}
;

#endif
