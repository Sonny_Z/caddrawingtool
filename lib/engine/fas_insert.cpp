﻿/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_insert.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>

#include "commondef.h"
#include "fas.h"
#include "fas_arc.h"
#include "fas_blocklist.h"
#include "fas_circle.h"
#include "fas_ellipse.h"
#include "fas_graphic.h"
#include "fas_graphicview.h"
#include "fas_layer.h"
#include "fas_math.h"
#include "fas_painterqt.h"
#include "fasutils.h"
#include "fas_point.h"
#include "fas_attdef.h"
#include <QDebug>
FAS_AttribData::FAS_AttribData()
{
    this->height=1;
}
FAS_InsertData::FAS_InsertData(const QString& _name,
                               FAS_Vector _insertionPoint,
                               FAS_Vector _scaleFactor,
                               double _angle,
                               int _cols, int _rows, FAS_Vector _spacing,
                               FAS_BlockList* _blockSource ,
                               FAS2::UpdateMode _updateMode ):
    name(_name)
  ,insertionPoint(_insertionPoint)
  ,scaleFactor(_scaleFactor)
  ,angle(_angle)
  ,cols(_cols)
  ,rows(_rows)
  ,spacing(_spacing)
  ,blockSource(_blockSource)
  ,updateMode(_updateMode)
  ,deviceInfo(COMMONDEF->getNewNotDeviceInfo())
{
}
FAS_InsertData::FAS_InsertData(const QString& _name,
                               FAS_Vector _insertionPoint,
                               FAS_Vector _scaleFactor,
                               double _angle,
                               int _cols, int _rows, FAS_Vector _spacing,
                               QString _insertHandleId,
                               FAS_BlockList* _blockSource ,
                               FAS2::UpdateMode _updateMode):
    name(_name)
  ,insertionPoint(_insertionPoint)
  ,scaleFactor(_scaleFactor)
  ,angle(_angle)
  ,cols(_cols)
  ,rows(_rows)
  ,spacing(_spacing)
  ,blockSource(_blockSource)
  ,updateMode(_updateMode)
  ,deviceInfo(COMMONDEF->getNewNotDeviceInfo())

{
    insertHandleId=_insertHandleId;
}
std::ostream& operator << (std::ostream& os, const FAS_InsertData& d)
{
    os << "(" << d.name.toLatin1().data() << ")";
    return os;
}

FAS_Insert::FAS_Insert(FAS_EntityContainer* parent, const FAS_InsertData& d)
    : FAS_EntityContainer(parent), data(d)
{
    block = nullptr;
    if (data.updateMode!=FAS2::NoUpdate)
    {
        update();
    }
}

FAS_Insert::~FAS_Insert()
{
    if(data.deviceInfo != nullptr)
    {
        delete data.deviceInfo;
        data.deviceInfo = nullptr;
    }
}

FAS_Entity* FAS_Insert::clone() const
{
    FAS_Insert* i = new FAS_Insert(*this);
    i->setOwner(isOwner());
    i->initId();
    i->detach();
//    i->setDeviceInfo(COMMONDEF->getNewNotDeviceInfo());
    i->getDeviceInfo()->setCodeInfo(this->getDeviceInfo()->getCodeInfo());
    i->getDeviceInfo()->setID(this->getDeviceInfo()->getID());
    i->getDeviceInfo()->setName(this->getDeviceInfo()->getName());
    i->getDeviceInfo()->setShowSVG(this->getDeviceInfo()->getShowSVG());
    i->getDeviceInfo()->setSvgRender(this->getDeviceInfo()->getSvgRender());

    return i;
}

// Updates the entity buffer of this insert entity.
void FAS_Insert::update()
{
    //    qDebug() << "INsert Name" << this->getData().name;
       auto readOnlyEnabled = isReadOnly();
        if (updateEnabled==false)
        {
            return;
        }
        //clear();
        FAS_Block* blk = getBlockForInsert();
        /*qDebug()<<blk->count();
        for(int i=0;i<blk->count();i++)
        {
            int size=blk->entityAt(i)->count();
            for(int j=0;j<size;)

        }*/
        if (!blk)
        {
            return;
        }
        if (isUndone())
        {
            return;
        }

        if(!blk->isVisible())
        {
            return;
        }

        if (fabs(data.scaleFactor.x) < 1.0e-6 || fabs(data.scaleFactor.y) < 1.0e-6)
        {
            return;
        }

        FAS_Pen tmpPen;

//        qDebug() << "Initial min(" << getMin().x << "," << getMin().y << ")," << "max(" << getMax().x << "," << getMax().y << ")";
        int adjustTimes = 0;
        for(auto e: *blk)
        {
            for (int c=0; c<data.cols; ++c)
            {
                for (int r=0; r<data.rows; ++r)
                {
                    if(!isReadOnly()){
                    if (e->rtti()==FAS2::EntityInsert)
                    {
                        static_cast<FAS_Insert*>(e)->update();
                        //continue;//there is a bug in the insert within insert when calculating border/position. Ignore it for now.
                    }
                    }

                    FAS_Entity* ne;
                    bool isNotClone = (e->rtti()==FAS2::EntityLine || e->rtti()==FAS2::EntityCircle||e->rtti()==FAS2::EntityPolyline
                                     ||e->rtti()== FAS2::EntityArc  || e->rtti()==FAS2::EntitySpline||e->rtti()==FAS2::EntityText
                                     ||e->rtti()==FAS2::EntityMText||e->rtti()==FAS2::EntitySolid||e->rtti()==FAS2::EntityHatch
                                     ||e->rtti()==FAS2::EntityInsert||e->rtti()==FAS2::EntityPoint||e->rtti()==FAS2::EntityEllipse
                                     ||e->rtti()==FAS2::EntityAttdef);//
                    if (isNotClone & readOnlyEnabled)
                    //if(true)
                    {
//                        qDebug() << adjustTimes << " e min(" << getMin().x << "," << getMin().y << ")," << "max(" << getMax().x << "," << getMax().y << ")";
                        ne=e;
                        ne->resetBorders();
                        if(e->rtti()==FAS2::EntityAttdef)
                        {

                            qDebug()<<"mtest ATTDEF";
                            FAS_ATTDEF* me1=dynamic_cast<FAS_ATTDEF*>(e);

                        }
//                        qDebug() << adjustTimes << " ne0 min(" << getMin().x << "," << getMin().y << ")," << "max(" << getMax().x << "," << getMax().y << ")";
                        if (fabs(data.scaleFactor.x)>1.0e-6 && fabs(data.scaleFactor.y)>1.0e-6)
                        {

                            ne->resetBasicData(data.insertionPoint +
                                                 FAS_Vector(data.spacing.x/data.scaleFactor.x*1, data.spacing.y/data.scaleFactor.y*1),data.insertionPoint, data.angle,data.scaleFactor);
                            ne->moveEntityBoarder(data.insertionPoint +
                                                 FAS_Vector(data.spacing.x/data.scaleFactor.x*1, data.spacing.y/data.scaleFactor.y*1),data.insertionPoint, data.angle,data.scaleFactor);

                        }
                        else
                        {
                            ne->resetBasicData(data.insertionPoint,data.insertionPoint, data.angle,data.scaleFactor);
                            ne->moveEntityBoarder(data.insertionPoint,data.insertionPoint, data.angle,data.scaleFactor);

                        }
//                        qDebug() << adjustTimes << " ne min(" << getMin().x << "," << getMin().y << ")," << "max(" << getMax().x << "," << getMax().y << ")";

                        if(ne->isVisible()){
                            if(e->rtti()!=FAS2::EntityInsert)
                            {
                                ne->calculateBorders();
                            }
                             adjustBorders(ne);
                        }

//                        qDebug() << adjustTimes << " ne~ min(" << getMin().x << "," << getMin().y << ")," << "max(" << getMax().x << "," << getMax().y << ")";

                    }
                    else{
//                        qDebug() << "CLONED Entity Type" <<e->rtti();
                         ne=e->clone();
                         if (fabs(data.scaleFactor.x)>1.0e-6 && fabs(data.scaleFactor.y)>1.0e-6){
                             ne->move(data.insertionPoint +
                                      FAS_Vector(data.spacing.x/data.scaleFactor.x*c, data.spacing.y/data.scaleFactor.y*r));
                         }else
                         {
                           ne->move(data.insertionPoint);

                         }
                         // Move because of block base point:
                         ne->move(blk->getBasePoint()*-1);

                         // Scale:
                         ne->scale(data.insertionPoint, data.scaleFactor);

                       // Rotate:
                        ne->rotate(data.insertionPoint, data.angle);
                    }

                    ne->initId();
                    if(e->rtti()!=FAS2::EntityInsert){

                    ne->setUpdateEnabled(false);
//                     if entity layer are 0 set to insert layer to allow "1 layer control"
                    FAS_Layer *l= ne->getLayer();//special fontchar block don't have
                    if (l  && ne->getLayer()->getName() == "0")
                        ne->setLayer(this->getLayer());
                    ne->setParent(this);
                    ne->setVisible(getFlag(FAS2::FlagVisible));

                    // Select:
                    ne->setSelected(isSelected());

                    // individual entities can be on indiv. layers
                    tmpPen = ne->getPen(false);

                    // color from block (free floating):
                    if (tmpPen.getColor()==FAS_Color(FAS2::FlagByBlock))
                    {
                        tmpPen.setColor(getPen().getColor());
                    }

                    // line width from block (free floating):
                    if (tmpPen.getWidth()==FAS2::WidthByBlock)
                    {
                        tmpPen.setWidth(getPen().getWidth());
                    }

                    // line type from block (free floating):
                    if (tmpPen.getLineType()==FAS2::LineByBlock)
                    {
                        tmpPen.setLineType(getPen().getLineType());
                    }

                    ne->setPen(tmpPen);
                    ne->setUpdateEnabled(true);
                    }
                    if(findEntity(ne)<0)
                    {
                        appendEntity(ne);
                        adjustTimes ++;
                    }
//                    qDebug() << adjustTimes << " adjusted min(" << getMin().x << "," << getMin().y << ")," << "max(" << getMax().x << "," << getMax().y << ")";
                }
            }
        }
        //        calculateBorders();
}

// return Pointer to the block associated with this Insert
FAS_Block* FAS_Insert::getBlockForInsert() const
{
    FAS_Block* blk = nullptr;
    if (block)
    {
        blk=block;
        return blk;
    }

    FAS_BlockList* blkList;

    if (!data.blockSource)
    {
        if (getGraphic())
        {
            blkList = getGraphic()->getBlockList();
        }
        else
        {
            blkList = nullptr;
        }
    }
    else
    {
        blkList = data.blockSource;
    }

    if (blkList)
    {
        blk = blkList->find(data.name);
    }

    block = blk;

    return blk;
}

bool FAS_Insert::isVisible() const
{
    FAS_Block* blk = getBlockForInsert();
    if (blk)
    {
        if (blk->isFrozen())
        {
            return false;
        }
    }

    return FAS_Entity::isVisible();
}

FAS_VectorSolutions FAS_Insert::getRefPoints() const
{
    return FAS_VectorSolutions{data.insertionPoint};
}

FAS_Vector FAS_Insert::getNearestRef(const FAS_Vector& coord, double* dist) const
{
    return getRefPoints().getClosest(coord, dist);
}

void FAS_Insert::move(const FAS_Vector& offset)
{
    data.insertionPoint.move(offset);
    update();
}

void FAS_Insert::rotate(const FAS_Vector& center, const double& angle)
{
    data.insertionPoint.rotate(center, angle);
    data.angle = FAS_Math::correctAngle(data.angle+angle);
    update();
}

void FAS_Insert::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    data.insertionPoint.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angleVector.angle());
    update();
}

void FAS_Insert::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    data.insertionPoint.scale(center, factor);
    data.scaleFactor.scale(FAS_Vector(0.0, 0.0), factor);
    data.spacing.scale(FAS_Vector(0.0, 0.0), factor);
    update();
}

void FAS_Insert::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    data.insertionPoint.mirror(axisPoint1, axisPoint2);
    FAS_Vector vec = FAS_Vector::polar(1.0, data.angle);
    vec.mirror(FAS_Vector(0.0,0.0), axisPoint2-axisPoint1);
    data.angle = FAS_Math::correctAngle(vec.angle()-M_PI);
    data.scaleFactor.x *= -1;
    update();
}

void FAS_Insert::draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset)
{
    if(!isVisible()){
        return;
    }

//    auto curInsertListSize = inserts.size();
//    if (curInsertListSize > 5000)
//    {
//        return;
//    }
//
    bool drawsvg = false;
    if (getDeviceInfo()->isValidDevice())
    {
        FAS_Vector diagonal = getMax() - getMin();
        double svgSize = (diagonal.x + diagonal.y) * 0.5;
        svgSize = svgSize * (view->getFactor().x + view->getFactor().y) * 0.5;

        FAS_Vector vector = view->toGui(FASUtils::getUpperLeftVector(this));
        if ((vector.x == 0) || (vector.y == 0))
            drawsvg = false;

        FAS_PainterQt* rsPainterQT = dynamic_cast<FAS_PainterQt*>(painter);
        QPainter* qtPainter = dynamic_cast<QPainter*>(rsPainterQT);

        if(qtPainter != nullptr)
        {
            if (this->isSelected())
            {
                QPen pen(Qt::green, 2, Qt::DashDotDotLine, Qt::FlatCap, Qt::MiterJoin);
                if(qtPainter != nullptr)
                {
                    qtPainter->setPen(pen);
                    COMMONDEF->drawEntityCount++;
                    qtPainter->drawRect(vector.x - svgSize*0.2, vector.y - svgSize*0.2, svgSize*1.4, svgSize*1.4);
                }
            }
            if (this->isHighlighted())
            {
                if(qtPainter != nullptr)
                {
                    QPen pen(Qt::yellow, 4, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin);
                    qtPainter->setPen(pen);
                    COMMONDEF->drawEntityCount++;
                    qtPainter->drawRect(vector.x - svgSize*0.2, vector.y - svgSize*0.2, svgSize*1.4, svgSize*1.4);
                }
            }
            if(/*getDeviceInfo()->getShowSVG()*/COMMONDEF->getSvgUiMode())
            {
                this->getDeviceInfo()->getSvgRender()->render(qtPainter, QRectF(vector.x, vector.y, svgSize, svgSize));
                return;
            }
        }
    }

    //FAS_EntityContainer::draw(painter, view, patternOffset);
    FAS_InsertData data1=this->data;
    FAS_EntityContainer::drawInsertEntity(painter, view, patternOffset,data1);
}

std::ostream& operator << (std::ostream& os, const FAS_Insert& i)
{
    os << " Insert: " << i.getData() << std::endl;
    return os;
}

void FAS_Insert::setDeviceInfo(DeviceInfo* info)
{
    data.deviceInfo = info;
}

DeviceInfo* FAS_Insert::getDeviceInfo() const
{
    return data.deviceInfo;
}
// Recalculates the borders of this entity container
//void FAS_Insert::calculateBorders()
//{
//    FAS_InsertData insertData=this->data;
//    for (FAS_Entity* e: entities)
//    {
//        FAS_Layer* layer = e->getLayer();
//        bool calculte = false;
//        if (e->isVisible())
//        {
//            if(nullptr == layer)
//                calculte = true;
//            else if(!layer->isFrozen() && layer->isPrint())
//                calculte = true;
//        }
//        if(calculte)
//            if (e->rtti()== FAS2::EntityLine || e->rtti()==FAS2::EntityCircle){
//            e->resetBorders();
//            e->resetBasicData();
//            if (fabs(insertData.scaleFactor.x)>1.0e-6 && fabs(insertData.scaleFactor.y)>1.0e-6)
//            {

//                e->moveEntityBoarder(insertData.insertionPoint +
//                                     FAS_Vector(insertData.spacing.x/insertData.scaleFactor.x*1, insertData.spacing.y/insertData.scaleFactor.y*1),insertData.insertionPoint, insertData.angle,insertData.scaleFactor);            }
//            else
//            {
//                e->moveEntityBoarder(insertData.insertionPoint,insertData.insertionPoint, insertData.angle,insertData.scaleFactor);
//            }

//            e->calculateBorders();
//            }
//            if (e)
//            {
//                // make sure a container is not empty (otherwise the border
//                if (!e->isContainer() || e->count()>0)
//                {
//                    //If max and min is the same, some error occurs, ignore it.
//                    if(abs(e->getMax().x - e->getMin().x) < FAS_TOLERANCE &&
//                            abs(e->getMax().y - e->getMin().y) < FAS_TOLERANCE)
//                        return;
//                    minV = FAS_Vector::minimum(e->getMin(),minV);
//                    maxV = FAS_Vector::maximum(e->getMax(),maxV);
//                }
//        }
//    }
//    // needed for correcting corrupt data (PLANS.dxf)
//    if (minV.x>maxV.x || minV.x>FAS_MAXDOUBLE || maxV.x>FAS_MAXDOUBLE
//            || minV.x<FAS_MINDOUBLE || maxV.x<FAS_MINDOUBLE)
//    {
//        minV.x = 0.0;
//        maxV.x = 0.0;
//    }
//    if (minV.y>maxV.y || minV.y>FAS_MAXDOUBLE || maxV.y>FAS_MAXDOUBLE
//            || minV.y<FAS_MINDOUBLE || maxV.y<FAS_MINDOUBLE)
//    {
//        minV.y = 0.0;
//        maxV.y = 0.0;
//    }

////    qDebug() << "INSERT NEW SIZE" <<maxV.x <<maxV.y << minV.x << minV.y;
//}

void FAS_Insert::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& dataInsert)
{
    if (!(painter && view))
    {
        return;
    }

    FAS_Entity* e=firstEntity(FAS2::ResolveNone);
    if (e)
    {
        FAS_Pen p=this->getPen(true);
        e->setPen(p);
        double patternOffset(0.0);
        //view->drawEntity(painter, e, patternOffset,dataInsert);
         view->drawEntity(painter, e, patternOffset);

        e = nextEntity(FAS2::ResolveNone);
        while(e)
        {

            //view->drawEntity(painter, e, patternOffset,dataInsert);
            view->drawEntity(painter, e, patternOffset);
            e = nextEntity(FAS2::ResolveNone);
        }
    }
}
void FAS_Insert::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){

    FAS_Vector insertionPoint=getInsertionPoint();
    FAS_Vector bkinsertionPoint=data.bkinsertionPoint;
    //qDebug() <<"insert reset basic data {bkinsertionPoint.valid data.bkangle data.angle}" <<bkinsertionPoint.valid << data.bkangle << data.angle;
    if(!bkinsertionPoint.valid)
    {
        data.bkinsertionPoint=insertionPoint;
        data.bkangle=data.angle;
        data.bkscaleFactor=data.scaleFactor;
        data.bkspacing=data.spacing;

    }else{
        data.insertionPoint=data.bkinsertionPoint;
        data.angle=data.bkangle;
        data.scaleFactor=data.bkscaleFactor;
        data.spacing=data.bkspacing;
    }
//    FAS_EntityContainer::resetBasicData(offset,center,angle,factor);
}

void FAS_Insert::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){

    //FAS_EntityContainer::moveEntityBoarder(offset,center,angle,factor);
    data.insertionPoint.move(offset);
    data.insertionPoint.rotate(center, angle);
    data.angle = FAS_Math::correctAngle(data.angle+angle);
    data.insertionPoint.scale(center, factor);
    data.scaleFactor.scale(FAS_Vector(0.0, 0.0), factor);
    data.spacing.scale(FAS_Vector(0.0, 0.0), factor);
    update();

}
void FAS_Insert::setAttibutes(std::string attributeLabel,std::string text,double height,double angle,int hAlign,int vAlign)
{
    //map.insert("twelve", 12);
    QString key=QString::fromStdString(attributeLabel);
    QString mtext=QString::fromStdString(text);
    FAS_AttribData v;
    v.text=mtext;
    v.height=height;
    v.angle=angle;
    v.hAlign=hAlign;
    v.vAlign=vAlign;
    data.mmap.insert(key,v);
}
