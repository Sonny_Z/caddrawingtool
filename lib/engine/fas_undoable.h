/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_UNDOABLE_H
#define FAS_UNDOABLE_H

#include "fas.h"
#include "fas_flags.h"

class FAS_UndoCycle;

/**
 * Base class for something that can be added and deleted and every 
 * addition and deletion can be undone.
 */
class FAS_Undoable : public FAS_Flags
{
public:
    virtual ~FAS_Undoable();

    /*
     * Runtime type identification for undoables.
     * Note that this is voluntarily. The default implementation 
     * returns FAS2::UndoableUnknown.
     */
    virtual FAS2::UndoableType undoRtti() const
    {
        return FAS2::UndoableUnknown;
    }

	void setUndoCycle(FAS_UndoCycle* cycle);
	void changeUndoState();
	void setUndoState(bool undone);
	bool isUndone() const;

	/**
	 * Can be overwriten by the implementing class to be notified
	 * when the undo state changes (the undoable becomes visible / invisible).
	 */
	virtual void undoStateChanged(bool /*undone*/) = 0;

private:
	FAS_UndoCycle* cycle = nullptr;
};

#endif
