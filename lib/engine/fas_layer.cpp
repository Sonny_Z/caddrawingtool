/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_layer.h"

//sort with alphabetical order
#include <iostream>
#include <QString>

FAS_LayerData::FAS_LayerData(const QString& name,
                             const FAS_Pen& pen,
                             bool frozen,
                             bool locked):
    name(name)
  ,pen(pen)
  ,frozen(frozen)
  ,locked(locked)
{
}

FAS_Layer::FAS_Layer(const QString& name):
    data(name, FAS_Pen(Qt::black, FAS2::Width00,FAS2::SolidLine), false, false)
{
}

FAS_Layer* FAS_Layer::clone() const
{
    return new FAS_Layer(*this);
}

// sets a new name for this layer.
void FAS_Layer::setName(const QString& name)
{
    data.name = name;
}

//return the name of this layer.
QString FAS_Layer::getName() const
{
    return data.name;
}

//sets the default pen for this layer.
void FAS_Layer::setPen(const FAS_Pen& pen)
{
    data.pen = pen;
}

// return default pen for this layer.
FAS_Pen FAS_Layer::getPen() const
{
    return data.pen;
}

bool FAS_Layer::isFrozen() const
{
    return data.frozen;
}

bool FAS_Layer::isConverted() const
{
    return data.converted;
}

void FAS_Layer::setConverted(bool c)
{
    data.converted = c;
}

// Toggles the visibility of this layer.
// Freezes the layer if it's not frozen, thaws the layer otherwise
void FAS_Layer::toggle()
{
    data.frozen = !data.frozen;
}

void FAS_Layer::freeze(bool freeze)
{
    data.frozen = freeze;
}

void FAS_Layer::toggleLock()
{
    data.locked = !data.locked;
}

void FAS_Layer::togglePrint()
{
    data.print = !data.print;
}

void FAS_Layer::toggleConstruction()
{
    data.construction = !data.construction;
}

void FAS_Layer::lock(bool l)
{
    data.locked = l;
}

bool FAS_Layer::isLocked() const
{
    return data.locked;
}

void FAS_Layer::visibleInLayerList(bool l)
{
    data.visibleInLayerList = l;
}

bool FAS_Layer::isVisibleInLayerList() const
{
    return data.visibleInLayerList;
}

bool FAS_Layer::setPrint( const bool print)
{
    return data.print = print;
}

bool FAS_Layer::isPrint() const
{
    return data.print;
}

bool FAS_Layer::isConstruction() const
{
    return data.construction;
}

bool FAS_Layer::setConstruction( const bool construction)
{
    data.construction = construction;
    return construction;
}

//Dumps the layers data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_Layer& l)
{
    os << " name: " << l.getName().toLatin1().data()
       << " pen: " << l.getPen()
       << " frozen: " << (int)l.isFrozen()
       << " address: " << &l
       << std::endl;
    return os;
}

