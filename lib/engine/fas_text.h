/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/


#ifndef FAS_TEXT_H
#define FAS_TEXT_H
#include "fas.h"
#if MYL
#include "fas_entity.h"
#include "fas_entitycontainer.h"
#define FAS_String QString
/**
 * Holds the data that defines a text entity.
 */
class FAS_TextData {
public:
    // Vertical alignments.
       enum VAlign
       {

           VABottom,   /**< Bottom */
           VAMiddle,   /**< Middle */
           VATop,       /**< Top. */
           VABaseline /**< Bottom */

       };

       // Horizontal alignments.
       enum HAlign
       {
           HALeft,     /**< Left */
           HACenter,   /**< Centered */
           HARight,    /**< Right */
           HAAligned,  /**< Aligned */
           HAMiddle,   /**< Middle */
           HAFit       /**< Fit */
       };

       // Text drawing direction.
       enum TextGeneration
       {
           None,      /**< Normal text */
           Backward,  /**< Mirrored in X */
           UpsideDown /**< Mirrored in Y */
       };
    /**
     * Default constructor. Leaves the data object uninitialized.
     */
    FAS_TextData() {}

    /**
     * Constructor with initialisation.
     *
     * @param insertionPoint Insertion point
     * @param height Nominal (initial) text height
     * @param width Reference rectangle width
     * @param valign Vertical alignment
     * @param halign Horizontal alignment
     * @param drawingDirection Drawing direction
     * @param lineSpacingStyle Line spacing style
     * @param lineSpacingFactor Line spacing factor
     * @param text Text string
     * @param style Text style name
     * @param angle Rotation angle
     * @param updateMode FAS2::Update will update the text entity instantly
     *    FAS2::NoUpdate will not update the entity. You can update
     *    it later manually using the update() method. This is
     *    often the case since you might want to adjust attributes
     *    after creating a text entity.
     */
    FAS_TextData(const FAS_Vector& insertionPoint,
                double height,
                double width,
                VAlign valign,
                HAlign halign,
                FAS2::TextDrawingDirection drawingDirection,
                FAS2::TextLineSpacingStyle lineSpacingStyle,
                double lineSpacingFactor,
                const FAS_String& text,
                const FAS_String& style,
                double angle,
                FAS2::UpdateMode updateMode = FAS2::Update) {
        this->insertionPoint = insertionPoint;
        this->height = height;
        this->width = width;
        this->valign = valign;
        this->halign = halign;
        this->drawingDirection = drawingDirection;
        this->lineSpacingStyle = lineSpacingStyle;
        this->lineSpacingFactor = lineSpacingFactor;
        this->style = style;
        this->angle = angle;
        this->text = text;
        this->updateMode = updateMode;
    }
    FAS_TextData(const FAS_Vector& insertionPoint,
                     const FAS_Vector& secondPoint,
                     double height,
                     double widthRel,
                     VAlign valign,
                     HAlign halign,
                     TextGeneration textGeneration,
                     const QString& text,
                     const QString& style,
                     double angle,
                     FAS2::UpdateMode updateMode = FAS2::Update)
    {
        this->insertionPoint = insertionPoint;
        this->secondPoint=secondPoint;
        this->height = height;
        this->width = widthRel;
        this->valign = valign;
        this->halign = halign;
        //this->drawingDirection = drawingDirection;
        //this->lineSpacingStyle = lineSpacingStyle;
        //this->lineSpacingFactor = lineSpacingFactor;
        this->style = style;
        this->angle = angle;
        this->text = text;
        this->updateMode = updateMode;
    }
    friend class FAS_Text;

    friend std::ostream& operator << (std::ostream& os, const FAS_TextData& td) {
        char* s=td.text.toLatin1().data();
        os << "(";
        os<<s ;
        os<< ")";
        return os;
    }

public:
    /** Insertion point */
    FAS_Vector insertionPoint;
    /** Nominal (initial) text height */
    double height;
    /** Reference rectangle width */
    double width;
    /** Vertical alignment */
    VAlign valign;
    /** Horizontal alignment */
    HAlign halign;
    /** Drawing direction */
    FAS2::TextDrawingDirection drawingDirection;
    /** Line spacing style */
    FAS2::TextLineSpacingStyle lineSpacingStyle;
    /** Line spacing factor */
    double lineSpacingFactor;
    /** Text string */
    FAS_String text;
    /** Text style name */
    FAS_String style;
    /** Rotation angle */
    double angle;
    /** Update mode */
    FAS2::UpdateMode updateMode;
    FAS_Vector secondPoint;
    /** Insertion point */
    FAS_Vector bkinsertionPoint;
    /** Second point for fit or aligned*/
    FAS_Vector bksecondPoint;
    /** Nominal (initial) text height */
    double bkheight;
    /** Rotation angle */
    double bkangle;
};



/**
 * Class for a text entity.
 * Please note that text strings can contain special
 * characters such as %%c for a diameter sign as well as unicode
 * characters. Line feeds are stored as real line feeds in the string.
 *
 * @author Andrew Mustun
 */
class FAS_Text : public FAS_EntityContainer {
public:
    FAS_Text(FAS_EntityContainer* parent,
            const FAS_TextData& d);
    virtual ~FAS_Text() {}

    virtual FAS_Entity* clone() {
        FAS_Text* t = new FAS_Text(*this);
        t->setOwner(isOwner());
        t->initId();
        t->detach();
        return t;
    }

    /**	@return FAS2::EntityText */
    virtual FAS2::EntityType rtti() const {
        return FAS2::EntityText;
    }

    /** @return Copy of data that defines the text. */
    FAS_TextData getData() const {
        return data;
    }

    void update();
    void updateAddLine(FAS_EntityContainer* textLine, int lineCounter);

    int getNumberOfLines();


    FAS_Vector getInsertionPoint() {
        return data.insertionPoint;
    }
    double getHeight() {
        return data.height;
    }
    void setHeight(double h) {
        data.height = h;
    }
    double getWidth() {
        return data.width;
    }
    void setAlignment(int a);
    int getAlignment();
    FAS_TextData::VAlign getVAlign() {
        return data.valign;
    }
    void setVAlign(FAS_TextData::VAlign va) {
        data.valign = va;
    }
    FAS_TextData::HAlign getHAlign() {
        return data.halign;
    }
    void setHAlign(FAS_TextData::HAlign ha) {
        data.halign = ha;
    }
    FAS2::TextDrawingDirection getDrawingDirection() {
        return data.drawingDirection;
    }
    FAS2::TextLineSpacingStyle getLineSpacingStyle() {
        return data.lineSpacingStyle;
    }
    void setLineSpacingFactor(double f) {
        data.lineSpacingFactor = f;
    }
    double getLineSpacingFactor() {
        return data.lineSpacingFactor;
    }
    void setText(const FAS_String& t);
    FAS_String getText() {
        return data.text;
    }
    void setStyle(const FAS_String& s) {
        data.style = s;
    }
    FAS_String getStyle() {
        return data.style;
    }
    void setAngle(double a) {
        data.angle = a;
    }
    double getAngle() {
        return data.angle;
    }
    double getUsedTextWidth() {
        return usedTextWidth;
    }
    double getUsedTextHeight() {
        return usedTextHeight;
    }

    virtual double getLength() {
        return -1.0;
    }

    virtual FAS_VectorSolutions getRefPoints();
    virtual FAS_Vector getNearestRef(const FAS_Vector& coord,
                                     double* dist = NULL);

    virtual void move(FAS_Vector offset);
    virtual void rotate(FAS_Vector center, double angle);
    virtual void scale(FAS_Vector center, FAS_Vector factor);
    virtual void mirror(FAS_Vector axisPoint1, FAS_Vector axisPoint2);
    virtual bool hasEndpointsWithinWindow(FAS_Vector v1, FAS_Vector v2);
    virtual void stretch(FAS_Vector firstCorner,
                         FAS_Vector secondCorner,
                         FAS_Vector offset);

    friend std::ostream& operator << (std::ostream& os, const FAS_Text& p);
    FAS_Vector getSecondPoint()
       {
           return data.secondPoint;
       }
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
    void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);

    virtual FAS_Vector getMiddlePoint(void)const override;
    virtual FAS_Vector getNearestEndpoint(const FAS_Vector& coord, double* dist = nullptr)const override;
    virtual void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    virtual FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                               bool onEntity = true, double* dist = nullptr,
                                               FAS_Entity** entity = nullptr) const override;
    virtual FAS_Vector getNearestMiddle(const FAS_Vector& coord, double* dist = nullptr, int middlePoints = 1) const override;
    virtual FAS_Vector getNearestDist(double distance, const FAS_Vector& coord, double* dist = nullptr) const override;
    virtual void calculateBorders() override;
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
protected:
    FAS_TextData data;

    /**
     * Text width used by the current contents of this text entity.
     * This property is updated by the update method.
     * @see update
     */
    double usedTextWidth;
    /**
     * Text height used by the current contents of this text entity.
     * This property is updated by the update method.
     * @see update
     */
    double usedTextHeight;
};
#else
#include "fas_atomicentity.h"
#include "fas_entitycontainer.h"

// Holds the data that defines a text entity.
struct FAS_TextData
{
    // Vertical alignments.
    enum VAlign
    {
        VABaseline, /**< Bottom */
        VABottom,   /**< Bottom */
        VAMiddle,   /**< Middle */
        VATop       /**< Top. */
    };

    // Horizontal alignments.
    enum HAlign
    {
        HALeft,     /**< Left */
        HACenter,   /**< Centered */
        HARight,    /**< Right */
        HAAligned,  /**< Aligned */
        HAMiddle,   /**< Middle */
        HAFit       /**< Fit */
    };

    // Text drawing direction.
    enum TextGeneration
    {
        None,      /**< Normal text */
        Backward,  /**< Mirrored in X */
        UpsideDown /**< Mirrored in Y */
    };

    FAS_TextData() {}

    /*
     * Constructor with initialisation.
     *
     * param insertionPoint Insertion point
     * param secondPoint Second point for aligned-fit
     * param height Nominal (initial) text height
     * param widthRel Reference rectangle width
     * param valign Vertical alignment
     * param halign Horizontal alignment
     * param textGeneration Text Generation
     * param text Text string
     * param style Text style name
     * param angle Rotation angle
     * param updateMode FAS2::Update will update the text entity instantly
     *    FAS2::NoUpdate will not update the entity. You can update
     *    it later manually using the update() method. This is
     *    often the case since you might want to adjust attributes
     *    after creating a text entity.
     */
    FAS_TextData(const FAS_Vector& insertionPoint,
                 const FAS_Vector& secondPoint,
                 double height,
                 double widthRel,
                 VAlign valign,
                 HAlign halign,
                 TextGeneration textGeneration,
                 const QString& text,
                 const QString& style,
                 double angle,
                 FAS2::UpdateMode updateMode = FAS2::Update);

    /** Insertion point */
    FAS_Vector insertionPoint;
    /** Second point for fit or aligned*/
    FAS_Vector secondPoint;
    /** Nominal (initial) text height */
    double height;
    /** Width/Height relation */
    double widthRel;
    /** Vertical alignment */
    VAlign valign;
    /** Horizontal alignment */
    HAlign halign;
    /** Text Generation */
    TextGeneration textGeneration;
    /** Text string */
    QString text;
    /** Text style name */
    QString style;
    /** Rotation angle */
    double angle;
    /** Update mode */
    FAS2::UpdateMode updateMode;


    /** Insertion point */
    FAS_Vector bkinsertionPoint;
    /** Second point for fit or aligned*/
    FAS_Vector bksecondPoint;
    /** Nominal (initial) text height */
    double bkheight;
    /** Rotation angle */
    double bkangle;
};

std::ostream& operator << (std::ostream& os, const FAS_TextData& td);

class FAS_Text : public FAS_AtomicEntity
{
public:
    FAS_Text(FAS_EntityContainer* parent,
             const FAS_TextData& d);

    virtual FAS_Entity* clone() const override;

    virtual FAS2::EntityType rtti() const override
    {
        return FAS2::EntityText;
    }

    FAS_TextData getData() const
    {
        return data;
    }

    void update() override;

    FAS_Vector getInsertionPoint()
    {
        return data.insertionPoint;
    }
    FAS_Vector getSecondPoint()
    {
        return data.secondPoint;
    }
    double getHeight()
    {
        return data.height;
    }
    void setHeight(double h)
    {
        data.height = h;
    }
    double getWidthRel() {
        return data.widthRel;
    }
    void setWidthRel(double w)
    {
        data.widthRel = w;
    }
    // bad functions, this is MText style align
    void setAlignment(int a);
    int getAlignment();

    FAS_TextData::VAlign getVAlign()
    {
        return data.valign;
    }
    void setVAlign(FAS_TextData::VAlign va)
    {
        data.valign = va;
    }
    FAS_TextData::HAlign getHAlign()
    {
        return data.halign;
    }
    void setHAlign(FAS_TextData::HAlign ha)
    {
        data.halign = ha;
    }
    FAS_TextData::TextGeneration getTextGeneration()
    {
        return data.textGeneration;
    }
    void setText(const QString& t);
    QString getText()
    {
        return data.text;
    }
    void setStyle(const QString& s)
    {
        data.style = s;
    }
    QString getStyle()
    {
        return data.style;
    }
    void setAngle(double a)
    {
        data.angle = a;
    }
    double getAngle()
    {
        return data.angle;
    }
    double getUsedTextWidth()
    {
        return usedTextWidth;
    }
    double getUsedTextHeight()
    {
        return usedTextHeight;
    }

    virtual FAS_Vector getMiddlePoint(void)const;
    //return The insertion point as endpoint.
    virtual FAS_Vector getNearestEndpoint(const FAS_Vector& coord, double* dist = nullptr)const override;
    virtual FAS_VectorSolutions getRefPoints() const override;
    virtual void move(const FAS_Vector& offset) override;
    virtual void rotate(const FAS_Vector& center, const double& angle) override;
    virtual void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    virtual void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    virtual void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    virtual bool hasEndpointsWithinWindow(const FAS_Vector& v1, const FAS_Vector& v2) override;
    virtual void stretch(const FAS_Vector& firstCorner,
                         const FAS_Vector& secondCorner,
                         const FAS_Vector& offset) override;
    virtual FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                               bool onEntity = true, double* dist = nullptr,
                                               FAS_Entity** entity = nullptr) const override;
    virtual FAS_Vector getNearestMiddle(const FAS_Vector& coord, double* dist = nullptr, int middlePoints = 1) const override;
    virtual FAS_Vector getNearestDist(double distance, const FAS_Vector& coord, double* dist = nullptr) const override;
    virtual void calculateBorders() override;

    friend std::ostream& operator << (std::ostream& os, const FAS_Text& p);
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
protected:
    FAS_TextData data;

    // Text width used by the current contents of this text entity.
    double usedTextWidth;
    // Text height used by the current contents of this text entity.
    double usedTextHeight;
};
#endif
#endif
