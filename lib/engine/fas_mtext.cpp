/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/
#include "fas_mtext.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>
#include <QPainter>
#include <QFontDatabase>

#include "fas_graphicview.h"
#include "fas_math.h"
#include "fas_painter.h"

FAS_MTextData::FAS_MTextData(const FAS_Vector& _insertionPoint,
                             double _height,
                             double _width,
                             VAlign _valign,
                             HAlign _halign,
                             MTextDrawingDirection _drawingDirection,
                             MTextLineSpacingStyle _lineSpacingStyle,
                             double _lineSpacingFactor,
                             const QString& _text,
                             const QString& _style,
                             double _angle,
                             FAS2::UpdateMode _updateMode):
    insertionPoint(_insertionPoint)
  ,height(_height)
  ,width(_width)
  ,valign(_valign)
  ,halign(_halign)
  ,drawingDirection(_drawingDirection)
  ,lineSpacingStyle(_lineSpacingStyle)
  ,lineSpacingFactor(_lineSpacingFactor)
  ,text(_text)
  ,style(_style)
  ,angle(_angle)
  ,updateMode(_updateMode)
{
}

std::ostream& operator << (std::ostream& os, const FAS_MTextData& td)
{
    os << "("
       <<td.insertionPoint<<','
      <<td.height<<','
     <<td.width<<','
    <<td.valign<<','
    <<td.halign<<','
    <<td.drawingDirection<<','
    <<td.lineSpacingStyle<<','
    <<td.lineSpacingFactor<<','
    <<td.text.toLatin1().data() <<','
    <<td.style.toLatin1().data()<<','
    <<td.angle<<','
    <<td.updateMode<<','
    <<")";
    return os;
}

FAS_MText::FAS_MText(FAS_EntityContainer* parent,
                     const FAS_MTextData& d)
    : data(d)
{
    usedTextHeight = 0.0;
    usedTextWidth = 0.0;
    setText(data.text);
    update();
}

FAS_Entity* FAS_MText::clone() const
{
    FAS_MText* t = new FAS_MText(*this);
    t->initId();
    return t;
}

void FAS_MText::setText(const QString& t)
{
    data.text = t;
    // handle some special flags embedded in the text:
    if (data.text.left(4)=="\\A0;")
    {
        data.text = data.text.mid(4);
        data.valign = FAS_MTextData::VABottom;
    }
    else if (data.text.left(4)=="\\A1;")
    {
        data.text = data.text.mid(4);
        data.valign = FAS_MTextData::VAMiddle;
    }
    else if (data.text.left(4)=="\\A2;")
    {
        data.text = data.text.mid(4);
        data.valign = FAS_MTextData::VATop;
    }
    if (data.updateMode==FAS2::Update)
    {
        update();
    }
}

int FAS_MText::getAlignment()
{
    if (data.valign==FAS_MTextData::VATop)
    {
        if (data.halign==FAS_MTextData::HALeft)
        {
            return 1;
        }
        else if (data.halign==FAS_MTextData::HACenter)
        {
            return 2;
        }
        else if (data.halign==FAS_MTextData::HARight)
        {
            return 3;
        }
    }
    else if (data.valign==FAS_MTextData::VAMiddle)
    {
        if (data.halign==FAS_MTextData::HALeft)
        {
            return 4;
        }
        else if (data.halign==FAS_MTextData::HACenter)
        {
            return 5;
        }
        else if (data.halign==FAS_MTextData::HARight)
        {
            return 6;
        }
    }
    else if (data.valign==FAS_MTextData::VABottom)
    {
        if (data.halign==FAS_MTextData::HALeft)
        {
            return 7;
        }
        else if (data.halign==FAS_MTextData::HACenter)
        {
            return 8;
        }
        else if (data.halign==FAS_MTextData::HARight)
        {
            return 9;
        }
    }
    return 1;
}

void FAS_MText::setAlignment(int a)
{
    switch (a%3)
    {
    default:
    case 1:
        data.halign = FAS_MTextData::HALeft;
        break;
    case 2:
        data.halign = FAS_MTextData::HACenter;
        break;
    case 0:
        data.halign = FAS_MTextData::HARight;
        break;
    }

    switch ((int)ceil(a/3.0))
    {
    default:
    case 1:
        data.valign = FAS_MTextData::VATop;
        break;
    case 2:
        data.valign = FAS_MTextData::VAMiddle;
        break;
    case 3:
        data.valign = FAS_MTextData::VABottom;
        break;
    }
}

/**
 * Updates the Inserts (letters) of this text. Called when the
 * text or it's data, position, alignment, .. changes.
 * This method also updates the usedTextWidth / usedTextHeight property.
 */
void FAS_MText::update()
{
    calculateBorders();
}

FAS_Vector FAS_MText::getNearestEndpoint(const FAS_Vector& coord, double* dist)const {
    if (dist) {
        *dist = data.insertionPoint.distanceTo(coord);
    }
    return data.insertionPoint;
}


FAS_VectorSolutions FAS_MText::getRefPoints() const{
    return FAS_VectorSolutions({data.insertionPoint});
}

void FAS_MText::move(const FAS_Vector& offset) {
    data.insertionPoint.move(offset);
    update();
}



void FAS_MText::rotate(const FAS_Vector& center, const double& angle) {
    FAS_Vector angleVector(angle);
    data.insertionPoint.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angle);
}
void FAS_MText::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    data.insertionPoint.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angleVector.angle());
}

void FAS_MText::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    data.insertionPoint.scale(center, factor);
    data.width*=factor.x;
    data.height*=factor.x;
    update();
}

void FAS_MText::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    data.insertionPoint.mirror(axisPoint1, axisPoint2);
    //double ang = axisPoint1.angleTo(axisPoint2);
    bool readable = FAS_Math::isAngleReadable(data.angle);

    FAS_Vector vec = FAS_Vector::polar(1.0, data.angle);
    vec.mirror(FAS_Vector(0.0,0.0), axisPoint2-axisPoint1);
    data.angle = vec.angle();

    bool corr;
    data.angle = FAS_Math::makeAngleReadable(data.angle, readable, &corr);

    if (corr) {
        if (data.halign==FAS_MTextData::HALeft) {
            data.halign=FAS_MTextData::HARight;
        } else if (data.halign==FAS_MTextData::HARight) {
            data.halign=FAS_MTextData::HALeft;
        }
    } else {
        if (data.valign==FAS_MTextData::VATop) {
            data.valign=FAS_MTextData::VABottom;
        } else if (data.valign==FAS_MTextData::VABottom) {
            data.valign=FAS_MTextData::VATop;
        }
    }
    update();
}

bool FAS_MText::hasEndpointsWithinWindow(const FAS_Vector& /*v1*/, const FAS_Vector& /*v2*/) {
    return false;
}

/**
 * Implementations must stretch the given range of the entity
 * by the given offset.
 */
void FAS_MText::stretch(const FAS_Vector& firstCorner, const FAS_Vector& secondCorner, const FAS_Vector& offset) {

    if (getMin().isInWindow(firstCorner, secondCorner) &&
            getMax().isInWindow(firstCorner, secondCorner)) {

        move(offset);
    }
}

// Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_MText& p) {
    os << " Text: " << p.getData() << "\n";
    return os;
}

void FAS_MText::draw(FAS_Painter* painter, FAS_GraphicView* view, double& /*patternOffset*/)
{

    if (!(painter && view)) {
        return;
    }
    COMMONDEF->drawEntityCount++;


    if (!view->isPrintPreview() && !view->isPrinting())
    {
        if (view->isPanning() || view->toGuiDY(getHeight()) < 4)
        {
            painter->drawRect(view->toGui(getMin()), view->toGui(getMax()));
            return;
        }
    }

    double xLeft = view->toGui(minV).x;
    double yTop = view->toGui(maxV).y;
    double height = this->getHeight() * 1.5;
    double width = height * this->getText().count() * 1.5;
    QFont font("Courier", height * view->getFactor().y * 0.55);
    painter->setFont(font);
    painter->drawTextH(xLeft, yTop, view->getFactor().x * width, view->getFactor().y * height, getText());
}

FAS_Vector FAS_MText::getNearestPointOnEntity(const FAS_Vector& coord, bool onEntity, double* dist, FAS_Entity** entity) const
{
    if(entity)
    {
        *entity = const_cast<FAS_MText*>(this);
    }
    double distance = 0.0;
    FAS_Vector result = coord;
    if(coord.isInWindow(minV,maxV))
    {
        distance = 0.0;
    }else
    {
        FAS_Vector middle = (minV + maxV) * 0.5;
        distance = coord.distanceTo(middle);
        result = middle;
    }

    if(dist)
    {
        *dist = distance;
    }
    return result;
}

FAS_Vector FAS_MText::getNearestMiddle(const FAS_Vector& coord, double* dist, int middlePoints) const
{
    middlePoints = 1;
    FAS_Vector middle = data.insertionPoint + FAS_Vector(data.width, data.height) * 0.5;
    if(dist)
    {
        *dist = coord.distanceTo(middle);
    }
    return middle;
}

FAS_Vector FAS_MText::getNearestDist(double distance, const FAS_Vector& coord, double* dist) const
{
    FAS_Vector middle = data.insertionPoint + FAS_Vector(data.width, data.height) * 0.5;
    if(dist)
    {
        *dist = coord.distanceTo(middle);
    }
    return middle;
}

FAS_Vector FAS_MText::getMiddlePoint() const
{
    return (getMin() + getMax()) * 0.5;
}

void FAS_MText::calculateBorders()
{
    double left = 0.0, right = 0.0, bottom = 0.0, top = 0.0;
    int rowNum=data.text.split("\r\n").length() - 1;
    int height=data.height;
    double width = data.height * data.text.size() * 0.6;
    switch (data.halign) {
    case FAS_MTextData::HALeft:
        left = data.insertionPoint.x;
        right = left + width;
        break;
    case FAS_MTextData::HARight:
        right = data.insertionPoint.x;
        left = right - width;
        break;
    case FAS_MTextData::HACenter:
        left = data.insertionPoint.x - 0.5 * width;
        right = data.insertionPoint.x + 0.5 * width;
        break;
    default:
        left = data.insertionPoint.x;
        right = left + width;
        break;
    }
    switch (data.valign) {
    case FAS_MTextData::VABottom:
        bottom = data.insertionPoint.y;
        top = bottom + height;
        break;
    case FAS_MTextData::VATop:
        top = data.insertionPoint.y;
        bottom = top - height;
        break;
    case FAS_MTextData::VAMiddle:
        top = data.insertionPoint.y + height * 0.5;
        bottom = data.insertionPoint.y - height * 0.5;
        break;
    default:
        bottom = data.insertionPoint.y;
        top = bottom + height;
        break;
    }

    minV = FAS_Vector(left, bottom);
    maxV = FAS_Vector(right, top);
}
void FAS_MText::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center1, const double& angle,const FAS_Vector& factor){

    FAS_Vector insertionPoint=getInsertionPoint();
    FAS_Vector bkinsertionPoint=data.bkinsertionPoint;
    if(!bkinsertionPoint.valid)
    {
        data.bkinsertionPoint=insertionPoint;
        data.bkwidth=data.width;
        data.bkheight=data.height;
        data.bkangle=data.angle;
    }else{
        data.insertionPoint=data.bkinsertionPoint;
        data.width=data.bkwidth;
        data.height=data.bkheight;
        data.angle=data.bkangle;
    }
}

void FAS_MText::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){
   data.insertionPoint.move(offset);

   FAS_Vector angleVector(angle);
   data.insertionPoint.rotate(center, angleVector);
   data.angle = FAS_Math::correctAngle(data.angle+angle);


   data.insertionPoint.scale(center, factor);
   data.width*=factor.x;
   data.height*=factor.x;

}
