/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_DIMRADIAL_H
#define FAS_DIMRADIAL_H

#include "fas_dimension.h"

// Holds the data that defines a radial dimension entity.
struct FAS_DimRadialData {
    FAS_DimRadialData();
    FAS_DimRadialData(const FAS_Vector& definitionPoint, double leader);

    // Definition point.
    FAS_Vector definitionPoint;
    // Leader length.
    double leader;
};

std::ostream& operator << (std::ostream& os, const FAS_DimRadialData& dd);

class FAS_DimRadial : public FAS_Dimension {
public:
    FAS_DimRadial(FAS_EntityContainer* parent,
                  const FAS_DimensionData& d,
                  const FAS_DimRadialData& ed);

    FAS_Entity* clone() const override;

    FAS2::EntityType rtti() const override{
        return FAS2::EntityDimRadial;
    }

    FAS_DimRadialData getEData() const {
        return edata;
    }

    FAS_VectorSolutions getRefPoints() const override;

    QString getMeasuredLabel() override;

    void updateDim(bool autoText=false) override;

    FAS_Vector getDefinitionPoint() {
        return edata.definitionPoint;
    }
    double getLeader() {
        return edata.leader;
    }

    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;

    friend std::ostream& operator << (std::ostream& os,
                                      const FAS_DimRadial& d);

protected:
    // Extended data.
    FAS_DimRadialData edata;
};

#endif
