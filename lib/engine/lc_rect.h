/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef LC_RECT_H
#define LC_RECT_H
#include "fas_vector.h"
#include <array>

namespace lc {
namespace geo {

typedef FAS_Vector Coordinate;
class Area
{

public:
    Area();
    // Create a new Area.
    Area(const Coordinate& coordA, const Coordinate& coordB);
    //given at a coordinate with a given width and height
    explicit Area(const Coordinate& coord, double width, double height);

    //Return the smalles corner (closest to (0,0,0) )
    const Coordinate& minP() const;

    // Return the heigest corner
    const Coordinate& maxP() const;
    // topLeftCorner return the upperLeftCorner coordinates
    Coordinate upperLeftCorner() const;
    Coordinate upperRightCorner() const;
    // return the lowerRight coordinates
    Coordinate lowerLeftCorner() const;
    Coordinate lowerRightCorner() const;

    // Returns the wid th of this area
    double width() const;

    //Returns the height f this area
    double height() const;

    //Test of a specific point lies within a area
    bool inArea(const Coordinate& point, double tolerance = 0.) const;

    //test if this object's fit's fully in area
    bool inArea(const Area& area) const;

    //returns true if any overlap is happening between the two area's, even if otherArea fit's within this area
    bool overlaps(const Area& otherArea) const;

    //count the number of corners this object has in otherArea
    short numCornersInside(const Area& otherArea) const;

    // two area's and expand if required to largest containing area
    Area merge(const Area& other) const;

    // two area's and expand if required to largest containing area
    Area merge(const Coordinate& other) const;

    // two area's and expand if required to largest containing area
    Area intersection(const Area& other, double tolerance = 0.) const;

     // return true if closest distance is smaller than or equal to tolerance
    bool intersects(Area const& rhs, double tolerance = 0.) const;

    // vector of this area
    Coordinate top() const;

    //vector of this area
    Coordinate bottom() const;

    // left vector for this area
    Coordinate left() const;

    //right vector of this area
    Coordinate right() const;

    // Increase the area on each side by increaseBy
    Area increaseBy (double increaseBy) const;
    //vertices generate vertices of the rectangular area
    std::array<Coordinate, 4> vertices() const;

private:
    static Coordinate Vector(Coordinate const& p, Coordinate const& q);
    friend std::ostream& operator<<(std::ostream& os, const Area& area);

private:
    Coordinate _minP;
    Coordinate _maxP;
};
}
}

using LC_Rect = lc::geo::Area;
#endif // LC_RECT_H
