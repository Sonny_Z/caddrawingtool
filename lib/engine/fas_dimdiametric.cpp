/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_dimdiametric.h"

//sort with alphabetical order
#include "fas_graphic.h"
#include "fas_units.h"
#include <iostream>


FAS_DimDiametricData::FAS_DimDiametricData():
    definitionPoint(false),
    leader(0.0)
{}

FAS_DimDiametricData::FAS_DimDiametricData(const FAS_Vector& _definitionPoint,
                                           double _leader):
    definitionPoint(_definitionPoint)
  ,leader(_leader)
{
}

std::ostream& operator << (std::ostream& os,
                           const FAS_DimDiametricData& dd) {
    os << "(" << dd.definitionPoint << "," << dd.leader << ")";
    return os;
}

FAS_DimDiametric::FAS_DimDiametric(FAS_EntityContainer* parent,
                                   const FAS_DimensionData& d,
                                   const FAS_DimDiametricData& ed)
    : FAS_Dimension(parent, d), edata(ed) {

    calculateBorders();
}

FAS_Entity* FAS_DimDiametric::clone() const
{
    FAS_DimDiametric* d = new FAS_DimDiametric(*this);
    d->setOwner(isOwner());
    d->initId();
    d->detach();
    return d;
}

//return Automatically created label for the default measurement of this dimension.
QString FAS_DimDiametric::getMeasuredLabel()
{
    double dist = data.definitionPoint.distanceTo(edata.definitionPoint) * getGeneralFactor();
    FAS_Graphic* graphic = getGraphic();
    QString ret;
    if (graphic)
    {
        int dimlunit = getGraphicVariableInt("$DIMLUNIT", 2);
        int dimdec = getGraphicVariableInt("$DIMDEC", 4);
        int dimzin = getGraphicVariableInt("$DIMZIN", 1);
        FAS2::LinearFormat format = graphic->getLinearFormat(dimlunit);
        ret = FAS_Units::formatLinear(dist, FAS2::None, format, dimdec);
        if (format == FAS2::Decimal)
            ret = stripZerosLinear(ret, dimzin);
        if (dimlunit==2)
        {
            if (getGraphicVariableInt("$DIMDSEP", 0) == 44)
                ret.replace(QChar('.'), QChar(','));
        }
    }
    else
    {
        ret = QString("%1").arg(dist);
    }

    return ret;
}

FAS_VectorSolutions FAS_DimDiametric::getRefPoints() const
{
    return FAS_VectorSolutions({edata.definitionPoint,
                                data.definitionPoint, data.middleOfText});
}


/*
 * Updates the sub entities of this dimension. Called when the
 * dimension or the position, alignment, .. changes.
 */
void FAS_DimDiametric::updateDim(bool autoText)
{
    clear();
    if (isUndone())
    {
        return;
    }
    // dimension line:
    updateCreateDimensionLine(data.definitionPoint, edata.definitionPoint, true, true, autoText);
    calculateBorders();
}



void FAS_DimDiametric::move(const FAS_Vector& offset)
{
    FAS_Dimension::move(offset);
    edata.definitionPoint.move(offset);
    update();
}



void FAS_DimDiametric::rotate(const FAS_Vector& center, const double& angle)
{
    rotate(center,FAS_Vector(angle));
}

void FAS_DimDiametric::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    FAS_Dimension::rotate(center, angleVector);

    edata.definitionPoint.rotate(center, angleVector);
    update();
}


void FAS_DimDiametric::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    FAS_Dimension::scale(center, factor);

    edata.definitionPoint.scale(center, factor);
    edata.leader*=factor.x;
    update();
}



void FAS_DimDiametric::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    FAS_Dimension::mirror(axisPoint1, axisPoint2);

    edata.definitionPoint.mirror(axisPoint1, axisPoint2);
    update();
}



void FAS_DimDiametric::moveRef(const FAS_Vector& ref, const FAS_Vector& offset)
{
    if (ref.distanceTo(edata.definitionPoint)<1.0e-4)
    {
        FAS_Vector c = (edata.definitionPoint + data.definitionPoint)/2.0;
        double d = c.distanceTo(edata.definitionPoint);
        double a = c.angleTo(edata.definitionPoint + offset);

        FAS_Vector v = FAS_Vector::polar(d, a);
        edata.definitionPoint = c + v;
        data.definitionPoint = c - v;
        updateDim(true);
    }
    else if (ref.distanceTo(data.definitionPoint)<1.0e-4)
    {
        FAS_Vector c = (edata.definitionPoint + data.definitionPoint)/2.0;
        double d = c.distanceTo(data.definitionPoint);
        double a = c.angleTo(data.definitionPoint + offset);

        FAS_Vector v = FAS_Vector::polar(d, a);
        data.definitionPoint = c + v;
        edata.definitionPoint = c - v;
        updateDim(true);
    }
    else if (ref.distanceTo(data.middleOfText)<1.0e-4)
    {
        data.middleOfText.move(offset);
        updateDim(false);
    }
}



// Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_DimDiametric& d) {
    os << " DimDiametric: " << d.getData() << "\n" << d.getEData() << "\n";
    return os;
}

