/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_dimaligned.h"

//sort with alphabetical order
#include <cmath>
#include "fas_constructionline.h"
#include "fas_graphic.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_units.h"
#include <iostream>

FAS_DimAlignedData::FAS_DimAlignedData():
    extensionPoint1(false),
    extensionPoint2(false)
{}

FAS_DimAlignedData::FAS_DimAlignedData(const FAS_Vector& _extensionPoint1,
                                       const FAS_Vector& _extensionPoint2):
    extensionPoint1(_extensionPoint1)
  ,extensionPoint2(_extensionPoint2)
{
}

std::ostream& operator << (std::ostream& os,
                           const FAS_DimAlignedData& dd) {
    os << "(" << dd.extensionPoint1 << "/" << dd.extensionPoint1 << ")";
    return os;
}

FAS_DimAligned::FAS_DimAligned(FAS_EntityContainer* parent,
                               const FAS_DimensionData& d,
                               const FAS_DimAlignedData& ed)
    : FAS_Dimension(parent, d), edata(ed) {

    calculateBorders();
}

FAS_Entity* FAS_DimAligned::clone() const{
    FAS_DimAligned* d = new FAS_DimAligned(*this);
    d->setOwner(isOwner());
    d->initId();
    d->detach();
    return d;
}

FAS_VectorSolutions FAS_DimAligned::getRefPoints() const
{
    return FAS_VectorSolutions({edata.extensionPoint1, edata.extensionPoint2,
                                data.definitionPoint, data.middleOfText});
}

/**
 * @return Automatically creted label for the default
 * measurement of this dimension.
 */
QString FAS_DimAligned::getMeasuredLabel() {
    double dist = edata.extensionPoint1.distanceTo(edata.extensionPoint2) * getGeneralFactor();

    FAS_Graphic* graphic = getGraphic();
    QString ret;
    if (graphic) {
        int dimlunit = getGraphicVariableInt("$DIMLUNIT", 2);
        int dimdec = getGraphicVariableInt("$DIMDEC", 4);
        int dimzin = getGraphicVariableInt("$DIMZIN", 1);
        FAS2::LinearFormat format = graphic->getLinearFormat(dimlunit);

        ret = FAS_Units::formatLinear(dist, FAS2::None, format, dimdec);
        if (format == FAS2::Decimal)
            ret = stripZerosLinear(ret, dimzin);
        //verify if units are decimal and comma separator
        if (dimlunit==2){
            if (getGraphicVariableInt("$DIMDSEP", 0) == 44)
                ret.replace(QChar('.'), QChar(','));
        }
    }
    else {
        ret = QString("%1").arg(dist);
    }
    return ret;
}


FAS_DimAlignedData const& FAS_DimAligned::getEData() const {
    return edata;
}

FAS_Vector const& FAS_DimAligned::getExtensionPoint1() const {
    return edata.extensionPoint1;
}

FAS_Vector const& FAS_DimAligned::getExtensionPoint2() const {
    return edata.extensionPoint2;
}

/*
 * Updates the sub entities of this dimension. Called when the
 * text or the position, alignment, .. changes.
 */
void FAS_DimAligned::updateDim(bool autoText)
{
    clear();

    if (isUndone()) {
        return;
    }

    // general scale (DIMSCALE)
    double dimscale = getGeneralScale();
    // distance from entities (DIMEXO)
    double dimexo = getExtensionLineOffset()*dimscale;
    // definition line definition (DIMEXE)
    double dimexe = getExtensionLineExtension()*dimscale;
    // text height (DIMTXT)
    //double dimtxt = getTextHeight();
    // text distance to line (DIMGAP)
    //double dimgap = getDimensionLineGap();

    // Angle from extension endpoints towards dimension line
    double extAngle = edata.extensionPoint2.angleTo(data.definitionPoint);
    // extension lines length
    double extLength = edata.extensionPoint2.distanceTo(data.definitionPoint);

    if (getFixedLengthOn()){
        double dimfxl = getFixedLength();
        if (extLength-dimexo > dimfxl)
            dimexo =  extLength - dimfxl;
    }

    FAS_Vector v1 = FAS_Vector::polar(dimexo, extAngle);
    FAS_Vector v2 = FAS_Vector::polar(dimexe, extAngle);
    FAS_Vector e1 = FAS_Vector::polar(1.0, extAngle);

    FAS_Pen pen(getExtensionLineColor(),
                getExtensionLineWidth(),
                FAS2::LineByBlock);

    // Extension line 1:
    FAS_Line* line = new FAS_Line{this,
            edata.extensionPoint1 + v1,
            edata.extensionPoint1 + e1*extLength + v2};
    line->setPen(pen);
    line->setLayer(nullptr);
    addEntity(line);

    // Extension line 2:
    line = new FAS_Line{this,
            edata.extensionPoint2 + v1,
            edata.extensionPoint2 + e1*extLength + v2};
    line->setPen(pen);
    line->setLayer(nullptr);
    addEntity(line);

    // Dimension line:
    updateCreateDimensionLine(edata.extensionPoint1 + e1*extLength,
                              edata.extensionPoint2 + e1*extLength,
                              true, true, autoText);

    calculateBorders();
}

void FAS_DimAligned::updateDimPoint(){
    // temporary construction line
    FAS_ConstructionLine tmpLine( nullptr,
                                  FAS_ConstructionLineData(edata.extensionPoint1, edata.extensionPoint2));

    FAS_Vector tmpP1 = tmpLine.getNearestPointOnEntity(data.definitionPoint);
    data.definitionPoint += edata.extensionPoint2 - tmpP1;
}


bool FAS_DimAligned::hasEndpointsWithinWindow(const FAS_Vector& v1, const FAS_Vector& v2) {
    return (edata.extensionPoint1.isInWindow(v1, v2) ||
            edata.extensionPoint2.isInWindow(v1, v2));
}

void FAS_DimAligned::move(const FAS_Vector& offset) {
    FAS_Dimension::move(offset);

    edata.extensionPoint1.move(offset);
    edata.extensionPoint2.move(offset);
    update();
}

void FAS_DimAligned::rotate(const FAS_Vector& center, const double& angle) {
    rotate(center,FAS_Vector(angle));
}

void FAS_DimAligned::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    FAS_Dimension::rotate(center, angleVector);
    edata.extensionPoint1.rotate(center, angleVector);
    edata.extensionPoint2.rotate(center, angleVector);
    update();
}

void FAS_DimAligned::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    FAS_Dimension::scale(center, factor);

    edata.extensionPoint1.scale(center, factor);
    edata.extensionPoint2.scale(center, factor);
    update();
}

void FAS_DimAligned::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    FAS_Dimension::mirror(axisPoint1, axisPoint2);

    edata.extensionPoint1.mirror(axisPoint1, axisPoint2);
    edata.extensionPoint2.mirror(axisPoint1, axisPoint2);
    update();
}

void FAS_DimAligned::stretch(const FAS_Vector& firstCorner,
                             const FAS_Vector& secondCorner,
                             const FAS_Vector& offset)
{
    if (getMin().isInWindow(firstCorner, secondCorner) &&
            getMax().isInWindow(firstCorner, secondCorner)) {

        move(offset);
    }
    else {
        //FAS_Vector v = data.definitionPoint - edata.extensionPoint2;
        double len = edata.extensionPoint2.distanceTo(data.definitionPoint);
        double ang1 = edata.extensionPoint1.angleTo(edata.extensionPoint2)
                + M_PI_2;

        if (edata.extensionPoint1.isInWindow(firstCorner,
                                             secondCorner)) {
            edata.extensionPoint1.move(offset);
        }
        if (edata.extensionPoint2.isInWindow(firstCorner,
                                             secondCorner)) {
            edata.extensionPoint2.move(offset);
        }

        double ang2 = edata.extensionPoint1.angleTo(edata.extensionPoint2)
                + M_PI_2;

        double diff = FAS_Math::getAngleDifference(ang1, ang2);
        if (diff>M_PI) {
            diff-=2*M_PI;
        }

        if (fabs(diff)>M_PI_2) {
            ang2 = FAS_Math::correctAngle(ang2+M_PI);
        }

        FAS_Vector v = FAS_Vector::polar(len, ang2);
        data.definitionPoint = edata.extensionPoint2 + v;
    }
    updateDim(true);
}

void FAS_DimAligned::moveRef(const FAS_Vector& ref, const FAS_Vector& offset) {

    if (ref.distanceTo(data.definitionPoint)<1.0e-4) {
        FAS_ConstructionLine l(nullptr,
                               FAS_ConstructionLineData(edata.extensionPoint1,
                                                        edata.extensionPoint2));
        double d = l.getDistanceToPoint(data.definitionPoint+offset);
        double a = edata.extensionPoint2.angleTo(data.definitionPoint);
        double ad = FAS_Math::getAngleDifference(a,
                                                 edata.extensionPoint2.angleTo(data.definitionPoint+offset));

        if (fabs(ad)>M_PI_2 && fabs(ad)<3.0/2.0*M_PI) {
            a = FAS_Math::correctAngle(a+M_PI);
        }

        FAS_Vector v = FAS_Vector::polar(d, a);
        data.definitionPoint = edata.extensionPoint2 + v;
        updateDim(true);
    }
    else if (ref.distanceTo(data.middleOfText)<1.0e-4) {
        data.middleOfText.move(offset);
        updateDim(false);
    }
    else if (ref.distanceTo(edata.extensionPoint1)<1.0e-4) {
        double a1 = edata.extensionPoint2.angleTo(edata.extensionPoint1);
        double a2 = edata.extensionPoint2.angleTo(edata.extensionPoint1+offset);
        double d1 = edata.extensionPoint2.distanceTo(edata.extensionPoint1);
        double d2 = edata.extensionPoint2.distanceTo(edata.extensionPoint1+offset);
        rotate(edata.extensionPoint2, a2-a1);
        if (fabs(d1)>1.0e-4) {
            scale(edata.extensionPoint2, FAS_Vector(d2/d1, d2/d1));
        }
        updateDim(true);
    }
    else if (ref.distanceTo(edata.extensionPoint2)<1.0e-4) {
        double a1 = edata.extensionPoint1.angleTo(edata.extensionPoint2);
        double a2 = edata.extensionPoint1.angleTo(edata.extensionPoint2+offset);
        double d1 = edata.extensionPoint1.distanceTo(edata.extensionPoint2);
        double d2 = edata.extensionPoint1.distanceTo(edata.extensionPoint2+offset);
        rotate(edata.extensionPoint1, a2-a1);
        if (fabs(d1)>1.0e-4) {
            scale(edata.extensionPoint1, FAS_Vector(d2/d1, d2/d1));
        }
        updateDim(true);
    }
}

std::ostream& operator << (std::ostream& os, const FAS_DimAligned& d) {
    os << " DimAligned: " << d.getData() << "\n" << d.getEData() << "\n";
    return os;
}
