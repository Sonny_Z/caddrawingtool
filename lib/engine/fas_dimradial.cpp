/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_dimradial.h"

//sort with alphabetical order
#include <cmath>
#include "fas_graphic.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_mtext.h"
#include "fas_solid.h"
#include "fas_units.h"
#include <iostream>

FAS_DimRadialData::FAS_DimRadialData():
    definitionPoint(false),
    leader(0.0)
{}

FAS_DimRadialData::FAS_DimRadialData(const FAS_Vector& _definitionPoint,
                                     double _leader):
    definitionPoint(_definitionPoint)
  ,leader(_leader)
{
}

std::ostream& operator << (std::ostream& os,
                           const FAS_DimRadialData& dd) {
    os << "(" << dd.definitionPoint << "/" << dd.leader << ")";
    return os;
}

FAS_DimRadial::FAS_DimRadial(FAS_EntityContainer* parent,
                             const FAS_DimensionData& d,
                             const FAS_DimRadialData& ed)
    : FAS_Dimension(parent, d), edata(ed) {}

FAS_Entity* FAS_DimRadial::clone() const {
    FAS_DimRadial* d = new FAS_DimRadial(*this);
    d->setOwner(isOwner());
    d->initId();
    d->detach();
    return d;
}


//return Automatically created label for the default measurement of this dimension.
QString FAS_DimRadial::getMeasuredLabel() {

    // Definitive dimension line:
    double dist = data.definitionPoint.distanceTo(edata.definitionPoint) * getGeneralFactor();
    FAS_Graphic* graphic = getGraphic();
    QString ret;
    if (graphic)
    {
        int dimlunit = getGraphicVariableInt("$DIMLUNIT", 2);
        int dimdec = getGraphicVariableInt("$DIMDEC", 4);
        int dimzin = getGraphicVariableInt("$DIMZIN", 1);
        FAS2::LinearFormat format = graphic->getLinearFormat(dimlunit);
        ret = FAS_Units::formatLinear(dist, FAS2::None, format, dimdec);
        if (format == FAS2::Decimal)
            ret = stripZerosLinear(ret, dimzin);
        //verify if units are decimal and comma separator
        if (dimlunit==2){
            if (getGraphicVariableInt("$DIMDSEP", 0) == 44)
                ret.replace(QChar('.'), QChar(','));
        }
    }
    else
    {
        ret = QString("%1").arg(dist);
    }

    return ret;
}


FAS_VectorSolutions FAS_DimRadial::getRefPoints() const
{
    return FAS_VectorSolutions({edata.definitionPoint, data.definitionPoint, data.middleOfText});
}


/*
 * Updates the sub entities of this dimension. Called when the
 * dimension or the position, alignment, .. changes.
 */
void FAS_DimRadial::updateDim(bool autoText)
{
    clear();
    if (isUndone())
    {
        return;
    }
    // general scale (DIMSCALE)
    double dimscale = getGeneralScale();

    FAS_Vector p1 = data.definitionPoint;
    FAS_Vector p2 = edata.definitionPoint;
    double angle = p1.angleTo(p2);

    // text height (DIMTXT)
    double dimtxt = getTextHeight()*dimscale;

    FAS_Pen pen(getDimensionLineColor(),
                getDimensionLineWidth(),
                FAS2::LineByBlock);
    FAS_MTextData textData;
    textData = FAS_MTextData(FAS_Vector(0.0,0.0),
                             dimtxt, 30.0,
                             FAS_MTextData::VAMiddle,
                             FAS_MTextData::HACenter,
                             FAS_MTextData::LeftToRight,
                             FAS_MTextData::Exact,
                             1.0,
                             getLabel(),
                             getTextStyle(),
                             0.0);
    FAS_MText* text = new FAS_MText(this, textData);
    double textWidth = text->getSize().x;
    double tick_size = getTickSize()*dimscale;
    double arrow_size = getArrowSize()*dimscale;
    double length = p1.distanceTo(p2); // line length
    bool outsideArrow = false;
    if (tick_size == 0 && arrow_size != 0)
    {
        // do we have to put the arrow / text outside of the arc?
        outsideArrow = (length < arrow_size*2+textWidth);
        double arrowAngle;

        if (outsideArrow) {
            length += arrow_size*2 + textWidth;
            arrowAngle = angle+M_PI;
        } else {
            arrowAngle = angle;
        }

        // create arrow:
        FAS_SolidData sd;
        FAS_Solid* arrow;

        arrow = new FAS_Solid(this, sd);
        arrow->shapeArrow(p2, arrowAngle, arrow_size);
        arrow->setPen(pen);
        arrow->setLayer(nullptr);
        addEntity(arrow);
    }

    FAS_Vector p3 = FAS_Vector::polar(length, angle);
    p3 += p1;

    // Create dimension line:
    FAS_Line* dimensionLine = new FAS_Line{this, p1, p3};
    dimensionLine->setPen(pen);
    dimensionLine->setLayer(nullptr);
    addEntity(dimensionLine);

    FAS_Vector distV;
    double textAngle;

    // text distance to line (DIMGAP)
    double dimgap = getDimensionLineGap()*dimscale;

    // rotate text so it's readable from the bottom or right (ISO)
    // quadrant 1 & 4
    if (angle > M_PI_2*3.0+0.001 || angle < M_PI_2+0.001)
    {
        distV.setPolar(dimgap + dimtxt/2.0, angle+M_PI_2);
        textAngle = angle;
    }
    // quadrant 2 & 3
    else
    {
        distV.setPolar(dimgap + dimtxt/2.0, angle-M_PI_2);
        textAngle = angle+M_PI;
    }

    // move text label:
    FAS_Vector textPos;

    if (data.middleOfText.valid && !autoText) {
        textPos = data.middleOfText;
    } else {
        if (outsideArrow) {
            textPos.setPolar(length-textWidth/2.0-arrow_size, angle);
        } else {
            textPos.setPolar(length/2.0, angle);
        }
        textPos += p1;
        // move text away from dimension line:
        textPos += distV;
        data.middleOfText = textPos;
    }

    text->rotate({0., 0.}, textAngle);
    text->move(textPos);

    text->setPen(FAS_Pen(getTextColor(), FAS2::WidthByBlock, FAS2::SolidLine));
    text->setLayer(nullptr);
    addEntity(text);

    calculateBorders();
}



void FAS_DimRadial::move(const FAS_Vector& offset) {
    FAS_Dimension::move(offset);

    edata.definitionPoint.move(offset);
    update();
}



void FAS_DimRadial::rotate(const FAS_Vector& center, const double& angle) {
    rotate(center,FAS_Vector(angle));
}


void FAS_DimRadial::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    FAS_Dimension::rotate(center, angleVector);

    edata.definitionPoint.rotate(center, angleVector);
    update();
}



void FAS_DimRadial::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    FAS_Dimension::scale(center, factor);

    edata.definitionPoint.scale(center, factor);
    edata.leader*=factor.x;
    update();
}

void FAS_DimRadial::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    FAS_Dimension::mirror(axisPoint1, axisPoint2);

    edata.definitionPoint.mirror(axisPoint1, axisPoint2);
    update();
}

void FAS_DimRadial::moveRef(const FAS_Vector& ref, const FAS_Vector& offset) {

    if (ref.distanceTo(edata.definitionPoint)<1.0e-4) {
        double d = data.definitionPoint.distanceTo(edata.definitionPoint);
        double a = data.definitionPoint.angleTo(edata.definitionPoint + offset);

        FAS_Vector v = FAS_Vector::polar(d, a);
        edata.definitionPoint = data.definitionPoint + v;
        updateDim(true);
    }
    else if (ref.distanceTo(data.middleOfText)<1.0e-4) {
        data.middleOfText.move(offset);
        updateDim(false);
    }
}

// Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_DimRadial& d) {
    os << " DimRadial: " << d.getData() << "\n" << d.getEData() << "\n";
    return os;
}
