/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_dimension.h"

//sort with alphabetical order
#include "fas_dxfprocessor.h"
#include "fas_information.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_solid.h"
#include "fas_units.h"
#include <iostream>

FAS_DimensionData::FAS_DimensionData():
    definitionPoint(false),
    middleOfText(false),
    valign(FAS_MTextData::VABottom),
    halign(FAS_MTextData::HALeft),
    lineSpacingStyle(FAS_MTextData::Exact),
    lineSpacingFactor(0.0),
    text(""),
    style(""),
    angle(0.0)
{}

/**
 * Constructor with initialisation.
 *
 * @param definitionPoint Definition point.
 * @param middleOfText Middle point of dimension text.
 * @param valign Vertical alignment.
 * @param halign Horizontal alignment.
 * @param lineSpacingStyle Line spacing style.
 * @param lineSpacingFactor Line spacing factor.
 * @param text Text string entered explicitly by user or null
 *         or "<>" for the actual measurement or " " (one blank space).
 *         for supressing the text.
 * @param style Dimension style name.
 * @param angle Rotation angle of dimension text away from
 *         default orientation.
 */
FAS_DimensionData::FAS_DimensionData(const FAS_Vector& _definitionPoint,
                                     const FAS_Vector& _middleOfText,
                                     FAS_MTextData::VAlign _valign,
                                     FAS_MTextData::HAlign _halign,
                                     FAS_MTextData::MTextLineSpacingStyle _lineSpacingStyle,
                                     double _lineSpacingFactor,
                                     QString _text,
                                     QString _style,
                                     double _angle):
    definitionPoint(_definitionPoint)
  ,middleOfText(_middleOfText)
  ,valign(_valign)
  ,halign(_halign)
  ,lineSpacingStyle(_lineSpacingStyle)
  ,lineSpacingFactor(_lineSpacingFactor)
  ,text(_text)
  ,style(_style)
  ,angle(_angle)
{
}

std::ostream& operator << (std::ostream& os,
                           const FAS_DimensionData& dd) {
    os << "("
       << dd.definitionPoint<<','
       <<dd.middleOfText<<','
      <<dd.valign<<','
     <<dd.halign<<','
    <<dd.lineSpacingStyle<<','
    <<dd.lineSpacingFactor<<','
    <<dd.text.toLatin1().data() <<','
    <<dd.style.toLatin1().data()<<','
    <<dd.angle
    << ")";
    return os;
}

/**
 * Constructor.
 */
FAS_Dimension::FAS_Dimension(FAS_EntityContainer* parent,
                             const FAS_DimensionData& d)
    : FAS_EntityContainer(parent), data(d) {
}

FAS_Vector FAS_Dimension::getNearestRef( const FAS_Vector& coord,
                                         double* dist /*= nullptr*/) const
{
    // override the FAS_EntityContainer methode
    // use FAS_Entity instead for refpoint dragging
    return FAS_Entity::getNearestRef( coord, dist);
}

FAS_Vector FAS_Dimension::getNearestSelectedRef( const FAS_Vector& coord,
                                                 double* dist /*= nullptr*/) const
{
    // override the FAS_EntityContainer methode
    // use FAS_Entity instead for refpoint dragging
    return FAS_Entity::getNearestSelectedRef( coord, dist);
}


/**
 * @return Dimension text. Either a text the user defined or
 *         the measured text.
 *
 * @param resolve false: return plain value. true: return measured
 *      label if appropriate.
 * @see getMeasuredLabel
 */
QString FAS_Dimension::getLabel(bool resolve) {
    if (!resolve) {
        return data.text;
    }

    QString ret="";

    // One space suppresses the text:
    if (data.text==" ") {
        ret = "";
    }

    // No text prints actual measurement:
    else if (data.text=="") {
        ret = getMeasuredLabel();
    }

    // Others print the text (<> is replaced by the measurement)
    else {
        ret = data.text;
        ret = ret.replace(QString("<>"), getMeasuredLabel());
    }

    return ret;
}


/**
 * Sets a new text for the label.
 */
void FAS_Dimension::setLabel(const QString& l) {
    data.text = l;
}



/**
 * Creates a dimensioning line (line with one, two or no arrows and a text).
 *
 * @param forceAutoText Automatically reposition the text label.
 */
void FAS_Dimension::updateCreateDimensionLine(const FAS_Vector& p1,
                                              const FAS_Vector& p2, bool arrow1, bool arrow2, bool forceAutoText) {

    // general scale (DIMSCALE)
    double dimscale = getGeneralScale();
    // text height (DIMTXT)
    double dimtxt = getTextHeight()*dimscale;
    // text distance to line (DIMGAP)
    double dimgap = getDimensionLineGap()*dimscale;

    // length of dimension line:
    double distance = p1.distanceTo(p2);
    // arrow size:
    double arrowSize = getArrowSize()*dimscale;

    // do we have to put the arrows outside of the line?
    bool outsideArrows = (distance<arrowSize*2.5);

    // arrow angles:
    double arrowAngle1, arrowAngle2;

    FAS_Pen pen(getDimensionLineColor(),
                getDimensionLineWidth(),
                FAS2::LineByBlock);

    // Create dimension line:
    FAS_Line* dimensionLine = new FAS_Line{this, p1, p2};
    dimensionLine->setPen(pen);
    //    dimensionLine->setPen(FAS_Pen(FAS2::FlagInvalid));
    dimensionLine->setLayer(nullptr);
    addEntity(dimensionLine);

    if (outsideArrows==false) {
        arrowAngle1 = dimensionLine->getAngle2();
        arrowAngle2 = dimensionLine->getAngle1();
    } else {
        arrowAngle1 = dimensionLine->getAngle1();
        arrowAngle2 = dimensionLine->getAngle2();

        // extend dimension line outside arrows
        FAS_Vector dir = FAS_Vector::polar(arrowSize*2, arrowAngle2);
        dimensionLine->setStartpoint(p1 + dir);
        dimensionLine->setEndpoint(p2 - dir);
    }
    double dimtsz=getTickSize()*dimscale;
    if(dimtsz < 0.01) {
        //display arrow
        // Arrows:
        FAS_SolidData sd;
        FAS_Solid* arrow;

        if (arrow1) {
            // arrow 1
            arrow = new FAS_Solid(this, sd);
            arrow->shapeArrow(p1,
                              arrowAngle1,
                              arrowSize);
            //        arrow->setPen(FAS_Pen(FAS2::FlagInvalid));
            arrow->setPen(pen);
            arrow->setLayer(nullptr);
            addEntity(arrow);
        }

        if (arrow2) {
            // arrow 2:
            arrow = new FAS_Solid(this, sd);
            arrow->shapeArrow(p2,
                              arrowAngle2,
                              arrowSize);
            //        arrow->setPen(FAS_Pen(FAS2::FlagInvalid));
            arrow->setPen(pen);
            arrow->setLayer(nullptr);
            addEntity(arrow);
        }
    }else{
        //display ticks
        // Arrows:

        FAS_Line* tick;
        FAS_Vector tickVector = FAS_Vector::polar(dimtsz,arrowAngle1 + M_PI*0.25); //tick is 45 degree away

        if (arrow1) {
            // tick 1
            tick = new FAS_Line(this, p1-tickVector, p1+tickVector);
            tick->setPen(pen);
            //        tick->setPen(FAS_Pen(FAS2::FlagInvalid));
            tick->setLayer(nullptr);
            addEntity(tick);
        }

        if (arrow2) {
            // tick 2:
            tick = new FAS_Line(this, p2-tickVector, p2+tickVector);
            tick->setPen(pen);
            //        tick->setPen(FAS_Pen(FAS2::FlagInvalid));
            tick->setLayer(nullptr);
            addEntity(tick);
        }
    }
    // Text label:
    FAS_MTextData textData;
    FAS_Vector textPos;

    double dimAngle1 = dimensionLine->getAngle1();
    double textAngle;
    bool corrected=false;
    if (getAlignText())
        textAngle =0.0;
    else
        textAngle = FAS_Math::makeAngleReadable(dimAngle1, true, &corrected);

    if (data.middleOfText.valid && !forceAutoText) {
        textPos = data.middleOfText;
    } else {
        textPos = dimensionLine->getMiddlePoint();

        if (!getAlignText()) {
            // rotate text so it's readable from the bottom or right (ISO)
            // quadrant 1 & 4
            double const a = corrected?-M_PI_2:M_PI_2;
            FAS_Vector distV = FAS_Vector::polar(dimgap + dimtxt/2.0, dimAngle1+a);

            // move text away from dimension line:
            textPos+=distV;
        }
        //// the next update should still be able to adjust this
        ////   auto text position. leave it invalid
        data.middleOfText = textPos;
    }

    textData = FAS_MTextData(textPos,
                             dimtxt, 30.0,
                             FAS_MTextData::VAMiddle,
                             FAS_MTextData::HACenter,
                             FAS_MTextData::LeftToRight,
                             FAS_MTextData::Exact,
                             1.0,
                             getLabel(),
                             getTextStyle(),
                             //                           "standard",
                             textAngle);

    FAS_MText* text = new FAS_MText(this, textData);

    // move text to the side:
    FAS_Vector distH;
    if (text->getUsedTextWidth()>distance) {
        distH.setPolar(text->getUsedTextWidth()/2.0
                       +distance/2.0+dimgap, textAngle);
        text->move(distH);
    }
    text->setPen(FAS_Pen(getTextColor(), FAS2::WidthByBlock, FAS2::SolidLine));
    //    text->setPen(FAS_Pen(FAS2::FlagInvalid));
    text->setLayer(nullptr);
    //horizontal text, split dimensionLine
    if (getAlignText()) {
        double w =text->getUsedTextWidth()/2+dimgap;
        double h = text->getUsedTextHeight()/2+dimgap;
        FAS_Vector v1 = textPos - FAS_Vector{w, h};
        FAS_Vector v2 = textPos + FAS_Vector{w, h};
        FAS_EntityContainer c;
        c.addRectangle(v1, v2);
        FAS_VectorSolutions sol1;
        for(FAS_Entity* e: c) {
            sol1.push_back(
                        FAS_Information::getIntersection(dimensionLine, e, true)
                        );
        }

        //are text intersecting dimensionLine?
        if (sol1.size()>1) {
            //yes, split dimension line
            FAS_Line* dimensionLine2 =
                    static_cast<FAS_Line*>(dimensionLine->clone());
            v1 = sol1.get(0);
            v2 = sol1.get(1);
            if (p1.distanceTo(v1) < p1.distanceTo(v2)) {
                dimensionLine->setEndpoint(v1);
                dimensionLine2->setStartpoint(v2);
            } else {
                dimensionLine->setEndpoint(v2);
                dimensionLine2->setStartpoint(v1);
            }
            addEntity(dimensionLine2);
        }
    }

    addEntity(text);
}


/**
 * @return general factor for linear dimensions.
 */
double FAS_Dimension::getGeneralFactor() {
    return getGraphicVariable("$DIMLFAC", 1.0, 40);
}

/**
 * @return general scale for dimensions.
 */
double FAS_Dimension::getGeneralScale() {
    return getGraphicVariable("$DIMSCALE", 1.0, 40);
}

/**
 * @return arrow size in drawing units.
 */
double FAS_Dimension::getArrowSize() {
    return getGraphicVariable("$DIMASZ", 2.5, 40);
}

/**
 * @return tick size in drawing units.
 */
double FAS_Dimension::getTickSize() {
    return getGraphicVariable("$DIMTSZ", 0., 40);
}


/**
 * @return extension line overlength in drawing units.
 */
double FAS_Dimension::getExtensionLineExtension() {
    return getGraphicVariable("$DIMEXE", 1.25, 40);
}



/**
 * @return extension line offset from entities in drawing units.
 */
double FAS_Dimension::getExtensionLineOffset() {
    return getGraphicVariable("$DIMEXO", 0.625, 40);
}



/**
 * @return extension line gap to text in drawing units.
 */
double FAS_Dimension::getDimensionLineGap() {
    return getGraphicVariable("$DIMGAP", 0.625, 40);
}



/**
 * @return Dimension lables text height.
 */
double FAS_Dimension::getTextHeight() {
    return getGraphicVariable("$DIMTXT", 2.5, 40);
}


/**
 * @return Dimension labels alignement text true= horizontal, false= aligned.
 */
bool FAS_Dimension::getAlignText() {
    int v = getGraphicVariableInt("$DIMTIH", 1);
    if (v>0) {
        addGraphicVariable("$DIMTIH", 1, 70);
        getGraphicVariableInt("$DIMTIH", 1);
        return true;
    }
    return false;
}


/**
 * @return Dimension fixed length for extension lines true= fixed, false= not fixed.
 */
bool FAS_Dimension::getFixedLengthOn() {
    int v = getGraphicVariableInt("$DIMFXLON", 0);
    if (v == 1) {
        addGraphicVariable("$DIMFXLON", 1, 70);
        getGraphicVariableInt("$DIMFXLON", 0);
        return true;
    }
    return false;
}

/**
 * @return Dimension fixed length for extension lines.
 */
double FAS_Dimension::getFixedLength() {
    return getGraphicVariable("$DIMFXL", 1.0, 40);
}


/**
 * @return extension line Width.
 */
FAS2::LineWidth FAS_Dimension::getExtensionLineWidth() {
    return FAS2::WidthByLayer;
    //return FAS2::WidthByLayer( getGraphicVariableInt("$DIMLWE", -2) ); //default -2 (FAS2::WidthByBlock)

}


/**
 * @return dimension line Width.
 */
FAS2::LineWidth FAS_Dimension::getDimensionLineWidth() {
    return FAS2::WidthByLayer;
    //return FAS2::intToLineWidth( getGraphicVariableInt("$DIMLWD", -2) ); //default -2 (FAS2::WidthByBlock)
}

/**
 * @return dimension line Color.
 */
FAS_Color FAS_Dimension::getDimensionLineColor() {
    return FAS_DXFProcessor::numberToColor(getGraphicVariableInt("$DIMCLRD", 0));
}


/**
 * @return extension line Color.
 */
FAS_Color FAS_Dimension::getExtensionLineColor() {
    return FAS_DXFProcessor::numberToColor(getGraphicVariableInt("$DIMCLRE", 0));
}


/**
 * @return dimension text Color.
 */
FAS_Color FAS_Dimension::getTextColor() {
    return FAS_DXFProcessor::numberToColor(getGraphicVariableInt("$DIMCLRT", 0));
}


/**
 * @return text style for dimensions.
 */
QString FAS_Dimension::getTextStyle() {
    return getGraphicVariableString("$DIMTXSTY", "standard");
}


/**
 * @return the given graphic variable or the default value given in mm
 * converted to the graphic unit.
 * If the variable is not found it is added with the given default
 * value converted to the local unit.
 */
double FAS_Dimension::getGraphicVariable(const QString& key, double defMM,
                                         int code) {

    double v = getGraphicVariableDouble(key, FAS_MINDOUBLE);
    if (v<=FAS_MINDOUBLE) {
        addGraphicVariable(
                    key,
                    FAS_Units::convert(defMM, FAS2::Millimeter, getGraphicUnit()),
                    code);
        v = getGraphicVariableDouble(key, 1.0);
    }

    return v;
}

/**
 * Removes zeros from angle string.
 *
 * @param angle The string representing angle.
 * @param zeros Zeros supresion (0 none, 1 suppres leading, 2 suppres trailing, 3 both)
 * Decimal separator are '.'
 *
 * @ret String with the formatted angle.
 */

QString FAS_Dimension::stripZerosAngle(QString angle, int zeros){
    if (zeros == 0) //do nothing
        return angle;
    if (zeros & 2){
        int end = angle.size() - 1;
        QChar format = angle[end--];  //stores & skip format char
        while (end > 0 && angle[end] == QChar('0')) // locate first 0 from end
            end--;
        if (angle[end] == QChar('.'))
            end--;
        angle.truncate(end+1);
        angle.append(format);
    }
    if (zeros & 1){
        if (angle[0] == QChar('0') && angle[1] == QChar('.'))
            angle = angle.remove(0, 1);
    }
    return angle;
}

/**
 * Removes zeros from linear string.
 *
 * @param linear The string representing linear measure.
 * @param zeros Zeros supresion (see dimzin)
 *
 * @ret String with the formatted linear measure.
 */

QString FAS_Dimension::stripZerosLinear(QString linear, int zeros){
    if (zeros == 1) //do nothing
        return linear;
    if (zeros & 8){
        int end = linear.size() - 1;
        while (end > 0 && linear[end] == QChar('0')) // locate first 0 from end
            end--;
        if (linear[end] == QChar('.'))
            end--;
        linear.truncate(end+1);
    }
    if (zeros & 4){
        if (linear[0] == QChar('0') && linear[1] == QChar('.'))
            linear = linear.remove(0, 1);
    }
    return linear;
}


void FAS_Dimension::move(const FAS_Vector& offset) {
    data.definitionPoint.move(offset);
    data.middleOfText.move(offset);
}



void FAS_Dimension::rotate(const FAS_Vector& center, const double& angle) {
    FAS_Vector angleVector(angle);
    data.definitionPoint.rotate(center, angleVector);
    data.middleOfText.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angle);
}

void FAS_Dimension::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    data.definitionPoint.rotate(center, angleVector);
    data.middleOfText.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angleVector.angle());
}


void FAS_Dimension::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    data.definitionPoint.scale(center, factor);
    data.middleOfText.scale(center, factor);
}



void FAS_Dimension::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    data.definitionPoint.mirror(axisPoint1, axisPoint2);
    data.middleOfText.mirror(axisPoint1, axisPoint2);
}

// EOF
