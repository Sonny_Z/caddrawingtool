﻿/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_ENTITYCONTAINER_H
#define FAS_ENTITYCONTAINER_H

#include <vector>
#include "fas_entity.h"

//Class representing a tree of entities.
class FAS_EntityContainer : public FAS_Entity
{
    typedef FAS_Entity * value_type;

public:
    FAS_EntityContainer(FAS_EntityContainer* parent = nullptr, bool owner=true);
    ~FAS_EntityContainer() override;

    FAS_Entity* clone() const override;
    virtual void detach();
    virtual void newDetach(QList<FAS_Entity*> origEntities, FAS_EntityContainer* parent);

    //return FAS2::EntityContainer.
    FAS2::EntityType rtti() const override
    {
        return FAS2::EntityContainer;
    }

    void reparent(FAS_EntityContainer* parent) override;

    //return true: because entities made from this class
    bool isContainer() const override
    {
        return true;
    }

    bool isAtomic() const override
    {
        return false;
    }

    double getLength() const override;

    void setVisible(bool v) override;

    bool setSelected(bool select = true) override;
    bool toggleSelected() override;

    virtual void selectWindow(FAS_Vector v1, FAS_Vector v2, bool select = true, bool cross = false);
    virtual void addEntity(FAS_Entity* entity);
    virtual void appendEntity(FAS_Entity* entity);
    virtual void prependEntity(FAS_Entity* entity);
    virtual void moveEntity(int index, QList<FAS_Entity *>& entList);
    virtual void insertEntity(int index, FAS_Entity* entity);
    virtual bool removeEntity(FAS_Entity* entity);

    //add four lines to form a rectangle by the diagonal vertices v0,v1
    void addRectangle(FAS_Vector const& v0, FAS_Vector const& v1);

    virtual FAS_Entity* firstEntity(FAS2::ResolveLevel level=FAS2::ResolveNone);
    virtual FAS_Entity* lastEntity(FAS2::ResolveLevel level=FAS2::ResolveNone);
    virtual FAS_Entity* nextEntity(FAS2::ResolveLevel level=FAS2::ResolveNone);
    virtual FAS_Entity* prevEntity(FAS2::ResolveLevel level=FAS2::ResolveNone);
    virtual FAS_Entity* entityAt(int index);
    virtual void setEntityAt(int index,FAS_Entity* en);
    virtual int findEntity(FAS_Entity const* const entity);
    virtual void clear();
    virtual bool isEmpty() const
    {
        return count()==0;
    }
    unsigned count() const override;
    unsigned countDeep() const override;
    virtual unsigned countSelected(bool deep=true, std::initializer_list<FAS2::EntityType> const& types = {});
    virtual double totalSelectedLength();

    // Enables/disables automatic update of borders on entity removals and additions. By default this is turned on.
    virtual void setAutoUpdateBorders(bool enable)
    {
        autoUpdateBorders = enable;
    }
    virtual void adjustBorders(FAS_Entity* entity);
    void calculateBorders() override;
    void calculateBlockBorders();
    void forcedCalculateBorders();
    void updateDimensions( bool autoText=true);
    virtual void updateInserts(int i);
    virtual void updateSplines();
    void update() override;
    virtual void renameInserts(const QString& oldName, const QString& newName);
    FAS_Vector getNearestEndpoint(const FAS_Vector& coord, double* dist = nullptr)const override;
    FAS_Vector getNearestEndpoint(const FAS_Vector& coord, double* dist, FAS_Entity** pEntity ) const;
    FAS_Entity* getNearestEntity(const FAS_Vector& point, double* dist = nullptr, FAS2::ResolveLevel level=FAS2::ResolveAll) const;

    FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                       bool onEntity = true,
                                       double* dist = nullptr,
                                       FAS_Entity** entity=nullptr)const override;

    FAS_Vector getNearestCenter(const FAS_Vector& coord, double* dist = nullptr)const override;
    FAS_Vector getNearestMiddle(const FAS_Vector& coord,
                                double* dist = nullptr,
                                int middlePoints = 1)const override;
    FAS_Vector getNearestDist(double distance,
                              const FAS_Vector& coord,
                              double* dist = nullptr) const override;
    FAS_Vector getNearestIntersection(const FAS_Vector& coord, double* dist = nullptr);
    FAS_Vector getNearestRef(const FAS_Vector& coord, double* dist = nullptr) const override;
    FAS_Vector getNearestSelectedRef(const FAS_Vector& coord, double* dist = nullptr) const override;
    static QList<FAS_Entity*> GetAllLineArcEntities(const FAS_EntityContainer* container);
    virtual FAS_Vector getMiddlePoint(void) const override;
    double getDistanceToPoint(const FAS_Vector& coord,
                              FAS_Entity** entity,
                              FAS2::ResolveLevel level=FAS2::ResolveNone,
                              double solidDist = FAS_MAXDOUBLE) const override;
    virtual bool optimizeContours();
    bool hasEndpointsWithinWindow(const FAS_Vector& v1, const FAS_Vector& v2) override;
    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2a) override;
    void stretch(const FAS_Vector& firstCorner,
                 const FAS_Vector& secondCorner,
                 const FAS_Vector& offset) override;
    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;
    void moveSelectedRef(const FAS_Vector& ref, const FAS_Vector& offset) override;
    void revertDirection() override;
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
    virtual void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);

    friend std::ostream& operator << (std::ostream& os, FAS_EntityContainer& ec);
    bool isOwner() const {return autoDelete;}
    void setOwner(bool owner) {autoDelete=owner;}
    virtual double areaLineIntegral() const override;
    //ignore this entity for entity catch for certain actions like catching circles to create tangent circles
    // return, true, indicate this entity container should be ignored
    bool ignoredOnModification() const;
    //brief begin/end to support range based loop
    QList<FAS_Entity *>::const_iterator begin() const;
    QList<FAS_Entity *>::const_iterator end() const;
    QList<FAS_Entity *>::iterator begin() ;
    QList<FAS_Entity *>::iterator end() ;
    FAS_Entity* last() const;
    FAS_Entity* first() const;

    QList<FAS_Entity*> getEntityList() const;
    QList<FAS_Insert*> getInsertList() const;
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;

    bool isReadOnly();

    int sortEntities();
    int calOptimalMaxDisplayEntities();

protected:

    /** entities in the container */
    QList<FAS_Entity *> entities;
    QList<FAS_Insert *> inserts;

    /** sub container used only temporarly for iteration. */
    FAS_EntityContainer* subContainer;

    //Automatically update the borders of the container when entities are added or removed.
    static bool autoUpdateBorders;

private:
    //ignoredSnap whether snapping is ignored, return true when entity of this container won't be considered for snapping points
    bool ignoredSnap() const;
    int entIdx;
    bool autoDelete;
};

#endif
