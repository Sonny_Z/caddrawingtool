/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_block.h"

//sort with alphabetical order
#include "fas_graphic.h"
#include <iostream>

FAS_BlockData::FAS_BlockData(const QString& _name, const FAS_Vector& _basePoint, bool _frozen):
    name(_name)
  ,basePoint(_basePoint)
  ,frozen(_frozen)
{
    this->set=0;
}

bool FAS_BlockData::isValid() const
{
    return (!name.isEmpty() && basePoint.valid);
}

FAS_Block::FAS_Block(FAS_EntityContainer* parent,
                     const FAS_BlockData& d)
    : FAS_Document(parent), data(d)
{
    pen = FAS_Pen(FAS_Color(0,255,0), FAS2::Width01, FAS2::SolidLine);
    data.type = nullptr;
}

FAS_Block::~FAS_Block()
{
}

FAS_Entity* FAS_Block::clone() const
{
    FAS_Block* blk = new FAS_Block(*this);
    blk->setOwner(isOwner());
    blk->detach();
    blk->initId();
    return blk;
}

FAS_LayerList* FAS_Block::getLayerList()
{
    FAS_Graphic* g = getGraphic();
    if (g) {
        return g->getLayerList();
    } else {
        return nullptr;
    }
}

FAS_BlockList* FAS_Block::getBlockList()
{
    FAS_Graphic* g = getGraphic();
    if (g)
    {
        return g->getBlockList();
    }
    else
    {
        return nullptr;
    }
}


bool FAS_Block::save(bool isAutoSave)
{
    FAS_Graphic* g = getGraphic();
    if (g) {
        return g->save(isAutoSave);
    } else {
        return false;
    }
}

bool FAS_Block::saveAs(const QString& filename, FAS2::FormatType type, bool force)
{
    FAS_Graphic* g=getGraphic();
/*
    if(force){
        auto c = new FAS_EntityContainer();
        c=(FAS_EntityContainer*)getGraphic()->clone();

        g = new FAS_Graphic(c);
        g->clear();
        auto b=this->clone();
        b->setSelected(false);
        b->setVisible(true);
        g->addEntity(b);
    }else{
        g=getGraphic();
    }*/

    if (g) {
        return g->saveAs(filename, type, force);
    } else {
        return false;
    }
}

// Sets the parent documents modified status to 'm'.
void FAS_Block::setModified(bool m)
{
    FAS_Graphic* p = getGraphic();
    if (p) {
        p->setModified(m);
    }
    modified = m;
}

std::ostream& operator << (std::ostream& os, const FAS_Block& b)
{
    os << " name: " << b.getName().toLatin1().data() << "\n";
    os << " entities: " << (FAS_EntityContainer&)b << "\n";
    return os;
}
