/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_flags.h"

//Constructor with initialisation to the given flags.
FAS_Flags::FAS_Flags(unsigned f):
    flags(f)
{
}

unsigned FAS_Flags::getFlags() const
{
    return flags;
}

void FAS_Flags::resetFlags()
{
    flags=0;
}

void FAS_Flags::setFlags(unsigned f)
{
    flags=f;
}

void FAS_Flags::setFlag(unsigned f)
{
    flags |= f;
}

void FAS_Flags::delFlag(unsigned f)
{
    flags &= ~f;
}

void FAS_Flags::toggleFlag(unsigned f)
{
    flags ^= f;
}

bool FAS_Flags::getFlag(unsigned f) const
{
    return flags&f;
}

