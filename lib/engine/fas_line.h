/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_LINE_H
#define FAS_LINE_H

#include "fas_atomicentity.h"

class LC_Quadratic;

// Holds the data that defines a line.
#if MYL
class FAS_LineData {
public:
    /**
     * Default constructor. Leaves the data object uninitialized.
     */
    FAS_LineData() {}

    FAS_LineData(const FAS_Vector& startpoint,
                const FAS_Vector& endpoint) {

        this->startpoint = startpoint;
        this->endpoint = endpoint;
    }

    friend class RS_Line;
    friend class RS_ActionDrawLine;

   /* friend std::ostream& operator << (std::ostream& os, const FAS_LineData& ld) {
        os << "(" << ld.startpoint <<
        "/" << ld.endpoint <<
        ")";
        return os;
    }*/

public:
    FAS_Vector startpoint;
    FAS_Vector endpoint;
    FAS_Vector bkstartpoin;
    FAS_Vector bkendpoint;
};
#else
struct FAS_LineData
{
    FAS_Vector startpoint;
    FAS_Vector endpoint;
    FAS_Vector bkstartpoin;
    FAS_Vector bkendpoint;
};

#endif
std::ostream& operator << (std::ostream& os, const FAS_LineData& ld);

// Class for a line entity.
class FAS_Line : public FAS_AtomicEntity
{
public:
    FAS_Line() {}
    FAS_Line(FAS_EntityContainer* parent,
             const FAS_LineData& d);
    FAS_Line(FAS_EntityContainer* parent, const FAS_Vector& pStart, const FAS_Vector& pEnd);
    FAS_Line(const FAS_Vector& pStart, const FAS_Vector& pEnd);

    FAS_Entity* clone() const override;

    FAS2::EntityType rtti() const override
    {
        return FAS2::EntityLine;
    }
    //return true
    bool isEdge() const override
    {
        return true;
    }

    FAS_LineData getData() const
    {
        return data;
    }

    FAS_VectorSolutions getRefPoints() const override;

    // Start point of the entity
    FAS_Vector getStartpoint() const override
    {
        return data.startpoint;
    }
    // End point of the entity */
    FAS_Vector getEndpoint() const override
    {
        return data.endpoint;
    }
    // Sets the startpoint
    void setStartpoint(FAS_Vector s)
    {
        data.startpoint = s;
        calculateBorders();
    }
    /** Sets the endpoint */
    void setEndpoint(FAS_Vector e)
    {
        data.endpoint = e;
        calculateBorders();
    }
    //The angle at which the line starts at the startpoint.
    double getDirection1() const override
    {
        return getAngle1();
    }
    //The angle at which the line starts at the endpoint.
    double getDirection2() const override
    {
        return getAngle2();
    }
    FAS_Vector getTangentDirection(const FAS_Vector& point)const override;

    void moveStartpoint(const FAS_Vector& pos) override;
    void moveEndpoint(const FAS_Vector& pos) override;
    FAS2::Ending getTrimPoint(const FAS_Vector& trimCoord, const FAS_Vector& trimPoint) override;
    FAS_Vector prepareTrim(const FAS_Vector& trimCoord, const FAS_VectorSolutions& trimSol) override;
    void reverse() override;
    // Sets the y coordinate of the startpoint
    void setStartpointY(double val)
    {
        data.startpoint.y = val;
        calculateBorders();
    }
    // Sets the y coordinate of the endpoint
    void setEndpointY(double val)
    {
        data.endpoint.y = val;
        calculateBorders();
    }
    bool hasEndpointsWithinWindow(const FAS_Vector& v1, const FAS_Vector& v2) override;

    //return The length of the line.
    double getLength() const override
    {
        return data.startpoint.distanceTo(data.endpoint);
    }

    // return The angle of the line (from start to endpoint).
    double getAngle1() const
    {
        return data.startpoint.angleTo(data.endpoint);
    }

    //return The angle of the line (from end to startpoint).
    double getAngle2() const
    {
        return data.endpoint.angleTo(data.startpoint);
    }
    bool isTangent(const FAS_CircleData&  circleData) const override;

    //return a perpendicular vector
    FAS_Vector getNormalVector() const;
    FAS_Vector getMiddlePoint()const override;
    FAS_Vector getNearestEndpoint(const FAS_Vector& coord, double* dist = nullptr)const override;
    FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                       bool onEntity=true, double* dist = nullptr, FAS_Entity** entity=nullptr)const override;
    FAS_Vector getNearestMiddle(const FAS_Vector& coord,
                                double* dist = nullptr,
                                int middlePoints = 1)const override;
    FAS_Vector getNearestDist(double distance, const FAS_Vector& coord,double* dist = nullptr)const override;
    FAS_Vector getNearestDist(double distance,bool startp)const override;
    void revertDirection() override;
    std::vector<FAS_Entity* > offsetTwoSides(const double& distance) const override;
    bool offset(const FAS_Vector& coord, const double& distance) override;
    void move(const FAS_Vector& offset) override;
    void rotate(const double& angle);
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& factor) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void stretch(const FAS_Vector& firstCorner, const FAS_Vector& secondCorner, const FAS_Vector& offset) override;
    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;

    // whether the entity's bounding box intersects with visible portion of graphic view
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
//    void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    friend std::ostream& operator << (std::ostream& os, const FAS_Line& l);
    void calculateBorders() override;
    LC_Quadratic getQuadratic() const override;
    double areaLineIntegral() const override;
protected:
    FAS_LineData data;
};

#endif
