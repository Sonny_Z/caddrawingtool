/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_ATOMICENTITY_H
#define FAS_ATOMICENTITY_H

#include "fas_entity.h"

struct FAS_CircleData;

// Base class representing an atomic entity, inluding FAS_Line, FAS_Arc, FAS_Circle, FAS_Ellipse
class FAS_AtomicEntity : public FAS_Entity
{
public:

    FAS_AtomicEntity(FAS_EntityContainer* parent = nullptr);

    //return false because entities made from subclasses are atomic entities.
    bool isContainer() const override;

    // return true because entities made from subclasses are atomic entities.
    bool isAtomic() const override;

    // return Always 1 for atomic entities.
    unsigned count() const override;

    // return Always 1 for atomic entities.
    unsigned countDeep() const override;

    // return the endpoint of the entity or an invalid vector if the entity has no endpoint.
    FAS_Vector getEndpoint() const override;

    // return the startpoint of the entity or an invalid vector if the entity has no startpoint.
    FAS_Vector getStartpoint() const override;

    // return the angle in which direction the entity starts.
    double getDirection1() const override;

    // return the angle in which direction the entity starts the opposite way.
    double getDirection2() const override;

    FAS_Vector getCenter() const override;
    double getRadius() const override;
    // return the nearest center for snapping
    FAS_Vector getNearestCenter(const FAS_Vector& coord, double* dist) const override;

    virtual void setStartpointSelected(bool select);
    virtual void setEndpointSelected(bool select);
    virtual bool isTangent(const FAS_CircleData& /* circleData */) const;
    bool isStartpointSelected() const;
    bool isEndpointSelected() const;
    void revertDirection() override;

    //create offset of the entity to the given direction and distance
    bool offset(const FAS_Vector& /*position*/, const double& /*distance*/) override;

    //  move the startpoint of the entity to  the given position.
    virtual void moveStartpoint(const FAS_Vector& /*pos*/);

    // Implementation must move the endpoint of the entity to the given position.
    virtual void moveEndpoint(const FAS_Vector& /*pos*/);

    // trim the startpoint of the entity to the given position.
    virtual void trimStartpoint(const FAS_Vector& pos);

    // Implementation must trim the endpoint of the entity to the given position.
    virtual void trimEndpoint(const FAS_Vector& pos);

    /*
     * Implementation must return which ending of the entity will
     * be trimmed if 'coord' is the coordinate chosen to indicate the
     * trim entity and 'trimPoint' is the point to which the entity will
     * be trimmed.
     */
    virtual FAS2::Ending getTrimPoint(const FAS_Vector& coord,
                                      const FAS_Vector& trimPoint);

    /*
     * Implementation must trim the entity in the case of multiple
     * intersections and return the trimPoint
     * trimCoord indicts the trigger trim position
     * trimSol contains intersections
     */
    virtual FAS_Vector prepareTrim(const FAS_Vector& trimCoord,
                                   const FAS_VectorSolutions& trimSol);
    virtual void reverse();
    void moveSelectedRef(const FAS_Vector& ref, const FAS_Vector& offset) override;
};


#endif
