/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_VARIABLE_H
#define FAS_VARIABLE_H

#include <QString>
#include "fas.h"
#include "fas_vector.h"

/**
 * A variable of type int, double, string or vector.
 * The variable also contains its type and an int code
 * which can identify its contents in any way.
 */
class FAS_Variable
{
    struct FAS_VariableContents
    {
        QString s;
        int i;
        double d;
        FAS_Vector v;
    };
public:

    FAS_Variable() {}
    FAS_Variable(const FAS_Vector& v, int c):
        code{c}
    {
        setVector(v);
    }
    FAS_Variable(const QString& v, int c):
        code{c}
    {
        setString(v);
    }
    FAS_Variable(int v, int c):
        code{c}
    {
        setInt(v);
    }
    FAS_Variable(double v, int c):
        code{c}
    {
        setDouble(v);
    }

    void setString(const QString& str)
    {
        contents.s = str;
        type = FAS2::VariableString;
    }
    void setInt(int i)
    {
        contents.i = i;
        type = FAS2::VariableInt;
    }
    void setDouble(double d)
    {
        contents.d = d;
        type = FAS2::VariableDouble;
    }
    void setVector(const FAS_Vector& v)
    {
        contents.v = v;
        type = FAS2::VariableVector;
    }

    QString getString() const
    {
        return contents.s;
    }
    int getInt() const
    {
        return contents.i;
    }
    double getDouble() const
    {
        return contents.d;
    }
    FAS_Vector getVector() const
    {
        return contents.v;
    }

    FAS2::VariableType getType() const
    {
        return type;
    }
    int getCode() const
    {
        return code;
    }

    //friend std::ostream& operator << (std::ostream& os, FAS_Variable& v);

private:
    FAS_VariableContents contents;
    FAS2::VariableType type = FAS2::VariableVoid;
    int code = 0;
};

#endif
