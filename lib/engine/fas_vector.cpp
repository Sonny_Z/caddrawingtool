/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

//sort with alphabetical order
#include <algorithm>
#include <cmath>
#include <iostream>
#include <QHash>

#include "fas_math.h"
#include "fas_vector.h"
#include "lc_rect.h"

// Constructor for a point with given coordinates.
FAS_Vector::FAS_Vector(double vx, double vy, double vz):
    x(vx)
  ,y(vy)
  ,z(vz)
  ,valid(true)
{
}

// Constructor for a unit vector with given angle
FAS_Vector::FAS_Vector(double angle):
    x(cos(angle))
  ,y(sin(angle))
  ,valid(true)
{
}

// Constructor for a point with given valid flag.
FAS_Vector::FAS_Vector(bool valid):
    valid(valid)
{
}

FAS_Vector::operator bool() const
{
    return valid;
}

// Sets to a unit vector by the direction angle
void FAS_Vector::set(double angle)
{
    x = cos(angle);
    y = sin(angle);
    z = 0.;
    valid = true;
}

// Sets a new position for the vector.
void FAS_Vector::set(double vx, double vy, double vz)
{
    x = vx;
    y = vy;
    z = vz;
    valid = true;
}

// Sets a new position for the vector in polar coordinates.
void FAS_Vector::setPolar(double radius, double angle)
{
    x = radius * cos(angle);
    y = radius * sin(angle);
    z = 0.0;
    valid = true;
}

FAS_Vector FAS_Vector::polar(double rho, double theta)
{
    return {rho*cos(theta), rho*sin(theta), 0.};
}

//return The angle from zero to this vector (in rad).
double FAS_Vector::angle() const
{
    return FAS_Math::correctAngle(atan2(y,x));
}

//return point blongs matrix ID for CAD recognition
QString FAS_Vector::getPointMatrixID() const
{
    int xMatrixInt=floor(x/200);
    int yMatrixInt=floor(y/200);

    return QString("%1_%2").arg(QString::number(xMatrixInt)).arg(QString::number(yMatrixInt));

}

//return point ID in matrix
QString FAS_Vector::getPointID() const{

    QString xString = QString::number(x,'f',0);
    QString yString = QString::number(y,'f',0);
    return QString("%1_%2").arg(xString).arg(yString);

}



/**
 * @return The angle from this and the given coordinate (in rad).
 */
double FAS_Vector::angleTo(const FAS_Vector& v) const
{
    if (!valid || !v.valid)
        return 0.0;
    return (v-(*this)).angle();
}

// return The angle from between two vectors using the current vector as the center return 0, if the angle is not well defined
double FAS_Vector::angleBetween(const FAS_Vector& v1, const FAS_Vector& v2) const
{
    if (!valid || !v1.valid || !v2.valid)
        return 0.0;
    FAS_Vector const vStart(v1 - (*this));
    FAS_Vector const vEnd(v2 - (*this));
    return FAS_Math::correctAngle( atan2( vStart.x*vEnd.y-vStart.y*vEnd.x, vStart.x*vEnd.x+vStart.y*vEnd.y));
}

// return Magnitude (length) of the vector.
double FAS_Vector::magnitude() const
{
    double ret(0.0);
    if (valid)
        ret = hypot(hypot(x, y), z);

    return ret;
}

// return square of vector length
double FAS_Vector::squared() const
{
    if (valid)
        return x*x + y*y + z*z;
    return FAS_MAXDOUBLE;
}

// return square of vector length
double FAS_Vector::squaredTo(const FAS_Vector& v1) const
{
    if (valid && v1.valid) {
        return  (*this - v1).squared();
    }
    return FAS_MAXDOUBLE;
}

FAS_Vector FAS_Vector::lerp(const FAS_Vector& v, double t) const
{
    return {x+(v.x-x)*t, y+(v.y-y)*t};
}

//return The distance between this and the given coordinate.
double FAS_Vector::distanceTo(const FAS_Vector& v) const
{
    if (!valid || !v.valid)
    {
        return FAS_MAXDOUBLE;
    }
    else {
        return (*this-v).magnitude();
    }
}

// return true is this vector is within the given range.
bool FAS_Vector::isInWindow(const FAS_Vector& firstCorner,
                            const FAS_Vector& secondCorner) const
{
    if (!valid) return false;
    return LC_Rect{firstCorner, secondCorner}.inArea(*this);
}

//return true is this vector is within the given range of ordered vectors
bool FAS_Vector::isInWindowOrdered(const FAS_Vector& vLow, const FAS_Vector& vHigh) const
{
    if(!valid)
        return false;
    return (x>=vLow.x && x<=vHigh.x && y>=vLow.y && y<=vHigh.y);
}

// move to the closest integer point
FAS_Vector FAS_Vector::toInteger()
{
    x = rint(x);
    y = rint(y);
    return *this;
}

//Moves this vector by the given offset. Equal to the operator +=.
FAS_Vector FAS_Vector::move(const FAS_Vector& offset)
{
    *this+=offset;
    return *this;
}

// Rotates this vector around 0/0 by the given angle.
FAS_Vector FAS_Vector::rotate(double ang)
{
    rotate(FAS_Vector{ang});
    return *this;
}

//Rotates this vector around 0/0 by the given vector
// if the vector is a unit, then, it's the same as rotating around
// 0/0 by the angle of the vector
FAS_Vector FAS_Vector::rotate(const FAS_Vector& angleVector)
{
    double x0 = x * angleVector.x - y * angleVector.y;
    y = x * angleVector.y + y * angleVector.x;
    x = x0;

    return *this;
}

// Rotates this vector around the given center by the given angle.
FAS_Vector FAS_Vector::rotate(const FAS_Vector& center, double ang)
{
    *this = center + (*this-center).rotate(ang);
    return *this;
}
FAS_Vector FAS_Vector::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    *this = center + (*this-center).rotate(angleVector);
    return *this;
}

// Scales this vector by the given factors with 0/0 as center.
FAS_Vector FAS_Vector::scale(double factor)
{
    x *= factor;
    y *= factor;
    return *this;
}

// Scales this vector by the given factors with 0/0 as center.
FAS_Vector FAS_Vector::scale(const FAS_Vector& factor)
{
    x *= factor.x;
    y *= factor.y;
    z *= factor.z;
    return *this;
}

FAS_Vector FAS_Vector::scale(const FAS_Vector& factor) const
{
    return {x * factor.x, y * factor.y, z * factor.z};
}

// Scales this vector by the given factors with the given center.
FAS_Vector FAS_Vector::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    *this = center + (*this-center).scale(factor);
    return *this;
}

// Mirrors this vector at the given axis, defined by two points on axis.
FAS_Vector FAS_Vector::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    FAS_Vector direction(axisPoint2-axisPoint1);
    double a= direction.squared();
    FAS_Vector ret(false);
    if(a<FAS_TOLERANCE2)
    {
        return ret;
    }
    ret= axisPoint1 + direction* dotP(*this - axisPoint1,direction)/a; //projection point
    *this = ret + ret - *this;
    return *this;
}

// Streams the vector components to stdout. e.g.: "1/4/0"
std::ostream& operator << (std::ostream& os, const FAS_Vector& v)
{
    if(v.valid)
    {
        os << v.x << "/" << v.y << "/" << v.z;
    }
    else
    {
        os << "invalid vector";
    }
    return os;
}

FAS_Vector FAS_Vector::operator + (const FAS_Vector& v) const
{
    return {x + v.x, y + v.y, z + v.z};
}

FAS_Vector FAS_Vector::operator - (const FAS_Vector& v) const
{
    return {x - v.x, y - v.y, z - v.z};
}

FAS_Vector FAS_Vector::operator + (double d) const
{
    return {x + d, y + d, z + d};
}

FAS_Vector FAS_Vector::operator - (double d) const
{
    return {x - d, y - d, z - d};
}

FAS_Vector FAS_Vector::operator * (const FAS_Vector& v) const
{
    return {x * v.x, y * v.y, z * v.z};
}

FAS_Vector FAS_Vector::operator / (const FAS_Vector& v) const
{
    if(fabs(v.x)> FAS_TOLERANCE && fabs(v.y)>FAS_TOLERANCE)
        return {x / v.x, y / v.y, std::isnormal(v.z)?z / v.z:z};
            return *this;
        }

        FAS_Vector FAS_Vector::operator * (double s) const
        {
        return {x * s, y * s, z * s};
}

FAS_Vector FAS_Vector::operator / (double s) const
{
    if(fabs(s)> FAS_TOLERANCE)
        return {x / s, y / s, z / s};

    return *this;
}

FAS_Vector FAS_Vector::operator - () const
{
    return {-x, -y, -z};
}

double FAS_Vector::dotP(const FAS_Vector& v1) const
{
    return x*v1.x+y*v1.y;
}

double FAS_Vector::dotP(const FAS_Vector& v1, const FAS_Vector& v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

// switch x,y for all vectors
FAS_Vector FAS_Vector::flipXY(void) const
{
    return {y, x};
}

FAS_Vector FAS_Vector::operator += (const FAS_Vector& v)
{
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}

FAS_Vector FAS_Vector::operator -= (const FAS_Vector& v)
{
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}

FAS_Vector FAS_Vector::operator *= (const FAS_Vector& v)
{
    x *= v.x;
    y *= v.y;
    z *= v.z;
    return *this;
}

FAS_Vector FAS_Vector::operator /= (const FAS_Vector& v)
{
    if (fabs(v.x)> FAS_TOLERANCE && fabs(v.y)>FAS_TOLERANCE){
        x /= v.x;
        y /= v.y;
        if (std::isnormal(v.z))
            z /= v.z;
    }
    return *this;
}

FAS_Vector FAS_Vector::operator *= (double s)
{
    x *= s;
    y *= s;
    z *= s;
    return *this;
}

FAS_Vector FAS_Vector::operator /= (double s)
{
    if(fabs(s)>FAS_TOLERANCE) {
        x /= s;
        y /= s;
        z /= s;
    }
    return *this;
}

bool FAS_Vector::operator == (const FAS_Vector& v) const
{
    return (x==v.x && y==v.y && z==v.z && valid && v.valid);
}

bool FAS_Vector::operator == (bool valid) const
{
    return this->valid == valid;
}

bool FAS_Vector::operator != (bool valid) const
{
    return this->valid != valid;
}

//A vector with the minimum components from the vectors v1 and v2.
FAS_Vector FAS_Vector::minimum (const FAS_Vector& v1, const FAS_Vector& v2)
{
    if (!v2) return v1;
    if (!v1) return v2;
    return {std::min(v1.x, v2.x),
                std::min(v1.y, v2.y),
                std::min(v1.z, v2.z)
    };
}

//A vector with the maximum values from the vectors v1 and v2
FAS_Vector FAS_Vector::maximum (const FAS_Vector& v1, const FAS_Vector& v2)
{
    if (!v2) return v1;
    if (!v1) return v2;
    return {std::max(v1.x, v2.x),
                std::max(v1.y, v2.y),
                std::max(v1.z, v2.z)
    };
}

//Cross product of two vectors.
FAS_Vector FAS_Vector::crossP(const FAS_Vector& v1, const FAS_Vector& v2)
{
    return {v1.y*v2.z - v1.z*v2.y,
                v1.z*v2.x - v1.x*v2.z,
                v1.x*v2.y - v1.y*v2.x};
}

// Constructor for no solution.
FAS_VectorSolutions::FAS_VectorSolutions():
    vector(0)
  ,tangent(false)
{
}

FAS_VectorSolutions::FAS_VectorSolutions(const std::vector<FAS_Vector>& l):
    vector( l.begin(), l.end())
  ,tangent(false)
{
}

/**
 * Constructor for num solutions.
 */
FAS_VectorSolutions::FAS_VectorSolutions(int num):
    vector(num, FAS_Vector(false))
  ,tangent(false)
{
}

FAS_VectorSolutions::FAS_VectorSolutions(std::initializer_list<FAS_Vector> const& l):
    vector(l)
  ,tangent(false)
{
}

/**
 * Allocates 'num' vectors.
 */
void FAS_VectorSolutions::alloc(size_t num)
{
    if(num<=vector.size()){
        vector.resize(num);
    }
    else
    {
        const std::vector<FAS_Vector> v(num - vector.size());
        vector.insert(vector.end(), v.begin(), v.end());
    }
}

FAS_Vector FAS_VectorSolutions::get(size_t i) const
{
    if(i<vector.size())
        return vector.at(i);
    return {};
}

const FAS_Vector&  FAS_VectorSolutions::operator [] (const size_t i) const
{
    return vector[i];
}

FAS_Vector&  FAS_VectorSolutions::operator [] (const size_t i)
{
    return vector[i];
}

size_t FAS_VectorSolutions::size() const
{
    return vector.size();
}
/**
 * Deletes vector array and resets everything.
 */
void FAS_VectorSolutions::clear()
{
    vector.clear();
    tangent = false;
}

// return vector solution number i or an invalid vector if there are less solutions.
const FAS_Vector& FAS_VectorSolutions::at(size_t i) const
{
    return vector.at(i);
}

// return Number of solutions available.
size_t FAS_VectorSolutions::getNumber() const
{
    return vector.size();
}

/*
 * true There's at least one valid solution.
 *  false There's no valid solution.
 */
bool FAS_VectorSolutions::hasValid() const
{
    for(const FAS_Vector& v: vector)
        if (v.valid)  return true;

    return false;
}

void FAS_VectorSolutions::resize(size_t n)
{
    vector.resize(n);
}

const std::vector<FAS_Vector>& FAS_VectorSolutions::getVector() const
{
    return vector;
}

std::vector<FAS_Vector>::const_iterator FAS_VectorSolutions::begin() const
{
    return vector.begin();
}

std::vector<FAS_Vector>::const_iterator FAS_VectorSolutions::end() const
{
    return vector.end();
}

std::vector<FAS_Vector>::iterator FAS_VectorSolutions::begin()
{
    return vector.begin();
}

std::vector<FAS_Vector>::iterator FAS_VectorSolutions::end()
{
    return vector.end();
}

void FAS_VectorSolutions::push_back(const FAS_Vector& v)
{
    vector.push_back(v);
}

void FAS_VectorSolutions::removeAt(const size_t i)
{
    if (vector.size()> i)
        vector.erase(vector.begin()+i);
}

FAS_VectorSolutions& FAS_VectorSolutions::push_back(const FAS_VectorSolutions& v)
{
    vector.insert(vector.end(), v.begin(), v.end());
    return *this;
}

/**
 * Sets the solution i to the given vector.
 * If i is greater than the current number of solutions available,
 * nothing happens.
 */
void FAS_VectorSolutions::set(size_t i, const FAS_Vector& v)
{
    if (i<vector.size())
    {
        vector[i] = v;
    }else
    {
        for(size_t j=vector.size();j<=i;++j)
            vector.push_back(v);
    }
}

//Sets the tangent flag.
void FAS_VectorSolutions::setTangent(bool t)
{
    tangent = t;
}

// @return true if at least one of the solutions is a double solution(tangent).
bool FAS_VectorSolutions::isTangent() const
{
    return tangent;
}

// Rotates all vectors around (0,0) by the given angle.
void FAS_VectorSolutions::rotate(double ang)
{
    FAS_Vector angleVector(ang);
    for (auto& vp: vector)
    {
        if (vp.valid)
        {
            vp.rotate(angleVector);
        }
    }
}

//Rotates all vectors around (0,0) by the given angleVector.
void FAS_VectorSolutions::rotate(const FAS_Vector& angleVector)
{
    for (auto& vp: vector) {
        if (vp.valid) {
            vp.rotate(angleVector);
        }
    }
}

// Rotates all vectors around the given center by the given angle.
void FAS_VectorSolutions::rotate(const FAS_Vector& center, double ang)
{
    const FAS_Vector angleVector(ang);
    for (auto& vp: vector)
    {
        if (vp.valid)
        {
            vp.rotate(center,angleVector);
        }
    }
}

void FAS_VectorSolutions::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    for (auto& vp: vector)
    {
        if (vp.valid)
        {
            vp.rotate(center, angleVector);
        }
    }
}

//Move all vectors around the given center by the given vector.
void FAS_VectorSolutions::move(const FAS_Vector& vp)
{
    for (FAS_Vector& v: vector)
    {
        if (v.valid)
        {
            v.move(vp);
        }
    }
}

// Scales all vectors by the given factors with the given center.
void FAS_VectorSolutions::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    for (auto& vp: vector)
    {
        if (vp.valid)
        {
            vp.scale(center, factor);
        }
    }
}

void FAS_VectorSolutions::scale( const FAS_Vector& factor)
{
    for (auto& vp: vector)
    {
        if (vp.valid)
        {
            vp.scale(factor);
        }
    }
}

//return vector solution which is the closest to the given coordinate. dist will contain the distance if it doesn't point to nullptr (default).
FAS_Vector FAS_VectorSolutions::getClosest(const FAS_Vector& coord, double* dist, size_t* index) const
{

    double curDist{0.};
    double minDist = FAS_MAXDOUBLE;
    FAS_Vector closestPoint{false};
    int pos(0);

    for (size_t i=0; i<vector.size(); i++)
    {
        if (vector[i].valid)
        {
            curDist = (coord - vector[i]).squared();
            if (curDist<minDist)
            {
                closestPoint = vector[i];
                minDist = curDist;
                pos=i;
            }
        }
    }
    if (dist)
    {
        *dist = sqrt(minDist);
    }
    if (index)
    {
        *index = pos;
    }
    return closestPoint;
}

/*
  * return the closest distance from the first counts fas_vectors
  *coord, distance to this point
  *counts, only consider this many points within solution
  */
double FAS_VectorSolutions::getClosestDistance(const FAS_Vector& coord, int counts)
{
    double ret=FAS_MAXDOUBLE*FAS_MAXDOUBLE;
    int i=vector.size();
    if (counts < i && counts >= 0) i=counts;
    std::for_each(vector.begin(), vector.begin() + i,
                  [&ret, &coord](FAS_Vector const& vp)
    {
        if(vp.valid)
        {
            double d=(coord - vp).squared();
            if(d<ret) ret=d;
        }
    }
    );

    return sqrt(ret);
}

/** switch x,y for all vectors */
FAS_VectorSolutions FAS_VectorSolutions::flipXY(void) const
{
    FAS_VectorSolutions ret;
    for(const auto& vp: vector)
        ret.push_back(vp.flipXY());
    return ret;
}

FAS_VectorSolutions FAS_VectorSolutions::operator = (const FAS_VectorSolutions& s)
{
    setTangent(s.isTangent());
    vector=s.vector;

    return *this;
}

std::ostream& operator << (std::ostream& os,
                           const FAS_VectorSolutions& s)
{
    for (const FAS_Vector& vp: s){
        os << "(" << vp << ")\n";
    }
    os << " tangent: " << (int)s.isTangent() << "\n";
    return os;
}
