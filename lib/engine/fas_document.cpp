/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_document.h"

FAS_Document::FAS_Document(FAS_EntityContainer* parent)
    : FAS_EntityContainer(parent), FAS_Undo()
{
    filename = "";
    autosaveFilename = "Unnamed";
    formatType = FAS2::FormatUnknown;
    setModified(false);
    FAS_Color col(FAS2::FlagByLayer);
    activePen = FAS_Pen(col, FAS2::WidthByLayer, FAS2::LineByLayer);

    gv = nullptr;//used to read/save current view
}
