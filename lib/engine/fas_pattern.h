/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_PATTERN_H
#define FAS_PATTERN_H

#include "fas_entitycontainer.h"

class FAS_PatternList;

/**
 * Patterns are used for hatches. They are stored in a FAS_PatternList.
 * Use FAS_PatternList to access a pattern.
 */
class FAS_Pattern : public FAS_EntityContainer
{
public:
    FAS_Pattern(const QString& fileName);
    FAS2::EntityType rtti() const
    {
        return FAS2::EntityPattern;
    }

    virtual bool loadPattern();
    // the fileName of this pattern.
    QString getFileName() const;

protected:
    // Pattern file name
    QString fileName;
    // Is this pattern currently loaded into memory?
    bool loaded;
};


#endif
