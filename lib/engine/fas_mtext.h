/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_MTEXT_H
#define FAS_MTEXT_H

#include "fas_entitycontainer.h"
#include "fas_atomicentity.h"

// Holds the data that defines a text entity.
struct FAS_MTextData
{
    // Vertical alignments.
    enum VAlign
    {
        VATop,      /**< Top. */
        VAMiddle,   /**< Middle */
        VABottom    /**< Bottom */
    };

    //Horizontal alignments.
    enum HAlign
    {
        HALeft,     /**< Left */
        HACenter,   /**< Centered */
        HARight     /**< Right */
    };

    //MText drawing direction.
    enum MTextDrawingDirection
    {
        LeftToRight,     /**< Left to right */
        TopToBottom,     /**< Top to bottom */
        ByStyle          /**< Inherited from associated text style */
    };

    // Line spacing style for MTexts.
    enum MTextLineSpacingStyle
    {
        AtLeast,        /**< Taller characters will override */
        Exact           /**< Taller characters will not override */
    };

    // Default constructor. Leaves the data object uninitialized.
    FAS_MTextData() {}

    /*
     * param insertionPoint Insertion point
     * param height Nominal (initial) text height
     * param width Reference rectangle width
     * param valign Vertical alignment
     * param halign Horizontal alignment
     * param drawingDirection Drawing direction
     * param lineSpacingStyle Line spacing style
     * param lineSpacingFactor Line spacing factor
     * param text Text string
     * param style Text style name
     * param angle Rotation angle
     * param updateMode FAS2::Update will update the text entity instantly
     *    FAS2::NoUpdate will not update the entity. You can update
     *    it later manually using the update() method. This is
     *    often the case since you might want to adjust attributes
     *    after creating a text entity.
     */
    FAS_MTextData(const FAS_Vector& insertionPoint,
                  double height,
                  double width,
                  VAlign valign,
                  HAlign halign,
                  MTextDrawingDirection drawingDirection,
                  MTextLineSpacingStyle lineSpacingStyle,
                  double lineSpacingFactor,
                  const QString& text,
                  const QString& style,
                  double angle,
                  FAS2::UpdateMode updateMode = FAS2::Update);
    /** Insertion point */
    FAS_Vector insertionPoint;
    /** Nominal (initial) text height */
    double height;
    /** Reference rectangle width */
    double width;
    /** Vertical alignment */
    VAlign valign;
    /** Horizontal alignment */
    HAlign halign;
    /** Drawing direction */
    MTextDrawingDirection drawingDirection;
    /** Line spacing style */
    MTextLineSpacingStyle lineSpacingStyle;
    /** Line spacing factor */
    double lineSpacingFactor;
    /** Text string */
    QString text;
    /** Text style name */
    QString style;
    /** Rotation angle */
    double angle;
    /** Update mode */
    FAS2::UpdateMode updateMode;

    /** Insertion point */
    FAS_Vector bkinsertionPoint;
    /** Nominal (initial) text height */
    double bkheight;
    /** Reference rectangle width */
    double bkwidth;
    /** Rotation angle */
    double bkangle;
};

std::ostream& operator << (std::ostream& os, const FAS_MTextData& td);

/*
 * Class for a text entity.
 * Please note that text strings can contain special
 * characters such as %%c for a diameter sign as well as unicode
 * characters. Line feeds are stored as real line feeds in the string.
 */
class FAS_MText : public FAS_AtomicEntity
{
public:
    FAS_MText(FAS_EntityContainer* parent,
              const FAS_MTextData& d);
    virtual ~FAS_MText(){}

    virtual FAS_Entity* clone() const override;

    virtual FAS2::EntityType rtti() const override
    {
        return FAS2::EntityMText;
    }
    FAS_MTextData getData() const
    {
        return data;
    }
    void update() override;
    FAS_Vector getInsertionPoint()
    {
        return data.insertionPoint;
    }
    double getHeight()
    {
        return data.height;
    }
    void setHeight(double h)
    {
        data.height = h;
    }
    double getWidth()
    {
        return data.width;
    }
    void setAlignment(int a);
    int getAlignment();
    FAS_MTextData::VAlign getVAlign()
    {
        return data.valign;
    }
    void setVAlign(FAS_MTextData::VAlign va)
    {
        data.valign = va;
    }
    FAS_MTextData::HAlign getHAlign()
    {
        return data.halign;
    }
    void setHAlign(FAS_MTextData::HAlign ha)
    {
        data.halign = ha;
    }
    FAS_MTextData::MTextDrawingDirection getDrawingDirection()
    {
        return data.drawingDirection;
    }
    FAS_MTextData::MTextLineSpacingStyle getLineSpacingStyle()
    {
        return data.lineSpacingStyle;
    }
    void setLineSpacingFactor(double f)
    {
        data.lineSpacingFactor = f;
    }
    double getLineSpacingFactor()
    {
        return data.lineSpacingFactor;
    }
    void setText(const QString& t);
    QString getText()
    {
        return data.text;
    }
    void setStyle(const QString& s)
    {
        data.style = s;
    }
    QString getStyle()
    {
        return data.style;
    }
    void setAngle(double a)
    {
        data.angle = a;
    }
    double getAngle()
    {
        return data.angle;
    }
    double getUsedTextWidth()
    {
        return usedTextWidth;
    }
    double getUsedTextHeight()
    {
        return usedTextHeight;
    }

    virtual FAS_Vector getMiddlePoint(void)const;
    // return The insertion point as endpoint.
    virtual FAS_Vector getNearestEndpoint(const FAS_Vector& coord, double* dist = nullptr)const override;
    virtual FAS_VectorSolutions getRefPoints() const override;

    virtual void move(const FAS_Vector& offset) override;
    virtual void rotate(const FAS_Vector& center, const double& angle) override;
    virtual void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    virtual void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    virtual void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    virtual bool hasEndpointsWithinWindow(const FAS_Vector& v1, const FAS_Vector& v2) override;
    virtual void stretch(const FAS_Vector& firstCorner,
                         const FAS_Vector& secondCorner,
                         const FAS_Vector& offset) override;

    virtual FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                              bool onEntity = true, double* dist = nullptr,
                                              FAS_Entity** entity = nullptr) const override;
    virtual FAS_Vector getNearestMiddle(const FAS_Vector& coord, double* dist = nullptr, int middlePoints = 1) const override;
    virtual FAS_Vector getNearestDist(double distance, const FAS_Vector& coord, double* dist = nullptr) const override;
    virtual void calculateBorders() override;
    friend std::ostream& operator << (std::ostream& os, const FAS_MText& p);
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
protected:
    FAS_MTextData data;
    // Text width used by the current contents of this text entity.
    double usedTextWidth;
    //Text height used by the current contents of this text entity.
    double usedTextHeight;
};

#endif
