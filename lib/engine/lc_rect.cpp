/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

//sort with alphabetical order
#include <iostream>
#include <QDebug>
#include "lc_rect.h"

using namespace lc::geo;

Coordinate LC_Rect::Vector(Coordinate const& p, Coordinate const& q)
{
    return {q.x - p.x, q.y - p.y};
}

/*
  * Create a new Area. The coordinates coordA and coordB will be ordered so that minP wil always be < maxP
  * The corordinates are not allowed to describe a volume
  */
LC_Rect::Area(const Coordinate& coordA, const Coordinate& coordB) :
    _minP{std::min(coordA.x, coordB.x), std::min(coordA.y, coordB.y)},
    _maxP{std::max(coordA.x, coordB.x), std::max(coordA.y, coordB.y)}
{
}

LC_Rect::Area() : _minP{0., 0.}, _maxP{0., 0.} {}

// given at a coordinate with a given width and height
LC_Rect::Area(const Coordinate& coord, double width, double height):
    Area(coord, {coord.x + width, coord.y + height})
{}

// Return the smalles corner (closest to (0,0,0) )
const Coordinate& LC_Rect::minP() const
{
    return _minP;
}

// Return the heigest corner
const Coordinate& LC_Rect::maxP() const
{
    return _maxP;
}
//return the upperLeftCorner coordinates. _minP is considered lowerLeft, _maxP is the upperRight
Coordinate LC_Rect::upperLeftCorner() const
{
    return {_minP.x, _maxP.y};
}
Coordinate LC_Rect::upperRightCorner() const
{
    return _maxP;
}

//lowerRightCorner return the lowerRight coordinates. _minP is considered lowerLeft, _maxP is the upperRight
Coordinate LC_Rect::lowerRightCorner() const
{
    return {_maxP.x, _minP.y};
}

Coordinate LC_Rect::lowerLeftCorner() const
{
    return _minP;
}

// Returns the wid th of this area
double LC_Rect::width() const
{
    return _maxP.x - _minP.x;
}

// Returns the height f this area
double LC_Rect::height() const
{
    return _maxP.y - _minP.y;
}

// return boolean true of the point is within the area
bool LC_Rect::inArea(const Coordinate& point, double tolerance) const
{
    return (point.x >= _minP.x - tolerance && point.x <= _maxP.x + tolerance && point.y >= _minP.y - tolerance && point.y <= _maxP.y + tolerance);
}

// test if this object's fit's fully in area
bool LC_Rect::inArea(const Area& area) const
{
    return _minP.x >= area._minP.x && _minP.y >= area._minP.y && _maxP.x <= area._maxP.x && _maxP.y <= area._maxP.y;
}

// returns true if any overlap is happening between the two area's, even if otherArea fit's within this area
bool LC_Rect::overlaps(const Area& otherArea) const
{
    return intersects(otherArea);
}

// count the number of corners this object has in otherArea
short LC_Rect::numCornersInside(const Area& otherArea) const
{
    short pointsInside = 0;

    if (otherArea.inArea(_minP))
    {
        pointsInside++;
    }

    if (otherArea.inArea(_maxP))
    {
        pointsInside++;
    }

    if (otherArea.inArea(upperLeftCorner()))
    {
        pointsInside++;
    }

    if (otherArea.inArea(lowerRightCorner()))
    {
        pointsInside++;
    }

    return pointsInside;
}

// two area's and expand if required to largest containing area
Area LC_Rect::merge(const Area& other) const
{
    return
    {
        {std::min(other.minP().x, this->minP().x), std::min(other.minP().y, this->minP().y)},
        {std::max(other.maxP().x, this->maxP().x), std::max(other.maxP().y, this->maxP().y)}
    };
}

// two area's and expand if required to largest containing area
Area LC_Rect::merge(const Coordinate& other) const
{
    return
    {
        {std::min(other.x, this->minP().x), std::min(other.y, this->minP().y)},
        {std::max(other.x, this->maxP().x), std::max(other.y, this->maxP().y)}
    };
}

// merge two area's and expand if required to largest containing area
Area LC_Rect::intersection(const Area& other, double tolerance) const
{
    Area const ret
    {
        {std::max(other.minP().x, this->minP().x), std::max(other.minP().y, this->minP().y)},
        {std::min(other.maxP().x, this->maxP().x), std::min(other.maxP().y, this->maxP().y)}
    };
    if (ret.width() < tolerance || ret.height() < tolerance)
    {
        return {};
    }
    return ret;
}

bool LC_Rect::intersects(Area const& rhs, double tolerance) const
{
    return maxP().x + tolerance >= rhs.minP().x &&
            maxP().y + tolerance >= rhs.minP().y &&
            rhs.maxP().x + tolerance >= minP().x &&
            rhs.maxP().y + tolerance >= minP().y;
}

// vector of this area
Coordinate LC_Rect::top() const
{
    return Vector(upperLeftCorner(), _maxP);
}

//bottom vector of this area
Coordinate LC_Rect::bottom() const
{
    return Vector(_minP, lowerRightCorner());
}

// left vector for this area
Coordinate LC_Rect::left() const
{
    return Vector(_minP, upperLeftCorner());
}

// right vector of this area
Coordinate LC_Rect::right() const
{
    return Vector(lowerRightCorner(), _maxP);
}

// Increase the area on each side by increaseBy
Area LC_Rect::increaseBy(double increaseBy) const
{
    return {_minP - increaseBy, _maxP + increaseBy};
}

std::array<Coordinate, 4> LC_Rect::vertices() const
{
    return {{lowerLeftCorner(), lowerRightCorner(),
                    upperRightCorner(), upperLeftCorner()}};
}

std::ostream& operator<<(std::ostream& os, const Area& area)
{
    os << "Area(" << area.minP() << " " << area.maxP() << ")";
    return os;
}
