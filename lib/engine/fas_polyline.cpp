/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_polyline.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>

#include "fas_arc.h"
#include "fas_graphicview.h"
#include "fas_information.h"
#include "fas_line.h"
#include "fas_insert.h"
#include "fas_math.h"

FAS_PolylineData::FAS_PolylineData():
    startpoint(false)
  ,endpoint(false)
{
}

FAS_PolylineData::FAS_PolylineData(const FAS_Vector& _startpoint,
                                   const FAS_Vector& _endpoint,
                                   bool _closed):
    startpoint(_startpoint)
  ,endpoint(_endpoint)
{
    if (_closed)
    {
        setFlag(FAS2::FlagClosed);
    }
}

std::ostream& operator << (std::ostream& os, const FAS_PolylineData& pd)
{
    os << "(" << pd.startpoint <<
          "/" << pd.endpoint <<
          ")";
    return os;
}

FAS_Polyline::FAS_Polyline(FAS_EntityContainer* parent)
    :FAS_EntityContainer(parent, true)
    ,closingEntity(nullptr)
    ,nextBulge(0.)
{
}

FAS_Polyline::FAS_Polyline(FAS_EntityContainer* parent,
                           const FAS_PolylineData& d)
    :FAS_EntityContainer(parent, true)
    ,data(d)
    ,closingEntity(nullptr)
    ,nextBulge(0.)
{
    calculateBorders();
}

FAS_Entity* FAS_Polyline::clone() const
{
    FAS_Polyline* p = new FAS_Polyline(*this);
    p->setOwner(isOwner());
    p->initId();
    p->detach();
    return p;
}

// Removes the last vertex of this polyline.
void FAS_Polyline::removeLastVertex()
{
    FAS_Entity* l = last();
    if (l)
    {
        removeEntity(l);
        l = last();
        if (l)
        {
            if (l->isAtomic())
            {
                data.endpoint = l->getEndpoint();
            }
        }
    }
}


/*
 * Adds a vertex from the endpoint of the last segment or
 * from the startpoint of the first segment to 'v' or
 * sets the startpoint to the point 'v'.
 * The very first vertex added with this method is the startpoint.
 * param v vertex coordinate to be added
 * param bulge The bulge of the arc or 0 for a line segment (see DXF documentation)
 * param prepend true: prepend at start instead of append at end
 *
 * return Pointer to the entity that was addded or nullptr if this
 *         was the first vertex added.
 */
FAS_Entity* FAS_Polyline::addVertex(const FAS_Vector& v, double bulge, bool prepend)
{
    FAS_Entity* entity = nullptr;
    // very first vertex:
    if (!data.startpoint.valid)
    {
        data.startpoint = data.endpoint = v;
        nextBulge = bulge;
    }
    // consequent vertices:
    else
    {
        // add entity to the polyline:
        entity = createVertex(v, nextBulge, prepend);
        if (entity)
        {
            if (!prepend)
            {
                FAS_EntityContainer::addEntity(entity);
                data.endpoint = v;
            }
            else
            {
                FAS_EntityContainer::insertEntity(0, entity);
                data.startpoint = v;
            }
        }
        nextBulge = bulge;
        endPolyline();
    }
    return entity;
}

/*
 * Appends a vertex list from the endpoint of the last segment
 * sets the startpoint to the first point if not exist.
 * The very first vertex added with this method is the startpoint if not exists.
 * param vl list of vertexs coordinate to be added
 * param Pair are FAS_Vector of coord and the bulge of the arc or 0 for a line segment (see DXF documentation)
 * return None
 */
void FAS_Polyline::appendVertexs(const std::vector< std::pair<FAS_Vector, double> >& vl)
{
    FAS_Entity* entity=nullptr;
    if (!vl.size())
        return;
    size_t idx = 0;
    if (!data.startpoint.valid)
    {
        data.startpoint = data.endpoint = vl.at(idx).first;
        nextBulge = vl.at(idx++).second;
    }
    // consequent vertices:
    for (; idx< vl.size();idx++)
    {
        entity = createVertex(vl.at(idx).first, nextBulge, false);
        data.endpoint = entity->getEndpoint();
        FAS_EntityContainer::addEntity(entity);
        nextBulge = vl.at(idx).second;
    }
    endPolyline();
}

/*
 * Creates a vertex from the endpoint of the last element or
 * sets the startpoint to the point 'v'.
 * The very first vertex added is the starting point.
 * param v vertex coordinate
 * param bulge The bulge of the arc (see DXF documentation)
 * param prepend true: Prepend instead of append at end
 * return Pointer to the entity that was created or nullptr if this
 *         was the first vertex added.
 */
FAS_Entity* FAS_Polyline::createVertex(const FAS_Vector& v, double bulge, bool prepend)
{
    FAS_Entity* entity=nullptr;
    // create line for the polyline:
    if (fabs(bulge)<FAS_TOLERANCE)
    {
        if (prepend)
        {
            entity = new FAS_Line{this, v, data.startpoint};
        }
        else
        {
            entity = new FAS_Line{this, data.endpoint, v};
        }
        entity->setSelected(isSelected());
        entity->setPen(FAS_Pen(FAS2::FlagInvalid));
        entity->setLayer(nullptr);
    }
    // create arc for the polyline:
    else
    {
        bool reversed = (bulge<0.0);
        double alpha = atan(bulge)*4.0;

        FAS_Vector middle;
        double dist;
        double angle;
        if (!prepend)
        {
            middle = (data.endpoint+v)/2.0;
            dist = data.endpoint.distanceTo(v)/2.0;
            angle = data.endpoint.angleTo(v);
        }
        else
        {
            middle = (data.startpoint+v)/2.0;
            dist = data.startpoint.distanceTo(v)/2.0;
            angle = v.angleTo(data.startpoint);
        }
        double const radius = fabs(dist / sin(alpha/2.0));
        double const wu = fabs(radius*radius - dist*dist);
        double h = sqrt(wu);
        if (bulge>0.0)
        {
            angle+=M_PI_2;
        }
        else
        {
            angle-=M_PI_2;
        }
        if (fabs(alpha)>M_PI)
        {
            h*=-1.0;
        }
        FAS_Vector center = FAS_Vector::polar(h, angle);
        center+=middle;
        double a1;
        double a2;
        if (!prepend)
        {
            a1 = center.angleTo(data.endpoint);
            a2 = center.angleTo(v);
        }
        else
        {
            a1 = center.angleTo(v);
            a2 = center.angleTo(data.startpoint);
        }

        FAS_ArcData const d(center, radius,
                            a1, a2,
                            reversed);

        entity = new FAS_Arc(this, d);
        entity->setSelected(isSelected());
        entity->setPen(FAS_Pen(FAS2::FlagInvalid));
        entity->setLayer(nullptr);
    }

    return entity;
}


// Ends polyline and adds the last entity if the polyline is closed
void FAS_Polyline::endPolyline()
{
    if (isClosed())
    {
        // remove old closing entity:
        if (closingEntity)
        {
            removeEntity(closingEntity);
        }
        // add closing entity to the polyline:
        closingEntity = createVertex(data.startpoint, nextBulge);
        if (closingEntity)
        {
            FAS_EntityContainer::addEntity(closingEntity);
        }
    }
    calculateBorders();
}

void FAS_Polyline::setClosed(bool cl, double bulge)
{
    Q_UNUSED(bulge);
    bool areClosed = isClosed();
    setClosed(cl);
    if (isClosed())
    {
        endPolyline();
    }
    else if (areClosed)
    {
        removeLastVertex();
    }
}

// sets a new start point of the polyline
void FAS_Polyline::setStartpoint(FAS_Vector const& v)
{
    data.startpoint = v;
    if (!data.endpoint.valid)
    {
        data.endpoint = v;
    }
}

//return Start point of the entity
FAS_Vector FAS_Polyline::getStartpoint() const
{
    return data.startpoint;
}

/** sets a new end point of the polyline */
void FAS_Polyline::setEndpoint(FAS_Vector const& v)
{
    data.endpoint = v;
}

//return End point of the entity
FAS_Vector FAS_Polyline::getEndpoint() const
{
    return data.endpoint;
}

// @return The bulge of the closing entity.
double FAS_Polyline::getClosingBulge() const
{
    if (isClosed())
    {
        FAS_Entity const* e = last();
        if (e && e->rtti()==FAS2::EntityArc)
        {
            return static_cast<FAS_Arc const*>(e)->getBulge();
        }
    }
    return 0.0;
}

bool FAS_Polyline::isClosed() const
{
    return data.getFlag(FAS2::FlagClosed);
}

void FAS_Polyline::setClosed(bool cl)
{
    if (cl)
    {
        data.setFlag(FAS2::FlagClosed);
    }
    else
    {
        data.delFlag(FAS2::FlagClosed);
    }
}

// Sets the polylines start and endpoint to match the first and last vertex.
void FAS_Polyline::updateEndpoints()
{
    FAS_Entity* e1 = firstEntity();
    if (e1 && e1->isAtomic())
    {
        FAS_Vector const& v = e1->getStartpoint();
        setStartpoint(v);
    }

    FAS_Entity const* e2 = last();
    if (isClosed())
    {
        e2 = prevEntity();
    }
    if (e2 && e2->isAtomic())
    {
        FAS_Vector const& v = e2->getEndpoint();
        setEndpoint(v);
    }
}

/**
 * Reimplementation of the addEntity method for a normal container.
 * This reimplementation deletes the given entity!
 *
 * To add entities use addVertex() or addSegment() instead.
 */
void FAS_Polyline::addEntity(FAS_Entity* /*entity*/)
{
}


FAS_VectorSolutions FAS_Polyline::getRefPoints() const
{
    FAS_VectorSolutions ret{{data.startpoint}};
    for(auto e: *this)
    {
        if (e->isAtomic())
        {
            ret.push_back(e->getEndpoint());
        }
    }
    ret.push_back( data.endpoint);
    return ret;
}

FAS_Vector FAS_Polyline::getNearestRef( const FAS_Vector& coord, double* dist /*= nullptr*/) const
{
    return FAS_Entity::getNearestRef( coord, dist);
}

FAS_Vector FAS_Polyline::getNearestSelectedRef( const FAS_Vector& coord, double* dist /*= nullptr*/) const
{
    return FAS_Entity::getNearestSelectedRef( coord, dist);
}

//this should handle modifyOffset
bool FAS_Polyline::offset(const FAS_Vector& coord, const double& distance)
{
    double dist;
    //find the nearest one
    int length=count();
    std::vector<FAS_Vector> intersections(length);
    if(length>1)
    {
        //sort the polyline entity start/end point order
        int i(0);
        double d0,d1;
        FAS_Entity* en0(entityAt(0));
        FAS_Entity* en1(entityAt(1));
        FAS_Vector vStart(en0->getStartpoint());
        FAS_Vector vEnd(en0->getEndpoint());
        en1->getNearestEndpoint(vStart,&d0);
        en1->getNearestEndpoint(vEnd,&d1);
        if(d0<d1) en0->revertDirection();
        for(i=1;i<length;en0=en1)
        {
            //linked to head-tail chain
            en1=entityAt(i);
            vStart=en1->getStartpoint();
            vEnd=en1->getEndpoint();
            en0->getNearestEndpoint(vStart,&d0);
            en0->getNearestEndpoint(vEnd,&d1);
            if(d0>d1) en1->revertDirection();
            intersections[i-1]=(en0->getEndpoint()+en1->getStartpoint())*0.5;
            i++;
        }
    }
    FAS_Entity* en(getNearestEntity(coord, &dist, FAS2::ResolveNone));
    if(!en) return false;
    int indexNearest=findEntity(en);
    FAS_Polyline* pnew= static_cast<FAS_Polyline*>(clone());
    int i;
    i=indexNearest;
    int previousIndex(i);
    pnew->entityAt(i)->offset(coord,distance);
    FAS_Vector vp;
    //offset all
    for(i=indexNearest-1;i>=0;i--)
    {
        FAS_VectorSolutions sol0=FAS_Information::getIntersection(pnew->entityAt(previousIndex),entityAt(i),true);
        double dmax(FAS_TOLERANCE15);
        FAS_Vector trimP(false);
        for(const FAS_Vector& vp: sol0)
        {
            double d0( (vp - pnew->entityAt(previousIndex)->getStartpoint()).squared());//potential bug, need to trim better
            if(d0>dmax)
            {
                dmax=d0;
                trimP=vp;
            }
        }
        if(trimP.valid)
        {
            static_cast<FAS_AtomicEntity*>(pnew->entityAt(previousIndex))->trimStartpoint(trimP);
            static_cast<FAS_AtomicEntity*>(pnew->entityAt(i))->trimEndpoint(trimP);
            vp=pnew->entityAt(previousIndex)->getMiddlePoint();
        }
        else
        {
            vp=pnew->entityAt(previousIndex)->getStartpoint();
            vp.rotate(entityAt(previousIndex)->getStartpoint(),entityAt(i)->getDirection2()-entityAt(previousIndex)->getDirection1()+M_PI);
        }
        pnew->entityAt(i)->offset(vp,distance);
        previousIndex=i;
    }

    previousIndex=indexNearest;
    for(i=indexNearest+1;i<length;i++)
    {
        FAS_VectorSolutions sol0=FAS_Information::getIntersection(pnew->entityAt(previousIndex),entityAt(i),true);
        double dmax(FAS_TOLERANCE15);
        FAS_Vector trimP(false);
        for(const FAS_Vector& vp: sol0)
        {
            double d0( (vp - pnew->entityAt(previousIndex)->getEndpoint()).squared());//potential bug, need to trim better
            if(d0>dmax)
            {
                dmax=d0;
                trimP=vp;
            }
        }
        if(trimP.valid)
        {
            static_cast<FAS_AtomicEntity*>(pnew->entityAt(previousIndex))->trimEndpoint(trimP);
            static_cast<FAS_AtomicEntity*>(pnew->entityAt(i))->trimStartpoint(trimP);
            vp=pnew->entityAt(previousIndex)->getMiddlePoint();
        }
        else
        {
            vp=pnew->entityAt(previousIndex)->getEndpoint();
            vp.rotate(entityAt(previousIndex)->getEndpoint(),entityAt(i)->getDirection1()-entityAt(previousIndex)->getDirection2()+M_PI);
        }
        pnew->entityAt(i)->offset(vp,distance);
        previousIndex=i;
    }
    //connect and trim
    for(i=0;i<length-1;i++)
    {
        FAS_VectorSolutions sol0=FAS_Information::getIntersection(pnew->entityAt(i),pnew->entityAt(i+1),true);
        if(sol0.getNumber()==0)
        {
            sol0=FAS_Information::getIntersection(pnew->entityAt(i),pnew->entityAt(i+1));
            FAS_VectorSolutions sol1;
            for(const FAS_Vector& vp: sol0)
            {
                if(!FAS_Math::isAngleBetween(intersections.at(i).angleTo(vp),
                                             pnew->entityAt(i)->getDirection2(),
                                             pnew->entityAt(i+1)->getDirection1(),
                                             false))
                {
                    sol1.push_back(vp);
                }
            }
            if(sol1.getNumber()==0) continue;
            FAS_Vector trimP(sol1.getClosest(intersections.at(i)));
            static_cast<FAS_AtomicEntity*>(pnew->entityAt(i))->trimEndpoint(trimP);
            static_cast<FAS_AtomicEntity*>(pnew->entityAt(i+1))->trimStartpoint(trimP);
        }
        else
        {
            FAS_Vector trimP(sol0.getClosest(pnew->entityAt(i)->getStartpoint()));
            static_cast<FAS_AtomicEntity*>(pnew->entityAt(i))->trimEndpoint(trimP);
            static_cast<FAS_AtomicEntity*>(pnew->entityAt(i+1))->trimStartpoint(trimP);
        }
    }

    *this = *pnew;
    return true;
}

void FAS_Polyline::move(const FAS_Vector& offset)
{
    FAS_EntityContainer::move(offset);
    data.startpoint.move(offset);
    data.endpoint.move(offset);
    calculateBorders();
}

void FAS_Polyline::rotate(const FAS_Vector& center, const double& angle)
{
    rotate(center, FAS_Vector(angle));
}


void FAS_Polyline::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    FAS_EntityContainer::rotate(center, angleVector);
    data.startpoint.rotate(center, angleVector);
    data.endpoint.rotate(center, angleVector);
    calculateBorders();
}

void FAS_Polyline::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    FAS_EntityContainer::scale(center, factor);
    data.startpoint.scale(center, factor);
    data.endpoint.scale(center, factor);
    calculateBorders();
}

void FAS_Polyline::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    FAS_EntityContainer::mirror(axisPoint1, axisPoint2);
    data.startpoint.mirror(axisPoint1, axisPoint2);
    data.endpoint.mirror(axisPoint1, axisPoint2);
    calculateBorders();
}

void FAS_Polyline::moveRef(const FAS_Vector& ref, const FAS_Vector& offset)
{
    FAS_EntityContainer::moveRef(ref, offset);
    if (ref.distanceTo(data.startpoint)<1.0e-4)
    {
        data.startpoint.move(offset);
    }
    if (ref.distanceTo(data.endpoint)<1.0e-4)
    {
        data.endpoint.move(offset);
    }
    calculateBorders();
}

void FAS_Polyline::revertDirection()
{
    FAS_EntityContainer::revertDirection();
    FAS_Vector tmp = data.startpoint;
    data.startpoint = data.endpoint;
    data.endpoint = tmp;
}

void FAS_Polyline::stretch(const FAS_Vector& firstCorner,
                           const FAS_Vector& secondCorner,
                           const FAS_Vector& offset)
{

    if (data.startpoint.isInWindow(firstCorner, secondCorner))
    {
        data.startpoint.move(offset);
    }
    if (data.endpoint.isInWindow(firstCorner, secondCorner))
    {
        data.endpoint.move(offset);
    }

    FAS_EntityContainer::stretch(firstCorner, secondCorner, offset);
    calculateBorders();
}


// Slightly optimized drawing for polylines.
void FAS_Polyline::draw(FAS_Painter* painter,FAS_GraphicView* view, double& /*patternOffset*/)
{

    if (!view)
        return;
    COMMONDEF->drawEntityCount++;
    // draw first entity and set correct pen:
    FAS_Entity* e = firstEntity(FAS2::ResolveNone);
    // We get the pen from the entitycontainer and apply it to the
    // first line so that subsequent line are draw in the right color
    // prevent segfault if polyline is empty
    if (e)
    {
        FAS_Pen p=this->getPen(true);
        e->setPen(p);
        double patternOffset=0.;
        view->drawEntity(painter, e, patternOffset);

        e = nextEntity(FAS2::ResolveNone);
        while(e)
        {
            view->drawEntityPlain(painter, e, patternOffset);
            e = nextEntity(FAS2::ResolveNone);
        }
    }
}
void FAS_Polyline::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,
                  double& patternOffset,FAS_InsertData& dataInsert)
{
    if (! (painter && view))
    {
        return;
    }

    if (!view)
        return;
    // draw first entity and set correct pen:
    FAS_Entity* e = firstEntity(FAS2::ResolveNone);
    // We get the pen from the entitycontainer and apply it to the
    // first line so that subsequent line are draw in the right color
    // prevent segfault if polyline is empty
    if (e)
    {
        FAS_Pen p=this->getPen(true);
        e->setPen(p);
        double patternOffset=0.;
        view->drawEntity(painter, e, patternOffset,dataInsert);

        e = nextEntity(FAS2::ResolveNone);
        while(e)
        {
            view->drawEntity(painter, e, patternOffset,dataInsert);
            e = nextEntity(FAS2::ResolveNone);
        }
    }
}
void FAS_Polyline::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor)
{
//    resetBorders();
    FAS_Vector mstartPoint=getStartpoint();
    FAS_Vector mendPoint=getEndpoint();
    FAS_Vector bkstartPoint=data.bkstartpoint;
    FAS_Vector bkendPoint=data.bkendpoint;
    if(!bkstartPoint.valid && !bkendPoint.valid){
        data.bkstartpoint=mstartPoint;
        data.bkendpoint=mendPoint;
    }else{
        data.startpoint=data.bkstartpoint;
        data.endpoint=data.bkendpoint;
    }
    FAS_EntityContainer::resetBasicData(offset,center,angle,factor);

}

void FAS_Polyline::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){

        data.startpoint.move(offset);
        data.endpoint.move(offset);

        data.startpoint.rotate(center, angle);
        data.endpoint.rotate(center, angle);

        data.startpoint.scale(center, factor);
        data.endpoint.scale(center, factor);

        FAS_EntityContainer::moveEntityBoarder(offset,center,angle,factor);
}


// Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_Polyline& l) {
    os << " Polyline: " << l.getData() << " {\n";
    os << (FAS_EntityContainer&)l;
    os << "\n}\n";
    return os;
}

