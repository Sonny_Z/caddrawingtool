/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_PATTERNLIST_H
#define FAS_PATTERNLIST_H

#include<map>
#include<memory>

#include "fas_pattern.h"

#define FAS_PATTERNLIST FAS_PatternList::instance()

// The global list of patterns. This is implemented as a singleton.
class FAS_PatternList
{
    using PTN_MAP = std::map<QString, std::unique_ptr<FAS_Pattern>>;
    FAS_PatternList() {}

public:
    static FAS_PatternList* instance();

    ~FAS_PatternList();
    FAS_PatternList(FAS_PatternList const&) = delete;
    FAS_PatternList& operator = (FAS_PatternList const&) = delete;
    FAS_PatternList(FAS_PatternList &&) = delete;
    FAS_PatternList& operator = (FAS_PatternList &&) = delete;

    void init();

    int countPatterns() const
    {
        return patterns.size();
    }

    // range based loop support
    PTN_MAP::iterator begin()
    {
        return patterns.begin();
    }
    PTN_MAP::const_iterator begin() const
    {
        return patterns.begin();
    }
    PTN_MAP::iterator end()
    {
        return patterns.end();
    }
    PTN_MAP::const_iterator end() const
    {
        return patterns.end();
    }

    FAS_Pattern* requestPattern(const QString& name);
    bool contains(const QString& name) const;
    friend std::ostream& operator << (std::ostream& os, FAS_PatternList& l);

private:
    //! patterns in the graphic
    PTN_MAP patterns;
};

#endif
