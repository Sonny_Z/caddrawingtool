#include "fas_attdef.h"
#include "fas_math.h"
#include "fas_graphicview.h"
#include "fas_painter.h"
#include "fas_painterqt.h"
#include "fas_insert.h"
FAS_AttdefData::FAS_AttdefData(FAS_TextData _textdata,QString _label):
textdata(_textdata),label(_label)
{

}
FAS_ATTDEF::FAS_ATTDEF(FAS_EntityContainer* parent,
                                  const FAS_AttdefData& d)//const FAS_MTextData& d)
    :FAS_Text(parent,d.textdata),data(d)
{

}
void FAS_ATTDEF::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center1, const double& angle,const FAS_Vector& factor){

    FAS_Vector secondPoint=getSecondPoint();
    FAS_Vector bksecondPoint=data.textdata.bksecondPoint;
    if(!bksecondPoint.valid)
    {
        data.textdata.bkinsertionPoint=data.textdata.insertionPoint;
        data.textdata.bksecondPoint=secondPoint;
        data.textdata.bkheight=data.textdata.height;
        data.textdata.bkangle=data.textdata.angle;
    }else{
        data.textdata.insertionPoint=data.textdata.bkinsertionPoint;
        data.textdata.secondPoint=data.textdata.bksecondPoint;
        data.textdata.height=data.textdata.bkheight;
        data.textdata.angle=data.textdata.bkangle;
    }
}

void FAS_ATTDEF::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){
    data.textdata.insertionPoint.move(offset);
    data.textdata.secondPoint.move(offset);


    FAS_Vector angleVector(angle);
    data.textdata.insertionPoint.rotate(center, angleVector);
    data.textdata.secondPoint.rotate(center, angleVector);
    data.textdata.angle = FAS_Math::correctAngle(data.textdata.angle+angle);


    data.textdata.insertionPoint.scale(center, factor);
    data.textdata.secondPoint.scale(center, factor);
    data.textdata.height*=factor.x;
    //update();

}
void FAS_ATTDEF::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& dataInsert)
{
    if (! (painter && view))
    {
        return;
    }

    FAS_PainterQt* painter1=dynamic_cast<FAS_PainterQt*>(painter);


    if (!(painter1 && view))
    {
        return;
    }
    COMMONDEF->drawEntityCount++;



    double xLeft = view->toGui(minV).x;
    double yTop = view->toGui(maxV).y;
    double height = data.textdata.height;//this->getHeight();

    QFont font("System", height * view->getFactor().y );
    painter1->setFont(font);





    if(painter1!=NULL)
    {
        FAS_Vector leftTop(xLeft,yTop);
        QMap<QString, FAS_AttribData> mmap=dataInsert.mmap;
//        qDebug()<<"label:"<<data.label;
        QString key=data.label;
        FAS_AttribData v=mmap[key];
        double angle=v.angle;//data.textdata.angle*180/M_PI;
        QTransform transform;
        transform.translate(xLeft, yTop);
        transform.rotate(-angle);
        painter1->setTransform(transform);
        //transform.scale(0.5, 1.0);

//        qDebug()<<"textvalue:"<<v.text;
        data.textdata.text=v.text;
        double width = v.height * v.text.size();

        painter1->drawTextH(0, 0, view->getFactor().x * width, view->getFactor().y * height, v.text);

        transform.rotate(angle);
        transform.translate(-xLeft, -yTop);
        painter1->setTransform(transform);

    }


}

void FAS_ATTDEF::calculateBorders()
{
    double left = 0.0, right = 0.0, bottom = 0.0, top = 0.0;
    double width = data.textdata.height * data.textdata.text.size() * 0.6;
    switch (data.textdata.halign) {
    case FAS_TextData::HALeft:
        left = data.textdata.insertionPoint.x;
        right = left + width;
        break;
    case FAS_TextData::HARight:
        right = data.textdata.insertionPoint.x;
        left = right - width;
        break;
    case FAS_TextData::HACenter:
    case FAS_TextData::HAMiddle:
    case FAS_TextData::HAFit:
        left = data.textdata.insertionPoint.x - 0.5 * width;
        right = data.textdata.insertionPoint.x + 0.5 * width;
        break;
    default:
        left = data.textdata.insertionPoint.x;
        right = left + width;
        break;
    }
    switch (data.textdata.valign) {
    case FAS_TextData::VABottom:
    case FAS_TextData::VABaseline:
        bottom = data.textdata.insertionPoint.y;
        top = bottom + data.textdata.height;
        break;
    case FAS_TextData::VATop:
        top = data.textdata.insertionPoint.y;
        bottom = top - data.textdata.height;
        break;
    case FAS_TextData::VAMiddle:
        top = data.textdata.insertionPoint.y + data.textdata.height * 0.5;
        bottom = data.textdata.insertionPoint.y - data.textdata.height * 0.5;
        break;
    default:
        bottom = data.textdata.insertionPoint.y;
        top = bottom + data.textdata.height;
        break;
    }

    minV = FAS_Vector(left, bottom);
    maxV = FAS_Vector(right, top);
}
