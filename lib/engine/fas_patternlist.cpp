/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/
#include "fas_patternlist.h"

//sort with alphabetical order
#include <iostream>
#include <QString>

#include "fas_system.h"
#include "fas_pattern.h"

FAS_PatternList* FAS_PatternList::instance()
{
    static FAS_PatternList instance;
    return &instance;
}

FAS_PatternList::~FAS_PatternList(){}

//Initializes the pattern list by creating empty FAS_Pattern
void FAS_PatternList::init()
{
    QStringList list = FAS_SYSTEM->getPatternList();
    patterns.clear();
    for (auto const& s: list)
    {
        QFileInfo fi(s);
        QString const name = fi.baseName().toLower();
        patterns[name] = std::unique_ptr<FAS_Pattern>{};
    }
}

// Pointer to the pattern with the given name or
FAS_Pattern* FAS_PatternList::requestPattern(const QString& name)
{
    QString name2 = name.toLower();
    if (patterns.count(name2))
    {
        if (!patterns[name2])
        {
            FAS_Pattern* p = new FAS_Pattern(name2);
            p->loadPattern();
            patterns[name2].reset(p);
        }
        return patterns[name2].get();
    }
    return nullptr;
}

bool FAS_PatternList::contains(const QString& name) const
{
    return patterns.count(name.toLower());
}

// Dumps the patterns to stdout.
std::ostream& operator << (std::ostream& os, FAS_PatternList& l)
{
    os << "Patternlist: \n";
    for (auto const& pa: l.patterns)
        if (pa.second)
            os<< *pa.second << '\n';
    return os;
}

