/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/


#ifndef FAS_UNDO_H
#define FAS_UNDO_H

#include <memory>
#include <vector>

class FAS_UndoCycle;
class FAS_Undoable;

// Undo / redo functionality. The internal undo list consists of
class FAS_Undo {
public:

    virtual bool undo();
    virtual bool redo();

    virtual int countUndoCycles();
    virtual int countRedoCycles();

    virtual void startUndoCycle();
    virtual void addUndoable(FAS_Undoable* u);
    virtual void endUndoCycle();

    /*
     * Must be overwritten by the implementing class and delete
     * the given Undoable (unrecoverable). This method is called
     * for Undoables that are no longer in the undo buffer.
     */
    virtual void removeUndoable(FAS_Undoable* u) = 0;
	void setGUIButtons() const;
    friend std::ostream& operator << (std::ostream& os, FAS_Undo& a);
    static bool test();
private:

	void addUndoCycle(std::shared_ptr<FAS_UndoCycle> const& i);
    // List of undo list items. every item is something that can be undone.
	std::vector<std::shared_ptr<FAS_UndoCycle>> undoList;
    /*
     * Index that points to the current position in the undo list.
     * The item it points on will be undone the next time undo is called.
     * The item after will be redone (if there is an item) when redo
     * is called.
     */
	int undoPointer = -1;
    /*
     * Current undo cycle.
     */
	std::shared_ptr<FAS_UndoCycle> currentCycle;
};

#endif

