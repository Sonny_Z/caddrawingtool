/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_CIRCLE_H
#define FAS_CIRCLE_H

#include <vector>
#include "fas_atomicentity.h"

class LC_Quadratic;

// Holds the data that defines a circle.
struct FAS_CircleData {
    FAS_CircleData() {}
    FAS_CircleData(FAS_Vector const& center, double radius);
    bool isValid() const;
    bool operator == (FAS_CircleData const&) const;
    FAS_Vector center;
    double radius;
    FAS_Vector bkcenter;
    double bkradius;
    bool isFirstDraw=true;
};

std::ostream& operator << (std::ostream& os, const FAS_CircleData& ad);

// Class for a circle entity.
class FAS_Circle : public FAS_AtomicEntity
{
public:
    FAS_Circle() {}
    FAS_Circle (FAS_EntityContainer* parent,
                const FAS_CircleData& d);
    ~FAS_Circle(){}

    FAS_Entity* clone() const override;

    FAS2::EntityType rtti() const override{
        return FAS2::EntityCircle;
    }
    bool isEdge() const  override{
        return true;
    }
    const FAS_CircleData& getData() const {
        return data;
    }

    FAS_VectorSolutions getRefPoints() const override;

    // return Direction 1. The angle at which the arc starts at the startpoint.
    double getDirection1() const override;
    // return Direction 2. The angle at which the arc starts at the endpoint.
    double getDirection2() const override;

    // return The center point (x) of this arc/
    FAS_Vector getCenter() const override;
    // Sets new center./
    void setCenter(const FAS_Vector& c);
    //return The radius of this arc
    double getRadius() const override;
    // Sets new radius.
    void setRadius(double r);
    double getAngleLength() const;
    double getLength() const override;
    bool isTangent(const FAS_CircleData&  circleData) const override;

    bool createFromCR(const FAS_Vector& c, double r);
    bool createFrom2P(const FAS_Vector& p1, const FAS_Vector& p2);
    bool createFrom3P(const FAS_Vector& p1, const FAS_Vector& p2,
                      const FAS_Vector& p3);
    bool createFrom3P(const FAS_VectorSolutions& sol);
    bool createInscribe(const FAS_Vector& coord, const std::vector<FAS_Line*>& lines);
    std::vector<FAS_Entity* > offsetTwoSides(const double& distance) const override;
    FAS_VectorSolutions createTan1_2P(const FAS_AtomicEntity* circle, const std::vector<FAS_Vector>& points);
    static FAS_VectorSolutions createTan2(const std::vector<FAS_AtomicEntity*>& circles, const double& r);
    static std::vector<FAS_Circle> solveAppolloniusSingle(const std::vector<FAS_Circle>& circles);
    std::vector<FAS_Circle> createTan3(const std::vector<FAS_AtomicEntity*>& circles);
    bool testTan3(const std::vector<FAS_AtomicEntity*>& circles);
    FAS_Vector getMiddlePoint(void)const override;
    FAS_Vector getNearestEndpoint(const FAS_Vector& coord,
                                  double* dist = nullptr) const override;
    FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                       bool onEntity = true, double* dist = nullptr, FAS_Entity** entity=nullptr)const override;
    FAS_Vector getNearestCenter(const FAS_Vector& coord,
                                double* dist = nullptr)const override;
    FAS_Vector getNearestMiddle(const FAS_Vector& coord,
                                double* dist = nullptr,
                                int middlePoints = 1 ) const override;
    FAS_Vector getNearestDist(double distance,
                              const FAS_Vector& coord,
                              double* dist = nullptr)const override;
    FAS_Vector getNearestDist(double distance,
                              bool startp)const override;
    FAS_Vector getNearestOrthTan(const FAS_Vector& coord,
                                 const FAS_Line& normal,
                                 bool onEntity = false) const override;

    bool offset(const FAS_Vector& coord, const double& distance) override;
    FAS_VectorSolutions getTangentPoint(const FAS_Vector& point) const override;//find the tangential points seeing from given point
    FAS_Vector getTangentDirection(const FAS_Vector& point)const override;
    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;
    /** whether the entity's bounding box intersects with visible portion of graphic view */
    bool isVisibleInWindow(FAS_GraphicView* view) const override;
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
//    void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    /* return the equation of the entity for quadratic,
    return a vector contains:
    m0 x^2 + m1 xy + m2 y^2 + m3 x + m4 y + m5 =0
    for linear:
    m0 x + m1 y + m2 =0
    */
    LC_Quadratic getQuadratic() const override;
    double areaLineIntegral() const override;
    friend std::ostream& operator << (std::ostream& os, const FAS_Circle& a);
    void calculateBorders() override;
protected:
    FAS_CircleData data;
};

#endif
