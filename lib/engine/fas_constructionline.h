/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_CONSTRUCTIONLINE_H
#define FAS_CONSTRUCTIONLINE_H

#include "fas_atomicentity.h"
#include "fas_vector.h"

// Holds the data that defines a construction line (a line which is not limited to both directions).
class FAS_ConstructionLineData {
public:
    FAS_ConstructionLineData();

    FAS_ConstructionLineData(const FAS_Vector& point1,
                             const FAS_Vector& point2);

    friend class FAS_ConstructionLine;

    friend std::ostream& operator << (std::ostream& os,
                                      const FAS_ConstructionLineData& ld);

private:
    FAS_Vector point1;
    FAS_Vector point2;
};


// Class for a construction line entity.
class FAS_ConstructionLine : public FAS_AtomicEntity {
public:
    FAS_ConstructionLine(){}
    FAS_ConstructionLine(FAS_EntityContainer* parent,
                         const FAS_ConstructionLineData& d);

    virtual FAS_Entity* clone() const;

    virtual ~FAS_ConstructionLine(){}

    virtual FAS2::EntityType rtti() const {
        return FAS2::EntityConstructionLine;
    }

    //return Copy of data that defines the line.
    FAS_ConstructionLineData const& getData() const;

    //return First definition point.
    FAS_Vector const& getPoint1() const;
    //return Second definition point.
    FAS_Vector const& getPoint2() const;

    virtual LC_Quadratic getQuadratic() const;
    virtual FAS_Vector getMiddlePoint(void) const;
    virtual FAS_Vector getNearestEndpoint(const FAS_Vector& coord,
                                          double* dist = nullptr)const;
    virtual FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                               bool onEntity = true, double* dist = nullptr, FAS_Entity** entity=nullptr)const;
    virtual FAS_Vector getNearestCenter(const FAS_Vector& coord,
                                        double* dist = nullptr)const;
    virtual FAS_Vector getNearestMiddle(const FAS_Vector& coord,
                                        double* dist = nullptr,
                                        int middlePoints = 1)const;
    virtual FAS_Vector getNearestDist(double distance,
                                      const FAS_Vector& coord,
                                      double* dist = nullptr)const;
    virtual double getDistanceToPoint(const FAS_Vector& coord,
                                      FAS_Entity** entity=nullptr,
                                      FAS2::ResolveLevel level=FAS2::ResolveNone,
                                      double solidDist = FAS_MAXDOUBLE) const;

    virtual void move(const FAS_Vector& offset);
    virtual void rotate(const FAS_Vector& center, const double& angle);
    virtual void rotate(const FAS_Vector& center, const FAS_Vector& angleVector);
    virtual void scale(const FAS_Vector& center, const FAS_Vector& factor);
    virtual void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2);

    virtual void draw(FAS_Painter* /*painter*/, FAS_GraphicView* /*view*/,
                      double& /*patternOffset*/) {}

    friend std::ostream& operator << (std::ostream& os,
                                      const FAS_ConstructionLine& l);

    virtual void calculateBorders();

protected:
    FAS_ConstructionLineData data;
};

#endif
