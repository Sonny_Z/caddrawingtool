/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_ENTITY_H
#define FAS_ENTITY_H

#include <map>
#include "fas_vector.h"
#include "fas_pen.h"
#include "fas_undoable.h"
#include "commondef.h"
class FAS_Block;
class FAS_Circle;
class FAS_Document;
class FAS_EntityContainer;
class FAS_Graphic;
class FAS_GraphicView;
class FAS_Insert;
class FAS_Line;
class FAS_Painter;
class FAS_Layer;
class LC_Quadratic;
class FAS_VectorSolutions;
class QString;
class FAS_InsertData;
//Base class for an entity (line, arc, circle, ...)
class FAS_Entity : public FAS_Undoable {
public:

    FAS_Entity(){}
    FAS_Entity(FAS_EntityContainer* parent=nullptr);
    virtual ~FAS_Entity() {}

    void init();
    virtual void initId();

    //enable entity's tree structure



    virtual FAS_Entity* clone() const = 0;

    virtual void reparent(FAS_EntityContainer* parent)
    {
        this->parent = parent;
    }
    void resetBorders();
    void moveBorders(const FAS_Vector& offset);
    void scaleBorders(const FAS_Vector& center, const FAS_Vector& factor);

    //Must be overwritten to return the rtti of this entity
    virtual FAS2::EntityType rtti() const
    {
        return FAS2::EntityUnknown;
    }

    // return FAS2::UndoableEntity
    virtual FAS2::UndoableType undoRtti() const override
    {
        return FAS2::UndoableEntity;
    }

    // return Unique Id of this entity.
    unsigned long int getId() const
    {
        return id;
    }

    //This method must be overwritten in subclasses and return the number of atomic entities in this entity.
    virtual unsigned int count() const= 0;

    // This method must be overwritten in subclasses and return the number of atomic entities in this entity including sub containers.
    virtual unsigned int countDeep() const= 0;
    virtual double getLength() const
    {
        return -1.0;
    }

    //return Parent of this entity or nullptr if this is a root entity.
    FAS_EntityContainer* getParent() const
    {
        return parent;
    }

    // Reparents this entity.
    void setParent(FAS_EntityContainer* p)
    {
        parent = p;
    }

    //return Parent of this entity or nullptr if this is a root entity.
    FAS_Entity* getCalParent() const
    {
        return calParent;
    }
    void setCalParent(FAS_Entity* p)
    {
        calParent = p;
    }

    QList<FAS_Entity*> getCalChilds() const
    {
        return calChilds;
    }

    void setCalChilds(FAS_Entity* p)
    {
        calChilds.append(p);
    }

    bool isLeaf()
    {
        if(0==calChilds.size()){
            return true;
        }

        return false;
    }

    bool isRoot()
    {
        if(nullptr==calParent){
            return true;
        }

        return false;
    }
    bool hasCalParent(){
        if(nullptr==calParent){
            return false;
        }

        return true;
    }

    bool addCalPath(FAS_Entity* line){
        calPath.append(line);
    }

    QList<FAS_Entity*> getCalPath(){
        return calPath;
    }

    bool setCalPath(QList<FAS_Entity*> &entitiesPath){
        calPath=entitiesPath;
        return true;
    }

    bool addMayRelatedPoints(QString point){
        relPoints.append(point);
        return true;
    }

    bool removeMayRelatedPoints(QString point){
        if(relPoints.contains(point)){
            for(int i=0;i<relPoints.size();i++){
                if(relPoints.value(i)==point){
                    relPoints.removeAt(i);
                    break;
                }
            }

         }
         return true;
    }

    void setConnectedPoints(QList<QString> points){
        connectedPoints=points;
    }

    bool addConnectedPoints(QString point){
        connectedPoints.append(point);
        return true;
    }

    bool removeConnectedPoints(QString point){
        if(connectedPoints.contains(point)){
            for(int i=0;i<connectedPoints.size();i++){
                if(connectedPoints.value(i)==point){
                    connectedPoints.removeAt(i);
                    break;
                }
            }

         }
         return true;
    }

    bool removeAllConnectedPoints(){
        connectedPoints.clear();
         return true;
    }
    int getConnectedPointsSize(){
        return connectedPoints.size();
    }

    QList<QString> getConnectedPoint(){
        return connectedPoints;
    }


    bool removeAllMayRelatedPoints(){
        relPoints.clear();

         return true;
    }

    int relatedPointsSize(){
        return relPoints.size();
    }

    QList<QString> getRelatedPoints(){
        return relPoints;
    }

    void setRelatedPoints(QList<QString> points){
        relPoints=points;
    }

    bool beginRelatedPointsTransaction(QString point){
        tempPoints.append(point);
         return true;
    }

    bool submitRelatedPointsTransaction(){
        if(tempPoints.size()>0){
            foreach(QString temPoint,tempPoints){
                relPoints.removeOne(temPoint);
            }
        }
        return true;
    }

    bool rollbackRelatedPointsTransaction(){
        if(tempPoints.size()>0){
            tempPoints.clear();
        }
        return true;
    }

    //get center for entities: arc, circle and ellipse
    virtual FAS_Vector getCenter() const;
    virtual double getRadius() const;
    FAS_Graphic* getGraphic() const;
    FAS_Block* getBlock() const;
    FAS_Insert* getInsert() const;
    FAS_Entity* getBlockOrInsert() const;
    FAS_Document* getDocument() const;

    void setLayer(const QString& name);
    void setLayer(FAS_Layer* l);
    void setLayerToActive();
    FAS_Layer* getLayer(bool resolve = true) const;

    /*
     * Sets the explicit pen for this entity or a pen with special
     * attributes such as BY_LAYER, ..
     */
    void setPen(const FAS_Pen& pen) {
        this->pen = pen;
    }
    void setPenToActive();
    FAS_Pen getPen(bool resolve = true) const;

    // Must be overwritten to return true if an entity type is a container for other entities (e.g. polyline, group, ...).
    virtual bool isContainer() const = 0;

    //Must be overwritten to return true if an entity type is an atomic entity.
    virtual bool isAtomic() const = 0;

    // Must be overwritten to return true if an entity type is a potential edge entity of a contour.
    virtual bool isEdge() const {
        return false;
    }

    //return true for all document entities (e.g. Graphics or Blocks).
    virtual bool isDocument() const {
        return false;
    }

    virtual bool setSelected(bool select);
    virtual bool toggleSelected();
    virtual bool isSelected() const;
    bool isParentSelected() const;
    virtual bool isProcessed() const;
    virtual void setProcessed(bool on);
    bool isInWindow(FAS_Vector v1, FAS_Vector v2) const;
    virtual bool hasEndpointsWithinWindow(const FAS_Vector& /*v1*/, const FAS_Vector& /*v2*/) {
        return false;
    }
    virtual bool isVisible() const;
    virtual void setVisible(bool v);
    virtual void setHighlighted(bool on);
    virtual bool isHighlighted() const;

    bool isLocked() const;

    void undoStateChanged(bool undone) override;
    virtual bool isUndone() const;

    /*
     * Can be implemented by child classes to update the entities
     * temporary subentities. update() is called if the entity's
     * paramters or undo state changed.
     */
    virtual void update() {}

    virtual void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) {}

    virtual void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) {}

    virtual void setUpdateEnabled(bool on) {
        updateEnabled = on;
    }

    // return minimum coordinate of the entity.
    FAS_Vector getMin() const {
        return minV;
    }

    // return minimum coordinate of the entity.
    FAS_Vector getMax() const {
        return maxV;
    }

    bool isInOrCoveredByEntity(FAS_Entity* e){
        bool result = false;
        auto minEP = e->getMin();
        auto maxEP = e->getMax();
        auto minE = this->getMin();
//        auto midE = this->getMiddlePoint();
        auto maxE = this->getMax();
        /*if(this->rtti() == FAS2::EntityInsert){
            if((minEP.x <= midE.x
                    && minEP.y <= midE.y)&&(
                     maxEP.x >= midE.x
                    && maxEP.y >= midE.y))
            {
                result = true;//is splitter

            }

        }else */if((minEP.x <= minE.x
                 && minEP.y <= minE.y)
                 &&( maxEP.x >= maxE.x
                 && maxEP.y >= maxE.y)){
                result = true;//is splitter
        }
        return result;
    }

    // This method returns the difference of max and min returned by the above functions.
    FAS_Vector getSize() const;
    double getLengthX() const;
    double getLengthY() const;

    void addGraphicVariable(const QString& key, double val, int code);
    void addGraphicVariable(const QString& key, int val, int code);
    void addGraphicVariable(const QString& key, const QString& val, int code);

    double getGraphicVariableDouble(const QString& key, double def);
    int getGraphicVariableInt(const QString& key, int def) const;
    QString getGraphicVariableString(const QString& key,
                                     const QString& def) const;
    virtual FAS_Vector getStartpoint() const;
    virtual FAS_Vector getEndpoint() const;
    //find the local direction at end points, derived entities
    // must implement this if direction is supported by the entity type
    virtual double getDirection1() const {
        return 0.;
    }
    virtual double getDirection2() const {
        return 0.;
    }

    //find the tangential points seeing from given point
    virtual FAS_VectorSolutions getTangentPoint(const FAS_Vector& /*point*/) const;
    virtual FAS_Vector getTangentDirection(const FAS_Vector& /*point*/)const;
    FAS2::Unit getGraphicUnit() const;

    // Must be overwritten to get all reference points of the entity.
    virtual FAS_VectorSolutions getRefPoints() const;

    /*
     * Must be overwritten to get the closest endpoint to the
     * given coordinate for this entity.
     *
     * @param coord Coordinate (typically a mouse coordinate)
     * @param dist Pointer to a value which will contain the measured
     * distance between 'coord' and the closest endpoint. The passed
     * pointer can also be nullptr in which case the distance will be
     * lost.
     *
     * @return The closest endpoint.
     */
    virtual FAS_Vector getNearestEndpoint(const FAS_Vector& coord,
                                          double* dist = nullptr)const = 0;

    /*
     * Must be overwritten to get the closest coordinate to the
     * given coordinate which is on this entity.
     *
     * param coord Coordinate (typically a mouse coordinate)
     * param dist Pointer to a value which will contain the measured
     * distance between \p coord and the point. The passed pointer can
     * also be \p nullptr in which case the distance will be lost.
     *
     * return The closest coordinate.
     */
    virtual FAS_Vector getNearestPointOnEntity(const FAS_Vector& /*coord*/,
                                               bool onEntity = true, double* dist = nullptr,
                                               FAS_Entity** entity = nullptr) const = 0;

    /*
     * Must be overwritten to get the (nearest) center point to the
     * given coordinate for this entity.
     *
     * param coord Coordinate (typically a mouse coordinate)
     * param dist Pointer to a value which will contain the measured
     * distance between 'coord' and the closest center point. The passed
     * pointer can also be nullptr in which case the distance will be
     * lost.
     *
     * return The closest center point.
     */
    virtual FAS_Vector getNearestCenter(const FAS_Vector& coord,
                                        double* dist = nullptr) const= 0;

    /*
     * Must be overwritten to get the (nearest) middle point to the
     * given coordinate for this entity.
     *
     * param coord Coordinate (typically a mouse coordinate)
     * param dist Pointer to a value which will contain the measured
     * distance between 'coord' and the closest middle point. The passed
     * pointer can also be nullptr in which case the distance will be
     * lost.
     *
     * return The closest middle point.
     */
    virtual FAS_Vector getMiddlePoint(void)const{
        return FAS_Vector(false);
    }
    virtual FAS_Vector getNearestMiddle(const FAS_Vector& coord,
                                        double* dist = nullptr,
                                        int middlePoints = 1
            ) const= 0;

    /*
     * Must be overwritten to get the nearest point with a given
     * distance to the endpoint to the given coordinate for this entity.
     *
     * param distance Distance to endpoint.
     * param coord Coordinate (typically a mouse coordinate)
     * param dist Pointer to a value which will contain the measured
     * distance between 'coord' and the closest point. The passed
     * pointer can also be nullptr in which case the distance will be
     * lost.
     *
     * return The closest point with the given distance to the endpoint.
     */
    virtual FAS_Vector getNearestDist(double distance,
                                      const FAS_Vector& coord,
                                      double* dist = nullptr) const= 0;

    /*
     * Must be overwritten to get the point with a given
     * distance to the start- or endpoint to the given coordinate for this entity.
     *
     * param distance Distance to endpoint.
     * param startp true = measured from Startpoint, false = measured from Endpoint
     *
     * return The point with the given distance to the start- or endpoint.
     */
    virtual FAS_Vector getNearestDist(double /*distance*/,
                                      bool /*startp*/) const{
        return FAS_Vector(false);
    }

    /*
     * Must be overwritten to get the nearest reference point for this entity.
     *
     * param coord Coordinate (typically a mouse coordinate)
     * param dist Pointer to a value which will contain the measured
     * distance between 'coord' and the closest point. The passed
     * pointer can also be nullptr in which case the distance will be
     * lost.
     *
     * return The closest point with the given distance to the endpoint.
     */
    virtual FAS_Vector getNearestRef(const FAS_Vector& coord,
                                     double* dist = nullptr) const;

    /*
     * Gets the nearest reference point of this entity if it is selected.
         * Containers re-implement this method to return the nearest reference
         * point of a selected sub entity.
     *
     * param coord Coordinate (typically a mouse coordinate)
     * param dist Pointer to a value which will contain the measured
     * distance between 'coord' and the closest point. The passed
     * pointer can also be nullptr in which case the distance will be
     * lost.
     *
     * return The closest point with the given distance to the endpoint.
     */
    virtual FAS_Vector getNearestSelectedRef(const FAS_Vector& coord,
                                             double* dist = nullptr) const;

    /*
     * Must be overwritten to get the shortest distance between this
     * entity and a coordinate.
     *
     * param coord Coordinate (typically a mouse coordinate)
     * param entity Pointer which will contain the (sub-)entity which is
     *               closest to the given point or nullptr if the caller is not
     *               interested in this information.
     * param level The resolve level.
     *
     * sa FAS2::ResolveLevel
     *
     * return The measured distance between \p coord and the entity.
     */
    virtual FAS_Vector getNearestOrthTan(const FAS_Vector& /*coord*/,
                                         const FAS_Line& /*normal*/,
                                         bool onEntity = false) const;
    virtual double getDistanceToPoint(const FAS_Vector& coord,
                                      FAS_Entity** entity = nullptr,
                                      FAS2::ResolveLevel level = FAS2::ResolveNone,
                                      double solidDist = FAS_MAXDOUBLE) const;

    virtual bool isPointOnEntity(const FAS_Vector& coord,
                                 double tolerance=20.*FAS_TOLERANCE) const;

    // Implementations must offset the entity by the given direction and distance.
    virtual bool offset(const FAS_Vector& /*coord*/, const double& /*distance*/) {return false;}

    // Implementations must offset the entity by the distance at both directions used to generate tangential circles
    virtual std::vector<FAS_Entity* > offsetTwoSides(const double& /*distance*/) const
    {
        return std::vector<FAS_Entity* >();
    }
    // implementations must revert the direction of an atomic entity
    virtual void revertDirection(){}
    // Implementations must move the entity by the given vector.
    virtual void move(const FAS_Vector& offset) = 0;
    // Implementations must rotate the entity by the given angle around the given center.
    virtual void rotate(const FAS_Vector& center, const double& angle) = 0;
    virtual void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) = 0;

    // Implementations must scale the entity by the given factors.
    virtual void scale(const FAS_Vector& center, const FAS_Vector& factor) = 0;

    // Acts like scale(FAS_Vector) but with equal factors. Equal to scale(center, FAS_Vector(factor, factor)).
    virtual void scale(const FAS_Vector& center, const double& factor)
    {
        scale(center, FAS_Vector(factor, factor));
    }
    virtual void scale(const FAS_Vector& factor)
    {
        scale(FAS_Vector(0.,0.), factor);
    }
    // Implementations must mirror the entity by the given axis.
    virtual void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) = 0;

    virtual void stretch(const FAS_Vector& firstCorner,
                         const FAS_Vector& secondCorner,
                         const FAS_Vector& offset);

    // Implementations must drag the reference point(s) of all (sub-)entities that are very close to ref by offset.
    virtual void moveRef(const FAS_Vector& /*ref*/,
                         const FAS_Vector& /*offset*/)
    {
        return;
    }

    // Implementations must drag the reference point(s) of selected (sub-)entities that are very close to ref by offset.
    virtual void moveSelectedRef(const FAS_Vector& /*ref*/,
                                 const FAS_Vector& /*offset*/)
    {
        return;
    }


    // whether the entity's bounding box intersects with visible portion of graphic view
    virtual bool isVisibleInWindow(FAS_GraphicView* view) const;
    // Implementations must draw the entity on the given device.
    virtual void draw(FAS_Painter* painter, FAS_GraphicView* view,
                      double& patternOffset ) = 0;

    virtual void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);

    double getStyleFactor(FAS_GraphicView* view);

    QString getUserDefVar(const QString& key) const;
    std::vector<QString> getAllKeys() const;
    void setUserDefVar(QString key, QString val);
    void delUserDefVar(QString key);

    friend std::ostream& operator << (std::ostream& os, FAS_Entity& e);

    // Recalculates the borders of this entity.
    virtual void calculateBorders() = 0;
    // whether the entity is on a constructionLayer
    // constructionLayer contains entities of infinite length, constructionLayer doesn't show up in print
    bool isConstruction(bool typeCheck = false) const; // ignore certain entity types for constructionLayer check
    // whether printing is enabled or disabled for the entity's layer
    bool isPrint(void) const;
    virtual LC_Quadratic getQuadratic() const;
    virtual double areaLineIntegral() const;

    /*
     * brief trimmable, whether the entity type can be trimmed
     * return true, for trimmable entity types
     * currently, trimmable types are: FAS_Line, FAS_Circle, FAS_Arc, FAS_Ellipse
     */
    bool trimmable() const;
    virtual bool isArc() const;
    virtual bool isArcCircleLine() const;

protected:
    // Entity's parent entity or nullptr is this entity has no parent.
    FAS_EntityContainer* parent = nullptr;
    // minimum coordinates
    FAS_Vector minV;
    // maximum coordinates
    FAS_Vector maxV;
    // Pointer to layer
    FAS_Layer* layer;
    // Entity id
    unsigned long int id;
    // pen (attributes) for this entity
    FAS_Pen pen;
    // auto updating enabled?
    bool updateEnabled;
    //CAD recognition parent
    FAS_Entity* calParent = nullptr;
    //children nodes
    QList<FAS_Entity*> calChilds;
    //may conneciton vector, will be update during recognition
    QList<QString> relPoints;
    //connected vector
    QList<QString> connectedPoints;
    //path to parent
    QList<FAS_Entity*> calPath;
    //Temp related points, if success end, then remove these points from relpoints.
    QList<QString> tempPoints;
private:
    std::map<QString, QString> varList;
};

#endif
