/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_ARC_H
#define FAS_ARC_H

#include "fas_atomicentity.h"
class LC_Quadratic;

// Holds the data that defines an arc.
struct FAS_ArcData
{
    FAS_ArcData() {}

    FAS_ArcData(const FAS_Vector& center,
                double radius,
                double angle1, double angle2,
                bool reversed);

    void reset();

    bool isValid() const;

    FAS_Vector center;
    double radius;
    double angle1;
    double angle2;
    bool reversed;
    FAS_Vector bkcenter;
    double bkradius;
    double bkangle1;
    double bkangle2;
    bool bkreversed;
};

std::ostream& operator << (std::ostream& os, const FAS_ArcData& ad);

// Class for an arc entity. All angles are in Rad.
class FAS_Arc : public FAS_AtomicEntity
{
public:
    FAS_Arc(){}
    FAS_Arc(FAS_EntityContainer* parent, const FAS_ArcData& d);

    FAS_Entity* clone() const override;

    FAS2::EntityType rtti() const override
    {
        return FAS2::EntityArc;
    }

    bool isEdge() const override {
        return true;
    }

    // data that defines the arc.
    FAS_ArcData getData() const {
        return data;
    }

    FAS_VectorSolutions getRefPoints() const override;

    // Sets new arc parameters.
    void setData(FAS_ArcData d) {
        data = d;
    }

    // The center point (x) of this arc
    FAS_Vector getCenter() const override {
        return data.center;
    }
    // Set new center.
    void setCenter(const FAS_Vector& c) {
        data.center = c;
    }

    // The radius of this arc
    double getRadius() const override {
        return data.radius;
    }
    // Sets new radius.
    void setRadius(double r) {
        data.radius = r;
    }

    // The start angle of this arc
    double getAngle1() const {
        return data.angle1;
    }
    // Sets new start angle.
    void setAngle1(double a1) {
        data.angle1 = a1;
    }
    // The end angle of this arc
    double getAngle2() const {
        return data.angle2;
    }
    // Sets new end angle.
    void setAngle2(double a2) {
        data.angle2 = a2;
    }
    // get angle relative arc center
    double getArcAngle(const FAS_Vector& vp) {
        return (vp - data.center).angle();
    }
    // The angle at which the arc starts at the startpoint.
    double getDirection1() const override;
    // The angle at which the arc starts at the endpoint.
    double getDirection2() const override;

    // true if the arc is reversed (clockwise),
    bool isReversed() const {
        return data.reversed;
    }
    /** sets the reversed status. */
    void setReversed(bool r) {
        data.reversed = r;
    }

    //Start point of the entity.
    FAS_Vector getStartpoint() const override;
    // End point of the entity.
    FAS_Vector getEndpoint() const override;
    std::vector<FAS_Entity* > offsetTwoSides(const double& distance) const override;

    void revertDirection() override;
    void correctAngles();//make sure angleLength() is not more than 2*M_PI
    void moveStartpoint(const FAS_Vector& pos) override;
    void moveEndpoint(const FAS_Vector& pos) override;
    bool offset(const FAS_Vector& position, const double& distance) override;

    void trimStartpoint(const FAS_Vector& pos) override;
    void trimEndpoint(const FAS_Vector& pos) override;

    FAS2::Ending getTrimPoint(const FAS_Vector& coord,
                              const FAS_Vector& trimPoint) override;
    /** choose an intersection to trim to based on mouse point */
    FAS_Vector prepareTrim(const FAS_Vector& mousePoint,
                           const FAS_VectorSolutions& trimSol)override;

    void reverse() override;

    FAS_Vector getMiddlePoint() const override;
    double getAngleLength() const;
    double getLength() const override;
    double getBulge() const;

    bool createFrom3P(const FAS_Vector& p1, const FAS_Vector& p2,
                      const FAS_Vector& p3);
    bool createFrom2PDirectionRadius(const FAS_Vector& startPoint, const FAS_Vector& endPoint,
                                     double direction1, double radius);
    bool createFrom2PDirectionAngle(const FAS_Vector& startPoint, const FAS_Vector& endPoint,
                                    double direction1, double angleLength);
    bool createFrom2PBulge(const FAS_Vector& startPoint, const FAS_Vector& endPoint,
                           double bulge);

    FAS_Vector getNearestEndpoint(const FAS_Vector& coord,
                                  double* dist = nullptr) const override;
    FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                       bool onEntity = true,
                                       double* dist = nullptr,
                                       FAS_Entity** entity=nullptr) const override;
    FAS_Vector getNearestCenter(const FAS_Vector& coord,
                                double* dist = nullptr) const override;
    FAS_Vector getNearestMiddle(const FAS_Vector& coord,
                                double* dist = nullptr,
                                int middlePoints = 1
            ) const override;
    FAS_Vector getNearestDist(double distance,
                              const FAS_Vector& coord,
                              double* dist = nullptr) const override;
    FAS_Vector getNearestDist(double distance,
                              bool startp) const override;
    FAS_Vector getNearestOrthTan(const FAS_Vector& coord,
                                 const FAS_Line& normal,
                                 bool onEntity = false) const override;
    FAS_VectorSolutions getTangentPoint(const FAS_Vector& point) const override;//find the tangential points seeing from given point
    FAS_Vector getTangentDirection(const FAS_Vector& point) const override;
    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;
    void stretch(const FAS_Vector& firstCorner,
                 const FAS_Vector& secondCorner,
                 const FAS_Vector& offset) override;

    /** find the visible part of the arc, and call drawVisible() to draw */
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
    /** directly draw the arc, assuming the whole arc is within visible window */
    void drawVisible(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset);

    friend std::ostream& operator << (std::ostream& os, const FAS_Arc& a);

    virtual void calculateBorders() override;
    /* return the equation of the entity for quadratic,
    return a vector contains:
    m0 x^2 + m1 xy + m2 y^2 + m3 x + m4 y + m5 =0

    for linear:
    m0 x + m1 y + m2 =0
    */
    virtual LC_Quadratic getQuadratic() const override;
    virtual double areaLineIntegral() const override;
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;

protected:
    FAS_ArcData data;
};

#endif
