/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_ellipse.h"

//sort with alphabetical order
#include "fas_circle.h"
#include "fas_graphicview.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_painter.h"
#include "fas_painterqt.h"
#include "fas_information.h"
#include "fas_linetypepattern.h"
#include "lc_quadratic.h"
#include "fas_insert.h"

// Workaround for Qt bug: https://bugreports.qt-project.org/browse/QTBUG-22829
// TODO: the Q_MOC_RUN detection shouldn't be necessary after this Qt bug is resolved
#ifndef Q_MOC_RUN
#include <boost/version.hpp>
#include <boost/math/tools/roots.hpp>
#include <boost/math/special_functions/ellint_2.hpp>
#endif

namespace{
//functor to solve for distance, used by snapDistance
class EllipseDistanceFunctor
{
public:
    EllipseDistanceFunctor(FAS_Ellipse const* ellipse, double const& target) :
        distance{target}
      , e{ellipse}
      , ra{e->getMajorRadius()}
      , k2{1.- e->getRatio()*e->getRatio()}
      , k2ra{k2 * ra}
    {
    }
    void setDistance(const double& target){
        distance=target;
    }
#if BOOST_VERSION > 104500
    boost::math::tuple<double, double, double> operator()(double const& z) const {
#else
    boost::fusion::tuple<double, double, double> operator()(double const& z) const {
#endif
        double const cz=cos(z);
        double const sz=sin(z);
        //delta amplitude
        double const d=sqrt(1-k2*sz*sz);
        // return f(x), f'(x) and f''(x)
#if BOOST_VERSION > 104500
        return boost::math::make_tuple(
            #else
        return boost::fusion::make_tuple(
            #endif
                    e->getEllipseLength(z)-distance,
                    ra*d,
                    k2ra*sz*cz/d
                    );
    }

private:
    double distance;
    FAS_Ellipse const* const e;
    const double ra;
    const double k2;
    const double k2ra;
};

// find end point after trimmed by amount
FAS_Vector getNearestDistHelper(FAS_Ellipse const& e,
                                double trimAmount,
                                FAS_Vector const& coord,
                                double* dist = nullptr)
{
    double const x1 = e.getAngle1();
    double const guess= x1 + M_PI;
    int const digits=std::numeric_limits<double>::digits;
    double const wholeLength = e.getEllipseLength(0, 0); // start/end angle 0 is used for whole ellipses
    double trimmed = e.getLength() + trimAmount;
    // choose the end to trim by the mouse position coord
    bool const trimEnd = coord.squaredTo(e.getStartpoint()) <= coord.squaredTo(e.getEndpoint());
    if (trimEnd)
        trimmed = trimAmount > 0 ? wholeLength - trimAmount : - trimAmount;
    //solve equation of the distance by second order newton_raphson
    EllipseDistanceFunctor X(&e, trimmed);
    using namespace boost::math::tools;
    double const sol =
            halley_iterate<EllipseDistanceFunctor,double>(X,
                                                          guess,
                                                          x1,
                                                          x1 + 2 * M_PI - FAS_TOLERANCE_ANGLE,
                                                          digits);

    FAS_Vector const vp = e.getEllipsePoint(sol);
    if (dist) *dist = vp.distanceTo(coord);
    return vp;
}
}

std::ostream& operator << (std::ostream& os, const FAS_EllipseData& ed)
{
    os << "(" << ed.center <<
          " " << ed.majorP <<
          " " << ed.ratio <<
          " " << ed.angle1 <<
          "," << ed.angle2 <<
          ")";
    return os;
}

FAS_Ellipse::FAS_Ellipse(FAS_EntityContainer* parent,
                         const FAS_EllipseData& d)
    :FAS_AtomicEntity(parent)
    ,data(d)
{
    calculateBorders();
}

FAS_Entity* FAS_Ellipse::clone() const
{
    FAS_Ellipse* e = new FAS_Ellipse(*this);
    e->initId();
    return e;
}

// Calculates the boundary box of this ellipse.
void FAS_Ellipse::calculateBorders()
{
    FAS_Vector startpoint = getStartpoint();
    FAS_Vector endpoint = getEndpoint();

    double minX = std::min(startpoint.x, endpoint.x);
    double minY = std::min(startpoint.y, endpoint.y);
    double maxX = std::max(startpoint.x, endpoint.x);
    double maxY = std::max(startpoint.y, endpoint.y);

    FAS_Vector vp;
    double amin,amax,a;
    vp.set(getMajorP().x,getRatio()*getMajorP().y);
    a = vp.angle();
    amin = FAS_Math::correctAngle(getAngle1()+a); // to the range of 0 to 2*M_PI
    amax = FAS_Math::correctAngle(getAngle2()+a); // to the range of 0 to 2*M_PI
    if( FAS_Math::isAngleBetween(M_PI,amin,amax,isReversed()) )
    {
        minX = data.center.x-vp.magnitude();
    }

    if( FAS_Math::isAngleBetween(2.*M_PI,amin,amax,isReversed()) )
    {
        maxX= data.center.x+vp.magnitude();
    }
    //y range
    vp.set(getMajorP().y, -getRatio()*getMajorP().x);
    a=vp.angle();
    amin=FAS_Math::correctAngle(getAngle1()+a); // to the range of 0 to 2*M_PI
    amax=FAS_Math::correctAngle(getAngle2()+a); // to the range of 0 to 2*M_PI
    if( FAS_Math::isAngleBetween(M_PI,amin,amax,isReversed()) )
    {
        minY= data.center.y-vp.magnitude();
    }
    if( FAS_Math::isAngleBetween(2.*M_PI,amin,amax,isReversed()) )
    {
        maxY= data.center.y+vp.magnitude();
    }
    minV.set(minX, minY);
    maxV.set(maxX, maxY);
}

//return the foci of ellipse
FAS_VectorSolutions FAS_Ellipse::getFoci() const
{
    FAS_Ellipse e=*this;
    if(getRatio()>1.)
        e.switchMajorMinor();
    FAS_Vector vp(e.getMajorP()*sqrt(1.-e.getRatio()*e.getRatio()));
    return FAS_VectorSolutions({getCenter()+vp, getCenter()-vp});
}

FAS_VectorSolutions FAS_Ellipse::getRefPoints() const
{
    FAS_VectorSolutions ret;
    if(isEllipticArc()){
        //no start/end point for whole ellipse
        ret.push_back(getStartpoint());
        ret.push_back(getEndpoint());
    }
    ret.push_back(data.center);
    ret.push_back(getFoci());
    ret.push_back(getMajorPoint());
    ret.push_back(getMinorPoint());
    return ret;
}

FAS_Vector FAS_Ellipse::getNearestEndpoint(const FAS_Vector& coord, double* dist)const
{
    double dist1, dist2;
    FAS_Vector startpoint = getStartpoint();
    FAS_Vector endpoint = getEndpoint();
    dist1 = (startpoint-coord).squared();
    dist2 = (endpoint-coord).squared();
    if (dist2<dist1)
    {
        if (dist)
        {
            *dist = sqrt(dist2);
        }
        return endpoint;
    }
    else
    {
        if (dist)
        {
            *dist = sqrt(dist1);
        }
        return startpoint;
    }
}

// find the tangential points from a given point, i.e., the tangent lines should passthe given point and tangential points
FAS_VectorSolutions FAS_Ellipse::getTangentPoint(const FAS_Vector& point) const
{
    FAS_Vector point2(point);
    point2.move(-getCenter());
    FAS_Vector aV(-getAngle());
    point2.rotate(aV);
    FAS_VectorSolutions sol;
    double a=getMajorRadius();
    if(a<FAS_TOLERANCE || getRatio()<FAS_TOLERANCE)
        return sol;
    FAS_Circle c(nullptr, FAS_CircleData(FAS_Vector(0.,0.),a));
    point2.y /= getRatio();
    sol=c.getTangentPoint(point2);
    sol.scale(FAS_Vector(1.,getRatio()));
    aV.y *= -1.;
    sol.rotate(aV);
    sol.move(getCenter());
    return sol;
}

FAS_Vector FAS_Ellipse::getTangentDirection(const FAS_Vector& point) const
{
    FAS_Vector vp(point-getCenter());
    FAS_Vector aV(-getAngle());
    vp.rotate(aV);
    vp.y /= getRatio();
    double a=getMajorRadius();
    if(a<FAS_TOLERANCE || getRatio()<FAS_TOLERANCE)
        return FAS_Vector(false);
    FAS_Circle c(nullptr, FAS_CircleData(FAS_Vector(0.,0.),a));
    FAS_Vector direction=c.getTangentDirection(vp);
    direction.y *= getRatio();
    aV.y *= -1.;
    direction.rotate(aV);
    return direction;
}

// find total length of the ellipse (arc)
double FAS_Ellipse::getLength() const
{
    FAS_Ellipse e(nullptr, data);
    if(e.getRatio()>1.)  e.switchMajorMinor();
    if(e.isReversed())
    {
        e.setReversed(false);
        std::swap(e.data.angle1,e.data.angle2);
    }
    return e.getEllipseLength(e.data.angle1,e.data.angle2);
}

// return the arc length between ellipse angle x1, x2
double FAS_Ellipse::getEllipseLength(double x1, double x2) const
{
    double a(getMajorRadius()),k(getRatio());
    k= 1-k*k;//elliptic modulus, or eccentricity
    x1=FAS_Math::correctAngle(x1);
    x2=FAS_Math::correctAngle(x2);
    if(x2 < x1+FAS_TOLERANCE_ANGLE) x2 += 2.*M_PI;
    double ret;
    if( x2 >= M_PI)
    {
        // the complete elliptic integral
        ret=  (static_cast< int>((x2+FAS_TOLERANCE_ANGLE)/M_PI) -
               (static_cast<int>((x1+FAS_TOLERANCE_ANGLE)/M_PI)
                ))*2;
        ret*=boost::math::ellint_2<double>(k);
    }
    else
    {
        ret = 0.0;
    }
    x1=fmod(x1,M_PI);
    x2=fmod(x2,M_PI);
    if( fabs(x2-x1)>FAS_TOLERANCE_ANGLE)
    {
        ret += FAS_Math::ellipticIntegral_2(k,x2)-FAS_Math::ellipticIntegral_2(k,x1);
    }
    return a*ret;
}

// arc length from start point (angle1)
double FAS_Ellipse::getEllipseLength(double x2) const
{
    return getEllipseLength(getAngle1(),x2);
}

/*
  * get the point on the ellipse arc and with arc distance from the start point
  * the distance is expected to be within 0 and getLength()
  * using Newton-Raphson from boost
  */

FAS_Vector FAS_Ellipse::getNearestDist(double distance,
                                       const FAS_Vector& coord,
                                       double* dist) const{
    if( ! isEllipticArc() )
    {
        return {};
    }
    FAS_Ellipse e(nullptr,data);
    if(e.getRatio()>1.) e.switchMajorMinor();
    if(e.isReversed())
    {
        std::swap(e.data.angle1,e.data.angle2);
        e.setReversed(false);
    }

    if(e.getMajorRadius() < FAS_TOLERANCE)
        return {}; //elipse too small

    if(getRatio()<FAS_TOLERANCE) {
        //treat the ellipse as a line
        FAS_Line line{e.minV,e.maxV};
        return line.getNearestDist(distance, coord, dist);
    }
    double x1=e.getAngle1();
    double x2=e.getAngle2();
    if(x2 < x1+FAS_TOLERANCE_ANGLE) x2 += 2 * M_PI;
    double const l0=e.getEllipseLength(x1,x2); // the getEllipseLength() function only defined for proper e
    if(distance > l0+FAS_TOLERANCE) return {}; // can not trim more than the current length
    if(distance > l0-FAS_TOLERANCE) return(getNearestEndpoint(coord,dist)); // trim to zero length
    return getNearestDistHelper(e, distance, coord, dist);
}


// switch the major/minor axis naming
bool FAS_Ellipse::switchMajorMinor(void)
{
    if (fabs(data.ratio) < FAS_TOLERANCE)
        return false;
    FAS_Vector vp_start=getStartpoint();
    FAS_Vector vp_end=getEndpoint();
    FAS_Vector vp=getMajorP();
    setMajorP(FAS_Vector(- data.ratio*vp.y, data.ratio*vp.x)); //direction pi/2 relative to old MajorP;
    setRatio(1./data.ratio);
    if( isEllipticArc())
    {
        //only reset start/end points for ellipse arcs, i.e., angle1 angle2 are not both zero
        setAngle1(getEllipseAngle(vp_start));
        setAngle2(getEllipseAngle(vp_end));
    }
    return true;
}

// return Start point of the entity.
FAS_Vector  FAS_Ellipse::getStartpoint() const
{
    if(isEllipticArc()) return getEllipsePoint(data.angle1);
    return FAS_Vector(false);
}

// return End point of the entity.
FAS_Vector  FAS_Ellipse::getEndpoint() const
{
    if(isEllipticArc()) return getEllipsePoint(data.angle2);
    return FAS_Vector(false);
}

// return Ellipse point by ellipse angle
FAS_Vector  FAS_Ellipse::getEllipsePoint(const double& a) const
{
    FAS_Vector p(a);
    double ra=getMajorRadius();
    p.scale(FAS_Vector(ra,ra*getRatio()));
    p.rotate(getAngle());
    p.move(getCenter());
    return p;
}

//find nearest point on ellipse to a given point
FAS_Vector FAS_Ellipse::getNearestPointOnEntity(const FAS_Vector& coord,
                                                bool onEntity, double* dist, FAS_Entity** entity)const
{
    FAS_Vector ret(false);
    /*
    if( ! coord.valid ) {
        if ( dist ) *dist=FAS_MAXDOUBLE;
        return ret;

    }

    if (entity) {
        *entity = const_cast<FAS_Ellipse*>(this);
    }
    ret=coord;
    ret.move(-getCenter());
    ret.rotate(-getAngle());
    double x=ret.x,y=ret.y;
    double a=getMajorRadius();
    double b=a*getRatio();
    //std::cout<<"(a= "<<a<<" b= "<<b<<" x= "<<x<<" y= "<<y<<" )\n";
    //std::cout<<"finding minimum for ("<<x<<"-"<<a<<"*cos(t))^2+("<<y<<"-"<<b<<"*sin(t))^2\n";
    double twoa2b2=2*(a*a-b*b);
    double twoax=2*a*x;
    double twoby=2*b*y;
    double a0=twoa2b2*twoa2b2;
    std::vector<double> ce(4,0.);
    std::vector<double> roots(0,0.);

    //need to handle a=b
    if(a0 > FAS_TOLERANCE2 ) { // a != b , ellipse
        ce[0]=-2.*twoax/twoa2b2;
        ce[1]= (twoax*twoax+twoby*twoby)/a0-1.;
        ce[2]= - ce[0];
        ce[3]= -twoax*twoax/a0;
        //std::cout<<"1::find cosine, variable c, solve(c^4 +("<<ce[0]<<")*c^3+("<<ce[1]<<")*c^2+("<<ce[2]<<")*c+("<<ce[3]<<")=0,c)\n";
        roots=FAS_Math::quarticSolver(ce);
    } else {//a=b, quadratic equation for circle
        a0=twoby/twoax;
        roots.push_back(sqrt(1./(1.+a0*a0)));
        roots.push_back(-roots[0]);
    }
    if(roots.size()==0) {
        //this should not happen
        std::cout<<"(a= "<<a<<" b= "<<b<<" x= "<<x<<" y= "<<y<<" )\n";
        std::cout<<"finding minimum for ("<<x<<"-"<<a<<"*cos(t))^2+("<<y<<"-"<<b<<"*sin(t))^2\n";
        std::cout<<"2::find cosine, variable c, solve(c^4 +("<<ce[0]<<")*c^3+("<<ce[1]<<")*c^2+("<<ce[2]<<")*c+("<<ce[3]<<")=0,c)\n";
        std::cout<<ce[0]<<' '<<ce[1]<<' '<<ce[2]<<' '<<ce[3]<<std::endl;
        std::cerr<<"FAS_Math::FAS_Ellipse::getNearestPointOnEntity() finds no root from quartic, this should not happen\n";
        return FAS_Vector(coord); // better not to return invalid: return FAS_Vector(false);
    }

//    FAS_Vector vp2(false);
    double d,dDistance(FAS_MAXDOUBLE*FAS_MAXDOUBLE);
    //double ea;
    for(size_t i=0; i<roots.size(); i++) {
        //I don't understand the reason yet, but I can do without checking whether sine/cosine are valid
        //if ( fabs(roots[i])>1.) continue;
        double const s=twoby*roots[i]/(twoax-twoa2b2*roots[i]); //sine
        //if (fabs(s) > 1. ) continue;
        double const d2=twoa2b2+(twoax-2.*roots[i]*twoa2b2)*roots[i]+twoby*s;
        if (d2<0) continue; // fartherest
        FAS_Vector vp3;
        vp3.set(a*roots[i],b*s);
        d=(vp3-ret).squared();
//        std::cout<<i<<" Checking: cos= "<<roots[i]<<" sin= "<<s<<" angle= "<<atan2(roots[i],s)<<" ds2= "<<d<<" d="<<d2<<std::endl;
        if( ret.valid && d>dDistance) continue;
        ret=vp3;
        dDistance=d;
//			ea=atan2(roots[i],s);
    }
    if( ! ret.valid ) {
        //this should not happen
//        std::cout<<ce[0]<<' '<<ce[1]<<' '<<ce[2]<<' '<<ce[3]<<std::endl;
//        std::cout<<"(x,y)=( "<<x<<" , "<<y<<" ) a= "<<a<<" b= "<<b<<" sine= "<<s<<" d2= "<<d2<<" dist= "<<d<<std::endl;
//        std::cout<<"FAS_Ellipse::getNearestPointOnEntity() finds no minimum, this should not happen\n";
    }
    if (dist) {
        *dist = sqrt(dDistance);
    }
    ret.rotate(getAngle());
    ret.move(getCenter());
//    ret=vp2;
    if (onEntity) {
        if (!FAS_Math::isAngleBetween(getEllipseAngle(ret), getAngle1(), getAngle2(), isReversed())) { // not on entity, use the nearest endpoint
               //std::cout<<"not on ellipse, ( "<<getAngle1()<<" "<<getEllipseAngle(ret)<<" "<<getAngle2()<<" ) reversed= "<<isReversed()<<"\n";
            ret=getNearestEndpoint(coord,dist);
        }
    }

//    if(! ret.valid) {
//        std::cout<<"FAS_Ellipse::getNearestOnEntity() returns invalid by mistake. This should not happen!"<<std::endl;
//    }
*/
    return ret;
}

bool FAS_Ellipse::isPointOnEntity(const FAS_Vector& coord,
                                  double tolerance) const {
    double t=fabs(tolerance);
    double a=getMajorRadius();
    double b=a*getRatio();
    FAS_Vector vp((coord - getCenter()).rotate(-getAngle()));
    if ( a<FAS_TOLERANCE )
    {
        if(fabs(vp.x)<FAS_TOLERANCE && fabs(vp.y) < b) return true;
        return false;
    }
    if ( b<FAS_TOLERANCE )
    {
        if (fabs(vp.y)<FAS_TOLERANCE && fabs(vp.x) < a) return true;
        return false;
    }
    vp.scale(FAS_Vector(1./a,1./b));

    if (fabs(vp.squared()-1.) > t) return false;
    return FAS_Math::isAngleBetween(vp.angle(),getAngle1(),getAngle2(),isReversed());
}

FAS_Vector FAS_Ellipse::getNearestCenter(const FAS_Vector& coord,
                                         double* dist) const{
    FAS_Vector   vCenter = data.center;
    double      distCenter = coord.distanceTo(data.center);

    FAS_VectorSolutions  vsFoci = getFoci();
    if( 2 == vsFoci.getNumber()) {
        FAS_Vector const& vFocus1 = vsFoci.get(0);
        FAS_Vector const& vFocus2 = vsFoci.get(1);

        double distFocus1 = coord.distanceTo(vFocus1);
        double distFocus2 = coord.distanceTo(vFocus2);

        if( distFocus1 < distCenter) {
            vCenter = vFocus1;
            distCenter = distFocus1;
        }
        else if( distFocus2 < distCenter) {
            vCenter = vFocus2;
            distCenter = distFocus2;
        }
    }

    if (dist) {
        *dist = distCenter;
    }
    return vCenter;
}

// create Ellipse with axes in x-/y- directions from 4 points
bool	FAS_Ellipse::createFrom4P(const FAS_VectorSolutions& sol)
{
    if (sol.getNumber() != 4 ) return (false); //only do 4 points
    std::vector<std::vector<double> > mt;
    std::vector<double> dn;
    int mSize(4);
    mt.resize(mSize);
    for(int i=0;i<mSize;i++) {//form the linear equation, c0 x^2 + c1 x + c2 y^2 + c3 y = 1
        mt[i].resize(mSize+1);
        mt[i][0]=sol.get(i).x * sol.get(i).x;
        mt[i][1]=sol.get(i).x ;
        mt[i][2]=sol.get(i).y * sol.get(i).y;
        mt[i][3]=sol.get(i).y ;
        mt[i][4]=1.;
    }
    if ( ! FAS_Math::linearSolver(mt,dn)) return false;
    double d(1.+0.25*(dn[1]*dn[1]/dn[0]+dn[3]*dn[3]/dn[2]));
    if(fabs(dn[0])<FAS_TOLERANCE15
            ||fabs(dn[2])<FAS_TOLERANCE15
            ||d/dn[0]<FAS_TOLERANCE15
            ||d/dn[2]<FAS_TOLERANCE15
            ) {
        //ellipse not defined
        return false;
    }
    data.center.set(-0.5*dn[1]/dn[0],-0.5*dn[3]/dn[2]); // center
    d=sqrt(d/dn[0]);
    data.majorP.set(d,0.);
    data.ratio=sqrt(dn[0]/dn[2]);
    data.angle1=0.;
    data.angle2=0.;

    return true;
}

// create Ellipse with center and 3 points
bool	FAS_Ellipse::createFromCenter3Points(const FAS_VectorSolutions& sol) {
    if(sol.getNumber()<3) return false; //need one center and 3 points on ellipse
    std::vector<std::vector<double> > mt;
    int mSize(sol.getNumber() -1);
    if( (sol.get(mSize) - sol.get(mSize-1)).squared() < FAS_TOLERANCE15 ) {
        //remove the last point
        mSize--;
    }

    mt.resize(mSize);
    std::vector<double> dn(mSize);
    switch(mSize){
    case 2:
        for(int i=0;i<mSize;i++){//form the linear equation
            mt[i].resize(mSize+1);
            FAS_Vector vp(sol.get(i+1)-sol.get(0)); //the first vector is center
            mt[i][0]=vp.x*vp.x;
            mt[i][1]=vp.y*vp.y;
            mt[i][2]=1.;
        }
        if ( ! FAS_Math::linearSolver(mt,dn) ) return false;
        if( dn[0]<FAS_TOLERANCE15 || dn[1]<FAS_TOLERANCE15) return false;
        setMajorP(FAS_Vector(1./sqrt(dn[0]),0.));
        setRatio(sqrt(dn[0]/dn[1]));
        setAngle1(0.);
        setAngle2(0.);
        setCenter(sol.get(0));
        return true;

    case 3:
        for(int i=0;i<mSize;i++){//form the linear equation
            mt[i].resize(mSize+1);
            FAS_Vector vp(sol.get(i+1)-sol.get(0)); //the first vector is center
            mt[i][0]=vp.x*vp.x;
            mt[i][1]=vp.x*vp.y;
            mt[i][2]=vp.y*vp.y;
            mt[i][3]=1.;
        }
        if ( ! FAS_Math::linearSolver(mt,dn) ) return false;
        setCenter(sol.get(0));
        return createFromQuadratic(dn);
    default:
        return false;
    }
    return false; // only for compiler warning
}

/** \brief create from quadratic form:
  * dn[0] x^2 + dn[1] xy + dn[2] y^2 =1
  * keep the ellipse center before calling this function
  */
bool FAS_Ellipse::createFromQuadratic(const std::vector<double>& dn){
    if(dn.size()!=3)
        return false;
    double a=dn[0];
    const double c=dn[1];
    double b=dn[2];

    //Eigen system
    const double d = a - b;
    const double s=hypot(d,c);
    // eigenvalues are required to be positive for ellipses
    if(s >= a+b ) return false;
    if(a>=b) {
        setMajorP(FAS_Vector(atan2(d+s, -c))/sqrt(0.5*(a+b-s)));
    }else{
        setMajorP(FAS_Vector(atan2(-c, s-d))/sqrt(0.5*(a+b-s)));
    }
    setRatio(sqrt((a+b-s)/(a+b+s)));

    // start/end angle at 0. means a whole ellipse, instead of an elliptic arc
    setAngle1(0.);
    setAngle2(0.);
    return true;
}

bool FAS_Ellipse::createFromQuadratic(const LC_Quadratic& q){
    if (!q.isQuadratic()) return false;
    auto  const& mQ=q.getQuad();
    double const& a=mQ(0,0);
    double const& c=2.*mQ(0,1);
    double const& b=mQ(1,1);
    auto  const& mL=q.getLinear();
    double const& d=mL(0);
    double const& e=mL(1);
    double determinant=c*c-4.*a*b;
    if(determinant>= -DBL_EPSILON) return false;
    const FAS_Vector eCenter=FAS_Vector(2.*b*d - e*c, 2.*a*e - d*c)/determinant;
    //generate centered quadratic
    LC_Quadratic qCentered=q;
    qCentered.move(-eCenter);
    if(qCentered.constTerm()>= -DBL_EPSILON) return false;
    const auto& mq2=qCentered.getQuad();
    const double factor=-1./qCentered.constTerm();
    //quadratic terms
    if(!createFromQuadratic({mq2(0,0)*factor, 2.*mq2(0,1)*factor, mq2(1,1)*factor})) return false;

    //move back to center
    move(eCenter);
    return true;
}

//create Ellipse inscribed in a quadrilateral
bool	FAS_Ellipse::createInscribeQuadrilateral(const std::vector<FAS_Line*>& lines)
{
    if(lines.size() != 4) return false; //only do 4 lines
    std::vector<std::unique_ptr<FAS_Line> > quad(4);
    { //form quadrilateral from intersections
        FAS_EntityContainer c0(nullptr, false);
        for(FAS_Line*const p: lines){//copy the line pointers
            c0.addEntity(p);
        }
        FAS_VectorSolutions const& s0=FAS_Information::createQuadrilateral(c0);
        if(s0.size()!=4) return false;
        for(size_t i=0; i<4; ++i){
            quad[i].reset(new FAS_Line{s0[i], s0[(i+1)%4]});
        }
    }

    //center of original square projected, intersection of diagonal
    FAS_Vector centerProjection;
    {
        std::vector<FAS_Line> diagonal;
        diagonal.emplace_back(quad[0]->getStartpoint(), quad[1]->getEndpoint());
        diagonal.emplace_back(quad[1]->getStartpoint(), quad[2]->getEndpoint());
        FAS_VectorSolutions const& sol=FAS_Information::getIntersectionLineLine( & diagonal[0],& diagonal[1]);
        if(sol.getNumber()==0) {//this should not happen
            return false;
        }
        centerProjection=sol.get(0);
    }

    std::vector<FAS_Vector> tangent;//holds the tangential points on edges, in the order of edges: 1 3 2 0
    int parallel=0;
    int parallel_index=0;
    for(int i=0;i<=1;++i) {
        FAS_VectorSolutions const& sol1=FAS_Information::getIntersectionLineLine(quad[i].get(), quad[(i+2)%4].get());
        FAS_Vector direction;
        if(sol1.getNumber()==0) {
            direction=quad[i]->getEndpoint()-quad[i]->getStartpoint();
            ++parallel;
            parallel_index=i;
        }else{
            direction=sol1.get(0)-centerProjection;
        }
        //                std::cout<<"Direction: "<<direction<<std::endl;
        FAS_Line l(centerProjection, centerProjection+direction);
        for(int k=1;k<=3;k+=2){
            FAS_VectorSolutions sol2=FAS_Information::getIntersectionLineLine(&l, quad[(i+k)%4].get());
            if(sol2.size()) tangent.push_back(sol2.get(0));
        }
    }

    if(tangent.size()<3) return false;

    //find ellipse center by projection
    FAS_Vector ellipseCenter;
    {
        FAS_Line cl0(quad[1]->getEndpoint(),(tangent[0]+tangent[2])*0.5);
        FAS_Line cl1(quad[2]->getEndpoint(),(tangent[1]+tangent[2])*0.5);
        FAS_VectorSolutions const& sol=FAS_Information::getIntersection(&cl0, &cl1,false);
        if(sol.getNumber()==0){
            //this should not happen
            return false;
        }
        ellipseCenter=sol.get(0);
    }
    //	qDebug()<<"parallel="<<parallel;
    if(parallel==1){
        //trapezoid
        FAS_Line* l0=quad[parallel_index].get();
        FAS_Line* l1=quad[(parallel_index+2)%4].get();
        FAS_Vector centerPoint=(l0->getMiddlePoint()+l1->getMiddlePoint())*0.5;
        //not symmetric, no inscribed ellipse
        if( fabs(centerPoint.distanceTo(l0->getStartpoint()) - centerPoint.distanceTo(l0->getEndpoint()))>FAS_TOLERANCE)
            return false;
        //symmetric
        double d=l0->getDistanceToPoint(centerPoint);
        double l=((l0->getLength()+l1->getLength()))*0.25;
        double k= 4.*d/fabs(l0->getLength()-l1->getLength());
        double theta=d/(l*k);
        if(theta>=1. || d<FAS_TOLERANCE) {
            return false;
        }
        theta=asin(theta);

        //major axis
        double a=d/(k*tan(theta));
        setCenter(FAS_Vector(0., 0.));
        setMajorP(FAS_Vector(a, 0.));
        setRatio(d/a);
        rotate(l0->getAngle1());
        setCenter(centerPoint);
        return true;

    }
    //    double ratio;
    //        std::cout<<"dn="<<dn[0]<<' '<<dn[1]<<' '<<dn[2]<<std::endl;
    std::vector<double> dn(3);
    FAS_Vector angleVector(false);

    for(size_t i=0;i<tangent.size();i++) {
        tangent[i] -= ellipseCenter;//relative to ellipse center
    }
    std::vector<std::vector<double> > mt;
    mt.clear();
    const double symTolerance=20.*FAS_TOLERANCE;
    for(const FAS_Vector& vp: tangent){
        std::vector<double> mtRow;
        mtRow.push_back(vp.x*vp.x);
        mtRow.push_back(vp.x*vp.y);
        mtRow.push_back(vp.y*vp.y);
        const double l= hypot(hypot(mtRow[0], mtRow[1]), mtRow[2]);
        bool addRow(true);
        for(const auto& v: mt){
            FAS_Vector const dv{v[0] - mtRow[0], v[1] - mtRow[1], v[2] - mtRow[2]};
            if( dv.magnitude() < symTolerance*l){
                //symmetric
                addRow=false;
                break;
            }
        }
        if(addRow) {
            mtRow.push_back(1.);
            mt.push_back(mtRow);
        }
    }
    switch(mt.size()){
    case 2:{// the quadrilateral is a parallelogram
        FAS_Vector majorP(tangent[0]);
        double dx(majorP.magnitude());
        if(dx<FAS_TOLERANCE2) return false; //refuse to return zero size ellipse
        angleVector.set(majorP.x/dx,-majorP.y/dx);
        for(size_t i=0;i<tangent.size();i++)tangent[i].rotate(angleVector);

        FAS_Vector minorP(tangent[2]);
        double dy2(minorP.squared());
        if(fabs(minorP.y)<FAS_TOLERANCE || dy2<FAS_TOLERANCE2) return false; //refuse to return zero size ellipse
        double ia2=1./(dx*dx);
        double ib2=1./(minorP.y*minorP.y);
        dn[0]=ia2;
        dn[1]=-2.*ia2*minorP.x/minorP.y;
        dn[2]=ib2*ia2*minorP.x*minorP.x+ib2;
    }
        break;
    case 4:
        mt.pop_back(); //only 3 points needed to form the qudratic form
        if ( ! FAS_Math::linearSolver(mt,dn) ) return false;
        break;
    default:
        return false; //invalid quadrilateral
    }

    if(! createFromQuadratic(dn)) return false;
    setCenter(ellipseCenter);

    if(angleVector.valid) {//need to rotate back, for the parallelogram case
        angleVector.y *= -1.;
        rotate(ellipseCenter,angleVector);
    }
    return true;

}

FAS_Vector FAS_Ellipse::getMiddlePoint()const{
    return getNearestMiddle(getCenter());
}
// get Nearest equidistant point
FAS_Vector FAS_Ellipse::getNearestMiddle(const FAS_Vector& coord,
                                         double* dist,
                                         int middlePoints
                                         ) const{
    if ( ! isEllipticArc() ) {
        //no middle point for whole ellipse, angle1=angle2=0
        if (dist) {
            *dist = FAS_MAXDOUBLE;
        }
        return FAS_Vector(false);
    }
    double ra(getMajorRadius());
    double rb(getRatio()*ra);
    if ( ra < FAS_TOLERANCE || rb < FAS_TOLERANCE ) {
        //zero radius, return the center
        FAS_Vector vp(getCenter());
        if (dist) {
            *dist = vp.distanceTo(coord);
        }
        return vp;
    }
    double amin=getCenter().angleTo(getStartpoint());
    double amax=getCenter().angleTo(getEndpoint());
    if(isReversed()) {
        std::swap(amin,amax);
    }
    double da=fmod(amax-amin+2.*M_PI, 2.*M_PI);
    if ( da < FAS_TOLERANCE ) {
        da = 2.*M_PI; //whole ellipse
    }
    FAS_Vector vp(getNearestPointOnEntity(coord,true,dist));
    double a=getCenter().angleTo(vp);
    int counts(middlePoints + 1);
    int i( static_cast<int>(fmod(a-amin+2.*M_PI,2.*M_PI)/da*counts+0.5));
    if(!i) i++; // remove end points
    if(i==counts) i--;
    a=amin + da*(double(i)/double(counts))-getAngle();
    vp.set(a);
    FAS_Vector vp2(vp);
    vp2.scale( FAS_Vector(1./ra,1./rb));
    vp.scale(1./vp2.magnitude());
    vp.rotate(getAngle());
    vp.move(getCenter());

    if (dist) {
        *dist = vp.distanceTo(coord);
    }
    return vp;
}

// get the tangential point of a tangential line orthogonal to a given line
FAS_Vector FAS_Ellipse::getNearestOrthTan(const FAS_Vector& coord,
                                          const FAS_Line& normal,
                                          bool onEntity ) const
{
    if ( !coord.valid ) {
        return FAS_Vector(false);
    }
    FAS_Vector direction=normal.getEndpoint() - normal.getStartpoint();
    if (direction.squared()< FAS_TOLERANCE15) {
        //undefined direction
        return FAS_Vector(false);
    }
    //scale to ellipse angle
    FAS_Vector aV(-getAngle());
    direction.rotate(aV);
    double angle=direction.scale(FAS_Vector(1.,getRatio())).angle();
    double ra(getMajorRadius());
    direction.set(ra*cos(angle),getRatio()*ra*sin(angle));//relative to center
    std::vector<FAS_Vector> sol;
    for(int i=0;i<2;i++){
        if(!onEntity ||
                FAS_Math::isAngleBetween(angle,getAngle1(),getAngle2(),isReversed())) {
            if(i){
                sol.push_back(- direction);
            }else{
                sol.push_back(direction);
            }
        }
        angle=FAS_Math::correctAngle(angle+M_PI);
    }
    if(sol.size()<1) return FAS_Vector(false);
    aV.y*=-1.;
    for(auto& v: sol){
        v.rotate(aV);
    }
    FAS_Vector vp;
    switch(sol.size()) {
    case 0:
        return FAS_Vector(false);
    case 2:
        if( FAS_Vector::dotP(sol[1],coord-getCenter())>0.) {
            vp=sol[1];
            break;
        }
    default:
        vp=sol[0];
    }
    return getCenter() + vp;
}


void FAS_Ellipse::move(const FAS_Vector& offset) {
    data.center.move(offset);
    moveBorders(offset);
}

void FAS_Ellipse::rotate(const FAS_Vector& center, const double& angle) {
    FAS_Vector angleVector(angle);
    data.center.rotate(center, angleVector);
    data.majorP.rotate(angleVector);
    calculateBorders();
}
void FAS_Ellipse::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    data.center.rotate(center, angleVector);
    data.majorP.rotate(angleVector);
    calculateBorders();
}

void FAS_Ellipse::rotate( const double& angle) {//rotate around origin
    FAS_Vector aV(angle);
    data.center.rotate(aV);
    data.majorP.rotate(aV);
    calculateBorders();
}
void FAS_Ellipse::rotate( const FAS_Vector& angleVector) {//rotate around origin
    data.center.rotate(angleVector);
    data.majorP.rotate(angleVector);
    calculateBorders();
}

// make sure angleLength() is not more than 2*M_PI
void FAS_Ellipse::correctAngles() {
    double *pa1= & data.angle1;
    double *pa2= & data.angle2;
    if (isReversed())
        std::swap(pa1,pa2);
    *pa2 = *pa1 + fmod(*pa2 - *pa1, 2.*M_PI);
    if ( fabs(data.angle1 - data.angle2) < FAS_TOLERANCE_ANGLE )
        *pa2 += 2.*M_PI;
}

void FAS_Ellipse::moveStartpoint(const FAS_Vector& pos) {
    data.angle1 = getEllipseAngle(pos);
    correctAngles(); // make sure angleLength is no more than 2*M_PI
    calculateBorders();
}

void FAS_Ellipse::moveEndpoint(const FAS_Vector& pos) {
    data.angle2 = getEllipseAngle(pos);
    correctAngles(); // make sure angleLength is no more than 2*M_PI
    calculateBorders();
}

FAS2::Ending FAS_Ellipse::getTrimPoint(const FAS_Vector& trimCoord,
                                       const FAS_Vector& /*trimPoint*/)
{
    double angM = getEllipseAngle(trimCoord);
    if (FAS_Math::getAngleDifference(angM, data.angle1,isReversed()) > FAS_Math::getAngleDifference(data.angle2,angM,isReversed())) {
        return FAS2::EndingStart;
    } else {
        return FAS2::EndingEnd;
    }
}

FAS_Vector FAS_Ellipse::prepareTrim(const FAS_Vector& trimCoord,
                                    const FAS_VectorSolutions& trimSol)
{
    //special trimming for ellipse arc
    if( ! trimSol.hasValid() ) return (FAS_Vector(false));
    if( trimSol.getNumber() == 1 ) return (trimSol.get(0));
    double am=getEllipseAngle(trimCoord);
    std::vector<double> ias;
    double ia(0.),ia2(0.);
    FAS_Vector is,is2;
    for(size_t ii=0; ii<trimSol.getNumber(); ++ii) { //find closest according ellipse angle
        ias.push_back(getEllipseAngle(trimSol.get(ii)));
        if( !ii ||  fabs( remainder( ias[ii] - am, 2*M_PI)) < fabs( remainder( ia -am, 2*M_PI)) ) {
            ia = ias[ii];
            is = trimSol.get(ii);
        }
    }
    std::sort(ias.begin(),ias.end());
    for(size_t ii=0; ii<trimSol.getNumber(); ++ii) { //find segment to enclude trimCoord
        if ( ! FAS_Math::isSameDirection(ia,ias[ii],FAS_TOLERANCE)) continue;
        if( FAS_Math::isAngleBetween(am,ias[(ii+trimSol.getNumber()-1)% trimSol.getNumber()],ia,false))  {
            ia2=ias[(ii+trimSol.getNumber()-1)% trimSol.getNumber()];
        } else {
            ia2=ias[(ii+1)% trimSol.getNumber()];
        }
        break;
    }
    for(const FAS_Vector& vp: trimSol) { //find segment to enclude trimCoord
        if ( ! FAS_Math::isSameDirection(ia2,getEllipseAngle(vp),FAS_TOLERANCE)) continue;
        is2=vp;
        break;
    }
    if(FAS_Math::isSameDirection(getAngle1(),getAngle2(),FAS_TOLERANCE_ANGLE)
            ||  FAS_Math::isSameDirection(ia2,ia,FAS_TOLERANCE) ) {
        //whole ellipse
        if( !FAS_Math::isAngleBetween(am,ia,ia2,isReversed())) {
            std::swap(ia,ia2);
            std::swap(is,is2);
        }
        setAngle1(ia);
        setAngle2(ia2);
        double da1=fabs(remainder(getAngle1()-am,2*M_PI));
        double da2=fabs(remainder(getAngle2()-am,2*M_PI));
        if(da2<da1) {
            std::swap(is,is2);
        }

    } else {
        double dia=fabs(remainder(ia-am,2*M_PI));
        double dia2=fabs(remainder(ia2-am,2*M_PI));
        double ai_min=std::min(dia,dia2);
        double da1=fabs(remainder(getAngle1()-am,2*M_PI));
        double da2=fabs(remainder(getAngle2()-am,2*M_PI));
        double da_min=std::min(da1,da2);
        if( da_min < ai_min ) {
            //trimming one end of arc
            bool irev= FAS_Math::isAngleBetween(am,ia2,ia, isReversed()) ;
            if ( FAS_Math::isAngleBetween(ia,getAngle1(),getAngle2(), isReversed()) &&
                 FAS_Math::isAngleBetween(ia2,getAngle1(),getAngle2(), isReversed()) )
            {
                if(irev) {
                    setAngle2(ia);
                    setAngle1(ia2);
                } else {
                    setAngle1(ia);
                    setAngle2(ia2);
                }
                da1=fabs(remainder(getAngle1()-am,2*M_PI));
                da2=fabs(remainder(getAngle2()-am,2*M_PI));
            }
            if( ((da1 < da2) && (FAS_Math::isAngleBetween(ia2,ia,getAngle1(),isReversed()))) ||
                    ((da1 > da2) && (FAS_Math::isAngleBetween(ia2,getAngle2(),ia,isReversed())))
                    ) {
                std::swap(is,is2);
            }
        } else {
            //choose intersection as new end
            if( dia > dia2) {
                std::swap(is,is2);
                std::swap(ia,ia2);
            }
            if(FAS_Math::isAngleBetween(ia,getAngle1(),getAngle2(),isReversed())) {
                if(FAS_Math::isAngleBetween(am,getAngle1(),ia,isReversed())) {
                    setAngle2(ia);
                } else {
                    setAngle1(ia);
                }
            }
        }
    }
    return is;
}

double FAS_Ellipse::getEllipseAngle(const FAS_Vector& pos) const {
    FAS_Vector m = pos-data.center;
    m.rotate(-data.majorP.angle());
    m.x *= data.ratio;
    return m.angle();
}

const FAS_EllipseData& FAS_Ellipse::getData() const
{
    return data;
}

void FAS_Ellipse::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    FAS_Vector vpStart;
    FAS_Vector vpEnd;
    if(isEllipticArc()){
        //only handle start/end points for ellipse arc
        vpStart=getStartpoint().scale(center,factor);
        vpEnd=getEndpoint().scale(center,factor);
    }
    data.center.scale(center, factor);
    FAS_Vector vp1(getMajorP());
    double a(vp1.magnitude());
    if(a<FAS_TOLERANCE) return; //ellipse too small
    vp1 *= 1./a;
    double ct=vp1.x;
    double ct2 = ct*ct; // cos^2 angle
    double st=vp1.y;
    double st2=1.0 - ct2; // sin^2 angle
    double kx2= factor.x * factor.x;
    double ky2= factor.y * factor.y;
    double b=getRatio()*a;
    double cA=0.5*a*a*(kx2*ct2+ky2*st2);
    double cB=0.5*b*b*(kx2*st2+ky2*ct2);
    double cC=a*b*ct*st*(ky2-kx2);
    if (factor.x < 0)
        setReversed(!isReversed());
    if (factor.y < 0)
        setReversed(!isReversed());
    FAS_Vector vp(cA-cB,cC);
    vp1.set(a,b);
    vp1.scale(FAS_Vector(0.5*vp.angle()));
    vp1.rotate(FAS_Vector(ct,st));
    vp1.scale(factor);
    setMajorP(vp1);
    a=cA+cB;
    b=vp.magnitude();
    setRatio( sqrt((a - b)/(a + b) ));
    if( isEllipticArc() ) {
        setAngle1(getEllipseAngle(vpStart));
        setAngle2(getEllipseAngle(vpEnd));
    }
    correctAngles();//avoid extra 2.*M_PI in angles
    scaleBorders(center,factor);
}


// return false, if both angle1/angle2 are zero
bool FAS_Ellipse::isEllipticArc() const
{
    return std::isnormal(getAngle1()) || std::isnormal(getAngle2());
}
// mirror by the axis of the line axisPoint1 and axisPoint2
void FAS_Ellipse::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    FAS_Vector center=getCenter();
    FAS_Vector majorp = center + getMajorP();
    FAS_Vector startpoint,endpoint;
    if( isEllipticArc() )  {
        startpoint = getStartpoint();
        endpoint = getEndpoint();
    }

    center.mirror(axisPoint1, axisPoint2);
    majorp.mirror(axisPoint1, axisPoint2);

    setCenter(center);
    setReversed(!isReversed());
    setMajorP(majorp - center);
    if( isEllipticArc() )  {
        //only reset start/end points for ellipse arcs, i.e., angle1 angle2 are not both zero
        startpoint.mirror(axisPoint1, axisPoint2);
        endpoint.mirror(axisPoint1, axisPoint2);
        setAngle1( getEllipseAngle(startpoint));
        setAngle2( getEllipseAngle(endpoint));
    }
    correctAngles();//avoid extra 2.*M_PI in angles
    calculateBorders();
}

//getDirection1 for start point
double FAS_Ellipse::getDirection1() const {
    FAS_Vector vp;
    if (isReversed()){
        vp.set(sin(getAngle1()), -getRatio()*cos(getAngle1()));
    } else {
        vp.set(-sin(getAngle1()), getRatio()*cos(getAngle1()));
    }
    return vp.angle()+getAngle();
}

//getDirection2 for end point
double FAS_Ellipse::getDirection2() const {
    FAS_Vector vp;
    if (isReversed()){
        vp.set(-sin(getAngle2()), getRatio()*cos(getAngle2()));
    } else {
        vp.set(sin(getAngle2()), -getRatio()*cos(getAngle2()));
    }
    return vp.angle()+getAngle();
}

void FAS_Ellipse::moveRef(const FAS_Vector& ref, const FAS_Vector& offset) {
    if(isEllipticArc()){
        FAS_Vector startpoint = getStartpoint();
        FAS_Vector endpoint = getEndpoint();

        if ((ref-startpoint).squared()<FAS_TOLERANCE_ANGLE) {
            moveStartpoint(startpoint+offset);
            correctAngles();//avoid extra 2.*M_PI in angles
            return;
        }
        if ((ref-endpoint).squared()<FAS_TOLERANCE_ANGLE) {
            moveEndpoint(endpoint+offset);
            correctAngles();//avoid extra 2.*M_PI in angles
            return;
        }
    }
    if ((ref-getCenter()).squared()<FAS_TOLERANCE_ANGLE) {
        //move center
        setCenter(getCenter()+offset);
        return;
    }

    if(data.ratio>1.) switchMajorMinor();
    auto foci=getFoci();
    for(size_t i=0; i< 2 ; i++){
        if ((ref-foci.at(i)).squared()<FAS_TOLERANCE_ANGLE) {
            auto focusNew=foci.at(i) + offset;
            //move focus
            auto center = getCenter() + offset*0.5;
            FAS_Vector majorP;
            if(getMajorP().dotP( foci.at(i) - getCenter()) >= 0.){
                majorP = focusNew - center;
            }else{
                majorP = center - focusNew;
            }
            double d=getMajorP().magnitude();
            double c=0.5*focusNew.distanceTo(foci.at(1-i));
            double k=majorP.magnitude();
            if(k<FAS_TOLERANCE2 || d < FAS_TOLERANCE ||
                    c >= d - FAS_TOLERANCE) return;
            majorP *= d/k;
            setCenter(center);
            setMajorP(majorP);
            setRatio(sqrt(d*d-c*c)/d);
            correctAngles();//avoid extra 2.*M_PI in angles
            if(data.ratio>1.) switchMajorMinor();
            return;
        }
    }

    //move major/minor points
    if ((ref-getMajorPoint()).squared()<FAS_TOLERANCE_ANGLE) {
        FAS_Vector majorP=getMajorP()+offset;
        double r=majorP.magnitude();
        if(r<FAS_TOLERANCE) return;
        double ratio = getRatio()*getMajorRadius()/r;
        setMajorP(majorP);
        setRatio(ratio);
        if(data.ratio>1.) switchMajorMinor();
        return;
    }
    if ((ref-getMinorPoint()).squared()<FAS_TOLERANCE_ANGLE) {
        FAS_Vector minorP=getMinorPoint() + offset;
        double r2=getMajorP().squared();
        if(r2<FAS_TOLERANCE2) return;
        FAS_Vector projected= getCenter() +
                getMajorP()*getMajorP().dotP(minorP-getCenter())/r2;
        double r=(minorP - projected).magnitude();
        if(r<FAS_TOLERANCE) return;
        double ratio = getRatio()*r/getMinorRadius();
        setRatio(ratio);
        if(data.ratio>1.) switchMajorMinor();
        return;
    }
}

/** whether the entity's bounding box intersects with visible portion of graphic view
//fix me, need to handle overlay container separately
*/
bool FAS_Ellipse::isVisibleInWindow(FAS_GraphicView* view) const
{
    FAS_Vector vpMin(view->toGraph(0,view->getHeight()));
    FAS_Vector vpMax(view->toGraph(view->getWidth(),0));
    //viewport
    QRectF visualRect(vpMin.x,vpMin.y,vpMax.x-vpMin.x, vpMax.y-vpMin.y);
    QPolygonF visualBox(visualRect);
    std::vector<FAS_Vector> vps;
    for(unsigned short i=0;i<4;i++){
        const QPointF& vp(visualBox.at(i));
        vps.push_back(FAS_Vector(vp.x(),vp.y()));
    }
    //check for intersection points with viewport
    for(unsigned short i=0;i<4;i++){
        FAS_Line line{vps.at(i),vps.at((i+1)%4)};
        FAS_Ellipse e0(nullptr, getData());
        if( FAS_Information::getIntersection(&e0, &line, true).size()>0) return true;
    }
    //is startpoint within viewport
    QRectF ellipseRect(minV.x, minV.y, maxV.x - minV.x, maxV.y - minV.y);
    return ellipseRect.intersects(visualRect);
}

/** return the equation of the entity
for quadratic,

return a vector contains:
m0 x^2 + m1 xy + m2 y^2 + m3 x + m4 y + m5 =0

for linear:
m0 x + m1 y + m2 =0
**/
LC_Quadratic FAS_Ellipse::getQuadratic() const
{
    std::vector<double> ce(6,0.);
    ce[0]=data.majorP.squared();
    ce[2]= data.ratio*data.ratio*ce[0];
    if(ce[0]<FAS_TOLERANCE2 || ce[2]<FAS_TOLERANCE2){
        return LC_Quadratic();
    }
    ce[0]=1./ce[0];
    ce[2]=1./ce[2];
    ce[5]=-1.;
    LC_Quadratic ret(ce);
    ret.rotate(getAngle());
    ret.move(data.center);
    return ret;
}

/**
 * @brief areaLineIntegral, line integral for contour area calculation by Green's Theorem
 * Contour Area =\oint x dy
 * @return line integral \oint x dy along the entity
 * \oint x dy = Cx y + \frac{1}{4}((a^{2}+b^{2})sin(2a)cos^{2}(t)-ab(2sin^{2}(a)sin(2t)-2t-sin(2t)))
 */
double FAS_Ellipse::areaLineIntegral() const
{
    const double a=getMajorRadius();
    const double b=getMinorRadius();
    if(!isEllipticArc())
        return M_PI*a*b;
    const double ab=a*b;
    const double r2=a*a+b*b;
    const double& cx=data.center.x;
    const double aE=getAngle();
    const double& a0=data.angle1;
    const double& a1=data.angle2;
    const double fStart=cx*getStartpoint().y+0.25*r2*sin(2.*aE)*cos(a0)*cos(a0)-0.25*ab*(2.*sin(aE)*sin(aE)*sin(2.*a0)-sin(2.*a0));
    const double fEnd=cx*getEndpoint().y+0.25*r2*sin(2.*aE)*cos(a1)*cos(a1)-0.25*ab*(2.*sin(aE)*sin(aE)*sin(2.*a1)-sin(2.*a1));
    return (isReversed()?fStart-fEnd:fEnd-fStart) + 0.5*ab*getAngleLength();
}

bool FAS_Ellipse::isReversed() const {
    return data.reversed;
}

void FAS_Ellipse::setReversed(bool r) {
    data.reversed = r;
}

double FAS_Ellipse::getAngle() const {
    return data.majorP.angle();
}

double FAS_Ellipse::getAngle1() const {
    return data.angle1;
}

void FAS_Ellipse::setAngle1(double a1) {
    data.angle1 = a1;
}

double FAS_Ellipse::getAngle2() const {
    return data.angle2;
}

void FAS_Ellipse::setAngle2(double a2) {
    data.angle2 = a2;
}

FAS_Vector FAS_Ellipse::getCenter() const {
    return data.center;
}

void FAS_Ellipse::setCenter(const FAS_Vector& c) {
    data.center = c;
}


const FAS_Vector& FAS_Ellipse::getMajorP() const {
    return data.majorP;
}

void FAS_Ellipse::setMajorP(const FAS_Vector& p) {
    data.majorP = p;
}

double FAS_Ellipse::getRatio() const {
    return data.ratio;
}

void FAS_Ellipse::setRatio(double r) {
    data.ratio = r;
}

double FAS_Ellipse::getAngleLength() const {
    double ret;
    if (isReversed()) {
        ret= FAS_Math::correctAngle(data.angle1-data.angle2);
    } else {
        ret= FAS_Math::correctAngle(data.angle2-data.angle1);
    }
    if(ret<FAS_TOLERANCE_ANGLE) ret=2.*M_PI;
    return ret;
}


double FAS_Ellipse::getMajorRadius() const {
    return data.majorP.magnitude();
}

FAS_Vector FAS_Ellipse::getMajorPoint() const{
    return data.center + data.majorP;
}

FAS_Vector FAS_Ellipse::getMinorPoint() const{
    return data.center +
            FAS_Vector(-data.majorP.y, data.majorP.x)*data.ratio;
}

double FAS_Ellipse::getMinorRadius() const {
    return data.majorP.magnitude()*data.ratio;
}

//void FAS_Ellipse::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& dataInsert){

//    if (! (painter && view))
//    {
//        return;
//    }

//    resetBasicData();

//    if (fabs(dataInsert.scaleFactor.x)>1.0e-6 && fabs(dataInsert.scaleFactor.y)>1.0e-6)
//    {

//        this->move(dataInsert.insertionPoint +
//                 FAS_Vector(dataInsert.spacing.x/dataInsert.scaleFactor.x*1, dataInsert.spacing.y/dataInsert.scaleFactor.y*1));
//    }
//    else
//    {
//        this->move(dataInsert.insertionPoint);
//    }
//    // Move because of block base point:
////    ne->move(dataInsert.*-1);

//    // Scale:
//    this->scale(dataInsert.insertionPoint, dataInsert.scaleFactor);

//    // Rotate:
//    this->rotate(dataInsert.insertionPoint, dataInsert.angle);


//    draw(painter,view,patternOffset);

//}
void FAS_Ellipse::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center1, const double& angle,const FAS_Vector& factor){

    // Ellipse center
    FAS_Vector bkcenter=data.bkcenter;
    // Endpoint of major axis relative to center.
    FAS_Vector bkmajorP=data.bkmajorP;
    // Ratio of minor axis to major axis.
    double bkratio=data.bkratio;
    // Start angle
    double bkangle1=data.bkangle1;
    // End angle
    double bkangle2=data.bkangle2;
    // Reversed (cw) flag
    bool bkreversed=data.bkreversed;

    // Ellipse center
    FAS_Vector center=this->getCenter();
    // Endpoint of major axis relative to center.
    FAS_Vector majorP=this->getMajorP();
    // Ratio of minor axis to major axis.
    double ratio=this->getRatio();
    // Start angle
    double angle1=this->getAngle1();
    // End angle
    double angle2=this->getAngle2();
    // Reversed (cw) flag
    bool reversed=this->isReversed();


    if(!bkcenter.valid && !bkmajorP.valid){

        data.bkcenter=center;
        data.bkmajorP=majorP;
        data.bkratio=ratio;
        data.bkangle1=angle1;
        data.bkangle2=angle2;
        data.bkreversed=reversed;
    }else{
        data.center=data.bkcenter;
        data.majorP=data.bkmajorP;
        data.ratio=data.bkratio;
        data.angle1=data.bkangle1;
        data.angle2=data.bkangle2;
        data.reversed=data.bkreversed;
    }
}

void FAS_Ellipse::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){
    data.center.move(offset);
    FAS_Vector angleVector(angle);
    data.center.rotate(center, angleVector);
    data.majorP.rotate(angleVector);

    FAS_Vector vpStart;
        FAS_Vector vpEnd;
        if(isEllipticArc()){
            //only handle start/end points for ellipse arc
            vpStart=getStartpoint().scale(center,factor);
            vpEnd=getEndpoint().scale(center,factor);
        }
        data.center.scale(center, factor);
        FAS_Vector vp1(getMajorP());
        double a(vp1.magnitude());
        if(a<FAS_TOLERANCE) return; //ellipse too small
        vp1 *= 1./a;
        double ct=vp1.x;
        double ct2 = ct*ct; // cos^2 angle
        double st=vp1.y;
        double st2=1.0 - ct2; // sin^2 angle
        double kx2= factor.x * factor.x;
        double ky2= factor.y * factor.y;
        double b=getRatio()*a;
        double cA=0.5*a*a*(kx2*ct2+ky2*st2);
        double cB=0.5*b*b*(kx2*st2+ky2*ct2);
        double cC=a*b*ct*st*(ky2-kx2);
        if (factor.x < 0)
            setReversed(!isReversed());
        if (factor.y < 0)
            setReversed(!isReversed());
        FAS_Vector vp(cA-cB,cC);
        vp1.set(a,b);
        vp1.scale(FAS_Vector(0.5*vp.angle()));
        vp1.rotate(FAS_Vector(ct,st));
        vp1.scale(factor);
        setMajorP(vp1);
        a=cA+cB;
        b=vp.magnitude();
        setRatio( sqrt((a - b)/(a + b) ));
        if( isEllipticArc() ) {
            setAngle1(getEllipseAngle(vpStart));
            setAngle2(getEllipseAngle(vpEnd));
        }
        correctAngles();//avoid extra 2.*M_PI in angles
        scaleBorders(center,factor);

}


void FAS_Ellipse::draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) {
    COMMONDEF->drawEntityCount++;
    if(isEllipticArc()==false){
        FAS_Ellipse arc(*this);
        arc.setAngle2(2.*M_PI);
        arc.setReversed(false);
        arc.draw(painter,view,patternOffset);
        return;
    }
    //only draw the visible portion of line
    FAS_Vector vpMin(view->toGraph(0,view->getHeight()));
    FAS_Vector vpMax(view->toGraph(view->getWidth(),0));
    QPolygonF visualBox(QRectF(vpMin.x,vpMin.y,vpMax.x-vpMin.x, vpMax.y-vpMin.y));

    FAS_Vector vpStart(isReversed()?getEndpoint():getStartpoint());
    FAS_Vector vpEnd(isReversed()?getStartpoint():getEndpoint());

    std::vector<FAS_Vector> vertex(0);
    for(unsigned short i=0;i<4;i++){
        const QPointF& vp(visualBox.at(i));
        vertex.emplace_back(vp.x(),vp.y());
    }
    /** angles at cross points */
    std::vector<double> crossPoints(0);

    double baseAngle=isReversed()?getAngle2():getAngle1();
    for(unsigned short i=0;i<4;i++){
        FAS_Line line{vertex.at(i),vertex.at((i+1)%4)};
        auto vpIts=FAS_Information::getIntersection(
                    static_cast<FAS_Entity*>(this), &line, true);
        //    std::cout<<"vpIts.size()="<<vpIts.size()<<std::endl;
        if( vpIts.size()==0) continue;
        for(const FAS_Vector& vp: vpIts){
            auto ap1=getTangentDirection(vp).angle();
            auto ap2=line.getTangentDirection(vp).angle();
            //ignore tangent points, because the arc doesn't cross over
            if( fabs( remainder(ap2 - ap1, M_PI) ) < FAS_TOLERANCE_ANGLE) continue;
            crossPoints.push_back(
                        FAS_Math::getAngleDifference(baseAngle, getEllipseAngle(vp))
                        );
        }
    }
    if(vpStart.isInWindowOrdered(vpMin, vpMax)) crossPoints.push_back(0.);
    if(vpEnd.isInWindowOrdered(vpMin, vpMax)) crossPoints.push_back(
                FAS_Math::getAngleDifference(baseAngle,isReversed()?getAngle1():getAngle2())
                );

    //sorting
    std::sort(crossPoints.begin(),crossPoints.end());
    //draw visible
    FAS_Ellipse arc(*this);
    arc.setSelected(isSelected());
    arc.setPen(getPen());
    arc.setReversed(false);
    if( crossPoints.size() >= 2) {
        for(size_t i=1;i<crossPoints.size();i+=2){
            arc.setAngle1(baseAngle+crossPoints[i-1]);
            arc.setAngle2(baseAngle+crossPoints[i]);
            arc.drawVisible(painter,view,patternOffset);
        }
        return;
    }
    //a workaround for buggy equation solver, can be removed, when line-ellipse equation solver is reliable
    if(isVisibleInWindow(view)){
        arc.drawVisible(painter,view,patternOffset);
    }
}

/** directly draw the arc, assuming the whole arc is within visible window */
void FAS_Ellipse::drawVisible(FAS_Painter* painter, FAS_GraphicView* view, double& /*patternOffset*/) {
    if (!(painter && view)) return;

    //visible in grahic view
    if(!isVisibleInWindow(view)) return;
    double ra(getMajorRadius()*view->getFactor().x);
    double rb(getRatio()*ra);
    if(std::min(ra, rb) < FAS_TOLERANCE) {//ellipse too small
        painter->drawLine(view->toGui(minV),view->toGui(maxV));
        return;
    }
    double mAngle=getAngle();
    FAS_Vector cp(view->toGui(getCenter()));
    //!isSelected() //modified 0724 ltree
    //    if (1 && (
    //             getPen().getLineType()==FAS2::SolidLine ||
    //             view->getDrawingMode()==FAS2::ModePreview)) {
    painter->drawEllipse(cp,
                         ra, rb,
                         mAngle,
                         getAngle1(), getAngle2(),
                         isReversed());
    return;
    //    }

    // Pattern:
    const FAS_LineTypePattern* pat = isSelected() ?
                &FAS_LineTypePattern::patternSolidLine :
                view->getPattern(getPen().getLineType());

    //    const FAS_LineTypePattern* pat = isSelected() ?
    //                &FAS_LineTypePattern::patternSolidLine :
    //                view->getPattern(getPen().getLineType());
    //modified 0724 ltree
    //const FAS_LineTypePattern* pat = view->getPattern(getPen().getLineType());

    if (!pat) {
        return;
    }

    // Pen to draw pattern is always solid:
    FAS_Pen pen = painter->getPen();
    pen.setLineType(FAS2::SolidLine);
    double a1(FAS_Math::correctAngle(getAngle1()));
    double a2(FAS_Math::correctAngle(getAngle2()));
    if (isReversed()) std::swap(a1,a2);
    if(a2 <a1+FAS_TOLERANCE_ANGLE) a2 += 2.*M_PI;
    painter->setPen(pen);
    if(pat->num <= 0){
        painter->drawEllipse(cp, ra, rb, mAngle, a1, a2, false);
        return;
    }

    std::vector<double> ds(pat->num, 0.);

    double dpmm=static_cast<FAS_PainterQt*>(painter)->getDpmm();
    for (size_t i = 0; i < pat->num; i++) {
        ds[i]= dpmm * pat->pattern[i]; //pattern length
        if(fabs(ds[i]) < 1.)
            ds[i] = copysign(1., ds[i]);
    }

    double curA(a1);
    bool notDone(true);

    //draw patterned ellipse
    for (size_t i=0; notDone; i = (i + 1) % pat->num) {
        double nextA = curA + std::abs(ds[i])/
                FAS_Vector(ra*sin(curA), rb*cos(curA)).magnitude();
        if (nextA > a2){
            nextA = a2;
            notDone = false;
        }
        if (ds[i] > 0.)
            painter->drawEllipse(cp, ra, rb, mAngle, curA, nextA, false);

        curA=nextA;
    }

}

/**
 * Dumps the point's data to stdout.
 */
std::ostream& operator << (std::ostream& os, const FAS_Ellipse& a) {
    os << " Ellipse: " << a.data << "\n";
    return os;
}

