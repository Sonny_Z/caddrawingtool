/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_ELLIPSE_H
#define FAS_ELLIPSE_H

#include "fas_atomicentity.h"

class LC_Quadratic;

// Holds the data that defines an ellipse.
struct FAS_EllipseData {
    // Ellipse center
    FAS_Vector center;
    // Endpoint of major axis relative to center.
    FAS_Vector majorP;
    // Ratio of minor axis to major axis.
    double ratio;
    // Start angle
    double angle1;
    // End angle
    double angle2;
    // Reversed (cw) flag
    bool reversed;

    // Ellipse center
    FAS_Vector bkcenter;
    // Endpoint of major axis relative to center.
    FAS_Vector bkmajorP;
    // Ratio of minor axis to major axis.
    double bkratio;
    // Start angle
    double bkangle1;
    // End angle
    double bkangle2;
    // Reversed (cw) flag
    bool bkreversed;
};

std::ostream& operator << (std::ostream& os, const FAS_EllipseData& ed);

class FAS_Ellipse : public FAS_AtomicEntity
{
public:
    FAS_Ellipse(){}
    FAS_Ellipse(FAS_EntityContainer* parent, const FAS_EllipseData& d);

    FAS_Entity* clone() const override;

    FAS2::EntityType rtti() const override{
        return FAS2::EntityEllipse;
    }

    FAS_Vector getStartpoint() const override;
    FAS_VectorSolutions getFoci() const;
    FAS_Vector getEndpoint() const override;
    FAS_Vector getEllipsePoint(const double& a) const; //find the point according to ellipse angle

    void moveStartpoint(const FAS_Vector& pos) override;
    void moveEndpoint(const FAS_Vector& pos) override;
    double getLength() const override;

    double getEllipseLength(double a1, double a2) const;
    double getEllipseLength(double a2) const;
    FAS_VectorSolutions getTangentPoint(const FAS_Vector& point) const override;//find the tangential points seeing from given point
    FAS_Vector getTangentDirection(const FAS_Vector& point)const override;
    FAS2::Ending getTrimPoint(const FAS_Vector& trimCoord,
                              const FAS_Vector& trimPoint) override;

    FAS_Vector prepareTrim(const FAS_Vector& trimCoord,
                           const FAS_VectorSolutions& trimSol) override;

    double getEllipseAngle (const FAS_Vector& pos) const;

    //return Copy of data that defines the ellipse.
    const FAS_EllipseData& getData() const;

    FAS_VectorSolutions getRefPoints() const override;

    // return true if the arc is reversed (clockwise),
    bool isReversed() const;
    // sets the reversed status.
    void setReversed(bool r);

    // return The rotation angle of this ellipse */
    double getAngle() const;

    // return The start angle of this arc */
    double getAngle1() const;
    /** Sets new start angle. */
    void setAngle1(double a1);
    // return The end angle of this arc */
    double getAngle2() const;
    /** Sets new end angle. */
    void setAngle2(double a2);


    // return The center point (x) of this arc
    FAS_Vector getCenter() const override;
    /** Sets new center. */
    void setCenter(const FAS_Vector& c);

    // return The endpoint of the major axis (relative to center).
    const FAS_Vector& getMajorP() const;
    // Sets new major point (relative to center).
    void setMajorP(const FAS_Vector& p);

    // return The ratio of minor to major axis
    double getRatio() const;
    // Sets new ratio.
    void setRatio(double r);

    // return Angle length in rad.
    double getAngleLength() const;

    // return The major radius of this ellipse. Same as getRadius()
    double getMajorRadius() const;

    // return the point by major minor radius directions
    FAS_Vector getMajorPoint() const;
    FAS_Vector getMinorPoint() const;

    // return The minor radius of this ellipse
    double getMinorRadius() const;
    bool isEllipticArc() const;
    bool isEdge() const override
    {
        return true;
    }
    bool createFrom4P(const FAS_VectorSolutions& sol);
    bool createFromCenter3Points(const FAS_VectorSolutions& sol);
    bool createFromQuadratic(const std::vector<double>& dn);
    bool createFromQuadratic(const LC_Quadratic& q);
    bool createInscribeQuadrilateral(const std::vector<FAS_Line*>& lines);
    FAS_Vector getMiddlePoint(void)const override;
    FAS_Vector getNearestEndpoint(const FAS_Vector& coord, double* dist = nullptr) const override;
    FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                       bool onEntity = true, double* dist = nullptr, FAS_Entity** entity=nullptr) const override;
    FAS_Vector getNearestCenter(const FAS_Vector& coord, double* dist = nullptr)const override;
    FAS_Vector getNearestMiddle(const FAS_Vector& coord, double* dist = nullptr, int middlePoints = 1)const override;
    FAS_Vector getNearestDist(double distance, const FAS_Vector& coord, double* dist = nullptr)const override;
    FAS_Vector getNearestOrthTan(const FAS_Vector& coord, const FAS_Line& normal, bool onEntity = false) const override;
    bool switchMajorMinor(void); //switch major minor axes to keep major the longer ellipse radius
    void correctAngles();//make sure angleLength() is not more than 2*M_PI
    bool isPointOnEntity(const FAS_Vector& coord, double tolerance=FAS_TOLERANCE) const override;
    void move(const FAS_Vector& offset) override;
    void rotate(const double& angle);
    void rotate(const FAS_Vector& angleVector);
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angle) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;

    // whether the entity's bounding box intersects with visible portion of graphic view
    bool isVisibleInWindow(FAS_GraphicView* view) const override;
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
//    void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;

    void drawVisible(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset);

    friend std::ostream& operator << (std::ostream& os, const FAS_Ellipse& a);

    //void calculateEndpoints() override;
    void calculateBorders() override;

    //direction of tangent at endpoints
    double getDirection1() const override;
    double getDirection2() const override;

    LC_Quadratic getQuadratic() const override;
    double areaLineIntegral() const override;

protected:
    FAS_EllipseData data;
};

#endif
//EOF
