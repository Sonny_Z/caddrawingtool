/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef LC_SPLINEPOINTS_H
#define LC_SPLINEPOINTS_H

#include <vector>
#include "fas_atomicentity.h"

class QPolygonF;
struct FAS_LineTypePattern;

// Holds the data that defines a spline.
struct LC_SplinePointsData
{
    LC_SplinePointsData() {}

    LC_SplinePointsData(bool closed, bool cut);

    bool closed;
    bool cut;
    // points on the spline.
    std::vector<FAS_Vector> splinePoints;
    std::vector<FAS_Vector> controlPoints;
};

std::ostream& operator << (std::ostream& os, const LC_SplinePointsData& ld);

class LC_SplinePoints : public FAS_AtomicEntity
{
private:
    void drawPattern(FAS_Painter* painter, FAS_GraphicView* view,
                     double& patternOffset, const FAS_LineTypePattern* pat);
    void drawSimple(FAS_Painter* painter, FAS_GraphicView* view);
    void UpdateControlPoints();
    void UpdateQuadExtent(const FAS_Vector& x1, const FAS_Vector& c1, const FAS_Vector& x2);
    int GetNearestQuad(const FAS_Vector& coord, double* dist, double* dt) const;
    FAS_Vector GetSplinePointAtDist(double dDist, int iStartSeg, double dStartT,
                                    int *piSeg, double *pdt) const;
    int GetQuadPoints(int iSeg, FAS_Vector *pvStart, FAS_Vector *pvControl,
                      FAS_Vector *pvEnd) const;

    bool offsetCut(const FAS_Vector& coord, const double& distance);
    bool offsetSpline(const FAS_Vector& coord, const double& distance);
    std::vector<FAS_Entity*> offsetTwoSidesSpline(const double& distance) const;
    std::vector<FAS_Entity*> offsetTwoSidesCut(const double& distance) const;
    LC_SplinePointsData data;

public:
    LC_SplinePoints(FAS_EntityContainer* parent, const LC_SplinePointsData& d);
    FAS_Entity* clone() const override;

    // return FAS2::EntitySpline
    FAS2::EntityType rtti() const override;

    // return false
    bool isEdge() const override;

    // return Copy of data that defines the spline.
    LC_SplinePointsData const& getData() const;
    LC_SplinePointsData& getData();

    // return Number of control points.
    size_t getNumberOfControlPoints() const;
    bool isClosed() const;
    void setClosed(bool c);
    void update() override;
    FAS_VectorSolutions getRefPoints() const override;
    // return Start point of the entity */
    FAS_Vector getStartpoint() const override;
    // return End point of the entity */
    FAS_Vector getEndpoint() const override;
    double getDirection1() const override;
    double getDirection2() const override;
    double getLength() const override;
    FAS_VectorSolutions getTangentPoint(const FAS_Vector& point) const override;
    FAS_Vector getTangentDirection(const FAS_Vector& point) const override;
    FAS_Vector getNearestEndpoint(const FAS_Vector& coord,
                                  double* dist = nullptr) const override;
    FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                       bool onEntity = true, double* dist = nullptr, FAS_Entity** entity = nullptr) const override;
    FAS_Vector getNearestMiddle(const FAS_Vector& coord,
                                double* dist = nullptr, int middlePoints = 1) const override;
    FAS_Vector getNearestDist(double distance,
                              const FAS_Vector& coord, double* dist = nullptr) const override;
    double getDistanceToPoint(const FAS_Vector& coord,
                              FAS_Entity** entity = nullptr, FAS2::ResolveLevel level = FAS2::ResolveNone,
                              double solidDist = FAS_MAXDOUBLE) const override;
    bool addPoint(const FAS_Vector& v);
    void removeLastPoint();
    void addControlPoint(const FAS_Vector& v);

    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;
    void revertDirection() override;

    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
    std::vector<FAS_Vector> const& getPoints() const;
    std::vector<FAS_Vector> const& getControlPoints() const;
    std::vector<FAS_Vector> getStrokePoints() const;

    friend std::ostream& operator << (std::ostream& os, const LC_SplinePoints& l);
    void calculateBorders() override;
    bool offset(const FAS_Vector& coord, const double& distance) override;
    std::vector<FAS_Entity*> offsetTwoSides(const double& distance) const override;
    static FAS_VectorSolutions getIntersection(FAS_Entity const* e1, FAS_Entity const* e2);
    FAS_VectorSolutions getLineIntersect(const FAS_Vector& x1, const FAS_Vector& x2);
    void addQuadIntersect(FAS_VectorSolutions *pVS, const FAS_Vector& x1,
                          const FAS_Vector& c1, const FAS_Vector& x2);
    FAS_VectorSolutions getSplinePointsIntersect(LC_SplinePoints* l1);
    FAS_VectorSolutions getQuadraticIntersect(FAS_Entity const* e1);
    LC_SplinePoints* cut(const FAS_Vector& pos);
    static QPolygonF getBoundingRect(const FAS_Vector& x1, const FAS_Vector& c1, const FAS_Vector& x2);
};

#endif

