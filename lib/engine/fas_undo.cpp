/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_undo.h"

//sort with alphabetical order
#include<iostream>

#include "fas_undocycle.h"
#include "mainwindow.h"

//Number of Cycles that can be undone.
int FAS_Undo::countUndoCycles()
{
    return undoPointer+1;
}

// return Number of Cycles that can be redone.
int FAS_Undo::countRedoCycles()
{
    return undoList.size()-1-undoPointer;
}

// Adds an Undo Cycle at the current position in the list.
void FAS_Undo::addUndoCycle(std::shared_ptr<FAS_UndoCycle> const& i)
{
    undoList.insert(undoList.begin() + (++undoPointer), i);
}

// Starts a new cycle for one undo step. Every undoable that is added after calling this method goes into this cycle.
void FAS_Undo::startUndoCycle()
{
    while (int(undoList.size()) > undoPointer+1)
    {
        auto& l = undoList.back();
        //remove the undoable in the current cyle
        for(auto u: l->getUndoables())
        {
            // Remove the pointer from _all_ other cycles:
            for(auto& cycle: undoList)
                if (&cycle != &l)
                    cycle->removeUndoable(u);

            if (u && u->isUndone())
            {
                removeUndoable(u);
            }
        }
        undoList.pop_back();
    }
    currentCycle = std::make_shared<FAS_UndoCycle>();
}

// Adds an undoable to the current undo cycle.
void FAS_Undo::addUndoable(FAS_Undoable* u)
{
    currentCycle->addUndoable(u);
}

// Ends the current undo cycle.
void FAS_Undo::endUndoCycle()
{
    addUndoCycle(currentCycle);
    setGUIButtons();
    currentCycle = std::make_shared<FAS_UndoCycle>();
}

// Undoes the last undo cycle.
bool FAS_Undo::undo()
{
    if (undoPointer < 0)
        return false;
    std::shared_ptr<FAS_UndoCycle> uc = undoList[undoPointer--];
    setGUIButtons();
    uc->changeUndoState();
    return true;
}

// Redoes the undo cycle which was at last undone.
bool FAS_Undo::redo()
{
    if (undoPointer+1 < int(undoList.size()))
    {
        std::shared_ptr<FAS_UndoCycle> uc = undoList[++undoPointer];
        setGUIButtons();
        uc->changeUndoState();
        return true;
    }
    return false;
}

// enable/disable redo/undo buttons in main application window
void FAS_Undo::setGUIButtons() const
{
    auto appWin = MainWindow::getAppWindow();
    if (!appWin) return;
    appWin->setRedoEnable(undoList.size() > 0 &&
                          undoPointer+1 < int(undoList.size()));
    appWin->setUndoEnable(undoList.size() > 0 && undoPointer >= 0);
}

// Dumps the undo list to stdout.
std::ostream& operator << (std::ostream& os, FAS_Undo& l) {
    os << "Undo List: " <<  "\n";
    os << " Pointer is at: " << l.undoPointer << "\n";

    for (int i = 0; i < int(l.undoList.size()); ++i) {

        if (i==l.undoPointer)
            os << " -->";
        else
            os << "    ";
        os << *(l.undoList.at(i)) << "\n";
    }
    return os;
}
