/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_dimangular.h"

//sort with alphabetical order
#include <cmath>
#include "fas_arc.h"
#include "fas_constructionline.h"
#include "fas_information.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_mtext.h"
#include "fas_solid.h"
#include "fas_units.h"
#include <iostream>

FAS_DimAngularData::FAS_DimAngularData():
    definitionPoint1(false),
    definitionPoint2(false),
    definitionPoint3(false),
    definitionPoint4(false)
{}

FAS_DimAngularData::FAS_DimAngularData(const FAS_Vector& _definitionPoint1,
                                       const FAS_Vector& _definitionPoint2,
                                       const FAS_Vector& _definitionPoint3,
                                       const FAS_Vector& _definitionPoint4):
    definitionPoint1(_definitionPoint1)
  ,definitionPoint2(_definitionPoint2)
  ,definitionPoint3(_definitionPoint3)
  ,definitionPoint4(_definitionPoint4)
{
}

std::ostream& operator << (std::ostream& os,
                           const FAS_DimAngularData& dd)
{
    os << "(" << dd.definitionPoint1 << "," << dd.definitionPoint2 << ","
       << dd.definitionPoint3 << "," << dd.definitionPoint3 << ")";
    return os;
}

FAS_DimAngular::FAS_DimAngular(FAS_EntityContainer* parent,
                               const FAS_DimensionData& d,
                               const FAS_DimAngularData& ed)
    : FAS_Dimension(parent, d), edata(ed)
{
    calculateBorders();
}

FAS_Entity* FAS_DimAngular::clone() const
{
    FAS_DimAngular* d = new FAS_DimAngular(*this);
    d->setOwner(isOwner());
    d->initId();
    d->detach();
    return d;
}

//return Automatically created label for the default measurement of this dimension.
QString FAS_DimAngular::getMeasuredLabel()
{
    QString ret;
    int dimaunit = getGraphicVariableInt("$DIMAUNIT", 0);
    int dimadec = getGraphicVariableInt("$DIMADEC", 0);
    int dimazin = getGraphicVariableInt("$DIMAZIN", 0);

    FAS2::AngleFormat format = FAS_Units::numberToAngleFormat(dimaunit);
    ret = FAS_Units::formatAngle(getAngle(), format, dimadec);
    if (format != FAS2::DegreesMinutesSeconds && format != FAS2::Surveyors)
        ret = stripZerosAngle(ret, dimazin);

    //verify if units are decimal and comma separator
    if (dimaunit !=1){
        if (getGraphicVariableInt("$DIMDSEP", 0) == 44)
            ret.replace(QChar('.'), QChar(','));
    }
    return ret;
}

// return Angle of the measured dimension.
double FAS_DimAngular::getAngle()
{
    double ang1 = 0.0;
    double ang2 = 0.0;
    bool reversed = false;
    FAS_Vector p1;
    FAS_Vector p2;

    getAngles(ang1, ang2, reversed, p1, p2);
    return reversed ? FAS_Math::correctAngle(ang1 - ang2) :  FAS_Math::correctAngle(ang2 - ang1);
}

// return Center of the measured dimension.
FAS_Vector FAS_DimAngular::getCenter() const {
    FAS_ConstructionLine l1(nullptr, FAS_ConstructionLineData(edata.definitionPoint1,
                                                              edata.definitionPoint2));
    FAS_ConstructionLine l2(nullptr, FAS_ConstructionLineData(edata.definitionPoint3,
                                                              data.definitionPoint));
    FAS_VectorSolutions vs = FAS_Information::getIntersection(&l1, &l2, false);

    return vs.get(0);
}

// finds out which angles this dimension actually measures.
bool FAS_DimAngular::getAngles(double& ang1, double& ang2, bool& reversed,
                               FAS_Vector& p1, FAS_Vector& p2)
{
    FAS_Vector vp0(edata.definitionPoint4 - getCenter());
    FAS_Vector vp1(edata.definitionPoint2 - edata.definitionPoint1);
    FAS_Vector vp2(data.definitionPoint - edata.definitionPoint3);

    double rp1 = vp1.squared();
    double rp2 = vp2.squared();
    double p0p1 = FAS_Vector::dotP(vp0, vp1);
    double p0p2 = FAS_Vector::dotP(vp0, vp2);
    double p1p2 = FAS_Vector::dotP(vp1, vp2);
    double d = rp1*rp2 - p1p2*p1p2;
    double a1 = d*(rp2*p0p1-p1p2*p0p2); // we only need the sign, so, use multiply instead of division to avoid divid by zero;
    if ( a1 >= 0.0)
    {
        p1 = edata.definitionPoint2;
    }
    else
    {
        vp1 *= -1;
        p1 = edata.definitionPoint1;
    }
    a1 = d*(rp1*p0p2-p1p2*p0p1);
    if ( a1 >= 0. )
    {
        p2 = data.definitionPoint;
    }
    else
    {
        vp2 *= -1;
        p2 = edata.definitionPoint3;
    }

    FAS_Vector center = getCenter();
    double ang = center.angleTo(edata.definitionPoint4);
    ang1=vp1.angle();
    ang2=vp2.angle();
    if ( !FAS_Math::isAngleBetween(ang, ang1,ang2,false))
    {
        reversed = true;
    }
    return true;
}
/*
 * Updates the sub entities of this dimension. Called when the
 * dimension or the position, alignment, .. changes.
 */
void FAS_DimAngular::updateDim(bool /*autoText*/) {
    clear();
    if (isUndone()) {
        return;
    }

    // general scale (DIMSCALE)
    double dimscale = getGeneralScale();
    // distance from entities (DIMEXO)
    double dimexo = getExtensionLineOffset()*dimscale;
    // extension line extension (DIMEXE)
    double dimexe = getExtensionLineExtension()*dimscale;
    // text height (DIMTXT)
    double dimtxt = getTextHeight()*dimscale;
    // text distance to line (DIMGAP)
    double dimgap = getDimensionLineGap()*dimscale;
    // arrow size:
    double arrowSize = getArrowSize()*dimscale;

    // find out center:
    FAS_Vector center = getCenter();

    if (!center.valid) {
        return;
    }

    double ang1 = 0.0;
    double ang2 = 0.0;
    bool reversed = false;
    FAS_Vector p1;
    FAS_Vector p2;

    getAngles(ang1, ang2, reversed, p1, p2);

    double rad = edata.definitionPoint4.distanceTo(center);

    double len;
    double dist;

    FAS_Pen pen(getExtensionLineColor(),
                getExtensionLineWidth(),
                FAS2::LineByBlock);

    // 1st extension line:
    dist = center.distanceTo(p1);
    len = rad - dist + dimexe;
    FAS_Vector dir = FAS_Vector::polar(1.0, ang1);
    FAS_Line* line = new FAS_Line{this,
            center + dir*dist + dir*dimexo,
            center + dir*dist + dir*len};
    line->setPen(pen);
    //    line->setPen(FAS_Pen(FAS2::FlagInvalid));
    line->setLayer(nullptr);
    addEntity(line);

    // 2nd extension line:
    dist = center.distanceTo(p2);
    len = rad - dist + dimexe;
    dir.setPolar(1.0, ang2);
    line = new FAS_Line{this,
            center + dir*dist + dir*dimexo,
            center + dir*dist + dir*len};
    line->setPen(pen);
    line->setLayer(nullptr);
    addEntity(line);

    // Create dimension line (arc):
    FAS_Arc* arc = new FAS_Arc(this,
                               FAS_ArcData(center,
                                           rad, ang1, ang2, reversed));
    pen.setWidth(getDimensionLineWidth());
    pen.setColor(getDimensionLineColor());
    arc->setPen(pen);
    arc->setLayer(nullptr);
    addEntity(arc);

    // length of dimension arc:
    double distance = arc->getLength();

    // do we have to put the arrows outside of the arc?
    bool outsideArrows = (distance<arrowSize*2);

    // arrow angles:
    double arrowAngle1, arrowAngle2;
    double arrowAng;
    if (rad>FAS_TOLERANCE_ANGLE)
    {
        arrowAng = arrowSize / rad;
    }
    else
    {
        arrowAng = 0.0;
    }

    FAS_Vector v1, v2;
    if (!arc->isReversed())
    {
        v1.setPolar(rad, arc->getAngle1()+arrowAng);
    }
    else
    {
        v1.setPolar(rad, arc->getAngle1()-arrowAng);
    }
    v1 += arc->getCenter();
    arrowAngle1 = arc->getStartpoint().angleTo(v1);


    if (!arc->isReversed())
    {
        v2.setPolar(rad, arc->getAngle2()-arrowAng);
    }
    else
    {
        v2.setPolar(rad, arc->getAngle2()+arrowAng);
    }
    v2+=arc->getCenter();
    arrowAngle2 = arc->getEndpoint().angleTo(v2);

    if (!outsideArrows)
    {
        arrowAngle1 = arrowAngle1+M_PI;
        arrowAngle2 = arrowAngle2+M_PI;
    }

    // Arrows:
    FAS_SolidData sd;
    FAS_Solid* arrow;

    // arrow 1
    arrow = new FAS_Solid(this, sd);
    arrow->shapeArrow(arc->getStartpoint(),
                      arrowAngle1,
                      arrowSize);
    arrow->setPen(pen);
    //    arrow->setPen(FAS_Pen(FAS2::FlagInvalid));
    arrow->setLayer(nullptr);
    addEntity(arrow);

    // arrow 2:
    arrow = new FAS_Solid(this, sd);
    arrow->shapeArrow(arc->getEndpoint(),
                      arrowAngle2,
                      arrowSize);
    arrow->setPen(pen);
    arrow->setLayer(nullptr);
    addEntity(arrow);

    // text label:
    FAS_MTextData textData;
    FAS_Vector textPos = arc->getMiddlePoint();

    FAS_Vector distV;
    double textAngle;
    double dimAngle1 = textPos.angleTo(arc->getCenter())-M_PI_2;

    // rotate text so it's readable from the bottom or right (ISO)
    // quadrant 1 & 4
    if (dimAngle1>M_PI_2*3.0+0.001 || dimAngle1<M_PI_2+0.001)
    {

        distV.setPolar(dimgap, dimAngle1+M_PI_2);
        textAngle = dimAngle1;
    }
    // quadrant 2 & 3
    else
    {
        distV.setPolar(dimgap, dimAngle1-M_PI_2);
        textAngle = dimAngle1+M_PI;
    }

    // move text away from dimension line:
    textPos+=distV;

    textData = FAS_MTextData(textPos,
                             dimtxt, 30.0,
                             FAS_MTextData::VABottom,
                             FAS_MTextData::HACenter,
                             FAS_MTextData::LeftToRight,
                             FAS_MTextData::Exact,
                             1.0,
                             getLabel(),
                             getTextStyle(),
                             textAngle);

    FAS_MText* text = new FAS_MText(this, textData);

    // move text to the side:
    text->setPen(FAS_Pen(getTextColor(), FAS2::WidthByBlock, FAS2::SolidLine));
    //    text->setPen(FAS_Pen(FAS2::FlagInvalid));
    text->setLayer(nullptr);
    addEntity(text);

    calculateBorders();
}

void FAS_DimAngular::move(const FAS_Vector& offset) {
    FAS_Dimension::move(offset);

    edata.definitionPoint1.move(offset);
    edata.definitionPoint2.move(offset);
    edata.definitionPoint3.move(offset);
    edata.definitionPoint4.move(offset);
    update();
}

void FAS_DimAngular::rotate(const FAS_Vector& center, const double& angle) {
    rotate(center, FAS_Vector(angle));
}

void FAS_DimAngular::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    FAS_Dimension::rotate(center, angleVector);

    edata.definitionPoint1.rotate(center, angleVector);
    edata.definitionPoint2.rotate(center, angleVector);
    edata.definitionPoint3.rotate(center, angleVector);
    edata.definitionPoint4.rotate(center, angleVector);
    update();
}

void FAS_DimAngular::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    FAS_Dimension::scale(center, factor);

    edata.definitionPoint1.scale(center, factor);
    edata.definitionPoint2.scale(center, factor);
    edata.definitionPoint3.scale(center, factor);
    edata.definitionPoint4.scale(center, factor);
    update();
}

void FAS_DimAngular::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    FAS_Dimension::mirror(axisPoint1, axisPoint2);

    edata.definitionPoint1.mirror(axisPoint1, axisPoint2);
    edata.definitionPoint2.mirror(axisPoint1, axisPoint2);
    edata.definitionPoint3.mirror(axisPoint1, axisPoint2);
    edata.definitionPoint4.mirror(axisPoint1, axisPoint2);
    update();
}

// Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_DimAngular& d) {
    os << " DimAngular: " << d.getData() << "\n" << d.getEData() << "\n";
    return os;
}

