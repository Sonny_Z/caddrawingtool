/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_constructionline.h"

//sort with alphabetical order
#include "lc_quadratic.h"

FAS_ConstructionLineData::FAS_ConstructionLineData():
    point1(false),
    point2(false)
{}

FAS_ConstructionLineData::FAS_ConstructionLineData(const FAS_Vector& point1,
                                                   const FAS_Vector& point2):
    point1(point1)
  ,point2(point2)
{
}

std::ostream& operator << (std::ostream& os,
                           const FAS_ConstructionLineData& ld)
{
    os << "(" << ld.point1 <<
          "/" << ld.point2 <<
          ")";
    return os;
}

FAS_ConstructionLine::FAS_ConstructionLine(FAS_EntityContainer* parent,
                                           const FAS_ConstructionLineData& d)
    :FAS_AtomicEntity(parent), data(d)
{

    calculateBorders();
}

FAS_Entity* FAS_ConstructionLine::clone() const
{
    FAS_ConstructionLine* c = new FAS_ConstructionLine(*this);
    c->initId();
    return c;
}

void FAS_ConstructionLine::calculateBorders()
{
    minV = FAS_Vector::minimum(data.point1, data.point2);
    maxV = FAS_Vector::maximum(data.point1, data.point2);
}

FAS_Vector FAS_ConstructionLine::getNearestEndpoint(const FAS_Vector& coord, double* dist) const
{
    double dist1, dist2;

    dist1 = (data.point1-coord).squared();
    dist2 = (data.point2-coord).squared();

    if (dist2<dist1)
    {
        if (dist) {
            *dist = sqrt(dist2);
        }
        return data.point2;
    }
    else
    {
        if (dist)
        {
            *dist = sqrt(dist1);
        }
        return data.point1;
    }
}

FAS_Vector FAS_ConstructionLine::getNearestPointOnEntity(const FAS_Vector& coord,
                                                         bool /*onEntity*/, double* /*dist*/, FAS_Entity** entity) const
{
    if (entity)
    {
        *entity = const_cast<FAS_ConstructionLine*>(this);
    }

    FAS_Vector ae = data.point2-data.point1;
    FAS_Vector ea = data.point1-data.point2;
    FAS_Vector ap = coord-data.point1;
    if (ae.magnitude()<1.0e-6 || ea.magnitude()<1.0e-6)
    {
        return FAS_Vector(false);
    }

    // Orthogonal projection from both sides:
    FAS_Vector ba = ae * FAS_Vector::dotP(ae, ap)
            / (ae.magnitude()*ae.magnitude());

    return data.point1 + ba;
}

FAS_Vector FAS_ConstructionLine::getNearestCenter(const FAS_Vector& /*coord*/,
                                                  double* dist) const
{
    if (dist)
    {
        *dist = FAS_MAXDOUBLE;
    }
    return FAS_Vector(false);
}

// return Copy of data that defines the line.
FAS_ConstructionLineData const& FAS_ConstructionLine::getData() const {
    return data;
}

// return First definition point.
FAS_Vector const& FAS_ConstructionLine::getPoint1() const {
    return data.point1;
}
// return Second definition point.
FAS_Vector const& FAS_ConstructionLine::getPoint2() const {
    return data.point2;
}

LC_Quadratic FAS_ConstructionLine::getQuadratic() const
{
    std::vector<double> ce(3,0.);
    auto dvp = data.point2 - data.point1;
    FAS_Vector normal(-dvp.y,dvp.x);
    ce[0] = normal.x;
    ce[1] = normal.y;
    ce[2] = -normal.dotP(data.point2);
    return LC_Quadratic(ce);
}

FAS_Vector FAS_ConstructionLine::getMiddlePoint() const{
    return FAS_Vector(false);
}
FAS_Vector FAS_ConstructionLine::getNearestMiddle(const FAS_Vector& /*coord*/,
                                                  double* dist, const int /*middlePoints*/)const {
    if (dist) {
        *dist = FAS_MAXDOUBLE;
    }
    return FAS_Vector(false);
}

FAS_Vector FAS_ConstructionLine::getNearestDist(double /*distance*/,
                                                const FAS_Vector& /*coord*/,
                                                double* dist) const{
    if (dist) {
        *dist = FAS_MAXDOUBLE;
    }
    return FAS_Vector(false);
}


double FAS_ConstructionLine::getDistanceToPoint(const FAS_Vector& coord,
                                                FAS_Entity** entity,
                                                FAS2::ResolveLevel /*level*/, double /*solidDist*/) const
{
    if (entity) {
        *entity = const_cast<FAS_ConstructionLine*>(this);
    }
    //double dist = FAS_MAXDOUBLE;
    FAS_Vector se = data.point2-data.point1;
    double d(se.magnitude());
    if(d<FAS_TOLERANCE) {
        //line too short
        return FAS_MAXDOUBLE;
    }
    se.set( se.x/d,-se.y/d); //normalized
    FAS_Vector vpc= coord - data.point1;
    vpc.rotate(se); // rotate to use the line as x-axis, and the distance is fabs(y)
    return ( fabs(vpc.y) );
}

void FAS_ConstructionLine::move(const FAS_Vector& offset) {
    data.point1.move(offset);
    data.point2.move(offset);
}

void FAS_ConstructionLine::rotate(const FAS_Vector& center, const double& angle) {
    FAS_Vector angleVector(angle);
    data.point1.rotate(center, angleVector);
    data.point2.rotate(center, angleVector);
}

void FAS_ConstructionLine::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    data.point1.rotate(center, angleVector);
    data.point2.rotate(center, angleVector);
}

void FAS_ConstructionLine::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    data.point1.scale(center, factor);
    data.point2.scale(center, factor);
}

void FAS_ConstructionLine::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    data.point1.mirror(axisPoint1, axisPoint2);
    data.point2.mirror(axisPoint1, axisPoint2);
}

std::ostream& operator << (std::ostream& os, const FAS_ConstructionLine& l) {
    os << " ConstructionLine: " << l.getData() << "\n";
    return os;
}
