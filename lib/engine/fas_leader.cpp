/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_leader.h"

//sort with alphabetical order
#include <iostream>

#include "fas_line.h"
#include "fas_solid.h"

FAS_Leader::FAS_Leader(FAS_EntityContainer* parent)
    :FAS_EntityContainer(parent)
    ,empty(true)
{
}

FAS_Leader::FAS_Leader(FAS_EntityContainer* parent,
                       const FAS_LeaderData& d)
    :FAS_EntityContainer(parent), data(d)
{
    empty = true;
}

FAS_Entity* FAS_Leader::clone() const
{
    FAS_Leader* p = new FAS_Leader(*this);
    p->setOwner(isOwner());
    p->initId();
    p->detach();
    return p;
}

void FAS_Leader::update()
{
    // find and delete arrow:
    for(auto e: entities)
    {
        if (e->rtti()==FAS2::EntitySolid)
        {
            removeEntity(e);
            break;
        }
    }
    if (isUndone())
    {
        return;
    }

    FAS_Entity* fe = firstEntity();
    if (fe && fe->isAtomic())
    {
        FAS_Vector p1 = ((FAS_AtomicEntity*)fe)->getStartpoint();
        FAS_Vector p2 = ((FAS_AtomicEntity*)fe)->getEndpoint();

        // first entity must be the line which gets the arrow:
        if (hasArrowHead())
        {
            FAS_Solid* s = new FAS_Solid(this, FAS_SolidData());
            s->shapeArrow(p1, p2.angleTo(p1), getGraphicVariableDouble("$DIMASZ", 2.5)* getGraphicVariableDouble("$DIMSCALE", 1.0));
            s->setPen(FAS_Pen(FAS2::FlagInvalid));
            s->setLayer(nullptr);
            FAS_EntityContainer::addEntity(s);
        }
    }
    calculateBorders();
}



// Adds a vertex from the endpoint of the last element or sets the startpoint to the point 'v'.
FAS_Entity* FAS_Leader::addVertex(const FAS_Vector& v)
{
    FAS_Entity* entity{nullptr};
    static FAS_Vector last = FAS_Vector{false};
    if (empty)
    {
        last = v;
        empty = false;
    }
    else
    {
        // add line to the leader:
        entity = new FAS_Line{this, {last, v}};
        entity->setPen(FAS_Pen(FAS2::FlagInvalid));
        entity->setLayer(nullptr);
        FAS_EntityContainer::addEntity(entity);
        if (count()==1 && hasArrowHead())
        {
            update();
        }

        last = v;
    }

    return entity;
}

/**
 * Reimplementation of the addEntity method for a normal container.
 * This reimplementation deletes the given entity!
 *
 * To add entities use addVertex() instead.
 */
void FAS_Leader::addEntity(FAS_Entity* entity)
{
    if (!entity)
        return;

    delete entity;
}

void FAS_Leader::move(const FAS_Vector& offset)
{
    FAS_EntityContainer::move(offset);
    update();
}

void FAS_Leader::rotate(const FAS_Vector& center, const double& angle)
{
    FAS_EntityContainer::rotate(center, angle);
    update();
}


void FAS_Leader::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    FAS_EntityContainer::rotate(center, angleVector);
    update();
}

void FAS_Leader::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    FAS_EntityContainer::scale(center, factor);
    update();
}

void FAS_Leader::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    FAS_EntityContainer::mirror(axisPoint1, axisPoint2);
    update();
}

void FAS_Leader::stretch(const FAS_Vector& firstCorner,
                         const FAS_Vector& secondCorner,
                         const FAS_Vector& offset)
{
    FAS_EntityContainer::stretch(firstCorner, secondCorner, offset);
    update();
}

// Dumps the leader's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_Leader& l) {
    os << " Leader: " << l.getData() << " {\n";

    os << (FAS_EntityContainer&)l;

    os << "\n}\n";

    return os;
}

std::ostream& operator << (std::ostream& os,
                           const FAS_LeaderData& /*ld*/) {
    os << "(Leader)";
    return os;
}

