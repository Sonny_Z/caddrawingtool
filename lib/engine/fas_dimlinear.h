/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_DIMLINEAR_H
#define FAS_DIMLINEAR_H

#include "fas_dimension.h"

// Holds the data that defines a linear dimension entity.
struct FAS_DimLinearData {
    FAS_DimLinearData();
    FAS_DimLinearData(const FAS_Vector& extensionPoint1,
                      const FAS_Vector& extensionPoint2,
                      double angle, double oblique);

    // Definition point. Startpoint of the first definition line.
    FAS_Vector extensionPoint1;
    // Definition point. Startpoint of the second definition line.
    FAS_Vector extensionPoint2;
    // Rotation angle in rad.
    double angle;
    // Oblique angle in rad.
    double oblique;
};

std::ostream& operator << (std::ostream& os,
                           const FAS_DimLinearData& dd);

class FAS_DimLinear : public FAS_Dimension {
public:
    FAS_DimLinear(FAS_EntityContainer* parent,
                  const FAS_DimensionData& d,
                  const FAS_DimLinearData& ed);
    virtual ~FAS_DimLinear() {}

    virtual FAS_Entity* clone() const;

    virtual FAS2::EntityType rtti() const {
        return FAS2::EntityDimLinear;
    }

    FAS_DimLinearData getEData() const {
        return edata;
    }

    virtual FAS_VectorSolutions getRefPoints() const;

    virtual QString getMeasuredLabel();

    virtual void updateDim(bool autoText=false);

    FAS_Vector getExtensionPoint1() const{
        return edata.extensionPoint1;
    }

    FAS_Vector getExtensionPoint2() const{
        return edata.extensionPoint2;
    }

    double getAngle() const{
        return edata.angle;
    }

    void setAngle(double a);

    double getOblique() const{
        return edata.oblique;
    }

    virtual void move(const FAS_Vector& offset);
    virtual void rotate(const FAS_Vector& center, const double& angle);
    virtual void rotate(const FAS_Vector& center, const FAS_Vector& angleVector);
    virtual void scale(const FAS_Vector& center, const FAS_Vector& factor);
    virtual void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2);
    virtual bool hasEndpointsWithinWindow(const FAS_Vector& v1, const FAS_Vector& v2);
    virtual void stretch(const FAS_Vector& firstCorner,
                         const FAS_Vector& secondCorner,
                         const FAS_Vector& offset);
    virtual void moveRef(const FAS_Vector& ref, const FAS_Vector& offset);

    friend std::ostream& operator << (std::ostream& os,
                                      const FAS_DimLinear& d);

protected:
    // Extended data.
    FAS_DimLinearData edata;
};

#endif
