/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include<iostream>
#include <cmath>
#include "fas_dimlinear.h"

#include "fas_constructionline.h"
#include "fas_graphic.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_units.h"

FAS_DimLinearData::FAS_DimLinearData():
    extensionPoint1(false),
    extensionPoint2(false),
    angle(0.0),
    oblique(0.0)
{}

FAS_DimLinearData::FAS_DimLinearData(const FAS_Vector& _extensionPoint1,
                                     const FAS_Vector& _extensionPoint2,
                                     double _angle, double _oblique):
    extensionPoint1(_extensionPoint1)
  ,extensionPoint2(_extensionPoint2)
  ,angle(_angle)
  ,oblique(_oblique)
{
}

std::ostream& operator << (std::ostream& os, const FAS_DimLinearData& dd)
{
    os << "(" << dd.extensionPoint1 << ","
       << dd.extensionPoint1 <<','
       << dd.angle <<','
       << dd.oblique <<','
       <<")";
    return os;
}

FAS_DimLinear::FAS_DimLinear(FAS_EntityContainer* parent,
                             const FAS_DimensionData& d,
                             const FAS_DimLinearData& ed)
    : FAS_Dimension(parent, d), edata(ed)
{
    calculateBorders();
}

FAS_Entity* FAS_DimLinear::clone() const
{
    FAS_DimLinear* d = new FAS_DimLinear(*this);
    d->setOwner(isOwner());
    d->initId();
    d->detach();
    return d;
}

FAS_VectorSolutions FAS_DimLinear::getRefPoints() const
{
    return FAS_VectorSolutions({edata.extensionPoint1, edata.extensionPoint2,
                                data.definitionPoint, data.middleOfText});
}
void FAS_DimLinear::setAngle(double a)
{
    edata.angle = FAS_Math::correctAngle(a);
}


//return Automatically created label for the default measurement of this dimension.

QString FAS_DimLinear::getMeasuredLabel()
{
    // direction of dimension line
    FAS_Vector dirDim = FAS_Vector::polar(100.0, edata.angle);

    // construction line for dimension line
    FAS_ConstructionLine dimLine(nullptr,
                                 FAS_ConstructionLineData(data.definitionPoint,
                                                          data.definitionPoint + dirDim));

    FAS_Vector dimP1 = dimLine.getNearestPointOnEntity(edata.extensionPoint1);
    FAS_Vector dimP2 = dimLine.getNearestPointOnEntity(edata.extensionPoint2);

    // Definitive dimension line:
    double dist = dimP1.distanceTo(dimP2) * getGeneralFactor();

    FAS_Graphic* graphic = getGraphic();
    QString ret;
    if (graphic)
    {
        int dimlunit = getGraphicVariableInt("$DIMLUNIT", 2);
        int dimdec = getGraphicVariableInt("$DIMDEC", 4);
        int dimzin = getGraphicVariableInt("$DIMZIN", 1);
        FAS2::LinearFormat format = graphic->getLinearFormat(dimlunit);
        ret = FAS_Units::formatLinear(dist, FAS2::None, format, dimdec);
        if (format == FAS2::Decimal)
            ret = stripZerosLinear(ret, dimzin);
        //verify if units are decimal and comma separator
        if (dimlunit==2)
        {
            if (getGraphicVariableInt("$DIMDSEP", 0) == 44)
                ret.replace(QChar('.'), QChar(','));
        }
    }
    else
    {
        ret = QString("%1").arg(dist);
    }
    return ret;
}



bool FAS_DimLinear::hasEndpointsWithinWindow(const FAS_Vector& v1, const FAS_Vector& v2) {
    return (edata.extensionPoint1.isInWindow(v1, v2) ||
            edata.extensionPoint2.isInWindow(v1, v2));
}



// Updates the sub entities of this dimension. Called when the text or the position, alignment changes.
void FAS_DimLinear::updateDim(bool autoText) {
    clear();
    if (isUndone())
    {
        return;
    }

    // general scale (DIMSCALE)
    double dimscale = getGeneralScale();
    // distance from entities (DIMEXO)
    double dimexo = getExtensionLineOffset()*dimscale;
    // extension line extension (DIMEXE)
    double dimexe = getExtensionLineExtension()*dimscale;

    // direction of dimension line
    FAS_Vector dirDim = FAS_Vector::polar(100.0, edata.angle);

    // construction line for dimension line
    FAS_ConstructionLine dimLine(
                nullptr,
                FAS_ConstructionLineData(data.definitionPoint,
                                         data.definitionPoint + dirDim));

    FAS_Vector dimP1 = dimLine.getNearestPointOnEntity(edata.extensionPoint1);
    FAS_Vector dimP2 = dimLine.getNearestPointOnEntity(edata.extensionPoint2);

    // Definitive dimension line:
    updateCreateDimensionLine(dimP1, dimP2, true, true, autoText);
    double extAngle1, extAngle2;

    if ((edata.extensionPoint1-dimP1).magnitude()<1e-6)
    {
        if ((edata.extensionPoint2-dimP2).magnitude()<1e-6)
        {
            //boot extension points are in dimension line only rotate 90
            extAngle2 = edata.angle + (M_PI_2);
        }
        else
        {
            //first extension point are in dimension line use second
            extAngle2 = edata.extensionPoint2.angleTo(dimP2);
        }
        extAngle1 = extAngle2;
    }
    else
    {
        //first extension point not are in dimension line use it
        extAngle1 = edata.extensionPoint1.angleTo(dimP1);
        if ((edata.extensionPoint2-dimP2).magnitude()<1e-6)
            extAngle2 = extAngle1;
        else
            extAngle2 = edata.extensionPoint2.angleTo(dimP2);
    }

    FAS_Vector vDimexe1 = FAS_Vector::polar(dimexe, extAngle1);
    FAS_Vector vDimexe2 = FAS_Vector::polar(dimexe, extAngle2);

    FAS_Vector vDimexo1, vDimexo2;
    if (getFixedLengthOn())
    {
        double dimfxl = getFixedLength();
        double extLength = (edata.extensionPoint1-dimP1).magnitude();
        if (extLength-dimexo > dimfxl)
            vDimexo1.setPolar(extLength - dimfxl, extAngle1);
        extLength = (edata.extensionPoint2-dimP2).magnitude();
        if (extLength-dimexo > dimfxl)
            vDimexo2.setPolar(extLength - dimfxl, extAngle2);
    }
    else
    {
        vDimexo1.setPolar(dimexo, extAngle1);
        vDimexo2.setPolar(dimexo, extAngle2);
    }

    FAS_Pen pen(getExtensionLineColor(),
                getExtensionLineWidth(),
                FAS2::LineByBlock);

    // extension lines:
    FAS_Line* line = new FAS_Line{this, edata.extensionPoint1+vDimexo1, dimP1+vDimexe1};
    line->setPen(pen);
    line->setLayer(nullptr);
    addEntity(line);
    line = new FAS_Line{this, edata.extensionPoint2+vDimexo2, dimP2+vDimexe2};
    line->setPen(pen);
    line->setLayer(nullptr);
    addEntity(line);
    calculateBorders();
}

void FAS_DimLinear::move(const FAS_Vector& offset) {
    FAS_Dimension::move(offset);

    edata.extensionPoint1.move(offset);
    edata.extensionPoint2.move(offset);
    update();
}

void FAS_DimLinear::rotate(const FAS_Vector& center, const double& angle) {
    FAS_Vector angleVector(angle);
    FAS_Dimension::rotate(center, angleVector);

    edata.extensionPoint1.rotate(center, angleVector);
    edata.extensionPoint2.rotate(center, angleVector);
    edata.angle = FAS_Math::correctAngle(edata.angle+angle);
    update();
}

void FAS_DimLinear::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    FAS_Dimension::rotate(center, angleVector);

    edata.extensionPoint1.rotate(center, angleVector);
    edata.extensionPoint2.rotate(center, angleVector);
    edata.angle = FAS_Math::correctAngle(edata.angle+angleVector.angle());
    update();
}

void FAS_DimLinear::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    FAS_Dimension::scale(center, factor);

    edata.extensionPoint1.scale(center, factor);
    edata.extensionPoint2.scale(center, factor);
    update();
}

void FAS_DimLinear::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    FAS_Dimension::mirror(axisPoint1, axisPoint2);

    edata.extensionPoint1.mirror(axisPoint1, axisPoint2);
    edata.extensionPoint2.mirror(axisPoint1, axisPoint2);

    FAS_Vector vec;
    vec.setPolar(1.0, edata.angle);
    vec.mirror(FAS_Vector(0.0,0.0), axisPoint2-axisPoint1);
    edata.angle = vec.angle();

    update();
}

void FAS_DimLinear::stretch(const FAS_Vector& firstCorner,
                            const FAS_Vector& secondCorner,
                            const FAS_Vector& offset)
{
    if (getMin().isInWindow(firstCorner, secondCorner) &&
            getMax().isInWindow(firstCorner, secondCorner))
    {
        move(offset);
    }
    else
    {
        if (edata.extensionPoint1.isInWindow(firstCorner,
                                             secondCorner)) {
            edata.extensionPoint1.move(offset);
        }
        if (edata.extensionPoint2.isInWindow(firstCorner,
                                             secondCorner)) {
            edata.extensionPoint2.move(offset);
        }
    }
    updateDim(true);
}

void FAS_DimLinear::moveRef(const FAS_Vector& ref, const FAS_Vector& offset)
{
    if (ref.distanceTo(data.definitionPoint)<1.0e-4)
    {
        data.definitionPoint += offset;
        updateDim(true);
    }
    else if (ref.distanceTo(data.middleOfText)<1.0e-4)
    {
        data.middleOfText += offset;
        updateDim(false);
    }
    else if (ref.distanceTo(edata.extensionPoint1)<1.0e-4)
    {
        edata.extensionPoint1 += offset;
        updateDim(true);
    }
    else if (ref.distanceTo(edata.extensionPoint2)<1.0e-4)
    {
        edata.extensionPoint2 += offset;
        updateDim(true);
    }
}

// Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_DimLinear& d) {
    os << " DimLinear: " << d.getData() << "\n" << d.getEData() << "\n";
    return os;
}

