/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_LAYERLIST_H
#define FAS_LAYERLIST_H

#include <QList>
#include "fas_layer.h"

class FAS_LayerListListener;
class QG_LayerWidget;

class FAS_LayerList
{
public:
    FAS_LayerList();
    void clear();

    // return Number of layers in the list.
    unsigned int count() const
    {
        return layers.count();
    }

    // return Layer at given position or nullptr if i is out of range.
    FAS_Layer* at(unsigned int i)
    {
        return layers.at(i);
    }
    QList<FAS_Layer*>::iterator begin();
    QList<FAS_Layer*>::iterator end();
    QList<FAS_Layer*>::const_iterator begin()const;
    QList<FAS_Layer*>::const_iterator end()const;

    void activate(const QString& name, bool notify = false);
    void activate(FAS_Layer* layer, bool notify = false);
    FAS_Layer* getActive()
    {
        return activeLayer;
    }
    virtual void add(FAS_Layer* layer);
    virtual void remove(FAS_Layer* layer);
    virtual void edit(FAS_Layer* layer, const FAS_Layer& source);
    FAS_Layer* find(const QString& name);
    int getIndex(const QString& name);
    int getIndex(FAS_Layer* layer);
    void toggle(const QString& name);
    void toggle(FAS_Layer* layer);
    void toggleLock(FAS_Layer* layer);
    void togglePrint(FAS_Layer* layer);
    void toggleConstruction(FAS_Layer* layer);
    void freezeAll(bool freeze);

    void addListener(FAS_LayerListListener* listener);
    void removeListener(FAS_LayerListListener* listener);

    // Sets the layer lists modified status to 'm'.
    void setModified(bool m)
    {
        modified = m;
    }

    // true The layer list has been modified.
    virtual bool isModified() const
    {
        return modified;
    }
    // sort by layer names
    void sort();

    friend std::ostream& operator << (std::ostream& os, FAS_LayerList& l);

private:
    // layers in the graphic
    QList<FAS_Layer*> layers;
    // List of registered LayerListListeners
    QList<FAS_LayerListListener*> layerListListeners;
    QG_LayerWidget* layerWidget;
    // Currently active layer
    FAS_Layer* activeLayer;
    // Flag set if the layer list was modified and not yet saved.
    bool modified;
};

#endif
