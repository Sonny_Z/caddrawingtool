/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_blocklist.h"

//sort with alphabetical order
#include "fas_block.h"
#include <iostream>
#include <QString>
#include <QRegExp>
#include <set>

FAS_BlockList::FAS_BlockList(bool owner)
{
    this->owner = owner;
    setModified(false);
}

void FAS_BlockList::clear()
{
    blocks.clear();
    blocksHash.clear();
    setModified(true);
}
bool FAS_BlockList::addClone(FAS_Block* block)
{
    if (!block)
    {
        return false;
    }


    // check if block already exists:
    //FAS_Block* b = find(block->getName());
    QString name=block->getName();
    if (!blocksHash.contains(name))
    {
        FAS_Block* clonedBlcok=block->clone()->getBlock();
        //clonedBlocks.append(clonedBlcok);
        // blocksHash.insert(block->getName(),block);
        blocksHash.insert(name,clonedBlcok);

        return true;
    }

}
/*
 * Adds a block to the block list. If a block with the same name
 * exists already, the given block will be deleted if the blocklist
 * owns the blocks.
 */
bool FAS_BlockList::add(FAS_Block* block, bool notify)
{
    if (!block)
    {
        return false;
    }


    // check if block already exists:
    FAS_Block* b = find(block->getName());
    if (!b)
    {
        blocks.append(block);
        //FAS_Block* clonedBlcok=block->clone()->getBlock();
        //clonedBlocks.append(clonedBlcok);
//        blocksHash.insert(block->getName(),block);
        setModified(true);
        return true;
    }
    else
    {
        if (owner)
        {
            delete block;
            block = nullptr;
        }
        return false;
    }
}

// Removes a block from the list.
void FAS_BlockList::remove(FAS_Block* block)
{
    // here the block is removed from the list but not deleted
    blocks.removeOne(block);
    setModified(true);

    // now it's save to delete the block
    if (owner)
    {
        delete block;
    }
}
FAS_Block* FAS_BlockList::first(){
    return blocks.first();
}

FAS_Block* FAS_BlockList::last(){
    return blocks.last();
}
// return Pointer to the block with the given name or nullptr if no such block was found.
FAS_Block* FAS_BlockList::find(const QString& name)
{
    //DFS
    std::vector<FAS_BlockList const*> nodes;
    std::set<FAS_BlockList const*> searched;
    searched.insert(nullptr);
    nodes.push_back(this);
    while (nodes.size())
    {
        auto list = nodes.back();
        nodes.pop_back();
        for (FAS_Block* b: *list)
        {
            auto bName = b->getName();
            if (name.compare(bName,Qt::CaseInsensitive)==0)
                return b;
            auto node = b->getBlockList();
            if (!searched.count(node))
            {
                searched.insert(list);
                nodes.push_back(b->getBlockList());
            }
        }
    }

    return nullptr;
}

// Switches on / off the given block.
void FAS_BlockList::toggle(const QString& name) {
    toggle(find(name));
}

// Switches on / off the given block.
void FAS_BlockList::toggle(FAS_Block* block) {
    if (!block)
    {
        return;
    }

    block->toggle();
}

// Freezes or defreezes all blocks.
void FAS_BlockList::freezeAll(bool freeze)
{
    for (int l=0; l<count(); l++)
    {
        at(l)->freeze(freeze);
    }
}

int FAS_BlockList::count() const
{
    return blocks.count();
}

// return Block at given position or nullptr if i is out of range.
FAS_Block* FAS_BlockList::at(int i)
{
    return blocks.at(i);
}
FAS_Block* FAS_BlockList::at(int i) const
{
    return blocks.at(i);
}
QList<FAS_Block*>::iterator FAS_BlockList::begin()
{
    return blocks.begin();
}

QList<FAS_Block*>::iterator FAS_BlockList::end()
{
    return blocks.end();
}

QList<FAS_Block*>::const_iterator FAS_BlockList::begin()const
{
    return blocks.begin();
}

QList<FAS_Block*>::const_iterator FAS_BlockList::end()const
{
    return blocks.end();
}

FAS_Block* FAS_BlockList::getBlockInHash(QString name){

    return blocksHash.find(name).value();

}

 QHash<QString,FAS_Block*> FAS_BlockList::getClonedHashBlocks()
 {
     return blocksHash;
 }
// Sets the layer lists modified status to 'm'.
void FAS_BlockList::setModified(bool m) {
    modified = m;
}

bool FAS_BlockList::isModified() const {
    return modified;
}

// Dumps the blocks to stdout.
std::ostream& operator << (std::ostream& os, FAS_BlockList& b) {

    os << "Blocklist: \n";
    for (int i=0; i<b.count(); ++i) {
        FAS_Block* blk = b.at(i);

        os << *blk << "\n";
    }

    return os;
}
