/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_pattern.h"

//sort with alphabetical order
#include "fas_dxfprocessor.h"
#include "fas_graphic.h"
#include "fas_layer.h"
#include "fas_system.h"

FAS_Pattern::FAS_Pattern(const QString& fileName)
    : FAS_EntityContainer(nullptr)
    ,fileName(fileName)
    ,loaded(false)
{
}

/*
 * Loads the given pattern file into this pattern.
 * Entities other than lines are ignored.
 */
bool FAS_Pattern::loadPattern()
{
    if (loaded)
    {
        return true;
    }

    QString path;
    // Search for the appropriate pattern if we have only the name of the pattern:
    if (!fileName.toLower().contains(".dxf"))
    {
        QStringList patterns = FAS_SYSTEM->getPatternList();
        QFileInfo file;
        for (QStringList::Iterator it = patterns.begin(); it!=patterns.end(); it++)
        {
            if (QFileInfo(*it).baseName().toLower()==fileName.toLower())
            {
                path = *it;
                break;
            }
        }
    }
    // We have the full path of the pattern:
    else
    {
        path = fileName;
    }

    // No pattern paths found:
    if (path.isEmpty())
    {
        return false;
    }

    FAS_Graphic gr;
    FAS_DXFProcessor dxfReader;
    dxfReader.fileImport(gr, path);
    for(auto e: gr)
    {
        if (e->rtti()==FAS2::EntityLine || e->rtti()==FAS2::EntityArc|| e->rtti()==FAS2::EntityEllipse)
        {
            FAS_Layer* l = e->getLayer();
            FAS_Entity* cl = e->clone();
            cl->reparent(this);
            if (l)
            {
                cl->setLayer(l->getName());
            }
            addEntity(cl);
        }
    }

    loaded = true;

    return true;
}

QString FAS_Pattern::getFileName() const {
    return fileName;
}

