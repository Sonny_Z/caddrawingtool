#ifndef FAS_FUNLIB_H
#define FAS_FUNLIB_H

#include "fas_graphic.h"
class FAS_FunLib
{
public:
    static bool writeFAS_Graphic(QString fileName,FAS_Graphic& graphic);
    static bool readFAS_Graphic(QString fileName,FAS_Graphic& graphic);
    static bool writeFAS_Graphic(QString fileName,QVector<FAS_Graphic>& graphics);
    static bool readFAS_Graphic(QString fileName,QVector<FAS_Graphic>& graphics);

};

#endif // FAS_FUNLIB_H
