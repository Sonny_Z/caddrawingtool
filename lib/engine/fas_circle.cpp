/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_circle.h"

//sort with alphabetical order
#include <cfloat>
#include "fas_arc.h"
#include "fas_graphicview.h"
#include "fas_information.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_painter.h"
#include "lc_quadratic.h"
#include "fas_insert.h"
#include <QPolygonF>

FAS_CircleData::FAS_CircleData(FAS_Vector const& center, double radius):
    center(center)
  , radius(radius)
{
}

bool FAS_CircleData::isValid() const {
    return (center.valid && radius>FAS_TOLERANCE);
}

bool FAS_CircleData::operator == (FAS_CircleData const& rhs) const
{
    if(! (center.valid && rhs.center.valid)) return false;
    if( center.squaredTo(rhs.center) > FAS_TOLERANCE2) return false;
    return fabs(radius - rhs.radius)< FAS_TOLERANCE;
}

std::ostream& operator << (std::ostream& os, const FAS_CircleData& ad)
{
    os << "(" << ad.center <<
          "/" << ad.radius <<
          ")";
    return os;
}

FAS_Circle::FAS_Circle(FAS_EntityContainer* parent,
                       const FAS_CircleData& d)
    :FAS_AtomicEntity(parent), data(d)
{
    calculateBorders();
}

FAS_Entity* FAS_Circle::clone() const
{
    FAS_Circle* c = new FAS_Circle(*this);
    c->initId();
    return c;
}


void FAS_Circle::calculateBorders()
{
    FAS_Vector r(data.radius, data.radius);
    minV = data.center - r;
    maxV = data.center + r;
}


// The center point (x) of this arc
FAS_Vector FAS_Circle::getCenter() const
{
    return data.center;
}
// Sets new center.
void FAS_Circle::setCenter(const FAS_Vector& c)
{
    data.center = c;
}
//return The radius of this arc.
double FAS_Circle::getRadius() const
{
    return data.radius;
}
/** Sets new radius. */
void FAS_Circle::setRadius(double r)
{
    data.radius = r;
}

// return Angle length in rad.
double FAS_Circle::getAngleLength() const {
    return 2*M_PI;
}

// return Length of the circle which is the circumference.
double FAS_Circle::getLength() const {
    return 2*M_PI*data.radius;
}

bool FAS_Circle::isTangent(const FAS_CircleData&  circleData) const
{
    const double d=circleData.center.distanceTo(data.center);
    const double r0=fabs(circleData.radius);
    const double r1=fabs(data.radius);
    if( fabs(d-fabs(r0-r1))<20.*FAS_TOLERANCE ||
            fabs(d-fabs(r0+r1))<20.*FAS_TOLERANCE )
        return true;
    return false;
}

// Creates this circle from a center point and a radius.
bool FAS_Circle::createFromCR(const FAS_Vector& c, double r)
{
    if (fabs(r)>FAS_TOLERANCE && c.valid )
    {
        data.radius = fabs(r);
        data.center = c;
        return true;
    }
    else
    {
        return false;
    }
}

// Creates this circle from two opposite points.
bool FAS_Circle::createFrom2P(const FAS_Vector& p1, const FAS_Vector& p2) {
    double r=0.5*p1.distanceTo(p2);
    if (r>FAS_TOLERANCE) {
        data.radius = r;
        data.center = (p1+p2)*0.5;
        return true;
    } else {
        return false;
    }
}

// Creates this circle from 3 given points which define the circle line.
bool FAS_Circle::createFrom3P(const FAS_Vector& p1, const FAS_Vector& p2,
                              const FAS_Vector& p3) {
    FAS_Vector vra=p2 - p1;
    FAS_Vector vrb=p3 - p1;
    double ra2=vra.squared()*0.5;
    double rb2=vrb.squared()*0.5;
    double crossp=vra.x * vrb.y - vra.y * vrb.x;
    if (fabs(crossp)< FAS_TOLERANCE2) {
        return false;
    }
    crossp=1./crossp;
    data.center.set((ra2*vrb.y - rb2*vra.y)*crossp,(rb2*vra.x - ra2*vrb.x)*crossp);
    data.radius=data.center.magnitude();
    data.center += p1;
    return true;
}
// create Circle from 3 points
bool FAS_Circle::createFrom3P(const FAS_VectorSolutions& sol) {
    if(sol.getNumber() < 2) return false;
    if(sol.getNumber() == 2) return createFrom2P(sol.get(0),sol.get(1));
    if((sol.get(1)-sol.get(2)).squared() < FAS_TOLERANCE2)
        return createFrom2P(sol.get(0),sol.get(1));
    FAS_Vector vra(sol.get(1) - sol.get(0));
    FAS_Vector vrb(sol.get(2) - sol.get(0));
    double ra2=vra.squared()*0.5;
    double rb2=vrb.squared()*0.5;
    double crossp=vra.x * vrb.y - vra.y * vrb.x;
    if (fabs(crossp)< FAS_TOLERANCE2) {
        return false;
    }
    crossp=1./crossp;
    data.center.set((ra2*vrb.y - rb2*vra.y)*crossp,(rb2*vra.x - ra2*vrb.x)*crossp);
    data.radius=data.center.magnitude();
    data.center += sol.get(0);
    return true;
}

// create circle inscribled in a triangle
bool FAS_Circle::createInscribe(const FAS_Vector& coord, const std::vector<FAS_Line*>& lines){
    if(lines.size()<3) return false;
    std::vector<FAS_Line*> tri(lines);
    FAS_VectorSolutions sol=FAS_Information::getIntersectionLineLine(tri[0],tri[1]);
    if(sol.getNumber() == 0 ) {//move parallel to opposite
        std::swap(tri[1],tri[2]);
        sol=FAS_Information::getIntersectionLineLine(tri[0],tri[1]);
    }
    if(sol.getNumber() == 0 ) return false;
    FAS_Vector vp0(sol.get(0));
    sol=FAS_Information::getIntersectionLineLine(tri[2],tri[1]);
    if(sol.getNumber() == 0 ) return false;
    FAS_Vector vp1(sol.get(0));
    FAS_Vector dvp(vp1-vp0);
    double a(dvp.squared());
    if( a< FAS_TOLERANCE2) return false; //three lines share a common intersecting point
    FAS_Vector vp(coord - vp0);
    vp -= dvp*(FAS_Vector::dotP(dvp,vp)/a); //normal component
    FAS_Vector vl0(tri[0]->getEndpoint() - tri[0]->getStartpoint());
    a=dvp.angle();
    double angle0(0.5*(vl0.angle() + a));
    if( FAS_Vector::dotP(vp,vl0) <0.) {
        angle0 += 0.5*M_PI;
    }

    FAS_Line line0(vp0, vp0+FAS_Vector(angle0));//first bisecting line
    vl0=(tri[2]->getEndpoint() - tri[2]->getStartpoint());
    angle0=0.5*(vl0.angle() + a+M_PI);
    if( FAS_Vector::dotP(vp,vl0) <0.) {
        angle0 += 0.5*M_PI;
    }
    FAS_Line line1(vp1, vp1+FAS_Vector(angle0));//second bisection line
    sol=FAS_Information::getIntersectionLineLine(&line0,&line1);
    if(sol.getNumber() == 0 ) return false;

    bool ret=createFromCR(sol.get(0),tri[1]->getDistanceToPoint(sol.get(0)));
    if(!ret) return false;
    for(auto p: lines){
        if(! p->isTangent(data)) return false;
    }
    return true;
}

std::vector<FAS_Entity* > FAS_Circle::offsetTwoSides(const double& distance) const
{
    std::vector<FAS_Entity*> ret(0,nullptr);
    ret.push_back(new FAS_Circle(nullptr, {getCenter(),getRadius()+distance}));
    if(fabs(getRadius()-distance)>FAS_TOLERANCE)
        ret.push_back(new FAS_Circle(nullptr, {getCenter(),fabs(getRadius()-distance)}));
    return ret;
}

FAS_VectorSolutions FAS_Circle::createTan1_2P(const FAS_AtomicEntity* circle, const std::vector<FAS_Vector>& points)
{
    FAS_VectorSolutions ret;
    if (!circle||points.size()<2) return ret;
    return LC_Quadratic::getIntersection(
                LC_Quadratic(circle,points[0]),
            LC_Quadratic(circle,points[1])
            );
}

// create a circle of radius r and tangential to two given entities
FAS_VectorSolutions FAS_Circle::createTan2(const std::vector<FAS_AtomicEntity*>& circles, const double& r)
{
    if(circles.size()<2) return false;
    auto e0=circles[0]->offsetTwoSides(r);
    auto e1=circles[1]->offsetTwoSides(r);
    FAS_VectorSolutions centers;
    if(e0.size() && e1.size()) {
        for(auto it0=e0.begin();it0!=e0.end();it0++){
            for(auto it1=e1.begin();it1!=e1.end();it1++){
                centers.push_back(FAS_Information::getIntersection(*it0,*it1));
            }
        }
    }
    for(auto it0=e0.begin();it0!=e0.end();it0++){
        delete *it0;
    }
    for(auto it0=e1.begin();it0!=e1.end();it0++){
        delete *it0;
    }
    return centers;

}

std::vector<FAS_Circle> FAS_Circle::createTan3(const std::vector<FAS_AtomicEntity*>& circles)
{
    std::vector<FAS_Circle> ret;
    if(circles.size()!=3) return ret;
    std::vector<FAS_Circle> cs;
    for(auto c: circles){
        cs.emplace_back(FAS_Circle(nullptr, {c->getCenter(),c->getRadius()}));
    }
    unsigned short flags=0;
    do{
        for(unsigned short j=0u;j<3u;++j){
            if(flags & (1u<<j)) {
                cs[j].setRadius( - fabs(cs[j].getRadius()));
            }else{
                cs[j].setRadius( fabs(cs[j].getRadius()));
            }
        }
        auto list=solveAppolloniusSingle(cs);
        if(list.size()>=1){
            for(FAS_Circle& c0: list){
                bool addNew=true;
                for(FAS_Circle& c: ret){
                    if((c0.getCenter()-c.getCenter()).squared()<FAS_TOLERANCE15 && fabs(c0.getRadius() - c.getRadius())<FAS_TOLERANCE){
                        addNew=false;
                        break;
                    }
                }
                if(addNew) ret.push_back(c0);
            }
        }


    }while(++flags<8u);
    //    std::cout<<__FILE__<<" : "<<__func__<<" : line "<<__LINE__<<std::endl;
    //    std::cout<<"before testing, ret.size()="<<ret.size()<<std::endl;
    for(size_t i=0;i<ret.size();){
        if(ret[i].testTan3(circles) == false) {
            ret.erase(ret.begin()+i);
        }else{
            ++i;
        }
    }
    //        DEBUG_HEADER
    //    std::cout<<"after testing, ret.size()="<<ret.size()<<std::endl;
    return ret;
}

bool FAS_Circle::testTan3(const std::vector<FAS_AtomicEntity*>& circles)
{

    if(circles.size()!=3) return false;

    for(auto const& c: circles){
        const double r0 = fabs(data.radius);
        const double r1 = fabs(c->getRadius());

        const double dist=fabs((data.center - c->getCenter()).magnitude());
        double const rmax=std::max(r0,r1);
        if( dist < rmax )
            return fabs(dist - fabs(r0 - r1)) <= sqrt(DBL_EPSILON)*rmax;
        else
            return fabs(dist - fabs(r0 + r1)) <= sqrt(DBL_EPSILON)*rmax;
    }
    return true;
}

// solve one of the eight Appollonius Equations
std::vector<FAS_Circle> FAS_Circle::solveAppolloniusSingle(const std::vector<FAS_Circle>& circles)
{
    std::vector<FAS_Circle> ret;

    std::vector<FAS_Vector> centers;
    std::vector<double> radii;

    for(auto c: circles){
        if(c.getCenter().valid==false) return ret;
        centers.push_back(c.getCenter());
        radii.push_back(c.getRadius());
    }
    /** form the linear equation to solve center in radius **/
    std::vector<std::vector<double> > mat(2,std::vector<double>(3,0.));
    mat[0][0]=centers[2].x - centers[0].x;
    mat[0][1]=centers[2].y - centers[0].y;
    mat[1][0]=centers[2].x - centers[1].x;
    mat[1][1]=centers[2].y - centers[1].y;
    if(fabs(mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0])<FAS_TOLERANCE2){
        size_t i0=0;
        if( centers[0].distanceTo(centers[1]) <= FAS_TOLERANCE ||  centers[0].distanceTo(centers[2]) <= FAS_TOLERANCE) i0 = 1;
        LC_Quadratic lc0(& (circles[i0]), & (circles[(i0+1)%3]));
        LC_Quadratic lc1(& (circles[i0]), & (circles[(i0+2)%3]));
        auto c0 = LC_Quadratic::getIntersection(lc0, lc1);
        for(size_t i=0; i<c0.size(); i++){
            const double dc =  c0[i].distanceTo(centers[i0]);
            ret.push_back(FAS_Circle(nullptr, {c0[i], fabs(dc - radii[i0])}));
            if( dc > radii[i0]) {
                ret.push_back(FAS_Circle(nullptr, {c0[i], dc + radii[i0]}));
            }
        }
        return ret;
    }
    // r^0 term
    mat[0][2]=0.5*(centers[2].squared()-centers[0].squared()+radii[0]*radii[0]-radii[2]*radii[2]);
    mat[1][2]=0.5*(centers[2].squared()-centers[1].squared()+radii[1]*radii[1]-radii[2]*radii[2]);
    std::vector<double> sm(2,0.);
    if(FAS_Math::linearSolver(mat,sm)==false){
        return ret;
    }

    FAS_Vector vp(sm[0],sm[1]);
    mat[0][2]= radii[0]-radii[2];
    mat[1][2]= radii[1]-radii[2];
    if(FAS_Math::linearSolver(mat,sm)==false){
        return ret;
    }
    FAS_Vector vq(sm[0],sm[1]);
    FAS_Vector dcp=vp-centers[0];
    double a=vq.squared()-1.;
    if(fabs(a)<FAS_TOLERANCE*1e-4) {
        return ret;
    }
    std::vector<double> ce(0,0.);
    ce.push_back(2.*(dcp.dotP(vq)-radii[0])/a);
    ce.push_back((dcp.squared()-radii[0]*radii[0])/a);
    std::vector<double>&& vr=FAS_Math::quadraticSolver(ce);
    for(size_t i=0; i < vr.size();i++){
        if(vr.at(i)<FAS_TOLERANCE) continue;
        ret.emplace_back(FAS_Circle(nullptr, {vp+vq*vr.at(i),fabs(vr.at(i))}));
    }

    return ret;
}

FAS_VectorSolutions FAS_Circle::getRefPoints() const
{
    FAS_Vector v1(data.radius, 0.0);
    FAS_Vector v2(0.0, data.radius);

    return FAS_VectorSolutions ({data.center,
                                 data.center+v1, data.center+v2,
                                 data.center-v1, data.center-v2});
}


// compute nearest endpoint, intersection with X/Y axis at 0, 90, 180 and 270 degree
FAS_Vector FAS_Circle::getNearestEndpoint(const FAS_Vector& coord, double* dist /*= nullptr*/) const
{
    return getNearestMiddle( coord, dist, 0);
}

FAS_Vector FAS_Circle::getNearestPointOnEntity(const FAS_Vector& coord,
                                               bool /*onEntity*/, double* dist, FAS_Entity** entity)const {

    if (entity) {
        *entity = const_cast<FAS_Circle*>(this);
    }
    FAS_Vector vp(coord - data.center);
    double d(vp.magnitude());
    if( d < FAS_TOLERANCE ) return FAS_Vector(false);
    vp =data.center+vp*(data.radius/d);

    if(dist){
        *dist=coord.distanceTo(vp);
    }
    return vp;
}

/*
  *find the tangential points from a given point, i.e., the tangent lines should pass
  * the given point and tangential points
  */
FAS_VectorSolutions FAS_Circle::getTangentPoint(const FAS_Vector& point) const {
    FAS_VectorSolutions ret;
    double r2(getRadius()*getRadius());
    if(r2<FAS_TOLERANCE2) return ret; //circle too small
    FAS_Vector vp(point-getCenter());
    double c2(vp.squared());
    if(c2<r2-getRadius()*2.*FAS_TOLERANCE) {
        //inside point, no tangential point
        return ret;
    }
    if(c2>r2+getRadius()*2.*FAS_TOLERANCE) {
        //external point
        FAS_Vector vp1(-vp.y,vp.x);
        vp1*=getRadius()*sqrt(c2-r2)/c2;
        vp *= r2/c2;
        vp += getCenter();
        if(vp1.squared()>FAS_TOLERANCE2) {
            ret.push_back(vp+vp1);
            ret.push_back(vp-vp1);
            return ret;
        }
    }
    ret.push_back(point);
    return ret;
}
FAS_Vector FAS_Circle::getTangentDirection(const FAS_Vector& point) const
{
    FAS_Vector vp(point-getCenter());
    return FAS_Vector(-vp.y,vp.x);

}

FAS_Vector FAS_Circle::getNearestCenter(const FAS_Vector& coord,
                                        double* dist) const{
    if (dist) {
        *dist = coord.distanceTo(data.center);
    }
    return data.center;
}

FAS_Vector FAS_Circle::getMiddlePoint(void)const
{
    return FAS_Vector(false);
}

// compute middlePoints for each quadrant of a circle
FAS_Vector FAS_Circle::getNearestMiddle(const FAS_Vector& coord,
                                        double* dist /*= nullptr*/,
                                        const int middlePoints /*= 1*/) const
{
    if( data.radius <= FAS_TOLERANCE) {
        //circle too short
        if ( nullptr != dist) {
            *dist = FAS_MAXDOUBLE;
        }
        return FAS_Vector(false);
    }

    FAS_Vector vPoint( getNearestPointOnEntity( coord, true, dist));
    int iCounts = middlePoints + 1;
    double dAngleSteps = M_PI_2 / iCounts;
    double dAngleToPoint = data.center.angleTo(vPoint);
    int iStepCount = static_cast<int>((dAngleToPoint + 0.5 * dAngleSteps) / dAngleSteps);
    if( 0 < middlePoints) {
        // for nearest middle eliminate start/endpoints
        int iQuadrant = static_cast<int>(dAngleToPoint / 0.5 / M_PI);
        int iQuadrantStep = iStepCount - iQuadrant * iCounts;
        if( 0 == iQuadrantStep) {
            ++iStepCount;
        }
        else if( iCounts == iQuadrantStep) {
            --iStepCount;
        }
    }

    vPoint.setPolar( data.radius, dAngleSteps * iStepCount);
    vPoint.move( data.center);

    if(dist) {
        *dist = vPoint.distanceTo( coord);
    }

    return vPoint;
}


FAS_Vector FAS_Circle::getNearestDist(double /*distance*/,
                                      const FAS_Vector& /*coord*/,
                                      double* dist) const{

    if (dist) {
        *dist = FAS_MAXDOUBLE;
    }
    return FAS_Vector(false);
}

FAS_Vector FAS_Circle::getNearestDist(double /*distance*/,
                                      bool /*startp*/) const{

    return FAS_Vector(false);
}

FAS_Vector FAS_Circle::getNearestOrthTan(const FAS_Vector& coord,
                                         const FAS_Line& normal,
                                         bool /*onEntity = false*/) const
{
    if ( !coord.valid) {
        return FAS_Vector(false);
    }
    FAS_Vector vp0(coord-getCenter());
    FAS_Vector vp1(normal.getAngle1());
    double d=FAS_Vector::dotP(vp0,vp1);
    if(d >= 0. ) {
        return getCenter() + vp1*getRadius();
    }else{
        return getCenter() - vp1*getRadius();
    }
}

void FAS_Circle::move(const FAS_Vector& offset) {
    data.center.move(offset);
    moveBorders(offset);
}

// this function creates offset
bool FAS_Circle::offset(const FAS_Vector& coord, const double& distance) {
    double r0(coord.distanceTo(getCenter()));
    if(r0 > getRadius()){
        //external
        r0 = getRadius()+ fabs(distance);
    }else{
        r0 = getRadius()- fabs(distance);
        if(r0<FAS_TOLERANCE) {
            return false;
        }
    }
    setRadius(r0);
    calculateBorders();
    return true;
}

void FAS_Circle::rotate(const FAS_Vector& center, const double& angle) {
    data.center.rotate(center, angle);
    calculateBorders();
}

void FAS_Circle::rotate(const FAS_Vector& center, const FAS_Vector& angleVector) {
    data.center.rotate(center, angleVector);
    calculateBorders();
}

void FAS_Circle::scale(const FAS_Vector& center, const FAS_Vector& factor) {
    data.center.scale(center, factor);
    data.radius *= fabs(factor.x);
    scaleBorders(center,factor);
}

double FAS_Circle::getDirection1() const{
    return M_PI_2;
}

double FAS_Circle::getDirection2() const{
    return M_PI_2*3.0;
}

void FAS_Circle::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) {
    data.center.mirror(axisPoint1, axisPoint2);
    calculateBorders();
}


/** whether the entity's bounding box intersects with visible portion of graphic view
//fix me, need to handle overlay container separately
*/
bool FAS_Circle::isVisibleInWindow(FAS_GraphicView* view) const
{

    FAS_Vector vpMin(view->toGraph(0,view->getHeight()));
    FAS_Vector vpMax(view->toGraph(view->getWidth(),0));
    QPolygonF visualBox(QRectF(vpMin.x,vpMin.y,vpMax.x-vpMin.x, vpMax.y-vpMin.y));
    std::vector<FAS_Vector> vps;
    for(unsigned short i=0;i<4;i++){
        const QPointF& vp(visualBox.at(i));
        vps.emplace_back(vp.x(),vp.y());
    }
    for(unsigned short i=0;i<4;i++){
        FAS_Line line{nullptr, {vps.at(i),vps.at((i+1)%4)}};
        FAS_Circle c0{nullptr, getData()};
        if( FAS_Information::getIntersection(&c0, &line, true).size()>0) return true;
    }
    if( getCenter().isInWindowOrdered(vpMin,vpMax)==false) return false;
    return (vpMin-getCenter()).squared() > getRadius()*getRadius();
}

/** draw circle as a 2 pi arc */
void FAS_Circle::draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) {
    COMMONDEF->drawEntityCount++;
    FAS_Arc arc(getParent(), FAS_ArcData(getCenter(),getRadius(),0.,2.*M_PI, false));
    arc.setSelected(isSelected());
    arc.setPen(getPen());
    arc.draw(painter,view,patternOffset);
}

//void FAS_Circle::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& insertData){

//    if (! (painter && view))
//    {
//        return;
//    }

//    resetBasicData();
//    if (fabs(insertData.scaleFactor.x)>1.0e-6 && fabs(insertData.scaleFactor.y)>1.0e-6)
//    {

//        this->moveEntityBoarder(insertData.insertionPoint +
//                             FAS_Vector(insertData.spacing.x/insertData.scaleFactor.x*1, insertData.spacing.y/insertData.scaleFactor.y*1),insertData.insertionPoint, insertData.angle,insertData.scaleFactor);            }
//    else
//    {
//        this->moveEntityBoarder(insertData.insertionPoint,insertData.insertionPoint, insertData.angle,insertData.scaleFactor);
//    }


// draw(painter,view,patternOffset);

//}
void FAS_Circle::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center1, const double& angle,const FAS_Vector& factor){


    FAS_Vector center=getCenter();
    double radius=getRadius();
    FAS_Vector bkcenter=data.bkcenter;
    double bkradius=data.bkradius;
    if(!bkcenter.valid){
        data.bkcenter=center;
        data.bkradius=radius;
    }else{
        data.center=data.bkcenter;
        data.radius=data.bkradius;
    }
}

void FAS_Circle::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){
        data.center.move(offset);
        data.center.rotate(center, angle);
        data.center.scale(center, factor);
        data.radius *= fabs(factor.x);
}


void FAS_Circle::moveRef(const FAS_Vector& ref, const FAS_Vector& offset) {
    if(ref.distanceTo(data.center)<1.0e-4){
        data.center += offset;
        return;
    }
    FAS_Vector v1(data.radius, 0.0);
    FAS_VectorSolutions sol;
    sol.push_back(data.center + v1);
    sol.push_back(data.center - v1);
    v1.set(0., data.radius);
    sol.push_back(data.center + v1);
    sol.push_back(data.center - v1);
    double dist;
    v1=sol.getClosest(ref,&dist);
    if(dist>1.0e-4) return;
    data.radius = data.center.distanceTo(v1 + offset);
}


/** return the equation of the entity
for quadratic,

return a vector contains:
m0 x^2 + m1 xy + m2 y^2 + m3 x + m4 y + m5 =0

for linear:
m0 x + m1 y + m2 =0
**/
LC_Quadratic FAS_Circle::getQuadratic() const
{
    std::vector<double> ce(6,0.);
    ce[0]=1.;
    ce[2]=1.;
    ce[5]=-data.radius*data.radius;
    LC_Quadratic ret(ce);
    ret.move(data.center);
    return ret;
}


/**
* @brief Returns area of full circle
* Note: Circular arcs are handled separately by FAS_Arc (areaLIneIntegral) 
* However, full ellipses and ellipse arcs are handled by FAS_Ellipse
* @return \pi r^2
*/
double FAS_Circle::areaLineIntegral() const
{
    const double r = getRadius();

    return M_PI*r*r;
}

std::ostream& operator << (std::ostream& os, const FAS_Circle& a) {
    os << " Circle: " << a.data << "\n";
    return os;
}

