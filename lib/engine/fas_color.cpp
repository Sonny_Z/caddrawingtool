/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_color.h"

//sort with alphabetical order
#include <iostream>

std::ostream& operator << (std::ostream& os, const FAS_Color& c) {
       os << " color: " << c.name().toLatin1().data()
       << " flags: " << (c.getFlag(FAS2::FlagByLayer) ? "FAS2::FlagByLayer " : "")
       << (c.getFlag(FAS2::FlagByBlock) ? "FAS2::FlagByBlock " : "");
       return os;
}
