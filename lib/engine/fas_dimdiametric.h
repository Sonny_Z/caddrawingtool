/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_DIMDIAMETER_H
#define FAS_DIMDIAMETER_H

#include "fas_dimension.h"

// Holds the data that defines a diametric dimension entity.
struct FAS_DimDiametricData {
    FAS_DimDiametricData();
    FAS_DimDiametricData(const FAS_Vector& definitionPoint, double leader);

    // Definition point.
    FAS_Vector definitionPoint;
    // Leader length.
    double leader;
};

std::ostream& operator << (std::ostream& os, const FAS_DimDiametricData& dd);

class FAS_DimDiametric : public FAS_Dimension
{
public:
    FAS_DimDiametric(FAS_EntityContainer* parent,
                     const FAS_DimensionData& d,
                     const FAS_DimDiametricData& ed);

    FAS_Entity* clone() const override;

    //return FAS2::EntityDimDiametric
    FAS2::EntityType rtti() const override{
        return FAS2::EntityDimDiametric;
    }

    FAS_DimDiametricData getEData() const {
        return edata;
    }

    FAS_VectorSolutions getRefPoints() const override;

    QString getMeasuredLabel() override;

    void updateDim(bool autoText=false) override;

    FAS_Vector getDefinitionPoint() {
        return edata.definitionPoint;
    }
    double getLeader() {
        return edata.leader;
    }
    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;

    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;

    friend std::ostream& operator << (std::ostream& os,
                                      const FAS_DimDiametric& d);

protected:
    // Extended data.
    FAS_DimDiametricData edata;
};

#endif
