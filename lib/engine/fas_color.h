/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_COLOR_H
#define FAS_COLOR_H

#include <QColor>

#include "fas.h"
#include "fas_flags.h"

/**
 * Color class.
 */
class FAS_Color: public QColor, public FAS_Flags {
public:
    FAS_Color() : QColor(), FAS_Flags() {}
    FAS_Color(int r, int g, int b) : QColor(r, g, b), FAS_Flags() {}
    FAS_Color(int r, int g, int b, int a) : QColor(r, g, b, a), FAS_Flags() {}
    FAS_Color(const QColor& c) : QColor(c), FAS_Flags() {}
    FAS_Color(const FAS_Color& c) : QColor(c), FAS_Flags()
    {
        setFlags(c.getFlags());
    }
    FAS_Color(unsigned int f) : QColor(), FAS_Flags(f) {}
    FAS_Color(QString name) : QColor(name), FAS_Flags() {}

    //return A copy of this color without flags.
    FAS_Color stripFlags() const
    {
        return FAS_Color(red(), green(), blue());
    }

    //return true if the color is defined by layer.
    bool isByLayer() const
    {
        return getFlag(FAS2::FlagByLayer);
    }

    // return true if the color is defined by block.
    bool isByBlock() const
    {
        return getFlag(FAS2::FlagByBlock);
    }

    QColor toQColor(void) const {
        QColor c0;
        c0.setRgb(red(),green(),blue());
        return c0;
    }

    FAS_Color& operator = (const FAS_Color& c) {
        setRgb(c.red(), c.green(), c.blue());
        setFlags(c.getFlags());

        return *this;
    }

    bool operator == (const FAS_Color& c) const {
        return (red()==c.red() &&
                green()==c.green() &&
                blue()==c.blue() &&
                getFlags()==c.getFlags());
    }

    friend std::ostream& operator << (std::ostream& os, const FAS_Color& c);
};

#endif

