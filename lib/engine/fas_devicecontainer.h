/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_DEVICECONTAINER_H
#define FAS_DEVICECONTAINER_H

#include "deviceinfo.h"
#include "fas_atomicentity.h"
#include "fas_entitycontainer.h"

class FAS_BlockList;

// Holds the data that defines an insert.
struct FAS_DeviceContainerData {
    FAS_DeviceContainerData() {}
    FAS_DeviceContainerData(const QString& name,
                   FAS_Vector insertionPoint,
                   FAS_Vector scaleFactor,
                   double angle,
                   int cols, int rows, FAS_Vector spacing,
                   FAS_BlockList* blockSource = nullptr,
                   FAS2::UpdateMode updateMode = FAS2::Update);

    QString name;
    FAS_Vector insertionPoint;
    FAS_Vector scaleFactor;
    double angle;
    int cols, rows;
    FAS_Vector spacing;
    FAS_BlockList* blockSource;
    FAS2::UpdateMode updateMode;
    DeviceInfo* deviceInfo = nullptr;
};

std::ostream& operator << (std::ostream& os, const FAS_DeviceContainerData& d);

/*
 * An insert inserts a block into the drawing at a certain location
 * with certain attributes (angle, scale, ...).
 * Inserts don't really contain other entities internally. They just
 * refer to a block. However, to the outside world they act exactly
 * like EntityContainer.
 */
class FAS_DeviceContainer : public FAS_AtomicEntity , public FAS_EntityContainer
{
public:
    FAS_DeviceContainer(FAS_EntityContainer* parent, const FAS_DeviceContainerData& d);
    virtual ~FAS_DeviceContainer();

    virtual FAS_Entity* clone() const;

    // return FAS2::EntityInsert */
    virtual FAS2::EntityType rtti() const
    {
        return FAS2::EntityInsert;
    }

    // return Copy of data that defines the insert.
    FAS_DeviceContainerData getData() const
    {
        return data;
    }
    virtual void reparent(FAS_EntityContainer* parent)
    {
        FAS_EntityContainer::reparent(parent);
        block = nullptr;
    }
    FAS_Block* getBlockForInsert() const;
    virtual void update();
    QString getName() const
    {
        return data.name;
    }
    void setName(const QString& newName)
    {
        data.name = newName;
        update();
    }
    FAS_Vector getInsertionPoint() const
    {
        return data.insertionPoint;
    }
    void setInsertionPoint(const FAS_Vector& i)
    {
        data.insertionPoint = i;
    }
    FAS_Vector getScale() const
    {
        return data.scaleFactor;
    }
    void setScale(const FAS_Vector& s)
    {
        data.scaleFactor = s;
    }
    double getAngle() const
    {
        return data.angle;
    }
    void setAngle(double a)
    {
        data.angle = a;
    }
    int getCols() const
    {
        return data.cols;
    }
    void setCols(int c)
    {
        data.cols = c;
    }
    int getRows() const
    {
        return data.rows;
    }
    void setRows(int r)
    {
        data.rows = r;
    }
    FAS_Vector getSpacing() const
    {
        return data.spacing;
    }
    void setSpacing(const FAS_Vector& s)
    {
        data.spacing = s;
    }

    virtual bool isVisible() const;

    virtual FAS_VectorSolutions getRefPoints() const;
    virtual FAS_Vector getNearestRef(const FAS_Vector& coord, double* dist = nullptr) const;

    virtual void move(const FAS_Vector& offset);
    virtual void rotate(const FAS_Vector& center, const double& angle);
    virtual void rotate(const FAS_Vector& center, const FAS_Vector& angleVector);
    virtual void scale(const FAS_Vector& center, const FAS_Vector& factor);
    virtual void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2);

    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;

    friend std::ostream& operator << (std::ostream& os, const FAS_DeviceContainer& i);

    void setDeviceInfo(DeviceInfo* type);
    DeviceInfo* getDeviceInfo() const;

protected:
    FAS_DeviceContainerData data;
    mutable FAS_Block* block;
};


#endif
