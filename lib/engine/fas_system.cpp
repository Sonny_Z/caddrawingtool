/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

//sort with alphabetical order
#include <iostream>
#include <QMap>
#include <QApplication>
#include <QTextCodec>
#include <QTranslator>
#include <QFileInfo>

#include "fas.h"
#include "fas_system.h"

#if QT_VERSION >= 0x050000
#include <QStandardPaths>
#else
#include <QDesktopServices>
#endif

FAS_System* FAS_System::uniqueInstance = nullptr;

void FAS_System::init(const QString& appName, const QString& appVersion,
                      const QString& appDirName, const QString& appDir)
{
    this->appName = appName;
    this->appVersion = appVersion;
    this->appDirName = appDirName;
    if (appDir=="")
    {
        this->appDir = QDir::currentPath();
    }
    else
    {
        this->appDir = appDir;
    }
    initialized = true;
}

// Checks if the system has been initialized and prints a warning otherwise to stderr.
bool FAS_System::checkInit()
{
    return initialized;
}

// Creates all given directories in the user's home.
bool FAS_System::createPaths(const QString& directory)
{
    QDir dir;
    dir.cd(QDir::homePath());
    dir.mkpath(directory);
    return true;
}

//return List of the absolute paths of the files found.
QStringList FAS_System::getFileList(const QString& subDirectory, const QString& fileExtension)
{
    checkInit();
    QStringList dirList = getDirectoryList(subDirectory);
    QStringList fileList;
    QString path;
    QDir dir;
    for (QStringList::Iterator it = dirList.begin(); it!=dirList.end(); ++it)
    {
        path = QString(*it);
        dir = QDir(path);
        if (dir.exists() && dir.isReadable())
        {
            QStringList files = dir.entryList( QStringList("*." + fileExtension) );
            for (QStringList::Iterator it2 = files.begin(); it2!=files.end(); it2++)
            {
                fileList += path + "/" + (*it2);
            }
        }
    }
    return fileList;
}

//List of all directories in subdirectory 'subDirectory' in all possible QCad directories.
QStringList FAS_System::getDirectoryList(const QString& _subDirectory)
{
    QStringList dirList;
    QString subDirectory=QDir::fromNativeSeparators(_subDirectory);

#ifdef Q_OS_MAC
#if QT_VERSION >= 0x050000
    dirList.append(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/" + appDirName + "/" + subDirectory);
#else
    dirList.append(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/" + appDirName + "/" + subDirectory);
#endif
#endif // Q_OS_MAC

#ifdef Q_OS_WIN32
#if QT_VERSION >= 0x050000
    dirList.append(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/" + appDirName + "/" + subDirectory);
#else
    dirList.append(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/" + appDirName + "/" + subDirectory);
#endif
#endif // Q_OS_WIN32

    // Unix home directory, it's old style but some people might have stuff there.
    dirList.append(getHomeDir() + "/." + appDirName + "/" + subDirectory);
    //local (application) directory has priority over other dirs:
    if (!appDir.isEmpty() && appDir!="/" && appDir!=getHomeDir()) {
        dirList.append(appDir + "/" + subDirectory);
    }

    // Ubuntu
    dirList.append("/usr/share/doc/" + appDirName + "/" + subDirectory);

    // Redhat style:
    dirList.append("/usr/share/" + appDirName + "/" + subDirectory);

    QStringList ret;
    for (QStringList::Iterator it = dirList.begin(); it!=dirList.end(); ++it)
    {
        if (QFileInfo(*it).isDir())
        {
            ret += (*it);
        }
    }

    return ret;
}

/**
 * Tries to convert the given encoding string to an encoding Qt knows.
 */
QString FAS_System::getEncoding(const QString& str) {
    QString l=str.toLower();

    if (l=="latin1" || l=="ansi_1252" || l=="iso-8859-1" ||
            l=="cp819" || l=="csiso" || l=="ibm819" || l=="iso_8859-1" ||
            l=="iso8859-1" || l=="iso-ir-100" || l=="l1")
    {
        return "Latin1";
    }
    else if (l=="big5" || l=="ansi_950" || l=="cn-big5" || l=="csbig5" || l=="x-x-big5")
    {
        return "Big5";
    }
    else if (l=="big5-hkscs")
    {
        return "Big5-HKSCS";
    }
    else if (l=="eucjp" || l=="euc-jp" || l=="cseucpkdfmtjapanese" ||
             l=="x-euc" || l=="x-euc-jp")
    {
        return "eucJP";
    }
    else if (l=="euckr")
    {
        return "eucKR";
    }
    else if (l=="gb2312" || l=="chinese" || l=="cn-gb" ||
             l=="csgb2312" || l=="csgb231280" || l=="csiso58gb231280" ||
             l=="gb_2312-80" || l=="gb231280" || l=="gb2312-80" || l=="gbk" ||
             l=="iso-ir-58") {
        return "GB2312";
    }
    else if (l=="gbk")
    {
        return "GBK";
    }
    else if (l=="gb18030")
    {
        return "GB18030";
    }
    else if (l=="jis7")
    {
        return "JIS7";
    }
    else if (l=="shift-jis" || l=="ansi_932" || l=="shift_jis" || l=="csShiftJIS" ||
             l=="cswindows31j" || l=="ms_kanji" || l=="x-ms-cp932" || l=="x-sjis")
    {
        return "Shift-JIS";
    }
    else if (l=="tscii")
    {
        return "TSCII";
    }
    else if (l=="utf88-bit")
    {
        return "utf88-bit";
    }
    else if (l=="utf16")
    {
        return "utf16";
    }
    else if (l=="utf8" || l=="utf-8")
    {
        return "utf-8";
    }
    else if (l=="koi8-r")
    {
        return "KOI8-R";
    }
    else if (l=="koi8-u")
    {
        return "KOI8-U";
    }
    else if (l=="iso8859-1")
    {
        return "ISO8859-1";
    }
    else if (l=="iso8859-2")
    {
        return "ISO8859-2";
    }
    else if (l=="iso8859-3")
    {
        return "ISO8859-3";
    }
    else if (l=="iso8859-4" || l=="ansi_1257")
    {
        return "ISO8859-4";
    }
    else if (l=="iso8859-5")
    {
        return "ISO8859-5";
    }
    else if (l=="iso8859-6" || l=="ansi_1256")
    {
        return "ISO8859-6";
    }
    else if (l=="iso8859-7" || l=="ansi_1253")
    {
        return "ISO8859-7";
    }
    else if (l=="iso8859-8")
    {
        return "ISO8859-8";
    }
    else if (l=="iso8859-8-i" || l=="ansi_1255")
    {
        return "ISO8859-8-i";
    }
    else if (l=="iso8859-9" || l=="ansi_1254")
    {
        return "ISO8859-9";
    }
    else if (l=="iso8859-10")
    {
        return "ISO8859-10";
    }
    else if (l=="iso8859-13")
    {
        return "ISO8859-13";
    }
    else if (l=="iso8859-14")
    {
        return "ISO8859-14";
    }
    else if (l=="iso8859-15")
    {
        return "ISO8859-15";
    }
    else if (l=="ibm 850")
    {
        return "IBM 850";
    }
    else if (l=="ibm 866")
    {
        return "IBM 866";
    }
    else if (l=="cp874")
    {
        return "CP874";
    }
    else if (l=="cp1250")
    {
        return "CP1250";
    }
    else if (l=="cp1251" || l=="ansi_1251")
    {
        return "CP1251";
    }
    else if (l=="cp1252")
    {
        return "CP1252";
    }
    else if (l=="cp1253")
    {
        return "CP1253";
    }
    else if (l=="cp1254")
    {
        return "CP1254";
    }
    else if (l=="cp1255")
    {
        return "CP1255";
    }
    else if (l=="cp1256")
    {
        return "CP1256";
    }
    else if (l=="cp1257")
    {
        return "CP1257";
    }
    else if (l=="cp1258")
    {
        return "CP1258";
    }
    else if (l=="apple roman")
    {
        return "Apple Roman";
    }
    else if (l=="tis-620")
    {
        return "TIS-620";
    }

    return "latin1";
}

static QMap<QByteArray,QByteArray> loc_map;

QByteArray FAS_System::localeToISO(const QByteArray& locale)
{
    if (loc_map.isEmpty())
    {
        loc_map["croatian"]="ISO8859-2";
        loc_map["cs"]="ISO8859-2";
        loc_map["cs_CS"]="ISO8859-2";
        loc_map["cs_CZ"]="ISO8859-2";
        loc_map["cz"]="ISO8859-2";
        loc_map["cz_CZ"]="ISO8859-2";
        loc_map["czech"]="ISO8859-2";
        loc_map["hr"]="ISO8859-2";
        loc_map["hr_HR"]="ISO8859-2";
        loc_map["hu"]="ISO8859-2";
        loc_map["hu_HU"]="ISO8859-2";
        loc_map["hungarian"]="ISO8859-2";
        loc_map["pl"]="ISO8859-2";
        loc_map["pl_PL"]="ISO8859-2";
        loc_map["polish"]="ISO8859-2";
        loc_map["ro"]="ISO8859-2";
        loc_map["ro_RO"]="ISO8859-2";
        loc_map["rumanian"]="ISO8859-2";
        loc_map["serbocroatian"]="ISO8859-2";
        loc_map["sh"]="ISO8859-2";
        loc_map["sh_SP"]="ISO8859-2";
        loc_map["sh_YU"]="ISO8859-2";
        loc_map["sk"]="ISO8859-2";
        loc_map["sk_SK"]="ISO8859-2";
        loc_map["sl"]="ISO8859-2";
        loc_map["sl_CS"]="ISO8859-2";
        loc_map["sl_SI"]="ISO8859-2";
        loc_map["slovak"]="ISO8859-2";
        loc_map["slovene"]="ISO8859-2";
        loc_map["sr_SP"]="ISO8859-2";

        loc_map["eo"]="ISO8859-3";

        loc_map["ee"]="ISO8859-4";
        loc_map["ee_EE"]="ISO8859-4";

        loc_map["mk"]="ISO8859-5";
        loc_map["mk_MK"]="ISO8859-5";
        loc_map["sp"]="ISO8859-5";
        loc_map["sp_YU"]="ISO8859-5";

        loc_map["ar_AA"]="ISO8859-6";
        loc_map["ar_SA"]="ISO8859-6";
        loc_map["arabic"]="ISO8859-6";

        loc_map["el"]="ISO8859-7";
        loc_map["el_GR"]="ISO8859-7";
        loc_map["greek"]="ISO8859-7";

        loc_map["hebrew"]="ISO8859-8";
        loc_map["he"]="ISO8859-8";
        loc_map["he_IL"]="ISO8859-8";
        loc_map["iw"]="ISO8859-8";
        loc_map["iw_IL"]="ISO8859-8";

        loc_map["tr"]="ISO8859-9";
        loc_map["tr_TR"]="ISO8859-9";
        loc_map["turkish"]="ISO8859-9";

        loc_map["lt"]="ISO8859-13";
        loc_map["lt_LT"]="ISO8859-13";
        loc_map["lv"]="ISO8859-13";
        loc_map["lv_LV"]="ISO8859-13";

        loc_map["et"]="ISO8859-15";
        loc_map["et_EE"]="ISO8859-15";
        loc_map["br_FR"]="ISO8859-15";
        loc_map["ca_ES"]="ISO8859-15";
        loc_map["de"]="ISO8859-15";
        loc_map["de_AT"]="ISO8859-15";
        loc_map["de_BE"]="ISO8859-15";
        loc_map["de_DE"]="ISO8859-15";
        loc_map["de_LU"]="ISO8859-15";
        loc_map["en_IE"]="ISO8859-15";
        loc_map["es"]="ISO8859-15";
        loc_map["es_EC"]="ISO8859-15";
        loc_map["es_ES"]="ISO8859-15";
        loc_map["eu_ES"]="ISO8859-15";
        loc_map["fi"]="ISO8859-15";
        loc_map["fi_FI"]="ISO8859-15";
        loc_map["finnish"]="ISO8859-15";
        loc_map["fr"]="ISO8859-15";
        loc_map["fr_FR"]="ISO8859-15";
        loc_map["fr_BE"]="ISO8859-15";
        loc_map["fr_LU"]="ISO8859-15";
        loc_map["french"]="ISO8859-15";
        loc_map["ga_IE"]="ISO8859-15";
        loc_map["gl_ES"]="ISO8859-15";
        loc_map["it"]="ISO8859-15";
        loc_map["it_IT"]="ISO8859-15";
        loc_map["oc_FR"]="ISO8859-15";
        loc_map["nl"]="ISO8859-15";
        loc_map["nl_BE"]="ISO8859-15";
        loc_map["nl_NL"]="ISO8859-15";
        loc_map["pt"]="ISO8859-15";
        loc_map["pt_PT"]="ISO8859-15";
        loc_map["sv_FI"]="ISO8859-15";
        loc_map["wa_BE"]="ISO8859-15";

        loc_map["uk"]="KOI8-U";
        loc_map["uk_UA"]="KOI8-U";
        loc_map["ru_YA"]="KOI8-U";
        loc_map["ukrainian"]="KOI8-U";

        loc_map["be"]="KOI8-R";
        loc_map["be_BY"]="KOI8-R";
        loc_map["bg"]="KOI8-R";
        loc_map["bg_BG"]="KOI8-R";
        loc_map["bulgarian"]="KOI8-R";
        loc_map["ba_RU"]="KOI8-R";
        loc_map["ky"]="KOI8-R";
        loc_map["ky_KG"]="KOI8-R";
        loc_map["kk"]="KOI8-R";
        loc_map["kk_KZ"]="KOI8-R";
    }
    QByteArray l = loc_map[locale];
    if (l.isEmpty())
        return "ISO8859-1";
    return l;
}
