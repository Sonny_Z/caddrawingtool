/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_SOLID_H
#define FAS_SOLID_H

#include <array>
#include "fas_atomicentity.h"
#include "fas_vector.h"

// Holds the data that defines a solid.
struct FAS_SolidData {
    FAS_SolidData();
    FAS_SolidData(const FAS_Vector& corner1,
                  const FAS_Vector& corner2,
                  const FAS_Vector& corner3);

    // Constructor for a solid with 4 corners.
    FAS_SolidData(const FAS_Vector& corner1,
                  const FAS_Vector& corner2,
                  const FAS_Vector& corner3,
                  const FAS_Vector& corner4);

    std::array<FAS_Vector, 4> corner;
    std::array<FAS_Vector, 4> bkcorner;
    FAS_Vector bkBasicData;
};


std::ostream& operator << (std::ostream& os, const FAS_SolidData& pd);

// Class for a solid entity (e.g. dimension arrows).
class FAS_Solid : public FAS_AtomicEntity {
public:
    FAS_Solid(FAS_EntityContainer* parent,
              const FAS_SolidData& d);

    FAS_Entity* clone() const override;

    FAS2::EntityType rtti() const  override{
        return FAS2::EntitySolid;
    }

    FAS_SolidData const& getData() const {
        return data;
    }

    bool isTriangle() const{
        return !data.corner[3].valid;
    }

    FAS_Vector getCorner(int num) const;

    void shapeArrow(const FAS_Vector& point,
                    double angle,
                    double arrowSize);

    FAS_Vector getNearestEndpoint(const FAS_Vector& coord,
                                  double* dist = nullptr)const override;
    FAS_Vector getNearestPointOnEntity(const FAS_Vector& coord,
                                       bool onEntity = true, double* dist = nullptr, FAS_Entity** entity = nullptr)const override;
    FAS_Vector getNearestCenter(const FAS_Vector& coord,
                                double* dist = nullptr) const override;
    FAS_Vector getNearestMiddle(const FAS_Vector& coord,
                                double* dist = nullptr,
                                int middlePoints = 1)const override;
    FAS_Vector getNearestDist(double distance,
                              const FAS_Vector& coord,
                              double* dist = nullptr)const override;

    double getDistanceToPoint(const FAS_Vector& coord,
                              FAS_Entity** entity=nullptr,
                              FAS2::ResolveLevel level=FAS2::ResolveNone,
                              double solidDist = FAS_MAXDOUBLE)const override;

    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;

    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;

    friend std::ostream& operator << (std::ostream& os, const FAS_Solid& p);

    // Recalculates the borders of this entity.
    void calculateBorders() override;

    // Check if is intersected by v1, v2 window.
    bool isInCrossWindow(const FAS_Vector& v1,const FAS_Vector& v2)const;
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
protected:
    FAS_SolidData data;

private:
    //helper method for getNearestPointOnEntity
    bool sign (const FAS_Vector& v1, const FAS_Vector& v2, const FAS_Vector& v3) const;
}
;

#endif
