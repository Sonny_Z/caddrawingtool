/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_entity.h"

//sort with alphabetical order
#include "fas_arc.h"
#include "fas_block.h"
#include "fas_circle.h"
#include "fas_ellipse.h"
#include "fas_graphic.h"
#include "fas_graphicview.h"
#include "fas_insert.h"
#include "fas_information.h"
#include "fas_layer.h"
#include "fas_line.h"
#include "fas_mtext.h"
#include "fas_point.h"
#include "fas_polyline.h"
#include "fas_text.h"
#include "fas_vector.h"
#include "fas_units.h"
#include "lc_quadratic.h"

#include <iostream>
#include <QPolygon>
#include <QString>
#include <utility>

FAS_Entity::FAS_Entity(FAS_EntityContainer* parent) {

    this->parent = parent;
    init();
}

void FAS_Entity::init() {
    resetBorders();
    setFlag(FAS2::FlagVisible);
    //layer = nullptr;
    //pen = FAS_Pen();
    updateEnabled = true;
    setLayerToActive();
    setPenToActive();
    initId();
}

// Gives this entity a new unique id.
void FAS_Entity::initId() {
    static unsigned long int idCounter = 0;
    id = idCounter++;
}

// Resets the borders of this element.
void FAS_Entity::resetBorders() {
    double maxd = FAS_MAXDOUBLE;
    double mind = FAS_MINDOUBLE;

    minV.set(maxd, maxd);
    maxV.set(mind, mind);
}


void FAS_Entity::moveBorders(const FAS_Vector& offset){
    minV.move(offset);
    maxV.move(offset);
}
void FAS_Entity::scaleBorders(const FAS_Vector& center, const FAS_Vector& factor){
    minV.scale(center,factor);
    maxV.scale(center,factor);
}

// Selects or deselects this entity.
bool FAS_Entity::setSelected(bool select) {
    if (select)
    {
        setFlag(FAS2::FlagSelected);
    }
    else
    {
        delFlag(FAS2::FlagSelected);
    }
    return true;
}

// Toggles select on this entity.
bool FAS_Entity::toggleSelected()
{
    return setSelected(!isSelected());
}

/*
 * return True if the entity is selected. Note that an entity might
 * not be selected but one of its parents is selected. In that case
 * this function returns false.
 */
bool FAS_Entity::isSelected() const {
    //bug 557, Selected entities in invisible layers are deleted
    return isVisible() && getFlag(FAS2::FlagSelected);
}

// return true if a parent entity of this entity is selected.
bool FAS_Entity::isParentSelected() const
{
    FAS_Entity const* p = this;
    while(p)
    {
        p = p->getParent();
        if (p && p->isSelected()==true)
        {
            return true;
        }
    }

    return false;
}

/*
 * Sets or resets the processed flag of this entity.
 * param on True to set, false to reset.
 */
void FAS_Entity::setProcessed(bool on)
{
    if (on) {
        setFlag(FAS2::FlagProcessed);
    } else {
        delFlag(FAS2::FlagProcessed);
    }
}

//return True if the processed flag is set.
bool FAS_Entity::isProcessed() const
{
    return getFlag(FAS2::FlagProcessed);
}

/*
 * Called when the undo state changed.
 * param undone true: entity has become invisible.
 *               false: entity has become visible.
 */
void FAS_Entity::undoStateChanged(bool /*undone*/)
{
    setSelected(false);
    update();
}

// return true if this entity or any parent entities are undone.
bool FAS_Entity::isUndone() const
{
    if (!parent)
    {
        return FAS_Undoable::isUndone();
    }
    else
    {
        return FAS_Undoable::isUndone() || parent->isUndone();
    }
}


//return True if the entity is in the given range.
bool FAS_Entity::isInWindow(FAS_Vector v1, FAS_Vector v2) const
{
    double right, left, top, bottom;

    right = std::max(v1.x, v2.x);
    left = std::min(v1.x, v2.x);
    top = std::max(v1.y, v2.y);
    bottom = std::min(v1.y, v2.y);

    return (getMin().x>=left &&
            getMax().x<=right &&
            getMin().y>=bottom &&
            getMax().y<=top);
}

double FAS_Entity::areaLineIntegral() const
{
    return 0.;
}

bool FAS_Entity::isArc() const
{
    switch (rtti())
    {
    case FAS2::EntityArc:
    case FAS2::EntityCircle:
        //ellipse implements its own test
    case FAS2::EntityEllipse:
        return true;
    default:
        return false;
    }
}

bool FAS_Entity::isArcCircleLine() const
{
    switch (rtti())
    {
    case FAS2::EntityArc:
    case FAS2::EntityCircle:
    case FAS2::EntityLine:
    case FAS2::EntityPoint:
        return true;
    default:
        return false;
    }
}

// whether the entity's bounding box intersects with visible portion of graphic view
bool FAS_Entity::isVisibleInWindow(FAS_GraphicView* view) const
{
    FAS_Vector vpMin(view->toGraph(0,view->getHeight()));
    FAS_Vector vpMax(view->toGraph(view->getWidth(),0));
    if( getStartpoint().isInWindowOrdered(vpMin, vpMax) )
        return true;
    if( getEndpoint().isInWindowOrdered(vpMin, vpMax) )
        return true;
    QPolygonF visualBox(QRectF(vpMin.x,vpMin.y,vpMax.x-vpMin.x, vpMax.y-vpMin.y));
    std::vector<FAS_Vector> vps;
    for(unsigned short i=0;i<4;i++)
    {
        const QPointF& vp(visualBox.at(i));
        vps.emplace_back(vp.x(),vp.y());
    }
    for(unsigned short i=0;i<4;i++)
    {
        FAS_Line const line{vps.at(i),vps.at((i+1)%4)};
        if( FAS_Information::getIntersection(this, &line, true).size()>0)
            return true;
    }
    if( minV.isInWindowOrdered(vpMin,vpMax)||maxV.isInWindowOrdered(vpMin,vpMax))
        return true;
    return false;
}

bool FAS_Entity::isPointOnEntity(const FAS_Vector& coord, double tolerance) const
{
    double dist = getDistanceToPoint(coord, nullptr, FAS2::ResolveNone);
    return (dist<=fabs(tolerance));
}

double FAS_Entity::getDistanceToPoint(const FAS_Vector& coord,
                                      FAS_Entity** entity,
                                      FAS2::ResolveLevel /*level*/,
                                      double /*solidDist*/) const
{
    if (entity)
    {
        *entity=const_cast<FAS_Entity*>(this);
    }
    double dToEntity = FAS_MAXDOUBLE;
    getNearestPointOnEntity(coord, true, &dToEntity, entity);

    if(getCenter().valid)
    {
        double dToCenter=getCenter().distanceTo(coord);
        return std::min(dToEntity,dToCenter);
    }
    else
        return dToEntity;
}

/*
 * return true Only if the entity and the layer it is on are visible.
 * The Layer might also be nullptr. In that case the layer visiblity
 * is ignored.
 */
bool FAS_Entity::isVisible() const
{
    if (!getFlag(FAS2::FlagVisible))
    {
        return false;
    }
    if (isUndone())
    {
        return false;
    }
    if (!getLayer())
    {
        return true;
    }

    if( isDocument() && (rtti()==FAS2::EntityBlock || rtti()==FAS2::EntityInsert))
    {
        return true;
    }
    if (layer)
    {
        if (!layer->isPrint() || layer->isFrozen())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    if (!layer)
    {
        if (!getLayer())
        {
            return true;
        }
        else
        {
            if (!getLayer()->isFrozen())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    if (!getBlockOrInsert())
    {
        return true;
    }

    if (getBlockOrInsert()->rtti()==FAS2::EntityBlock)
    {
        return !(getLayer(false) && getLayer(false)->isFrozen());
    }


    if (!getBlockOrInsert()->getLayer())
    {
        return true;
    }

    if (!getBlockOrInsert()->getLayer()->isFrozen())
    {
        return true;
    }

    return false;
}

void FAS_Entity::setVisible(bool v)
{
    if (v)
    {
        setFlag(FAS2::FlagVisible);
    }
    else
    {
        delFlag(FAS2::FlagVisible);
    }
}

// Sets the highlight status of the entity. Highlighted entities usually indicate a feedback to a user action.
void FAS_Entity::setHighlighted(bool on)
{
    if (on)
    {
        setFlag(FAS2::FlagHighlighted);
    }
    else
    {
        delFlag(FAS2::FlagHighlighted);
    }
}

FAS_Vector FAS_Entity::getStartpoint() const
{
    return {};
}

FAS_Vector FAS_Entity::getEndpoint() const
{
    return {};
}

FAS_VectorSolutions FAS_Entity::getTangentPoint(const FAS_Vector& /*point*/) const
{
    return {};
}

FAS_Vector FAS_Entity::getTangentDirection(const FAS_Vector& /*point*/)const
{
    return {};
}

//return true if the entity is highlighted.
bool FAS_Entity::isHighlighted() const{
    return getFlag(FAS2::FlagHighlighted);
}


FAS_Vector FAS_Entity::getSize() const
{
    return maxV-minV;
}
double FAS_Entity::getLengthX() const
{
    return abs(maxV.x - minV.x);
}
double FAS_Entity::getLengthY() const
{
    return abs(maxV.y - minV.y);
}

// return true if the layer this entity is on is locked.
bool FAS_Entity::isLocked() const
{
    return getLayer(true) && getLayer()->isLocked();
}

FAS_Vector FAS_Entity::getCenter() const
{
    return FAS_Vector{};
}

double FAS_Entity::getRadius() const
{
    return FAS_MAXDOUBLE;
}

// return The parent graphic in which this entity is stored
FAS_Graphic* FAS_Entity::getGraphic() const
{
    if (rtti()==FAS2::EntityGraphic)
    {
        FAS_Graphic const* ret=static_cast<FAS_Graphic const*>(this);
        return const_cast<FAS_Graphic*>(ret);
    }
    else if (!parent)
    {
        return nullptr;
    }
    return parent->getGraphic();
}

//return The parent block in which this entity is stored
FAS_Block* FAS_Entity::getBlock() const
{
    if (rtti()==FAS2::EntityBlock)
    {
        FAS_Block const* ret=static_cast<FAS_Block const*>(this);
        return const_cast<FAS_Block*>(ret);
    }
    else if (!parent)
    {
        return nullptr;
    }
    return parent->getBlock();
}

LC_Quadratic FAS_Entity::getQuadratic() const
{
    return LC_Quadratic{};
}

//The parent insert in which this entity is stored
FAS_Insert* FAS_Entity::getInsert() const
{
    if (rtti()==FAS2::EntityInsert)
    {
        FAS_Insert const* ret=static_cast<FAS_Insert const*>(this);
        return const_cast<FAS_Insert*>(ret);
    }
    else if (!parent)
    {
        return nullptr;
    }
    else
    {
        return parent->getInsert();
    }
}

//return The parent block or insert in which this entity is stored
FAS_Entity* FAS_Entity::getBlockOrInsert() const
{
    FAS_Entity* ret{nullptr};
    switch(rtti())
    {
    case FAS2::EntityBlock:
    case FAS2::EntityInsert:
        ret=const_cast<FAS_Entity*>(this);
        break;
    default:
        if(parent)
        {
            return parent->getBlockOrInsert();
        }
        break;
    }
    return ret;
}

// return The parent document in which this entity is stored
FAS_Document* FAS_Entity::getDocument() const
{
    if (isDocument())
    {
        FAS_Document const* ret=static_cast<FAS_Document const*>(this);
        return const_cast<FAS_Document*>(ret);
    }
    else if (!parent)
    {
        return nullptr;
    }
    return parent->getDocument();
}

/*
 * Sets a variable value for the parent graphic object.
 * param key Variable name (e.g. "$DIMASZ")
 * param val Default value
 */
void FAS_Entity::addGraphicVariable(const QString& key, double val, int code)
{
    FAS_Graphic* graphic = getGraphic();
    if (graphic) {
        graphic->addVariable(key, val, code);
    }
}
/*
 * Sets a variable value for the parent graphic object.
 * param key Variable name (e.g. "$DIMASZ")
 * param val Default value
 */
void FAS_Entity::addGraphicVariable(const QString& key, int val, int code)
{
    FAS_Graphic* graphic = getGraphic();
    if (graphic) {
        graphic->addVariable(key, val, code);
    }
}

/*
 * Sets a variable value for the parent graphic object.
 * param key Variable name (e.g. "$DIMASZ")
 * param val Default value
 */
void FAS_Entity::addGraphicVariable(const QString& key, const QString& val, int code)
{
    FAS_Graphic* graphic = getGraphic();
    if (graphic) {
        graphic->addVariable(key, val, code);
    }
}

/*
 * A safe member function to return the given variable.
 * param key Variable name (e.g. "$DIMASZ")
 * param def Default value
 * return value of variable or default value if the given variable
 *    doesn't exist.
 */
double FAS_Entity::getGraphicVariableDouble(const QString& key, double def)
{
    FAS_Graphic* graphic = getGraphic();
    double ret=def;
    if (graphic) {
        ret = graphic->getVariableDouble(key, def);
    }
    return ret;
}

/*
 * A safe member function to return the given variable.
 * param key Variable name (e.g. "$DIMASZ")
 * param def Default value
 * return value of variable or default value if the given variable
 *    doesn't exist.
 */
int FAS_Entity::getGraphicVariableInt(const QString& key, int def) const
{
    FAS_Graphic* graphic = getGraphic();
    int ret=def;
    if (graphic) {
        ret = graphic->getVariableInt(key, def);
    }
    return ret;
}

/*
 * A safe member function to return the given variable.
 * param key Variable name (e.g. "$DIMASZ")
 * param def Default value
 * return value of variable or default value if the given variable
 *    doesn't exist.
 */
QString FAS_Entity::getGraphicVariableString(const QString& key, const QString&  def) const
{
    FAS_Graphic* graphic = getGraphic();
    QString ret=def;
    if (graphic) {
        ret = graphic->getVariableString(key, def);
    }
    return ret;
}

/*
 * return The unit the parent graphic works on or None if there's no
 * parent graphic.
 */
FAS2::Unit FAS_Entity::getGraphicUnit() const
{
    FAS_Graphic* graphic = getGraphic();
    FAS2::Unit ret = FAS2::None;
    if (graphic) {
        ret = graphic->getUnit();
    }
    return ret;
}

// Returns a pointer to the layer this entity is on or nullptr.
FAS_Layer* FAS_Entity::getLayer(bool resolve) const
{
    if (resolve)
    {
        // we have no layer but a parent that might have one.
        // return parent's layer instead:
        if (!layer /*|| layer->getName()=="ByBlock"*/)
        {
            if (parent)
            {
                return parent->getLayer(true);
            }
            else
            {
                return nullptr;
            }
        }
    }
    return layer;
}

// Sets the layer of this entity to the layer with the given name
void FAS_Entity::setLayer(const QString& name)
{
    FAS_Graphic* graphic = getGraphic();
    if (graphic)
    {
        layer = graphic->findLayer(name);
    }
    else
    {
        layer = nullptr;
    }
}

// Sets the layer of this entity to the layer given.
void FAS_Entity::setLayer(FAS_Layer* l)
{
    layer = l;
}

/*
 * Sets the layer of this entity to the current layer of
 * the graphic this entity is in. If this entity (and none
 * of its parents) are in a graphic the layer is set to nullptr.
 */
void FAS_Entity::setLayerToActive()
{
    FAS_Graphic* graphic = getGraphic();
    if (graphic)
    {
        layer = graphic->getActiveLayer();
    }
    else
    {
        layer = nullptr;
    }
}

/*
 * Gets the pen needed to draw this entity.
 * The attributes can also come from the layer this entity is on
 * if the flags are set accordingly.
 * param resolve true: Resolve the pen to a drawable pen (e.g. the pen
 *         from the layer or parent..)
 *         false: Don't resolve and return a pen or ByLayer, ByBlock,
 * return Pen for this entity.
 */
FAS_Pen FAS_Entity::getPen(bool resolve) const
{

    if (!resolve)
    {
        return pen;
    }
    else
    {
        FAS_Pen p = pen;
        FAS_Layer* l = getLayer(true);

        // use parental attributes (e.g. vertex of a polyline, block
        // entities when they are drawn in block documents):
        if (parent)
        {
            //if pen is invalid gets all from parent
            if (!p.isValid() )
            {
                p = parent->getPen();
            }
            //pen is valid, verify byBlock parts
            FAS_EntityContainer* ep = parent;
            //If parent is byblock check parent.parent (nested blocks)
            while (p.getColor().isByBlock())
            {
                if (ep)
                {
                    p.setColor(parent->getPen().getColor());
                    ep = ep->parent;
                }
                else
                    break;
            }
            ep = parent;
            while (p.getWidth()==FAS2::WidthByBlock)
            {
                if (ep)
                {
                    p.setWidth(parent->getPen().getWidth());
                    ep = ep->parent;
                }
                else
                    break;
            }
            ep = parent;
            while (p.getLineType()==FAS2::LineByBlock)
            {
                if (ep)
                {
                    p.setLineType(parent->getPen().getLineType());
                    ep = ep->parent;
                }
                else
                    break;
            }
        }
        // check byLayer attributes:
        if (l)
        {
            // use layer's color:
            if (p.getColor().isByLayer())
            {
                p.setColor(l->getPen().getColor());
            }

            // use layer's width:
            if (p.getWidth()==FAS2::WidthByLayer)
            {
                p.setWidth(l->getPen().getWidth());
            }

            // use layer's linetype:
            if (p.getLineType()==FAS2::LineByLayer)
            {
                p.setLineType(l->getPen().getLineType());
            }
        }
        return p;
    }
}



/*
 * Sets the pen of this entity to the current pen of
 * the graphic this entity is in. If this entity (and none
 * of its parents) are in a graphic the pen is not changed.
 */
void FAS_Entity::setPenToActive()
{
    FAS_Document* doc = getDocument();
    if (doc)
    {
        pen = doc->getActivePen();
    }

}

void FAS_Entity::stretch(const FAS_Vector& firstCorner,
                         const FAS_Vector& secondCorner,
                         const FAS_Vector& offset) {

    if (getMin().isInWindow(firstCorner, secondCorner) &&
            getMax().isInWindow(firstCorner, secondCorner))
    {
        move(offset);
    }
}



//return Factor for scaling the line styles considering the current paper scaling and the fact that styles are stored in Millimeter.
double FAS_Entity::getStyleFactor(FAS_GraphicView* view)
{
    double styleFactor = 1.0;
    if (!view) return styleFactor;
    if (view->isPrinting()==false && view->isDraftMode())
    {
        styleFactor = 1.0/view->getFactor().x;
    }
    else
    {
        // the factor caused by the unit:
        FAS2::Unit unit = FAS2::None;
        FAS_Graphic* g = getGraphic();
        if (g)
        {
            unit = g->getUnit();
            styleFactor = FAS_Units::convert(1.0, FAS2::Millimeter, unit);
        }
        if (((int)getPen(true).getWidth())>0)
        {
            styleFactor *= ((double)getPen(true).getWidth()/100.0);
        }
        else if (((int)getPen(true).getWidth())==0)
        {
            styleFactor *= 0.01;
        }
    }

    if (view->isPrinting() || view->isPrintPreview() || view->isDraftMode()==false)
    {
        FAS_Graphic* graphic = getGraphic();
        if (graphic && graphic->getPaperScale()>1.0e-6)
        {
            styleFactor /= graphic->getPaperScale();
        }
    }

    if (styleFactor*view->getFactor().x<0.2)
    {
        styleFactor = -1.0;
    }

    return styleFactor;
}


// return User defined variable connected to this entity or nullptr if not found.
QString FAS_Entity::getUserDefVar(const QString& key) const
{
    auto it=varList.find(key);
    if(it==varList.end()) return nullptr;
    return varList.at(key);
}

// return a line tangent to entity and orthogonal to the line (*normal)
FAS_Vector FAS_Entity::getNearestOrthTan(const FAS_Vector& /*coord*/,
                                         const FAS_Line& /*normal*/,
                                         bool /*onEntity = false*/) const
{
    return FAS_Vector(false);
}

// Add a user defined variable to this entity.
void FAS_Entity::setUserDefVar(QString key, QString val)
{
    varList.insert(std::make_pair(key, val));
}

// Deletes the given user defined variable.
void FAS_Entity::delUserDefVar(QString key)
{
    varList.erase(key);
}

// return A list of all keys connected to this entity.
std::vector<QString> FAS_Entity::getAllKeys() const
{
    std::vector<QString> ret(0);
    for(auto const& v: varList)
    {
        ret.push_back(v.first);
    }
    return ret;
}

// constructionLayer contains entities of infinite length, constructionLayer doesn't show up in print
bool FAS_Entity::isConstruction(bool typeCheck) const
{
    if(typeCheck &&  getParent() &&  rtti() != FAS2::EntityLine)
    {
        return false;
    }
    if (layer)
        return layer->isConstruction();
    return false;
}

// whether printing is enabled or disabled for the entity's layer
bool FAS_Entity::isPrint(void) const
{
    if (nullptr != layer)
        return layer->isPrint();
    return true;
}

bool FAS_Entity::trimmable() const
{
    switch(rtti())
    {
    case FAS2::EntityArc:
    case FAS2::EntityCircle:
    case FAS2::EntityEllipse:
    case FAS2::EntityLine:
    case FAS2::EntitySplinePoints:
        return true;
    default:
        return false;
    }
}

FAS_VectorSolutions FAS_Entity::getRefPoints() const
{
    return FAS_VectorSolutions();
}

FAS_Vector FAS_Entity::getNearestRef(const FAS_Vector& coord, double* dist) const
{
    FAS_VectorSolutions const&& s = getRefPoints();
    return s.getClosest(coord, dist);
}
void FAS_Entity::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& insertData)
{
    if (! (painter && view))
    {
        return;
    }
    if(!isVisible()){
        return;
    }

//    resetBasicData();
//    if (fabs(insertData.scaleFactor.x)>1.0e-6 && fabs(insertData.scaleFactor.y)>1.0e-6)
//    {

//        moveEntityBoarder(insertData.insertionPoint +
//                             FAS_Vector(insertData.spacing.x/insertData.scaleFactor.x*1, insertData.spacing.y/insertData.scaleFactor.y*1),insertData.insertionPoint, insertData.angle,insertData.scaleFactor);            }
//    else
//    {
//        moveEntityBoarder(insertData.insertionPoint,insertData.insertionPoint, insertData.angle,insertData.scaleFactor);
//    }


 draw(painter,view,patternOffset);

}
FAS_Vector FAS_Entity::getNearestSelectedRef(const FAS_Vector& coord,
                                             double* dist) const
{
    if (isSelected())
    {
        return getNearestRef(coord, dist);
    }
    else
    {
        return FAS_Vector(false);
    }
}

// Dumps the elements data to stdout.
std::ostream& operator << (std::ostream& os, FAS_Entity& e)
{
    return os;

    os << " {Entity id: " << e.id;
    if (e.parent) {
        os << " | parent id: " << e.parent->getId() << "\n";
    } else {
        os << " | no parent\n";
    }

    os << " flags: " << (e.getFlag(FAS2::FlagVisible) ? "FAS2::FlagVisible" : "");
    os << (e.getFlag(FAS2::FlagUndone) ? " FAS2::FlagUndone" : "");
    os << (e.getFlag(FAS2::FlagSelected) ? " FAS2::FlagSelected" : "");
    os << "\n";

    if (!e.layer) {
        os << " layer: nullptr ";
    } else {
        os << " layer: " << e.layer->getName().toLatin1().data() << " ";
        os << " layer address: " << e.layer << " ";
    }

    os << e.pen << "\n";

    os << "variable list:\n";
    for(auto const& v: e.varList){
        os << v.first.toLatin1().data()<< ": "
           << v.second.toLatin1().data()
           << ", ";
    }

    // There should be a better way then this...
    switch(e.rtti()) {
    case FAS2::EntityPoint:
        os << (FAS_Point&)e;
        break;

    case FAS2::EntityLine:
        os << (FAS_Line&)e;
        break;

    case FAS2::EntityPolyline:
        os << (FAS_Polyline&)e;
        break;

    case FAS2::EntityArc:
        os << (FAS_Arc&)e;
        break;

    case FAS2::EntityCircle:
        os << (FAS_Circle&)e;
        break;

    case FAS2::EntityEllipse:
        os << (FAS_Ellipse&)e;
        break;

    case FAS2::EntityInsert:
        os << (FAS_Insert&)e;
        break;

    case FAS2::EntityMText:
        os << (FAS_MText&)e;
        break;

    case FAS2::EntityText:
        os << (FAS_Text&)e;
        break;

    default:
        os << "Unknown Entity";
        break;
    }
    os << "}\n\n";

    return os;
}
