/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_UNDOLISTITEM_H
#define FAS_UNDOLISTITEM_H

#include <iosfwd>
#include <set>

#include "fas_entity.h"
#include "fas_undoable.h"

/*
 * An Undo Cycle represents an action that was triggered and can
 * be undone. It stores all the pointers to the Undoables affected by
 * the action. Undoables are entities in a container that can be
 * created and deleted.
 * Undo Cycles are stored within classes derrived from FAS_Undo.
 */
class FAS_UndoCycle
{
public:
    FAS_UndoCycle(/*FAS2::UndoType type*/){}

    // Adds an Undoable to this Undo Cycle. Every Cycle can contain one or more Undoables.
    void addUndoable(FAS_Undoable* u);
    //Removes an undoable from the list.
    void removeUndoable(FAS_Undoable* u);
    //! change undo state of all undoable in the current cycle
    void changeUndoState();
    friend std::ostream& operator << (std::ostream& os,
                                      FAS_UndoCycle& uc);
    friend class FAS_Undo;
    std::set<FAS_Undoable*> const& getUndoables() const;

private:
    // List of entity id's that were affected by this action
    std::set<FAS_Undoable*> undoables;
};

#endif
