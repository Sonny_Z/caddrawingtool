﻿/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_entitycontainer.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>
#include <QObject>
#include <set>

#include "fas_dimension.h"
#include "fas_ellipse.h"
#include "fas_graphicview.h"
#include "fas_information.h"
#include "fas_insert.h"
#include "fas_layer.h"
#include "fas_line.h"
#include "fas_spline.h"
#include "fas_solid.h"
#include "qdebug.h"
#include "commondef.h"
#include "fasutils.h"
bool FAS_EntityContainer::autoUpdateBorders = true;


FAS_EntityContainer::FAS_EntityContainer(FAS_EntityContainer* parent, bool owner)
    : FAS_Entity(parent)
{
    autoDelete=owner;
    subContainer = nullptr;
    entIdx = -1;
}
FAS_EntityContainer::~FAS_EntityContainer()
{
    clear();
}

FAS_Entity* FAS_EntityContainer::clone() const
{
    FAS_EntityContainer* ec = new FAS_EntityContainer(*this);
    ec->setOwner(autoDelete);
    ec->detach();
    ec->initId();
    return ec;
}

// Detaches shallow copies and creates deep copies of all subentities.
void FAS_EntityContainer::detach()
{
    QList<FAS_Entity*> tmp;
    bool autoDel = isOwner();
    setOwner(false);

    // make deep copies of all entities:
    for(auto e: entities)
    {
        if (!e->getFlag(FAS2::FlagTemp))
        {
            tmp.append(e->clone());
        }
    }

    // clear shared pointers:
    entities.clear();
    setOwner(autoDel);

    // point to new deep copies:
    for(auto e: tmp)
    {
        entities.append(e);
        e->reparent(this);
    }
}

void FAS_EntityContainer::newDetach(QList<FAS_Entity*> origEntities, FAS_EntityContainer* parent)
{
    QList<FAS_Entity*> tmp;
    bool autoDel = isOwner();
    setOwner(false);
    // make deep copies of all entities:
    for(auto e: origEntities)
    {
        if (!e->getFlag(FAS2::FlagTemp))
        {
            tmp.append(e->clone());
        }
    }

    // clear shared pointers:
    entities.clear();
    setOwner(autoDel);

    // point to new deep copies:
    for(auto e: tmp)
    {
        entities.append(e);
        e->reparent(parent);
    }
}

void FAS_EntityContainer::reparent(FAS_EntityContainer* parent)
{
    FAS_Entity::reparent(parent);

    // All sub-entities:
    for(auto e: entities)
    {
        e->reparent(parent);
    }
}


void FAS_EntityContainer::setVisible(bool v)
{
    FAS_Entity::setVisible(v);
    // All sub-entities:
    for(auto e: entities)
    {
        e->setVisible(v);
    }
}

// return Total length of all entities in this container.
double FAS_EntityContainer::getLength() const
{
    double ret = 0.0;
    for(auto e: entities)
    {
        if (e->isVisible())
        {
            double l = e->getLength();
            if (l < 0.0)
            {
                ret = -1.0;
                break;
            }
            else
            {
                ret += l;
            }
        }
    }
    return ret;
}


// Selects this entity.
bool FAS_EntityContainer::setSelected(bool select)
{
    // This entity's select:
    if (FAS_Entity::setSelected(select))
    {
        // All sub-entity's select:
        for(auto e: entities)
        {
            if (e->isVisible())
            {
                e->setSelected(select);
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

// Toggles select on this entity.
bool FAS_EntityContainer::toggleSelected()
{
    // Toggle this entity's select:
    if (FAS_Entity::toggleSelected())
    {
        return true;
    } else {
        return false;
    }
}

// Selects all entities within the given area.
void FAS_EntityContainer::selectWindow(FAS_Vector v1, FAS_Vector v2,                                   bool select, bool cross)
{
    bool included;
    for(auto e: entities)
    {
        included = false;
        if (e->isVisible())
        {
            if (e->isInWindow(v1, v2))
            {
                included = true;
            }
            else if (cross)
            {
                FAS_EntityContainer l;
                l.addRectangle(v1, v2);
                FAS_VectorSolutions sol;
                if (e->isContainer())
                {
                    FAS_EntityContainer* ec = (FAS_EntityContainer*)e;
                    for (FAS_Entity* se = ec->firstEntity(FAS2::ResolveAll);
                         se && included == false;
                         se = ec->nextEntity(FAS2::ResolveAll))
                    {
                        if (se->rtti() == FAS2::EntitySolid)
                        {
                            included = static_cast<FAS_Solid*>(se)->isInCrossWindow(v1,v2);
                        }
                        else
                        {
                            for (auto line: l)
                            {
                                sol = FAS_Information::getIntersection(se, line, true);
                                if (sol.hasValid())
                                {
                                    included = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (e->rtti() == FAS2::EntitySolid)
                {
                    included = static_cast<FAS_Solid*>(e)->isInCrossWindow(v1,v2);
                }
                else
                {
                    for (auto line: l)
                    {
                        sol = FAS_Information::getIntersection(e, line, true);
                        if (sol.hasValid())
                        {
                            included = true;
                            break;
                        }
                    }
                }
            }
        }

        if (included)
        {
            e->setSelected(select);
        }
    }
}

// Adds a entity to this container and updates the borders of this entity-container if autoUpdateBorders is true.
void FAS_EntityContainer::addEntity(FAS_Entity* entity)
{
    if (!entity)
        return;

    if (entity->rtti() == FAS2::EntityImage || entity->rtti() == FAS2::EntityHatch)
    {
        entities.prepend(entity);
    }
    else
    {
        entities.append(entity);
    }
    if (entity->rtti() == FAS2::EntityInsert)
    {
        inserts.prepend((FAS_Insert*)entity);
    }
    if (autoUpdateBorders)
    {
        adjustBorders(entity);
    }
}

//Insert a entity at the end of entities list and updates the borders of this entity-container if autoUpdateBorders is true.
void FAS_EntityContainer::appendEntity(FAS_Entity* entity)
{
    if (!entity)
        return;
    entities.append(entity);
    if (entity->rtti() == FAS2::EntityInsert)
    {
        inserts.prepend((FAS_Insert*)entity);
    }
    if (autoUpdateBorders)
        adjustBorders(entity);
}

// Insert a entity at the start of entities list and updates the borders of this entity-container if autoUpdateBorders is true.
void FAS_EntityContainer::prependEntity(FAS_Entity* entity)
{
    if (!entity)
        return;
    entities.prepend(entity);
    if (autoUpdateBorders)
        adjustBorders(entity);
}

// Move a entity list in this container at the given position, the borders of this entity-container if autoUpdateBorders is true.
void FAS_EntityContainer::moveEntity(int index, QList<FAS_Entity *>& entList)
{
    if (entList.isEmpty())
        return;
    int ci = 0; //current index for insert without invert order
    bool ret, into = false;
    FAS_Entity *mid = nullptr;
    if (index < 1)
    {
        ci = 0;
    }
    else if (index >= entities.size() )
    {
        ci = entities.size() - entList.size();
    }
    else
    {
        into = true;
        mid = entities.at(index);
    }

    for (int i = 0; i < entList.size(); ++i)
    {
        FAS_Entity *e = entList.at(i);
        ret = entities.removeOne(e);
        //if e not exist in entities list remove from entList
        if (!ret)
        {
            entList.removeAt(i);
        }
    }
    if (into)
    {
        ci = entities.indexOf(mid);
    }

    for(auto e: entList)
    {
        entities.insert(ci++, e);
    }
}

// Inserts a entity to this container at the given position and updates the borders of this entity-container if autoUpdateBorders is true.
void FAS_EntityContainer::insertEntity(int index, FAS_Entity* entity) {
    if (!entity) return;

    entities.insert(index, entity);
    if (autoUpdateBorders)
        adjustBorders(entity);
}


// Removes an entity from this container and updates the borders of this entity-container if autoUpdateBorders is true.
bool FAS_EntityContainer::removeEntity(FAS_Entity* entity)
{
    bool ret;
    ret = entities.removeOne(entity);
    if (autoDelete && ret)
    {
        delete entity;
    }
    if (autoUpdateBorders)
    {
        calculateBorders();
    }
    return ret;
}

//Erases all entities in this container and resets the borders..
void FAS_EntityContainer::clear()
{
    if (autoDelete)
    {
        while (!entities.isEmpty()){
            auto e = entities.takeFirst();
//            qDebug() << "Entity ID:" <<e->getId();

//            qDebug() << "Entity ID:" <<e->getDocument();
//            qDebug() << "e->getLength:" <<e->getLength();
//            qDebug() << "e->getMax().x:" <<e->getMax().x;
//            qDebug() << "e->getCenter().x:" <<e->getCenter().x;
//            qDebug() << "e->getEndpoint():" <<e->getEndpoint().x;
//            FAS2::EntityType type=e->rtti();
//            if(!type || type>30){
//                continue;
////                qDebug() << "continued loop";
//            }
//            qDebug() << "Entity Type:" <<e->rtti();
//            if(e && e->rtti()==FAS2::EntityInsert){
//                auto v=e->getBlock();
//                if(!v){
//                     continue;
//                }
//            }
            if(nullptr != e){
//                if(e->rtti()==FAS2::EntityBlock || e->rtti()==FAS2::EntityInsert){
//                    continue;
//                }
                delete e;
                e = nullptr;
            }
        }

    } else
        entities.clear();
    resetBorders();
}

unsigned int FAS_EntityContainer::count() const
{
    return entities.size();
}


/**
 * Counts all entities (leaves of the tree).
 */
unsigned int FAS_EntityContainer::countDeep() const
{
    unsigned int c=0;
    for(auto t: *this)
    {
        c += t->countDeep();
    }
    return c;
}



// Counts the selected entities in this container.
unsigned int FAS_EntityContainer::countSelected(bool deep, std::initializer_list<FAS2::EntityType> const& types)
{
    unsigned int c=0;
    std::set<FAS2::EntityType> type = types;

    for (FAS_Entity* t: entities)
    {
        if (t->isSelected())
            if (!types.size() || type.count(t->rtti()))
                c++;

        if (t->isContainer())
            c += static_cast<FAS_EntityContainer*>(t)->countSelected(deep);
    }

    return c;
}

// Counts the selected entities in this container.
double FAS_EntityContainer::totalSelectedLength()
{
    double ret(0.0);
    for (FAS_Entity* e: entities)
    {
        if (e->isVisible() && e->isSelected())
        {
            double l = e->getLength();
            if (l >= 0.0)
            {
                ret += l;
            }
        }
    }
    return ret;
}


// Adjusts the borders of this graphic (max/min values)
void FAS_EntityContainer::adjustBorders(FAS_Entity* entity)
{
    if (entity)
    {
        // make sure a container is not empty (otherwise the border
        if (!entity->isContainer() || entity->count()>0)
        {
            //If max and min is the same, some error occurs, ignore it.
            if(abs(entity->getMax().x - entity->getMin().x) < FAS_TOLERANCE &&
                    abs(entity->getMax().y - entity->getMin().y) < FAS_TOLERANCE)
                return;
            minV = FAS_Vector::minimum(entity->getMin(),minV);
            maxV = FAS_Vector::maximum(entity->getMax(),maxV);
        }
    }
}


// Recalculates the borders of this entity container
void FAS_EntityContainer::calculateBorders()
{
    resetBorders();
    for (FAS_Entity* e: entities)
    {
        if(e->rtti()==FAS2::EntityInsert){
            continue;
        }
        FAS_Layer* layer = e->getLayer();
        bool calculte = false;
        if (e->isVisible())
        {
            if(nullptr == layer)
                calculte = true;
            else if(!layer->isFrozen() && layer->isPrint())
                calculte = true;
        }
        if(calculte)
        {
            e->calculateBorders();
            adjustBorders(e);
        }
    }
    // needed for correcting corrupt data (PLANS.dxf)
    if (minV.x>maxV.x || minV.x>FAS_MAXDOUBLE || maxV.x>FAS_MAXDOUBLE
            || minV.x<FAS_MINDOUBLE || maxV.x<FAS_MINDOUBLE)
    {
        minV.x = 0.0;
        maxV.x = 0.0;
    }
    if (minV.y>maxV.y || minV.y>FAS_MAXDOUBLE || maxV.y>FAS_MAXDOUBLE
            || minV.y<FAS_MINDOUBLE || maxV.y<FAS_MINDOUBLE)
    {
        minV.y = 0.0;
        maxV.y = 0.0;
    }

}

// Recalculates the borders of this entity container
void FAS_EntityContainer::calculateBlockBorders()
{
    resetBorders();
    for (FAS_Entity* e: entities)
    {
        if(e->rtti()==FAS2::EntityInsert){
            adjustBorders(e);
            continue;
        }
        FAS_Layer* layer = e->getLayer();
        bool calculte = false;
        if (e->isVisible())
        {
            if(nullptr == layer)
                calculte = true;
            else if(!layer->isFrozen() && layer->isPrint())
                calculte = true;
        }
        if(calculte)
        {
            e->calculateBorders();
            adjustBorders(e);
        }
    }
    // needed for correcting corrupt data (PLANS.dxf)
    if (minV.x>maxV.x || minV.x>FAS_MAXDOUBLE || maxV.x>FAS_MAXDOUBLE
            || minV.x<FAS_MINDOUBLE || maxV.x<FAS_MINDOUBLE)
    {
        minV.x = 0.0;
        maxV.x = 0.0;
    }
    if (minV.y>maxV.y || minV.y>FAS_MAXDOUBLE || maxV.y>FAS_MAXDOUBLE
            || minV.y<FAS_MINDOUBLE || maxV.y<FAS_MINDOUBLE)
    {
        minV.y = 0.0;
        maxV.y = 0.0;
    }

}

// Recalculates the borders of this entity container including invisible entities.
void FAS_EntityContainer::forcedCalculateBorders()
{
    resetBorders();
    for (FAS_Entity* e: entities)
    {
        if (e->isContainer())
        {
            ((FAS_EntityContainer*)e)->forcedCalculateBorders();
        }
        else
        {
            e->calculateBorders();
        }
        adjustBorders(e);
    }

    // needed for correcting corrupt data (PLANS.dxf)
    if (minV.x>maxV.x || minV.x>FAS_MAXDOUBLE || maxV.x>FAS_MAXDOUBLE
            || minV.x<FAS_MINDOUBLE || maxV.x<FAS_MINDOUBLE)
    {
        minV.x = 0.0;
        maxV.x = 0.0;
    }
    if (minV.y>maxV.y || minV.y>FAS_MAXDOUBLE || maxV.y>FAS_MAXDOUBLE
            || minV.y<FAS_MINDOUBLE || maxV.y<FAS_MINDOUBLE)
    {
        minV.y = 0.0;
        maxV.y = 0.0;
    }
}

//Updates all Dimension entities in this container.
void FAS_EntityContainer::updateDimensions(bool autoText)
{
    for (FAS_Entity* e: entities)
    {
        if (FAS_Information::isDimension(e->rtti()))
        {
            // update and reposition label:
            ((FAS_Dimension*)e)->updateDim(autoText);
        } else if(e->rtti()==FAS2::EntityDimLeader)
            e->update();
        else if (e->isContainer())
        {
            ((FAS_EntityContainer*)e)->updateDimensions(autoText);
        }
    }
}

// Updates all Insert entities in this container.
void FAS_EntityContainer::updateInserts(int loopSize)
{
   // qDebug() << "entities size:" << entities.size() ;
//    qDebug() << "entities loop num:" << loopSize ;
    int mcount=entities.count();
    int mcount1=COMMONDEF->readLineNumber;
    int c1=0;
    for (FAS_Entity* e: entities)
    {
        c1++;
        double c2=(1.0*c1/mcount);
        COMMONDEF->readLineNumber=mcount1+mcount1*c2;
//        if(loopSize>=10){
//            break;
//        }
        // Only update our own inserts and not inserts of inserts
        if (e->rtti() == FAS2::EntityInsert)
        {
//            qDebug() << "entities loop start num:" << loopSize ;
            ((FAS_Insert*)e)->update();
//           qDebug() << "entities loop end num:" << loopSize ;
            loopSize++;

        }
        else if (e->isContainer()&& e->rtti()!=FAS2::EntityHatch)//
        {
//            qDebug() << "Container loop start num:" << loopSize ;
            ((FAS_EntityContainer*)e)->updateInserts(loopSize);
//            qDebug() << "Container loop end num:" << loopSize ;
             loopSize++;

        }
//        e->setVisible(getFlag(FAS2::FlagVisible));


    }
}

// Renames all inserts with name 'oldName' to 'newName'
void FAS_EntityContainer::renameInserts(const QString& oldName, const QString& newName)
{
    for (FAS_Entity* e: entities)
    {
        if (e->rtti()==FAS2::EntityInsert)
        {
            FAS_Insert* i = ((FAS_Insert*)e);
            if (i->getName()==oldName)
            {
                i->setName(newName);
            }
        }
        else if (e->isContainer())
        {
            ((FAS_EntityContainer*)e)->renameInserts(oldName, newName);
        }
    }
}

// Updates all Spline entities in this container.
void FAS_EntityContainer::updateSplines() {
    for (FAS_Entity* e: entities)
    {
        if (e->rtti()==FAS2::EntitySpline)
        {
            ((FAS_Spline*)e)->update();
        }
        else if (e->isContainer() && e->rtti()!=FAS2::EntityHatch)
        {
            ((FAS_EntityContainer*)e)->updateSplines();
        }
    }
}


// Updates the sub entities of this container.
void FAS_EntityContainer::update()
{
    for (FAS_Entity* e: entities)
    {
        e->update();
    }
}

void FAS_EntityContainer::addRectangle(FAS_Vector const& v0, FAS_Vector const& v1)
{
    addEntity(new FAS_Line{this, v0, {v1.x, v0.y}});
    addEntity(new FAS_Line{this, {v1.x, v0.y}, v1});
    addEntity(new FAS_Line{this, v1, {v0.x, v1.y}});
    addEntity(new FAS_Line{this, {v0.x, v1.y}, v0});
}

// Returns the first entity or nullptr if this graphic is empty.
FAS_Entity* FAS_EntityContainer::firstEntity(FAS2::ResolveLevel level)
{
    FAS_Entity* e = nullptr;
    entIdx = -1;
    switch (level)
    {
    case FAS2::ResolveNone:
        if (!entities.isEmpty())
        {
            entIdx = 0;
            return entities.first();
        }
        break;

    case FAS2::ResolveAllButInserts:
        subContainer=nullptr;
        if (!entities.isEmpty())
        {
            entIdx = 0;
            e = entities.first();
        }
        if (e && e->isContainer() && e->rtti()!=FAS2::EntityInsert)
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->firstEntity(level);
            if (!e)
            {
                subContainer = nullptr;
                e = nextEntity(level);
            }
        }
        return e;
        break;

    case FAS2::ResolveAllButTextImage:
    case FAS2::ResolveAllButTexts:
        subContainer=nullptr;
        if (!entities.isEmpty())
        {
            entIdx = 0;
            e = entities.first();
        }
        if (e && e->isContainer() && e->rtti()!=FAS2::EntityText && e->rtti()!=FAS2::EntityMText)
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->firstEntity(level);
            if (!e)
            {
                subContainer = nullptr;
                e = nextEntity(level);
            }
        }
        return e;
        break;

    case FAS2::ResolveAll:
        subContainer=nullptr;
        if (!entities.isEmpty())
        {
            entIdx = 0;
            e = entities.first();
        }
        if (e && e->isContainer())
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->firstEntity(level);
            if (!e)
            {
                subContainer = nullptr;
                e = nextEntity(level);
            }
        }
        return e;
        break;
    }

    return nullptr;
}



// Returns the last entity or \p nullptr if this graphic is empty.
FAS_Entity* FAS_EntityContainer::lastEntity(FAS2::ResolveLevel level)
{
    FAS_Entity* e = nullptr;
    if(!entities.size()) return nullptr;
    entIdx = entities.size()-1;
    switch (level)
    {
    case FAS2::ResolveNone:
        if (!entities.isEmpty())
            return entities.last();
        break;

    case FAS2::ResolveAllButInserts:
    {
        if (!entities.isEmpty())
            e = entities.last();
        subContainer = nullptr;
        if (e && e->isContainer() && e->rtti()!=FAS2::EntityInsert)
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->lastEntity(level);
        }
        return e;
    }
        break;
    case FAS2::ResolveAllButTextImage:
    case FAS2::ResolveAllButTexts:
    {
        if (!entities.isEmpty())
            e = entities.last();
        subContainer = nullptr;
        if (e && e->isContainer() && e->rtti()!=FAS2::EntityText && e->rtti()!=FAS2::EntityMText)
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->lastEntity(level);
        }
        return e;
    }
        break;

    case FAS2::ResolveAll:
    {
        if (!entities.isEmpty())
            e = entities.last();
        subContainer = nullptr;
        if (e && e->isContainer())
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->lastEntity(level);
        }
        return e;
    }
        break;
    }

    return nullptr;
}


//Returns the next entity or container
FAS_Entity* FAS_EntityContainer::nextEntity(FAS2::ResolveLevel level)
{
    ++entIdx;
    switch (level)
    {
    case FAS2::ResolveNone:
        if ( entIdx < entities.size() )
            return entities.at(entIdx);
        break;

    case FAS2::ResolveAllButInserts:
    {
        FAS_Entity* e=nullptr;
        if (subContainer)
        {
            e = subContainer->nextEntity(level);
            if (e) {
                --entIdx; //return a sub-entity, index not advanced
                return e;
            }
            else
            {
                if ( entIdx < entities.size() )
                    e = entities.at(entIdx);
            }
        }
        else
        {
            if ( entIdx < entities.size() )
                e = entities.at(entIdx);
        }
        if (e && e->isContainer() && e->rtti()!=FAS2::EntityInsert)
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->firstEntity(level);
            // emtpy container:
            if (!e)
            {
                subContainer = nullptr;
                e = nextEntity(level);
            }
        }
        return e;
    }
        break;

    case FAS2::ResolveAllButTextImage:
    case FAS2::ResolveAllButTexts:
    {
        FAS_Entity* e=nullptr;
        if (subContainer)
        {
            e = subContainer->nextEntity(level);
            if (e)
            {
                --entIdx; //return a sub-entity, index not advanced
                return e;
            }
            else
            {
                if ( entIdx < entities.size() )
                    e = entities.at(entIdx);
            }
        }
        else
        {
            if ( entIdx < entities.size() )
                e = entities.at(entIdx);
        }
        if (e && e->isContainer() && e->rtti()!=FAS2::EntityText && e->rtti()!=FAS2::EntityMText )
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->firstEntity(level);
            // emtpy container:
            if (!e)
            {
                subContainer = nullptr;
                e = nextEntity(level);
            }
        }
        return e;
    }
        break;

    case FAS2::ResolveAll:
    {
        FAS_Entity* e=nullptr;
        if (subContainer)
        {
            e = subContainer->nextEntity(level);
            if (e)
            {
                --entIdx; //return a sub-entity, index not advanced
                return e;
            }
            else
            {
                if ( entIdx < entities.size() )
                    e = entities.at(entIdx);
            }
        }
        else
        {
            if ( entIdx < entities.size() )
                e = entities.at(entIdx);
        }
        if (e && e->isContainer()) {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->firstEntity(level);
            // emtpy container:
            if (!e)
            {
                subContainer = nullptr;
                e = nextEntity(level);
            }
        }
        return e;
    }
        break;
    }
    return nullptr;
}

// Returns the prev entity or container or  nullptr if the last entity returned by  prev() was the first entity in the container.
FAS_Entity* FAS_EntityContainer::prevEntity(FAS2::ResolveLevel level) {
    //set entIdx pointing in prev entity and check if is out of range
    --entIdx;
    switch (level)
    {
    case FAS2::ResolveNone:
        if (entIdx >= 0)
            return entities.at(entIdx);
        break;

    case FAS2::ResolveAllButInserts:
    {
        FAS_Entity* e=nullptr;
        if (subContainer)
        {
            e = subContainer->prevEntity(level);
            if (e)
            {
                return e;
            }
            else
            {
                if (entIdx >= 0)
                    e = entities.at(entIdx);
            }
        }
        else
        {
            if (entIdx >= 0)
                e = entities.at(entIdx);
        }
        if (e && e->isContainer() && e->rtti()!=FAS2::EntityInsert)
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->lastEntity(level);
            // emtpy container:
            if (!e)
            {
                subContainer = nullptr;
                e = prevEntity(level);
            }
        }
        return e;
    }

    case FAS2::ResolveAllButTextImage:
    case FAS2::ResolveAllButTexts:
    {
        FAS_Entity* e=nullptr;
        if (subContainer)
        {
            e = subContainer->prevEntity(level);
            if (e)
            {
                return e;
            }
            else
            {
                if (entIdx >= 0)
                    e = entities.at(entIdx);
            }
        }
        else
        {
            if (entIdx >= 0)
                e = entities.at(entIdx);
        }
        if (e && e->isContainer() && e->rtti()!=FAS2::EntityText && e->rtti()!=FAS2::EntityMText)
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->lastEntity(level);
            // emtpy container:
            if (!e)
            {
                subContainer = nullptr;
                e = prevEntity(level);
            }
        }
        return e;
    }

    case FAS2::ResolveAll:
    {
        FAS_Entity* e=nullptr;
        if (subContainer)
        {
            e = subContainer->prevEntity(level);
            if (e)
            {
                ++entIdx; //return a sub-entity, index not advanced
                return e;
            }
            else
            {
                if (entIdx >= 0)
                    e = entities.at(entIdx);
            }
        }
        else
        {
            if (entIdx >= 0)
                e = entities.at(entIdx);
        }
        if (e && e->isContainer())
        {
            subContainer = (FAS_EntityContainer*)e;
            e = ((FAS_EntityContainer*)e)->lastEntity(level);
            // emtpy container:
            if (!e)
            {
                subContainer = nullptr;
                e = prevEntity(level);
            }
        }
        return e;
    }
    }
    return nullptr;
}

// return Entity at the given index or nullptr if the index is out of range.
FAS_Entity* FAS_EntityContainer::entityAt(int index)
{
    if (entities.size() > index && index >= 0)
        return entities.at(index);
    else
        return nullptr;
}

void FAS_EntityContainer::setEntityAt(int index,FAS_Entity* en)
{
    if(autoDelete && entities.at(index))
    {
        delete entities.at(index);
    }
    entities[index] = en;
}

// Finds the given entity and makes it the current entity if found.
int FAS_EntityContainer::findEntity(FAS_Entity const* const entity)
{
    entIdx = entities.indexOf(const_cast<FAS_Entity*>(entity));
    return entIdx;
}

// return The point which is closest to 'coord'
FAS_Vector FAS_EntityContainer::getNearestEndpoint(const FAS_Vector& coord,                                                double* dist)const
{
    double minDist = FAS_MAXDOUBLE;  // minimum measured distance
    double curDist;                 // currently measured distance
    FAS_Vector closestPoint(false);  // closest found endpoint
    FAS_Vector point;                // endpoint found

    for (FAS_Entity* en: entities)
    {
        if (en->isVisible() && !en->getParent()->ignoredOnModification())
        {
            //no end point for Insert, text, Dim
            point = en->getNearestEndpoint(coord, &curDist);
            if (point.valid && curDist<minDist)
            {
                closestPoint = point;
                minDist = curDist;
                if (dist)
                {
                    *dist = minDist;
                }
            }
        }
    }

    return closestPoint;
}

// return The point which is closest to 'coord'
FAS_Vector FAS_EntityContainer::getNearestEndpoint(const FAS_Vector& coord, double* dist,  FAS_Entity** pEntity)const
{
    double minDist = FAS_MAXDOUBLE;  // minimum measured distance
    double curDist;                 // currently measured distance
    FAS_Vector closestPoint(false);  // closest found endpoint
    FAS_Vector point;                // endpoint found
    unsigned i0=0;
    for(auto en: entities)
    {
        if (!en->getParent()->ignoredOnModification() )
        {
            //no end point for Insert, text, Dim
            point = en->getNearestEndpoint(coord, &curDist);
            if (point.valid && curDist<minDist)
            {
                closestPoint = point;
                minDist = curDist;
                if (dist)
                {
                    *dist = minDist;
                }
                if(pEntity)
                {
                    *pEntity=en;
                }
            }
        }
        i0++;
    }
    return closestPoint;
}

FAS_Vector FAS_EntityContainer::getNearestPointOnEntity(const FAS_Vector& coord,
                                                        bool onEntity, double* dist, FAS_Entity** entity)const
{
    FAS_Vector point(false);
    FAS_Entity* en = getNearestEntity(coord, dist, FAS2::ResolveNone);
    if (en && en->isVisible() && !en->getParent()->ignoredSnap())
    {
        point = en->getNearestPointOnEntity(coord, onEntity, dist, entity);
    }

    return point;
}



FAS_Vector FAS_EntityContainer::getNearestCenter(const FAS_Vector& coord, double* dist) const
{
    double minDist = FAS_MAXDOUBLE;  // minimum measured distance
    double curDist = FAS_MAXDOUBLE;  // currently measured distance
    FAS_Vector closestPoint(false);  // closest found endpoint
    FAS_Vector point;                // endpoint found

    for(auto en: entities)
    {
        if (en->isVisible() && !en->getParent()->ignoredSnap())
        {
            //no center point for spline, text, Dim
            point = en->getNearestCenter(coord, &curDist);
            if (point.valid && curDist<minDist)
            {
                closestPoint = point;
                minDist = curDist;
            }
        }
    }
    if (dist)
    {
        *dist = minDist;
    }
    return closestPoint;
}

// return the nearest of equidistant middle points of the line.

FAS_Vector FAS_EntityContainer::getNearestMiddle(const FAS_Vector& coord,
                                                 double* dist,
                                                 int middlePoints
                                                 ) const
{
    double minDist = FAS_MAXDOUBLE;  // minimum measured distance
    double curDist = FAS_MAXDOUBLE;  // currently measured distance
    FAS_Vector closestPoint(false);  // closest found endpoint
    FAS_Vector point;                // endpoint found

    for(auto en: entities)
    {
        if (en->isVisible() && !en->getParent()->ignoredSnap())
        {
            //no midle point for spline, text, Dim
            point = en->getNearestMiddle(coord, &curDist, middlePoints);
            if (point.valid && curDist<minDist)
            {
                closestPoint = point;
                minDist = curDist;
            }
        }
    }
    if (dist)
    {
        *dist = minDist;
    }
    return closestPoint;
}

FAS_Vector FAS_EntityContainer::getNearestDist(double distance, const FAS_Vector& coord, double* dist) const
{
    FAS_Vector point(false);
    FAS_Entity* closestEntity;

    closestEntity = getNearestEntity(coord, nullptr, FAS2::ResolveNone);

    if (closestEntity)
    {
        point = closestEntity->getNearestDist(distance, coord, dist);
    }
    return point;
}

//return The intersection which is closest to 'coord'
FAS_Vector FAS_EntityContainer::getNearestIntersection(const FAS_Vector& coord, double* dist)
{
    double minDist = FAS_MAXDOUBLE;  // minimum measured distance
    double curDist = FAS_MAXDOUBLE;  // currently measured distance
    FAS_Vector closestPoint(false);  // closest found endpoint
    FAS_Vector point;                // endpoint found
    FAS_VectorSolutions sol;
    FAS_Entity* closestEntity;

    closestEntity = getNearestEntity(coord, nullptr, FAS2::ResolveAllButTextImage);

    if (closestEntity)
    {
        for (FAS_Entity* en = firstEntity(FAS2::ResolveAllButTextImage); en; en = nextEntity(FAS2::ResolveAllButTextImage))
        {
            if ( !en->isVisible()|| en->getParent()->ignoredSnap() )
            {
                continue;
            }
            sol = FAS_Information::getIntersection(closestEntity, en, true);
            point=sol.getClosest(coord,&curDist,nullptr);
            if(sol.getNumber()>0 && curDist<minDist)
            {
                closestPoint=point;
                minDist=curDist;
            }
        }
    }
    if(dist && closestPoint.valid)
    {
        *dist = minDist;
    }
    return closestPoint;
}

FAS_Vector FAS_EntityContainer::getNearestRef(const FAS_Vector& coord, double* dist) const
{
    double minDist = FAS_MAXDOUBLE;  // minimum measured distance
    double curDist;                 // currently measured distance
    FAS_Vector closestPoint(false);  // closest found endpoint
    FAS_Vector point;                // endpoint found

    for(auto en: entities)
    {
        if (en->isVisible())
        {
            point = en->getNearestRef(coord, &curDist);
            if (point.valid && curDist<minDist)
            {
                closestPoint = point;
                minDist = curDist;
                if (dist)
                {
                    *dist = minDist;
                }
            }
        }
    }
    return closestPoint;
}


FAS_Vector FAS_EntityContainer::getNearestSelectedRef(const FAS_Vector& coord, double* dist) const
{
    double minDist = FAS_MAXDOUBLE;  // minimum measured distance
    double curDist;                 // currently measured distance
    FAS_Vector closestPoint(false);  // closest found endpoint
    FAS_Vector point;                // endpoint found

    for(auto en: entities)
    {
        if (en->isVisible() && en->isSelected() && !en->isParentSelected())
        {
            point = en->getNearestSelectedRef(coord, &curDist);
            if (point.valid && curDist<minDist)
            {
                closestPoint = point;
                minDist = curDist;
                if (dist)
                {
                    *dist = minDist;
                }
            }
        }
    }

    return closestPoint;
}


double FAS_EntityContainer::getDistanceToPoint(const FAS_Vector& coord,
                                               FAS_Entity** entity,
                                               FAS2::ResolveLevel level,
                                               double solidDist) const
{
    double minDist = FAS_MAXDOUBLE;      // minimum measured distance
    double curDist;                     // currently measured distance
    FAS_Entity* closestEntity = nullptr;    // closest entity found
    FAS_Entity* subEntity = nullptr;

    for(auto e: entities)
    {
        if (e->isVisible())
        {
            if(level==FAS2::ResolveAllButTextImage && e->rtti()==FAS2::EntityImage)
                continue;
            curDist = e->getDistanceToPoint(coord, &subEntity, level, solidDist);

            if (curDist <= minDist)
            {
                switch(level)
                {
                case FAS2::ResolveAll:
                case FAS2::ResolveAllButTextImage:
                    closestEntity = subEntity;
                    break;
                default:
                    closestEntity = e;
                }
                minDist = curDist;
            }
        }
    }

    if (entity)
    {
        *entity = closestEntity;
    }
    return minDist;
}

FAS_Entity* FAS_EntityContainer::getNearestEntity(const FAS_Vector& coord, double* dist, FAS2::ResolveLevel level) const
{
    FAS_Entity* e = nullptr;
    // distance for points inside solids:
    double solidDist = FAS_MAXDOUBLE;
    if (dist)
    {
        solidDist = *dist;
    }
    double d = getDistanceToPoint(coord, &e, level, solidDist);
    if (e && e->isVisible()==false)
    {
        e = nullptr;
    }

    // if d is negative, use the default distance (used for points inside solids)
    if (dist)
    {
        *dist = d;
    }
    return e;
}

/*
 * Rearranges the atomic entities in this container in a way that connected
 * entities are stored in the right order and direction.
 * Non-recoursive. Only affects atomic entities in this container.
 */
bool FAS_EntityContainer::optimizeContours()
{
    FAS_EntityContainer tmp;
    tmp.setAutoUpdateBorders(false);
    bool closed=true;

    // accept all full circles
    QList<FAS_Entity*> enList;
    for(auto e1: entities)
    {
        if (!e1->isEdge() || e1->isContainer() )
        {
            enList<<e1;
            continue;
        }

        //detect circles and whole ellipses
        switch(e1->rtti())
        {
        case FAS2::EntityEllipse:
            if(static_cast<FAS_Ellipse*>(e1)->isEllipticArc())
                continue;
        case FAS2::EntityCircle:
            tmp.addEntity(e1->clone());
            enList<<e1;
        default:
            continue;
        }

    }

    // remove unsupported entities
    for(FAS_Entity* it: enList)
        removeEntity(it);

    // check and form a closed contour
    // the first entity
    FAS_Entity* current(nullptr);
    if(count()>0)
    {
        current=entityAt(0)->clone();
        tmp.addEntity(current);
        removeEntity(entityAt(0));
    }
    else
    {
        if(tmp.count()==0)
            return false;
    }
    FAS_Vector vpStart;
    FAS_Vector vpEnd;
    if(current)
    {
        vpStart=current->getStartpoint();
        vpEnd=current->getEndpoint();
    }
    FAS_Entity* next(nullptr);
    /** connect entities **/
    while(count()>0)
    {
        double dist(0.);
        if(dist>1e-8)
        {
            if(vpEnd.squaredTo(vpStart)<1e-8)
            {
                FAS_Entity* e2=entityAt(0);
                tmp.addEntity(e2->clone());
                vpStart=e2->getStartpoint();
                vpEnd=e2->getEndpoint();
                removeEntity(e2);
                continue;
            }
            closed=false;
        }
        if(next && closed)
        {
            next->setProcessed(true);
            FAS_Entity* eTmp = next->clone();
            if(vpEnd.squaredTo(eTmp->getStartpoint())>vpEnd.squaredTo(eTmp->getEndpoint()))
                eTmp->revertDirection();
            vpEnd=eTmp->getEndpoint();
            tmp.addEntity(eTmp);
            removeEntity(next);
        }
        else
        {
            closed=false;	//workaround if next is nullptr
            break;			//workaround if next is nullptr

        }
    }
    if(vpEnd.valid && vpEnd.squaredTo(vpStart)>1e-8)
    {
        if(closed)
            closed=false;
    }

    // add new sorted entities:
    for(auto en: tmp)
    {
        en->setProcessed(false);
        addEntity(en->clone());
    }
    return closed;
}


bool FAS_EntityContainer::hasEndpointsWithinWindow(const FAS_Vector& v1, const FAS_Vector& v2)
{
    for(auto e: entities){
        if (e->hasEndpointsWithinWindow(v1, v2))
        {
            return true;
        }
    }
    return false;
}

void FAS_EntityContainer::move(const FAS_Vector& offset)
{
    for(auto e: entities)
    {
        e->move(offset);
        if (autoUpdateBorders)
        {
            e->moveBorders(offset);
        }
    }
    if (autoUpdateBorders)
    {
        moveBorders(offset);
    }
}

void FAS_EntityContainer::rotate(const FAS_Vector& center, const double& angle)
{
    FAS_Vector angleVector(angle);
    for(auto e: entities)
    {
        e->rotate(center, angleVector);
    }
    if (autoUpdateBorders)
    {
        calculateBorders();
    }
}

void FAS_EntityContainer::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    for(auto e: entities)
    {
        e->rotate(center, angleVector);
    }
    if (autoUpdateBorders)
    {
        calculateBorders();
    }
}


void FAS_EntityContainer::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    if (fabs(factor.x) > FAS_TOLERANCE && fabs(factor.y) > FAS_TOLERANCE)
    {
        for(auto e: entities)
        {
            e->scale(center, factor);
        }
    }
    if (autoUpdateBorders)
    {
        calculateBorders();
    }
}



void FAS_EntityContainer::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    if (axisPoint1.distanceTo(axisPoint2)>FAS_TOLERANCE)
    {
        for(auto e: entities)
        {
            e->mirror(axisPoint1, axisPoint2);
        }
    }
}


void FAS_EntityContainer::stretch(const FAS_Vector& firstCorner,
                                  const FAS_Vector& secondCorner,
                                  const FAS_Vector& offset)
{

    if (getMin().isInWindow(firstCorner, secondCorner) &&
            getMax().isInWindow(firstCorner, secondCorner))
    {
        move(offset);
    }
    else
    {
        for(auto e: entities)
        {
            e->stretch(firstCorner, secondCorner, offset);
        }
    }
    update();
}

void FAS_EntityContainer::moveRef(const FAS_Vector& ref, const FAS_Vector& offset)
{
    for(auto e: entities)
    {
        e->moveRef(ref, offset);
    }
    if (autoUpdateBorders)
    {
        calculateBorders();
    }
}


void FAS_EntityContainer::moveSelectedRef(const FAS_Vector& ref, const FAS_Vector& offset)
{
    for(auto e: entities)
    {
        e->moveSelectedRef(ref, offset);
    }
    if (autoUpdateBorders)
    {
        calculateBorders();
    }
}

void FAS_EntityContainer::revertDirection()
{
    for(int k = 0; k < entities.size() / 2; ++k)
    {
        entities.swap(k, entities.size() - 1 - k);
    }

    for(FAS_Entity*const entity: entities)
    {
        entity->revertDirection();
    }
}
void FAS_EntityContainer::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view, double& /*patternOffset*/,FAS_InsertData& data)
{
    if (!(painter && view))
    {
        return;
    }

    if (COMMONDEF->fastDisplay){
        //To speed up display, quit when inserts list size is bigger than N; May 10 2021
        int bigEntityThd = COMMONDEF->bigEntityThd;
        auto isBigNum = FASUtils::getEntityCountWithBigCondition(this, bigEntityThd);
        if (isBigNum >= bigEntityThd){
            return;
        }
    }

    auto readOnlyEnabled = isReadOnly();
    foreach (auto e, entities)
    {

        bool isNotClone = (e->rtti()==FAS2::EntityLine || e->rtti()==FAS2::EntityCircle||e->rtti()==FAS2::EntityPolyline
                         ||e->rtti()== FAS2::EntityArc || e->rtti()==FAS2::EntitySpline||e->rtti()==FAS2::EntityText
                         ||e->rtti()==FAS2::EntityMText||e->rtti()==FAS2::EntitySolid||e->rtti()==FAS2::EntityHatch
                         ||e->rtti()==FAS2::EntityInsert||e->rtti()==FAS2::EntityPoint||e->rtti()==FAS2::EntityEllipse
                           ||e->rtti()==FAS2::EntityAttdef);//
        if(MYL)
        {
            isNotClone = (e->rtti()==FAS2::EntityLine || e->rtti()==FAS2::EntityCircle||e->rtti()==FAS2::EntityPolyline
                                     ||e->rtti()== FAS2::EntityArc || e->rtti()==FAS2::EntitySpline||e->rtti()==FAS2::EntityText
                                     ||e->rtti()==FAS2::EntityMText||e->rtti()==FAS2::EntitySolid||e->rtti()==FAS2::EntityHatch
                                     ||e->rtti()==FAS2::EntityInsert||e->rtti()==FAS2::EntityPoint||e->rtti()==FAS2::EntityEllipse
                                       ||e->rtti()==FAS2::EntityAttdef||e->rtti()==FAS2::EntityContainer);//
        }
        if(isNotClone & readOnlyEnabled){
//        if(false){
            view->drawEntity(painter, e,data);
        }
        else{
            view->drawEntity(painter, e);
        }
    }
}
// FAS_EntityContainer::draw() draw entities in order
void FAS_EntityContainer::draw(FAS_Painter* painter, FAS_GraphicView* view, double& /*patternOffset*/)
{
    if (!(painter && view))
    {
        return;
    }

    int mcount=entities.count();

    for(int i=0;i<mcount;i++)
    {
        COMMONDEF->drawProgressValue=qRound((i+1)*100.0/mcount);
        view->drawEntity(painter,this->entityAt(i));

        //April 21, to speed up display.
        //In this mode, the entity will be fist sorted according certain criteria.
        //User could update this sort algorithm in this file sortEntities function.
        //Then only the first N entities will be displayed on GUI;
        if (COMMONDEF->fastDisplay == true)
        {
            int temp = COMMONDEF->drawEntityCount;
            int optimalNumber = calOptimalMaxDisplayEntities();
            if (temp >= optimalNumber)
            {
                qDebug() << "UI painting entity count:::"<<temp;
                QString appPerformanceLog = COMMONDEF->getAppPerformanceLog();
                QString uiPaintingLogContent = QString::fromUtf8("\r\nUI painting entity count:::%1(%2)\r\n").arg(temp).arg(mcount);
                COMMONDEF->saveToFile(uiPaintingLogContent,appPerformanceLog,true);

                break;
            }
        }

    }
    /*foreach (auto e, entities)
    {

        view->drawEntity(painter, e);
    }*/



}

double FAS_EntityContainer::areaLineIntegral() const
{
    double contourArea=0.;
    double closedArea=0.;

    for(auto e: entities)
    {
        e->setLayer(getLayer());
        switch (e->rtti())
        {
        case FAS2::EntityLine:
        case FAS2::EntityArc:
            contourArea += e->areaLineIntegral();
            break;
        case FAS2::EntityCircle:
            closedArea += e->areaLineIntegral();
            break;
        case FAS2::EntityEllipse:
            if(static_cast<FAS_Ellipse*>(e)->isArc())
                contourArea += e->areaLineIntegral();
            else
                closedArea += e->areaLineIntegral();
        default:
            break;
        }
    }
    return fabs(contourArea)+closedArea;
}

bool FAS_EntityContainer::ignoredOnModification() const
{
    switch(rtti())
    {
    case FAS2::EntitySpline:
    case FAS2::EntityMText:        /**< Text 15*/
    case FAS2::EntityText:         /**< Text 15*/
    case FAS2::EntityDimAligned:   /**< Aligned Dimension */
    case FAS2::EntityDimLinear:    /**< Linear Dimension */
    case FAS2::EntityDimRadial:    /**< Radial Dimension */
    case FAS2::EntityDimDiametric: /**< Diametric Dimension */
    case FAS2::EntityDimAngular:   /**< Angular Dimension */
    case FAS2::EntityDimLeader:    /**< Leader Dimension */
    case FAS2::EntityHatch:
        return true;
    default:
        return false;
    }
}

bool FAS_EntityContainer::ignoredSnap() const
{
    if (getParent() && getParent()->rtti() == FAS2::EntityHatch)
        return true;
    return ignoredOnModification();
}

QList<FAS_Entity *>::const_iterator FAS_EntityContainer::begin() const
{
    return entities.begin();
}

QList<FAS_Entity *>::const_iterator FAS_EntityContainer::end() const
{
    return entities.end();
}

QList<FAS_Entity *>::iterator FAS_EntityContainer::begin()
{
    return entities.begin();
}

QList<FAS_Entity *>::iterator FAS_EntityContainer::end()
{
    return entities.end();
}

//Dumps the entities to stdout.
std::ostream& operator << (std::ostream& os, FAS_EntityContainer& ec)
{

    static int indent = 0;
    char* tab = new char[indent*2+1];
    for(int i=0; i<indent*2; ++i)
    {
        tab[i] = ' ';
    }
    tab[indent*2] = '\0';

    ++indent;

    unsigned long int id = ec.getId();

    os << tab << "EntityContainer[" << id << "]: \n";
    os << tab << "Borders[" << id << "]: "
       << ec.minV << " - " << ec.maxV << "\n";
    if (ec.getLayer()) {
        os << tab << "Layer[" << id << "]: "
           << ec.getLayer()->getName().toLatin1().data() << "\n";
    } else {
        os << tab << "Layer[" << id << "]: <nullptr>\n";
    }
    os << tab << " Flags[" << id << "]: "
       << (ec.getFlag(FAS2::FlagVisible) ? "FAS2::FlagVisible" : "");
    os << (ec.getFlag(FAS2::FlagUndone) ? " FAS2::FlagUndone" : "");
    os << (ec.getFlag(FAS2::FlagSelected) ? " FAS2::FlagSelected" : "");
    os << "\n";


    os << tab << "Entities[" << id << "]: \n";
    for(auto t: ec)
    {
        switch (t->rtti())
        {
        case FAS2::EntityInsert:
            os << tab << *((FAS_Insert*)t);
            os << tab << *((FAS_Entity*)t);
            os << tab << *((FAS_EntityContainer*)t);
            break;
        default:
            if (t->isContainer())
            {
                os << tab << *((FAS_EntityContainer*)t);
            } else {
                os << tab << *t;
            }
            break;
        }
    }
    os << tab << "\n\n";
    --indent;
    delete[] tab;
    return os;
}


FAS_Entity* FAS_EntityContainer::first() const
{
    return entities.first();
}

FAS_Entity* FAS_EntityContainer::last() const
{
    return entities.last();
}

QList<FAS_Entity*> FAS_EntityContainer::getEntityList() const
{
    return entities;
}


QList<FAS_Insert*> FAS_EntityContainer::getInsertList() const
{
    return inserts;
}

QList<FAS_Entity*> FAS_EntityContainer::GetAllLineArcEntities(const FAS_EntityContainer* input)
{
    QList<FAS_Entity*> result;
    QList<FAS_Entity*> entities = input->getEntityList();
    for(FAS_Entity* entity : entities)
    {
        if(entity->isContainer())
        {
            QList<FAS_Entity*> tempResult = GetAllLineArcEntities((FAS_EntityContainer*)entity);
            result.append(tempResult);
        }
        else if(FAS2::EntityLine == entity->rtti())
        {
            result.append(entity);
        }
        else if(FAS2::EntityArc == entity->rtti())
        {
            result.append(entity);
        }
    }
    return result;
}

FAS_Vector FAS_EntityContainer::getMiddlePoint() const
{
    FAS_Vector middlPoint;
/*
    QList<FAS_Entity*> entities = GetAllLineArcEntities(this);
    int num = 0;
    if(!entities.empty())
    {
        for(auto oneEnt : entities)
        {
            if(FAS2::EntityLine == oneEnt->rtti() || FAS2::EntityArc == oneEnt->rtti())
            {
                middlPoint += oneEnt->getMiddlePoint();
                num++;
            }
        }
    }
    if(num != 0)
    {
        middlPoint = middlPoint/num;
    }
    else
    {
        middlPoint = this->getCenter();
    }
*/
    middlPoint = (getMax() + getMin()) * 0.5;
    return middlPoint;
}
void FAS_EntityContainer::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor)
{

    for(int i=0;i<this->count();i++)
    {
        FAS_Entity* e=this->entityAt(i);
         e->resetBasicData(offset,center,angle,factor);
    }
}

void FAS_EntityContainer::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor)
{
    for(int i=0;i<this->count();i++)
    {
        this->entityAt(i)->moveEntityBoarder(offset,center,angle,factor);
    }
}

bool FAS_EntityContainer::isReadOnly(){
    return COMMONDEF->isReadOnlyMode();
}

//Compare each 2 entities based on certain criteria; April 28, 2021
int compareEntity(FAS_Entity * entityA, FAS_Entity * entityB){

    //infoA.num.toDouble() < infoB.num.toDouble();
    double criteriaA;
    double criteriaB;

    /*
     * Please update your criteria here;
    */
//    criteriaA = entityA->getSize().x + entityA->getSize().y;
//    criteriaB = entityB->getSize().x + entityB->getSize().y;
//    criteriaA = entityA->getLengthX() + entityA->getLengthY();
//    criteriaB = entityB->getLengthX() + entityB->getLengthY();

    criteriaA = entityA->getSize().magnitude();
    criteriaB = entityB->getSize().magnitude();

    int compareResult = criteriaA > criteriaB;

    return compareResult;
}

//Sort entities based on certain criteria; April 28, 2021
//All entities will be sore according to criteria in compareEntity function;
//Then the entities will be displayed on GUI with from top 1 to top N. The purpose is to save display time;
//Plot 1000 entities will consume 80ms. For CAD file which contains more than 80K enitites, it will cost 6.4s to display.
//This will make user feel very slow respone. So right now, only top N entities will be displayed to save time;
int FAS_EntityContainer::sortEntities(){

    int len = entities.length();
    if (len == 0)
    {
        return 0;
    }

    qSort(entities.begin(), entities.end(), compareEntity);

    return 1;
}

//For fast display purpose, we need to drop some entities, this function is used to calcuate maximal displayed entities on GUI;
//It's a tradeoff between display performance and max entities;
int FAS_EntityContainer::calOptimalMaxDisplayEntities()
{
    int mcount = entities.count();
    int minThd = COMMONDEF->minDisplayEntities;
    int maxThd = COMMONDEF->maxDisplayEntities;

    int optimalNumber = minThd; //default value;

    //When user set minThd and maxThd same, return directly;
    if (minThd == maxThd)
    {
        optimalNumber = minThd;
        return optimalNumber;
    }

    //Otherwise, map the mcount to different bins for fast display purpose;
    if (mcount <= 1*maxThd)
    {
        optimalNumber = maxThd;
    }
    else
    {
        int binNumber = 8;
        int step =(int)((maxThd - minThd)/binNumber);
        if (mcount>1*maxThd && mcount<=2*maxThd)
        {
            optimalNumber = minThd + 1*step;
        }
        else if (mcount>2*maxThd && mcount<=3*maxThd)
        {
            optimalNumber = minThd + 2*step;
        }
        else if (mcount>3*maxThd && mcount<=4*maxThd)
        {
            optimalNumber = minThd + 3*step;
        }
        else if (mcount>4*maxThd && mcount<=5*maxThd)
        {
            optimalNumber = minThd + 4*step;
        }
        else if (mcount>5*maxThd && mcount<=6*maxThd)
        {
            optimalNumber = minThd + 5*step;
        }
        else if (mcount>6*maxThd && mcount<=7*maxThd)
        {
            optimalNumber = minThd + 6*step;
        }
        else if (mcount>7*maxThd && mcount<=8*maxThd)
        {
            optimalNumber = minThd + 7*step;
        }
        else
        {
            optimalNumber = minThd + 8*step;
        }
    }

    return optimalNumber;
}
