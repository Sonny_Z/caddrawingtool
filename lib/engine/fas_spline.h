/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_SPLINE_H
#define FAS_SPLINE_H

#include <vector>
#include "fas_entitycontainer.h"

/**
 * Holds the data that defines a line.
 */
struct FAS_SplineData
{
    FAS_SplineData() {}
    FAS_SplineData(int degree, bool closed);

    // Degree of the spline (1, 2, 3)
    size_t degree;
    // Closed flag.
    bool closed;
    // Control points of the spline.
    std::vector<FAS_Vector> controlPoints;
    std::vector<double> knotslist;



    size_t bkdegree;
    std::vector<FAS_Vector> bkcontrolPoints;
    std::vector<double> bkknotslist;
    FAS_Vector bkBasicData;

};

std::ostream& operator << (std::ostream& os, const FAS_SplineData& ld);

class FAS_Spline : public FAS_EntityContainer
{
public:
    FAS_Spline(FAS_EntityContainer* parent,
               const FAS_SplineData& d);
    FAS_Entity* clone() const override;
    // return FAS2::EntitySpline */
    FAS2::EntityType rtti() const override
    {
        return FAS2::EntitySpline;
    }
    // return false */
    bool isEdge() const override
    {
        return false;
    }

    // return Copy of data that defines the spline.
    const FAS_SplineData& getData() const
    {
        return data;
    }

    // Sets the splines degree (1-3).
    void setDegree(size_t deg);

    // return Degree of this spline curve (1-3).
    size_t getDegree() const;

    // return 0. */
    int getNumberOfKnots()
    {
        return 0;
    }

    // return Number of control points. */
    size_t getNumberOfControlPoints() const;
    bool isClosed() const;
    void setClosed(bool c);

    FAS_VectorSolutions getRefPoints() const override;
    FAS_Vector getNearestRef( const FAS_Vector& coord, double* dist = nullptr) const override;
    FAS_Vector getNearestSelectedRef( const FAS_Vector& coord, double* dist = nullptr) const override;

    // return Start point of the entity */
    FAS_Vector getStartpoint() const override;
    // return End point of the entity */
    FAS_Vector getEndpoint() const override;
    void update() override;

//    void resetBasicData() override;
    FAS_Vector getNearestEndpoint(const FAS_Vector& coord, double* dist = nullptr)const override;
    FAS_Vector getNearestCenter(const FAS_Vector& coord, double* dist = nullptr)const override;
    FAS_Vector getNearestMiddle(const FAS_Vector& coord, double* dist = nullptr, int middlePoints = 1)const override;
    FAS_Vector getNearestDist(double distance, const FAS_Vector& coord, double* dist = nullptr)const override;
    void addControlPoint(const FAS_Vector& v);
    void removeLastControlPoint();
    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void moveRef(const FAS_Vector& ref, const FAS_Vector& offset) override;
    void revertDirection() override;
    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
    const std::vector<FAS_Vector>& getControlPoints() const;
    friend std::ostream& operator << (std::ostream& os, const FAS_Spline& l);
    void calculateBorders() override;
    void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;

private:
    std::vector<double> knot(size_t num, size_t order) const;
    void rbspline(size_t npts, size_t k, size_t p1,
                  const std::vector<FAS_Vector>& b,
                  const std::vector<double>& h,
                  std::vector<FAS_Vector>& p) const;

    std::vector<double> knotu(size_t num, size_t order) const;
    void rbsplinu(size_t npts, size_t k, size_t p1,
                  const std::vector<FAS_Vector>& b,
                  const std::vector<double>& h,
                  std::vector<FAS_Vector>& p) const;

protected:
    FAS_SplineData data;
}
;

#endif
