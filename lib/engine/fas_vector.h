/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_VECTOR_H
#define FAS_VECTOR_H

#include <vector>
#include <iosfwd>
#include "fas.h"

class QString;

// Represents a 3d vector (x/y/z)
class FAS_Vector {
public:
    FAS_Vector(){}
    FAS_Vector(double vx, double vy, double vz=0.0);
    explicit FAS_Vector(double angle);
    explicit FAS_Vector(bool valid);

    //operator bool explicit and implicit conversion to bool
    explicit operator bool() const;

    //return point blongs matrix ID for CAD recognition
    QString getPointMatrixID() const;

    //return point ID in matrix
    QString getPointID() const;

    void set(double angle); // set to unit vector by the direction of angle
    void set(double vx, double vy, double vz=0.0);
    void setPolar(double radius, double angle);
    // construct by cartesian, or polar coordinates
    static FAS_Vector polar(double rho, double theta);

    double distanceTo(const FAS_Vector& v) const;
    double angle() const;
    double angleTo(const FAS_Vector& v) const;
    double angleBetween(const FAS_Vector& v1, const FAS_Vector& v2) const;
    double magnitude() const;
    double squared() const; //return square of length
    double squaredTo(const FAS_Vector& v1) const; //return square of length
    FAS_Vector lerp(const FAS_Vector& v, double t) const;

    bool isInWindow(const FAS_Vector& firstCorner, const FAS_Vector& secondCorner) const;
    bool isInWindowOrdered(const FAS_Vector& vLow, const FAS_Vector& vHigh) const;
    FAS_Vector toInteger();
    FAS_Vector move(const FAS_Vector& offset);
    FAS_Vector rotate(double ang);
    FAS_Vector rotate(const FAS_Vector& angleVector);
    FAS_Vector rotate(const FAS_Vector& center, double ang);
    FAS_Vector rotate(const FAS_Vector& center, const FAS_Vector& angleVector);
    FAS_Vector scale(double factor);
    FAS_Vector scale(const FAS_Vector& factor);
    FAS_Vector scale(const FAS_Vector& factor) const;
    FAS_Vector scale(const FAS_Vector& center, const FAS_Vector& factor);
    FAS_Vector mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2);
    double dotP(const FAS_Vector& v1) const;

    FAS_Vector operator + (const FAS_Vector& v) const;
    FAS_Vector operator + (double d) const;
    FAS_Vector operator - (const FAS_Vector& v) const;
    FAS_Vector operator - (double d) const;
    FAS_Vector operator * (const FAS_Vector& v) const;
    FAS_Vector operator / (const FAS_Vector& v) const;
    FAS_Vector operator * (double s) const;
    FAS_Vector operator / (double s) const;
    FAS_Vector operator - () const;
    FAS_Vector operator += (const FAS_Vector& v);
    FAS_Vector operator -= (const FAS_Vector& v);
    FAS_Vector operator *= (const FAS_Vector& v);
    FAS_Vector operator /= (const FAS_Vector& v);
    FAS_Vector operator *= (double s);
    FAS_Vector operator /= (double s);

    bool operator == (const FAS_Vector& v) const;
    bool operator != (const FAS_Vector& v) const
    {
        return !operator==(v);
    }
    bool operator == (bool valid) const;
    bool operator != (bool valid) const;

    static FAS_Vector minimum(const FAS_Vector& v1, const FAS_Vector& v2);
    static FAS_Vector maximum(const FAS_Vector& v1, const FAS_Vector& v2);
    static FAS_Vector crossP(const FAS_Vector& v1, const FAS_Vector& v2);
    static double dotP(const FAS_Vector& v1, const FAS_Vector& v2);
    FAS_Vector flipXY(void) const;

    friend std::ostream& operator << (std::ostream&, const FAS_Vector& v);

public:
    double x=0.;
    double y=0.;
    double z=0.;
    bool valid=false;
};


//Represents one to 4 vectors. Typically used to return multiplesolutions from a function.
class FAS_VectorSolutions {
public:
    typedef FAS_Vector value_type;
    FAS_VectorSolutions();
    FAS_VectorSolutions(const std::vector<FAS_Vector>& s);
    FAS_VectorSolutions(std::initializer_list<FAS_Vector> const& l);
    FAS_VectorSolutions(int num);
    ~FAS_VectorSolutions(){}

    void alloc(size_t num);
    void clear();
     //return indexed member, or invalid vector, if out of range
    FAS_Vector get(size_t i) const;
    const FAS_Vector& at(size_t i) const;
    const FAS_Vector&  operator [] (const size_t i) const;
    FAS_Vector&  operator [] (const size_t i);
    size_t getNumber() const;
    size_t size() const;
    void resize(size_t n);
    bool hasValid() const;
    void set(size_t i, const FAS_Vector& v);
    void push_back(const FAS_Vector& v);
    void removeAt(const size_t i);
    FAS_VectorSolutions& push_back(const FAS_VectorSolutions& v);
    void setTangent(bool t);
    bool isTangent() const;
    FAS_Vector getClosest(const FAS_Vector& coord, double* dist=nullptr, size_t* index=nullptr) const;
    double getClosestDistance(const FAS_Vector& coord, int counts = -1); //default to search all
    const std::vector<FAS_Vector>& getVector() const;
    std::vector<FAS_Vector>::const_iterator begin() const;
    std::vector<FAS_Vector>::const_iterator end() const;
    std::vector<FAS_Vector>::iterator begin();
    std::vector<FAS_Vector>::iterator end();
    void rotate(double ang);
    void rotate(const FAS_Vector& angleVector);
    void rotate(const FAS_Vector& center, double ang);
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector);
    void move(const FAS_Vector& vp);
    void scale(const FAS_Vector& center, const FAS_Vector& factor);
    void scale(const FAS_Vector& factor);
    /** switch x,y for all vectors */
    FAS_VectorSolutions flipXY(void) const;
    FAS_VectorSolutions operator = (const FAS_VectorSolutions& s);
    friend std::ostream& operator << (std::ostream& os,
                                      const FAS_VectorSolutions& s);
private:
    std::vector<FAS_Vector> vector;
    bool tangent;
};

#endif

// EOF
