/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_overlaybox.h"

//sort with alphabetical order
#include <iostream>
#include <QBrush>

#include "fas_graphicview.h"
#include "fas_painter.h"

FAS_OverlayBox::FAS_OverlayBox(FAS_EntityContainer* parent,
                             const FAS_OverlayBoxData& d)
    :FAS_AtomicEntity(parent), data(d)
{
}

FAS_Entity* FAS_OverlayBox::clone() const
{
    FAS_OverlayBox* l = new FAS_OverlayBox(*this);
    l->initId();
    return l;
}

void FAS_OverlayBox::draw(FAS_Painter* painter, FAS_GraphicView* view, double& /*patternOffset*/)
{
    if (painter==nullptr || view==nullptr)
    {
        return;
    }

    FAS_Vector v1=view->toGui(getCorner1());
    FAS_Vector v2=view->toGui(getCorner2());

    QRectF selectRect(v1.x, v1.y, v2.x - v1.x, v2.y - v1.y);

    if (v1.x > v2.x)
    {
        FAS_Pen p(FAS_Color(50,255,50),FAS2::Width00,FAS2::DashLine);
        painter->setPen(p);
        painter->fillRect(selectRect, FAS_Color(9, 255, 9, 90));
    }
    else
    {
        painter->setPen(QColor(50, 50, 255));
        painter->fillRect(selectRect, FAS_Color(9, 9, 255, 90));
    }
    painter->drawRect(v1, v2);
}

//Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_OverlayBox& l)
{
    os << " Line: " << l.getData() << "\n";
    return os;
}

std::ostream& operator << (std::ostream& os, const FAS_OverlayBoxData& ld)
{
        os << "(" << ld.corner1 <<
              "/" << ld.corner2 <<
              ")";
        return os;
}


