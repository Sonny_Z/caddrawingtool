/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_SYSTEM_H
#define FAS_SYSTEM_H

#include <QDir>
#include <QList>
#include <QSharedPointer>

#define FAS_SYSTEM FAS_System::instance()

// Class for some system methods such as file system operations.
class FAS_System
{
protected:
    FAS_System()
    {
        initialized = false;
    }

public:
    static FAS_System* instance()
    {
        if (uniqueInstance==nullptr)
        {
            uniqueInstance = new FAS_System();
        }
        return uniqueInstance;
    }

    void init(const QString& appName, const QString& appVersion,
              const QString& appDirName, const QString& appDir="");

    bool checkInit();
    bool createPaths(const QString& p);

    //return Users home directory.
    QString getHomeDir()
    {
        return QDir::homePath();
    }

    //return Current directory.
    QString getCurrentDir()
    {
        return QDir::currentPath();
    }

    //return a list of absolute paths to all font files found.
    QStringList getFontList()
    {
        QStringList ret = getFileList("fonts", "cxf");
        return ret;
    }

    //return a list of absolute paths to all NEW font files found.
    QStringList getNewFontList()
    {
        QStringList ret = getFileList("fonts", "lff");
        return ret;
    }

    //return a list of absolute paths to all hatch pattern files found.
    QStringList getPatternList()
    {
        QStringList ret = getFileList("patterns", "dxf");
        return ret;
    }

    QStringList getFileList(const QString& subDirectory, const QString& fileExtension);

    QStringList getDirectoryList(const QString& subDirectory);

    static QString getEncoding(const QString& str);

    // Returns ISO code for given locale. Needed for win32 to convert from system encodings.
    static QByteArray localeToISO(const QByteArray& locale);

protected:
    static FAS_System* uniqueInstance;

    QString appName;
    QString appVersion;
    QString appDirName;
    QString appDir;
    bool initialized;
};

#endif

