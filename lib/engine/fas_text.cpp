/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/
#include "fas.h"
#if MYL
#include "fas_font.h"
#include "fas_text.h"

#include "fas_fontlist.h"
#include "fas_insert.h"
#include "fas_math.h"

/**
 * Constructor.
 */
FAS_Text::FAS_Text(FAS_EntityContainer* parent,
                 const FAS_TextData& d)
        : FAS_EntityContainer(parent), data(d) {

    usedTextHeight = 0.0;
    usedTextWidth = 0.0;
    setText(data.text);
}



/**
 * Sets a new text. The entities representing the
 * text are updated.
 */
void FAS_Text::setText(const FAS_String& t) {
    data.text = t;

    // handle some special flags embedded in the text:
    if (data.text.left(4)=="\\A0;") {
        data.text = data.text.mid(4);
        data.valign = FAS_TextData::VABottom;
    } else if (data.text.left(4)=="\\A1;") {
        data.text = data.text.mid(4);
        data.valign = FAS_TextData::VAMiddle;
    } else if (data.text.left(4)=="\\A2;") {
        data.text = data.text.mid(4);
        data.valign = FAS_TextData::VATop;
    }

    if (data.updateMode==FAS2::Update) {
        update();
        //calculateBorders();
    }
}



/**
 * Gets the alignment as an int.
 *
 * @return  1: top left ... 9: bottom right
 */
int FAS_Text::getAlignment() {
    if (data.valign=FAS_TextData::VATop) {
        if (data.halign==FAS_TextData::HALeft) {
            return 1;
        } else if (data.halign==FAS_TextData::HACenter) {
            return 2;
        } else if (data.halign==FAS_TextData::HARight) {
            return 3;
        }
    } else if (data.valign==FAS_TextData::VAMiddle) {
        if (data.halign==FAS_TextData::HALeft) {
            return 4;
        } else if (data.halign==FAS_TextData::HACenter) {
            return 5;
        } else if (data.halign==FAS_TextData::HARight) {
            return 6;
        }
    } else if (data.valign==FAS_TextData::VABottom) {
        if (data.halign==FAS_TextData::HALeft) {
            return 7;
        } else if (data.halign==FAS_TextData::HACenter) {
            return 8;
        } else if (data.halign==FAS_TextData::HARight) {
            return 9;
        }
    }

    return 1;
}



/**
 * Sets the alignment from an int.
 *
 * @param a 1: top left ... 9: bottom right
 */
void FAS_Text::setAlignment(int a) {
    switch (a%3) {
    default:
    case 1:
        data.halign = FAS_TextData::HALeft;
        break;
    case 2:
        data.halign = FAS_TextData::HACenter;
        break;
    case 0:
        data.halign = FAS_TextData::HARight;
        break;
    }

    switch ((int)ceil(a/3.0)) {
    default:
    case 1:
        data.valign = FAS_TextData::VATop;
        break;
    case 2:
        data.valign = FAS_TextData::VAMiddle;
        break;
    case 3:
        data.valign = FAS_TextData::VABottom;
        break;
    }

}



/**
 * @return Number of lines in this text entity.
 */
int FAS_Text::getNumberOfLines() {
    int c=1;

    for (int i=0; i<(int)data.text.length(); ++i) {
        if (data.text.at(i).unicode()==0x0A) {
            c++;
        }
    }

    return c;
}




/**
 * Updates the Inserts (letters) of this text. Called when the
 * text or it's data, position, alignment, .. changes.
 * This method also updates the usedTextWidth / usedTextHeight property.
 */
void FAS_Text::update() {

    //FAS_DEBUG->print("FAS_Text::update");

    clear();

    if (isUndone()) {
        return;
    }

    usedTextWidth = 0.0;
    usedTextHeight = 0.0;
    //this->data.angle=90*M_PI/180;
    FAS_Font* font = FAS_FONTLIST->requestFont(data.style);

    if (font==NULL) {
        return;
    }

    FAS_Vector letterPos = FAS_Vector(0.0, -9.0);
    FAS_Vector letterSpace = FAS_Vector(font->getLetterSpacing(), 0.0);
    FAS_Vector space = FAS_Vector(font->getWordSpacing(), 0.0);
    int lineCounter = 0;

    // Every single text line gets stored in this entity container
    //  so we can move the whole line around easely:
    FAS_EntityContainer* oneLine = new FAS_EntityContainer(this);

    // First every text line is created with
    //   alignement: top left
    //   angle: 0
    //   height: 9.0
    // Rotation, scaling and centering is done later

    // For every letter:
    for (int i=0; i<(int)data.text.length(); ++i) {
        switch (data.text.at(i).unicode()) {
        case 0x0A:
            // line feed:
            updateAddLine(oneLine, lineCounter++);
            oneLine = new FAS_EntityContainer(this);
            letterPos = FAS_Vector(0.0, -9.0);
            break;

        case 0x20:
            // Space:
            letterPos+=space;
            break;

        case 0x5C: {
                // code (e.g. \S, \P, ..)
                i++;
                int ch = data.text.at(i).unicode();
                switch (ch) {
                case 'P':
                    updateAddLine(oneLine, lineCounter++);
                    oneLine = new FAS_EntityContainer(this);
                    letterPos = FAS_Vector(0.0, -9.0);
                    break;

                case 'S': {
                        FAS_String up;
                        FAS_String dw;
                        //letterPos += letterSpace;

                        // get upper string:
                        i++;
                        while (data.text.at(i).unicode()!='^' &&
                               //data.text.at(i).unicode()!='/' &&
                               data.text.at(i).unicode()!='\\' &&
                               //data.text.at(i).unicode()!='#' &&
                                i<(int)data.text.length()) {
                            up += data.text.at(i);
                            i++;
                        }

                        i++;

                        if (data.text.at(i-1).unicode()=='^' &&
                             data.text.at(i).unicode()==' ') {
                            i++;
                        }

                        // get lower string:
                        while (data.text.at(i).unicode()!=';' &&
                                i<(int)data.text.length()) {
                            dw += data.text.at(i);
                            i++;
                        }

                        // add texts:
                        FAS_Text* upper =
                            new FAS_Text(
                                oneLine,
                                FAS_TextData(letterPos + FAS_Vector(0.0,9.0),
                                            4.0, 100.0, FAS_TextData::VATop,
                                            FAS_TextData::HALeft,
                                            FAS2::LeftToRight, FAS2::Exact,
                                            1.0, up, data.style,
                                            0.0, FAS2::Update));
                        upper->setLayer(NULL);
                        upper->setPen(FAS_Pen(FAS2::FlagInvalid));
                        oneLine->addEntity(upper);

                        FAS_Text* lower =
                            new FAS_Text(
                                oneLine,
                                FAS_TextData(letterPos+FAS_Vector(0.0,4.0),
                                            4.0, 100.0, FAS_TextData::VATop,
                                            FAS_TextData::HALeft,
                                            FAS2::LeftToRight, FAS2::Exact,
                                            1.0, dw, data.style,
                                            0.0, FAS2::Update));
                        lower->setLayer(NULL);
                        lower->setPen(FAS_Pen(FAS2::FlagInvalid));
                        oneLine->addEntity(lower);

                        // move cursor:
                        upper->calculateBorders();
                        lower->calculateBorders();

                        double w1 = upper->getSize().x;
                        double w2 = lower->getSize().x;

                        if (w1>w2) {
                            letterPos += FAS_Vector(w1, 0.0);
                        } else {
                            letterPos += FAS_Vector(w2, 0.0);
                        }
                        letterPos += letterSpace;
                    }
                    break;

                default:
                    break;
                }
            }
            break;

        default: {
                // One Letter:
                if (font->findLetter(QString(data.text.at(i))) != NULL) {

                    //FAS_DEBUG->print("FAS_Text::update: insert a "
                     // "letter at pos: %f/%f", letterPos.x, letterPos.y);

                    FAS_InsertData d(FAS_String(data.text.at(i)),
                                    letterPos,
                                    FAS_Vector(1.0, 1.0),
                                    0.0,
                                    1,1, FAS_Vector(0.0,0.0),
                                    font->getLetterList(), FAS2::NoUpdate);

                    FAS_Insert* letter = new FAS_Insert(this, d);
                    FAS_Vector letterWidth;
                    letter->setPen(FAS_Pen(FAS2::FlagInvalid));
                    letter->setLayer(NULL);
                    letter->update();
                    letter->forcedCalculateBorders();

                    // until 2.0.4.5:
                    //letterWidth = FAS_Vector(letter->getSize().x, 0.0);
                    // from 2.0.4.6:
                    letterWidth = FAS_Vector(letter->getMax().x-letterPos.x, 0.0);

                    oneLine->addEntity(letter);

                    // next letter position:
                    letterPos += letterWidth;
                    letterPos += letterSpace;
                }
            }
            break;
        }
    }

    updateAddLine(oneLine, lineCounter);
    usedTextHeight -= data.height*data.lineSpacingFactor*1.6
                      - data.height;
    forcedCalculateBorders();

    //FAS_DEBUG->print("FAS_Text::update: OK");
}



/**
 * Used internally by update() to add a text line created with
 * default values and alignment to this text container.
 *
 * @param textLine The text line.
 * @param lineCounter Line number.
 */
void FAS_Text::updateAddLine(FAS_EntityContainer* textLine, int lineCounter) {
    //FAS_DEBUG->print("FAS_Text::updateAddLine: width: %f", textLine->getSize().x);

    //textLine->forcedCalculateBorders();
    //FAS_DEBUG->print("FAS_Text::updateAddLine: width 2: %f", textLine->getSize().x);

    // Move to correct line position:
    textLine->move(FAS_Vector(0.0, -9.0 * lineCounter
                             * data.lineSpacingFactor * 1.6));

    textLine->forcedCalculateBorders();
    FAS_Vector textSize = textLine->getSize();

    //FAS_DEBUG->print("FAS_Text::updateAddLine: width 2: %f", textSize.x);

    // Horizontal Align:
    switch (data.halign) {
    case FAS_TextData::HACenter:
        //FAS_DEBUG->print("FAS_Text::updateAddLine: move by: %f", -textSize.x/2.0);
        textLine->move(FAS_Vector(-textSize.x/2.0, 0.0));
        break;

    case FAS_TextData::HARight:
        textLine->move(FAS_Vector(-textSize.x, 0.0));
        break;

    default:
        break;
    }

    // Vertical Align:
    double vSize = getNumberOfLines()*9.0*data.lineSpacingFactor*1.6
                   - (9.0*data.lineSpacingFactor*1.6 - 9.0);

    switch (data.valign) {
    case FAS_TextData::VAMiddle:
        textLine->move(FAS_Vector(0.0, vSize/2.0));
        break;

    case FAS_TextData::VABottom:
        textLine->move(FAS_Vector(0.0, vSize));
        break;

    default:
        break;
    }

    // Scale:
    textLine->scale(FAS_Vector(0.0,0.0),
                    FAS_Vector(data.height/9.0, data.height/9.0));

    textLine->forcedCalculateBorders();

    // Update actual text size (before rotating, after scaling!):
    if (textLine->getSize().x>usedTextWidth) {
        usedTextWidth = textLine->getSize().x;
    }

    usedTextHeight += data.height*data.lineSpacingFactor*1.6;

    // Rotate:
    textLine->rotate(FAS_Vector(0.0,0.0), data.angle);

    // Move:
    textLine->move(data.insertionPoint);
    textLine->setPen(FAS_Pen(FAS2::FlagInvalid));
    textLine->setLayer(NULL);
    textLine->forcedCalculateBorders();

    addEntity(textLine);
}



FAS_VectorSolutions FAS_Text::getRefPoints() {
    std::vector<FAS_Vector> mv;
    mv.push_back(data.insertionPoint);
    //FAS_VectorSolutions ret(data.insertionPoint);
    FAS_VectorSolutions ret(mv);
    return ret;
}


FAS_Vector FAS_Text::getNearestRef(const FAS_Vector& coord,
                                     double* dist) {

    //return getRefPoints().getClosest(coord, dist);
    return FAS_Entity::getNearestRef(coord, dist);
}


void FAS_Text::move(FAS_Vector offset) {
    data.insertionPoint.move(offset);
    update();
}



void FAS_Text::rotate(FAS_Vector center, double angle) {
    data.insertionPoint.rotate(center, angle);
    data.angle = FAS_Math::correctAngle(data.angle+angle);
    update();
}



void FAS_Text::scale(FAS_Vector center, FAS_Vector factor) {
    data.insertionPoint.scale(center, factor);
    data.width*=factor.x;
    data.height*=factor.x;
    update();
}



void FAS_Text::mirror(FAS_Vector axisPoint1, FAS_Vector axisPoint2) {
    data.insertionPoint.mirror(axisPoint1, axisPoint2);
    //double ang = axisPoint1.angleTo(axisPoint2);
    bool readable = FAS_Math::isAngleReadable(data.angle);

    FAS_Vector vec;
    vec.setPolar(1.0, data.angle);
    vec.mirror(FAS_Vector(0.0,0.0), axisPoint2-axisPoint1);
    data.angle = vec.angle();

    bool corr;
    data.angle = FAS_Math::makeAngleReadable(data.angle, readable, &corr);

    if (corr) {
        if (data.halign==FAS_TextData::HALeft) {
            data.halign=FAS_TextData::HARight;
        } else if (data.halign==FAS_TextData::HARight) {
            data.halign=FAS_TextData::HALeft;
        }
    } else {
        if (data.valign==FAS_TextData::VATop) {
            data.valign=FAS_TextData::VABottom;
        } else if (data.valign==FAS_TextData::VABottom) {
            data.valign=FAS_TextData::VATop;
        }
    }
    update();
}



bool FAS_Text::hasEndpointsWithinWindow(FAS_Vector /*v1*/, FAS_Vector /*v2*/) {
    return false;
}



/**
 * Implementations must stretch the given range of the entity
 * by the given offset.
 */
void FAS_Text::stretch(FAS_Vector firstCorner,
                      FAS_Vector secondCorner,
                      FAS_Vector offset) {

    if (getMin().isInWindow(firstCorner, secondCorner) &&
            getMax().isInWindow(firstCorner, secondCorner)) {

        move(offset);
    }
}



/**
 * Dumps the point's data to stdout.
 */
std::ostream& operator << (std::ostream& os, const FAS_Text& p) {
    os << " Text: " << p.getData() << "\n";
    return os;
}
void FAS_Text::draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset)
{
//    qDebug()<<"draw1";
    FAS_EntityContainer::draw(painter,view,patternOffset);
}
void FAS_Text::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data)
{
//    qDebug()<<"drawInsertEntity1";
    FAS_EntityContainer::drawInsertEntity(painter,view,patternOffset,data);
}
FAS_Vector FAS_Text::getMiddlePoint() const
{
    return (getMin() + getMax()) * 0.5;
}
FAS_Vector FAS_Text::getNearestEndpoint(const FAS_Vector& coord, double* dist)const
{
    if (dist) {
        *dist = data.insertionPoint.distanceTo(coord);
    }
    return data.insertionPoint;
}
void FAS_Text::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    data.insertionPoint.rotate(center, angleVector);
    data.secondPoint.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angleVector.angle());
}
FAS_Vector FAS_Text::getNearestPointOnEntity(const FAS_Vector& coord, bool onEntity, double* dist, FAS_Entity** entity) const
{
    if(entity)
    {
        *entity = const_cast<FAS_Text*>(this);
    }
    double distance = 0.0;
    FAS_Vector result = coord;
    if(coord.isInWindow(minV,maxV))
    {
        distance = 0.0;
    }else
    {
        FAS_Vector middle = (minV + maxV) * 0.5;
        distance = coord.distanceTo(middle);
        result = middle;
    }

    if(dist)
    {
        *dist = distance;
    }
    return result;
}



FAS_Vector FAS_Text::getNearestDist(double distance, const FAS_Vector& coord, double* dist) const
{
    //double width = data.widthRel * data.height;
    double width = data.width;
    FAS_Vector middle = data.insertionPoint + FAS_Vector(width, data.height) * 0.5;
    if(dist)
    {
        *dist = coord.distanceTo(middle);
    }
    return middle;
}
FAS_Vector FAS_Text::getNearestMiddle(const FAS_Vector& coord, double* dist, int middlePoints) const
{
    //double width = data.widthRel * data.height;
    double width = data.width;
    middlePoints = 1;
    FAS_Vector middle = data.insertionPoint + FAS_Vector(width, data.height) * 0.5;
    if(dist)
    {
        *dist = coord.distanceTo(middle);
    }
    return middle;
}

void FAS_Text::calculateBorders()
{
    //resetBorders();
    this->minV.x=FAS_MAXDOUBLE;
    this->minV.y=FAS_MAXDOUBLE;
    this->maxV.x=FAS_MINDOUBLE;
    this->maxV.y=FAS_MINDOUBLE;
    for (int i=0;i<entities.count();i++)
    {
        FAS_Entity* e=entities[i];
        if(e->isContainer())
        {
            FAS_EntityContainer* container1=dynamic_cast<FAS_EntityContainer*>(e);
            for(int k=0;k<container1->count();k++)
            {

                FAS_Entity* ek=container1->getEntityList().at(k);
                FAS_Insert* insert=dynamic_cast<FAS_Insert*>(ek);
                if(insert!=NULL)
                {
                    for(int j=0;j<insert->getEntityList().count();j++)
                    {
                        FAS_Entity* e1=insert->getEntityList().at(j);
                        e1->calculateBorders();
                        FAS_Vector v1=e1->getMin();
                        FAS_Vector v2=e1->getMax();
                        if(this->minV.x>e1->getMin().x)
                        {
                            this->minV.x=e1->getMin().x;
                        }
                        if(this->minV.y>e1->getMin().y)
                        {
                            this->minV.y=e1->getMin().y;
                        }
                        if(this->maxV.x<e1->getMax().x)
                        {
                            this->maxV.x=e1->getMax().x;
                        }
                        if(this->maxV.y<e1->getMax().y)
                        {
                            this->maxV.y=e1->getMax().y;
                        }
                    }//end for
                }//end if
             }//end for
        }//end if


    }
    // needed for correcting corrupt data (PLANS.dxf)

   /* double left = 0.0, right = 0.0, bottom = 0.0, top = 0.0;
    double width = data.height * data.text.size() * 0.6;
    switch (data.halign) {
    case FAS_TextData::HALeft:
        left = data.insertionPoint.x;
        right = left + width;
        break;
    case FAS_TextData::HARight:
        right = data.insertionPoint.x;
        left = right - width;
        break;
    case FAS_TextData::HACenter:
    case FAS_TextData::HAMiddle:
    case FAS_TextData::HAFit:
        left = data.insertionPoint.x - 0.5 * width;
        right = data.insertionPoint.x + 0.5 * width;
        break;
    default:
        left = data.insertionPoint.x;
        right = left + width;
        break;
    }
    switch (data.valign) {
    case FAS_TextData::VABottom:
    case FAS_TextData::VABaseline:
        bottom = data.insertionPoint.y;
        top = bottom + data.height;
        break;
    case FAS_TextData::VATop:
        top = data.insertionPoint.y;
        bottom = top - data.height;
        break;
    case FAS_TextData::VAMiddle:
        top = data.insertionPoint.y + data.height * 0.5;
        bottom = data.insertionPoint.y - data.height * 0.5;
        break;
    default:
        bottom = data.insertionPoint.y;
        top = bottom + data.height;
        break;
    }

    minV = FAS_Vector(left, bottom);
    maxV = FAS_Vector(right, top);*/
}
void FAS_Text::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center1, const double& angle,const FAS_Vector& factor){

    FAS_Vector secondPoint=getSecondPoint();
    FAS_Vector bksecondPoint=data.bksecondPoint;
    if(!bksecondPoint.valid)
    {
        data.bkinsertionPoint=data.insertionPoint;
        data.bksecondPoint=secondPoint;
        data.bkheight=data.height;
        data.bkangle=data.angle;
    }else{
        data.insertionPoint=data.bkinsertionPoint;
        data.secondPoint=data.bksecondPoint;
        data.height=data.bkheight;
        data.angle=data.bkangle;
    }
    for (int i=0;i<entities.count();i++)
    {
        FAS_Entity* e=entities[i];
        if(e->isContainer())
        {
            FAS_EntityContainer* container1=dynamic_cast<FAS_EntityContainer*>(e);
            for(int k=0;k<container1->count();k++)
            {

                FAS_Entity* ek=container1->getEntityList().at(k);
                FAS_Insert* insert=dynamic_cast<FAS_Insert*>(ek);
                if(insert!=NULL)
                {
                    for(int j=0;j<insert->getEntityList().count();j++)
                    {
                        FAS_Entity* e1=insert->getEntityList().at(j);
                        e1->resetBasicData(offset,center1,angle,factor);


                    }//end for
                }//end if insert
             }//end for
        }//end if container
    }//end for
}

void FAS_Text::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){
    data.insertionPoint.move(offset);
    data.secondPoint.move(offset);


    FAS_Vector angleVector(angle);
    data.insertionPoint.rotate(center, angleVector);
    data.secondPoint.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angle);


    data.insertionPoint.scale(center, factor);
    data.secondPoint.scale(center, factor);
    data.height*=factor.x;
    //FAS_EntityContainer::moveEntityBoarder(offset,center,angle,factor);
    for (int i=0;i<entities.count();i++)
    {
        FAS_Entity* e=entities[i];
        if(e->isContainer())
        {
            FAS_EntityContainer* container1=dynamic_cast<FAS_EntityContainer*>(e);
            for(int k=0;k<container1->count();k++)
            {

                FAS_Entity* ek=container1->getEntityList().at(k);
                FAS_Insert* insert=dynamic_cast<FAS_Insert*>(ek);
                if(insert!=NULL)
                {
                    for(int j=0;j<insert->getEntityList().count();j++)
                    {
                        FAS_Entity* e1=insert->getEntityList().at(j);
                        e1->moveEntityBoarder(offset,center,angle,factor);


                    }//end for
                }//end if insert
             }//end for
        }//end if container
    }//end for

}
#else
#include "fas_text.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>

#include "fas_graphicview.h"
#include "fas_math.h"
#include "fas_painter.h"

FAS_TextData::FAS_TextData(const FAS_Vector& _insertionPoint,
                           const FAS_Vector& _secondPoint,
                           double _height,
                           double _widthRel,
                           VAlign _valign,
                           HAlign _halign,
                           TextGeneration _textGeneration,
                           const QString& _text,
                           const QString& _style,
                           double _angle,
                           FAS2::UpdateMode _updateMode):
    insertionPoint(_insertionPoint)
  ,secondPoint(_secondPoint)
  ,height(_height)
  ,widthRel(_widthRel)
  ,valign(_valign)
  ,halign(_halign)
  ,textGeneration(_textGeneration)
  ,text(_text)
  ,style(_style)
  ,angle(_angle)
  ,updateMode(_updateMode)
{
}


std::ostream& operator << (std::ostream& os, const FAS_TextData& td) {
    os << "("
       <<td.insertionPoint<<','
      <<td.secondPoint<<','
     <<td.height<<','
    <<td.widthRel<<','
    <<td.valign<<','
    <<td.halign<<','
    <<td.textGeneration<<','
    <<td.text.toLatin1().data() <<','
    <<td.style.toLatin1().data()<<','
    <<td.angle<<','
    <<td.updateMode<<','
    <<")";
    return os;
}

/**
 * Constructor.
 */
FAS_Text::FAS_Text(FAS_EntityContainer* parent, const FAS_TextData& d)
    :data(d)
{
    usedTextHeight = 0.0;
    usedTextWidth = 0.0;
    setText(data.text);
}

FAS_Entity* FAS_Text::clone() const{
    FAS_Text* t = new FAS_Text(*this);
    t->initId();
    return t;
}

/**
 * Sets a new text. The entities representing the
 * text are updated.
 */
void FAS_Text::setText(const QString& t) {
    data.text = t;

    // handle some special flags embedded in the text:
    if (data.text.left(4)=="\\A0;") {
        data.text = data.text.mid(4);
        data.valign = FAS_TextData::VABottom;
    } else if (data.text.left(4)=="\\A1;") {
        data.text = data.text.mid(4);
        data.valign = FAS_TextData::VAMiddle;
    } else if (data.text.left(4)=="\\A2;") {
        data.text = data.text.mid(4);
        data.valign = FAS_TextData::VATop;
    }

    if (data.updateMode==FAS2::Update) {
        update();
        //calculateBorders();
    }
}

/**
 * Gets the alignment as an int.
 *
 * @return  1: top left ... 9: bottom right
 */
// bad function, this is MText style align
int FAS_Text::getAlignment() {
    if (data.valign==FAS_TextData::VATop) {
        if (data.halign==FAS_TextData::HALeft) {
            return 1;
        } else if (data.halign==FAS_TextData::HACenter) {
            return 2;
        } else if (data.halign==FAS_TextData::HARight) {
            return 3;
        }
    } else if (data.valign==FAS_TextData::VAMiddle) {
        if (data.halign==FAS_TextData::HALeft) {
            return 4;
        } else if (data.halign==FAS_TextData::HACenter) {
            return 5;
        } else if (data.halign==FAS_TextData::HARight) {
            return 6;
        }
    } else if (data.valign==FAS_TextData::VABaseline) {
        if (data.halign==FAS_TextData::HALeft) {
            return 7;
        } else if (data.halign==FAS_TextData::HACenter) {
            return 8;
        } else if (data.halign==FAS_TextData::HARight) {
            return 9;
        }
    } else if (data.valign==FAS_TextData::VABottom) {
        if (data.halign==FAS_TextData::HALeft) {
            return 10;
        } else if (data.halign==FAS_TextData::HACenter) {
            return 11;
        } else if (data.halign==FAS_TextData::HARight) {
            return 12;
        }
    }
    if (data.halign==FAS_TextData::HAFit) {
        return 13;
    } else if (data.halign==FAS_TextData::HAAligned) {
        return 14;
    } else if (data.halign==FAS_TextData::HAMiddle) {
        return 15;
    }

    return 1;
}



/**
 * Sets the alignment from an int.
 *
 * @param a 1: top left ... 9: bottom right
 */
// bad function, this is MText style align
void FAS_Text::setAlignment(int a) {
    switch (a%3) {
    default:
    case 1:
        data.halign = FAS_TextData::HALeft;
        break;
    case 2:
        data.halign = FAS_TextData::HACenter;
        break;
    case 0:
        data.halign = FAS_TextData::HARight;
        break;
    }

    switch ((int)ceil(a/3.0)) {
    default:
    case 1:
        data.valign = FAS_TextData::VATop;
        break;
    case 2:
        data.valign = FAS_TextData::VAMiddle;
        break;
    case 3:
        data.valign = FAS_TextData::VABaseline;
        break;
    case 4:
        data.valign = FAS_TextData::VABottom;
        break;
    }
    if (a > 12) {
        data.valign = FAS_TextData::VABaseline;
        if (a == 13) {
            data.halign = FAS_TextData::HAFit;
        } else if (a == 14) {
            data.halign = FAS_TextData::HAAligned;
        } else if (a == 15) {
            data.halign = FAS_TextData::HAMiddle;
        }
    }

}

/**
 * Updates the Inserts (letters) of this text. Called when the
 * text or it's data, position, alignment, .. changes.
 * This method also updates the usedTextWidth / usedTextHeight property.
 */
void FAS_Text::update()
{
    calculateBorders();
}


FAS_Vector FAS_Text::getNearestEndpoint(const FAS_Vector& coord, double* dist)const
{
    if (dist) {
        *dist = data.insertionPoint.distanceTo(coord);
    }
    return data.insertionPoint;
}

FAS_VectorSolutions FAS_Text::getRefPoints() const
{
    FAS_VectorSolutions ret({data.insertionPoint, data.secondPoint});
    return ret;
}

void FAS_Text::move(const FAS_Vector& offset)
{
    data.insertionPoint.move(offset);
    data.secondPoint.move(offset);
}



void FAS_Text::rotate(const FAS_Vector& center, const double& angle)
{
    FAS_Vector angleVector(angle);
    data.insertionPoint.rotate(center, angleVector);
    data.secondPoint.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angle);
}
void FAS_Text::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    data.insertionPoint.rotate(center, angleVector);
    data.secondPoint.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angleVector.angle());
}



void FAS_Text::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    data.insertionPoint.scale(center, factor);
    data.secondPoint.scale(center, factor);
    data.height*=factor.x;
    update();
}

void FAS_Text::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    bool readable = FAS_Math::isAngleReadable(data.angle);

    FAS_Vector vec = FAS_Vector::polar(1.0, data.angle);
    vec.mirror(FAS_Vector(0.0,0.0), axisPoint2-axisPoint1);
    data.angle = vec.angle();

    bool corr;
    data.angle = FAS_Math::makeAngleReadable(data.angle, readable, &corr);

    if (corr) {
        data.insertionPoint.mirror(axisPoint1, axisPoint2);
        data.secondPoint.mirror(axisPoint1, axisPoint2);
        if (data.halign==FAS_TextData::HALeft) {
            data.halign=FAS_TextData::HARight;
        } else if (data.halign==FAS_TextData::HARight) {
            data.halign=FAS_TextData::HALeft;
        } else if (data.halign==FAS_TextData::HAFit || data.halign==FAS_TextData::HAAligned) {
            FAS_Vector tmp = data.insertionPoint;
            data.insertionPoint = data.secondPoint;
            data.secondPoint = tmp;
        }
    }
    else
    {
        FAS_Vector minP = FAS_Vector(getMin().x, getMax().y);
        minP = minP.mirror(axisPoint1, axisPoint2);
        double mirrAngle = axisPoint1.angleTo(axisPoint2)*2.0;
        data.insertionPoint.move(minP - getMin());
        data.secondPoint.move(minP - getMin());
        data.insertionPoint.rotate(minP, mirrAngle);
        data.secondPoint.rotate(minP, mirrAngle);
    }
    update();
}

bool FAS_Text::hasEndpointsWithinWindow(const FAS_Vector& /*v1*/, const FAS_Vector& /*v2*/) {
    return false;
}


/**
 * Implementations must stretch the given range of the entity
 * by the given offset.
 */
void FAS_Text::stretch(const FAS_Vector& firstCorner, const FAS_Vector& secondCorner, const FAS_Vector& offset)
{
    if (getMin().isInWindow(firstCorner, secondCorner) &&
            getMax().isInWindow(firstCorner, secondCorner)) {

        move(offset);
    }
}

/**
 * Dumps the point's data to stdout.
 */
std::ostream& operator << (std::ostream& os, const FAS_Text& p)
{
    os << " Text: " << p.getData() << "\n";
    return os;
}


void FAS_Text::draw(FAS_Painter* painter, FAS_GraphicView* view, double& /*patternOffset*/)
{
    if (!(painter && view))
    {
        return;
    }
    COMMONDEF->drawEntityCount++;

    if (!view->isPrintPreview() && !view->isPrinting())
    {
        if (view->isPanning() || view->toGuiDY(getHeight()) < 4)
        {
            painter->drawRect(view->toGui(getMin()), view->toGui(getMax()));
            return;
        }
    }

    double xLeft = view->toGui(minV).x;
    double yTop = view->toGui(maxV).y;
    double height = this->getHeight() * 1.5;
    double width = height * this->getText().count() * 1.5;
    QFont font("Courier", height * view->getFactor().y * 0.55);
    painter->setFont(font);
    painter->drawTextH(xLeft, yTop, view->getFactor().x * width, view->getFactor().y * height, getText());
}


FAS_Vector FAS_Text::getNearestPointOnEntity(const FAS_Vector& coord, bool onEntity, double* dist, FAS_Entity** entity) const
{
    if(entity)
    {
        *entity = const_cast<FAS_Text*>(this);
    }
    double distance = 0.0;
    FAS_Vector result = coord;
    if(coord.isInWindow(minV,maxV))
    {
        distance = 0.0;
    }else
    {
        FAS_Vector middle = (minV + maxV) * 0.5;
        distance = coord.distanceTo(middle);
        result = middle;
    }

    if(dist)
    {
        *dist = distance;
    }
    return result;
}

FAS_Vector FAS_Text::getNearestMiddle(const FAS_Vector& coord, double* dist, int middlePoints) const
{
    double width = data.widthRel * data.height;
    middlePoints = 1;
    FAS_Vector middle = data.insertionPoint + FAS_Vector(width, data.height) * 0.5;
    if(dist)
    {
        *dist = coord.distanceTo(middle);
    }
    return middle;
}

FAS_Vector FAS_Text::getNearestDist(double distance, const FAS_Vector& coord, double* dist) const
{
    double width = data.widthRel * data.height;
    FAS_Vector middle = data.insertionPoint + FAS_Vector(width, data.height) * 0.5;
    if(dist)
    {
        *dist = coord.distanceTo(middle);
    }
    return middle;
}

FAS_Vector FAS_Text::getMiddlePoint() const
{
    return (getMin() + getMax()) * 0.5;
}

void FAS_Text::calculateBorders()
{
    double left = 0.0, right = 0.0, bottom = 0.0, top = 0.0;
    double width = data.height * data.text.size() * 0.6;
    switch (data.halign) {
    case FAS_TextData::HALeft:
        left = data.insertionPoint.x;
        right = left + width;
        break;
    case FAS_TextData::HARight:
        right = data.insertionPoint.x;
        left = right - width;
        break;
    case FAS_TextData::HACenter:
    case FAS_TextData::HAMiddle:
    case FAS_TextData::HAFit:
        left = data.insertionPoint.x - 0.5 * width;
        right = data.insertionPoint.x + 0.5 * width;
        break;
    default:
        left = data.insertionPoint.x;
        right = left + width;
        break;
    }
    switch (data.valign) {
    case FAS_TextData::VABottom:
    case FAS_TextData::VABaseline:
        bottom = data.insertionPoint.y;
        top = bottom + data.height;
        break;
    case FAS_TextData::VATop:
        top = data.insertionPoint.y;
        bottom = top - data.height;
        break;
    case FAS_TextData::VAMiddle:
        top = data.insertionPoint.y + data.height * 0.5;
        bottom = data.insertionPoint.y - data.height * 0.5;
        break;
    default:
        bottom = data.insertionPoint.y;
        top = bottom + data.height;
        break;
    }

    minV = FAS_Vector(left, bottom);
    maxV = FAS_Vector(right, top);
}
void FAS_Text::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center1, const double& angle,const FAS_Vector& factor){

    FAS_Vector secondPoint=getSecondPoint();
    FAS_Vector bksecondPoint=data.bksecondPoint;
    if(!bksecondPoint.valid)
    {
        data.bkinsertionPoint=data.insertionPoint;
        data.bksecondPoint=secondPoint;
        data.bkheight=data.height;
        data.bkangle=data.angle;
    }else{
        data.insertionPoint=data.bkinsertionPoint;
        data.secondPoint=data.bksecondPoint;
        data.height=data.bkheight;
        data.angle=data.bkangle;
    }
}

void FAS_Text::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){
    data.insertionPoint.move(offset);
    data.secondPoint.move(offset);


    FAS_Vector angleVector(angle);
    data.insertionPoint.rotate(center, angleVector);
    data.secondPoint.rotate(center, angleVector);
    data.angle = FAS_Math::correctAngle(data.angle+angle);


    data.insertionPoint.scale(center, factor);
    data.secondPoint.scale(center, factor);
    data.height*=factor.x;
    //update();

}
#endif
