/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_LEADER_H
#define FAS_LEADER_H

#include "fas_entity.h"
#include "fas_entitycontainer.h"

// Holds the data that defines a leader.
class FAS_LeaderData
{
public:
    FAS_LeaderData() {}
    FAS_LeaderData(bool arrowHeadFlag)
    {
        arrowHead = arrowHeadFlag;
    }

    friend std::ostream& operator << (std::ostream& os,
                                      const FAS_LeaderData& /*ld*/);
    bool arrowHead;
};


//Class for a leader entity (kind of a polyline arrow).
class FAS_Leader : public FAS_EntityContainer
{
public:
    FAS_Leader(FAS_EntityContainer* parent=nullptr);
    FAS_Leader(FAS_EntityContainer* parent, const FAS_LeaderData& d);

    FAS_Entity* clone() const override;

    FAS2::EntityType rtti() const override
    {
        return FAS2::EntityDimLeader;
    }

    void update() override;

    FAS_LeaderData getData() const {
        return data;
    }

    //return true: if this leader has an arrow at the beginning./
    bool hasArrowHead()
    {
        return data.arrowHead;
    }

    FAS_Entity* addVertex(const FAS_Vector& v);
    void addEntity(FAS_Entity* entity) override;

    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void stretch(const FAS_Vector& firstCorner, const FAS_Vector& secondCorner, const FAS_Vector& offset) override;

    friend std::ostream& operator << (std::ostream& os, const FAS_Leader& l);

protected:
    FAS_LeaderData data;
    bool empty;
};

#endif
