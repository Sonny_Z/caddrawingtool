/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_line.h"

//sort with alphabetical order
#include "fas_circle.h"
#include "fas_graphic.h"
#include "fas_graphicview.h"
#include "fas_linetypepattern.h"
#include "fas_information.h"
#include "fas_painter.h"
#include "fas_painterqt.h"
#include "lc_quadratic.h"
#include "lc_rect.h"
#include "fas_insert.h"
#include "qdebug.h"
std::ostream& operator << (std::ostream& os, const FAS_LineData& ld) {
    os << "FAS_LINE: ((" << ld.startpoint <<
          ")(" << ld.endpoint <<
          "))";
    return os;
}

FAS_Line::FAS_Line(FAS_EntityContainer* parent,
                   const FAS_LineData& d)
    :FAS_AtomicEntity(parent), data(d)
{
    calculateBorders();
}

//construct a line from two endpoints
FAS_Line::FAS_Line(FAS_EntityContainer* parent, const FAS_Vector& pStart, const FAS_Vector& pEnd)
    :FAS_AtomicEntity(parent), data({pStart,pEnd})
{
    calculateBorders();
}

//construct a line from two endpoints, to be used for construction
FAS_Line::FAS_Line(const FAS_Vector& pStart, const FAS_Vector& pEnd)
    :FAS_AtomicEntity(nullptr), data({pStart,pEnd})
{
    calculateBorders();
}

FAS_Entity* FAS_Line::clone() const
{
    FAS_Line* l = new FAS_Line(*this);
    l->initId();
    return l;
}

void FAS_Line::calculateBorders()
{
    minV = FAS_Vector::minimum(data.startpoint, data.endpoint);
    maxV = FAS_Vector::maximum(data.startpoint, data.endpoint);
}

FAS_VectorSolutions FAS_Line::getRefPoints() const
{
    return FAS_VectorSolutions({data.startpoint, data.endpoint});
}
FAS_Vector FAS_Line::getNearestEndpoint(const FAS_Vector& coord, double* dist)const
{
    double dist1((data.startpoint-coord).squared());
    double dist2((data.endpoint-coord).squared());
    if (dist2<dist1)
    {
        if (dist)
        {
            *dist = sqrt(dist2);
        }
        return data.endpoint;
    }
    else
    {
        if (dist)
        {
            *dist = sqrt(dist1);
        }
        return data.startpoint;
    }
}

FAS_Vector FAS_Line::getNearestPointOnEntity(const FAS_Vector& coord, bool onEntity, double* dist, FAS_Entity** entity)const
{
    if (entity)
    {
        *entity = const_cast<FAS_Line*>(this);
    }
    FAS_Vector direction = data.endpoint-data.startpoint;
    FAS_Vector vpc=coord-data.startpoint;
    double a=direction.squared();
    if( a < FAS_TOLERANCE2)
    {
        //line too short
        vpc=getMiddlePoint();
    }
    else
    {
        //find projection on line
        const double t=FAS_Vector::dotP(vpc,direction)/a;
        if( !isConstruction() && onEntity && ( t<=-FAS_TOLERANCE || t>=1.+FAS_TOLERANCE ) )
        {
            return getNearestEndpoint(coord,dist);
        }
        vpc = data.startpoint + direction*t;
    }

    if (dist)
    {
        *dist = vpc.distanceTo(coord);
    }
    return vpc;
}

FAS_Vector FAS_Line::getMiddlePoint()const
{
    return (getStartpoint() + getEndpoint())*0.5;
}
// return the nearest of equidistant middle points of the line.
FAS_Vector FAS_Line::getNearestMiddle(const FAS_Vector& coord,
                                      double* dist,
                                      int middlePoints
                                      )const
{
    FAS_Vector dvp(getEndpoint() - getStartpoint());
    double l=dvp.magnitude();
    if( l<= FAS_TOLERANCE) {
        //line too short
        return const_cast<FAS_Line*>(this)->getNearestCenter(coord, dist);
    }
    FAS_Vector vp0(getNearestPointOnEntity(coord,true,dist));
    int counts=middlePoints+1;
    int i( static_cast<int>(vp0.distanceTo(getStartpoint())/l*counts+0.5));
    if(!i) i++; // remove end points
    if(i==counts) i--;
    vp0=getStartpoint() + dvp*(double(i)/double(counts));

    if (dist)
    {
        *dist=vp0.distanceTo(coord);
    }
    return vp0;
}

FAS_Vector FAS_Line::getNearestDist(double distance, const FAS_Vector& coord, double* dist) const
{
    FAS_Vector dv = FAS_Vector::polar(distance, getAngle1());
    FAS_Vector ret;
    if( (coord-getStartpoint()).squared()<  (coord-getEndpoint()).squared() )
    {
        ret = getStartpoint() + dv;
    }
    else
    {
        ret = getEndpoint() - dv;
    }
    if (dist)
        *dist=coord.distanceTo(ret);

    return ret;
}

FAS_Vector FAS_Line::getNearestDist(double distance, bool startp) const
{
    double a1 = getAngle1();
    FAS_Vector dv = FAS_Vector::polar(distance, a1);
    FAS_Vector ret;
    if (startp)
    {
        ret = data.startpoint + dv;
    }
    else
    {
        ret = data.endpoint - dv;
    }
    return ret;
}

LC_Quadratic FAS_Line::getQuadratic() const
{
    std::vector<double> ce(3,0.);
    auto dvp=data.endpoint - data.startpoint;
    FAS_Vector normal(-dvp.y,dvp.x);
    ce[0]=normal.x;
    ce[1]=normal.y;
    ce[2]= -normal.dotP(data.endpoint);
    return LC_Quadratic(ce);
}

double FAS_Line::areaLineIntegral() const
{
    return 0.5*(data.endpoint.y - data.startpoint.y)*(data.startpoint.x + data.endpoint.x);
}


FAS_Vector  FAS_Line::getTangentDirection(const FAS_Vector& /*point*/)const{
    return getEndpoint() - getStartpoint();
}

void FAS_Line::moveStartpoint(const FAS_Vector& pos) {
    data.startpoint = pos;
    calculateBorders();
}

void FAS_Line::moveEndpoint(const FAS_Vector& pos) {
    data.endpoint = pos;
    calculateBorders();
}

FAS_Vector FAS_Line::prepareTrim(const FAS_Vector& trimCoord,
                                 const FAS_VectorSolutions& trimSol)
{
    if ( ! trimSol.hasValid())
        return(FAS_Vector(false));
    if ( trimSol.getNumber() == 1 )
        return(trimSol.get(0));
    auto vp0 = trimSol.getClosest(trimCoord, nullptr, 0);

    double dr2=trimCoord.squaredTo(vp0);
    //the trim point found is closer to mouse location (trimCoord) than both end points, return this trim point
    if(dr2 < trimCoord.squaredTo(getStartpoint()) && dr2 < trimCoord.squaredTo(getEndpoint())) return vp0;
    //the closer endpoint to trimCoord
    FAS_Vector vp1=(trimCoord.squaredTo(getStartpoint()) <= trimCoord.squaredTo(getEndpoint()))?getStartpoint():getEndpoint();

    //searching for intersection in the direction of the closer end point
    auto dvp1=vp1 - trimCoord;
    FAS_VectorSolutions sol1;
    for(size_t i=0; i<trimSol.size(); i++)
    {
        auto dvp2=trimSol.at(i) - trimCoord;
        if( FAS_Vector::dotP(dvp1, dvp2) > FAS_TOLERANCE)
            sol1.push_back(trimSol.at(i));
    }
    //if found intersection in direction, return the closest to trimCoord from it
    if(sol1.size())
        return sol1.getClosest(trimCoord, nullptr, 0);

    //no intersection by direction, return previously found closest intersection
    return vp0;
}

FAS2::Ending FAS_Line::getTrimPoint(const FAS_Vector& trimCoord, const FAS_Vector& trimPoint)
{
    FAS_Vector vp1=getStartpoint() - trimCoord;
    FAS_Vector vp2=trimPoint - trimCoord;
    if ( FAS_Vector::dotP(vp1,vp2) < 0 )
    {
        return FAS2::EndingEnd;
    }
    else
    {
        return FAS2::EndingStart;
    }
}

void FAS_Line::reverse()
{
    std::swap(data.startpoint, data.endpoint);
}

bool FAS_Line::hasEndpointsWithinWindow(const FAS_Vector& firstCorner, const FAS_Vector& secondCorner)
{
    FAS_Vector vLow( std::min(firstCorner.x, secondCorner.x), std::min(firstCorner.y, secondCorner.y));
    FAS_Vector vHigh( std::max(firstCorner.x, secondCorner.x), std::max(firstCorner.y, secondCorner.y));

    return data.startpoint.isInWindowOrdered(vLow, vHigh)
            || data.endpoint.isInWindowOrdered(vLow, vHigh);

}

//this function creates offset
bool FAS_Line::offset(const FAS_Vector& coord, const double& distance)
{
    FAS_Vector direction{getEndpoint()-getStartpoint()};
    double ds(direction.magnitude());
    direction /= ds;
    FAS_Vector vp(coord-getStartpoint());
    direction.set(-direction.y,direction.x);
    if(FAS_Vector::dotP(direction,vp)<0.)
    {
        direction *= -1.;
    }
    direction*=distance;
    move(direction);
    return true;
}

bool FAS_Line::isTangent(const FAS_CircleData&  circleData) const
{
    double d;
    getNearestPointOnEntity(circleData.center,false,&d);
    if(fabs(d-circleData.radius)<20.*FAS_TOLERANCE)
        return true;
    return false;
}

FAS_Vector FAS_Line::getNormalVector() const
{
    FAS_Vector vp=data.endpoint  - data.startpoint; //direction vector
    double r=vp.magnitude();
    if (r< FAS_TOLERANCE)
        return FAS_Vector{false};
    return FAS_Vector{-vp.y,vp.x}/r;
}

std::vector<FAS_Entity* > FAS_Line::offsetTwoSides(const double& distance) const
{
    std::vector<FAS_Entity*> ret(0);
    FAS_Vector const& vp=getNormalVector()*distance;
    ret.push_back(new FAS_Line{data.startpoint+vp,data.endpoint+vp});
    ret.push_back(new FAS_Line{data.startpoint-vp,data.endpoint-vp});
    return ret;
}

void FAS_Line::revertDirection(){
    std::swap(data.startpoint,data.endpoint);
}

void FAS_Line::move(const FAS_Vector& offset)
{
    data.startpoint.move(offset);
    data.endpoint.move(offset);
    moveBorders(offset);
}

void FAS_Line::rotate(const double& angle)
{
    FAS_Vector rvp(angle);
    data.startpoint.rotate(rvp);
    data.endpoint.rotate(rvp);
    calculateBorders();
}



void FAS_Line::rotate(const FAS_Vector& center, const double& angle)
{
    FAS_Vector rvp(angle);
    data.startpoint.rotate(center, rvp);
    data.endpoint.rotate(center, rvp);
    calculateBorders();
}

void FAS_Line::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    data.startpoint.rotate(center, angleVector);
    data.endpoint.rotate(center, angleVector);
    calculateBorders();
}

void FAS_Line::scale(const FAS_Vector& factor)
{
    data.startpoint.scale(factor);
    data.endpoint.scale(factor);
    calculateBorders();
}

void FAS_Line::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    data.startpoint.scale(center, factor);
    data.endpoint.scale(center, factor);
    calculateBorders();
}

void FAS_Line::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    data.startpoint.mirror(axisPoint1, axisPoint2);
    data.endpoint.mirror(axisPoint1, axisPoint2);
    calculateBorders();
}

void FAS_Line::stretch(const FAS_Vector& firstCorner,
                       const FAS_Vector& secondCorner,
                       const FAS_Vector& offset)
{
    FAS_Vector const vLow{std::min(firstCorner.x, secondCorner.x),
                std::min(firstCorner.y, secondCorner.y)
                         };
    FAS_Vector const vHigh{std::max(firstCorner.x, secondCorner.x),
                std::max(firstCorner.y, secondCorner.y)
                          };

    if (getStartpoint().isInWindowOrdered(vLow, vHigh))
    {
        moveStartpoint(getStartpoint() + offset);
    }
    if (getEndpoint().isInWindowOrdered(vLow, vHigh))
    {
        moveEndpoint(getEndpoint() + offset);
    }
}

void FAS_Line::moveRef(const FAS_Vector& ref, const FAS_Vector& offset)
{
    if(  fabs(data.startpoint.x -ref.x)<1.0e-4 &&
         fabs(data.startpoint.y -ref.y)<1.0e-4 )
    {
        moveStartpoint(data.startpoint+offset);
    }
    if(  fabs(data.endpoint.x -ref.x)<1.0e-4 &&
         fabs(data.endpoint.y -ref.y)<1.0e-4 )
    {
        moveEndpoint(data.endpoint+offset);
    }
}

//void FAS_Line::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,
//                  double& patternOffset,FAS_InsertData& insertData)
//{
//    if (! (painter && view))
//    {
//        return;
//    }

//    this->resetBasicData();
//    if (fabs(insertData.scaleFactor.x)>1.0e-6 && fabs(insertData.scaleFactor.y)>1.0e-6)
//    {

//        this->moveEntityBoarder(insertData.insertionPoint +
//                             FAS_Vector(insertData.spacing.x/insertData.scaleFactor.x*1, insertData.spacing.y/insertData.scaleFactor.y*1),insertData.insertionPoint, insertData.angle,insertData.scaleFactor);            }
//    else
//    {
//        this->moveEntityBoarder(insertData.insertionPoint,insertData.insertionPoint, insertData.angle,insertData.scaleFactor);
//    }


//    draw(painter,view,patternOffset);
//}
void FAS_Line::draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset)
{

    if (! (painter && view))
    {
        return;
    }
    COMMONDEF->drawEntityCount++;

    auto viewportRect = view->getViewRect();
    FAS_VectorSolutions endPoints(0);
    endPoints.push_back(getStartpoint());
    endPoints.push_back(getEndpoint());

    FAS_EntityContainer ec(nullptr);
    ec.addRectangle(viewportRect.minP(), viewportRect.maxP());

    if ((endPoints[0] - getStartpoint()).squared() >
            (endPoints[1] - getStartpoint()).squared() )
        std::swap(endPoints[0],endPoints[1]);

    FAS_Vector pStart{view->toGui(endPoints.at(0))};
    FAS_Vector pEnd{view->toGui(endPoints.at(1))};
    FAS_Vector direction = pEnd-pStart;

    if (isConstruction(true) && direction.squared() > FAS_TOLERANCE)
    {
        //extend line on a construction layer to fill the whole view
        FAS_VectorSolutions vpIts;
        for(auto p: ec)
        {
            auto const sol=FAS_Information::getIntersection(this, p, false);
            for (auto const& vp: sol)
            {
                if (vpIts.getClosestDistance(vp) <= FAS_TOLERANCE * 10.)
                    continue;
                vpIts.push_back(vp);
            }
        }

        //draw construction lines up to viewport border
        switch (vpIts.size())
        {
        case 2:
            // no need to sort intersections
            break;
        case 3:
        case 4:
        {
            // will use the inner two points
            size_t i{0};
            for (size_t j = 0; j < vpIts.size(); ++j)
                if (viewportRect.inArea(vpIts.at(j), FAS_TOLERANCE * 10.))
                    std::swap(vpIts[j], vpIts[i++]);

        }
            break;
        default:
            //should not happen
            return;
        }
        pStart=view->toGui(vpIts.get(0));
        pEnd=view->toGui(vpIts.get(1));
        direction=pEnd-pStart;
    }
    double  length=direction.magnitude();
    patternOffset -= length;
    if (( !isSelected() &&
          ( getPen().getLineType()==FAS2::SolidLine || view->getDrawingMode()==FAS2::ModePreview)) )
    {
        //if length is too small, attempt to draw the line, could be a potential bug
        painter->drawLine(pStart,pEnd);
        return;
    }
    // Pattern:
    const FAS_LineTypePattern* pat;
    if (isSelected())
    {
        pat = &FAS_LineTypePattern::patternSolidLine; //commented
    }
    else
    {
        pat = view->getPattern(getPen().getLineType());
    }
    if (!pat)
    {
        painter->drawLine(pStart,pEnd);
        return;
    }
    if(length<=FAS_TOLERANCE)
    {
        painter->drawLine(pStart,pEnd);
        return; //avoid division by zero
    }
    direction/=length;
    FAS_Pen pen = painter->getPen();

    pen.setLineType(FAS2::SolidLine);
    painter->setPen(pen);

    if (pat->num <= 0)
    {
        painter->drawLine(view->toGui(getStartpoint()),
                          view->toGui(getEndpoint()));
        return;
    }

    double patternSegmentLength = pat->totalLength;

    // create pattern:
    std::vector<FAS_Vector> dp(pat->num);
    std::vector<double> ds(pat->num);
    double dpmm=static_cast<FAS_PainterQt*>(painter)->getDpmm();
    for (size_t i=0; i < pat->num; ++i)
    {
        ds[i]=dpmm*pat->pattern[i];
        if (fabs(ds[i]) < 1. ) ds[i] = copysign(1., ds[i]);
        dp[i] = direction*fabs(ds[i]);
    }
    double total= remainder(patternOffset-0.5*patternSegmentLength,patternSegmentLength) -0.5*patternSegmentLength;

    FAS_Vector curP{pStart+direction*total};
    for (int j=0; total<length; j=(j+1)%pat->num)
    {
        // line segment (otherwise space segment)
        double const t2=total+fabs(ds[j]);
        FAS_Vector const& p3=curP+dp[j];
        if (ds[j]>0.0 && t2 > 0.0)
        {
            // drop the whole pattern segment line, for ds[i]<0:
            // trim end points of pattern segment line to line
            FAS_Vector const& p1 =(total > -0.5)?curP:pStart;
            FAS_Vector const& p2 =(t2 < length+0.5)?p3:pEnd;
            painter->drawLine(p1,p2);
        }
        total=t2;
        curP=p3;
    }
}

void FAS_Line::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor)
{
//    resetBorders();
    FAS_Vector mstartPoint=getStartpoint();
    FAS_Vector mendPoint=getEndpoint();
    FAS_Vector bkstartPoint=data.bkstartpoin;
    FAS_Vector bkendPoint=data.bkendpoint;
    if(!bkstartPoint.valid && !bkendPoint.valid){
        data.bkstartpoin=mstartPoint;
        data.bkendpoint=mendPoint;
    }else{
        data.startpoint=data.bkstartpoin;
        data.endpoint=data.bkendpoint;
    }
}

void FAS_Line::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){
        data.startpoint.move(offset);
        data.endpoint.move(offset);
        FAS_Vector rvp(angle);
        data.startpoint.rotate(center, rvp);
        data.endpoint.rotate(center, rvp);
        data.startpoint.scale(center, factor);
        data.endpoint.scale(center, factor);
}

// Dumps the point's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_Line& l) {
    os << " Line: " << l.getData() << "\n";
    return os;
}


