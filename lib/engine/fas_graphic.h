/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/


#ifndef FAS_GRAPHIC_H
#define FAS_GRAPHIC_H

#include <QDateTime>
#include "fas_blocklist.h"
#include "fas_layerlist.h"
#include "fas_variabledict.h"
#include "fas_document.h"
#include <QDataStream>
class FAS_VariableDict;
class QG_LayerWidget;

// A graphic document which can contain entities layers and blocks.
class FAS_Graphic : public FAS_Document
{
public:
    FAS_Graphic(FAS_EntityContainer* parent = nullptr);
    virtual ~FAS_Graphic();

    virtual FAS2::EntityType rtti() const
    {
        return FAS2::EntityGraphic;
    }

    virtual unsigned long int countLayerEntities(FAS_Layer* layer);

    virtual FAS_LayerList* getLayerList()
    {
        return &layerList;
    }
    virtual FAS_BlockList* getBlockList()
    {
        return &blockList;
    }

    virtual void newDoc();
    virtual bool save(bool isAutoSave = false);
    virtual bool saveAs(const QString& filename, FAS2::FormatType type, bool force = false);
    virtual bool open(const QString& filename, FAS2::FormatType type);

    // Wrappers for Layer functions:
    void clearLayers()
    {
        layerList.clear();
    }
    unsigned countLayers() const
    {
        return layerList.count();
    }
    FAS_Layer* layerAt(unsigned i)
    {
        return layerList.at(i);
    }
    void activateLayer(const QString& name)
    {
        layerList.activate(name);
    }
    void activateLayer(FAS_Layer* layer)
    {
        layerList.activate(layer);
    }
    FAS_Layer* getActiveLayer()
    {
        return layerList.getActive();
    }
    virtual void addLayer(FAS_Layer* layer)
    {
        layerList.add(layer);
    }
    virtual void addEntity(FAS_Entity* entity);
    virtual void removeLayer(FAS_Layer* layer);
    virtual void editLayer(FAS_Layer* layer, const FAS_Layer& source)
    {
        layerList.edit(layer, source);
    }
    FAS_Layer* findLayer(const QString& name)
    {
        return layerList.find(name);
    }
    void toggleLayer(const QString& name)
    {
        layerList.toggle(name);
    }
    void toggleLayer(FAS_Layer* layer)
    {
        layerList.toggle(layer);
    }
    void toggleLayerLock(FAS_Layer* layer)
    {
        layerList.toggleLock(layer);
    }
    void toggleLayerPrint(FAS_Layer* layer)
    {
        layerList.togglePrint(layer);
    }
    void toggleLayerConstruction(FAS_Layer* layer)
    {
        layerList.toggleConstruction(layer);
    }
    void freezeAllLayers(bool freeze)
    {
        layerList.freezeAll(freeze);
    }

    void addLayerListListener(FAS_LayerListListener* listener)
    {
        layerList.addListener(listener);
    }
    void removeLayerListListener(FAS_LayerListListener* listener)
    {
        layerList.removeListener(listener);
    }
    // Wrapper for block functions:
    void clearBlocks()
    {
        blockList.clear();
    }
    unsigned countBlocks()
    {
        return blockList.count();
    }
    FAS_Block* blockAt(unsigned i)
    {
        return blockList.at(i);
    }
    virtual bool addBlock(FAS_Block* block, bool notify=true)
    {
        return blockList.add(block, notify);
    }
    virtual bool addCloneBlock(FAS_Block* block, bool notify=true)
    {
        return blockList.addClone(block);
    }
    virtual void removeBlock(FAS_Block* block)
    {
        blockList.remove(block);
    }
    FAS_Block* findBlock(const QString& name)
    {
        return blockList.find(name);
    }
    void toggleBlock(const QString& name)
    {
        blockList.toggle(name);
    }
    void toggleBlock(FAS_Block* block)
    {
        blockList.toggle(block);
    }
    void freezeAllBlocks(bool freeze)
    {
        blockList.freezeAll(freeze);
    }
    // Wrappers for variable functions:
    void clearVariables()
    {
        variableDict.clear();
    }
    int countVariables()
    {
        return variableDict.count();
    }

    void addVariable(const QString& key, const FAS_Vector& value, int code)
    {
        variableDict.add(key, value, code);
    }
    void addVariable(const QString& key, const QString& value, int code)
    {
        variableDict.add(key, value, code);
    }
    void addVariable(const QString& key, int value, int code)
    {
        variableDict.add(key, value, code);
    }
    void addVariable(const QString& key, double value, int code)
    {
        variableDict.add(key, value, code);
    }

    FAS_Vector getVariableVector(const QString& key, const FAS_Vector& def)
    {
        return variableDict.getVector(key, def);
    }
    QString getVariableString(const QString& key, const QString& def)
    {
        return variableDict.getString(key, def);
    }
    int getVariableInt(const QString& key, int def)
    {
        return variableDict.getInt(key, def);
    }
    double getVariableDouble(const QString& key, double def)
    {
        return variableDict.getDouble(key, def);
    }

    void removeVariable(const QString& key)
    {
        variableDict.remove(key);
    }

    QHash<QString, FAS_Variable>& getVariableDict()
    {
        return variableDict.getVariableDict();
    }

    FAS2::LinearFormat getLinearFormat();
    FAS2::LinearFormat getLinearFormat(int f);
    int getLinearPrecision();
    FAS2::AngleFormat getAngleFormat();
    int getAnglePrecision();

    FAS_Vector getPaperSize();
    void setPaperSize(const FAS_Vector& s);

    FAS_Vector getPaperInsertionBase();
    void setPaperInsertionBase(const FAS_Vector& p);

    FAS2::PaperFormat getPaperFormat(bool* landscape);
    void setPaperFormat(FAS2::PaperFormat f, bool landscape);

    double getPaperScale();
    void setPaperScale(double s);

    virtual void setUnit(FAS2::Unit u);
    virtual FAS2::Unit getUnit();

    bool isGridOn();
    void setGridOn(bool on);
    bool isIsometricGrid();
    void setIsometricGrid(bool on);
    void setCrosshairType(FAS2::CrosshairType chType);
    FAS2::CrosshairType getCrosshairType();

    bool isDraftOn();
    void setDraftOn(bool on);

    void centerToPage();
    bool fitToPage();

    // true The document has been modified since it was last saved.
    virtual bool isModified() const
    {
        return modified || layerList.isModified() || blockList.isModified();
    }

    // Sets the documents modified status to 'm'.
    virtual void setModified(bool m)
    {
        modified = m;
        layerList.setModified(m);
        blockList.setModified(m);
    }
    virtual QDateTime getModifyTime(void)
    {
        return modifiedTime;
    }

    //if set to true, will refuse to modify paper scale
    void setPaperScaleFixed(bool fixed)
    {
        paperScaleFixed=fixed;
    }
    bool getPaperScaleFixed()
    {
        return paperScaleFixed;
    }

    friend std::ostream& operator << (std::ostream& os, FAS_Graphic& g);

    int clean();

private:
    QDateTime modifiedTime;
    QString currentFileName; //keep a copy of filename for the modifiedTime

    FAS_LayerList layerList;
    FAS_BlockList blockList;
    FAS_VariableDict variableDict;
    FAS2::CrosshairType crosshairType; //corss hair type used by isometric grid
    //if set to true, will refuse to modify paper scale
    bool paperScaleFixed;
#if 1
    friend QDataStream &operator<<(QDataStream &out,const FAS_Graphic &para);
    friend QDataStream &operator>>(QDataStream &in, FAS_Graphic &para);
#endif
};


#endif
