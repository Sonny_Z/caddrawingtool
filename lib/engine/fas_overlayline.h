/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_OVERLAYLINE_H
#define FAS_OVERLAYLINE_H

#include "fas_line.h"

class FAS_OverlayLine : public FAS_Line
{
public:
    FAS_OverlayLine(FAS_EntityContainer* parent, const FAS_LineData& d);

    virtual void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset);

    FAS2::EntityType rtti() const override
    {
        return FAS2::EntityOverlayLine;
    }
}
;

#endif
