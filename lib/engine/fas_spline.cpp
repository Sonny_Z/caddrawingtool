/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_spline.h"

//sort with alphabetical order
#include <iostream>
#include <cmath>
#include <numeric>

#include "fas_graphicview.h"
#include "fas_line.h"
#include "fas_painter.h"
#include <QDebug>
FAS_SplineData::FAS_SplineData(int _degree, bool _closed):
    degree(_degree)
  ,closed(_closed)
{
}

std::ostream& operator << (std::ostream& os, const FAS_SplineData& ld)
{
    os << "( degree: " << ld.degree <<
          " closed: " << ld.closed;
    if (ld.controlPoints.size())
    {
        os << "\n(control points:\n";
        for (auto const& v: ld.controlPoints)
            os<<v;
        os<<")\n";
    }
    if (ld.knotslist.size())
    {
        os << "\n(knot vector:\n";
        for (auto const& v: ld.knotslist)
            os<<v;
        os<<")\n";
    }
    os  << ")";
    return os;
}

FAS_Spline::FAS_Spline(FAS_EntityContainer* parent,
                       const FAS_SplineData& d)
    :FAS_EntityContainer(parent), data(d)
{
    calculateBorders();
}

FAS_Entity* FAS_Spline::clone() const
{
    FAS_Spline* l = new FAS_Spline(*this);
    l->setOwner(isOwner());
    l->initId();
    l->detach();
    return l;
}



void FAS_Spline::calculateBorders()
{
    resetBorders();
    for (auto it = data.controlPoints.begin(); it!=data.controlPoints.end(); ++it)
    {
        minV = FAS_Vector::minimum(*it, minV);
        maxV = FAS_Vector::maximum(*it, maxV);
    }
}


void FAS_Spline::setDegree(size_t deg)
{
    if (deg>=1 && deg<=3)
    {
        data.degree = deg;
    }
}

// return Degree of this spline curve (1-3).
size_t FAS_Spline::getDegree() const
{
    return data.degree;
}

size_t FAS_Spline::getNumberOfControlPoints() const
{
    return data.controlPoints.size();
}

bool FAS_Spline::isClosed() const
{
    return data.closed;
}

// Sets the closed falg of this spline.
void FAS_Spline::setClosed(bool c)
{
    data.closed = c;
    update();
}

FAS_VectorSolutions FAS_Spline::getRefPoints() const
{
    return FAS_VectorSolutions(data.controlPoints);
}

FAS_Vector FAS_Spline::getNearestRef( const FAS_Vector& coord, double* dist /*= nullptr*/) const
{
    return FAS_Entity::getNearestRef(coord, dist);
}

FAS_Vector FAS_Spline::getNearestSelectedRef( const FAS_Vector& coord, double* dist /*= nullptr*/) const
{
    return FAS_Entity::getNearestSelectedRef(coord, dist);
}

void FAS_Spline::update()
{
    clear();
    if (isUndone())
    {
        return;
    }

    if (data.degree<1 || data.degree>3)
    {
        return;
    }

    if (data.controlPoints.size() < data.degree+1)
    {
        return;
    }

    resetBorders();

    std::vector<FAS_Vector> tControlPoints = data.controlPoints;

    if (data.closed)
    {
        for (size_t i=0; i<data.degree; ++i)
        {
            tControlPoints.push_back(data.controlPoints.at(i));
        }
    }

    const size_t npts = tControlPoints.size();
    // order:
    const size_t  k = data.degree+1;
    // resolution:
    const size_t  p1 = getGraphicVariableInt("$SPLINESEGS", 8) * npts;

    std::vector<double> h(npts+1, 1.);
    std::vector<FAS_Vector> p(p1, {0., 0.});
    if (data.closed)
    {
        rbsplinu(npts,k,p1,tControlPoints,h,p);
    }
    else
    {
        rbspline(npts,k,p1,tControlPoints,h,p);
    }

    FAS_Vector prev{};
    for (auto const& vp: p)
    {
        if (prev.valid)
        {
            FAS_Line* line = new FAS_Line{this, prev, vp};
            line->setLayer(nullptr);
            line->setPen(FAS2::FlagInvalid);
            addEntity(line);
        }
        prev = vp;
        minV = FAS_Vector::minimum(prev, minV);
        maxV = FAS_Vector::maximum(prev, maxV);
    }
}

FAS_Vector FAS_Spline::getStartpoint() const
{
    if (data.closed) return FAS_Vector(false);
    return static_cast<FAS_Line*>(const_cast<FAS_Spline*>(this)->firstEntity())->getStartpoint();
}

FAS_Vector FAS_Spline::getEndpoint() const
{
    if (data.closed) return FAS_Vector(false);
    return static_cast<FAS_Line*>(const_cast<FAS_Spline*>(this)->lastEntity())->getEndpoint();
}


FAS_Vector FAS_Spline::getNearestEndpoint(const FAS_Vector& coord,
                                          double* dist)const
{
    double minDist = FAS_MAXDOUBLE;
    FAS_Vector ret(false);
    if(! data.closed)
    {
        // no endpoint for closed spline
        FAS_Vector vp1(getStartpoint());
        FAS_Vector vp2(getEndpoint());
        double d1( (coord-vp1).squared());
        double d2( (coord-vp2).squared());
        if( d1<d2)
        {
            ret=vp1;
            minDist=sqrt(d1);
        }
        else
        {
            ret=vp2;
            minDist=sqrt(d2);
        }
    }
    if (dist)
    {
        *dist = minDist;
    }
    return ret;
}

FAS_Vector FAS_Spline::getNearestCenter(const FAS_Vector& /*coord*/,
                                        double* dist) const{

    if (dist) {
        *dist = FAS_MAXDOUBLE;
    }

    return FAS_Vector(false);
}

FAS_Vector FAS_Spline::getNearestMiddle(const FAS_Vector& /*coord*/, double* dist, int /*middlePoints*/)const
{
    if (dist)
    {
        *dist = FAS_MAXDOUBLE;
    }

    return FAS_Vector(false);
}

FAS_Vector FAS_Spline::getNearestDist(double /*distance*/,
                                      const FAS_Vector& /*coord*/,
                                      double* dist) const{
    if (dist)
    {
        *dist = FAS_MAXDOUBLE;
    }

    return FAS_Vector(false);
}

void FAS_Spline::move(const FAS_Vector& offset)
{
    FAS_EntityContainer::move(offset);
    for (FAS_Vector& vp: data.controlPoints)
    {
        vp.move(offset);
    }
}

void FAS_Spline::rotate(const FAS_Vector& center, const double& angle)
{
    rotate(center,FAS_Vector(angle));
}

void FAS_Spline::rotate(const FAS_Vector& center, const FAS_Vector& angleVector)
{
    FAS_EntityContainer::rotate(center, angleVector);
    for (FAS_Vector& vp: data.controlPoints) {
        vp.rotate(center, angleVector);
    }
}

void FAS_Spline::scale(const FAS_Vector& center, const FAS_Vector& factor)
{
    for (FAS_Vector& vp: data.controlPoints)
    {
        vp.scale(center, factor);
    }
    update();
}

void FAS_Spline::mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2)
{
    for (FAS_Vector& vp: data.controlPoints)
    {
        vp.mirror(axisPoint1, axisPoint2);
    }
    update();
}

void FAS_Spline::moveRef(const FAS_Vector& ref, const FAS_Vector& offset)
{
    for (FAS_Vector& vp: data.controlPoints)
    {
        if (ref.distanceTo(vp)<1.0e-4)
        {
            vp.move(offset);
        }
    }
    update();
}

void FAS_Spline::revertDirection()
{
    std::reverse(data.controlPoints.begin(), data.controlPoints.end());
}

void FAS_Spline::draw(FAS_Painter* painter, FAS_GraphicView* view, double& /*patternOffset*/)
{

    if (!(painter && view))
    {
        return;
    }
COMMONDEF->drawEntityCount++;
    FAS_Entity* e=firstEntity(FAS2::ResolveNone);
    if (e)
    {
        FAS_Pen p=this->getPen(true);
        e->setPen(p);
        double patternOffset(0.0);
        view->drawEntity(painter, e, patternOffset);
        e = nextEntity(FAS2::ResolveNone);
        while(e)
        {
            view->drawEntityPlain(painter, e, patternOffset);
            e = nextEntity(FAS2::ResolveNone);
        }
    }
}

//return The reference points of the spline.
const std::vector<FAS_Vector>& FAS_Spline::getControlPoints() const
{
    return data.controlPoints;
}

// Appends the given point to the control points.
void FAS_Spline::addControlPoint(const FAS_Vector& v)
{
    data.controlPoints.push_back(v);
}

// Removes the control point that was last added.
void FAS_Spline::removeLastControlPoint() {
    data.controlPoints.pop_back();
}

// Generates B-Spline open knot vector with multiplicity equal to the order at the ends.
std::vector<double> FAS_Spline::knot(size_t num, size_t order) const
{
    if (data.knotslist.size() == num + order) {
        return data.knotslist;
    }

    std::vector<double> knotVector(num + order, 0.);
    //use uniform knots
    std::iota(knotVector.begin() + order, knotVector.begin() + num + 1, 1);
    std::fill(knotVector.begin() + num + 1, knotVector.end(), knotVector[num]);
    return knotVector;
}

//Generates rational B-spline basis functions for an open knot vector.
namespace
{
std::vector<double> rbasis(int c, double t, int npts,
                           const std::vector<double>& x,
                           const std::vector<double>& h)
{
    int const nplusc = npts + c;
    std::vector<double> temp(nplusc,0.);
    // calculate the first order nonrational basis functions n[i]
    for (int i = 0; i< nplusc-1; i++)
        if ((t >= x[i]) && (t < x[i+1]))
            temp[i] = 1;

    for (int k = 2; k <= c; k++)
    {
        for (int i = 0; i < nplusc-k; i++)
        {
            // if the lower order basis function is zero skip the calculation
            if (temp[i] != 0)
                temp[i] = ((t-x[i])*temp[i])/(x[i+k-1]-x[i]);
            // if the lower order basis function is zero skip the calculation
            if (temp[i+1] != 0)
                temp[i] += ((x[i+k]-t)*temp[i+1])/(x[i+k]-x[i+1]);
        }
    }

    // pick up last point
    if (t >= x[nplusc-1])
        temp[npts-1] = 1;

    // calculate sum for denominator of rational basis functions
    double sum = 0.;
    for (int i = 0; i < npts; i++)
    {
        sum += temp[i]*h[i];
    }

    std::vector<double> r(npts, 0);
    // form rational basis functions and put in r vector
    if (sum != 0)
    {
        for (int i = 0; i < npts; i++)
            r[i] = (temp[i]*h[i])/sum;
    }
    return r;
}
}


// Generates a rational B-spline curve using a uniform open knot vector.
void FAS_Spline::rbspline(size_t npts, size_t k, size_t p1,
                          const std::vector<FAS_Vector>& b,
                          const std::vector<double>& h,
                          std::vector<FAS_Vector>& p) const
{
    size_t const nplusc = npts + k;
    // generate the open knot vector
    auto const x = knot(npts, k);
    // calculate the points on the rational B-spline curve
    double t = 0;
    double const step = x[nplusc-1]/(p1-1);
    for (auto& vp: p)
    {
        if (x[nplusc-1] - t < 5e-6) t = x[nplusc-1];
        // generate the basis function for this value of t
        auto const nbasis = rbasis(k, t, npts, x, h);
        // generate a point on the curve
        for (size_t i = 0; i < npts; i++)
            vp += b[i] * nbasis[i];
        t += step;
    }
}

std::vector<double> FAS_Spline::knotu(size_t num, size_t order) const
{
    if (data.knotslist.size() == num + order)
    {
        return data.knotslist;
    }
    std::vector<double> knotVector(num + order, 0.);
    std::iota(knotVector.begin(), knotVector.end(), 0);
    return knotVector;
}

void FAS_Spline::rbsplinu(size_t npts, size_t k, size_t p1,
                          const std::vector<FAS_Vector>& b,
                          const std::vector<double>& h,
                          std::vector<FAS_Vector>& p) const
{
    size_t const nplusc = npts + k;
    /* generate the periodic knot vector */
    std::vector<double> const x = knotu(npts, k);
    /*    calculate the points on the rational B-spline curve */
    double t = k-1;
    double const step = double(npts - k + 1)/(p1 - 1);
    for (auto& vp: p)
    {
        if (x[nplusc-1] - t < 5e-6) t = x[nplusc-1];
        /* generate the basis function for this value of t */
        auto const nbasis = rbasis(k, t, npts, x, h);
        /* generate a point on the curve, for x, y, z */
        for (size_t i = 0; i < npts; i++)
            vp += b[i] * nbasis[i];
        t += step;
    }
}
void FAS_Spline::drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& dataInsert)
{
    if (!(painter && view))
    {
        return;
    }

    FAS_Entity* e=firstEntity(FAS2::ResolveNone);
    if (e)
    {
        FAS_Pen p=this->getPen(true);
        e->setPen(p);
        double patternOffset(0.0);
        //view->drawEntity(painter, e, patternOffset,dataInsert);
         view->drawEntity(painter, e, patternOffset);

        e = nextEntity(FAS2::ResolveNone);
        while(e)
        {

            //view->drawEntity(painter, e, patternOffset,dataInsert);
            view->drawEntity(painter, e, patternOffset);
            e = nextEntity(FAS2::ResolveNone);
        }
    }
}
void FAS_Spline::resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){



    if(!data.bkBasicData.valid){
        data.bkBasicData=FAS_Vector(true);
        data.bkdegree=data.degree;
        data.bkknotslist=data.knotslist;
        data.bkcontrolPoints=data.controlPoints;
    }else{
        data.degree=data.bkdegree;
        data.knotslist=data.bkknotslist;
        data.controlPoints=data.bkcontrolPoints;

    }
    FAS_EntityContainer::resetBasicData(offset,center,angle,factor);
}

void FAS_Spline::moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor){

    FAS_EntityContainer::moveEntityBoarder(offset,center,angle,factor);
    for (FAS_Vector& vp: data.controlPoints)
    {
        vp.move(offset);
    }
    FAS_Vector angleVector=FAS_Vector(angle);
    for (FAS_Vector& vp: data.controlPoints) {
        vp.rotate(center, angleVector);
    }
    for (FAS_Vector& vp: data.controlPoints)
    {
        vp.scale(center, factor);
    }
    update();

//    qDebug()<<"splinemmove:"<<offset.x<<offset.y<<center.x<<center.y<<angle<<factor.x<<factor.y;

}
// Dumps the spline's data to stdout.
std::ostream& operator << (std::ostream& os, const FAS_Spline& l) {
    os << " Spline: " << l.getData() << "\n";
    return os;
}


