/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_layerlist.h"

//sort with alphabetical order
#include <iostream>

#include "fas_layer.h"
#include "fas_layerlistlistener.h"

/**
 * Default constructor.
 */
FAS_LayerList::FAS_LayerList()
{
    activeLayer = nullptr;
	setModified(false);
}

// Removes all layers in the layerlist.
void FAS_LayerList::clear()
{
    layers.clear();
	setModified(true);
}


QList<FAS_Layer*>::iterator FAS_LayerList::begin()
{
    return layers.begin();
}

QList<FAS_Layer*>::iterator FAS_LayerList::end()
{
    return layers.end();
}

QList<FAS_Layer*>::const_iterator FAS_LayerList::begin()const
{
    return layers.begin();
}

QList<FAS_Layer*>::const_iterator FAS_LayerList::end()const
{
    return layers.end();
}



//Activates the given layer.
void FAS_LayerList::activate(const QString& name, bool notify)
{
    activate(find(name), notify);
}

//Activates the given layer.
void FAS_LayerList::activate(FAS_Layer* layer, bool notify) {
    activeLayer = layer;
    if (notify)
    {
       for (int i=0; i<layerListListeners.size(); ++i)
       {
           FAS_LayerListListener* l = layerListListeners.at(i);
           l->layerActivated(activeLayer);
       }
    }
}

// sort by layer names
void FAS_LayerList::sort()
{
    std::stable_sort(layers.begin(), layers.end(), [](const FAS_Layer* l0, const FAS_Layer* l1 )->bool
    {
        return l0->getName() < l1->getName();
    });
}

/*
 * Adds a layer to the layer list.
 * If there is already a layer with the same name, no layer is 
 * added. In that case the layer passed to the methode will be deleted!
 * If no layer was active so far, the new layer becomes the active one.
 *
 * Listeners are notified.
 */
void FAS_LayerList::add(FAS_Layer* layer) {
    if (layer==nullptr)
    {
        return;
    }
    // check if layer already exists:
    FAS_Layer* l = find(layer->getName());
    if (l==nullptr)
    {
        layers.append(layer);
        this->sort();
        // notify listeners
        for (int i=0; i<layerListListeners.size(); ++i)
        {
            FAS_LayerListListener* l = layerListListeners.at(i);
            l->layerAdded(layer);
        }
		setModified(true);
        // if there was no active layer so far, activate this one.
        if (activeLayer==nullptr)
        {
            activate(layer);
        }
    }
    else
    {
        // if there was no active layer so far, activate this one.
        if (activeLayer==nullptr)
        {
            activate(l);
        }

        l->freeze( layer->isFrozen());
        l->lock( layer->isLocked());
        l->setPrint( layer->isPrint());
        l->setConverted( layer->isConverted());
        l->setConstruction( layer->isConstruction());
        l->visibleInLayerList( layer->isVisibleInLayerList());
        l->setPen(layer->getPen());

        delete layer;
        layer = nullptr;
    }
}

// Removes a layer from the list.
void FAS_LayerList::remove(FAS_Layer* layer)
{
    if (layer==nullptr)
    {
        return;
    }
    // here the layer is removed from the list but not deleted
    layers.removeOne(layer);
    for (int i=0; i<layerListListeners.size(); ++i)
    {
        FAS_LayerListListener* l = layerListListeners.at(i);
        l->layerRemoved(layer);
    }
		
	setModified(true);

    // activate an other layer if necessary:
    if (activeLayer==layer) {
        activate(layers.first());
    }
    // now it's save to delete the layer
    delete layer;
}

/**
 * Changes a layer's attributes. The attributes of layer 'layer'
 * are copied from layer 'source'.
 * Listeners are notified.
 */
void FAS_LayerList::edit(FAS_Layer* layer, const FAS_Layer& source)
{
    if (layer==nullptr)
        return;

    *layer = source;

    for (int i=0; i<layerListListeners.size(); ++i)
    {
        FAS_LayerListListener* l = layerListListeners.at(i);
        l->layerEdited(layer);
    }
	setModified(true);
}

//return the layer with the given name
FAS_Layer* FAS_LayerList::find(const QString& name)
{
    FAS_Layer* ret = nullptr;
    for (int i=0; i<layers.size(); ++i)
    {
        FAS_Layer* l = layers.at(i);
        if (l->getName()==name)
        {
            ret = l;
            break;
        }
    }
    return ret;
}

//'return Index of the given layer in the layer list or -1 if the layer was not found.
int FAS_LayerList::getIndex(const QString& name)
{
    int ret = -1;
    for (int i=0; i<layers.size(); i++)
    {
        FAS_Layer* l = layers.at(i);
        if (l->getName()==name)
        {
            ret = i;
            break;
        }
    }
    return ret;
}

//return Index of the given layer in the layer list or -1 if the layer was not found.
int FAS_LayerList::getIndex(FAS_Layer* layer)
{
    return layers.indexOf(layer);
}

//Switches on / off the given layer.
void FAS_LayerList::toggle(const QString& name)
{
    toggle(find(name));
}



// Switches on / off the given layer.
void FAS_LayerList::toggle(FAS_Layer* layer)
{
    if (layer==nullptr)
        return;

    layer->toggle();
    setModified(true);

    // Notify listeners:
    for (auto *i : layerListListeners)
    {
        if (!i)
        {
            continue;
        }
        FAS_LayerListListener *l = (FAS_LayerListListener *)i;
        l->layerToggled(layer);
    }
}

// Locks or unlocks the given layer.
void FAS_LayerList::toggleLock(FAS_Layer* layer)
{
    if (layer==nullptr)
        return;

    layer->toggleLock();
    setModified(true);

    // Notify listeners:
    for (int i=0; i<layerListListeners.size(); ++i)
    {
        FAS_LayerListListener* l = layerListListeners.at(i);
        l->layerToggledLock(layer);
    }
}

//Switch printing for the given layer on / off.
void FAS_LayerList::togglePrint(FAS_Layer* layer)
{
    if (layer==nullptr)
        return;

    layer->togglePrint();
    setModified(true);

    // Notify listeners:
    for (int i=0; i<layerListListeners.size(); ++i)
    {
        FAS_LayerListListener* l = layerListListeners.at(i);
        l->layerToggledPrint(layer);
    }
}

// Switch construction attribute for the given layer on / off.
void FAS_LayerList::toggleConstruction(FAS_Layer* layer) {
    if (layer==nullptr)
        return;

    layer->toggleConstruction();
    setModified(true);

    // Notify listeners:
    for (int i=0; i<layerListListeners.size(); ++i)
    {
        FAS_LayerListListener* l = layerListListeners.at(i);
        l->layerToggledConstruction(layer);
    }
}

// Freezes or defreezes all layers.
void FAS_LayerList::freezeAll(bool freeze)
{
    for (unsigned l=0; l<count(); l++)
    {
        if (at(l)->isVisibleInLayerList())
        {
             at(l)->freeze(freeze);
        }
    }
    setModified(true);

    for (int i=0; i<layerListListeners.size(); ++i)
    {
        FAS_LayerListListener* l = layerListListeners.at(i);
        l->layerToggled(nullptr);
    }
}

void FAS_LayerList::addListener(FAS_LayerListListener* listener)
{
    layerListListeners.append(listener);
}

void FAS_LayerList::removeListener(FAS_LayerListListener* listener)
{
    layerListListeners.removeOne(listener);
}

// Dumps the layers to stdout.
std::ostream& operator << (std::ostream& os, FAS_LayerList& l)
{
    os << "Layerlist: \n";
    for (unsigned i=0; i<l.count(); i++)
    {
        os << *(l.at(i)) << "\n";
    }
    return os;
}
