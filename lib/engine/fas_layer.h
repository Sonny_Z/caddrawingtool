/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_LAYER_H
#define FAS_LAYER_H

#ifdef __hpux
#include <sys/_size_t.h>
#endif

#include <iosfwd>

#include "fas_pen.h"

class QString;

// Holds the data that defines a layer.
struct FAS_LayerData {
    FAS_LayerData() {}

    FAS_LayerData(const QString& name,
                  const FAS_Pen& pen,
                  bool frozen,
                  bool locked);
    // Layer name
    QString name;
    // default pen for this layer
    FAS_Pen pen;
    // Frozen flag
    bool frozen;
    // Locked flag
    bool locked;
    // Print flag
    bool print=true;
    // Converted flag (cam)
    bool converted;
    // a construction layer has entities of infinite length, and will never be printed out
    bool construction=false;
    // visible in layer list
    bool visibleInLayerList;
};

class FAS_Layer
{
public:
    explicit FAS_Layer(const QString& name);
    FAS_Layer* clone() const;
    // sets a new name for this layer.
    void setName(const QString& name);
    // return the name of this layer.
    QString getName() const;
    // sets the default pen for this layer.
    void setPen(const FAS_Pen& pen);
    // return default pen for this layer.
    FAS_Pen getPen() const;
    bool isFrozen() const;
    // true the layer has been converted already
    bool isConverted() const;
    void setConverted(bool c);

    // Toggles the visibility of this layer.
    void toggle();
    void freeze(bool freeze);
    // Toggles the lock of this layer.
    void toggleLock();
    //Toggles printing of this layer on / off.
    void togglePrint();
    // Toggles construction attribute of this layer on / off.
    void toggleConstruction();
    // Locks/Unlocks this layer.
    void lock(bool l);
    //return the LOCK state of the Layer
    bool isLocked() const;
    // set visibility of layer in layer list
    void visibleInLayerList(bool l);
    // return the visibility of the Layer in layer list
    bool isVisibleInLayerList() const;
    // set the PRINT state of the Layer
    bool setPrint( const bool print);
    // return the PRINT state of the Layer
    bool isPrint() const;
    /*
     * whether the layer is a construction layer
     * The construction layer property is stored
     * in extended data in the DXF layer table
     */
    bool isConstruction() const;
    // set the construction attribute for the layer
    bool setConstruction( const bool construction);
    friend std::ostream& operator << (std::ostream& os, const FAS_Layer& l);

private:
    //! Layer data
    FAS_LayerData data;
};

#endif
