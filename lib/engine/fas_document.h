/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_DOCUMENT_H
#define FAS_DOCUMENT_H

#include "fas_layerlist.h"
#include "fas_entitycontainer.h"
#include "fas_undo.h"

class FAS_BlockList;

/*
 * Base class for documents. Documents can be either graphics or
 * blocks and are typically shown in graphic views. Documents hold
 * an active pen for drawing in the Document, a file name and they
 * know whether they have been modified or not.
 */
class FAS_Document : public FAS_EntityContainer, public FAS_Undo
{
public:
    FAS_Document(FAS_EntityContainer* parent=nullptr);

    virtual FAS_LayerList* getLayerList() = 0;
    virtual FAS_BlockList* getBlockList() = 0;

    virtual void newDoc() = 0;
    virtual bool save(bool isAutoSave = false) = 0;
    virtual bool saveAs(const QString &filename, FAS2::FormatType type, bool force) = 0;
    virtual bool open(const QString &filename, FAS2::FormatType type) = 0;

    //return true for all document entities (e.g. Graphics or Blocks).
    virtual bool isDocument() const {
        return true;
    }

    // Removes an entity from the entiy container. Implementation from FAS_Undo.
    virtual void removeUndoable(FAS_Undoable* u)
    {
        if (u && u->undoRtti()==FAS2::UndoableEntity)
        {
            removeEntity(static_cast<FAS_Entity*>(u));
        }
    }

    //return Currently active drawing pen.
    FAS_Pen getActivePen() const
    {
        return activePen;
    }

    // Sets the currently active drawing pen to p.
    void setActivePen(FAS_Pen p)
    {
        activePen = p;
    }

    // @return File name of the document currently loaded. Note, that the default file name is empty.
    QString getFilename() const
    {
        return filename;
    }

    // return Auto-save file name of the document currently loaded.
    QString getAutoSaveFilename() const
    {
        return autosaveFilename;
    }

    // Sets file name for the document currently loaded.
    void setFilename(const QString& fn)
    {
        filename = fn;
    }

    //Sets the documents modified status to 'm'.
    virtual void setModified(bool m)
    {
        //std::cout << "FAS_Document::setModified: %d" << (int)m << std::endl;
        modified = m;
    }

     /* return true The document has been modified since it was last saved.
     *  return false The document has not been modified since it was last saved.
     */
    virtual bool isModified() const
    {
        return modified;
    }

    // Overwritten to set modified flag before starting an undo cycle.
    virtual void startUndoCycle() {
        setModified(true);
        FAS_Undo::startUndoCycle();
    }

    void setGraphicView(FAS_GraphicView * g) {gv = g;}
    FAS_GraphicView* getGraphicView() {return gv;}


    void setDetectedStatus(bool detected)
    {
        isDetected = detected;
    }
    bool getDetectedStatus()
    {
        return isDetected;
    }

    void setRecognizedDeviceHiden(bool isHiden)
    {
        isHidenRecognizedDevice = isHiden;
    }
    bool getRecognizedDeviceHiden()
    {
        return isHidenRecognizedDevice;
    }
protected:
    /** Flag set if the recognized devices is hiden. */
    bool isHidenRecognizedDevice = false;
    /** Flag set if the devices document are detected. */
    bool isDetected = false;
    /** Flag set if the document was modified and not yet saved. */
    bool modified;
    /** Active pen. */
    FAS_Pen activePen;
    /** File name of the document or empty for a new document. */
    QString filename;
    /** Auto-save file name of document. */
    QString autosaveFilename;
    /** Format type */
    FAS2::FormatType formatType;
    FAS_GraphicView * gv;//used to read/save current view

};

#endif
