/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_INSERT_H
#define FAS_INSERT_H

#include "deviceinfo.h"
#include "fas_entitycontainer.h"
#include "dxf_entities.h"
class FAS_BlockList;
class FAS_AttribData {
public:
    FAS_AttribData();
public:
    QString text;
    double  height;
    double angle;
    int hAlign;
    int vAlign;

};

// Holds the data that defines an insert.
struct FAS_InsertData {
    FAS_InsertData() {}
    FAS_InsertData(const QString& name,
                   FAS_Vector insertionPoint,
                   FAS_Vector scaleFactor,
                   double angle,
                   int cols, int rows, FAS_Vector spacing,
                   FAS_BlockList* blockSource = nullptr,
                   FAS2::UpdateMode updateMode = FAS2::Update);
    FAS_InsertData(const QString& name,
                   FAS_Vector insertionPoint,
                   FAS_Vector scaleFactor,
                   double angle,
                   int cols, int rows, FAS_Vector spacing,
                   QString insertHandleId,
                   FAS_BlockList* blockSource = nullptr,
                   FAS2::UpdateMode updateMode = FAS2::Update);

    QString name;
    FAS_Vector insertionPoint;
    FAS_Vector scaleFactor;
    double angle;
    int cols, rows;
    FAS_Vector spacing;
    FAS_BlockList* blockSource;
    FAS2::UpdateMode updateMode;
    DeviceInfo* deviceInfo = nullptr;
    FAS_Vector bkinsertionPoint;
    double bkangle;
    FAS_Vector bkscaleFactor;
    FAS_Vector bkspacing;
    QMap<QString, FAS_AttribData> mmap;
    QString insertHandleId;

};

std::ostream& operator << (std::ostream& os, const FAS_InsertData& d);

/*
 * An insert inserts a block into the drawing at a certain location
 * with certain attributes (angle, scale, ...).
 * Inserts don't really contain other entities internally. They just
 * refer to a block. However, to the outside world they act exactly
 * like EntityContainer.
 */
class FAS_Insert : public FAS_EntityContainer
{
public:
    FAS_Insert(FAS_EntityContainer* parent, const FAS_InsertData& d);
    virtual ~FAS_Insert();

    virtual FAS_Entity* clone() const;

    // return FAS2::EntityInsert */
    virtual FAS2::EntityType rtti() const
    {
        return FAS2::EntityInsert;
    }

    // return Copy of data that defines the insert.
    FAS_InsertData getData() const
    {
        return data;
    }
    virtual void reparent(FAS_EntityContainer* parent)
    {
        FAS_Entity::reparent(parent);
        block = nullptr;
    }
    FAS_Block* getBlockForInsert() const;
    FAS_Block getBlockForInsertData() const;
    virtual void update();

    QString getName() const
    {
        return data.name;
    }
    void setName(const QString& newName)
    {
        data.name = newName;
        update();
    }
    FAS_Vector getInsertionPoint() const
    {
        return data.insertionPoint;
    }
    void setInsertionPoint(const FAS_Vector& i)
    {
        data.insertionPoint = i;
    }
    FAS_Vector getScale() const
    {
        return data.scaleFactor;
    }
    void setScale(const FAS_Vector& s)
    {
        data.scaleFactor = s;
    }
    double getAngle() const
    {
        return data.angle;
    }
    void setAngle(double a)
    {
        data.angle = a;
    }
    int getCols() const
    {
        return data.cols;
    }
    void setCols(int c)
    {
        data.cols = c;
    }
    int getRows() const
    {
        return data.rows;
    }
    void setRows(int r)
    {
        data.rows = r;
    }
    FAS_Vector getSpacing() const
    {
        return data.spacing;
    }
    void setSpacing(const FAS_Vector& s)
    {
        data.spacing = s;
    }

    virtual bool isVisible() const;

    virtual FAS_VectorSolutions getRefPoints() const;
    virtual FAS_Vector getNearestRef(const FAS_Vector& coord, double* dist = nullptr) const;

    virtual void move(const FAS_Vector& offset);
    virtual void rotate(const FAS_Vector& center, const double& angle);
    virtual void rotate(const FAS_Vector& center, const FAS_Vector& angleVector);
    virtual void scale(const FAS_Vector& center, const FAS_Vector& factor);
    virtual void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2);

    void draw(FAS_Painter* painter, FAS_GraphicView* view, double& patternOffset) override;
//    void calculateBorders() override;

    friend std::ostream& operator << (std::ostream& os, const FAS_Insert& i);

    void setDeviceInfo(DeviceInfo* type);
    DeviceInfo* getDeviceInfo() const;
    void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void setAttibutes(std::string attributeLabel,std::string text,double height,double angle,int hAlign,int vAlign);

 public:
    FAS_InsertData data;
    mutable FAS_Block* block;
};


#endif
