/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_PEN_H
#define FAS_PEN_H

#include "fas.h"
#include "fas_color.h"
#include "fas_flags.h"

// A pen stores attributes for painting such as line width, linetype, color, ...
class FAS_Pen : public FAS_Flags
{
public:
    FAS_Pen() : FAS_Flags()
    {
        setColor(FAS_Color(0,0,0));
        setWidth(FAS2::Width00);
        setLineType(FAS2::SolidLine);
        setScreenWidth(0);
    }
    FAS_Pen(const FAS_Color& c,
            FAS2::LineWidth w,
            FAS2::LineType t) : FAS_Flags()
    {
        setColor(c);
        setWidth(w);
        setLineType(t);
        setScreenWidth(0);
    }
    FAS_Pen(unsigned int f) : FAS_Flags(f)
    {
        setColor(FAS_Color(0,0,0));
        setWidth(FAS2::Width00);
        setLineType(FAS2::SolidLine);
        setScreenWidth(0);
    }
    virtual ~FAS_Pen() {}

    FAS2::LineType getLineType() const
    {
        return lineType;
    }
    void setLineType(FAS2::LineType t)
    {
        lineType = t;
    }
    FAS2::LineWidth getWidth() const
    {
        return width;
    }
    void setWidth(FAS2::LineWidth w)
    {
        width = w;
    }
    double getScreenWidth() const
    {
        return screenWidth;
    }
    void setScreenWidth(double w)
    {
        screenWidth = w;
    }
    const FAS_Color& getColor() const
    {
        return color;
    }
    void setColor(const FAS_Color& c)
    {
        color = c;
    }
    bool isValid()
    {
        return !getFlag(FAS2::FlagInvalid);
    }

    bool operator == (const FAS_Pen& p) const
    {
        return (lineType==p.lineType && width==p.width && color==p.color);
    }

    bool operator != (const FAS_Pen& p) const
    {
        return !(*this==p);
    }

    friend std::ostream& operator << (std::ostream& os, const FAS_Pen& p);

protected:
    FAS2::LineType lineType;
    FAS2::LineWidth width;
    double screenWidth;
    FAS_Color color;
};

#endif
