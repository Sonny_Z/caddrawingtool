/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include <iostream>
#include"fas_undocycle.h"

// Adds an Undoable to this Undo Cycle. Every Cycle can contain one or more Undoables.
void FAS_UndoCycle::addUndoable(FAS_Undoable* u)
{
    if (u) undoables.insert(u);
}

// Removes an undoable from the list.
void FAS_UndoCycle::removeUndoable(FAS_Undoable* u)
{
    if (u) undoables.erase(u);
}

void FAS_UndoCycle::changeUndoState()
{
    for (FAS_Undoable* u: undoables)
        u->changeUndoState();
}

std::set<FAS_Undoable*> const& FAS_UndoCycle::getUndoables() const
{
    return undoables;
}

std::ostream& operator << (std::ostream& os,
                           FAS_UndoCycle& uc)
{
    os << " Undo item: " << "\n";
    os << "   Undoable ids: ";
    for (auto u: uc.undoables)
    {
        if (u->undoRtti()==FAS2::UndoableEntity)
        {
            FAS_Entity* e = (FAS_Entity*)u;
            os << e->getId() << (u->isUndone() ? "*" : "") << " ";
        } else
        {
            os << "|";
        }
    }
    return os;
}

