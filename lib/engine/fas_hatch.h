/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_HATCH_H
#define FAS_HATCH_H

#include "fas_entitycontainer.h"

// Holds the data that defines a hatch entity.
struct FAS_HatchData {
    FAS_HatchData() {}
    ~FAS_HatchData() {}
    FAS_HatchData(bool solid,
                  double scale,
                  double angle,
                  const QString& pattern);
    bool solid;
    double scale;
    double angle;
    QString pattern;

    FAS_Vector bkBasicData;
    double bkangle;
};

std::ostream& operator << (std::ostream& os, const FAS_HatchData& td);

class FAS_Hatch : public FAS_EntityContainer
{
public:
    enum FAS_HatchError
    {
        HATCH_UNDEFINED = -1,
        HATCH_OK,
        HATCH_INVALID_CONTOUR,
        HATCH_PATTERN_NOT_FOUND,
        HATCH_TOO_SMALL,
        HATCH_AREA_TOO_BIG
    };

    FAS_Hatch() {}

    FAS_Hatch(FAS_EntityContainer* parent, const FAS_HatchData& d);

    FAS_Entity* clone() const override;

    FAS2::EntityType rtti() const override
    {
        return FAS2::EntityHatch;
    }

    //return true: if this is a hatch with lines (hatch pattern),
    //return false: if this is filled with a solid color.
    bool isContainer() const override;

    // return Copy of data that defines the hatch. */
    FAS_HatchData getData() const
    {
        return data;
    }

    bool validate();

    int countLoops() const;

    //return true if this is a solid fill. false if it is a pattern hatch.
    bool isSolid() const
    {
        return data.solid;
    }
    void setSolid(bool solid)
    {
        data.solid = solid;
    }

    QString getPattern()
    {
        return data.pattern;
    }
    void setPattern(const QString& pattern)
    {
        data.pattern = pattern;
    }

    double getScale()
    {
        return data.scale;
    }
    void setScale(double scale)
    {
        data.scale = scale;
    }

    double getAngle()
    {
        return data.angle;
    }
    void setAngle(double angle)
    {
        data.angle = angle;
    }
    double getTotalArea();

    void calculateBorders() override;
    void update() override;
    int getUpdateError()
    {
        return updateError;
    }
    void activateContour(bool on);
    void draw(FAS_Painter* painter, FAS_GraphicView* view,
              double& patternOffset) override;

    double getDistanceToPoint(const FAS_Vector& coord,
                              FAS_Entity** entity = nullptr,
                              FAS2::ResolveLevel level = FAS2::ResolveNone,
                              double solidDist = FAS_MAXDOUBLE) const override;

    void move(const FAS_Vector& offset) override;
    void rotate(const FAS_Vector& center, const double& angle) override;
    void rotate(const FAS_Vector& center, const FAS_Vector& angleVector) override;
    void scale(const FAS_Vector& center, const FAS_Vector& factor) override;
    void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2) override;
    void stretch(const FAS_Vector& firstCorner, const FAS_Vector& secondCorner, const FAS_Vector& offset) override;
    void drawInsertEntity(FAS_Painter* painter, FAS_GraphicView* view,double& patternOffset,FAS_InsertData& data);
    void resetBasicData(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;
    void moveEntityBoarder(const FAS_Vector& offset,const FAS_Vector& center, const double& angle,const FAS_Vector& factor) override;

    friend std::ostream& operator << (std::ostream& os, const FAS_Hatch& p);

protected:
    FAS_HatchData data;
    FAS_EntityContainer* hatch;
    bool updateRunning;
    bool needOptimization;
    int  updateError;
};

#endif
