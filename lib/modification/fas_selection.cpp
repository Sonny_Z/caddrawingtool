/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_selection.h"

//sort with alphabetical order
#include "fas_layer.h"

FAS_Selection::FAS_Selection(FAS_EntityContainer& container,
                             FAS_GraphicView* graphicView) {
    this->container = &container;
    this->graphicView = graphicView;
    graphic = container.getGraphic();
}

// Selects or deselects the given entitiy.
void FAS_Selection::selectSingle(FAS_Entity* e)
{
    if (e && e->isVisible())
    {
        if (graphicView)
        {
            graphicView->deleteEntity(e);
        }
        e->toggleSelected();
        if (graphicView) {
            graphicView->drawEntity(e);
        }
    }
}

/**
 * Selects all entities on visible layers.
 */
void FAS_Selection::selectAll(bool select) {
    if (graphicView) {
        //graphicView->deleteEntity(container);
    }

    for(auto e: *container){
        if (e && e->isVisible()) {
            e->setSelected(select);
        }
    }

    if (graphicView) {
        graphicView->redraw();
    }
}

QList<FAS_Entity*> FAS_Selection::getSelectedEntityList()
{
    QList<FAS_Entity*> selectedEntityList;

    for(auto e: *container) {
        if (e->isSelected()) {
            if(e->isContainer() && ((FAS_EntityContainer*)e)->isEmpty()){
                continue;
            }
            selectedEntityList.append(e);
        }
    }

    return selectedEntityList;
}

// Selects all entities on visible layers.
void FAS_Selection::invertSelection()
{
    if (graphicView) {
    }

    for(auto e: *container){
        if (e && e->isVisible()) {
            e->toggleSelected();
        }
    }

    if (graphicView) {
        graphicView->redraw();
    }
}

/**
 * Selects all entities that are completely in the given window.
 *
 * @param v1 First corner of the window to select.
 * @param v2 Second corner of the window to select.
 * @param select true: select, false: deselect
 */
void FAS_Selection::selectWindow(const FAS_Vector& v1, const FAS_Vector& v2,
                                 bool select, bool cross) {

    container->selectWindow(v1, v2, select, cross);

    if (graphicView) {
        graphicView->redraw();
    }
}

// EOF
