/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_MODIFICATION_H
#define FAS_MODIFICATION_H

#include "fas_vector.h"
#include "fas_pen.h"
#include <QHash>

class FAS_Entity;
class FAS_EntityContainer;
class FAS_Document;
class FAS_Graphic;
class FAS_GraphicView;

// Holds the data needed for move modifications.
class FAS_MoveData {
public:
    int number;
    bool useCurrentAttributes;
    bool useCurrentLayer;
    FAS_Vector offset;
};

// Holds the data needed for moving reference points.
class FAS_MoveRefData {
public:
    FAS_Vector ref;
    FAS_Vector offset;
};
// Holds the data needed for changing attributes.
class FAS_AttributesData {
public:
    QString layer;
    FAS_Pen pen;
    bool changeLayer;
    bool changeColor;
    bool changeLineType;
    bool changeWidth;
};

class FAS_Modification
{
public:
    FAS_Modification()=delete;
    FAS_Modification(FAS_EntityContainer& entityContainer,
                     FAS_GraphicView* graphicView=nullptr,
                     bool handleUndo=true);

    void remove();
    bool changeAttributes(FAS_AttributesData& data);
    bool changeAttributes(FAS_AttributesData& data, FAS_EntityContainer* container, std::vector<FAS_Entity*> addList);

    bool move(FAS_MoveData& data);
    bool moveRef(FAS_MoveRefData& data);

private:
    void deselectOriginals(bool remove);
    void addNewEntities(std::vector<FAS_Entity*>& addList);

protected:
    FAS_EntityContainer* container;
    FAS_Graphic* graphic;
    FAS_Document* document;
    FAS_GraphicView* graphicView;
    bool handleUndo;
};

#endif
