/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_modification.h"

//sort with alphabetical order
#include <cmath>

#include "fas_graphicview.h"
#include "fas_insert.h"
#include "fasutils.h"

FAS_Modification::FAS_Modification(FAS_EntityContainer& container,
                                 FAS_GraphicView* graphicView,
                                 bool handleUndo)
{
    this->container = &container;
    this->graphicView = graphicView;
    this->handleUndo = handleUndo;
    graphic = container.getGraphic();
    document = container.getDocument();
}

/**
 * Deletes all selected entities.
 */
void FAS_Modification::remove()
{
    if (!container)
    {
        return;
    }

    if (document)
    {
        document->startUndoCycle();
    }

    for(auto e: *container)
    {
        if (e && e->isSelected())
        {
            e->setSelected(false);
            e->changeUndoState();
            if (document)
            {
                document->addUndoable(e);
            }
        }
    }

    if (document)
    {
        document->endUndoCycle();
    }

    graphicView->redraw(FAS2::RedrawDrawing);
}

// Changes the attributes of all selected
bool FAS_Modification::changeAttributes(FAS_AttributesData& data)
{
    if (!container)
    {
        return false;
    }

    std::vector<FAS_Entity*> addList;

    if (document)
    {
        document->startUndoCycle();
    }

    for(auto e: *container)
    {
        if (e && e->isSelected())
        {
            FAS_Entity* ec = e->clone();
            ec->setSelected(false);

            FAS_Pen pen = ec->getPen(false);

            if (data.changeLayer==true)
            {
                ec->setLayer(data.layer);
            }

            if (data.changeColor==true)
            {
                pen.setColor(data.pen.getColor());
            }
            if (data.changeLineType==true)
            {
                pen.setLineType(data.pen.getLineType());
            }
            if (data.changeWidth==true)
            {
                pen.setWidth(data.pen.getWidth());
            }

            ec->setPen(pen);
            ec->update();
            addList.push_back(ec);
        }
    }

    deselectOriginals(true);
    addNewEntities(addList);

    if (document)
    {
        document->endUndoCycle();
    }

    if (graphicView)
    {
        graphicView->redraw(FAS2::RedrawDrawing);
    }

    return true;
}


/**
 * Moves all selected entities with the given data for the move
 * modification.
 */
bool FAS_Modification::move(FAS_MoveData& data)
{
    if (!container)
    {
        return false;
    }

    std::vector<FAS_Entity*> addList;

    if (document && handleUndo)
    {
        document->startUndoCycle();
    }

    // Create new entites
    for (int num=1; num<=data.number || (data.number==0 && num<=1); num++)
    {
        for(FAS_Entity* e: *container)
        {
            if (e && e->isSelected())
            {
                FAS_Entity* ec = e->clone();
                ec->move(data.offset*num);
                if (data.useCurrentLayer)
                {
                    ec->setLayerToActive();
                }
                if (data.useCurrentAttributes)
                {
                    ec->setPenToActive();
                }
                if (ec->rtti()==FAS2::EntityInsert)
                {
                    ((FAS_Insert*)ec)->update();
                }
                addList.push_back(ec);

                //FASUtils::updateDevicePostion(e, ec);
                if (ec->rtti()==FAS2::EntityInsert && ec->getInsert()->getDeviceInfo()->isValidDevice())
                {
                    QString codeText = ec->getInsert()->getDeviceInfo()->getCodeInfo();
                    FASUtils::getInstance()->getCodeToDeviceMapForMutation()->remove(codeText, e);
                    FASUtils::getInstance()->getCodeToDeviceMapForMutation()->insert(codeText, ec);
                    FAS_Entity* codeEntity = FASUtils::getInstance()->getAllCodeEntityHash().value(codeText);
                    if(codeEntity != nullptr)
                    {
                        FAS_Entity* codeEntCopy = codeEntity->clone();
                        codeEntCopy->move(data.offset*num);
                        codeEntCopy->update();
                        addList.push_back(codeEntCopy);
                        if(data.number == 0)
                        {
                            codeEntity->changeUndoState();
                            if (document && handleUndo)
                                document->addUndoable(codeEntity);
                        }
                    }
                }
            }
        }
    }

    deselectOriginals(data.number==0);
    addNewEntities(addList);

    if (document && handleUndo)
    {
        document->endUndoCycle();
    }

    if (graphicView)
    {
        graphicView->redraw(FAS2::RedrawDrawing);
    }
    return true;
}

// Deselects all selected entities and removes them if remove is true;
void FAS_Modification::deselectOriginals(bool remove)
{
    for(auto e: *container)
    {
        if (e)
        {
            bool selected = false;
            if (e->isSelected())
            {
                selected = true;
            }
            if (selected)
            {
                e->setSelected(false);
                if (remove)
                {
                    e->changeUndoState();
                    if (document && handleUndo)
                    {
                        document->addUndoable(e);
                    }
                }
            }
        }
    }
}


// Adds the given entities to the container and draws the entities if there's a graphic view available.
void FAS_Modification::addNewEntities(std::vector<FAS_Entity*>& addList)
{
    for(FAS_Entity* e: addList)
    {
        if (e)
        {
            container->addEntity(e);
            if (document && handleUndo)
            {
                document->addUndoable(e);
            }
        }
    }
}

// Moves all reference points of selected entities with the given data.
bool FAS_Modification::moveRef(FAS_MoveRefData& data)
{
    if (!container)
    {
        return false;
    }
    if(container->isLocked() || ! container->isVisible())
        return false;

    std::vector<FAS_Entity*> addList;

    if (document && handleUndo)
    {
        document->startUndoCycle();
    }

    // Create new entites
    for(auto e: *container){
        if (e && e->isSelected())
        {
            FAS_Entity* ec = e->clone();
            ec->moveRef(data.ref, data.offset);
            ec->setSelected(true);
            addList.push_back(ec);
        }
    }

    deselectOriginals(true);
    addNewEntities(addList);

    if (document && handleUndo)
    {
        document->endUndoCycle();
    }

    if (graphicView)
    {
        graphicView->redraw(FAS2::RedrawDrawing);
    }
    return true;
}

// EOF
