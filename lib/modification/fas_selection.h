/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_SELECTION_H
#define FAS_SELECTION_H

#include "fas_entitycontainer.h"
#include "fas_graphicview.h"

class FAS_Selection
{
public:
    FAS_Selection(FAS_EntityContainer& entityContainer,
                  FAS_GraphicView* graphicView=nullptr);

    void selectSingle(FAS_Entity* e);
    void selectAll(bool select=true);
    void deselectAll()
    {
        selectAll(false);
    }
    void invertSelection();
    void selectWindow(const FAS_Vector& v1, const FAS_Vector& v2,
                      bool select=true, bool cross=false);
    void deselectWindow(const FAS_Vector& v1, const FAS_Vector& v2)
    {
        selectWindow(v1, v2, false);
    }

    QList<FAS_Entity*> getSelectedEntityList();

protected:
    FAS_EntityContainer* container;
    FAS_Graphic* graphic;
    FAS_GraphicView* graphicView;
};

#endif
