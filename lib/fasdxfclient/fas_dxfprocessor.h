/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_DXFPROCESSOR_H
#define FAS_DXFPROCESSOR_H

#include <QHash>

#include "fas_color.h"
#include "fas_dimension.h"
#include "fas_graphic.h"
#include "libdxf_writer.h"

class FAS_Point;
class FAS_Line;
class FAS_Circle;
class FAS_Arc;
class FAS_Ellipse;
class FAS_Solid;
class FAS_Polyline;
class FAS_Spline;
class LC_SplinePoints;
class FAS_Insert;
class FAS_MText;
class FAS_Text;
class FAS_Hatch;
class FAS_Image;
class FAS_Leader;
class FAS_Polyline;
class FAS_ATTDEF;
class FAS_ATTRIB;

// This format filter class can import and export DXF files.
class FAS_DXFProcessor : public DXF_Interface
{
public:
    FAS_DXFProcessor();
    ~FAS_DXFProcessor();

    // Import:
    virtual bool fileImport(FAS_Graphic& g, const QString& file, FAS2::FormatType type = FAS2::FormatDXF);

    // Methods from DXF_CreationInterface:
    virtual void addHeader(const DXF_Header* data);
    virtual void addLType(const DXF_LType& /*data*/){}
    virtual void addLayer(const DXF_Layer& data);
    virtual void addDimStyle(const DXF_Dimstyle& data);
    virtual void addVport(const DXF_Vport& data);
    virtual void addTextStyle(const DXF_Textstyle& /*data*/){}
    virtual void addAppId(const DXF_AppId& /*data*/){}
    virtual void addBlock(const DXF_Block& data);
    virtual void endBlock();
    virtual void addPoint(const DXF_Point& data);
    virtual void addLine(const DXF_Line& data);
    virtual void addRay(const DXF_Ray& data);
    virtual void addXline(const DXF_Xline& data);
    virtual void addCircle(const DXF_Circle& data);
    virtual void addArc(const DXF_Arc& data);
    virtual void addEllipse(const DXF_Ellipse& data);
    virtual void addLWPolyline(const DXF_LWPolyline& data);
    virtual void addText(const DXF_Text& data);
    virtual void addPolyline(const DXF_Polyline& data);
    virtual void addSpline(const DXF_Spline* data);
    virtual void addKnot(const DXF_Entity&) {}
    virtual void addInsert(const DXF_Insert& data);
    virtual void addTrace(const DXF_Trace& data);
    virtual void addSolid(const DXF_Solid& data);
    virtual void addMText(const DXF_MText& data);
    FAS_DimensionData convDimensionData(const DXF_Dimension* data);
    virtual void addDimAlign(const DXF_DimAligned *data);
    virtual void addDimLinear(const DXF_DimLinear *data);
    virtual void addDimRadial(const DXF_DimRadial *data);
    virtual void addDimDiametric(const DXF_DimDiametric *data);
    virtual void addDimAngular(const DXF_DimAngular *data);
    virtual void addDimAngular3P(const DXF_DimAngular3p *data);
    virtual void addDimOrdinate(const DXF_DimOrdinate *data);
    virtual void addLeader(const DXF_Leader *data);
    virtual void addHatch(const DXF_Hatch* data);
    virtual void addViewport(const DXF_Viewport& /*data*/){}
    virtual void addImage(const DXF_Image* data);
    virtual void linkImage(const DXF_ImageDef* data);
    virtual void add3dFace(const DXF_3Dface& data);
    virtual void addComment(const char*);
    virtual void addAttdef(const DXF_Attdef &data);
    virtual void addAttrib(const DXF_Attrib &data);

    // Export:
    virtual bool fileExport(FAS_Graphic& g, const QString& file, FAS2::FormatType type);

    virtual void writeHeader(DXF_Header& data);
    virtual void writeEntities();
    virtual void writeLTypes();
    virtual void writeLayers();
    virtual void writeTextstyles();
    virtual void writeVports();
    virtual void writeBlockRecords();
    virtual void writeBlocks();
    virtual void writeDimstyles();
    virtual void writeAppId();

    void writePoint(FAS_Point* p);
    void writeLine(FAS_Line* l);
    void writeCircle(FAS_Circle* c);
    void writeArc(FAS_Arc* a);
    void writeEllipse(FAS_Ellipse* s);
    void writeSolid(FAS_Solid* s);
    void writeLWPolyline(FAS_Polyline* l);
    void writeSpline(FAS_Spline* s);
    void writeSplinePoints(LC_SplinePoints *s);
    void writeInsert(FAS_Insert* i);
    void writeMText(FAS_MText* t);
    void writeText(FAS_Text* t);
    void writeHatch(FAS_Hatch* h);
    void writeImage(FAS_Image* i);
    void writeLeader(FAS_Leader* l);
    void writeDimension(FAS_Dimension* d);
    void writePolyline(FAS_Polyline* p);
    void writeAttdef(FAS_ATTDEF* t);
    void writeSvgObjects(FAS_Graphic* graphic, const QString& file);


    void setEntityAttributes(FAS_Entity* entity, const DXF_Entity* attrib);
    void getEntityAttributes(DXF_Entity* ent, const FAS_Entity* entity);

    static QString toDxfString(const QString& str);
    static QString toNativeString(const QString& data);

public:
    FAS_Pen attributesToPen(const DXF_Layer* att) const;
    static FAS_Color numberToColor(int num);
    static int colorToNumber(const FAS_Color& col, int *rgb);
    static FAS2::LineType nameToLineType(const QString& name);
    static QString lineTypeToName(FAS2::LineType lineType);
    static FAS2::LineWidth numberToWidth(DXF_LW_Conv::lineWidth lw);
    static DXF_LW_Conv::lineWidth widthToNumber(FAS2::LineWidth width);
    static FAS2::AngleFormat numberToAngleFormat(int num);
    static int angleFormatToNumber(FAS2::AngleFormat af);
    static FAS2::Unit numberToUnit(int num);
    static int unitToNumber(FAS2::Unit unit);
    static bool isVariableTwoDimensional(const QString& var);
    static FAS_Insert* findInsert(FAS_EntityContainer* currentContainer,QString owner);

private:
    void prepareBlocks();
    void writeEntity(FAS_Entity* e);

private:
    // Pointer to the graphic we currently operate on.
    FAS_Graphic* graphic;
    // File name. Used to find out the full path of images.
    QString file;
    // Pointer to current entity container (either block or graphic)
    FAS_EntityContainer* currentContainer;
    // File codePage. Used to find the text coder.
    QString codePage;
    // File version.
    QString versionStr;
    int version;
    // Library File version.
    QString libVersionStr;
    int libVersion;
    int libRelease;
    // dimension style.
    QString dimStyle;
    // text style.
    QString textStyle;
    // Temporary list to handle unnamed blocks fot write R12 dxf.
    QHash <FAS_Entity*, QString> noNameBlock;
    QHash <QString, QString> fontList;
    LIBDXFWriter *dxfWriter = nullptr;
    // If saved version are 2004 or above can save color in RGB value.
    bool exactColor;
};

#endif
