/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_dxfprocessor.h"

//sort with alphabetical order
#include <cstdlib>
#include <QStringList>
#include <QTextCodec>

#include "fas_arc.h"
#include "fas_block.h"
#include "fas_circle.h"
#include "fas_dimaligned.h"
#include "fas_dimangular.h"
#include "fas_dimdiametric.h"
#include "fas_dimlinear.h"
#include "fas_dimradial.h"
#include "fas_ellipse.h"
#include "fas_graphicview.h"
#include "fas_hatch.h"
#include "fas_image.h"
#include "fas_insert.h"
#include "fas_layer.h"
#include "fas_leader.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_mtext.h"
#include "fas_point.h"
#include "fas_polyline.h"
#include "fas_solid.h"
#include "fas_spline.h"
#include "fas_system.h"
#include "fas_text.h"
#include "lc_splinepoints.h"
#include "libdxf_parser.h"
#include "fasutils.h"
#include "fas_attdef.h"

#include "qdebug.h"

FAS_DXFProcessor::FAS_DXFProcessor():DXF_Interface()
{
    currentContainer = nullptr;
    graphic = nullptr;
    // Init hash to change the QCAD "normal" style to the more correct ISO-3059
    // or draftsight symbol (AR*.shx) to sy*.lff
    fontList["normal"] = "iso";
    fontList["normallatin1"] = "iso";
    fontList["normallatin2"] = "iso";
    fontList["arastro"] = "syastro";
    fontList["armap"] = "symap";
    fontList["math"] = "symath";
    fontList["armeteo"] = "symeteo";
    fontList["armusic"] = "symusic";
}

FAS_DXFProcessor::~FAS_DXFProcessor()
{
}

qint64 hashCode(const QString & str)
{
  QByteArray hash = QCryptographicHash::hash(
    QByteArray::fromRawData((const char*)str.utf16(), str.length()*2),
    QCryptographicHash::Md5
  );
  Q_ASSERT(hash.size() == 16);
  QDataStream stream(&hash, QIODevice::ReadOnly);
  qint64 a, b;
  stream >> a >> b;
  return a ^ b;
}

// Implementation of the method used for FAS_Import to communicate with this filter.
bool FAS_DXFProcessor::fileImport(FAS_Graphic& g, const QString& file, FAS2::FormatType type)
{
    if(type != FAS2::FormatDXF)
    {
        return false;
    }

    graphic = &g;
    currentContainer = graphic;

    this->file = file;
    // add some variables that need to be there for DXF drawings:
    graphic->addVariable("$DIMSTYLE", "Standard", 2);
    dimStyle = "Standard";
    codePage = "ANSI_1252";
    textStyle = "Standard";
    //reset library version
    libVersionStr = "1.0";
    libVersion = 0;
    libRelease = 0;


    auto *gbk = QTextCodec::codecForName("UTF-8");
    std::string fileName;
    if(gbk==0)
    {
        gbk=QTextCodec::codecForName("gb18030");
        if(gbk==0)
        {
            gbk=QTextCodec::codecForName("GBK");
            if(gbk==0)
            {
                gbk=QTextCodec::codecForName("gb2312");
            }
        }
    }
    auto fileDir=QFile::encodeName(gbk->toUnicode(gbk->fromUnicode(file).data()));
    QFileInfo finfo(fileDir);
    auto relativeFilePath = "./temp/" + finfo.fileName();

    //auto fileDir=QFile::encodeName(gbk->toUnicode(file.toUtf8()));
    LIBDXFParser dxfParser(relativeFilePath.toUtf8());
    bool success = dxfParser.read(this, true);
    if (success==false)
    {
        return false;
    }

    //set current layer
    FAS_Layer* cl = graphic->findLayer(graphic->getVariableString("$CLAYER", "0"));
    if (cl )
    {
        //require to notify
        graphic->getLayerList()->activate(cl, true);
    }

//    qDebug() << "graphic->updateInserts() start";
    graphic->updateInserts(0);
//    qDebug() << "graphic->updateInserts() end";
    if(COMMONDEF->getSvgUiMode()){
        writeSvgObjects(graphic, fileDir);
         //draw insert JPG or SVG
    //     auto sourceImageDir = "./temp/" + finfo.baseName();
    //     QList<FAS_Insert *> ints=graphic->getInsertList();
    //     QList<QString> validatedBlockList;
    //     QList<QString> nonValidatedBlockList;
    //     for(FAS_Insert* entity : ints)
    //     {
    //         QString name = QString::fromUtf8("%1").arg(hashCode(entity->getInsert()->getName()));
    //         if(!entity->isVisible() || validatedBlockList.contains(name) || nonValidatedBlockList.contains(name)){
    //             continue;
    //         }
    //         double blockSizeX=entity->getLengthX();
    //         double blockSizeY=entity->getLengthY();
    //         if(blockSizeX+blockSizeY>10000 || blockSizeX+blockSizeY<10){
    //             nonValidatedBlockList.append(name);
    //             continue;
    //         }

    //         if(entity->getEntityList().size()==0){
    //             nonValidatedBlockList.append(name);
    //             continue;
    //         }

    //         validatedBlockList.append(name);

    //         FAS_EntityContainer* container=new FAS_EntityContainer();
    //         FAS_Insert* entityClone=entity->clone()->getInsert();
    //         entityClone->setAngle(0.0);
    //         entityClone->update();
    //         entityClone->calculateBlockBorders();
    //         container->addEntity(entityClone);
    //         container->adjustBorders(entityClone);
    //         FASUtils::doConvertDeviceToImage(sourceImageDir,name,"jpg",container,QSize(92,112));
    ////         FASUtils::doConvertDeviceToImage(file,name,"svg",container,QSize(92,112));
    //         qDebug() << "Draw Entity Donw" << name;
    //         qDebug() << "X size" << blockSizeX;
    //         qDebug() << "Y size" << blockSizeY;

    //     }
    }


    return true;
}

void FAS_DXFProcessor::writeSvgObjects(FAS_Graphic* graphic, const QString& file){

    //draw insert JPG or SVG
    QFileInfo finfo(file);

     auto sourceSvgDir = "./svgStore/" + finfo.baseName();
     QList<FAS_Insert *> ints=graphic->getInsertList();
     QList<QString> validatedBlockList;
     QList<QString> nonValidatedBlockList;
     for(FAS_Insert* oneDevice : ints)
     {
//         QString name = QString::fromUtf8("%1").arg(hashCode(entity->getInsert()->getName()));
//         if(!entity->isVisible() || validatedBlockList.contains(name) || nonValidatedBlockList.contains(name)){
//             continue;
//         }
//         double blockSizeX=entity->getLengthX();
//         double blockSizeY=entity->getLengthY();
//         if(blockSizeX+blockSizeY>10000 || blockSizeX+blockSizeY<10){
//             nonValidatedBlockList.append(name);
//             continue;
//         }

//         if(entity->getEntityList().size()==0){
//             nonValidatedBlockList.append(name);
//             continue;
//         }

//         validatedBlockList.append(name);

//         FAS_EntityContainer* container=new FAS_EntityContainer();
//         FAS_Insert* entityClone=entity->clone()->getInsert();
//         entityClone->setAngle(0.0);
//         entityClone->update();
//         entityClone->calculateBlockBorders();
//         container->addEntity(entityClone);
//         container->adjustBorders(entityClone);
//         FASUtils::doConvertDeviceToImage(sourceImageDir,name,"jpg",container,QSize(92,112));
//    //         FASUtils::doConvertDeviceToImage(file,name,"svg",container,QSize(92,112));
//         qDebug() << "Draw Entity Donw" << name;
//         qDebug() << "X size" << blockSizeX;
//         qDebug() << "Y size" << blockSizeY;

         //A insert with a attribute entity text value different from default value shoule be checked;
         auto blockEntities = oneDevice->getBlockForInsert()->getEntityList();
         double height;
         QString defaultText, text;
         bool isDrawImageInAtt = false;
         foreach(auto e, blockEntities){
             if(e->rtti() == FAS2::EntityAttdef){
                 auto defEntity = dynamic_cast<FAS_ATTDEF*>(e);
                 height = defEntity->data.textdata.height;
                 if(fabs(height) < 0.1){
                     continue;
                 }
                 auto label = defEntity->data.label;
                 text = oneDevice->data.mmap.value(label).text;
                 defaultText = defEntity->getText();
                 if(!text.contains(defaultText, Qt::CaseInsensitive)){
                     isDrawImageInAtt = true;
                     break;
                 }
     //                auto isVisiable = defEntity->isVisible();
             }
         }

         auto newInsert = oneDevice;
         auto container = new FAS_EntityContainer();
         auto oneBlock = newInsert->getBlockForInsert();
         auto svgName = QString::fromUtf8("%1").arg(COMMONDEF->hash(oneBlock->getName()));
         if(oneDevice->data.mmap.size()>0 && isDrawImageInAtt)
         {
//            QString blockFeatureCode=oneBlock->getType()->getPerfectMatchFlag();

//            long ID=newInsert->getId();
//            imageIdFeatureCodeMap.insert(ID,blockFeatureCode);
//            auto imageName = QString::fromUtf8("%1_%2_%3_%4")
//                    .arg(ID)
//                    .arg(COMMONDEF->getSystem(true))
//                    .arg(COMMONDEF->hash(oneBlock->getName()))
//                    .arg(blockFeatureCode);
            container->addEntity(newInsert);
            container->adjustBorders(newInsert);
//            COMMONDEF->convertDeviceToImage=true;
//            utils->doConvertDeviceToImage(desDir,imageName,"jpg",container,QSize(92,112));
//            COMMONDEF->convertDeviceToImage=false;
            FASUtils::svgGenerator(sourceSvgDir,svgName,container,QSize(92,112));
            //updater SVG file path
//                           oneDevice->getDeviceInfo()->getSvgRender()->load()

            container->setOwner(false);
            container->clear();
            delete container;
         }
         else
         {
         newInsert = oneDevice->clone()->getInsert();
     //       newInsert->setScale(FAS_Vector(1,1));
         newInsert->setSelected(false);
         newInsert->setVisible(true);
         newInsert->setOwner(false);
         bool isDiffcultBlock=false;
                 QList<FAS_Entity*> entities=newInsert->getEntityList();
                 FAS_Block* blockInInsert;
                 for(FAS_Entity* entity:entities){
                     if(FAS2::EntityInsert==entity->rtti()){
                                 isDiffcultBlock=true;
                                 break;
                     }
                 }
                 if(!isDiffcultBlock){
                     newInsert->clear();
                     newInsert->addEntity(oneBlock);
                     newInsert->calculateBlockBorders();
                 }else{
                     newInsert->setAngle(0.0);
                     newInsert->update();
                 }

//         QString blockFeatureCode=oneBlock->getType()->getPerfectMatchFlag();
//         long ID=newInsert->getId();
//         imageIdFeatureCodeMap.insert(ID,blockFeatureCode);
//         auto imageName = QString::fromUtf8("%1_%2_%3_%4")
//                 .arg(ID)
//                 .arg(COMMONDEF->getSystem(true))
//                 .arg(COMMONDEF->hash(oneBlock->getName()))
//                 .arg(blockFeatureCode);
         container->addEntity(newInsert);
         container->adjustBorders(newInsert);
//         COMMONDEF->convertDeviceToImage=true;
//         utils->doConvertDeviceToImage(desDir,COMMONDEF->hash(oneBlock->getName()),"jpg",container,QSize(92,112));
//         COMMONDEF->convertDeviceToImage=false;
         FASUtils::svgGenerator(sourceSvgDir,svgName,container,QSize(92,112));
         //updater SVG file path
     //        oneDevice->getDeviceInfo()->getSvgRender()->load()

     //        qDebug() <<"block ID name count entitysize"<<ID <<blockName <<devicesOfName.count() << devicesOfName.first()->getEntityList().size() << oneDevice->getInsertList().size();

         }
         auto svgSavePath=QString::fromUtf8("%1/%2.svg").arg(sourceSvgDir).arg(svgName);
         auto deviceInfo = new DeviceInfo(0,"name","svg",svgSavePath);


//         auto deciceInfo = oneDevice->getDeviceInfo();
         FASUtils::setBlcokType(oneBlock, deviceInfo);
         oneDevice->setDeviceInfo(deviceInfo);
//         oneDevice->getDeviceInfo()->getSvgRender()->load(svgSavePath);
     }
}

// Implementation of the method which handles layers.
void FAS_DXFProcessor::addLayer(const DXF_Layer &data)
{
    QString name = QString::fromUtf8(data.name.c_str());
    if (name != "0" && graphic->findLayer(name))
    {
        return;
    }
    FAS_Layer* layer = new FAS_Layer(name);
    layer->setPen(attributesToPen(&data));

    if (data.flags&0x01)
    {
        layer->freeze(true);
    }
    if (data.flags&0x04)
    {
        layer->lock(true);
    }
    if (data.color < 0)
    {
        layer->freeze(true);
    }
    layer->setPrint(data.plotF);
    graphic->addLayer(layer);
}

// Implementation of the method which handles dimension styles.
void FAS_DXFProcessor::addDimStyle(const DXF_Dimstyle& data)
{
    QString dimstyle = graphic->getVariableString("$DIMSTYLE", "standard");

    if (QString::compare(data.name.c_str(), dimstyle, Qt::CaseInsensitive) == 0)
    {
        graphic->addVariable("$DIMDEC", data.dimdec, 70);
        graphic->addVariable("$DIMADEC", data.dimadec, 70);
    }
}

// Implementation of the method which handles vports.
void FAS_DXFProcessor::addVport(const DXF_Vport &data)
{
    QString name = QString::fromStdString(data.name);
    if (name.toLower() == "*active")
    {
        data.grid == 1? graphic->setGridOn(true):graphic->setGridOn(false);
        graphic->setIsometricGrid(data.snapStyle);
        graphic->setCrosshairType( (FAS2::CrosshairType)data.snapIsopair);
    }
}

/*
 * Implementation of the method which handles blocks.
 * todo Adding blocks to blocks (stack for currentContainer)
 */
void FAS_DXFProcessor::addBlock(const DXF_Block& data)
{
    QString name = QString::fromUtf8(data.name.c_str());
    FAS_Vector bp(data.basePoint.x, data.basePoint.y);
    FAS_Block* block = new FAS_Block(graphic, FAS_BlockData(name, bp, false ));
    if (graphic->addBlock(block))
    {
        currentContainer = block;
    }
}

// Implementation of the method which closes blocks.
void FAS_DXFProcessor::endBlock()
{
    if (currentContainer->rtti() == FAS2::EntityBlock)
    {
        FAS_Block *bk = (FAS_Block *)currentContainer;
        if (!(bk->getName().startsWith("*D")) ){
            graphic->addCloneBlock(bk);
        }

        //remove unnamed blocks *D only if version != R12
        if (version!=1009)
        {
            if (bk->getName().startsWith("*D") ){
                graphic->removeBlock(bk);
            }

        }



    }
    currentContainer = graphic;
}

// Implementation of the method which handles point entities.
void FAS_DXFProcessor::addPoint(const DXF_Point& data)
{
    FAS_Vector v(data.basePoint.x, data.basePoint.y);
    FAS_Point* entity = new FAS_Point(currentContainer, FAS_PointData(v));
    setEntityAttributes(entity, &data);
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles line entities.
void FAS_DXFProcessor::addLine(const DXF_Line& data)
{
    FAS_Vector v1(data.basePoint.x, data.basePoint.y);
    FAS_Vector v2(data.secPoint.x, data.secPoint.y);
    FAS_Line* entity = new FAS_Line{currentContainer, {v1, v2}};
    setEntityAttributes(entity, &data);
    if (currentContainer)
        currentContainer->addEntity(entity);
}


// Implementation of the method which handles ray entities.
void FAS_DXFProcessor::addRay(const DXF_Ray& data)
{
    FAS_Vector v1{data.basePoint.x, data.basePoint.y};
    FAS_Vector v2{data.basePoint.x+data.secPoint.x,
                data.basePoint.y+data.secPoint.y};
    FAS_Line* entity = new FAS_Line{currentContainer, {v1, v2}};
    setEntityAttributes(entity, &data);
    if (currentContainer)
        currentContainer->addEntity(entity);
}

//Implementation of the method which handles line entities.
void FAS_DXFProcessor::addXline(const DXF_Xline& data)
{
    FAS_Vector v1(data.basePoint.x, data.basePoint.y);
    FAS_Vector v2(data.basePoint.x+data.secPoint.x, data.basePoint.y+data.secPoint.y);
    FAS_Line* entity = new FAS_Line{currentContainer, {v1, v2}};
    setEntityAttributes(entity, &data);
    if (currentContainer)
        currentContainer->addEntity(entity);
}

void FAS_DXFProcessor::addCircle(const DXF_Circle& data)
{
    FAS_Vector v{data.basePoint.x, data.basePoint.y};
    FAS_Circle* entity = new FAS_Circle(currentContainer, {v, data.radious});
    setEntityAttributes(entity, &data);
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles arc entities.
void FAS_DXFProcessor::addArc(const DXF_Arc& data)
{
    FAS_Vector v(data.basePoint.x, data.basePoint.y);
    FAS_ArcData d(v, data.radious,
                  data.staangle,
                  data.endangle,
                  false);
    FAS_Arc* entity = new FAS_Arc(currentContainer, d);
    setEntityAttributes(entity, &data);
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles ellipse entities.
void FAS_DXFProcessor::addEllipse(const DXF_Ellipse& data)
{
    FAS_Vector v1(data.basePoint.x, data.basePoint.y);
    FAS_Vector v2(data.secPoint.x, data.secPoint.y);
    double ang2 = data.endparam;
    if (fabs(ang2 - 2.*M_PI) < FAS_TOLERANCE &&
            fabs(data.staparam) < FAS_TOLERANCE)
        ang2 = 0.;
    FAS_Ellipse* entity = new FAS_Ellipse{currentContainer,
    {v1, v2,
            data.ratio,
            data.staparam, ang2, false}};
    setEntityAttributes(entity, &data);

    currentContainer->addEntity(entity);
}

// Implementation of the method which handles trace entities.
void FAS_DXFProcessor::addTrace(const DXF_Trace& data)
{
    FAS_Solid* entity;
    FAS_Vector v1{data.basePoint.x, data.basePoint.y};
    FAS_Vector v2{data.secPoint.x, data.secPoint.y};
    FAS_Vector v3{data.thirdPoint.x, data.thirdPoint.y};
    FAS_Vector v4{data.fourPoint.x, data.fourPoint.y};
    if (v3 == v4)
        entity = new FAS_Solid(currentContainer, FAS_SolidData(v1, v2, v3));
    else
        entity = new FAS_Solid(currentContainer, FAS_SolidData(v1, v2, v3,v4));

    setEntityAttributes(entity, &data);
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles solid entities.
void FAS_DXFProcessor::addSolid(const DXF_Solid& data)
{
    addTrace(data);
}

// Implementation of the method which handles lightweight polyline entities.
void FAS_DXFProcessor::addLWPolyline(const DXF_LWPolyline& data)
{
    if (data.vertlist.empty())
        return;
    FAS_PolylineData d(FAS_Vector{},
                       FAS_Vector{},
                       data.flags&0x1);
    FAS_Polyline *polyline = new FAS_Polyline(currentContainer, d);
    setEntityAttributes(polyline, &data);

    std::vector< std::pair<FAS_Vector, double> > verList;
    for (auto const& v: data.vertlist)
        verList.emplace_back(std::make_pair(FAS_Vector{v->x, v->y}, v->bulge));

    polyline->appendVertexs(verList);

    currentContainer->addEntity(polyline);
}


// Implementation of the method which handles polyline entities.
void FAS_DXFProcessor::addPolyline(const DXF_Polyline& data)
{
    if ( data.flags&0x10)
        return; //the polyline is a polygon mesh, not handled

    if ( data.flags&0x40)
        return; //the polyline is a poliface mesh, TODO convert

    FAS_PolylineData d(FAS_Vector{},
                       FAS_Vector{},
                       data.flags&0x1);
    FAS_Polyline *polyline = new FAS_Polyline(currentContainer, d);
    setEntityAttributes(polyline, &data);

    std::vector< std::pair<FAS_Vector, double> > verList;

    for (auto const& v: data.vertlist)
        verList.emplace_back( std::make_pair(FAS_Vector{v->basePoint.x, v->basePoint.y}, v->bulge));

    polyline->appendVertexs(verList);

    currentContainer->addEntity(polyline);
}


// Implementation of the method which handles splines.
void FAS_DXFProcessor::addSpline(const DXF_Spline* data)
{
    if(data->degree == 2)
    {
        LC_SplinePoints* splinePoints;
        LC_SplinePointsData d(((data->flags&0x1)==0x1), true);
        splinePoints = new LC_SplinePoints(currentContainer, d);
        setEntityAttributes(splinePoints, data);
        currentContainer->addEntity(splinePoints);

        for(auto const& vert: data->controllist)
        {
            FAS_Vector v(vert->x, vert->y);
            splinePoints->addControlPoint(v);
        }
        splinePoints->update();
        return;
    }

    FAS_Spline* spline;
    if (data->degree>=1 && data->degree<=3)
    {
        FAS_SplineData d(data->degree, ((data->flags&0x1)==0x1));
        if (data->knotslist.size())
            d.knotslist = data->knotslist;
        spline = new FAS_Spline(currentContainer, d);
        setEntityAttributes(spline, data);
        currentContainer->addEntity(spline);
    }
    else
    {
        return;
    }
    for (auto const& vert: data->controllist)
        spline->addControlPoint({vert->x, vert->y});

    if (data->ncontrol== 0 && data->degree != 2)
    {
        for (auto const& vert: data->fitlist)
            spline->addControlPoint({vert->x, vert->y});
    }
    spline->update();
}

// Implementation of the method which handles inserts.
void FAS_DXFProcessor::addInsert(const DXF_Insert& data)
{
    FAS_Vector ip(data.basePoint.x, data.basePoint.y);
    FAS_Vector sc(data.xscale, data.yscale, data.zscale);
    FAS_Vector sp(data.colspace, data.rowspace);
    FAS_InsertData d( QString::fromUtf8(data.name.c_str()),
                      ip, sc, data.angle,
                      data.colcount, data.rowcount,
                      sp, QString::fromStdString(data.insertHandleId),nullptr, FAS2::NoUpdate);

    FAS_Insert* entity = new FAS_Insert(currentContainer, d);
    setEntityAttributes(entity, &data);
    currentContainer->addEntity(entity);
}
// multi texts (MTEXT).
void FAS_DXFProcessor::addMText(const DXF_MText& data)
{
    FAS_MTextData::VAlign valign;
    FAS_MTextData::HAlign halign;
    FAS_MTextData::MTextDrawingDirection dir;
    FAS_MTextData::MTextLineSpacingStyle lss;
    QString sty = QString::fromUtf8(data.style.c_str());
    sty=sty.toLower();

    if (data.textgen<=3)
    {
        valign=FAS_MTextData::VATop;
    }
    else if (data.textgen<=6)
    {
        valign=FAS_MTextData::VAMiddle;
    }
    else
    {
        valign=FAS_MTextData::VABottom;
    }

    if (data.textgen%3==1)
    {
        halign=FAS_MTextData::HALeft;
    }
    else if (data.textgen%3==2)
    {
        halign=FAS_MTextData::HACenter;
    }
    else
    {
        halign=FAS_MTextData::HARight;
    }

    if (data.alignH==1)
    {
        dir = FAS_MTextData::LeftToRight;
    }
    else if (data.alignH==3)
    {
        dir = FAS_MTextData::TopToBottom;
    }
    else
    {
        dir = FAS_MTextData::ByStyle;
    }

    if (data.alignV==1)
    {
        lss = FAS_MTextData::AtLeast;
    }
    else
    {
        lss = FAS_MTextData::Exact;
    }

    QString mtext = toNativeString(QString::fromUtf8(data.text.c_str()));
    // use default style for the drawing:
    if (sty.isEmpty())
    {
        // japanese, cyrillic:
        if (codePage=="ANSI_932" || codePage=="ANSI_1251")
        {
            sty = "Unicode";
        }
        else
        {
            sty = textStyle;
        }
    }
    else
    {
        sty = fontList.value(sty, sty);
    }

    double interlin = data.interlin;
    double angle = data.angle*M_PI/180.;
    FAS_Vector ip = FAS_Vector(data.basePoint.x, data.basePoint.y);
    FAS_MTextData d(ip, data.height, data.widthscale,
                    valign, halign,
                    dir, lss,
                    interlin,
                    mtext, sty, angle,
                    FAS2::NoUpdate);
    FAS_MText* entity = new FAS_MText(currentContainer, d);
    setEntityAttributes(entity, &data);
    entity->update();
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles texts (TEXT).
void FAS_DXFProcessor::addText(const DXF_Text& data)
{
    FAS_Vector refPoint = FAS_Vector(data.basePoint.x, data.basePoint.y);;
    FAS_Vector secPoint = FAS_Vector(data.secPoint.x, data.secPoint.y);;
    double angle = data.angle;

    if (data.alignV !=0 || data.alignH !=0 ||data.alignH ==DXF_Text::HMiddle)
    {
        if (data.alignH !=DXF_Text::HAligned && data.alignH !=DXF_Text::HFit)
        {
            secPoint = FAS_Vector(data.basePoint.x, data.basePoint.y);
            refPoint = FAS_Vector(data.secPoint.x, data.secPoint.y);
        }
    }

    FAS_TextData::VAlign valign = (FAS_TextData::VAlign)data.alignV;
    FAS_TextData::HAlign halign = (FAS_TextData::HAlign)data.alignH;
    FAS_TextData::TextGeneration dir;
    QString sty = QString::fromUtf8(data.style.c_str());
    sty=sty.toLower();

    if (data.textgen==2)
    {
        dir = FAS_TextData::Backward;
    }
    else if (data.textgen==4)
    {
        dir = FAS_TextData::UpsideDown;
    }
    else
    {
        dir = FAS_TextData::None;
    }

    QString mtext = toNativeString(QString::fromUtf8(data.text.c_str()));
    // use default style for the drawing:
    if (sty.isEmpty())
    {
        // japanese, cyrillic:
        if (codePage=="ANSI_932" || codePage=="ANSI_1251")
        {
            sty = "Unicode";
        }
        else
        {
            sty = textStyle;
        }
    }
    else
    {
        sty = fontList.value(sty, sty);
    }

    FAS_TextData d(refPoint, secPoint, data.height, data.widthscale,
                   valign, halign, dir,
                   mtext, sty, angle*M_PI/180,
                   FAS2::NoUpdate);
    FAS_Text* entity = new FAS_Text(currentContainer, d);

    setEntityAttributes(entity, &data);
    entity->update();
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles dimensions (DIMENSION).
FAS_DimensionData FAS_DXFProcessor::convDimensionData(const  DXF_Dimension* data)
{
    DXF_Coord crd = data->getDefPoint();
    FAS_Vector defP(crd.x, crd.y);
    crd = data->getTextPoint();
    FAS_Vector midP(crd.x, crd.y);
    FAS_MTextData::VAlign valign;
    FAS_MTextData::HAlign halign;
    FAS_MTextData::MTextLineSpacingStyle lss;
    QString sty = QString::fromUtf8(data->getStyle().c_str());

    QString t; //= data.text;
    if (fabs(crd.x)<1.0e-6 && fabs(crd.y)<1.0e-6)
    {
        midP = FAS_Vector(false);
    }

    if (data->getAlign()<=3)
    {
        valign=FAS_MTextData::VATop;
    }
    else if (data->getAlign()<=6)
    {
        valign=FAS_MTextData::VAMiddle;
    }
    else
    {
        valign=FAS_MTextData::VABottom;
    }

    if (data->getAlign()%3==1)
    {
        halign=FAS_MTextData::HALeft;
    }
    else if (data->getAlign()%3==2)
    {
        halign=FAS_MTextData::HACenter;
    }
    else
    {
        halign=FAS_MTextData::HARight;
    }

    if (data->getTextLineStyle()==1)
    {
        lss = FAS_MTextData::AtLeast;
    }
    else
    {
        lss = FAS_MTextData::Exact;
    }

    t = toNativeString(QString::fromUtf8( data->getText().c_str() ));
    if (sty.isEmpty())
    {
        sty = dimStyle;
    }
    // data needed to add the actual dimension entity
    return FAS_DimensionData(defP, midP,
                             valign, halign,
                             lss,
                             data->getTextLineFactor(),
                             t, sty, data->getDir());
}

// Implementation of the method which handles aligned dimensions (DIMENSION).
void FAS_DXFProcessor::addDimAlign(const DXF_DimAligned *data)
{
    FAS_DimensionData dimensionData = convDimensionData((DXF_Dimension*)data);
    FAS_Vector ext1(data->getDef1Point().x, data->getDef1Point().y);
    FAS_Vector ext2(data->getDef2Point().x, data->getDef2Point().y);
    FAS_DimAlignedData d(ext1, ext2);
    FAS_DimAligned* entity = new FAS_DimAligned(currentContainer,
                                                dimensionData, d);
    setEntityAttributes(entity, data);
    entity->updateDimPoint();
    entity->update();
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles linear dimensions (DIMENSION).
void FAS_DXFProcessor::addDimLinear(const DXF_DimLinear *data)
{
    FAS_DimensionData dimensionData = convDimensionData((DXF_Dimension*)data);
    FAS_Vector dxt1(data->getDef1Point().x, data->getDef1Point().y);
    FAS_Vector dxt2(data->getDef2Point().x, data->getDef2Point().y);
    FAS_DimLinearData d(dxt1, dxt2, FAS_Math::deg2rad(data->getAngle()),
                        FAS_Math::deg2rad(data->getOblique()));
    FAS_DimLinear* entity = new FAS_DimLinear(currentContainer,
                                              dimensionData, d);
    setEntityAttributes(entity, data);
    entity->update();
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles radial dimensions (DIMENSION).
void FAS_DXFProcessor::addDimRadial(const DXF_DimRadial* data)
{
    FAS_DimensionData dimensionData = convDimensionData((DXF_Dimension*)data);
    FAS_Vector dp(data->getDiameterPoint().x, data->getDiameterPoint().y);
    FAS_DimRadialData d(dp, data->getLeaderLength());
    FAS_DimRadial* entity = new FAS_DimRadial(currentContainer,
                                              dimensionData, d);
    setEntityAttributes(entity, data);
    entity->update();
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles diametric dimensions (DIMENSION).
void FAS_DXFProcessor::addDimDiametric(const DXF_DimDiametric* data)
{
    FAS_DimensionData dimensionData = convDimensionData((DXF_Dimension*)data);
    FAS_Vector dp(data->getDiameter1Point().x, data->getDiameter1Point().y);
    FAS_DimDiametricData d(dp, data->getLeaderLength());
    FAS_DimDiametric* entity = new FAS_DimDiametric(currentContainer,
                                                    dimensionData, d);
    setEntityAttributes(entity, data);
    entity->update();
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles angular dimensions (DIMENSION).
void FAS_DXFProcessor::addDimAngular(const DXF_DimAngular* data)
{
    FAS_DimensionData dimensionData = convDimensionData(data);
    FAS_Vector dp1(data->getFirstLine1().x, data->getFirstLine1().y);
    FAS_Vector dp2(data->getFirstLine2().x, data->getFirstLine2().y);
    FAS_Vector dp3(data->getSecondLine1().x, data->getSecondLine1().y);
    FAS_Vector dp4(data->getDimPoint().x, data->getDimPoint().y);
    FAS_DimAngularData d(dp1, dp2, dp3, dp4);
    FAS_DimAngular* entity = new FAS_DimAngular(currentContainer,
                                                dimensionData, d);
    setEntityAttributes(entity, data);
    entity->update();
    currentContainer->addEntity(entity);
}

// Implementation of the method which handles angular dimensions (DIMENSION).
void FAS_DXFProcessor::addDimAngular3P(const DXF_DimAngular3p* data)
{
    FAS_DimensionData dimensionData = convDimensionData(data);
    FAS_Vector dp1(data->getFirstLine().x, data->getFirstLine().y);
    FAS_Vector dp2(data->getSecondLine().x, data->getSecondLine().y);
    FAS_Vector dp3(data->getVertexPoint().x, data->getVertexPoint().y);
    FAS_Vector dp4 = dimensionData.definitionPoint;
    dimensionData.definitionPoint = FAS_Vector(data->getVertexPoint().x, data->getVertexPoint().y);
    FAS_DimAngularData d(dp1, dp2, dp3, dp4);
    FAS_DimAngular* entity = new FAS_DimAngular(currentContainer,
                                                dimensionData, d);
    setEntityAttributes(entity, data);
    entity->update();
    currentContainer->addEntity(entity);
}

void FAS_DXFProcessor::addDimOrdinate(const DXF_DimOrdinate* /*data*/)
{
}

// Implementation of the method which handles leader entities.
void FAS_DXFProcessor::addLeader(const DXF_Leader *data)
{
    FAS_LeaderData d(data->arrow!=0);
    FAS_Leader* leader = new FAS_Leader(currentContainer, d);
    setEntityAttributes(leader, data);
    for (auto const& vert: data->vertexlist)
        leader->addVertex({vert->x, vert->y});
    leader->update();
    currentContainer->addEntity(leader);
}

// Implementation of the method which handles hatch entities.
void FAS_DXFProcessor::addHatch(const DXF_Hatch *data)
{
    FAS_Hatch* hatch;
    FAS_EntityContainer* hatchLoop;
    hatch = new FAS_Hatch(currentContainer,
                          FAS_HatchData(data->solid, data->scale, data->angle,
                                        QString::fromUtf8(data->name.c_str())));
    setEntityAttributes(hatch, data);
    currentContainer->appendEntity(hatch);

    for (unsigned int i=0; i < data->looplist.size(); i++)
    {
        auto& loop = data->looplist.at(i);
        if ((loop->type & 32) == 32)
            continue;
        hatchLoop = new FAS_EntityContainer(hatch);
        hatchLoop->setLayer(nullptr);
        hatch->addEntity(hatchLoop);
        FAS_Entity* e = nullptr;
        if ((loop->type & 2) == 2)
        {
            //polyline, convert to lines & arcs
            DXF_LWPolyline* pline = (DXF_LWPolyline *)loop->objlist.at(0).get();
            FAS_Polyline polyline{nullptr, FAS_PolylineData(FAS_Vector(false), FAS_Vector(false), pline->flags)};
            for (auto const& vert: pline->vertlist)
                polyline.addVertex(FAS_Vector{vert->x, vert->y}, vert->bulge);

            for (FAS_Entity* e=polyline.firstEntity(); e; e=polyline.nextEntity())
            {
                FAS_Entity* tmp = e->clone();
                tmp->reparent(hatchLoop);
                tmp->setLayer(nullptr);
                hatchLoop->addEntity(tmp);
            }

        }
        else
        {
            for (unsigned int j=0; j<loop->objlist.size(); j++)
            {
                e = nullptr;
                auto& ent = loop->objlist.at(j);
                switch (ent->eType)
                {
                case DXF::LINE:
                {
                    DXF_Line *e2 = (DXF_Line *)ent.get();
                    e = new FAS_Line{hatchLoop,
                    {{e2->basePoint.x, e2->basePoint.y},
                    {e2->secPoint.x, e2->secPoint.y}}};
                    break;
                }
                case DXF::ARC:
                {
                    DXF_Arc *e2 = (DXF_Arc *)ent.get();
                    if (e2->isccw && e2->staangle<1.0e-6 && e2->endangle>FAS_Math::deg2rad(360)-1.0e-6)
                    {
                        e = new FAS_Circle(hatchLoop,
                        {{e2->basePoint.x, e2->basePoint.y},
                         e2->radious});
                    }
                    else
                    {
                        if (e2->isccw)
                        {
                            e = new FAS_Arc(hatchLoop,
                                            FAS_ArcData(FAS_Vector(e2->basePoint.x, e2->basePoint.y), e2->radious,
                                                        FAS_Math::correctAngle(e2->staangle),
                                                        FAS_Math::correctAngle(e2->endangle),
                                                        false));
                        }
                        else
                        {
                            e = new FAS_Arc(hatchLoop,
                                            FAS_ArcData(FAS_Vector(e2->basePoint.x, e2->basePoint.y), e2->radious,
                                                        FAS_Math::correctAngle(2*M_PI-e2->staangle),
                                                        FAS_Math::correctAngle(2*M_PI-e2->endangle),
                                                        true));
                        }
                    }
                    break;
                }
                case DXF::ELLIPSE:
                {
                    DXF_Ellipse *e2 = (DXF_Ellipse *)ent.get();
                    double ang1 = e2->staparam;
                    double ang2 = e2->endparam;
                    if ( fabs(ang2 - 2.*M_PI) < 1.0e-10 && fabs(ang1) < 1.0e-10 )
                        ang2 = 0.0;
                    else
                    {
                        //convert angle to parameter
                        ang1 = atan(tan(ang1)/e2->ratio);
                        ang2 = atan(tan(ang2)/e2->ratio);
                        if (ang1 < 0)
                        {
                            //quadrant 2 & 4
                            ang1 +=M_PI;
                            if (e2->staparam > M_PI) //quadrant 4
                                ang1 +=M_PI;
                        }
                        else if (e2->staparam > M_PI)
                        {
                            //3 quadrant
                            ang1 +=M_PI;
                        }
                        if (ang2 < 0)
                        {
                            //quadrant 2 & 4
                            ang2 +=M_PI;
                            if (e2->endparam > M_PI) //quadrant 4
                                ang2 +=M_PI;
                        }
                        else if (e2->endparam > M_PI)
                        {
                            //3 quadrant
                            ang2 +=M_PI;
                        }
                    }
                    e = new FAS_Ellipse{hatchLoop, {{e2->basePoint.x, e2->basePoint.y}, {e2->secPoint.x, e2->secPoint.y},
                            e2->ratio, ang1, ang2, !e2->isccw}};
                    break;
                }
                default:
                    break;
                }
                if (e)
                {
                    e->setLayer(nullptr);
                    hatchLoop->addEntity(e);
                }
            }
        }
    }

    if (hatch->validate())
    {
        hatch->update();
    }
    else
    {
        graphic->removeEntity(hatch);
    }
}


// Implementation of the method which handles image entities.
void FAS_DXFProcessor::addImage(const DXF_Image *data)
{
    FAS_Vector ip(data->basePoint.x, data->basePoint.y);
    FAS_Vector uv(data->secPoint.x, data->secPoint.y);
    FAS_Vector vv(data->vVector.x, data->vVector.y);
    FAS_Vector size(data->sizeu, data->sizev);
    FAS_Image* image = new FAS_Image( currentContainer,
                                      FAS_ImageData(data->ref, ip, uv, vv, size,
                                                    QString(""), data->brightness,
                                                    data->contrast, data->fade));
    setEntityAttributes(image, data);
    currentContainer->appendEntity(image);
}

// Implementation of the method which links image entities to image files.
void FAS_DXFProcessor::linkImage(const DXF_ImageDef *data)
{
    int handle = data->handle;
    QString sfile(QString::fromUtf8(data->name.c_str()));
    QFileInfo fiDxf(file);
    QFileInfo fiBitmap(sfile);
    // first: absolute path:
    if (!fiBitmap.exists())
    {
        // try relative path:
        QString f1 = fiDxf.absolutePath() + "/" + sfile;
        if (QFileInfo(f1).exists())
        {
            sfile = f1;
        }
        else
        {
            // try drawing path:
            QString f2 = fiDxf.absolutePath() + "/" + fiBitmap.fileName();
            if (QFileInfo(f2).exists())
            {
                sfile = f2;
            }
        }
    }

    // Also link images in subcontainers (e.g. inserts):
    for (FAS_Entity* e=graphic->firstEntity(FAS2::ResolveNone);
         e; e=graphic->nextEntity(FAS2::ResolveNone))
    {
        if (e->rtti()==FAS2::EntityImage)
        {
            FAS_Image* img = (FAS_Image*)e;
            if (img->getHandle()==handle)
            {
                img->setFile(sfile);
                img->update();
            }
        }
    }

    // update images in blocks:
    for (unsigned i=0; i<graphic->countBlocks(); ++i)
    {
        FAS_Block* b = graphic->blockAt(i);
        for (FAS_Entity* e=b->firstEntity(FAS2::ResolveNone);
             e; e=b->nextEntity(FAS2::ResolveNone))
        {
            if (e->rtti()==FAS2::EntityImage)
            {
                FAS_Image* img = (FAS_Image*)e;
                if (img->getHandle()==handle)
                {
                    img->setFile(sfile);
                    img->update();
                }
            }
        }
    }
}

using std::map;
// Sets the header variables from the DXF file.
void FAS_DXFProcessor::addHeader(const DXF_Header* data)
{
    FAS_Graphic* container = nullptr;
    if (currentContainer->rtti()==FAS2::EntityGraphic)
    {
        container = (FAS_Graphic*)currentContainer;
    } else return;

    map<std::string,DXF_Variant *>::const_iterator it;
    for ( it=data->vars.begin() ; it != data->vars.end(); ++it )
    {
        QString key = QString::fromStdString((*it).first);
        DXF_Variant *var = (*it).second;
        switch (var->type())
        {
        case DXF_Variant::COORD:
            container->addVariable(key,
                                   FAS_Vector(var->content.v->x, var->content.v->y, var->content.v->z), var->code());
            break;
        case DXF_Variant::STRING:
            container->addVariable(key, QString::fromUtf8(var->content.s->c_str()), var->code());
            break;
        case DXF_Variant::INTEGER:
            container->addVariable(key, var->content.i, var->code());
            break;
        case DXF_Variant::DOUBLE:
            container->addVariable(key, var->content.d, var->code());
            break;
        default:
            break;
        }

    }
    codePage = graphic->getVariableString("$DWGCODEPAGE", "ANSI_1252");
    textStyle = graphic->getVariableString("$TEXTSTYLE", "Standard");
    dimStyle = graphic->getVariableString("$DIMSTYLE", "Standard");
    //initialize units vars if not are present in dxf file
    graphic->getVariableInt("$LUNITS", 2);
    graphic->getVariableInt("$LUPREC", 4);
    graphic->getVariableInt("$AUNITS", 0);
    graphic->getVariableInt("$AUPREC", 4);

    QString acadver = versionStr = graphic->getVariableString("$ACADVER", "");
    acadver.replace(QRegExp("[a-zA-Z]"), "");
    bool ok;
    version=acadver.toInt(&ok);
    if (!ok)
    {
        version = 1021;
    }
}


// Implementation of the method used for FAS_Export to communicate with this filter.
bool FAS_DXFProcessor::fileExport(FAS_Graphic& g, const QString& file, FAS2::FormatType type)
{
    this->graphic = &g;
    // check if we can write to that directory:
#ifndef Q_OS_WIN
    QString path = QFileInfo(file).absolutePath();
    if (QFileInfo(path).isWritable()==false) {
        return false;
    }
    //
#endif

    // set version for DXF filter:
    DXF::Version exportVersion = DXF::AC1021;
    version = 1021;
    exactColor = true;

    dxfWriter = new LIBDXFWriter(QFile::encodeName(file));
    bool success = dxfWriter->write(this, exportVersion, false); //ascii
    delete dxfWriter;
    if (!success)
    {
        return false;
    }
    return success;
}

// Prepare unnamed blocks.
void FAS_DXFProcessor::prepareBlocks()
{
    FAS_Block *blk;
    int dimNum = 0, hatchNum= 0;
    QString prefix, sufix;

    //check for existing *D?? or  *U??
    for (unsigned i = 0; i < graphic->countBlocks(); i++)
    {
        blk = graphic->blockAt(i);
        prefix = blk->getName().left(2).toUpper();
        sufix = blk->getName().mid(2);
        if (prefix == "*D")
        {
            if (sufix.toInt() > dimNum) dimNum = sufix.toInt();
        }
        else if (prefix == "*U")
        {
            if (sufix.toInt() > hatchNum) hatchNum = sufix.toInt();
        }
    }
    //Add a name to each dimension, in dxfR12 also for hatches
    for (FAS_Entity *e = graphic->firstEntity(FAS2::ResolveNone); e ; e = graphic->nextEntity(FAS2::ResolveNone))
    {
        if ( !(e->getFlag(FAS2::FlagUndone)) )
        {
            switch (e->rtti())
            {
            case FAS2::EntityDimLinear:
            case FAS2::EntityDimAligned:
            case FAS2::EntityDimAngular:
            case FAS2::EntityDimRadial:
            case FAS2::EntityDimDiametric:
            case FAS2::EntityDimLeader:
                prefix = "*D" + QString::number(++dimNum);
                noNameBlock[e] = prefix;
                break;
            case FAS2::EntityHatch:
                if (version==1009) {
                    if ( !((FAS_Hatch*)e)->isSolid() )
                    {
                        prefix = "*U" + QString::number(++hatchNum);
                        noNameBlock[e] = prefix;
                    }
                }
                break;
            default:
                break;
            }//end switch
        }//end if !FAS2::FlagUndone
    }
}

// Writes block records (just the name, not the entities in it).
void FAS_DXFProcessor::writeBlockRecords()
{
    //first prepare and send unnamed blocks, the while loop can be ommited for R12
    prepareBlocks();
    QHash<FAS_Entity*, QString>::const_iterator it = noNameBlock.constBegin();
    while (it != noNameBlock.constEnd())
    {
        dxfWriter->writeBlockRecord(it.value().toStdString());
        ++it;
    }

    //next send "normal" blocks
    FAS_Block *blk;
    for (unsigned i = 0; i < graphic->countBlocks(); i++)
    {
        blk = graphic->blockAt(i);
        if (!blk->isUndone())
        {
            dxfWriter->writeBlockRecord(blk->getName().toUtf8().data());
        }
    }
}

// Writes blocks.
void FAS_DXFProcessor::writeBlocks()
{
    FAS_Block *blk;
    //write unnamed blocks
    QHash<FAS_Entity*, QString>::const_iterator it = noNameBlock.constBegin();
    while (it != noNameBlock.constEnd())
    {
        DXF_Block block;
        block.name = it.value().toStdString();
        block.basePoint.x = 0.0;
        block.basePoint.y = 0.0;
        block.basePoint.z = 0.0;
        block.flags = 1;//flag for unnamed block
        dxfWriter->writeBlock(&block);
        FAS_EntityContainer *ct = (FAS_EntityContainer *)it.key();
        for (FAS_Entity* e=ct->firstEntity(FAS2::ResolveNone);
             e; e=ct->nextEntity(FAS2::ResolveNone))
        {
            if ( !(e->getFlag(FAS2::FlagUndone)) )
            {
                writeEntity(e);
            }
        }
        ++it;
    }

    //next write "normal" blocks
    for (unsigned i = 0; i < graphic->countBlocks(); i++)
    {
        blk = graphic->blockAt(i);
        if (!blk->isUndone())
        {
            DXF_Block block;
            block.name = blk->getName().toUtf8().data();
            block.basePoint.x = blk->getBasePoint().x;
            block.basePoint.y = blk->getBasePoint().y;
            block.basePoint.z = blk->getBasePoint().z;
            dxfWriter->writeBlock(&block);
            for (FAS_Entity* e=blk->firstEntity(FAS2::ResolveNone);
                 e; e=blk->nextEntity(FAS2::ResolveNone))
            {
                if ( !(e->getFlag(FAS2::FlagUndone)) )
                {
                    writeEntity(e);
                }
            }
        }
    }
}


void FAS_DXFProcessor::writeHeader(DXF_Header& data)
{
    FAS_Vector v;
    QHash<QString, FAS_Variable>vars = graphic->getVariableDict();
    QHash<QString, FAS_Variable>::iterator it = vars.begin();
    if (!vars.contains ( "$DWGCODEPAGE" ))
    {
        codePage = FAS_SYSTEM->localeToISO(QLocale::system().name().toLocal8Bit());
        vars.insert(QString("$DWGCODEPAGE"), FAS_Variable(codePage, 0) );
    }

    while (it != vars.end())
    {
        switch (it.value().getType())
        {
        case FAS2::VariableInt:
            data.addInt(it.key().toStdString(), it.value().getInt(), it.value().getCode());
            break;
        case FAS2::VariableDouble:
            data.addDouble(it.key().toStdString(), it.value().getDouble(), it.value().getCode());
            break;
        case FAS2::VariableString:
            data.addStr(it.key().toStdString(), it.value().getString().toUtf8().data(), it.value().getCode());
            break;
        case FAS2::VariableVector:
            v = it.value().getVector();
            data.addCoord(it.key().toStdString(), DXF_Coord(v.x, v.y, v.z), it.value().getCode());
            break;
        default:
            break;
        }
        ++it;
    }
    v = graphic->getMin();
    data.addCoord("$EXTMIN", DXF_Coord(v.x, v.y, 0.0), 0);
    v = graphic->getMax();
    data.addCoord("$EXTMAX", DXF_Coord(v.x, v.y, 0.0), 0);

    //when saving a block, there is no active layer. ignore it to avoid crash
    if(graphic->getActiveLayer()==0) return;
    data.addStr("$CLAYER", (graphic->getActiveLayer()->getName()).toUtf8().data(), 8);
}

void FAS_DXFProcessor::writeLTypes()
{
    DXF_LType ltype;
    // Standard linetypes for  AutoCAD
    ltype.name = "CONTINUOUS";
    ltype.desc = "Solid line";
    dxfWriter->writeLineType(&ltype);
    ltype.name = "ByLayer";
    dxfWriter->writeLineType(&ltype);
    ltype.name = "ByBlock";
    dxfWriter->writeLineType(&ltype);

    ltype.name = "DOT";
    ltype.desc = "Dot . . . . . . . . . . . . . . . . . . . . . .";
    ltype.size = 2;
    ltype.length = 6.35;
    ltype.path.push_back(0.0);
    ltype.path.push_back(-6.35);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DOTTINY";
    ltype.desc = "Dot (.15x) .....................................";
    ltype.size = 2;
    ltype.length = 0.9525;
    ltype.path.push_back(0.0);
    ltype.path.push_back(-0.9525);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DOT2";
    ltype.desc = "Dot (.5x) .....................................";
    ltype.size = 2;
    ltype.length = 3.175;
    ltype.path.push_back(0.0);
    ltype.path.push_back(-3.175);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DOTX2";
    ltype.desc = "Dot (2x) .  .  .  .  .  .  .  .  .  .  .  .  .";
    ltype.size = 2;
    ltype.length = 12.7;
    ltype.path.push_back(0.0);
    ltype.path.push_back(-12.7);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DASHED";
    ltype.desc = "Dashed _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _";
    ltype.size = 2;
    ltype.length = 19.05;
    ltype.path.push_back(12.7);
    ltype.path.push_back(-6.35);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DASHEDTINY";
    ltype.desc = "Dashed (.15x) _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _";
    ltype.size = 2;
    ltype.length = 2.8575;
    ltype.path.push_back(1.905);
    ltype.path.push_back(-0.9525);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DASHED2";
    ltype.desc = "Dashed (.5x) _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _";
    ltype.size = 2;
    ltype.length = 9.525;
    ltype.path.push_back(6.35);
    ltype.path.push_back(-3.175);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DASHEDX2";
    ltype.desc = "Dashed (2x) ____  ____  ____  ____  ____  ___";
    ltype.size = 2;
    ltype.length = 38.1;
    ltype.path.push_back(25.4);
    ltype.path.push_back(-12.7);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DASHDOT";
    ltype.desc = "Dash dot __ . __ . __ . __ . __ . __ . __ . __";
    ltype.size = 4;
    ltype.length = 25.4;
    ltype.path.push_back(12.7);
    ltype.path.push_back(-6.35);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-6.35);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DASHDOTTINY";
    ltype.desc = "Dash dot (.15x) _._._._._._._._._._._._._._._.";
    ltype.size = 4;
    ltype.length = 3.81;
    ltype.path.push_back(1.905);
    ltype.path.push_back(-0.9525);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-0.9525);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DASHDOT2";
    ltype.desc = "Dash dot (.5x) _._._._._._._._._._._._._._._.";
    ltype.size = 4;
    ltype.length = 12.7;
    ltype.path.push_back(6.35);
    ltype.path.push_back(-3.175);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-3.175);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DASHDOTX2";
    ltype.desc = "Dash dot (2x) ____  .  ____  .  ____  .  ___";
    ltype.size = 4;
    ltype.length = 50.8;
    ltype.path.push_back(25.4);
    ltype.path.push_back(-12.7);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-12.7);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DIVIDE";
    ltype.desc = "Divide ____ . . ____ . . ____ . . ____ . . ____";
    ltype.size = 6;
    ltype.length = 31.75;
    ltype.path.push_back(12.7);
    ltype.path.push_back(-6.35);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-6.35);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-6.35);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DIVIDETINY";
    ltype.desc = "Divide (.15x) __..__..__..__..__..__..__..__.._";
    ltype.size = 6;
    ltype.length = 4.7625;
    ltype.path.push_back(1.905);
    ltype.path.push_back(-0.9525);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-0.9525);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-0.9525);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DIVIDE2";
    ltype.desc = "Divide (.5x) __..__..__..__..__..__..__..__.._";
    ltype.size = 6;
    ltype.length = 15.875;
    ltype.path.push_back(6.35);
    ltype.path.push_back(-3.175);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-3.175);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-3.175);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "DIVIDEX2";
    ltype.desc = "Divide (2x) ________  .  .  ________  .  .  _";
    ltype.size = 6;
    ltype.length = 63.5;
    ltype.path.push_back(25.4);
    ltype.path.push_back(-12.7);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-12.7);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-12.7);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "BORDER";
    ltype.desc = "Border __ __ . __ __ . __ __ . __ __ . __ __ .";
    ltype.size = 6;
    ltype.length = 44.45;
    ltype.path.push_back(12.7);
    ltype.path.push_back(-6.35);
    ltype.path.push_back(12.7);
    ltype.path.push_back(-6.35);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-6.35);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "BORDERTINY";
    ltype.desc = "Border (.15x) __.__.__.__.__.__.__.__.__.__.__.";
    ltype.size = 6;
    ltype.length = 6.6675;
    ltype.path.push_back(1.905);
    ltype.path.push_back(-0.9525);
    ltype.path.push_back(1.905);
    ltype.path.push_back(-0.9525);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-0.9525);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "BORDER2";
    ltype.desc = "Border (.5x) __.__.__.__.__.__.__.__.__.__.__.";
    ltype.size = 6;
    ltype.length = 22.225;
    ltype.path.push_back(6.35);
    ltype.path.push_back(-3.175);
    ltype.path.push_back(6.35);
    ltype.path.push_back(-3.175);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-3.175);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "BORDERX2";
    ltype.desc = "Border (2x) ____  ____  .  ____  ____  .  ___";
    ltype.size = 6;
    ltype.length = 88.9;
    ltype.path.push_back(25.4);
    ltype.path.push_back(-12.7);
    ltype.path.push_back(25.4);
    ltype.path.push_back(-12.7);
    ltype.path.push_back(0.0);
    ltype.path.push_back(-12.7);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "CENTER";
    ltype.desc = "Center ____ _ ____ _ ____ _ ____ _ ____ _ ____";
    ltype.size = 4;
    ltype.length = 50.8;
    ltype.path.push_back(31.75);
    ltype.path.push_back(-6.35);
    ltype.path.push_back(6.35);
    ltype.path.push_back(-6.35);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "CENTERTINY";
    ltype.desc = "Center (.15x) ___ _ ___ _ ___ _ ___ _ ___ _ ___";
    ltype.size = 4;
    ltype.length = 7.62;
    ltype.path.push_back(4.7625);
    ltype.path.push_back(-0.9525);
    ltype.path.push_back(0.9525);
    ltype.path.push_back(-0.9525);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "CENTER2";
    ltype.desc = "Center (.5x) ___ _ ___ _ ___ _ ___ _ ___ _ ___";
    ltype.size = 4;
    ltype.length = 28.575;
    ltype.path.push_back(19.05);
    ltype.path.push_back(-3.175);
    ltype.path.push_back(3.175);
    ltype.path.push_back(-3.175);
    dxfWriter->writeLineType(&ltype);

    ltype.path.clear();
    ltype.name = "CENTERX2";
    ltype.desc = "Center (2x) ________  __  ________  __  _____";
    ltype.size = 4;
    ltype.length = 101.6;
    ltype.path.push_back(63.5);
    ltype.path.push_back(-12.7);
    ltype.path.push_back(12.7);
    ltype.path.push_back(-12.7);
    dxfWriter->writeLineType(&ltype);
}

void FAS_DXFProcessor::writeLayers()
{
    DXF_Layer lay;
    FAS_LayerList* ll = graphic->getLayerList();
    int exact_rgb;
    for (unsigned int i = 0; i < ll->count(); i++)
    {
        lay.reset();
        FAS_Layer* l = ll->at(i);
        FAS_Pen pen = l->getPen();
        lay.name = l->getName().toUtf8().data();
        lay.color = colorToNumber(pen.getColor(), &exact_rgb);
        lay.color24 = exact_rgb;
        lay.lWeight = widthToNumber(pen.getWidth());
        lay.lineType = lineTypeToName(pen.getLineType()).toStdString();
        lay.flags = l->isFrozen() ? 0x01 : 0x00;
        if (l->isLocked()) lay.flags |=0x04;
        lay.plotF = l->isPrint();
        if( l->isConstruction())
        {
            lay.extData.push_back(new DXF_Variant(1001, "LibreCad"));
            lay.extData.push_back(new DXF_Variant(1070, 1));
        }
        dxfWriter->writeLayer(&lay);
    }
}

void FAS_DXFProcessor::writeTextstyles()
{
    QHash<QString, QString> styles;
    QString sty;
    //Find fonts used by text entities in drawing
    for (FAS_Entity *e = graphic->firstEntity(FAS2::ResolveNone); e ; e = graphic->nextEntity(FAS2::ResolveNone))
    {
        if ( !(e->getFlag(FAS2::FlagUndone)) )
        {
            switch (e->rtti())
            {
            case FAS2::EntityMText:
                sty = ((FAS_MText*)e)->getStyle();
                break;
            case FAS2::EntityText:
                sty = ((FAS_Text*)e)->getStyle();
                break;
            default:
                sty.clear();
                break;
            }
            if (!sty.isEmpty() && !styles.contains(sty))
                styles.insert(sty, sty);
        }
    }
    //Find fonts used by text entities in blocks
    FAS_Block *blk;
    for (unsigned i = 0; i < graphic->countBlocks(); i++)
    {
        blk = graphic->blockAt(i);
        for (FAS_Entity *e = blk->firstEntity(FAS2::ResolveNone); e ; e = blk->nextEntity(FAS2::ResolveNone))
        {
            if ( !(e->getFlag(FAS2::FlagUndone)) )
            {
                switch (e->rtti())
                {
                case FAS2::EntityMText:
                    sty = ((FAS_MText*)e)->getStyle();
                    break;
                case FAS2::EntityText:
                    sty = ((FAS_Text*)e)->getStyle();
                    break;
                default:
                    sty.clear();
                    break;
                }
                if (!sty.isEmpty() && !styles.contains(sty))
                    styles.insert(sty, sty);
            }
        }
    }
    DXF_Textstyle ts;
    QHash<QString, QString>::const_iterator it = styles.constBegin();
    while (it != styles.constEnd())
    {
        ts.name = (it.key()).toStdString();
        ts.font = it.value().toStdString();
        dxfWriter->writeTextstyle( &ts );
        ++it;
    }
}

void FAS_DXFProcessor::writeVports()
{
    DXF_Vport vp;
    vp.name = "*Active";
    graphic->isGridOn()? vp.grid = 1 : vp.grid = 0;
    FAS_Vector spacing = graphic->getVariableVector("$GRIDUNIT", FAS_Vector(0.0,0.0));
    vp.gridBehavior = 3;
    vp.gridSpacing.x = spacing.x;
    vp.gridSpacing.y = spacing.y;
    vp.snapStyle = graphic->isIsometricGrid();
    vp.snapIsopair = graphic->getCrosshairType();
    if (vp.snapIsopair > 2)
        vp.snapIsopair = 0;
    if (fabs(spacing.x) < 1.0e-6)
    {
        vp.gridBehavior = 7; //auto
        vp.gridSpacing.x = 10;
    }
    if (fabs(spacing.y) < 1.0e-6)
    {
        vp.gridBehavior = 7; //auto
        vp.gridSpacing.y = 10;
    }
    FAS_GraphicView *gv = graphic->getGraphicView();
    if (gv )
    {
        FAS_Vector fac =gv->getFactor();
        vp.height = gv->getHeight()/fac.y;
        vp.ratio = (double)gv->getWidth() / (double)gv->getHeight();
        vp.center.x = ( gv->getWidth() - gv->getOffsetX() )/ (fac.x * 2.0);
        vp.center.y = ( gv->getHeight() - gv->getOffsetY() )/ (fac.y * 2.0);
    }
    dxfWriter->writeVport(&vp);
}

void FAS_DXFProcessor::writeDimstyles()
{
    DXF_Dimstyle dsty;
    dsty.name = "Standard";
    dsty.dimscale = graphic->getVariableDouble("$DIMSCALE", 1.0);
    dsty.dimasz = graphic->getVariableDouble("$DIMASZ", 2.5);
    dsty.dimexo = graphic->getVariableDouble("$DIMEXO", 0.625);
    dsty.dimexe = graphic->getVariableDouble("$DIMEXE", 1.25);
    dsty.dimfxl = graphic->getVariableDouble("$DIMFXL", 1.0);
    dsty.dimtxt = graphic->getVariableDouble("$DIMTXT", 2.5);
    dsty.dimtsz = graphic->getVariableDouble("$DIMTSZ", 2.5);
    dsty.dimlfac = graphic->getVariableDouble("$DIMLFAC", 1.0);
    dsty.dimgap = graphic->getVariableDouble("$DIMGAP", 0.625);
    dsty.dimtih = graphic->getVariableInt("$DIMTIH", 2);
    dsty.dimzin = graphic->getVariableInt("$DIMZIN", 1);
    dsty.dimazin = graphic->getVariableInt("$DIMAZIN", 0);
    dsty.dimclrd = graphic->getVariableInt("$DIMCLRD", 0);
    dsty.dimclre = graphic->getVariableInt("$DIMCLRE", 0);
    dsty.dimclrt = graphic->getVariableInt("$DIMCLRT", 0);
    dsty.dimadec = graphic->getVariableInt("$DIMADEC", 0);
    dsty.dimdec = graphic->getVariableInt("$DIMDEC", 2);
    dsty.dimaunit = graphic->getVariableInt("$DIMAUNIT", 0);
    dsty.dimlunit = graphic->getVariableInt("$DIMLUNIT", 2);
    dsty.dimdsep = graphic->getVariableInt("$DIMDSEP", 0);
    dsty.dimfxlon = graphic->getVariableInt("$DIMFXLON", 0);
    dsty.dimtxsty = graphic->getVariableString("$DIMTXSTY", "standard").toStdString();
    dsty.dimlwd = graphic->getVariableInt("$DIMLWD", -2);
    dsty.dimlwe = graphic->getVariableInt("$DIMLWE", -2);
    dxfWriter->writeDimstyle(&dsty);
}

void FAS_DXFProcessor::writeAppId()
{
    DXF_AppId ai;
    ai.name ="SolutionClient";
    dxfWriter->writeAppId(&ai);
}

void FAS_DXFProcessor::writeEntities()
{
    for (FAS_Entity *e = graphic->firstEntity(FAS2::ResolveNone); e ; e = graphic->nextEntity(FAS2::ResolveNone))
    {
        if ( !(e->getFlag(FAS2::FlagUndone)) )
        {
            writeEntity(e);
        }
    }
}

void FAS_DXFProcessor::writeEntity(FAS_Entity* e)
{
    switch (e->rtti())
    {
    case FAS2::EntityPoint:
        writePoint((FAS_Point*)e);
        break;
    case FAS2::EntityLine:
        writeLine((FAS_Line*)e);
        break;
    case FAS2::EntityCircle:
        writeCircle((FAS_Circle*)e);
        break;
    case FAS2::EntityArc:
        writeArc((FAS_Arc*)e);
        break;
    case FAS2::EntitySolid:
        writeSolid((FAS_Solid*)e);
        break;
    case FAS2::EntityEllipse:
        writeEllipse((FAS_Ellipse*)e);
        break;
    case FAS2::EntityPolyline:
        writeLWPolyline((FAS_Polyline*)e);
        break;
    case FAS2::EntitySpline:
        writeSpline((FAS_Spline*)e);
        break;
    case FAS2::EntitySplinePoints:
        writeSplinePoints((LC_SplinePoints*)e);
        break;
    case FAS2::EntityInsert:
        writeInsert((FAS_Insert*)e);
        break;
    case FAS2::EntityMText:
        writeMText((FAS_MText*)e);
        break;
    case FAS2::EntityText:
        writeText((FAS_Text*)e);
        break;
    case FAS2::EntityDimLinear:
    case FAS2::EntityDimAligned:
    case FAS2::EntityDimAngular:
    case FAS2::EntityDimRadial:
    case FAS2::EntityDimDiametric:
        writeDimension((FAS_Dimension*)e);
        break;
    case FAS2::EntityDimLeader:
        writeLeader((FAS_Leader*)e);
        break;
    case FAS2::EntityHatch:
        writeHatch((FAS_Hatch*)e);
        break;
    case FAS2::EntityImage:
        writeImage((FAS_Image*)e);
        break;
    case FAS2::EntityAttdef:
        writeAttdef((FAS_ATTDEF*)e);
        break;
    default:
        break;
    }
}

// Writes the given Point entity to the file.
void FAS_DXFProcessor::writePoint(FAS_Point* p)
{
    DXF_Point point;
    getEntityAttributes(&point, p);
    point.basePoint.x = p->getStartpoint().x;
    point.basePoint.y = p->getStartpoint().y;
    dxfWriter->writePoint(&point);
}


// Writes the given Line( entity to the file.
void FAS_DXFProcessor::writeLine(FAS_Line* l)
{
    DXF_Line line;
    getEntityAttributes(&line, l);
    line.basePoint.x = l->getStartpoint().x;
    line.basePoint.y = l->getStartpoint().y;
    line.secPoint.x = l->getEndpoint().x;
    line.secPoint.y = l->getEndpoint().y;
    dxfWriter->writeLine(&line);
}

// Writes the given circle entity to the file.
void FAS_DXFProcessor::writeCircle(FAS_Circle* c)
{
    DXF_Circle circle;
    getEntityAttributes(&circle, c);
    circle.basePoint.x = c->getCenter().x;
    circle.basePoint.y = c->getCenter().y;
    circle.radious = c->getRadius();
    dxfWriter->writeCircle(&circle);
}

// Writes the given arc entity to the file.
void FAS_DXFProcessor::writeArc(FAS_Arc* a)
{
    DXF_Arc arc;
    getEntityAttributes(&arc, a);
    arc.basePoint.x = a->getCenter().x;
    arc.basePoint.y = a->getCenter().y;
    arc.radious = a->getRadius();
    if (a->isReversed())
    {
        arc.staangle = a->getAngle2();
        arc.endangle = a->getAngle1();
    }
    else
    {
        arc.staangle = a->getAngle1();
        arc.endangle = a->getAngle2();
    }
    dxfWriter->writeArc(&arc);
}

// Writes the given polyline entity to the file as lwpolyline.
void FAS_DXFProcessor::writeLWPolyline(FAS_Polyline* l)
{
    //skip if are empty polyline
    if (l->isEmpty())
        return;
    // version 12 are old style polyline
    if (version==1009)
    {
        writePolyline(l);
        return;
    }
    DXF_LWPolyline pol;
    FAS_Entity* currEntity = 0;
    FAS_Entity* nextEntity = 0;
    FAS_AtomicEntity* ae = nullptr;
    double bulge=0.0;

    for (FAS_Entity* e=l->firstEntity(FAS2::ResolveNone); e; e=nextEntity)
    {
        currEntity = e;
        nextEntity = l->nextEntity(FAS2::ResolveNone);
        if (!e->isAtomic())
            continue;

        ae = (FAS_AtomicEntity*)e;
        // Write vertex:
        if (e->rtti()==FAS2::EntityArc)
        {
            bulge = ((FAS_Arc*)e)->getBulge();
        }
        else
            bulge = 0.0;
        pol.addVertex( DXF_Vertex2D(ae->getStartpoint().x, ae->getStartpoint().y, bulge));
    }
    if (l->isClosed())
    {
        pol.flags = 1;
    }
    else
    {
        ae = (FAS_AtomicEntity*)currEntity;
        if (ae->rtti()==FAS2::EntityArc)
        {
            bulge = ((FAS_Arc*)ae)->getBulge();
        }
        pol.addVertex( DXF_Vertex2D(ae->getEndpoint().x, ae->getEndpoint().y, bulge));
    }
    pol.vertexnum = pol.vertlist.size();
    getEntityAttributes(&pol, l);
    dxfWriter->writeLWPolyline(&pol);
}

// Writes the given polyline entity to the file (old style).
void FAS_DXFProcessor::writePolyline(FAS_Polyline* p)
{
    DXF_Polyline pol;
    FAS_Entity* currEntity = 0;
    FAS_Entity* nextEntity = 0;
    FAS_AtomicEntity* ae = nullptr;
    double bulge=0.0;

    for (FAS_Entity* e=p->firstEntity(FAS2::ResolveNone); e; e=nextEntity)
    {
        currEntity = e;
        nextEntity = p->nextEntity(FAS2::ResolveNone);

        if (!e->isAtomic())
        {
            continue;
        }
        ae = (FAS_AtomicEntity*)e;

        // Write vertex:
        if (e->rtti()==FAS2::EntityArc)
        {
            bulge = ((FAS_Arc*)e)->getBulge();
        }
        else
            bulge = 0.0;
        pol.addVertex( DXF_Vertex(ae->getStartpoint().x, ae->getStartpoint().y, 0.0, bulge));
    }
    if (p->isClosed())
    {
        pol.flags = 1;
    }
    else
    {
        ae = (FAS_AtomicEntity*)currEntity;
        if (ae->rtti()==FAS2::EntityArc)
        {
            bulge = ((FAS_Arc*)ae)->getBulge();
        }
        pol.addVertex( DXF_Vertex(ae->getEndpoint().x, ae->getEndpoint().y, 0.0, bulge));
    }
    getEntityAttributes(&pol, p);
    dxfWriter->writePolyline(&pol);
}

// Writes the given spline entity to the file.
void FAS_DXFProcessor::writeSpline(FAS_Spline *s)
{
    if (s->getNumberOfControlPoints() < s->getDegree()+1)
    {
        return;
    }

    // version 12 do not support Spline write as polyline
    if (version==1009)
    {
        DXF_Polyline pol;
        FAS_Entity* e;
        for (e=s->firstEntity(FAS2::ResolveNone); e; e=s->nextEntity(FAS2::ResolveNone))
        {
            pol.addVertex( DXF_Vertex(e->getStartpoint().x, e->getStartpoint().y, 0.0, 0.0));
        }
        if (s->isClosed())
        {
            pol.flags = 1;
        }
        else
        {
            pol.addVertex( DXF_Vertex(s->getEndpoint().x,
                                      s->getEndpoint().y, 0.0, 0.0));
        }
        getEntityAttributes(&pol, s);
        dxfWriter->writePolyline(&pol);
        return;
    }

    DXF_Spline sp;
    if (s->isClosed())
        sp.flags = 11;
    else
        sp.flags = 8;
    sp.ncontrol = s->getNumberOfControlPoints();
    sp.degree = s->getDegree();
    sp.nknots = sp.ncontrol + sp.degree + 1;

    // write spline knots:
    if (s->getData().knotslist.size())
    {
        sp.knotslist = s->getData().knotslist;
    }
    else
    {
        int k = sp.degree+1;
        for (int i=1; i<=sp.nknots; i++)
        {
            if (i<=k)
            {
                sp.knotslist.push_back(0.0);
            }
            else if (i<=sp.nknots-k)
            {
                sp.knotslist.push_back(1.0/(sp.nknots-2*k+1) * (i-k));
            }
            else
            {
                sp.knotslist.push_back(1.0);
            }
        }
    }

    // write spline control points:
    auto cp = s->getControlPoints();
    for (const FAS_Vector& v: cp)
        sp.controllist.push_back(std::make_shared<DXF_Coord>(v.x, v.y, 0.));
    getEntityAttributes(&sp, s);
    dxfWriter->writeSpline(&sp);
}

// Writes the given spline entity to the file.
void FAS_DXFProcessor::writeSplinePoints(LC_SplinePoints *s)
{
    int nCtrls = s->getNumberOfControlPoints();
    auto const& cp = s->getControlPoints();

    if(nCtrls < 3)
    {
        if(nCtrls > 1)
        {
            DXF_Line line;
            line.basePoint.x = cp.at(0).x;
            line.basePoint.y = cp.at(0).y;
            line.secPoint.x = cp.at(1).x;
            line.secPoint.y = cp.at(1).y;
            getEntityAttributes(&line, s);
            dxfWriter->writeLine(&line);
        }
        return;
    }

    // version 12 do not support Spline write as polyline
    if(version == 1009)
    {
        DXF_Polyline pol;
        auto const& sp = s->getStrokePoints();

        for(size_t i = 0; i < sp.size(); i++)
        {
            pol.addVertex(DXF_Vertex(sp.at(i).x, sp.at(i).y, 0.0, 0.0));
        }

        if(s->isClosed()) pol.flags = 1;

        getEntityAttributes(&pol, s);
        dxfWriter->writePolyline(&pol);
        return;
    }

    DXF_Spline sp;

    if(s->isClosed()) sp.flags = 11;
    else sp.flags = 8;

    sp.ncontrol = nCtrls;
    sp.degree = 2;
    sp.nknots = nCtrls + 3;

    // write spline knots:
    for(int i = 1; i <= sp.nknots; i++)
    {
        if(i <= 3)
        {
            sp.knotslist.push_back(0.0);
        }
        else if(i <= nCtrls)
        {
            sp.knotslist.push_back((i - 3.0)/(nCtrls - 2.0));
        }
        else
        {
            sp.knotslist.push_back(1.0);
        }
    }

    // write spline control points:
    for (auto const& v: cp)
        sp.controllist.push_back(std::make_shared<DXF_Coord>(v.x, v.y, 0.));

    getEntityAttributes(&sp, s);
    dxfWriter->writeSpline(&sp);
}

// Writes the given Ellipse entity to the file.
void FAS_DXFProcessor::writeEllipse(FAS_Ellipse* s)
{
    DXF_Ellipse el;
    getEntityAttributes(&el, s);
    el.basePoint.x = s->getCenter().x;
    el.basePoint.y = s->getCenter().y;
    el.secPoint.x = s->getMajorP().x;
    el.secPoint.y = s->getMajorP().y;
    el.ratio = s->getRatio();
    if (s->isReversed())
    {
        el.staparam = s->getAngle2();
        el.endparam = s->getAngle1();
    }
    else
    {
        el.staparam = s->getAngle1();
        el.endparam = s->getAngle2();
    }
    dxfWriter->writeEllipse(&el);
}

// Writes the given block insert entity to the file.
void FAS_DXFProcessor::writeInsert(FAS_Insert* i)
{
    DXF_Insert in;
    getEntityAttributes(&in, i);
    in.basePoint.x = i->getInsertionPoint().x;
    in.basePoint.y = i->getInsertionPoint().y;
    in.basePoint.z = i->getInsertionPoint().z;
    in.name = i->getName().toUtf8().data();
    in.xscale = i->getScale().x;
    in.yscale = i->getScale().y;
    in.zscale = i->getScale().z;
    in.angle = i->getAngle();
    in.colcount = i->getCols();
    in.rowcount = i->getRows();
    in.colspace = i->getSpacing().x;
    in.rowspace =i->getSpacing().y;
    dxfWriter->writeInsert(&in);
}

// Writes the given mText entity to the file.
void FAS_DXFProcessor::writeMText(FAS_MText* t)
{
    DXF_Text *text;
    DXF_Text txt1;
    DXF_MText txt2;

    if (version==1009)
        text = &txt1;
    else
        text = &txt2;

    getEntityAttributes(text, t);
    text->basePoint.x = t->getInsertionPoint().x;
    text->basePoint.y = t->getInsertionPoint().y;
    text->height = t->getHeight();
    text->angle = t->getAngle()*180/M_PI;
    text->style = t->getStyle().toStdString();

    if (version==1009)
    {
        if (t->getHAlign()==FAS_MTextData::HALeft)
        {
            text->alignH =DXF_Text::HLeft;
        } else if (t->getHAlign()==FAS_MTextData::HACenter)
        {
            text->alignH =DXF_Text::HCenter;
        }
        else if (t->getHAlign()==FAS_MTextData::HARight)
        {
            text->alignH = DXF_Text::HRight;
        }
        if (t->getVAlign()==FAS_MTextData::VATop)
        {
            text->alignV = DXF_Text::VTop;
        }
        else if (t->getVAlign()==FAS_MTextData::VAMiddle)
        {
            text->alignV = DXF_Text::VMiddle;
        } else if (t->getVAlign()==FAS_MTextData::VABottom)
        {
            text->alignV = DXF_Text::VBaseLine;
        }
        QStringList txtList = t->getText().split('\n',QString::KeepEmptyParts);
        double dist = t->getLineSpacingFactor()*5*t->getHeight()/3;
        bool setSec = false;
        if (text->alignH != DXF_Text::HLeft || text->alignV != DXF_Text::VBaseLine)
        {
            text->secPoint.x = t->getInsertionPoint().x;
            text->secPoint.y = t->getInsertionPoint().y;
            setSec = true;
        }
        if (text->alignV == DXF_Text::VTop)
            dist = dist * -1;
        for (int i=0; i<txtList.size();++i)
        {
            if (!txtList.at(i).isEmpty())
            {
                text->text = toDxfString(txtList.at(i)).toUtf8().data();
                FAS_Vector inc  = FAS_Vector::polar(dist*i, t->getAngle()+M_PI_2);
                if (setSec)
                {
                    text->secPoint.x += inc.x;
                    text->secPoint.y += inc.y;
                }
                else
                {
                    text->basePoint.x += inc.x;
                    text->basePoint.y += inc.y;
                }
                dxfWriter->writeText(text);
            }
        }
    }
    else
    {
        if (t->getHAlign()==FAS_MTextData::HALeft)
        {
            text->textgen =1;
        }
        else if (t->getHAlign()==FAS_MTextData::HACenter)
        {
            text->textgen =2;
        }
        else if (t->getHAlign()==FAS_MTextData::HARight)
        {
            text->textgen = 3;
        }
        if (t->getVAlign()==FAS_MTextData::VAMiddle)
        {
            text->textgen += 3;
        }
        else if (t->getVAlign()==FAS_MTextData::VABottom)
        {
            text->textgen += 6;
        }
        if (t->getDrawingDirection() == FAS_MTextData::LeftToRight)
            text->alignH = (DXF_Text::HAlign)1;
        else if (t->getDrawingDirection() == FAS_MTextData::TopToBottom)
            text->alignH = (DXF_Text::HAlign)3;
        else
            text->alignH = (DXF_Text::HAlign)5;
        if (t->getLineSpacingStyle() == FAS_MTextData::AtLeast)
            text->alignV = (DXF_Text::VAlign)1;
        else
            text->alignV = (DXF_Text::VAlign)2;

        text->text = toDxfString(t->getText()).toUtf8().data();
        text->widthscale =t->getUsedTextWidth(); //getSize().x;
        txt2.interlin = t->getLineSpacingFactor();
        dxfWriter->writeMText((DXF_MText*)text);
    }
}

// Writes the given Text entity to the file.
void FAS_DXFProcessor::writeText(FAS_Text* t)
{
    DXF_Text text;
    getEntityAttributes(&text, t);
    text.basePoint.x = t->getInsertionPoint().x;
    text.basePoint.y = t->getInsertionPoint().y;
    text.height = t->getHeight();
    text.angle = t->getAngle()*180/M_PI;
    text.style = t->getStyle().toStdString();
    text.alignH =(DXF_Text::HAlign)t->getHAlign();
    text.alignV =(DXF_Text::VAlign)t->getVAlign();

    if (text.alignV != DXF_Text::VBaseLine || text.alignH != DXF_Text::HLeft)
    {
        if (text.alignH == DXF_Text::HAligned || text.alignH == DXF_Text::HFit)
        {
            text.secPoint.x = t->getSecondPoint().x;
            text.secPoint.y = t->getSecondPoint().y;
        }
        else
        {
            text.secPoint.x = t->getInsertionPoint().x;
            text.secPoint.y = t->getInsertionPoint().y;
        }
    }
    if (!t->getText().isEmpty())
    {
        text.text = toDxfString(t->getText()).toUtf8().data();
        dxfWriter->writeText(&text);
    }
}

// Writes the given dimension entity to the file.
void FAS_DXFProcessor::writeDimension(FAS_Dimension* d)
{
    QString blkName;
    if (noNameBlock.contains(d))
    {
        blkName = noNameBlock.take(d);
    }

    // version 12 are inserts of *D blocks
    if (version==1009)
    {
        if (!blkName.isEmpty())
        {
            DXF_Insert in;
            getEntityAttributes(&in, d);
            in.basePoint.x = in.basePoint.y = 0.0;
            in.basePoint.z = 0.0;
            in.name = blkName.toStdString();
            in.xscale = in.yscale = 1.0;
            in.zscale = 1.0;
            in.angle = 0.0;
            in.colcount = in.rowcount = 1;
            in.colspace = in.rowspace = 0.0;
            dxfWriter->writeInsert(&in);
        }
        return;
    }

    DXF_Dimension* dim;
    int attachmentPoint=1;
    if (d->getHAlign()==FAS_MTextData::HALeft)
    {
        attachmentPoint=1;
    }
    else if (d->getHAlign()==FAS_MTextData::HACenter)
    {
        attachmentPoint=2;
    }
    else if (d->getHAlign()==FAS_MTextData::HARight)
    {
        attachmentPoint=3;
    }
    if (d->getVAlign()==FAS_MTextData::VATop)
    {
        attachmentPoint+=0;
    }
    else if (d->getVAlign()==FAS_MTextData::VAMiddle) {
        attachmentPoint+=3;
    }
    else if (d->getVAlign()==FAS_MTextData::VABottom)
    {
        attachmentPoint+=6;
    }

    switch (d->rtti())
    {
    case FAS2::EntityDimAligned:
    {
        FAS_DimAligned* da = (FAS_DimAligned*)d;
        DXF_DimAligned * dd = new DXF_DimAligned();
        dim = dd ;
        dim->type = 1 +32;
        dd->setDef1Point(DXF_Coord (da->getExtensionPoint1().x, da->getExtensionPoint1().y, 0.0));
        dd->setDef2Point(DXF_Coord (da->getExtensionPoint2().x, da->getExtensionPoint2().y, 0.0));
        break;
    }
    case FAS2::EntityDimDiametric:
    {
        FAS_DimDiametric* dr = (FAS_DimDiametric*)d;
        DXF_DimDiametric * dd = new DXF_DimDiametric();
        dim = dd ;
        dim->type = 3+32;
        dd->setDiameter1Point(DXF_Coord (dr->getDefinitionPoint().x, dr->getDefinitionPoint().y, 0.0));
        dd->setLeaderLength(dr->getLeader());
        break;
    }
    case FAS2::EntityDimRadial:
    {
        FAS_DimRadial* dr = (FAS_DimRadial*)d;
        DXF_DimRadial * dd = new DXF_DimRadial();
        dim = dd ;
        dim->type = 4+32;
        dd->setDiameterPoint(DXF_Coord (dr->getDefinitionPoint().x, dr->getDefinitionPoint().y, 0.0));
        dd->setLeaderLength(dr->getLeader());
        break;
    }
    case FAS2::EntityDimAngular:
    {
        FAS_DimAngular* da = static_cast<FAS_DimAngular*>(d);
        if (da->getDefinitionPoint3() == da->getData().definitionPoint)
        {
            DXF_DimAngular3p * dd = new DXF_DimAngular3p();
            dim = dd ;
            dim->type = 5+32;
            dd->setFirstLine(DXF_Coord (da->getDefinitionPoint().x, da->getDefinitionPoint().y, 0.0)); //13
            dd->setSecondLine(DXF_Coord (da->getDefinitionPoint().x, da->getDefinitionPoint().y, 0.0)); //14
            dd->SetVertexPoint(DXF_Coord (da->getDefinitionPoint().x, da->getDefinitionPoint().y, 0.0)); //15
            dd->setDimPoint(DXF_Coord (da->getDefinitionPoint().x, da->getDefinitionPoint().y, 0.0)); //10
        }
        else
        {
            DXF_DimAngular * dd = new DXF_DimAngular();
            dim = dd ;
            dim->type = 2+32;
            dd->setFirstLine1(DXF_Coord (da->getDefinitionPoint1().x, da->getDefinitionPoint1().y, 0.0)); //13
            dd->setFirstLine2(DXF_Coord (da->getDefinitionPoint2().x, da->getDefinitionPoint2().y, 0.0)); //14
            dd->setSecondLine1(DXF_Coord (da->getDefinitionPoint3().x, da->getDefinitionPoint3().y, 0.0)); //15
            dd->setDimPoint(DXF_Coord (da->getDefinitionPoint4().x, da->getDefinitionPoint4().y, 0.0)); //16
        }
        break;
    }
    default:
    { //default to DimLinear
        FAS_DimLinear* dl = (FAS_DimLinear*)d;
        DXF_DimLinear * dd = new DXF_DimLinear();
        dim = dd ;
        dim->type = 0+32;
        dd->setDef1Point(DXF_Coord (dl->getExtensionPoint1().x, dl->getExtensionPoint1().y, 0.0));
        dd->setDef2Point(DXF_Coord (dl->getExtensionPoint2().x, dl->getExtensionPoint2().y, 0.0));
        dd->setAngle( FAS_Math::rad2deg(dl->getAngle()) );
        dd->setOblique(dl->getOblique());
        break;
    }
    }
    getEntityAttributes(dim, d);
    dim->setDefPoint(DXF_Coord(d->getDefinitionPoint().x, d->getDefinitionPoint().y, 0));
    dim->setTextPoint(DXF_Coord(d->getMiddleOfText().x, d->getMiddleOfText().y, 0));
    dim->setStyle (d->getStyle().toUtf8().data());
    dim->setAlign (attachmentPoint);
    dim->setTextLineStyle(d->getLineSpacingStyle());
    dim->setText (toDxfString(d->getText()).toUtf8().data());
    dim->setTextLineFactor(d->getLineSpacingFactor());
    if (!blkName.isEmpty())
    {
        dim->setName(blkName.toStdString());
    }

    dxfWriter->writeDimension(dim);
    delete dim;
}


// Writes the given leader entity to the file.
void FAS_DXFProcessor::writeLeader(FAS_Leader* l)
{
    DXF_Leader leader;
    getEntityAttributes(&leader, l);
    leader.style = "Standard";
    leader.arrow = l->hasArrowHead();
    leader.leadertype = 0;
    leader.flag = 3;
    leader.hookline = 0;
    leader.hookflag = 0;
    leader.textheight = 1;
    leader.textwidth = 10;
    leader.vertnum = l->count();
    FAS_Line* li =nullptr;
    for (FAS_Entity* v=l->firstEntity(FAS2::ResolveNone); v; v=l->nextEntity(FAS2::ResolveNone))
    {
        if (v->rtti()==FAS2::EntityLine)
        {
            li = (FAS_Line*)v;
            leader.vertexlist.push_back(std::make_shared<DXF_Coord>(li->getStartpoint().x, li->getStartpoint().y, 0.0));
        }
    }
    if (li )
        leader.vertexlist.push_back(std::make_shared<DXF_Coord>(li->getEndpoint().x, li->getEndpoint().y, 0.0));

    dxfWriter->writeLeader(&leader);
}

// Writes the given hatch entity to the file.
void FAS_DXFProcessor::writeHatch(FAS_Hatch * h)
{
    // version 12 are inserts of *U blocks
    if (version==1009)
    {
        if (noNameBlock.contains(h))
        {
            DXF_Insert in;
            getEntityAttributes(&in, h);
            in.basePoint.x = in.basePoint.y = 0.0;
            in.basePoint.z = 0.0;
            in.name = noNameBlock.value(h).toUtf8().data();
            in.xscale = in.yscale = 1.0;
            in.zscale = 1.0;
            in.angle = 0.0;
            in.colcount = in.rowcount = 1;
            in.colspace = in.rowspace = 0.0;
            dxfWriter->writeInsert(&in);
        }
        return;
    }

    bool writeIt = true;
    if (h->countLoops()>0)
    {
        // check if all of the loops contain entities:
        for (FAS_Entity* l=h->firstEntity(FAS2::ResolveNone); l; l=h->nextEntity(FAS2::ResolveNone))
        {
            if (l->isContainer() && !l->getFlag(FAS2::FlagTemp))
            {
                if (l->count()==0)
                {
                    writeIt = false;
                }
            }
        }
    }
    else
    {
        writeIt = false;
    }

    if (!writeIt)
    {
        return;
    }

    DXF_Hatch ha;
    getEntityAttributes(&ha, h);
    ha.solid = h->isSolid();
    ha.scale = h->getScale();
    ha.angle = h->getAngle();
    if (ha.solid)
        ha.name = "SOLID";
    else
        ha.name = h->getPattern().toUtf8().data();
    ha.loopsnum = h->countLoops();

    for (FAS_Entity* l=h->firstEntity(FAS2::ResolveNone); l; l=h->nextEntity(FAS2::ResolveNone))
    {
        // Write hatch loops:
        if (l->isContainer() && !l->getFlag(FAS2::FlagTemp))
        {
            FAS_EntityContainer* loop = (FAS_EntityContainer*)l;
            std::shared_ptr<DXF_HatchLoop> lData = std::make_shared<DXF_HatchLoop>(0);

            for (FAS_Entity* ed=loop->firstEntity(FAS2::ResolveNone); ed; ed=loop->nextEntity(FAS2::ResolveNone))
            {

                // Write hatch loop edges:
                if (ed->rtti()==FAS2::EntityLine)
                {
                    FAS_Line* ln = (FAS_Line*)ed;
                    std::shared_ptr<DXF_Line> line = std::make_shared<DXF_Line>();
                    line->basePoint.x = ln->getStartpoint().x;
                    line->basePoint.y = ln->getStartpoint().y;
                    line->secPoint.x = ln->getEndpoint().x;
                    line->secPoint.y = ln->getEndpoint().y;
                    lData->objlist.push_back(line);
                }
                else if (ed->rtti()==FAS2::EntityArc)
                {
                    FAS_Arc* ar = (FAS_Arc*)ed;
                    std::shared_ptr<DXF_Arc> arc = std::make_shared<DXF_Arc>();
                    arc->basePoint.x = ar->getCenter().x;
                    arc->basePoint.y = ar->getCenter().y;
                    arc->radious = ar->getRadius();
                    if (!ar->isReversed())
                    {
                        arc->staangle = ar->getAngle1();
                        arc->endangle = ar->getAngle2();
                        arc->isccw = true;
                    }
                    else
                    {
                        arc->staangle = 2*M_PI-ar->getAngle1();
                        arc->endangle = 2*M_PI-ar->getAngle2();
                        arc->isccw = false;
                    }
                    lData->objlist.push_back(arc);
                }
                else if (ed->rtti()==FAS2::EntityCircle)
                {
                    FAS_Circle* ci = (FAS_Circle*)ed;
                    std::shared_ptr<DXF_Arc> arc = std::make_shared<DXF_Arc>();
                    arc->basePoint.x = ci->getCenter().x;
                    arc->basePoint.y = ci->getCenter().y;
                    arc->radious = ci->getRadius();
                    arc->staangle = 0.0;
                    arc->endangle = 2*M_PI; //2*M_PI;
                    arc->isccw = true;
                    lData->objlist.push_back(arc);
                }
                else if (ed->rtti()==FAS2::EntityEllipse)
                {
                    FAS_Ellipse* el = (FAS_Ellipse*)ed;
                    std::shared_ptr<DXF_Ellipse> ell = std::make_shared<DXF_Ellipse>();
                    ell->basePoint.x = el->getCenter().x;
                    ell->basePoint.y = el->getCenter().y;
                    ell->secPoint.x = el->getMajorP().x;
                    ell->secPoint.y = el->getMajorP().y;
                    ell->ratio = el->getRatio();
                    double rot = el->getMajorP().angle();
                    double startAng = el->getCenter().angleTo(el->getStartpoint()) - rot;
                    double endAng = el->getCenter().angleTo(el->getEndpoint()) - rot;
                    if (startAng < 0) startAng = M_PI*2 + startAng;
                    if (endAng < 0) endAng = M_PI*2 + endAng;
                    ell->staparam = startAng;
                    ell->endparam = endAng;
                    ell->isccw = !el->isReversed();
                    lData->objlist.push_back(ell);
                }
            }
            lData->update(); //change to DXF_HatchLoop
            ha.appendLoop(lData);
        }
    }
    dxfWriter->writeHatch(&ha);
}

// Writes the given Solid entity to the file.
void FAS_DXFProcessor::writeSolid(FAS_Solid* s)
{
    FAS_SolidData data;
    DXF_Solid solid;
    FAS_Vector corner;
    getEntityAttributes(&solid, s);
    corner = s->getCorner(0);
    solid.basePoint.x = corner.x;
    solid.basePoint.y = corner.y;
    corner = s->getCorner(1);
    solid.secPoint.x = corner.x;
    solid.secPoint.y = corner.y;
    corner = s->getCorner(2);
    solid.thirdPoint.x = corner.x;
    solid.thirdPoint.y = corner.y;
    if (s->isTriangle())
    {
        solid.fourPoint.x = solid.thirdPoint.x;
        solid.fourPoint.y = solid.thirdPoint.y;
    }
    else
    {
        corner = s->getCorner(3);
        solid.fourPoint.x = corner.x;
        solid.fourPoint.y = corner.y;
    }
    dxfWriter->writeSolid(&solid);
}


void FAS_DXFProcessor::writeImage(FAS_Image * i)
{
    DXF_Image image;
    getEntityAttributes(&image, i);

    image.basePoint.x = i->getInsertionPoint().x;
    image.basePoint.y = i->getInsertionPoint().y;
    image.secPoint.x = i->getUVector().x;
    image.secPoint.y = i->getUVector().y;
    image.vVector.x = i->getVVector().x;
    image.vVector.y = i->getVVector().y;
    image.sizeu = i->getWidth();
    image.sizev = i->getHeight();
    image.brightness = i->getBrightness();
    image.contrast = i->getContrast();
    image.fade = i->getFade();

    DXF_ImageDef *imgDef = dxfWriter->writeImage(&image, i->getFile().toUtf8().data());
    if (imgDef )
    {
        imgDef->loaded = 1;
        imgDef->u = i->getData().size.x;
        imgDef->v = i->getData().size.y;
        imgDef->up = 1;
        imgDef->vp = 1;
        imgDef->resolution = 0;
    }
}

// Sets the entities attributes according to the attributes that come from a DXF file.
void FAS_DXFProcessor::setEntityAttributes(FAS_Entity* entity, const DXF_Entity* attrib)
{
    FAS_Pen pen;
    pen.setColor(Qt::black);
    pen.setLineType(FAS2::SolidLine);
    QString layName = toNativeString(QString::fromUtf8(attrib->layer.c_str()));

    // Layer: add layer in case it doesn't exist:
    if (!graphic->findLayer(layName))
    {
        DXF_Layer lay;
        lay.name = attrib->layer;
        addLayer(lay);
    }
    entity->setLayer(layName);

    // Color:
    if (attrib->color24 >= 0)
    {
        pen.setColor(FAS_Color(attrib->color24 >> 16,
                               attrib->color24 >> 8 & 0xFF,
                               attrib->color24 & 0xFF));
    }
    else if (attrib->color >= 0)
    {
        pen.setColor(numberToColor(attrib->color));
    }
    // Linetype:
    pen.setLineType(nameToLineType( QString::fromUtf8(attrib->lineType.c_str()) ));
    // Width:
    pen.setWidth(numberToWidth(attrib->lWeight));
    entity->setPen(pen);
}

// Gets the entities attributes as a DL_Attributes object.
void FAS_DXFProcessor::getEntityAttributes(DXF_Entity* ent, const FAS_Entity* entity)
{
    // Layer:
    FAS_Layer* layer = entity->getLayer();
    QString layerName;
    if (layer)
    {
        layerName = layer->getName();
    }
    else
    {
        layerName = "0";
    }

    FAS_Pen pen = entity->getPen(false);
    // Color:
    int exact_rgb;
    int color = colorToNumber(pen.getColor(), &exact_rgb);
    // Linetype:
    QString lineType = lineTypeToName(pen.getLineType());
    // Width:
    DXF_LW_Conv::lineWidth width = widthToNumber(pen.getWidth());
    ent->layer = toDxfString(layerName).toUtf8().data();
    ent->color = color;
    ent->color24 = exact_rgb;
    ent->lWeight = width;
    ent->lineType = lineType.toUtf8().data();
}

//return Pen with the same attributes as 'attrib'.
FAS_Pen FAS_DXFProcessor::attributesToPen(const DXF_Layer* att) const
{
    FAS_Color col;
    if (att->color24 >= 0)
        col = FAS_Color(att->color24 >> 16,
                        att->color24 >> 8 & 0xFF,
                        att->color24 & 0xFF);
    else if (att->color >= 0)
    {
        col = numberToColor(att->color);
    }

    FAS_Pen pen(col, numberToWidth(att->lWeight),
                nameToLineType(QString::fromUtf8(att->lineType.c_str())) );
    return pen;
}

//Converts a color index (num) into a FAS_Color object.
FAS_Color FAS_DXFProcessor::numberToColor(int num)
{
    if (num==0)
    {
        return FAS_Color(FAS2::FlagByBlock);
    }
    else if (num==256)
    {
        return FAS_Color(FAS2::FlagByLayer);
    }
    else if (num<=255 && num>=0)
    {
        return FAS_Color(DXF::dxfColors[num][0],
                DXF::dxfColors[num][1],
                DXF::dxfColors[num][2]);
    }
    else
    {
        return FAS_Color(FAS2::FlagByLayer);
    }
    return FAS_Color();
}

// The color that fits best is chosen.
int FAS_DXFProcessor::colorToNumber(const FAS_Color& col, int *rgb)
{
    *rgb = -1;
    // Special color BYBLOCK:
    if (col.getFlag(FAS2::FlagByBlock))
    {
        return 0;
    }
    // Special color BYLAYER
    else if (col.getFlag(FAS2::FlagByLayer))
    {
        return 256;
    }
    // Special color black is not in the table but white represents both
    // black and white
    else if (col.red()==0 && col.green()==0 && col.blue()==0)
    {
        return 7;
    }
    // All other colors
    else
    {
        int num=0;
        int diff = 255*3;  // smallest difference to a color in the table found so far

        // Run through the whole table and compare
        for (int i=1; i<=255; i++)
        {
            int d = abs(col.red()-DXF::dxfColors[i][0])
                    + abs(col.green()-DXF::dxfColors[i][1])
                    + abs(col.blue()-DXF::dxfColors[i][2]);

            if (d<diff)
            {
                diff = d;
                num = i;
                if (d==0)
                {
                    break;
                }
            }
        }
        if(diff != 0)
        {
            *rgb = 0;
            *rgb = col.red()<<16 | col.green()<<8 | col.blue();
        }
        return num;
    }
}

void FAS_DXFProcessor::add3dFace(const DXF_3Dface& data)
{
    FAS_PolylineData d(FAS_Vector(false),
                       FAS_Vector(false),
                       !data.invisibleflag);
    FAS_Polyline *polyline = new FAS_Polyline(currentContainer, d);
    setEntityAttributes(polyline, &data);
    FAS_Vector v1(data.basePoint.x, data.basePoint.y);
    FAS_Vector v2(data.secPoint.x, data.secPoint.y);
    FAS_Vector v3(data.thirdPoint.x, data.thirdPoint.y);
    FAS_Vector v4(data.fourPoint.x, data.fourPoint.y);

    polyline->addVertex(v1, 0.0);
    polyline->addVertex(v2, 0.0);
    polyline->addVertex(v3, 0.0);
    polyline->addVertex(v4, 0.0);

    currentContainer->addEntity(polyline);
}

void FAS_DXFProcessor::addComment(const char*)
{
}

// Converts a line type name (e.g. "CONTINUOUS") into a FAS2::LineType
FAS2::LineType FAS_DXFProcessor::nameToLineType(const QString& name) {

    QString uName = name.toUpper();

    // Standard linetypes for  AutoCAD
    if (uName.isEmpty() || uName=="BYLAYER") {
        return FAS2::LineByLayer;
    } else if (uName=="BYBLOCK") {
        return FAS2::LineByBlock;
    } else if (uName=="CONTINUOUS" || uName=="ACAD_ISO01W100") {
        return FAS2::SolidLine;
    } else if (uName=="ACAD_ISO07W100" || uName=="DOT") {
        return FAS2::DotLine;
    } else if (uName=="DOTTINY") {
        return FAS2::DotLineTiny;
    } else if (uName=="DOT2") {
        return FAS2::DotLine2;
    } else if (uName=="DOTX2") {
        return FAS2::DotLineX2;
    } else if (uName=="ACAD_ISO02W100" || uName=="ACAD_ISO03W100" ||
               uName=="DASHED" || uName=="HIDDEN") {
        return FAS2::DashLine;
    } else if (uName=="DASHEDTINY" || uName=="HIDDEN2") {
        return FAS2::DashLineTiny;
    } else if (uName=="DASHED2" || uName=="HIDDEN2") {
        return FAS2::DashLine2;
    } else if (uName=="DASHEDX2" || uName=="HIDDENX2") {
        return FAS2::DashLineX2;
    } else if (uName=="ACAD_ISO10W100" || uName=="DASHDOT") {
        return FAS2::DashDotLine;
    } else if (uName=="DASHDOTTINY") {
        return FAS2::DashDotLineTiny;
    } else if (uName=="DASHDOT2") {
        return FAS2::DashDotLine2;
    } else if (uName=="ACAD_ISO04W100" || uName=="DASHDOTX2") {
        return FAS2::DashDotLineX2;
    } else if (uName=="ACAD_ISO12W100" || uName=="DIVIDE") {
        return FAS2::DivideLine;
    } else if (uName=="DIVIDETINY") {
        return FAS2::DivideLineTiny;
    } else if (uName=="DIVIDE2") {
        return FAS2::DivideLine2;
    } else if (uName=="ACAD_ISO05W100" || uName=="DIVIDEX2") {
        return FAS2::DivideLineX2;
    } else if (uName=="CENTER") {
        return FAS2::CenterLine;
    } else if (uName=="CENTERTINY") {
        return FAS2::CenterLineTiny;
    } else if (uName=="CENTER2") {
        return FAS2::CenterLine2;
    } else if (uName=="CENTERX2") {
        return FAS2::CenterLineX2;
    } else if (uName=="BORDER") {
        return FAS2::BorderLine;
    } else if (uName=="BORDERTINY") {
        return FAS2::BorderLineTiny;
    } else if (uName=="BORDER2") {
        return FAS2::BorderLine2;
    } else if (uName=="BORDERX2") {
        return FAS2::BorderLineX2;
    }

    return FAS2::SolidLine;
}

// Converts a FAS_LineType into a name for a line type.
QString FAS_DXFProcessor::lineTypeToName(FAS2::LineType lineType)
{
    // Standard linetypes for QCad II / AutoCAD
    switch (lineType)
    {
    case FAS2::SolidLine:
        return "CONTINUOUS";
        break;
    case FAS2::DotLine:
        return "DOT";
        break;
    case FAS2::DotLineTiny:
        return "DOTTINY";
        break;
    case FAS2::DotLine2:
        return "DOT2";
        break;
    case FAS2::DotLineX2:
        return "DOTX2";
        break;
    case FAS2::DashLine:
        return "DASHED";
        break;
    case FAS2::DashLineTiny:
        return "DASHEDTINY";
        break;
    case FAS2::DashLine2:
        return "DASHED2";
        break;
    case FAS2::DashLineX2:
        return "DASHEDX2";
        break;

    case FAS2::DashDotLine:
        return "DASHDOT";
        break;
    case FAS2::DashDotLineTiny:
        return "DASHDOTTINY";
        break;
    case FAS2::DashDotLine2:
        return "DASHDOT2";
        break;
    case FAS2::DashDotLineX2:
        return "DASHDOTX2";
        break;
    case FAS2::DivideLine:
        return "DIVIDE";
        break;
    case FAS2::DivideLineTiny:
        return "DIVIDETINY";
        break;
    case FAS2::DivideLine2:
        return "DIVIDE2";
        break;
    case FAS2::DivideLineX2:
        return "DIVIDEX2";
        break;
    case FAS2::CenterLine:
        return "CENTER";
        break;
    case FAS2::CenterLineTiny:
        return "CENTERTINY";
        break;
    case FAS2::CenterLine2:
        return "CENTER2";
        break;
    case FAS2::CenterLineX2:
        return "CENTERX2";
        break;
    case FAS2::BorderLine:
        return "BORDER";
        break;
    case FAS2::BorderLineTiny:
        return "BORDERTINY";
        break;
    case FAS2::BorderLine2:
        return "BORDER2";
        break;
    case FAS2::BorderLineX2:
        return "BORDERX2";
        break;
    case FAS2::LineByLayer:
        return "ByLayer";
        break;
    case FAS2::LineByBlock:
        return "ByBlock";
        break;
    default:
        break;
    }
    return "CONTINUOUS";
}

// Converts a DXF_LW_Conv::lineWidth into a FAS2::LineWidth.
FAS2::LineWidth FAS_DXFProcessor::numberToWidth(DXF_LW_Conv::lineWidth lw)
{
    switch (lw) {
    case DXF_LW_Conv::widthByLayer:
        return FAS2::WidthByLayer;
        break;
    case DXF_LW_Conv::widthByBlock:
        return FAS2::WidthByBlock;
        break;
    case DXF_LW_Conv::widthDefault:
        return FAS2::WidthDefault;
        break;
    case DXF_LW_Conv::width00:
        return FAS2::Width00;
        break;
    case DXF_LW_Conv::width01:
        return FAS2::Width01;
        break;
    case DXF_LW_Conv::width02:
        return FAS2::Width02;
        break;
    case DXF_LW_Conv::width03:
        return FAS2::Width03;
        break;
    case DXF_LW_Conv::width04:
        return FAS2::Width04;
        break;
    case DXF_LW_Conv::width05:
        return FAS2::Width05;
        break;
    case DXF_LW_Conv::width06:
        return FAS2::Width06;
        break;
    case DXF_LW_Conv::width07:
        return FAS2::Width07;
        break;
    case DXF_LW_Conv::width08:
        return FAS2::Width08;
        break;
    case DXF_LW_Conv::width09:
        return FAS2::Width09;
        break;
    case DXF_LW_Conv::width10:
        return FAS2::Width10;
        break;
    case DXF_LW_Conv::width11:
        return FAS2::Width11;
        break;
    case DXF_LW_Conv::width12:
        return FAS2::Width12;
        break;
    case DXF_LW_Conv::width13:
        return FAS2::Width13;
        break;
    case DXF_LW_Conv::width14:
        return FAS2::Width14;
        break;
    case DXF_LW_Conv::width15:
        return FAS2::Width15;
        break;
    case DXF_LW_Conv::width16:
        return FAS2::Width16;
        break;
    case DXF_LW_Conv::width17:
        return FAS2::Width17;
        break;
    case DXF_LW_Conv::width18:
        return FAS2::Width18;
        break;
    case DXF_LW_Conv::width19:
        return FAS2::Width19;
        break;
    case DXF_LW_Conv::width20:
        return FAS2::Width20;
        break;
    case DXF_LW_Conv::width21:
        return FAS2::Width21;
        break;
    case DXF_LW_Conv::width22:
        return FAS2::Width22;
        break;
    case DXF_LW_Conv::width23:
        return FAS2::Width23;
        break;
    default:
        break;
    }
    return FAS2::WidthDefault;
}

// Converts a FAS2::LineWidth into an DXF_LW_Conv::lineWidth.
DXF_LW_Conv::lineWidth FAS_DXFProcessor::widthToNumber(FAS2::LineWidth width)
{
    switch (width)
    {
    case FAS2::WidthByLayer:
        return DXF_LW_Conv::widthByLayer;
        break;
    case FAS2::WidthByBlock:
        return DXF_LW_Conv::widthByBlock;
        break;
    case FAS2::WidthDefault:
        return DXF_LW_Conv::widthDefault;
        break;
    case FAS2::Width00:
        return DXF_LW_Conv::width00;
        break;
    case FAS2::Width01:
        return DXF_LW_Conv::width01;
        break;
    case FAS2::Width02:
        return DXF_LW_Conv::width02;
        break;
    case FAS2::Width03:
        return DXF_LW_Conv::width03;
        break;
    case FAS2::Width04:
        return DXF_LW_Conv::width04;
        break;
    case FAS2::Width05:
        return DXF_LW_Conv::width05;
        break;
    case FAS2::Width06:
        return DXF_LW_Conv::width06;
        break;
    case FAS2::Width07:
        return DXF_LW_Conv::width07;
        break;
    case FAS2::Width08:
        return DXF_LW_Conv::width08;
        break;
    case FAS2::Width09:
        return DXF_LW_Conv::width09;
        break;
    case FAS2::Width10:
        return DXF_LW_Conv::width10;
        break;
    case FAS2::Width11:
        return DXF_LW_Conv::width11;
        break;
    case FAS2::Width12:
        return DXF_LW_Conv::width12;
        break;
    case FAS2::Width13:
        return DXF_LW_Conv::width13;
        break;
    case FAS2::Width14:
        return DXF_LW_Conv::width14;
        break;
    case FAS2::Width15:
        return DXF_LW_Conv::width15;
        break;
    case FAS2::Width16:
        return DXF_LW_Conv::width16;
        break;
    case FAS2::Width17:
        return DXF_LW_Conv::width17;
        break;
    case FAS2::Width18:
        return DXF_LW_Conv::width18;
        break;
    case FAS2::Width19:
        return DXF_LW_Conv::width19;
        break;
    case FAS2::Width20:
        return DXF_LW_Conv::width20;
        break;
    case FAS2::Width21:
        return DXF_LW_Conv::width21;
        break;
    case FAS2::Width22:
        return DXF_LW_Conv::width22;
        break;
    case FAS2::Width23:
        return DXF_LW_Conv::width23;
        break;
    default:
        break;
    }
    return DXF_LW_Conv::widthDefault;
}


// Converts a native unicode string into a DXF encoded string.
QString FAS_DXFProcessor::toDxfString(const QString& str)
{
    QString res = "";
    int j=0;
    for (int i=0; i<str.length(); ++i)
    {
        int c = str.at(i).unicode();
        if (c>175 || c<11)
        {
            res.append(str.mid(j,i-j));
            j=i;

            switch (c)
            {
            case 0x0A:
                res+="\\P";
                break;
                // diameter:
            case 0x2205:// Empty_set, diameter is 0x2300 need to add in all fonts
            case 0x2300:
                res+="%%C";
                break;
                // degree:
            case 0x00B0:
                res+="%%D";
                break;
                // plus/minus
            case 0x00B1:
                res+="%%P";
                break;
            default:
                j--;
                break;
            }
            j++;
        }
    }
    res.append(str.mid(j));
    return res;
}

// Converts a DXF encoded string into a native Unicode string.
QString FAS_DXFProcessor::toNativeString(const QString& data)
{
    QString res;
    // Ignore font tags:
    int j = 0;
    int size=0;
    for (int i=0; i<data.length(); ++i) {
//        if (data.at(i).unicode() == 0x7B){ //is '{' ?
//            if (i+1 < data.length() && data.at(i+1).unicode() == 0x5c){ //and is "{\" ?
//                //check known codes
//                if ( i+2 < data.length() &&
//                        ((data.at(i+2).unicode() == 0x66) || //is "\f" ?
//                         (data.at(i+2).unicode() == 0x48) || //is "\H" ?
//                         (data.at(i+2).unicode() == 0x43) || //is "\C" ?
//                         (data.at(i+2).unicode() == 0x57))  //is "\W" ?
//                   ) {
//                    //found tag, append parsed part
//                    res.append(data.mid(j,i-j));
//                    int pos = data.indexOf(0x7D, i+3);//find '}'
//                    if (pos <0) break; //'}' not found
//                    QString tmp = data.mid(i+1, pos-i-1);
//                    do {
//                        tmp = tmp.remove(0,tmp.indexOf(0x3B, 0)+1 );//remove to ';'
//                    } while(tmp.startsWith("\\f") || tmp.startsWith("\\H") || tmp.startsWith("\\C") || tmp.startsWith("\\W"));
//                    res.append(tmp);
//                    i = j = pos;
//                    ++j;
//                }
//            }
//        }

//        if (data.at(i).unicode() == 0x7B){ //is '{' ?
            if (i < data.length() && data.at(i).unicode() == 0x5c){ //and is "{\" ?
                //check known codes
                if ( i+1 < data.length() &&
                        ((data.at(i+1).unicode() == 0x66) || //is "\f" ?
                         (data.at(i+1).unicode() == 0x48) || //is "\H" ?
                         (data.at(i+1).unicode() == 0x43) || //is "\C" ?
                         (data.at(i+1).unicode() == 0x57))  //is "\W" ?
                   ) {
                    //found tag, append parsed part
                    int pos = data.indexOf(0x3B, i);//find ';'
                    if (pos <0) break; //'}' not found
                    i = j = pos;
                    ++j;
                }else{
                    res.append(data.at(i));
                }
            }else{
                if (data.at(i).unicode() == 0x0A){
                    res.append("\n");
                }else if (data.at(i).unicode() == 0x7B||data.at(i).unicode() == 0x7D){

                }else{
                    if(size<50){
                    res.append(data.at(i));
                    }
                    size++;
                }
            }


//        }

    }


    // Line feed:
    res = res.replace(QRegExp("\\\\P"), "\r\n");
    // Space:
    res = res.replace(QRegExp("\\\\~"), " ");
    // Tab:
    res = res.replace(QRegExp("\\^I"), "    ");//RLZ: change 4 spaces for \t when mtext have support for tab
    // diameter:
    res = res.replace(QRegExp("%%[cC]"), QChar(0x2300));//s Empty_set is 0x2205, diameter is 0x2300 need to add in all fonts
    // degree:
    res = res.replace(QRegExp("%%[dD]"), QChar(0x00B0));
    // plus/minus
    res = res.replace(QRegExp("%%[pP]"), QChar(0x00B1));

    return res;
}

// Converts the given number from a DXF file into an AngleFormat enum.
FAS2::AngleFormat FAS_DXFProcessor::numberToAngleFormat(int num)
{
    FAS2::AngleFormat af;
    switch (num)
    {
    default:
    case 0:
        af = FAS2::DegreesDecimal;
        break;
    case 1:
        af = FAS2::DegreesMinutesSeconds;
        break;
    case 2:
        af = FAS2::Gradians;
        break;
    case 3:
        af = FAS2::Radians;
        break;
    case 4:
        af = FAS2::Surveyors;
        break;
    }
    return af;
}

// Converts AngleFormat enum to DXF number.
int FAS_DXFProcessor::angleFormatToNumber(FAS2::AngleFormat af)
{
    int num;
    switch (af)
    {
    default:
    case FAS2::DegreesDecimal:
        num = 0;
        break;
    case FAS2::DegreesMinutesSeconds:
        num = 1;
        break;
    case FAS2::Gradians:
        num = 2;
        break;
    case FAS2::Radians:
        num = 3;
        break;
    case FAS2::Surveyors:
        num = 4;
        break;
    }
    return num;
}

// converts a DXF unit setting (e.g. INSUNITS) to a unit enum.
FAS2::Unit FAS_DXFProcessor::numberToUnit(int num)
{
    switch (num) {
    default:
    case  0:
        return FAS2::None;
        break;
    case  1:
        return FAS2::Inch;
        break;
    case  2:
        return FAS2::Foot;
        break;
    case  3:
        return FAS2::Mile;
        break;
    case  4:
        return FAS2::Millimeter;
        break;
    case  5:
        return FAS2::Centimeter;
        break;
    case  6:
        return FAS2::Meter;
        break;
    case  7:
        return FAS2::Kilometer;
        break;
    case  8:
        return FAS2::Microinch;
        break;
    case  9:
        return FAS2::Mil;
        break;
    case 10:
        return FAS2::Yard;
        break;
    case 11:
        return FAS2::Angstrom;
        break;
    case 12:
        return FAS2::Nanometer;
        break;
    case 13:
        return FAS2::Micron;
        break;
    case 14:
        return FAS2::Decimeter;
        break;
    case 15:
        return FAS2::Decameter;
        break;
    case 16:
        return FAS2::Hectometer;
        break;
    case 17:
        return FAS2::Gigameter;
        break;
    case 18:
        return FAS2::Astro;
        break;
    case 19:
        return FAS2::Lightyear;
        break;
    case 20:
        return FAS2::Parsec;
        break;
    }
    return FAS2::None;
}

// Converst a unit enum into a DXF unit number e.g. for INSUNITS.
int FAS_DXFProcessor::unitToNumber(FAS2::Unit unit)
{
    switch (unit)
    {
    default:
    case FAS2::None:
        return  0;
        break;
    case FAS2::Inch:
        return  1;
        break;
    case FAS2::Foot:
        return  2;
        break;
    case FAS2::Mile:
        return  3;
        break;
    case FAS2::Millimeter:
        return  4;
        break;
    case FAS2::Centimeter:
        return  5;
        break;
    case FAS2::Meter:
        return  6;
        break;
    case FAS2::Kilometer:
        return  7;
        break;
    case FAS2::Microinch:
        return  8;
        break;
    case FAS2::Mil:
        return  9;
        break;
    case FAS2::Yard:
        return 10;
        break;
    case FAS2::Angstrom:
        return 11;
        break;
    case FAS2::Nanometer:
        return 12;
        break;
    case FAS2::Micron:
        return 13;
        break;
    case FAS2::Decimeter:
        return 14;
        break;
    case FAS2::Decameter:
        return 15;
        break;
    case FAS2::Hectometer:
        return 16;
        break;
    case FAS2::Gigameter:
        return 17;
        break;
    case FAS2::Astro:
        return 18;
        break;
    case FAS2::Lightyear:
        return 19;
        break;
    case FAS2::Parsec:
        return 20;
        break;
    }

    return 0;
}

// Checks if the given variable is two-dimensional (e.g. $LIMMIN).
bool FAS_DXFProcessor::isVariableTwoDimensional(const QString& var)
{
    if (var=="$LIMMIN" ||
            var=="$LIMMAX" ||
            var=="$PLIMMIN" ||
            var=="$PLIMMAX" ||
            var=="$GRIDUNIT" ||
            var=="$VIEWCTR")
    {
        return true;
    }
    else
    {
        return false;
    }
}
//myl
FAS_Insert* FAS_DXFProcessor::findInsert(FAS_EntityContainer* currentContainer,QString owner)
{
   for(int i=0;i<currentContainer->count();i++)
   {
       FAS_Entity *e=currentContainer->entityAt(i);
       if(e->rtti()==FAS2::EntityInsert)
       {
           FAS_Insert* insert=dynamic_cast<FAS_Insert*>(e);
           if(insert->data.insertHandleId==owner)
           {
               return insert;
           }
       }


   }
   return NULL;
}
// multi texts (MTEXT).
void FAS_DXFProcessor::addAttdef(const DXF_Attdef& data)
{

    FAS_Vector refPoint = FAS_Vector(data.basePoint.x, data.basePoint.y);;
    FAS_Vector secPoint = FAS_Vector(data.secPoint.x, data.secPoint.y);;
    double angle = data.angle;

    if (data.alignV !=0 || data.alignH !=0 ||data.alignH ==DXF_Text::HMiddle)
    {
        if (data.alignH !=DXF_Text::HAligned && data.alignH !=DXF_Text::HFit)
        {
            secPoint = FAS_Vector(data.basePoint.x, data.basePoint.y);
            refPoint = FAS_Vector(data.secPoint.x, data.secPoint.y);
        }
    }

    FAS_TextData::VAlign valign = (FAS_TextData::VAlign)data.alignV;
    FAS_TextData::HAlign halign = (FAS_TextData::HAlign)data.alignH;
    FAS_TextData::TextGeneration dir;
    QString sty = QString::fromUtf8(data.style.c_str());
    sty=sty.toLower();

    if (data.textgen==2)
    {
        dir = FAS_TextData::Backward;
    }
    else if (data.textgen==4)
    {
        dir = FAS_TextData::UpsideDown;
    }
    else
    {
        dir = FAS_TextData::None;
    }

    QString mtext = toNativeString(QString::fromUtf8(data.text.c_str()));
    // use default style for the drawing:
    if (sty.isEmpty())
    {
        // japanese, cyrillic:
        if (codePage=="ANSI_932" || codePage=="ANSI_1251")
        {
            sty = "Unicode";
        }
        else
        {
            sty = textStyle;
        }
    }
    else
    {
        sty = fontList.value(sty, sty);
    }

    FAS_TextData d(refPoint, secPoint, data.height, data.widthscale,
                   valign, halign, dir,
                   mtext, sty, angle*M_PI/180,
                   FAS2::NoUpdate);
    QString label=QString::fromStdString(data.label);
    FAS_AttdefData d1(d,label);
    FAS_ATTDEF* entity = new FAS_ATTDEF(currentContainer, d1);

    setEntityAttributes(entity, &data);
    entity->update();
    currentContainer->addEntity(entity);
}
// multi texts (MTEXT).
void FAS_DXFProcessor::addAttrib(const DXF_Attrib& data)
{

    FAS_Insert* insert=findInsert(currentContainer,QString::fromStdString(data.owner));
    if(insert!=NULL)
    {
        double height=data.height;
        insert->setAttibutes(data.attributeLabel,data.text,height,data.angle,data.alignH,data.alignV);
    }
}
// Writes the given mText entity to the file.
void FAS_DXFProcessor::writeAttdef(FAS_ATTDEF* t)
{
    DXF_Text text;
    getEntityAttributes(&text, t);
    text.basePoint.x = t->getInsertionPoint().x;
    text.basePoint.y = t->getInsertionPoint().y;
    text.height = t->getHeight();
    text.angle = t->getAngle()*180/M_PI;
    text.style = t->getStyle().toStdString();
    text.alignH =(DXF_Text::HAlign)t->getHAlign();
    text.alignV =(DXF_Text::VAlign)t->getVAlign();

    if (text.alignV != DXF_Text::VBaseLine || text.alignH != DXF_Text::HLeft)
    {
        if (text.alignH == DXF_Text::HAligned || text.alignH == DXF_Text::HFit)
        {
            text.secPoint.x = t->getSecondPoint().x;
            text.secPoint.y = t->getSecondPoint().y;
        }
        else
        {
            text.secPoint.x = t->getInsertionPoint().x;
            text.secPoint.y = t->getInsertionPoint().y;
        }
    }
    if (!t->getText().isEmpty())
    {
        text.text = toDxfString(t->getText()).toUtf8().data();
        dxfWriter->writeText(&text);
    }
}

// EOF

