/******************************************************************************
**  FASDXFLIB - Library to read/write DXF files (ascii & binary)              **
******************************************************************************/

#include <cstdlib>
#include "dxf_entities.h"
#include "dxfreader.h"

//! Calculate arbitrary axis
void DXF_Entity::calculateAxis(DXF_Coord extDirection){
    //Follow the arbitrary DXF definitions for extrusion axes.
    if (fabs(extDirection.x) < 0.015625 && fabs(extDirection.y) < 0.015625)
    {
        extAxisX.x = extDirection.z;
        extAxisX.y = 0;
        extAxisX.z = -extDirection.x;
    }
    else
    {
        //Otherwise, implement Ax = Wz x N where Wz is [0,0,1] per the DXF spec.
        extAxisX.x = -extDirection.y;
        extAxisX.y = extDirection.x;
        extAxisX.z = 0;
    }

    extAxisX.unitize();

    //Ay = N x Ax
    extAxisY.x = (extDirection.y * extAxisX.z) - (extAxisX.y * extDirection.z);
    extAxisY.y = (extDirection.z * extAxisX.x) - (extAxisX.z * extDirection.x);
    extAxisY.z = (extDirection.x * extAxisX.y) - (extAxisX.x * extDirection.y);

    extAxisY.unitize();
}

//! Extrude a point using arbitrary axis
void DXF_Entity::extrudePoint(DXF_Coord extDirection, DXF_Coord *point)
{
    double px, py, pz;
    px = (extAxisX.x*point->x)+(extAxisY.x*point->y)+(extDirection.x*point->z);
    py = (extAxisX.y*point->x)+(extAxisY.y*point->y)+(extDirection.y*point->z);
    pz = (extAxisX.z*point->x)+(extAxisY.z*point->y)+(extDirection.z*point->z);

    point->x = px;
    point->y = py;
    point->z = pz;
}

bool DXF_Entity::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 5:
        handle = reader->getHandleString();
        break;
    case 330:
        parentHandle = reader->getHandleString();
        break;
    case 8:
        layer = reader->getUtf8String();
        break;
    case 6:
        lineType = reader->getUtf8String();
        break;
    case 62:
        color = reader->getInt32();
        break;
    case 370:
        lWeight = DXF_LW_Conv::dxfInt2lineWidth(reader->getInt32());
        break;
    case 48:
        ltypeScale = reader->getDouble();
        break;
    case 60:
        visible = reader->getBool();
        break;
    case 420:
        color24 = reader->getInt32();
        break;
    case 430:
        colorName = reader->getString();
        break;
    case 67:
        space = static_cast<DXF::Space>(reader->getInt32());
        break;
    case 102:
        parseDxfGroups(code, reader);
        break;
    case 1000:
    case 1001:
    case 1002:
    case 1003:
    case 1004:
    case 1005:
        extData.push_back(std::make_shared<DXF_Variant>(code, reader->getString()));
        break;
    case 1010:
    case 1011:
    case 1012:
    case 1013:
        curr =std::make_shared<DXF_Variant>(code, DXF_Coord(reader->getDouble(), 0.0, 0.0));
        extData.push_back(curr);
        break;
    case 1020:
    case 1021:
    case 1022:
    case 1023:
        if (curr)
            curr->setCoordY(reader->getDouble());
        break;
    case 1030:
    case 1031:
    case 1032:
    case 1033:
        if (curr)
            curr->setCoordZ(reader->getDouble());
        break;
    case 1040:
    case 1041:
    case 1042:
        extData.push_back(std::make_shared<DXF_Variant>(code, reader->getDouble() ));
        break;
    case 1070:
    case 1071:
        extData.push_back(std::make_shared<DXF_Variant>(code, reader->getInt32() ));
        break;
    default:
        break;
    }
    return true;
}

//parses dxf 102 groups to read entity
bool DXF_Entity::parseDxfGroups(int code, dxfReader *reader)
{
    std::list<DXF_Variant> ls;
    DXF_Variant curr;
    int nc;
    std::string appName= reader->getString();
    if (!appName.empty() && appName.at(0)== '{')
    {
        curr.addString(code, appName.substr(1, (int) appName.size()-1));
        ls.push_back(curr);
        while (code !=102 && appName.at(0)== '}')
        {
            reader->readRec(&nc);
            if (code == 330 || code == 360)
                curr.addInt(code, reader->getHandleString());
            else
            {
                switch (reader->type)
                {
                case dxfReader::STRING:
                    curr.addString(code, reader->getString());
                    break;
                case dxfReader::INT32:
                case dxfReader::INT64:
                    curr.addInt(code, reader->getInt32());
                    break;
                case dxfReader::DOUBLE:
                    curr.addDouble(code, reader->getDouble());
                    break;
                case dxfReader::BOOL:
                    curr.addInt(code, reader->getInt32());
                    break;
                default:
                    break;
                }
            }
            ls.push_back(curr);
        }
    }

    appData.push_back(ls);
    return true;
}

void DXF_Point::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 10:
        basePoint.x = reader->getDouble();
        break;
    case 20:
        basePoint.y = reader->getDouble();
        break;
    case 30:
        basePoint.z = reader->getDouble();
        break;
    case 39:
        thickness = reader->getDouble();
        break;
    case 210:
        haveExtrusion = true;
        extDirection.x = reader->getDouble();
        break;
    case 220:
        extDirection.y = reader->getDouble();
        break;
    case 230:
        extDirection.z = reader->getDouble();
        break;
    default:
        DXF_Entity::parseCode(code, reader);
        break;
    }
}

void DXF_Line::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 11:
        secPoint.x = reader->getDouble();
        break;
    case 21:
        secPoint.y = reader->getDouble();
        break;
    case 31:
        secPoint.z = reader->getDouble();
        break;
    default:
        DXF_Point::parseCode(code, reader);
        break;
    }
}

void DXF_Circle::applyExtrusion()
{
    if (haveExtrusion)
    {
        //NOTE: Commenting these out causes the the arcs being tested to be located
        //on the other side of the y axis (all x dimensions are negated).
        calculateAxis(extDirection);
        extrudePoint(extDirection, &basePoint);
    }
}

void DXF_Circle::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 40:
        radious = reader->getDouble();
        break;
    default:
        DXF_Point::parseCode(code, reader);
        break;
    }
}

void DXF_Arc::applyExtrusion()
{
    DXF_Circle::applyExtrusion();
    if(haveExtrusion)
    {
        // If the extrusion vector has a z value less than 0, the angles for the arc
        // have to be mirrored since DXF files use the right hand rule.
        if (fabs(extDirection.x) < 0.015625 && fabs(extDirection.y) < 0.015625 && extDirection.z < 0.0)
        {
            staangle = M_PI-staangle;
            endangle = M_PI-endangle;

            double temp = staangle;
            staangle = endangle;
            endangle = temp;
        }
    }
}

void DXF_Arc::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 50:
        staangle = reader->getDouble()/ ARAD;
        break;
    case 51:
        endangle = reader->getDouble()/ ARAD;
        break;
    default:
        DXF_Circle::parseCode(code, reader);
        break;
    }
}

void DXF_Ellipse::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 40:
        ratio = reader->getDouble();
        break;
    case 41:
        staparam = reader->getDouble();
        break;
    case 42:
        endparam = reader->getDouble();
        break;
    default:
        DXF_Line::parseCode(code, reader);
        break;
    }
}

void DXF_Ellipse::applyExtrusion()
{
    if (haveExtrusion)
    {
        calculateAxis(extDirection);
        extrudePoint(extDirection, &secPoint);
        double intialparam = staparam;
        if (extDirection.z < 0.)
        {
            staparam = M_PIx2 - endparam;
            endparam = M_PIx2 - intialparam;
        }
    }
}

//if ratio > 1 minor axis are greather than major axis, correct it
void DXF_Ellipse::correctAxis()
{
    bool complete = false;
    if (staparam == endparam)
    {
        staparam = 0.0;
        endparam = M_PIx2; //2*M_PI;
        complete = true;
    }
    if (ratio > 1)
    {
        if ( fabs(endparam - staparam - M_PIx2) < 1.0e-10)
            complete = true;
        double incX = secPoint.x;
        secPoint.x = -(secPoint.y * ratio);
        secPoint.y = incX*ratio;
        ratio = 1/ratio;
        if (!complete)
        {
            if (staparam < M_PI_2)
                staparam += M_PI *2;
            if (endparam < M_PI_2)
                endparam += M_PI *2;
            endparam -= M_PI_2;
            staparam -= M_PI_2;
        }
    }
}

//parts are the number of vertex to split polyline, default 128
void DXF_Ellipse::toPolyline(DXF_Polyline *pol, int parts)
{
    double radMajor, radMinor, cosRot, sinRot, incAngle, curAngle;
    double cosCurr, sinCurr;
    radMajor = hypot(secPoint.x, secPoint.y);
    radMinor = radMajor*ratio;
    //calculate sin & cos of included angle
    incAngle = atan2(secPoint.y, secPoint.x);
    cosRot = cos(incAngle);
    sinRot = sin(incAngle);
    incAngle = M_PIx2 / parts;
    curAngle = staparam;
    int i = static_cast<int>(curAngle / incAngle);
    do {
        if (curAngle > endparam)
        {
            curAngle = endparam;
            i = parts+2;
        }
        cosCurr = cos(curAngle);
        sinCurr = sin(curAngle);
        double x = basePoint.x + (cosCurr*cosRot*radMajor) - (sinCurr*sinRot*radMinor);
        double y = basePoint.y + (cosCurr*sinRot*radMajor) + (sinCurr*cosRot*radMinor);
        pol->addVertex( DXF_Vertex(x, y, 0.0, 0.0));
        curAngle = (++i)*incAngle;
    } while (i<parts);
    if ( fabs(endparam - staparam - M_PIx2) < 1.0e-10)
    {
        pol->flags = 1;
    }
    pol->layer = this->layer;
    pol->lineType = this->lineType;
    pol->color = this->color;
    pol->lWeight = this->lWeight;
    pol->extDirection = this->extDirection;
}

void DXF_Trace::applyExtrusion()
{
    if (haveExtrusion)
    {
        calculateAxis(extDirection);
        extrudePoint(extDirection, &basePoint);
        extrudePoint(extDirection, &secPoint);
        extrudePoint(extDirection, &thirdPoint);
        extrudePoint(extDirection, &fourPoint);
    }
}

void DXF_Trace::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 12:
        thirdPoint.x = reader->getDouble();
        break;
    case 22:
        thirdPoint.y = reader->getDouble();
        break;
    case 32:
        thirdPoint.z = reader->getDouble();
        break;
    case 13:
        fourPoint.x = reader->getDouble();
        break;
    case 23:
        fourPoint.y = reader->getDouble();
        break;
    case 33:
        fourPoint.z = reader->getDouble();
        break;
    default:
        DXF_Line::parseCode(code, reader);
        break;
    }
}

void DXF_Solid::parseCode(int code, dxfReader *reader)
{
    DXF_Trace::parseCode(code, reader);
}

void DXF_3Dface::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 70:
        invisibleflag = reader->getInt32();
        break;
    default:
        DXF_Trace::parseCode(code, reader);
        break;
    }
}

void DXF_Block::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 2:
        name = reader->getUtf8String();
        break;
    case 70:
        flags = reader->getInt32();
        break;
    default:
        DXF_Point::parseCode(code, reader);
        break;
    }
}

void DXF_Insert::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 2:
        name = reader->getUtf8String();
        break;
    case 5:
        insertHandleId=reader->getUtf8String();//reader->getInt32();
        break;
    case 41:
        xscale = reader->getDouble();
        break;
    case 42:
        yscale = reader->getDouble();
        break;
    case 43:
        zscale = reader->getDouble();
        break;
    case 50:
        angle = reader->getDouble();
        angle = angle/ARAD; //convert to radian
        break;
    case 70:
        colcount = reader->getInt32();
        break;
    case 71:
        rowcount = reader->getInt32();
        break;
    case 44:
        colspace = reader->getDouble();
        break;
    case 45:
        rowspace = reader->getDouble();
        break;
    default:
        DXF_Point::parseCode(code, reader);
        break;
    }
}

void DXF_Insert::applyExtrusion()
{
    if(extDirection.z == -1)
    {
        basePoint.x *= -1;
        extDirection.z *= -1;
    }
}

void DXF_LWPolyline::applyExtrusion()
{
    if (haveExtrusion) {
        calculateAxis(extDirection);
        for (unsigned int i=0; i<vertlist.size(); i++)
        {
            auto& vert = vertlist.at(i);
            DXF_Coord v(vert->x, vert->y, elevation);
            extrudePoint(extDirection, &v);
            vert->x = v.x;
            vert->y = v.y;
        }
    }
}

void DXF_LWPolyline::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 10: {
        vertex = std::make_shared<DXF_Vertex2D>();
        vertlist.push_back(vertex);
        vertex->x = reader->getDouble();
        break; }
    case 20:
        if(vertex)
            vertex->y = reader->getDouble();
        break;
    case 40:
        if(vertex)
            vertex->stawidth = reader->getDouble();
        break;
    case 41:
        if(vertex)
            vertex->endwidth = reader->getDouble();
        break;
    case 42:
        if(vertex)
            vertex->bulge = reader->getDouble();
        break;
    case 38:
        elevation = reader->getDouble();
        break;
    case 39:
        thickness = reader->getDouble();
        break;
    case 43:
        width = reader->getDouble();
        break;
    case 70:
        flags = reader->getInt32();
        break;
    case 90:
        vertexnum = reader->getInt32();
        vertlist.reserve(vertexnum);
        break;
    case 210:
        haveExtrusion = true;
        extDirection.x = reader->getDouble();
        break;
    case 220:
        extDirection.y = reader->getDouble();
        break;
    case 230:
        extDirection.z = reader->getDouble();
        break;
    default:
        DXF_Entity::parseCode(code, reader);
        break;
    }
}

void DXF_Text::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 40:
        height = reader->getDouble();
        break;
    case 41:
        widthscale = reader->getDouble();
        break;
    case 50:
        angle = reader->getDouble();
        break;
    case 51:
        oblique = reader->getDouble();
        break;
    case 71:
        textgen = reader->getInt32();
        break;
    case 72:
        alignH = (HAlign)reader->getInt32();
        break;
    case 73:
        alignV = (VAlign)reader->getInt32();
        break;
    case 1:
        text = reader->getUtf8String();
        break;
    case 7:
        style = reader->getUtf8String();
        break;
    default:
        DXF_Line::parseCode(code, reader);
        break;
    }
}

void DXF_MText::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 1:
        text += reader->getString();
        text = reader->toUtf8String(text);
        break;
    case 11:
        haveXAxis = true;
        DXF_Text::parseCode(code, reader);
        break;
    case 3:
        text += reader->getString();
        break;
    case 44:
        interlin = reader->getDouble();
        break;
    default:
        DXF_Text::parseCode(code, reader);
        break;
    }
}

void DXF_MText::updateAngle()
{
    if (haveXAxis)
    {
        angle = atan2(secPoint.y, secPoint.x)*180/M_PI;
    }
}
void DXF_Attdef::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 40:
        height = reader->getDouble();
        break;
    case 41:
        widthscale = reader->getDouble();
        break;
    case 50:
        angle = reader->getDouble();
        break;
    case 51:
        oblique = reader->getDouble();
        break;
    case 71:
        textgen = reader->getInt32();
        break;
    case 72:
        alignH = (HAlign)reader->getInt32();
        break;
    case 73:
        alignV = (VAlign)reader->getInt32();
        break;
    case 1:
        text = reader->getUtf8String();
        break;
    case 7:
        style = reader->getUtf8String();
        break;
    case 2:
        label=reader->getUtf8String();
        break;
    default:
        DXF_Line::parseCode(code, reader);
        break;
    }
}

void DXF_Attrib::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 40:
        height = reader->getDouble();
        break;
    case 41:
        widthscale = reader->getDouble();
        break;
    case 50:
        angle = reader->getDouble();
        break;
    case 51:
        oblique = reader->getDouble();
        break;
    case 71:
        textgen = reader->getInt32();
        break;
    case 72:
        alignH = (HAlign)reader->getInt32();
        break;
    case 73:
        alignV = (VAlign)reader->getInt32();
        break;
    case 1:
        text = reader->getUtf8String();
        break;
    case 7:
        style = reader->getUtf8String();
        break;
    case 2:
        attributeLabel=reader->getUtf8String();
        break;
    case 330:
        owner=reader->getUtf8String();//myl
        break;
    default:
        DXF_Line::parseCode(code, reader);
        break;
    }
}

void DXF_Polyline::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 70:
        flags = reader->getInt32();
        break;
    case 40:
        defstawidth = reader->getDouble();
        break;
    case 41:
        defendwidth = reader->getDouble();
        break;
    case 71:
        vertexcount = reader->getInt32();
        break;
    case 72:
        facecount = reader->getInt32();
        break;
    case 73:
        smoothM = reader->getInt32();
        break;
    case 74:
        smoothN = reader->getInt32();
        break;
    case 75:
        curvetype = reader->getInt32();
        break;
    default:
        DXF_Point::parseCode(code, reader);
        break;
    }
}

void DXF_Vertex::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 70:
        flags = reader->getInt32();
        break;
    case 40:
        stawidth = reader->getDouble();
        break;
    case 41:
        endwidth = reader->getDouble();
        break;
    case 42:
        bulge = reader->getDouble();
        break;
    case 50:
        tgdir = reader->getDouble();
        break;
    case 71:
        vindex1 = reader->getInt32();
        break;
    case 72:
        vindex2 = reader->getInt32();
        break;
    case 73:
        vindex3 = reader->getInt32();
        break;
    case 74:
        vindex4 = reader->getInt32();
        break;
    case 91:
        identifier = reader->getInt32();
        break;
    default:
        DXF_Point::parseCode(code, reader);
        break;
    }
}

void DXF_Hatch::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 2:
        name = reader->getUtf8String();
        break;
    case 70:
        solid = reader->getInt32();
        break;
    case 71:
        associative = reader->getInt32();
        break;
    case 72:        /*edge type*/
        if (ispol){ //if is polyline is a as_bulge flag
            break;
        } else if (reader->getInt32() == 1){ //line
            addLine();
        } else if (reader->getInt32() == 2){ //arc
            addArc();
        } else if (reader->getInt32() == 3){ //elliptic arc
            addEllipse();
        } else if (reader->getInt32() == 4){ //spline
            addSpline();
        }
        break;
    case 10:
        if (pt) pt->basePoint.x = reader->getDouble();
        else if (pline) {
            plvert = pline->addVertex();
            plvert->x = reader->getDouble();
        }
        break;
    case 20:
        if (pt) pt->basePoint.y = reader->getDouble();
        else if (plvert) plvert ->y = reader->getDouble();
        break;
    case 11:
        if (line) line->secPoint.x = reader->getDouble();
        else if (ellipse) ellipse->secPoint.x = reader->getDouble();
        break;
    case 21:
        if (line) line->secPoint.y = reader->getDouble();
        else if (ellipse) ellipse->secPoint.y = reader->getDouble();
        break;
    case 40:
        if (arc) arc->radious = reader->getDouble();
        else if (ellipse) ellipse->ratio = reader->getDouble();
        break;
    case 41:
        scale = reader->getDouble();
        break;
    case 42:
        if (plvert) plvert ->bulge = reader->getDouble();
        break;
    case 50:
        if (arc) arc->staangle = reader->getDouble()/ARAD;
        else if (ellipse) ellipse->staparam = reader->getDouble()/ARAD;
        break;
    case 51:
        if (arc) arc->endangle = reader->getDouble()/ARAD;
        else if (ellipse) ellipse->endparam = reader->getDouble()/ARAD;
        break;
    case 52:
        angle = reader->getDouble();
        break;
    case 73:
        if (arc) arc->isccw = reader->getInt32();
        else if (pline) pline->flags = reader->getInt32();
        break;
    case 75:
        hstyle = reader->getInt32();
        break;
    case 76:
        hpattern = reader->getInt32();
        break;
    case 77:
        doubleflag = reader->getInt32();
        break;
    case 78:
        deflines = reader->getInt32();
        break;
    case 91:
        loopsnum = reader->getInt32();
        looplist.reserve(loopsnum);
        break;
    case 92:
        loop = std::make_shared<DXF_HatchLoop>(reader->getInt32());
        looplist.push_back(loop);
        if (reader->getInt32() & 2)
        {
            ispol = true;
            clearEntities();
            pline = std::make_shared<DXF_LWPolyline>();
            loop->objlist.push_back(pline);
        }
        else
            ispol = false;
        break;
    case 93:
        if (pline) pline->vertexnum = reader->getInt32();
        else loop->numedges = reader->getInt32();//aqui reserve
        break;
    case 98: //seed points ??
        clearEntities();
        break;
    default:
        DXF_Point::parseCode(code, reader);
        break;
    }
}

void DXF_Spline::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 210:
        normalVec.x = reader->getDouble();
        break;
    case 220:
        normalVec.y = reader->getDouble();
        break;
    case 230:
        normalVec.z = reader->getDouble();
        break;
    case 12:
        tgStart.x = reader->getDouble();
        break;
    case 22:
        tgStart.y = reader->getDouble();
        break;
    case 32:
        tgStart.z = reader->getDouble();
        break;
    case 13:
        tgEnd.x = reader->getDouble();
        break;
    case 23:
        tgEnd.y = reader->getDouble();
        break;
    case 33:
        tgEnd.z = reader->getDouble();
        break;
    case 70:
        flags = reader->getInt32();
        break;
    case 71:
        degree = reader->getInt32();
        break;
    case 72:
        nknots = reader->getInt32();
        break;
    case 73:
        ncontrol = reader->getInt32();
        break;
    case 74:
        nfit = reader->getInt32();
        break;
    case 42:
        tolknot = reader->getDouble();
        break;
    case 43:
        tolcontrol = reader->getDouble();
        break;
    case 44:
        tolfit = reader->getDouble();
        break;
    case 10: {
        controlpoint = std::make_shared<DXF_Coord>();
        controllist.push_back(controlpoint);
        controlpoint->x = reader->getDouble();
        break; }
    case 20:
        if(controlpoint)
            controlpoint->y = reader->getDouble();
        break;
    case 30:
        if(controlpoint)
            controlpoint->z = reader->getDouble();
        break;
    case 11: {
        fitpoint = std::make_shared<DXF_Coord>();
        fitlist.push_back(fitpoint);
        fitpoint->x = reader->getDouble();
        break; }
    case 21:
        if(fitpoint)
            fitpoint->y = reader->getDouble();
        break;
    case 31:
        if(fitpoint)
            fitpoint->z = reader->getDouble();
        break;
    case 40:
        knotslist.push_back(reader->getDouble());
        break;
    default:
        DXF_Entity::parseCode(code, reader);
        break;
    }
}

void DXF_Image::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 12:
        vVector.x = reader->getDouble();
        break;
    case 22:
        vVector.y = reader->getDouble();
        break;
    case 32:
        vVector.z = reader->getDouble();
        break;
    case 13:
        sizeu = reader->getDouble();
        break;
    case 23:
        sizev = reader->getDouble();
        break;
    case 340:
        ref = reader->getHandleString();
        break;
    case 280:
        clip = reader->getInt32();
        break;
    case 281:
        brightness = reader->getInt32();
        break;
    case 282:
        contrast = reader->getInt32();
        break;
    case 283:
        fade = reader->getInt32();
        break;
    default:
        DXF_Line::parseCode(code, reader);
        break;
    }
}

void DXF_Dimension::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 1:
        text = reader->getUtf8String();
        break;
    case 2:
        name = reader->getString();
        break;
    case 3:
        style = reader->getUtf8String();
        break;
    case 70:
        type = reader->getInt32();
        break;
    case 71:
        align = reader->getInt32();
        break;
    case 72:
        linesty = reader->getInt32();
        break;
    case 10:
        defPoint.x = reader->getDouble();
        break;
    case 20:
        defPoint.y = reader->getDouble();
        break;
    case 30:
        defPoint.z = reader->getDouble();
        break;
    case 11:
        textPoint.x = reader->getDouble();
        break;
    case 21:
        textPoint.y = reader->getDouble();
        break;
    case 31:
        textPoint.z = reader->getDouble();
        break;
    case 12:
        clonePoint.x = reader->getDouble();
        break;
    case 22:
        clonePoint.y = reader->getDouble();
        break;
    case 32:
        clonePoint.z = reader->getDouble();
        break;
    case 13:
        def1.x = reader->getDouble();
        break;
    case 23:
        def1.y = reader->getDouble();
        break;
    case 33:
        def1.z = reader->getDouble();
        break;
    case 14:
        def2.x = reader->getDouble();
        break;
    case 24:
        def2.y = reader->getDouble();
        break;
    case 34:
        def2.z = reader->getDouble();
        break;
    case 15:
        circlePoint.x = reader->getDouble();
        break;
    case 25:
        circlePoint.y = reader->getDouble();
        break;
    case 35:
        circlePoint.z = reader->getDouble();
        break;
    case 16:
        arcPoint.x = reader->getDouble();
        break;
    case 26:
        arcPoint.y = reader->getDouble();
        break;
    case 36:
        arcPoint.z = reader->getDouble();
        break;
    case 41:
        linefactor = reader->getDouble();
        break;
    case 53:
        rot = reader->getDouble();
        break;
    case 50:
        angle = reader->getDouble();
        break;
    case 52:
        oblique = reader->getDouble();
        break;
    case 40:
        length = reader->getDouble();
        break;
    case 51:
        hdir = reader->getDouble();
        break;
    default:
        DXF_Entity::parseCode(code, reader);
        break;
    }
}

void DXF_Leader::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 3:
        style = reader->getUtf8String();
        break;
    case 71:
        arrow = reader->getInt32();
        break;
    case 72:
        leadertype = reader->getInt32();
        break;
    case 73:
        flag = reader->getInt32();
        break;
    case 74:
        hookline = reader->getInt32();
        break;
    case 75:
        hookflag = reader->getInt32();
        break;
    case 76:
        vertnum = reader->getInt32();
        break;
    case 77:
        coloruse = reader->getInt32();
        break;
    case 40:
        textheight = reader->getDouble();
        break;
    case 41:
        textwidth = reader->getDouble();
        break;
    case 10:
        vertexpoint= std::make_shared<DXF_Coord>();
        vertexlist.push_back(vertexpoint);
        vertexpoint->x = reader->getDouble();
        break;
    case 20:
        if(vertexpoint)
            vertexpoint->y = reader->getDouble();
        break;
    case 30:
        if(vertexpoint)
            vertexpoint->z = reader->getDouble();
        break;
    case 340:
        annotHandle = reader->getHandleString();
        break;
    case 210:
        extrusionPoint.x = reader->getDouble();
        break;
    case 220:
        extrusionPoint.y = reader->getDouble();
        break;
    case 230:
        extrusionPoint.z = reader->getDouble();
        break;
    case 211:
        horizdir.x = reader->getDouble();
        break;
    case 221:
        horizdir.y = reader->getDouble();
        break;
    case 231:
        horizdir.z = reader->getDouble();
        break;
    case 212:
        offsetblock.x = reader->getDouble();
        break;
    case 222:
        offsetblock.y = reader->getDouble();
        break;
    case 232:
        offsetblock.z = reader->getDouble();
        break;
    case 213:
        offsettext.x = reader->getDouble();
        break;
    case 223:
        offsettext.y = reader->getDouble();
        break;
    case 233:
        offsettext.z = reader->getDouble();
        break;
    default:
        DXF_Entity::parseCode(code, reader);
        break;
    }
}

void DXF_Viewport::parseCode(int code, dxfReader *reader)
{
    switch (code)
    {
    case 40:
        pswidth = reader->getDouble();
        break;
    case 41:
        psheight = reader->getDouble();
        break;
    case 68:
        vpstatus = reader->getInt32();
        break;
    case 69:
        vpID = reader->getInt32();
        break;
    case 12: {
        centerPX = reader->getDouble();
        break; }
    case 22:
        centerPY = reader->getDouble();
        break;
    default:
        DXF_Point::parseCode(code, reader);
        break;
    }
}
