/******************************************************************************
**  FASDXFLIB - Library to read/write DXF files (ascii & binary)              **
******************************************************************************/

#ifndef DXF_ENTITIES_H
#define DXF_ENTITIES_H

#include <string>
#include <vector>
#include <list>
#include <memory>
#include "dxf_base.h"

class dxfReader;
class DXF_Polyline;

namespace DXF
{
//! Entity's type.
enum ETYPE
{
    E3DFACE,
    ARC,
    BLOCK,
    CIRCLE,
    DIMENSION,
    DIMALIGNED,
    DIMLINEAR,
    DIMRADIAL,
    DIMDIAMETRIC,
    DIMANGULAR,
    DIMANGULAR3P,
    DIMORDINATE,
    ELLIPSE,
    HATCH,
    IMAGE,
    INSERT,
    LEADER,
    LINE,
    LWPOLYLINE,
    MTEXT,
    POINT,
    POLYLINE,
    RAY,
    SOLID,
    SPLINE,
    TEXT,
    TRACE,
    UNDERLAY,
    VERTEX,
    VIEWPORT,
    XLINE,
    ATTDEF,
    ATTRIB,
    UNKNOWN
};

}

// Base class for entities
class DXF_Entity
{
public:
    //initializes default values
    DXF_Entity() {}
    virtual ~DXF_Entity(){}
    void reset()
    {
        extData.clear();
        curr.reset();
    }

    virtual void applyExtrusion() = 0;

    //parses dxf pair to read entity
    bool parseCode(int code, dxfReader *reader);
    //calculates extrusion axis (normal vector)
    void calculateAxis(DXF_Coord extDirection);
    //apply extrusion to @extDirection and return data in @point
    void extrudePoint(DXF_Coord extDirection, DXF_Coord *point);
    //parses dxf 102 groups to read entity
    bool parseDxfGroups(int code, dxfReader *reader);

public:
    enum DXF::ETYPE eType = DXF::UNKNOWN;     // enum: entity type, code 0 */
    duint32 handle = DXF::NoHandle;            // entity identifier, code 5 */
    std::list<std::list<DXF_Variant> > appData; // list of application data, code 102 */
    duint32 parentHandle = DXF::NoHandle;      // Soft-pointer ID/handle to owner BLOCK_RECORD object, code 330 */
    DXF::Space space = DXF::ModelSpace;          // space indicator, code 67*/
    UTF8STRING layer = "0";          // layer name, code 8 */
    UTF8STRING lineType = "BYLAYER";       // line type, code 6 */
    duint32 material = DXF::MaterialByLayer;          // hard pointer id to material object, code 347 */
    int color = DXF::ColorByLayer;                 // entity color, code 62 */
    enum DXF_LW_Conv::lineWidth lWeight = DXF_LW_Conv::widthByLayer; // entity lineweight, code 370 */
    double ltypeScale = 1.0;         // linetype scale, code 48 */
    bool visible = true;              // entity visibility, code 60 */
    int numProxyGraph = 0;         // Number of bytes in proxy graphics, code 92 */
    std::string proxyGraphics; // proxy graphics bytes, code 310 */
    int color24 = -1;               // 24-bit color, code 420 */
    std::string colorName;     // color name, code 430 */
    int transparency = DXF::Opaque;          // transparency, code 440 */
    int plotStyle = DXF::DefaultPlotStyle;             // hard pointer id to plot style object, code 390 */
    DXF::ShadowMode shadow = DXF::CastAndReceieveShadows;    // shadow mode, code 284 */
    bool haveExtrusion = false;        // set to true if the entity have extrusion*/
    std::vector<std::shared_ptr<DXF_Variant>> extData; // FIFO list of extended data, codes 1000 to 1071*/

private:
    void init(DXF_Entity const& rhs);
    DXF_Coord extAxisX;
    DXF_Coord extAxisY;
    std::shared_ptr<DXF_Variant> curr;
};

//Handle point entity
class DXF_Point : public DXF_Entity
{
public:
    DXF_Point()
    {
        eType = DXF::POINT;
        basePoint.x = basePoint.y = basePoint.z = extDirection.x = extDirection.y = 0;
        extDirection.z = 1;
        thickness = 0;
    }

    virtual void applyExtrusion(){}
    void parseCode(int code, dxfReader *reader);

public:
    DXF_Coord basePoint;      //  base point, code 10, 20 & 30 */
    double thickness;         // thickness, code 39 */
    DXF_Coord extDirection;       //  Dir extrusion normal vector, code 210, 220 & 230 */
};

//Handle line entity
class DXF_Line : public DXF_Point
{
public:
    DXF_Line()
    {
        eType = DXF::LINE;
        secPoint.z = 0;
    }

    void parseCode(int code, dxfReader *reader);

public:
    DXF_Coord secPoint;        // second point, code 11, 21 & 31 */
};

//Handle ray entity
class DXF_Ray : public DXF_Line
{
public:
    DXF_Ray()
    {
        eType = DXF::RAY;
    }
};

//Handle xline entity
class DXF_Xline : public DXF_Ray
{
public:
    DXF_Xline()
    {
        eType = DXF::XLINE;
    }
};

//Handle circle entity
class DXF_Circle : public DXF_Point
{
public:
    DXF_Circle()
    {
        eType = DXF::CIRCLE;
    }

    virtual void applyExtrusion();
    void parseCode(int code, dxfReader *reader);
public:
    double radious;                 // radius, code 40 */
};

//Handle arc entity
class DXF_Arc : public DXF_Circle
{
public:
    DXF_Arc()
    {
        eType = DXF::ARC;
        isccw = 1;
    }

    virtual void applyExtrusion();

    //! center point in OCS
    const DXF_Coord & center() { return basePoint; }
    //! the radius of the circle
    double radius() { return radious; }
    //! start angle in radians
    double startAngle() { return staangle; }
    //! end angle in radians
    double endAngle() { return endangle; }
    //! thickness
    double thick() { return thickness; }
    //! extrusion
    const DXF_Coord & extrusion() { return extDirection; }
    //! interpret code in dxf reading process or dispatch to inherited class
    void parseCode(int code, dxfReader *reader);
public:
    double staangle;            // start angle, code 50 in radians*/
    double endangle;            // end angle, code 51 in radians */
    int isccw;                  // is counter clockwise arc?, only used in hatch, code 73 */
};

//Handle ellipse entity
class DXF_Ellipse : public DXF_Line
{
public:
    DXF_Ellipse()
    {
        eType = DXF::ELLIPSE;
        isccw = 1;
    }

    void toPolyline(DXF_Polyline *pol, int parts = 128);
    virtual void applyExtrusion();

    //! interpret code in dxf reading process or dispatch to inherited class
    void parseCode(int code, dxfReader *reader);
    void correctAxis();

public:
    double ratio;        // ratio, code 40 */
    double staparam;     // start parameter, code 41, 0.0 for full ellipse*/
    double endparam;     // end parameter, code 42, 2*PI for full ellipse */
    int isccw;           // is counter clockwise arc?, only used in hatch, code 73 */
};

//Handle trace entity
class DXF_Trace : public DXF_Line
{
public:
    DXF_Trace()
    {
        eType = DXF::TRACE;
        thirdPoint.z = 0;
        fourPoint.z = 0;
    }

    virtual void applyExtrusion();
    void parseCode(int code, dxfReader *reader);

public:
    DXF_Coord thirdPoint;        // third point, code 12, 22 & 32 */
    DXF_Coord fourPoint;        // four point, code 13, 23 & 33 */
};

//Handle solid entity
class DXF_Solid : public DXF_Trace
{
public:
    DXF_Solid()
    {
        eType = DXF::SOLID;
    }

    //! interpret code in dxf reading process or dispatch to inherited class
    void parseCode(int code, dxfReader *reader);
public:
    //! first corner (2D)
    const DXF_Coord & firstCorner() { return basePoint; }
    //! second corner (2D)
    const DXF_Coord & secondCorner() { return secPoint; }
    //! third corner (2D)
    const DXF_Coord & thirdCorner() { return thirdPoint; }
    //! fourth corner (2D)
    const DXF_Coord & fourthCorner() { return thirdPoint; }
    //! thickness
    double thick() { return thickness; }
    //! elevation
    double elevation() { return basePoint.z; }
    //! extrusion
    const DXF_Coord & extrusion() { return extDirection; }

};

//Handle 3dface entity
class DXF_3Dface : public DXF_Trace
{
public:
    enum InvisibleEdgeFlags
    {
        NoEdge = 0x00,
        FirstEdge = 0x01,
        SecodEdge = 0x02,
        ThirdEdge = 0x04,
        FourthEdge = 0x08,
        AllEdges = 0x0F
    };

    DXF_3Dface()
    {
        eType = DXF::E3DFACE;
        invisibleflag = 0;
    }

    virtual void applyExtrusion(){}

    //! first corner in WCS
    const DXF_Coord & firstCorner() { return basePoint; }
    //! second corner in WCS
    const DXF_Coord & secondCorner() { return secPoint; }
    //! third corner in WCS
    const DXF_Coord & thirdCorner() { return thirdPoint; }
    //! fourth corner in WCS
    const DXF_Coord & fourthCorner() { return fourPoint; }
    //! edge visibility flags
    InvisibleEdgeFlags edgeFlags() { return (InvisibleEdgeFlags)invisibleflag; }

    //! interpret code in dxf reading process or dispatch to inherited class
    void parseCode(int code, dxfReader *reader);
public:
    int invisibleflag;       // invisible edge flag, code 70 */

};

//Handle block entries
class DXF_Block : public DXF_Point
{
public:
    DXF_Block()
    {
        eType = DXF::BLOCK;
        layer = "0";
        flags = 0;
        name = "*U0";
    }

    virtual void applyExtrusion(){}
    void parseCode(int code, dxfReader *reader);

public:
    UTF8STRING name;             // block name, code 2 */
    int flags;                   // block type, code 70 */

};


//Handle insert entries
class DXF_Insert : public DXF_Point
{
public:
    DXF_Insert()
    {
        eType = DXF::INSERT;
        xscale = 1;
        yscale = 1;
        zscale = 1;
        angle = 0;
        colcount = 1;
        rowcount = 1;
        colspace = 0;
        rowspace = 0;
    }

    virtual void applyExtrusion();
    void parseCode(int code, dxfReader *reader);

public:
    UTF8STRING name;         // block name, code 2 */
    double xscale;           // x scale factor, code 41 */
    double yscale;           // y scale factor, code 42 */
    double zscale;           // z scale factor, code 43 */
    double angle;            // rotation angle in radians, code 50 */
    int colcount;            // column count, code 70 */
    int rowcount;            // row count, code 71 */
    double colspace;         // column space, code 44 */
    double rowspace;         // row space, code 45 */
    std::string insertHandleId;//custom add
};

//Handle lwpolyline entity
class DXF_LWPolyline : public DXF_Entity
{
public:
    DXF_LWPolyline()
    {
        eType = DXF::LWPOLYLINE;
        elevation = thickness = width = 0.0;
        flags = 0;
        extDirection.x = extDirection.y = 0;
        extDirection.z = 1;
    }
    
    DXF_LWPolyline(const DXF_LWPolyline& p):DXF_Entity(p)
    {
        this->eType = DXF::LWPOLYLINE;
        this->elevation = p.elevation;
        this->thickness = p.thickness;
        this->width = p.width;
        this->flags = p.flags;
        this->extDirection = p.extDirection;
        for (unsigned i=0; i<p.vertlist.size(); i++)
            this->vertlist.push_back(
                    std::make_shared<DXF_Vertex2D>(*p.vertlist.at(i))
                    );
    }
    // TODO rule of 5

    virtual void applyExtrusion();
    void addVertex (DXF_Vertex2D v)
    {
        std::shared_ptr<DXF_Vertex2D> vert = std::make_shared<DXF_Vertex2D>(v);
        vertlist.push_back(vert);
    }
    std::shared_ptr<DXF_Vertex2D> addVertex ()
    {
        std::shared_ptr<DXF_Vertex2D> vert = std::make_shared<DXF_Vertex2D>();
        vert->stawidth = 0;
        vert->endwidth = 0;
        vert->bulge = 0;
        vertlist.push_back(vert);
        return vert;
    }
    void parseCode(int code, dxfReader *reader);

public:
    int vertexnum;            // number of vertex, code 90 */
    int flags;                // polyline flag, code 70, default 0 */
    double width;             // constant width, code 43 */
    double elevation;         // elevation, code 38 */
    double thickness;         // thickness, code 39 */
    DXF_Coord extDirection;       //  Dir extrusion normal vector, code 210, 220 & 230 */
    std::shared_ptr<DXF_Vertex2D> vertex;       // current vertex to add data */
    std::vector<std::shared_ptr<DXF_Vertex2D>> vertlist;  // vertex list */
};

//Handle insert entries
class DXF_Text : public DXF_Line
{
public:
#if 1//MYL
    enum VAlign
    {
        VBottom,        // Bottom = 0 */
        VMiddle,        // Middle = 1 */
        VTop,            // Top = 2 */
        VBaseLine  // Top = 3 */
    };
    //! Horizontal alignments.
    enum HAlign
    {
        HLeft = 0,     // Left = 0 */
        HCenter,       // Centered = 1 */
        HRight,        // Right = 2 */
        HAligned,      // Aligned = 3 (if VAlign==0) */
        HMiddle,       // middle = 4 (if VAlign==0) */
        HFit           // fit into point = 5 (if VAlign==0) */
    };

    DXF_Text()
    {
        eType = DXF::TEXT;
        angle = 0;
        widthscale = 1;
        oblique = 0;
        style = "STANDARD";
        textgen = 0;
        alignH = HLeft;
        alignV = VBottom;
    }

    #else
    // Vertical alignments.
    enum VAlign
    {
        VBaseLine = 0,  // Top = 0 */
        VBottom,        // Bottom = 1 */
        VMiddle,        // Middle = 2 */
        VTop            // Top = 3 */
    };

    //! Horizontal alignments.
    enum HAlign
    {
        HLeft = 0,     // Left = 0 */
        HCenter,       // Centered = 1 */
        HRight,        // Right = 2 */
        HAligned,      // Aligned = 3 (if VAlign==0) */
        HMiddle,       // middle = 4 (if VAlign==0) */
        HFit           // fit into point = 5 (if VAlign==0) */
    };

    DXF_Text()
    {
        eType = DXF::TEXT;
        angle = 0;
        widthscale = 1;
        oblique = 0;
        style = "STANDARD";
        textgen = 0;
        alignH = HLeft;
        alignV = VBaseLine;
    }
    #endif
    virtual void applyExtrusion(){}
    void parseCode(int code, dxfReader *reader);

public:
    double height;             // height text, code 40 */
    UTF8STRING text;           // text string, code 1 */
    double angle;              // rotation angle in degrees (360), code 50 */
    double widthscale;         // width factor, code 41 */
    double oblique;            // oblique angle, code 51 */
    UTF8STRING style;          // style name, code 7 */
    int textgen;               // text generation, code 71 */
    enum HAlign alignH;        // horizontal align, code 72 */
    enum VAlign alignV;        // vertical align, code 73 */


};

//Handle insert entries
class DXF_MText : public DXF_Text
{
public:
    //! Attachments.
    enum Attach
    {
        TopLeft = 1,
        TopCenter,
        TopRight,
        MiddleLeft,
        MiddleCenter,
        MiddleRight,
        BottomLeft,
        BottomCenter,
        BottomRight
    };

    DXF_MText()
    {
        eType = DXF::MTEXT;
        interlin = 1;
        alignV = (VAlign)TopLeft;
        textgen = 1;
        haveXAxis = false;    //if true needed to recalculate angle
    }

    void parseCode(int code, dxfReader *reader);
    void updateAngle();    //recalculate angle if 'haveXAxis' is true

public:
    double interlin;     // width factor, code 44 */
private:
    bool haveXAxis;
};
//Handle insert entries
class DXF_Attdef : public DXF_Text
{
public:
    DXF_Attdef()
    {
        eType = DXF::TEXT;
        angle = 0;
        widthscale = 1;
        oblique = 0;
        style = "STANDARD";
        textgen = 0;
        alignH = HLeft;
        alignV = VBaseLine; //if true needed to recalculate angle
        label="";//label myl custom code 2
    }
    virtual void applyExtrusion(){}
    void parseCode(int code, dxfReader *reader);
public:
    UTF8STRING label;//label myl custom code 2


};
//Handle insert entries
class DXF_Attrib : public DXF_Text
{
public:
    DXF_Attrib()
    {
        eType = DXF::TEXT;
        angle = 0;
        widthscale = 1;
        oblique = 0;
        style = "STANDARD";
        textgen = 0;
        alignH = HLeft;
        alignV = VBaseLine; //if true needed to recalculate angle
        owner="";
        attributeLabel="";
    }
    virtual void applyExtrusion(){}
    void parseCode(int code, dxfReader *reader);
public:
    UTF8STRING owner;//myl custom
    UTF8STRING attributeLabel;//myl custom


};

//Handle vertex
class DXF_Vertex : public DXF_Point
{
public:
    DXF_Vertex()
    {
        eType = DXF::VERTEX;
        stawidth = endwidth = bulge = 0;
        vindex1 = vindex2 = vindex3 = vindex4 = 0;
        flags = identifier = 0;
    }
    DXF_Vertex(double sx, double sy, double sz, double b)
    {
        stawidth = endwidth = 0;
        vindex1 = vindex2 = vindex3 = vindex4 = 0;
        flags = identifier = 0;
        basePoint.x = sx;
        basePoint.y =sy;
        basePoint.z =sz;
        bulge = b;
    }
    void parseCode(int code, dxfReader *reader);

public:
    double stawidth;          // Start width, code 40 */
    double endwidth;          // End width, code 41 */
    double bulge;             // bulge, code 42 */

    int flags;                 // vertex flag, code 70, default 0 */
    double tgdir;           // curve fit tangent direction, code 50 */
    int vindex1;             // polyface mesh vertex index, code 71, default 0 */
    int vindex2;             // polyface mesh vertex index, code 72, default 0 */
    int vindex3;             // polyface mesh vertex index, code 73, default 0 */
    int vindex4;             // polyface mesh vertex index, code 74, default 0 */
    int identifier;           // vertex identifier, code 91, default 0 */
};

//Handle polyline entity
class DXF_Polyline : public DXF_Point
{
public:
    DXF_Polyline()
    {
        eType = DXF::POLYLINE;
        defstawidth = defendwidth = 0.0;
        basePoint.x = basePoint.y = 0.0;
        flags = vertexcount = facecount = 0;
        smoothM = smoothN = curvetype = 0;
    }
    void addVertex (DXF_Vertex v)
    {
        std::shared_ptr<DXF_Vertex> vert = std::make_shared<DXF_Vertex>();
        vert->basePoint.x = v.basePoint.x;
        vert->basePoint.y = v.basePoint.y;
        vert->basePoint.z = v.basePoint.z;
        vert->stawidth = v.stawidth;
        vert->endwidth = v.endwidth;
        vert->bulge = v.bulge;
        vertlist.push_back(vert);
    }
    void appendVertex (std::shared_ptr<DXF_Vertex> const& v)
    {
        vertlist.push_back(v);
    }
    void parseCode(int code, dxfReader *reader);

public:
    int flags;               // polyline flag, code 70, default 0 */
    double defstawidth;      // Start width, code 40, default 0 */
    double defendwidth;      // End width, code 41, default 0 */
    int vertexcount;         // polygon mesh M vertex or  polyface vertex num, code 71, default 0 */
    int facecount;           // polygon mesh N vertex or  polyface face num, code 72, default 0 */
    int smoothM;             // smooth surface M density, code 73, default 0 */
    int smoothN;             // smooth surface M density, code 74, default 0 */
    int curvetype;           // curves & smooth surface type, code 75, default 0 */

    std::vector<std::shared_ptr<DXF_Vertex>> vertlist;  // vertex list */

private:
    std::list<duint32>hadlesList; //list of handles, only in 2004+
    duint32 firstEH;      //handle of first entity, only in pre-2004
    duint32 lastEH;       //handle of last entity, only in pre-2004
};


//Handle spline entity
class DXF_Spline : public DXF_Entity
{
public:
    DXF_Spline()
    {
        eType = DXF::SPLINE;
        flags = nknots = ncontrol = nfit = 0;
        tolknot = tolcontrol = tolfit = 0.0000001;

    }
    virtual void applyExtrusion(){}
    void parseCode(int code, dxfReader *reader);

public:
    DXF_Coord normalVec;      // normal vector, code 210, 220, 230 */
    DXF_Coord tgStart;        // start tangent, code 12, 22, 32 */
    DXF_Coord tgEnd;          // end tangent, code 13, 23, 33 */
    int flags;                // spline flag, code 70 */
    int degree;               // degree of the spline, code 71 */
    dint32 nknots;            // number of knots, code 72, default 0 */
    dint32 ncontrol;          // number of control points, code 73, default 0 */
    dint32 nfit;              // number of fit points, code 74, default 0 */
    double tolknot;           // knot tolerance, code 42, default 0.0000001 */
    double tolcontrol;        // control point tolerance, code 43, default 0.0000001 */
    double tolfit;            // fit point tolerance, code 44, default 0.0000001 */

    std::vector<double> knotslist;           // knots list, code 40 */
    std::vector<std::shared_ptr<DXF_Coord>> controllist;  // control points list, code 10, 20 & 30 */
    std::vector<std::shared_ptr<DXF_Coord>> fitlist;      // fit points list, code 11, 21 & 31 */

private:
    std::shared_ptr<DXF_Coord> controlpoint;   // current control point to add data */
    std::shared_ptr<DXF_Coord> fitpoint;       // current fit point to add data */
};

//Handle hatch loop
class DXF_HatchLoop
{
public:
    DXF_HatchLoop(int t)
    {
        type = t;
        numedges = 0;
    }

    void update()
    {
        numedges = objlist.size();
    }

public:
    int type;               // boundary path type, code 92, polyline=2, default=0 */
    int numedges;           // number of edges (if not a polyline), code 93 */
    std::vector<std::shared_ptr<DXF_Entity>> objlist;      // entities list */
};

//Handle hatch entity
class DXF_Hatch : public DXF_Point
{
public:
    DXF_Hatch()
    {
        eType = DXF::HATCH;
        angle = scale = 0.0;
        basePoint.x = basePoint.y = basePoint.z = 0.0;
        loopsnum = hstyle = associative = 0;
        solid = hpattern = 1;
        deflines = doubleflag = 0;
        clearEntities();
    }

    void appendLoop (std::shared_ptr<DXF_HatchLoop> const& v)
    {
        looplist.push_back(v);
    }

    virtual void applyExtrusion(){}
    void parseCode(int code, dxfReader *reader);

public:
    UTF8STRING name;           // hatch pattern name, code 2 */
    int solid;                 // solid fill flag, code 70, solid=1, pattern=0 */
    int associative;           // associativity, code 71, associatve=1, non-assoc.=0 */
    int hstyle;                // hatch style, code 75 */
    int hpattern;              // hatch pattern type, code 76 */
    int doubleflag;            // hatch pattern double flag, code 77, double=1, single=0 */
    int loopsnum;              // namber of boundary paths (loops), code 91 */
    double angle;              // hatch pattern angle, code 52 */
    double scale;              // hatch pattern scale, code 41 */
    int deflines;              // number of pattern definition lines, code 78 */

    std::vector<std::shared_ptr<DXF_HatchLoop>> looplist;  // polyline list */

private:
    void clearEntities()
    {
        pt.reset();
        line.reset();
        pline.reset();
        arc.reset();
        ellipse.reset();
        spline.reset();
        plvert.reset();
    }

    void addLine()
    {
        clearEntities();
        if (loop)
        {
            pt = line = std::make_shared<DXF_Line>();
            loop->objlist.push_back(line);
        }
    }

    void addArc()
    {
        clearEntities();
        if (loop)
        {
            pt = arc = std::make_shared<DXF_Arc>();
            loop->objlist.push_back(arc);
        }
    }

    void addEllipse()
    {
        clearEntities();
        if (loop) {
            pt = ellipse = std::make_shared<DXF_Ellipse>();
            loop->objlist.push_back(ellipse);
        }
    }

    void addSpline()
    {
        clearEntities();
        if (loop)
        {
            pt.reset();
            spline = std::make_shared<DXF_Spline>();
            loop->objlist.push_back(spline);
        }
    }

    std::shared_ptr<DXF_HatchLoop> loop;       // current loop to add data */
    std::shared_ptr<DXF_Line> line;
    std::shared_ptr<DXF_Arc> arc;
    std::shared_ptr<DXF_Ellipse> ellipse;
    std::shared_ptr<DXF_Spline> spline;
    std::shared_ptr<DXF_LWPolyline> pline;
    std::shared_ptr<DXF_Point> pt;
    std::shared_ptr<DXF_Vertex2D> plvert;
    bool ispol;
};

//Handle image entity
class DXF_Image : public DXF_Line
{
public:
    DXF_Image()
    {
        eType = DXF::IMAGE;
        fade = clip = 0;
        brightness = contrast = 50;
    }

    void parseCode(int code, dxfReader *reader);

public:
    duint32 ref;               // Hard reference to imagedef object, code 340 */
    DXF_Coord vVector;         // V-vector of single pixel, x coordinate, code 12, 22 & 32 */
    double sizeu;              // image size in pixels, U value, code 13 */
    double sizev;              // image size in pixels, V value, code 23 */
    double dz;                 // z coordinate, code 33 */
    int clip;                  // Clipping state, code 280, 0=off 1=on */
    int brightness;            // Brightness value, code 281, (0-100) default 50 */
    int contrast;              // Brightness value, code 282, (0-100) default 50 */
    int fade;                  // Brightness value, code 283, (0-100) default 0 */

};


//! Base class for dimension entity
class DXF_Dimension : public DXF_Entity
{
public:
    DXF_Dimension()
    {
        eType = DXF::DIMENSION;
        type = 0;
        linesty = 1;
        linefactor = extDirection.z = 1.0;
        angle = oblique = rot = 0.0;
        align = 5;
        style = "STANDARD";
        defPoint.z = extDirection.x = extDirection.y = 0;
        textPoint.z = rot = 0;
        clonePoint.x = clonePoint.y = clonePoint.z = 0;
    }

    DXF_Dimension(const DXF_Dimension& d): DXF_Entity(d)
    {
        eType = DXF::DIMENSION;
        type =d.type;
        name = d.name;
        defPoint = d.defPoint;
        textPoint = d.textPoint;
        text = d.text;
        style = d.style;
        align = d.align;
        linesty = d.linesty;
        linefactor = d.linefactor;
        rot = d.rot;
        extDirection = d.extDirection;
        clonePoint = d.clonePoint;
        def1 = d.def1;
        def2 = d.def2;
        angle = d.angle;
        oblique = d.oblique;
        arcPoint = d.arcPoint;
        circlePoint = d.circlePoint;
        length = d.length;
    }
    virtual ~DXF_Dimension() {}

    virtual void applyExtrusion(){}
    void parseCode(int code, dxfReader *reader);

public:
    DXF_Coord getDefPoint() const {return defPoint;}      // Definition point, code 10, 20 & 30 */
    void setDefPoint(const DXF_Coord p) {defPoint =p;}
    DXF_Coord getTextPoint() const {return textPoint;}    // Middle point of text, code 11, 21 & 31 */
    void setTextPoint(const DXF_Coord p) {textPoint =p;}
    std::string getStyle() const {return style;}          // Dimension style, code 3 */
    void setStyle(const std::string s) {style = s;}
    int getAlign() const { return align;}                 // attachment point, code 71 */
    void setAlign(const int a) { align = a;}
    int getTextLineStyle() const { return linesty;}       // Dimension text line spacing style, code 72, default 1 */
    void setTextLineStyle(const int l) { linesty = l;}
    std::string getText() const {return text;}            // Dimension text explicitly entered by the user, code 1 */
    void setText(const std::string t) {text = t;}
    double getTextLineFactor() const { return linefactor;} // Dimension text line spacing factor, code 41, default 1? */
    void setTextLineFactor(const double l) { linefactor = l;}
    double getDir() const { return rot;}                  // rotation angle of the dimension text, code 53 (optional) default 0 */
    void setDir(const double d) { rot = d;}

    DXF_Coord getExtrusion(){return extDirection;}            // extrusion, code 210, 220 & 230 */
    void setExtrusion(const DXF_Coord p) {extDirection =p;}
    std::string getName(){return name;}                   // Name of the block that contains the entities, code 2 */
    void setName(const std::string s) {name = s;}

protected:
    DXF_Coord getPt2() const {return clonePoint;}
    void setPt2(const DXF_Coord p) {clonePoint= p;}
    DXF_Coord getPt3() const {return def1;}
    void setPt3(const DXF_Coord p) {def1= p;}
    DXF_Coord getPt4() const {return def2;}
    void setPt4(const DXF_Coord p) {def2= p;}
    DXF_Coord getPt5() const {return circlePoint;}
    void setPt5(const DXF_Coord p) {circlePoint= p;}
    DXF_Coord getPt6() const {return arcPoint;}
    void setPt6(const DXF_Coord p) {arcPoint= p;}
    double getAn50() const {return angle;}      // Angle of rotated, horizontal, or vertical dimensions, code 50 */
    void setAn50(const double d) {angle = d;}
    double getOb52() const {return oblique;}    // oblique angle, code 52 */
    void setOb52(const double d) {oblique = d;}
    double getRa40() const {return length;}    // Leader length, code 40 */
    void setRa40(const double d) {length = d;}
public:
    int type;                  // Dimension type, code 70 */
private:
    std::string name;          // Name of the block that contains the entities, code 2 */
    DXF_Coord defPoint;        //  definition point, code 10, 20 & 30 (WCS) */
    DXF_Coord textPoint;       // Middle point of text, code 11, 21 & 31 (OCS) */
    UTF8STRING text;           // Dimension text explicitly entered by the user, code 1 */
    UTF8STRING style;          // Dimension style, code 3 */
    int align;                 // attachment point, code 71 */
    int linesty;               // Dimension text line spacing style, code 72, default 1 */
    double linefactor;         // Dimension text line spacing factor, code 41, default 1? (value range 0.25 to 4.00*/
    double rot;                // rotation angle of the dimension text, code 53 */
    DXF_Coord extDirection;        //  extrusion normal vector, code 210, 220 & 230 */

    double hdir;               // horizontal direction for the dimension, code 51, default ? */
    DXF_Coord clonePoint;      // Insertion point for clones (Baseline & Continue), code 12, 22 & 32 (OCS) */
    DXF_Coord def1;            // Definition point 1for linear & angular, code 13, 23 & 33 (WCS) */
    DXF_Coord def2;            // Definition point 2, code 14, 24 & 34 (WCS) */
    double angle;              // Angle of rotated, horizontal, or vertical dimensions, code 50 */
    double oblique;            // oblique angle, code 52 */

    DXF_Coord circlePoint;     // Definition point for diameter, radius & angular dims code 15, 25 & 35 (WCS) */
    DXF_Coord arcPoint;        // Point defining dimension arc, x coordinate, code 16, 26 & 36 (OCS) */
    double length;             // Leader length, code 40 */
};


//Handle  aligned dimension entity
class DXF_DimAligned : public DXF_Dimension
{
public:
    DXF_DimAligned()
    {
        eType = DXF::DIMALIGNED;
    }
    DXF_DimAligned(const DXF_Dimension& d): DXF_Dimension(d)
    {
        eType = DXF::DIMALIGNED;
    }

    DXF_Coord getClonepoint() const {return getPt2();}      // Insertion for clones (Baseline & Continue), 12, 22 & 32 */
    void setClonePoint(DXF_Coord c){setPt2(c);}

    DXF_Coord getDimPoint() const {return getDefPoint();}   // dim line location point, code 10, 20 & 30 */
    void setDimPoint(const DXF_Coord p){setDefPoint(p);}
    DXF_Coord getDef1Point() const {return getPt3();}       // Definition point 1, code 13, 23 & 33 */
    void setDef1Point(const DXF_Coord p) {setPt3(p);}
    DXF_Coord getDef2Point() const {return getPt4();}       // Definition point 2, code 14, 24 & 34 */
    void setDef2Point(const DXF_Coord p) {setPt4(p);}
};

//Handle  linear or rotated dimension entity
class DXF_DimLinear : public DXF_DimAligned
{
public:
    DXF_DimLinear()
    {
        eType = DXF::DIMLINEAR;
    }
    DXF_DimLinear(const DXF_Dimension& d): DXF_DimAligned(d)
    {
        eType = DXF::DIMLINEAR;
    }

    double getAngle() const {return getAn50();}          // Angle of rotated, horizontal, or vertical dimensions, code 50 */
    void setAngle(const double d) {setAn50(d);}
    double getOblique() const {return getOb52();}      // oblique angle, code 52 */
    void setOblique(const double d) {setOb52(d);}
};

//Handle radial dimension entity
class DXF_DimRadial : public DXF_Dimension
{
    public:
        DXF_DimRadial()
    {
        eType = DXF::DIMRADIAL;
    }
    DXF_DimRadial(const DXF_Dimension& d): DXF_Dimension(d)
    {
        eType = DXF::DIMRADIAL;
    }

    DXF_Coord getCenterPoint() const {return getDefPoint();}   // center point, code 10, 20 & 30 */
    void setCenterPoint(const DXF_Coord p){setDefPoint(p);}
    DXF_Coord getDiameterPoint() const {return getPt5();}      // Definition point for radius, code 15, 25 & 35 */
    void setDiameterPoint(const DXF_Coord p){setPt5(p);}
    double getLeaderLength() const {return getRa40();}         // Leader length, code 40 */
    void setLeaderLength(const double d) {setRa40(d);}
};

//Handle radial dimension entity
class DXF_DimDiametric : public DXF_Dimension
{
public:
    DXF_DimDiametric()
    {
        eType = DXF::DIMDIAMETRIC;
    }
    DXF_DimDiametric(const DXF_Dimension& d): DXF_Dimension(d)
    {
        eType = DXF::DIMDIAMETRIC;
    }

    DXF_Coord getDiameter1Point() const {return getPt5();}      // First definition point for diameter, code 15, 25 & 35 */
    void setDiameter1Point(const DXF_Coord p){setPt5(p);}
    DXF_Coord getDiameter2Point() const {return getDefPoint();} // Oposite point for diameter, code 10, 20 & 30 */
    void setDiameter2Point(const DXF_Coord p){setDefPoint(p);}
    double getLeaderLength() const {return getRa40();}          // Leader length, code 40 */
    void setLeaderLength(const double d) {setRa40(d);}
};

//Handle angular dimension entity
class DXF_DimAngular : public DXF_Dimension
{
public:
    DXF_DimAngular()
    {
        eType = DXF::DIMANGULAR;
    }
    DXF_DimAngular(const DXF_Dimension& d): DXF_Dimension(d)
    {
        eType = DXF::DIMANGULAR;
    }

    DXF_Coord getFirstLine1() const {return getPt3();}       // Definition point line 1-1, code 13, 23 & 33 */
    void setFirstLine1(const DXF_Coord p) {setPt3(p);}
    DXF_Coord getFirstLine2() const {return getPt4();}       // Definition point line 1-2, code 14, 24 & 34 */
    void setFirstLine2(const DXF_Coord p) {setPt4(p);}
    DXF_Coord getSecondLine1() const {return getPt5();}      // Definition point line 2-1, code 15, 25 & 35 */
    void setSecondLine1(const DXF_Coord p) {setPt5(p);}
    DXF_Coord getSecondLine2() const {return getDefPoint();} // Definition point line 2-2, code 10, 20 & 30 */
    void setSecondLine2(const DXF_Coord p){setDefPoint(p);}
    DXF_Coord getDimPoint() const {return getPt6();}         // Dimension definition point, code 16, 26 & 36 */
    void setDimPoint(const DXF_Coord p) {setPt6(p);}
};


//Handle angular 3p dimension entity
class DXF_DimAngular3p : public DXF_Dimension
{
public:
    DXF_DimAngular3p()
    {
        eType = DXF::DIMANGULAR3P;
    }
    DXF_DimAngular3p(const DXF_Dimension& d): DXF_Dimension(d)
    {
        eType = DXF::DIMANGULAR3P;
    }

    DXF_Coord getFirstLine() const {return getPt3();}       // Definition point line 1, code 13, 23 & 33 */
    void setFirstLine(const DXF_Coord p) {setPt3(p);}
    DXF_Coord getSecondLine() const {return getPt4();}       // Definition point line 2, code 14, 24 & 34 */
    void setSecondLine(const DXF_Coord p) {setPt4(p);}
    DXF_Coord getVertexPoint() const {return getPt5();}      // Vertex point, code 15, 25 & 35 */
    void SetVertexPoint(const DXF_Coord p) {setPt5(p);}
    DXF_Coord getDimPoint() const {return getDefPoint();}    // Dimension definition point, code 10, 20 & 30 */
    void setDimPoint(const DXF_Coord p) {setDefPoint(p);}
};

//Handle ordinate dimension entity
class DXF_DimOrdinate : public DXF_Dimension
{
public:
    DXF_DimOrdinate()
    {
        eType = DXF::DIMORDINATE;
    }
    DXF_DimOrdinate(const DXF_Dimension& d): DXF_Dimension(d)
    {
        eType = DXF::DIMORDINATE;
    }

    DXF_Coord getOriginPoint() const {return getDefPoint();}   // Origin definition point, code 10, 20 & 30 */
    void setOriginPoint(const DXF_Coord p) {setDefPoint(p);}
    DXF_Coord getFirstLine() const {return getPt3();}          // Feature location point, code 13, 23 & 33 */
    void setFirstLine(const DXF_Coord p) {setPt3(p);}
    DXF_Coord getSecondLine() const {return getPt4();}         // Leader end point, code 14, 24 & 34 */
    void setSecondLine(const DXF_Coord p) {setPt4(p);}
};


//Handle leader entity
class DXF_Leader : public DXF_Entity
{
public:
    DXF_Leader()
    {
        eType = DXF::LEADER;
        flag = 3;
        hookflag = vertnum = leadertype = 0;
        extrusionPoint.x = extrusionPoint.y = 0.0;
        arrow = 1;
        extrusionPoint.z = 1.0;
    }

    virtual void applyExtrusion(){}
    void parseCode(int code, dxfReader *reader);

public:
    UTF8STRING style;          // Dimension style name, code 3 */
    int arrow;                 // Arrowhead flag, code 71, 0=Disabled; 1=Enabled */
    int leadertype;            // Leader path type, code 72, 0=Straight line segments; 1=Spline */
    int flag;                  // Leader creation flag, code 73, default 3 */
    int hookline;              // Hook line direction flag, code 74, default 1 */
    int hookflag;              // Hook line flag, code 75 */
    double textheight;         // Text annotation height, code 40 */
    double textwidth;          // Text annotation width, code 41 */
    int vertnum;               // Number of vertices, code 76 */
    int coloruse;              // Color to use if leader's DIMCLRD = BYBLOCK, code 77 */
    duint32 annotHandle;       // Hard reference to associated annotation, code 340 */
    DXF_Coord extrusionPoint;  // Normal vector, code 210, 220 & 230 */
    DXF_Coord horizdir;        // "Horizontal" direction for leader, code 211, 221 & 231 */
    DXF_Coord offsetblock;     // Offset of last leader vertex from block, code 212, 222 & 232 */
    DXF_Coord offsettext;      // Offset of last leader vertex from annotation, code 213, 223 & 233 */

    std::vector<std::shared_ptr<DXF_Coord>> vertexlist;  // vertex points list, code 10, 20 & 30 */

private:
    std::shared_ptr<DXF_Coord> vertexpoint;   // current control point to add data */
};

//Handle viewport entity
class DXF_Viewport : public DXF_Point
{
public:
    DXF_Viewport()
    {
        eType = DXF::VIEWPORT;
        vpstatus = 0;
        pswidth = 205;
        psheight = 156;
        centerPX = 128.5;
        centerPY = 97.5;
    }

    virtual void applyExtrusion(){}
    void parseCode(int code, dxfReader *reader);

public:
    double pswidth;           // Width in paper space units, code 40 */
    double psheight;          // Height in paper space units, code 41 */
    int vpstatus;             // Viewport status, code 68 */
    int vpID;                 // Viewport ID, code 69 */
    double centerPX;          // view center point X, code 12 */
    double centerPY;          // view center point Y, code 22 */
    double snapPX;          // Snap base point X, code 13 */
    double snapPY;          // Snap base point Y, code 23 */
    double snapSpPX;          // Snap spacing X, code 14 */
    double snapSpPY;          // Snap spacing Y, code 24 */
    //TODO: complete in dxf
    DXF_Coord viewDir;        // View direction vector, code 16, 26 & 36 */
    DXF_Coord viewTarget;     // View target point, code 17, 27, 37 */
    double viewLength;        // Perspective lens length, code 42 */
    double frontClip;         // Front clip plane Z value, code 43 */
    double backClip;          // Back clip plane Z value, code 44 */
    double viewHeight;        // View height in model space units, code 45 */
    double snapAngle;         // Snap angle, code 50 */
    double twistAngle;        // view twist angle, code 51 */

private:
    duint32 frozenLyCount;
};
#endif

// EOF

