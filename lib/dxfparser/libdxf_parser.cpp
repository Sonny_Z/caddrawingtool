/******************************************************************************
**  LIBDXFParserLIB - Library to read/write DXF files (ascii & binary)              **
******************************************************************************/


#include "libdxf_parser.h"
#include <tchar.h>
#include <fstream>
#include <algorithm>
#include <sstream>
#include "dxfreader.h"

#include <QTime>
#include <QDebug>

#define FIRSTHANDLE 48
using namespace std;
LIBDXFParser::LIBDXFParser(const char* name)
{
    fileName = name;
    reader = nullptr;
    applyExt = false;
}
LIBDXFParser::~LIBDXFParser()
{
    if (reader != nullptr)
        delete reader;
//    if(iface != nullptr){
//        delete iface;
//    }
//    fileName.clear();
//    nextentity.clear();
}

bool LIBDXFParser::read(DXF_Interface *interface_, bool ext)
{
    bool isOk = false;

    applyExt = ext;
    std::ifstream filestr;
    if ( interface_ == nullptr )
        return isOk;
    auto fileNameBta = fileName.c_str();
    //设置代码页为简体中文
//    std::locale loc1 = std::locale::global(std::locale(".936"));
//    std::locale prev_loc = std::locale::global( std::locale("chs") );
//    locale::global(locale("chs"));
//    setlocale(LC_ALL,  " Chinese-simplified " );
//    std::locale loc = std::locale::global(std::locale(""));
    filestr.open (fileNameBta, std::ios_base::in | std::ios::binary);
    //恢复原来的代码页
//    std::locale::global( loc );
//    locale::global(locale("C"));
//    std::locale::global( prev_loc );
//    std::locale::global(std::locale(loc1));
    if (!filestr.is_open())
        return isOk;
    if (!filestr.good())
        return isOk;

    char line[22];
    char line2[22] = "Binary DXF\r\n";
    line2[20] = (char)26;
    line2[21] = '\0';
    filestr.read (line, 22);
    filestr.close();
    iface = interface_;
    if (strcmp(line, line2) == 0)
    {
        filestr.open (fileName.c_str(), std::ios_base::in | std::ios::binary);
        binFile = true;
        //skip sentinel
        filestr.seekg (22, std::ios::beg);
        reader = new dxfReaderBinary(&filestr);
    }
    else
    {
        binFile = false;
        filestr.open (fileName.c_str(), std::ios_base::in);
        reader = new dxfReaderAscii(&filestr);
    }

    isOk = processDxf();
    filestr.close();
    delete reader;
    reader = nullptr;
    return isOk;
}

/********* Reader Process *********/
bool LIBDXFParser::processDxf()
{
    int code;
    bool more = true;
    std::string sectionstr;
    reader->setIgnoreComments(false);
    while (reader->readRec(&code))
    {
        if (code == 999)
        {
            header.addComment(reader->getString());
        }
        else if (code == 0)
        {
            // ignore further comments
            reader->setIgnoreComments(true);
            sectionstr = reader->getString();
            if (sectionstr == "EOF")
            {
                return true;  //found EOF terminate
            }
            if (sectionstr == "SECTION")
            {
                more = reader->readRec(&code);
                if (!more)
                    return false; //wrong dxf file
                if (code == 2)
                {
                    sectionstr = reader->getString();
                    //found section, process it
                    if (sectionstr == "HEADER")
                    {
                        processHeader();
                    }
                    else if (sectionstr == "TABLES")
                    {
                        processTables();
                    }
                    else if (sectionstr == "BLOCKS")
                    {
                        QTime startTime = QTime::currentTime();

                        processBlocks();

                        QTime blockTime = QTime::currentTime();
//                        qDebug() << "It takes " << startTime.secsTo(blockTime) << " seconds to processBlocks.";

                    }
                    else if (sectionstr == "ENTITIES")
                    {
                        QTime startTime = QTime::currentTime();

                        processEntities(false);

                        QTime entitiesTime = QTime::currentTime();
//                        qDebug() << "It takes " << startTime.secsTo(entitiesTime) << " seconds to processEntitie.";
                    }
                    else if (sectionstr == "OBJECTS")
                    {
                        //processObjects();
                    }
                }
            }
        }
    }
    return true;
}

/********* Header Section *********/
bool LIBDXFParser::processHeader()
{
    int code;
    std::string sectionstr;
    while (reader->readRec(&code))
    {
        if (code == 0)
        {
            sectionstr = reader->getString();
            if (sectionstr == "ENDSEC")
            {
                iface->addHeader(&header);
                return true;  //found ENDSEC terminate
            }
        }
        else
            header.parseCode(code, reader);
    }
    return true;
}

/********* Tables Section *********/
bool LIBDXFParser::processTables()
{
    int code;
    std::string sectionstr;
    bool more = true;
    while (reader->readRec(&code))
    {
        if (code == 0)
        {
            sectionstr = reader->getString();
            if (sectionstr == "TABLE")
            {
                more = reader->readRec(&code);
                if (!more)
                    return false; //wrong dxf file
                if (code == 2)
                {
                    sectionstr = reader->getString();
                    //found section, process it
                    if (sectionstr == "LTYPE")
                    {
                        processLType();
                    }
                    else if (sectionstr == "LAYER")
                    {
                        processLayer();
                    }
                    else if (sectionstr == "STYLE")
                    {
                        processTextStyle();
                    }
                    else if (sectionstr == "VPORT")
                    {
                        processVports();
                    }
                    else if (sectionstr == "APPID")
                    {
                        processAppId();
                    }
                    else if (sectionstr == "DIMSTYLE")
                    {
                        processDimStyle();
                    }
                    else if (sectionstr == "BLOCK_RECORD")
                    {
                        //processBlockRecord();
                    }
                }
            } else if (sectionstr == "ENDSEC") {
                return true;  //found ENDSEC terminate
            }
        }
    }
    return true;
}

bool LIBDXFParser::processLType()
{
    int code;
    std::string sectionstr;
    bool reading = false;
    DXF_LType ltype;
    while (reader->readRec(&code))
    {
        if (code == 0)
        {
            if (reading)
            {
                ltype.update();
                iface->addLType(ltype);
            }
            sectionstr = reader->getString();
            if (sectionstr == "LTYPE")
            {
                reading = true;
                ltype.reset();
            } else if (sectionstr == "ENDTAB")
            {
                return true;  //found ENDTAB terminate
            }
        }
        else if (reading)
            ltype.parseCode(code, reader);
    }
    return true;
}

bool LIBDXFParser::processLayer()
{
    int code;
    std::string sectionstr;
    bool reading = false;
    DXF_Layer layer;
    while (reader->readRec(&code))
    {
        if (code == 0) {
            if (reading)
                iface->addLayer(layer);
            sectionstr = reader->getString();
            if (sectionstr == "LAYER")
            {
                reading = true;
                layer.reset();
            } else if (sectionstr == "ENDTAB")
            {
                return true;  //found ENDTAB terminate
            }
        } else if (reading)
            layer.parseCode(code, reader);
    }
    return true;
}

bool LIBDXFParser::processDimStyle()
{
    int code;
    std::string sectionstr;
    bool reading = false;
    DXF_Dimstyle dimSty;
    while (reader->readRec(&code))
    {
        if (code == 0)
        {
            if (reading)
                iface->addDimStyle(dimSty);
            sectionstr = reader->getString();
            if (sectionstr == "DIMSTYLE")
            {
                reading = true;
                dimSty.reset();
            } else if (sectionstr == "ENDTAB")
            {
                return true;  //found ENDTAB terminate
            }
        } else if (reading)
            dimSty.parseCode(code, reader);
    }
    return true;
}

bool LIBDXFParser::processTextStyle()
{
    int code;
    std::string sectionstr;
    bool reading = false;
    DXF_Textstyle textStyle;
    while (reader->readRec(&code))
    {
        if (code == 0)
        {
            if (reading)
                iface->addTextStyle(textStyle);
            sectionstr = reader->getString();
            if (sectionstr == "STYLE")
            {
                reading = true;
                textStyle.reset();
            }
            else if (sectionstr == "ENDTAB")
            {
                return true;  //found ENDTAB terminate
            }
        }
        else if (reading)
            textStyle.parseCode(code, reader);
    }
    return true;
}

bool LIBDXFParser::processVports()
{
    int code;
    std::string sectionstr;
    bool reading = false;
    DXF_Vport vp;
    while (reader->readRec(&code))
    {
        if (code == 0) {
            if (reading)
                iface->addVport(vp);
            sectionstr = reader->getString();
            if (sectionstr == "VPORT")
            {
                reading = true;
                vp.reset();
            } else if (sectionstr == "ENDTAB")
            {
                return true;  //found ENDTAB terminate
            }
        } else if (reading)
            vp.parseCode(code, reader);
    }
    return true;
}

bool LIBDXFParser::processAppId()
{
    int code;
    std::string sectionstr;
    bool reading = false;
    DXF_AppId vp;
    while (reader->readRec(&code))
    {
        if (code == 0)
        {
            if (reading)
                iface->addAppId(vp);
            sectionstr = reader->getString();
            if (sectionstr == "APPID")
            {
                reading = true;
                vp.reset();
            }
            else if (sectionstr == "ENDTAB")
            {
                return true;  //found ENDTAB terminate
            }
        }
        else if (reading)
            vp.parseCode(code, reader);
    }
    return true;
}

/********* Block Section *********/

bool LIBDXFParser::processBlocks()
{
    int code;
    std::string sectionstr;
    while (reader->readRec(&code))
    {
        if (code == 0)
        {
            sectionstr = reader->getString();
            if (sectionstr == "BLOCK")
            {
                processBlock();
            }
            else if (sectionstr == "ENDSEC")
            {
                return true;  //found ENDSEC terminate
            }
        }
    }
    return true;
}

bool LIBDXFParser::processBlock()
{
    int code;
    DXF_Block block;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->addBlock(block);
            if (nextentity == "ENDBLK")
            {
                iface->endBlock();
                return true;  //found ENDBLK, terminate
            }
            else
            {
                processEntities(true);
                iface->endBlock();
                return true;  //found ENDBLK, terminate
            }
        }
        default:
            block.parseCode(code, reader);
            break;
        }
    }
    return true;
}


/********* Entities Section *********/
bool LIBDXFParser::processEntities(bool isblock)
{
    int code;
    if (!reader->readRec(&code))
    {
        return false;
    }
    bool next = true;
    if (code == 0)
    {
        nextentity = reader->getString();
    }
    else if (!isblock)
    {
        return false;  //first record in entities is 0
    }
    do
    {
        if (nextentity == "ENDSEC" || nextentity == "ENDBLK")
        {
            return true;  //found ENDSEC or ENDBLK terminate
        } else if (nextentity == "POINT") {
            processPoint();
        } else if (nextentity == "LINE") {
            processLine();
        } else if (nextentity == "CIRCLE") {
            processCircle();
        } else if (nextentity == "ARC") {
            processArc();
        } else if (nextentity == "ELLIPSE") {
            processEllipse();
        } else if (nextentity == "TRACE") {
            processTrace();
        } else if (nextentity == "SOLID") {
            processSolid();
        } else if (nextentity == "INSERT") {
            processInsert();
        } else if (nextentity == "LWPOLYLINE") {
            processLWPolyline();
        } else if (nextentity == "POLYLINE") {
            processPolyline();
        } else if (nextentity == "TEXT" ) {//|| nextentity == "ATTDEF" || nextentity == "ATTRIB"
            processText();
        } else if (nextentity == "MTEXT") {
            processMText();
        } else if (nextentity == "HATCH") {
            processHatch();
        } else if (nextentity == "SPLINE") {
            processSpline();
        } else if (nextentity == "3DFACE") {
            process3dface();
        } else if (nextentity == "VIEWPORT") {
            processViewport();
        } else if (nextentity == "IMAGE") {
            processImage();
        } else if (nextentity == "DIMENSION") {
            processDimension();
        } else if (nextentity == "LEADER") {
            processLeader();
        } else if (nextentity == "RAY") {
            processRay();
        } else if (nextentity == "XLINE") {
            processXline();
        } else if(nextentity=="ATTDEF")
        {
            processAttdef();
        } else if(nextentity=="ATTRIB")
        {
            processAttrib();
        }
        else
        {
            if (reader->readRec(&code))
            {
                if (code == 0)
                    nextentity = reader->getString();
            } else
                return false; //end of file without ENDSEC
        }
    } while (next);
    return true;
}

bool LIBDXFParser::processEllipse()
{
    int code;
    DXF_Ellipse ellipse;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            if (applyExt)
                ellipse.applyExtrusion();
            iface->addEllipse(ellipse);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            ellipse.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processTrace()
{
    int code;
    DXF_Trace trace;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0: {
            nextentity = reader->getString();
            if (applyExt)
                trace.applyExtrusion();
            iface->addTrace(trace);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            trace.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processSolid()
{
    int code;
    DXF_Solid solid;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            if (applyExt)
                solid.applyExtrusion();
            iface->addSolid(solid);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            solid.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::process3dface()
{
    int code;
    DXF_3Dface face;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->add3dFace(face);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            face.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processViewport()
{
    int code;
    DXF_Viewport vp;
    while (reader->readRec(&code))
    {
        switch (code) {
        case 0: {
            nextentity = reader->getString();
            iface->addViewport(vp);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            vp.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processPoint()
{
    int code;
    DXF_Point point;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->addPoint(point);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            point.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processLine()
{
    int code;
    DXF_Line line;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->addLine(line);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            line.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processRay()
{
    int code;
    DXF_Ray line;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->addRay(line);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            line.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processXline()
{
    int code;
    DXF_Xline line;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->addXline(line);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            line.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processCircle()
{
    int code;
    DXF_Circle circle;
    while (reader->readRec(&code))
    {
        switch (code) {
        case 0: {
            nextentity = reader->getString();
            if (applyExt)
                circle.applyExtrusion();
            iface->addCircle(circle);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            circle.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processArc()
{
    int code;
    DXF_Arc arc;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            if (applyExt)
                arc.applyExtrusion();
            iface->addArc(arc);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            arc.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processInsert()
{
    int code;
    DXF_Insert insert;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            if(applyExt)
                insert.applyExtrusion();
            iface->addInsert(insert);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            insert.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processLWPolyline()
{
    int code;
    DXF_LWPolyline pl;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            if (applyExt)
                pl.applyExtrusion();
            iface->addLWPolyline(pl);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            pl.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processPolyline()
{
    int code;
    DXF_Polyline pl;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            if (nextentity != "VERTEX") {
                iface->addPolyline(pl);
                return true;  //found new entity or ENDSEC, terminate
            } else {
                processVertex(&pl);
            }
        }
        default:
            pl.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processVertex(DXF_Polyline *pl)
{
    int code;
    std::shared_ptr<DXF_Vertex> v = std::make_shared<DXF_Vertex>();
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
            pl->appendVertex(v);
            nextentity = reader->getString();
            if (nextentity == "SEQEND")
                return true;  //found SEQEND no more vertex, terminate
            else if (nextentity == "VERTEX")
                v.reset(new DXF_Vertex); //another vertex

        default:
            v->parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processText()
{
    int code;
    DXF_Text txt;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->addText(txt);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            txt.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processMText()
{
    int code;
    DXF_MText txt;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            txt.updateAngle();
            iface->addMText(txt);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            txt.parseCode(code, reader);
            break;
        }
    }
    return true;
}
bool LIBDXFParser::processAttdef()
{
    int code;
    DXF_Attdef txt;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            //txt.updateAngle();
            iface->addAttdef(txt);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            txt.parseCode(code, reader);
            break;
        }
    }
    return true;
}
bool LIBDXFParser::processAttrib()
{
    int code;
    DXF_Attrib txt;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            //txt.updateAngle();
            iface->addAttrib(txt);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            txt.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processHatch()
{
    int code;
    DXF_Hatch hatch;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->addHatch(&hatch);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            hatch.parseCode(code, reader);
            break;
        }
    }
    return true;
}


bool LIBDXFParser::processSpline()
{
    int code;
    DXF_Spline sp;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->addSpline(&sp);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            sp.parseCode(code, reader);
            break;
        }
    }
    return true;
}


bool LIBDXFParser::processImage()
{
    int code;
    DXF_Image img;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->addImage(&img);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            img.parseCode(code, reader);
            break;
        }
    }
    return true;
}


bool LIBDXFParser::processDimension()
{
    int code;
    DXF_Dimension dim;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            int type = dim.type & 0x0F;
            switch (type)
            {
            case 0:
            {
                DXF_DimLinear d(dim);
                iface->addDimLinear(&d);
                break;
            }
            case 1:
            {
                DXF_DimAligned d(dim);
                iface->addDimAlign(&d);
                break;
            }
            case 2:
            {
                DXF_DimAngular d(dim);
                iface->addDimAngular(&d);
                break;
            }
            case 3:
            {
                DXF_DimDiametric d(dim);
                iface->addDimDiametric(&d);
                break;
            }
            case 4:
            {
                DXF_DimRadial d(dim);
                iface->addDimRadial(&d);
                break;
            }
            case 5:
            {
                DXF_DimAngular3p d(dim);
                iface->addDimAngular3P(&d);
                break;
            }
            case 6:
            {
                DXF_DimOrdinate d(dim);
                iface->addDimOrdinate(&d);
                break;
            }
            default:
                break;
            }
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            dim.parseCode(code, reader);
            break;
        }
    }
    return true;
}

bool LIBDXFParser::processLeader()
{
    int code;
    DXF_Leader leader;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->addLeader(&leader);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            leader.parseCode(code, reader);
            break;
        }
    }
    return true;
}


/********* Objects Section *********/

bool LIBDXFParser::processObjects()
{
    int code;
    if (!reader->readRec(&code))

    {
        return false;
    }
    bool next = true;
    if (code == 0) {
        nextentity = reader->getString();
    }
    else
    {
        return false;  //first record in objects is 0
    }
    do {
        if (nextentity == "ENDSEC")
        {
            return true;  //found ENDSEC terminate
        } else if (nextentity == "IMAGEDEF")
        {
            processImageDef();
        }
        else
        {
            if (reader->readRec(&code))
            {
                if (code == 0)
                    nextentity = reader->getString();
            } else
                return false; //end of file without ENDSEC
        }

    } while (next);
    return true;
}

bool LIBDXFParser::processImageDef()
{
    int code;
    DXF_ImageDef img;
    while (reader->readRec(&code))
    {
        switch (code)
        {
        case 0:
        {
            nextentity = reader->getString();
            iface->linkImage(&img);
            return true;  //found new entity or ENDSEC, terminate
        }
        default:
            img.parseCode(code, reader);
            break;
        }
    }
    return true;
}
