/******************************************************************************
**  FASDXFLIB - Library to read/write DXF files (ascii & binary)              **
******************************************************************************/

#ifndef DXF_OBJECTS_H
#define DXF_OBJECTS_H


#include <string>
#include <vector>
#include <map>
#include "dxf_base.h"

class dxfReader;
class dxfWriter;

namespace DXF
{

// Table entries type.
     enum TTYPE
     {
         UNKNOWNT,
         LTYPE,
         LAYER,
         STYLE,
         DIMSTYLE,
         VPORT,
         BLOCK_RECORD,
         APPID,
         IMAGEDEF
     };
}

// Base class for tables entries
class DXF_TableEntry
{
public:
    //initializes default values
    DXF_TableEntry()
    {
        tType = DXF::UNKNOWNT;
        flags = 0;
        parentHandle = 0;
        curr = nullptr;
    }

    virtual ~DXF_TableEntry()
    {
        for (std::vector<DXF_Variant*>::iterator it=extData.begin(); it!=extData.end(); ++it)
            delete *it;

        extData.clear();
    }

    DXF_TableEntry(const DXF_TableEntry& e)
    {
        tType = e.tType;
        handle = e.handle;
        parentHandle = e.parentHandle;
        name = e.name;
        flags = e.flags;
        curr = e.curr;
        for (std::vector<DXF_Variant*>::const_iterator it=e.extData.begin(); it!=e.extData.end(); ++it){
            extData.push_back(new DXF_Variant(*(*it)));
        }
    }

    void parseCode(int code, dxfReader *reader);
protected:
    void reset()
    {
        flags =0;
        for (std::vector<DXF_Variant*>::iterator it=extData.begin(); it!=extData.end(); ++it)
            delete *it;
        extData.clear();
    }

public:
    enum DXF::TTYPE tType;     // enum: entity type, code 0 */
    duint32 handle;            // entity identifier, code 5 */
    int parentHandle;          // Soft-pointer ID/handle to owner object, code 330 */
    UTF8STRING name;           // entry name, code 2 */
    int flags;                 // Flags relevant to entry, code 70 */
    std::vector<DXF_Variant*> extData; // FIFO list of extended data, codes 1000 to 1071*/

private:
    DXF_Variant* curr;
};


//Handle dimstyle entries
class DXF_Dimstyle : public DXF_TableEntry
{
    //friend class FASDXF;
public:
    DXF_Dimstyle() { reset();}

    void reset()
    {
        tType = DXF::DIMSTYLE;
        dimasz = dimtxt = dimexe = 0.18;
        dimexo = 0.0625;
        dimgap = dimcen = 0.09;
        dimtxsty = "Standard";
        dimscale = dimlfac = dimtfac = dimfxl = 1.0;
        dimdli = 0.38;
        dimrnd = dimdle = dimtp = dimtm = dimtsz = dimtvp = 0.0;
        dimaltf = 25.4;
        dimtol = dimlim = dimse1 = dimse2 = dimtad = dimzin = 0;
        dimtoh = dimtolj = 1;
        dimalt = dimtofl = dimsah = dimtix = dimsoxd = dimfxlon = 0;
        dimaltd = dimunit = dimaltu = dimalttd = dimlunit = 2;
        dimclrd = dimclre = dimclrt = dimjust = dimupt = 0;
        dimazin = dimaltz = dimaltttz = dimtzin = dimfrac = 0;
        dimtih = dimadec = dimaunit = dimsd1 = dimsd2 = dimtmove = 0;
        dimaltrnd = 0.0;
        dimdec = dimtdec = 4;
        dimfit = dimatfit = 3;
        dimdsep = '.';
        dimlwd = dimlwe = -2;
        DXF_TableEntry::reset();
    }

    void parseCode(int code, dxfReader *reader);

public:
    //V12
    UTF8STRING dimpost;       // code 3 */
    UTF8STRING dimapost;      // code 4 */
/* handle are code 105 */
    UTF8STRING dimblk;        // code 5, code 342 V2000+ */
    UTF8STRING dimblk1;       // code 6, code 343 V2000+ */
    UTF8STRING dimblk2;       // code 7, code 344 V2000+ */
    double dimscale;          // code 40 */
    double dimasz;            // code 41 */
    double dimexo;            // code 42 */
    double dimdli;            // code 43 */
    double dimexe;            // code 44 */
    double dimrnd;            // code 45 */
    double dimdle;            // code 46 */
    double dimtp;             // code 47 */
    double dimtm;             // code 48 */
    double dimfxl;            // code 49 V2007+ */
    double dimtxt;            // code 140 */
    double dimcen;            // code 141 */
    double dimtsz;            // code 142 */
    double dimaltf;           // code 143 */
    double dimlfac;           // code 144 */
    double dimtvp;            // code 145 */
    double dimtfac;           // code 146 */
    double dimgap;            // code 147 */
    double dimaltrnd;         // code 148 V2000+ */
    int dimtol;               // code 71 */
    int dimlim;               // code 72 */
    int dimtih;               // code 73 */
    int dimtoh;               // code 74 */
    int dimse1;               // code 75 */
    int dimse2;               // code 76 */
    int dimtad;               // code 77 */
    int dimzin;               // code 78 */
    int dimazin;              // code 79 V2000+ */
    int dimalt;               // code 170 */
    int dimaltd;              // code 171 */
    int dimtofl;              // code 172 */
    int dimsah;               // code 173 */
    int dimtix;               // code 174 */
    int dimsoxd;              // code 175 */
    int dimclrd;              // code 176 */
    int dimclre;              // code 177 */
    int dimclrt;              // code 178 */
    int dimadec;              // code 179 V2000+ */
    int dimunit;              // code 270 R13+ (obsolete 2000+, use dimlunit & dimfrac) */
    int dimdec;               // code 271 R13+ */
    int dimtdec;              // code 272 R13+ */
    int dimaltu;              // code 273 R13+ */
    int dimalttd;             // code 274 R13+ */
    int dimaunit;             // code 275 R13+ */
    int dimfrac;              // code 276 V2000+ */
    int dimlunit;             // code 277 V2000+ */
    int dimdsep;              // code 278 V2000+ */
    int dimtmove;             // code 279 V2000+ */
    int dimjust;              // code 280 R13+ */
    int dimsd1;               // code 281 R13+ */
    int dimsd2;               // code 282 R13+ */
    int dimtolj;              // code 283 R13+ */
    int dimtzin;              // code 284 R13+ */
    int dimaltz;              // code 285 R13+ */
    int dimaltttz;            // code 286 R13+ */
    int dimfit;               // code 287 R13+  (obsolete 2000+, use dimatfit & dimtmove)*/
    int dimupt;               // code 288 R13+ */
    int dimatfit;             // code 289 V2000+ */
    int dimfxlon;             // code 290 V2007+ */
    UTF8STRING dimtxsty;      // code 340 R13+ */
    UTF8STRING dimldrblk;     // code 341 V2000+ */
    int dimlwd;               // code 371 V2000+ */
    int dimlwe;               // code 372 V2000+ */
};


//Handle line type entries
class DXF_LType : public DXF_TableEntry
{
    //friend class FASDXF;
public:
    DXF_LType() { reset();}

    void reset(){
        tType = DXF::LTYPE;
        desc = "";
        size = 0;
        length = 0.0;
        pathIdx = 0;
        DXF_TableEntry::reset();
    }

    void parseCode(int code, dxfReader *reader);
    void update();

public:
    UTF8STRING desc;           // descriptive string, code 3 */
    int size;                 // element number, code 73 */
    double length;            // total length of pattern, code 40 */
    std::vector<double> path;  // trace, point or space length sequence, code 49 */
private:
    int pathIdx;
};


//Handle layer entries
class DXF_Layer : public DXF_TableEntry
{
    //friend class FASDXF;
public:
    DXF_Layer() { reset();}

    void reset()
    {
        tType = DXF::LAYER;
        lineType = "CONTINUOUS";
        color = 7; // default BYLAYER (256)
        plotF = true; // default TRUE (plot yes)
        lWeight = DXF_LW_Conv::widthDefault; // default BYDEFAULT (dxf -3, dwg 31)
        color24 = -1; //default -1 not set
        DXF_TableEntry::reset();
    }

    void parseCode(int code, dxfReader *reader);

public:
    UTF8STRING lineType;            // line type, code 6 */
    int color;                      // layer color, code 62 */
    int color24;                    // 24-bit color, code 420 */
    bool plotF;                     // Plot flag, code 290 */
    enum DXF_LW_Conv::lineWidth lWeight; // layer lineweight, code 370 */
    std::string handlePlotS;        // Hard-pointer ID/handle of plotstyle, code 390 */
    std::string handleMaterialS;        // Hard-pointer ID/handle of materialstyle, code 347 */
};

//Handle block record entries
class DXF_Block_Record : public DXF_TableEntry
{
    //friend class FASDXF;
public:
    DXF_Block_Record() { reset();}
    void reset() {
        tType = DXF::BLOCK_RECORD;
        flags = 0;
        firstEH = lastEH = DXF::NoHandle;
        DXF_TableEntry::reset();
    }

public:
//Note:    int DXF_TableEntry::flags; contains code 70 of block
    int insUnits;             // block insertion units, code 70 of block_record*/
private:
    duint32 block;   //handle for block entity
    duint32 endBlock;//handle for end block entity
    duint32 firstEH; //handle of first entity, only in pre-2004
    duint32 lastEH;  //handle of last entity, only in pre-2004
    std::vector<duint32>entMap;
};

//Handle text style entries
class DXF_Textstyle : public DXF_TableEntry
{
    //friend class FASDXF;
public:
    DXF_Textstyle() { reset();}

    void reset()
    {
        tType = DXF::STYLE;
        height = oblique = 0.0;
        width = lastHeight = 1.0;
        font="txt";
        genFlag = 0; //2= X mirror, 4= Y mirror
        fontFamily = 0;
        DXF_TableEntry::reset();
    }

    void parseCode(int code, dxfReader *reader);

public:
    double height;          // Fixed text height (0 not set), code 40 */
    double width;           // Width factor, code 41 */
    double oblique;         // Oblique angle, code 50 */
    int genFlag;            // Text generation flags, code 71 */
    double lastHeight;      // Last height used, code 42 */
    UTF8STRING font;        // primary font file name, code 3 */
    UTF8STRING bigFont;     // bigfont file name or blank if none, code 4 */
    int fontFamily;         // ttf font family, italic and bold flags, code 1071 */
};

//Handle vport entries
class DXF_Vport : public DXF_TableEntry
{
public:
    DXF_Vport() { reset();}

    void reset()
    {
        tType = DXF::VPORT;
        UpperRight.x = UpperRight.y = 1.0;
        snapSpacing.x = snapSpacing.y = 10.0;
        gridSpacing = snapSpacing;
        center.x = 0.651828;
        center.y = -0.16;
        viewDir.z = 1;
        height = 5.13732;
        ratio = 2.4426877;
        lensHeight = 50;
        frontClip = backClip = snapAngle = twistAngle = 0.0;
        viewMode = snap = grid = snapStyle = snapIsopair = 0;
        fastZoom = 1;
        circleZoom = 100;
        ucsIcon = 3;
        gridBehavior = 7;
        DXF_TableEntry::reset();
    }

    void parseCode(int code, dxfReader *reader);

public:
    DXF_Coord lowerLeft;     // Lower left corner, code 10 & 20 */
    DXF_Coord UpperRight;    // Upper right corner, code 11 & 21 */
    DXF_Coord center;        // center point in WCS, code 12 & 22 */
    DXF_Coord snapBase;      // snap base point in DCS, code 13 & 23 */
    DXF_Coord snapSpacing;   // snap Spacing, code 14 & 24 */
    DXF_Coord gridSpacing;   // grid Spacing, code 15 & 25 */
    DXF_Coord viewDir;       // view direction from target point, code 16, 26 & 36 */
    DXF_Coord viewTarget;    // view target point, code 17, 27 & 37 */
    double height;           // view height, code 40 */
    double ratio;            // viewport aspect ratio, code 41 */
    double lensHeight;       // lens height, code 42 */
    double frontClip;        // front clipping plane, code 43 */
    double backClip;         // back clipping plane, code 44 */
    double snapAngle;        // snap rotation angle, code 50 */
    double twistAngle;       // view twist angle, code 51 */
    int viewMode;            // view mode, code 71 */
    int circleZoom;          // circle zoom percent, code 72 */
    int fastZoom;            // fast zoom setting, code 73 */
    int ucsIcon;             // UCSICON setting, code 74 */
    int snap;                // snap on/off, code 75 */
    int grid;                // grid on/off, code 76 */
    int snapStyle;           // snap style, code 77 */
    int snapIsopair;         // snap isopair, code 78 */
    int gridBehavior;        // grid behavior, code 60, undocummented */
};


//Handle imagedef entries
class DXF_ImageDef : public DXF_TableEntry {
public:
    DXF_ImageDef()
    {
        reset();
    }

    void reset(){
        tType = DXF::IMAGEDEF;
        imgVersion = 0;
        DXF_TableEntry::reset();
    }

    void parseCode(int code, dxfReader *reader);

public:
    UTF8STRING name;          // File name of image, code 1 */
    int imgVersion;           // class version, code 90, 0=R14 version */
    double u;                 // image size in pixels U value, code 10 */
    double v;                 // image size in pixels V value, code 20 */
    double up;                // default size of one pixel U value, code 11 */
    double vp;                // default size of one pixel V value, code 12 really is 21*/
    int loaded;               // image is loaded flag, code 280, 0=unloaded, 1=loaded */
    int resolution;           // resolution units, code 281, 0=no, 2=centimeters, 5=inch */

    std::map<std::string,std::string> reactors;
};

//Handle AppId entries
class DXF_AppId : public DXF_TableEntry
{
public:
    DXF_AppId() { reset();}

    void reset()
    {
        tType = DXF::APPID;
        flags = 0;
        name = "";
    }
    void parseCode(int code, dxfReader *reader){DXF_TableEntry::parseCode(code, reader);}
};

//Handle header entries
class DXF_Header
{
    friend class FASDXF;
public:
    DXF_Header();
    ~DXF_Header()
    {
        clearVars();
    }

    DXF_Header(const DXF_Header& h)
    {
        this->version = h.version;
        this->comments = h.comments;
        for (std::map<std::string,DXF_Variant*>::const_iterator it=h.vars.begin(); it!=h.vars.end(); ++it)
        {
            this->vars[it->first] = new DXF_Variant( *(it->second) );
        }
        this->curr = nullptr;
    }
    DXF_Header& operator=(const DXF_Header &h)
    {
       if(this != &h)
       {
           clearVars();
           this->version = h.version;
           this->comments = h.comments;
           for (std::map<std::string,DXF_Variant*>::const_iterator it=h.vars.begin(); it!=h.vars.end(); ++it)
           {
               this->vars[it->first] = new DXF_Variant( *(it->second) );
           }
       }
       return *this;
    }

    void addDouble(std::string key, double value, int code);
    void addInt(std::string key, int value, int code);
    void addStr(std::string key, std::string value, int code);
    void addCoord(std::string key, DXF_Coord value, int code);
    std::string getComments() const {return comments;}
    void write(dxfWriter *writer, DXF::Version ver);
    void addComment(std::string c);

    void parseCode(int code, dxfReader *reader);
private:
    bool getDouble(std::string key, double *varDouble);
    bool getInt(std::string key, int *varInt);
    bool getStr(std::string key, std::string *varStr);
    bool getCoord(std::string key, DXF_Coord *varStr);
    void clearVars()
    {
        for (std::map<std::string,DXF_Variant*>::iterator it=vars.begin(); it!=vars.end(); ++it)
            delete it->second;
        vars.clear();
    }

public:
    std::map<std::string,DXF_Variant*> vars;
private:
    std::string comments;
    std::string name;
    DXF_Variant* curr;
    int version; //to use on read

    duint32 linetypeCtrl;
    duint32 layerCtrl;
    duint32 styleCtrl;
    duint32 dimstyleCtrl;
    duint32 appidCtrl;
    duint32 blockCtrl;
    duint32 viewCtrl;
    duint32 ucsCtrl;
    duint32 vportCtrl;
    duint32 vpEntHeaderCtrl;
};

namespace DXF
{
const unsigned char dxfColors[][3] =
{
    {  0,  0,  0}, // unused
    {255,  0,  0}, // 1 red
    {255,255,  0}, // 2 yellow
    {  0,255,  0}, // 3 green
    {  0,255,255}, // 4 cyan
    {  0,  0,255}, // 5 blue
    {255,  0,255}, // 6 magenta
    {  0,  0,  0}, // 7 black or white
    {128,128,128}, // 8 50% gray
    {192,192,192}, // 9 75% gray
    {255,  0,  0}, // 10
    {255,127,127},
    {204,  0,  0},
    {204,102,102},
    {153,  0,  0},
    {153, 76, 76}, // 15
    {127,  0,  0},
    {127, 63, 63},
    { 76,  0,  0},
    { 76, 38, 38},
    {255, 63,  0}, // 20
    {255,159,127},
    {204, 51,  0},
    {204,127,102},
    {153, 38,  0},
    {153, 95, 76}, // 25
    {127, 31,  0},
    {127, 79, 63},
    { 76, 19,  0},
    { 76, 47, 38},
    {255,127,  0}, // 30
    {255,191,127},
    {204,102,  0},
    {204,153,102},
    {153, 76,  0},
    {153,114, 76}, // 35
    {127, 63,  0},
    {127, 95, 63},
    { 76, 38,  0},
    { 76, 57, 38},
    {255,191,  0}, // 40
    {255,223,127},
    {204,153,  0},
    {204,178,102},
    {153,114,  0},
    {153,133, 76}, // 45
    {127, 95,  0},
    {127,111, 63},
    { 76, 57,  0},
    { 76, 66, 38},
    {255,255,  0}, // 50
    {255,255,127},
    {204,204,  0},
    {204,204,102},
    {153,153,  0},
    {153,153, 76}, // 55
    {127,127,  0},
    {127,127, 63},
    { 76, 76,  0},
    { 76, 76, 38},
    {191,255,  0}, // 60
    {223,255,127},
    {153,204,  0},
    {178,204,102},
    {114,153,  0},
    {133,153, 76}, // 65
    { 95,127,  0},
    {111,127, 63},
    { 57, 76,  0},
    { 66, 76, 38},
    {127,255,  0}, // 70
    {191,255,127},
    {102,204,  0},
    {153,204,102},
    { 76,153,  0},
    {114,153, 76}, // 75
    { 63,127,  0},
    { 95,127, 63},
    { 38, 76,  0},
    { 57, 76, 38},
    { 63,255,  0}, // 80
    {159,255,127},
    { 51,204,  0},
    {127,204,102},
    { 38,153,  0},
    { 95,153, 76}, // 85
    { 31,127,  0},
    { 79,127, 63},
    { 19, 76,  0},
    { 47, 76, 38},
    {  0,255,  0}, // 90
    {127,255,127},
    {  0,204,  0},
    {102,204,102},
    {  0,153,  0},
    { 76,153, 76}, // 95
    {  0,127,  0},
    { 63,127, 63},
    {  0, 76,  0},
    { 38, 76, 38},
    {  0,255, 63}, // 100
    {127,255,159},
    {  0,204, 51},
    {102,204,127},
    {  0,153, 38},
    { 76,153, 95}, // 105
    {  0,127, 31},
    { 63,127, 79},
    {  0, 76, 19},
    { 38, 76, 47},
    {  0,255,127}, // 110
    {127,255,191},
    {  0,204,102},
    {102,204,153},
    {  0,153, 76},
    { 76,153,114}, // 115
    {  0,127, 63},
    { 63,127, 95},
    {  0, 76, 38},
    { 38, 76, 57},
    {  0,255,191}, // 120
    {127,255,223},
    {  0,204,153},
    {102,204,178},
    {  0,153,114},
    { 76,153,133}, // 125
    {  0,127, 95},
    { 63,127,111},
    {  0, 76, 57},
    { 38, 76, 66},
    {  0,255,255}, // 130
    {127,255,255},
    {  0,204,204},
    {102,204,204},
    {  0,153,153},
    { 76,153,153}, // 135
    {  0,127,127},
    { 63,127,127},
    {  0, 76, 76},
    { 38, 76, 76},
    {  0,191,255}, // 140
    {127,223,255},
    {  0,153,204},
    {102,178,204},
    {  0,114,153},
    { 76,133,153}, // 145
    {  0, 95,127},
    { 63,111,127},
    {  0, 57, 76},
    { 38, 66, 76},
    {  0,127,255}, // 150
    {127,191,255},
    {  0,102,204},
    {102,153,204},
    {  0, 76,153},
    { 76,114,153}, // 155
    {  0, 63,127},
    { 63, 95,127},
    {  0, 38, 76},
    { 38, 57, 76},
    {  0, 66,255}, // 160
    {127,159,255},
    {  0, 51,204},
    {102,127,204},
    {  0, 38,153},
    { 76, 95,153}, // 165
    {  0, 31,127},
    { 63, 79,127},
    {  0, 19, 76},
    { 38, 47, 76},
    {  0,  0,255}, // 170
    {127,127,255},
    {  0,  0,204},
    {102,102,204},
    {  0,  0,153},
    { 76, 76,153}, // 175
    {  0,  0,127},
    { 63, 63,127},
    {  0,  0, 76},
    { 38, 38, 76},
    { 63,  0,255}, // 180
    {159,127,255},
    { 50,  0,204},
    {127,102,204},
    { 38,  0,153},
    { 95, 76,153}, // 185
    { 31,  0,127},
    { 79, 63,127},
    { 19,  0, 76},
    { 47, 38, 76},
    {127,  0,255}, // 190
    {191,127,255},
    {102,  0,204},
    {153,102,204},
    { 76,  0,153},
    {114, 76,153}, // 195
    { 63,  0,127},
    { 95, 63,127},
    { 38,  0, 76},
    { 57, 38, 76},
    {191,  0,255}, // 200
    {223,127,255},
    {153,  0,204},
    {178,102,204},
    {114,  0,153},
    {133, 76,153}, // 205
    { 95,  0,127},
    {111, 63,127},
    { 57,  0, 76},
    { 66, 38, 76},
    {255,  0,255}, // 210
    {255,127,255},
    {204,  0,204},
    {204,102,204},
    {153,  0,153},
    {153, 76,153}, // 215
    {127,  0,127},
    {127, 63,127},
    { 76,  0, 76},
    { 76, 38, 76},
    {255,  0,191}, // 220
    {255,127,223},
    {204,  0,153},
    {204,102,178},
    {153,  0,114},
    {153, 76,133}, // 225
    {127,  0, 95},
    {127, 63, 11},
    { 76,  0, 57},
    { 76, 38, 66},
    {255,  0,127}, // 230
    {255,127,191},
    {204,  0,102},
    {204,102,153},
    {153,  0, 76},
    {153, 76,114}, // 235
    {127,  0, 63},
    {127, 63, 95},
    { 76,  0, 38},
    { 76, 38, 57},
    {255,  0, 63}, // 240
    {255,127,159},
    {204,  0, 51},
    {204,102,127},
    {153,  0, 38},
    {153, 76, 95}, // 245
    {127,  0, 31},
    {127, 63, 79},
    { 76,  0, 19},
    { 76, 38, 47},
    { 51, 51, 51}, // 250
    { 91, 91, 91},
    {132,132,132},
    {173,173,173},
    {214,214,214},
    {255,255,255}  // 255
};

}

#endif

// EOF

