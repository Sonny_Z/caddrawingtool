/******************************************************************************
**  FASDXFLIB - Library to read/write DXF files (ascii & binary)              **
******************************************************************************/

#ifndef DXF_BASE_H
#define DXF_BASE_H

#define DXF_VERSION "0.1.0"

#include <string>
#include <list>
#include <cmath>

#define UTF8STRING std::string
#define DXF_UNUSED(x) (void)x

#ifndef M_PI
#define M_PI       3.141592653589793238462643
#endif
#ifndef M_PI_2
#define M_PI_2       1.57079632679489661923
#endif
#define M_PIx2      6.283185307179586 // 2*PI
#define ARAD 57.29577951308232

typedef signed int dint32;              /* 32 bit signed */

typedef unsigned short duint16;         /* 16 bit unsigned */
typedef unsigned int duint32;           /* 32 bit unsigned */

namespace DXF {

//! Version numbers for the DXF Format.
enum Version {
    UNKNOWNV,     // UNKNOWN VERSION. */
    AC1006,       // R10. */
    AC1009,       // R11 & R12. */
    AC1012,       // R13. */
    AC1014,       // R14. */
    AC1015,       // ACAD 2000. */
    AC1018,       // ACAD 2004. */
    AC1021,       // ACAD 2007. */
    AC1024,       // ACAD 2010. */
    AC1027        // ACAD 2013. */
};

//! Special codes for colors
enum ColorCodes {
    ColorByLayer = 256,
    ColorByBlock = 0
};

//! Spaces
enum Space {
    ModelSpace = 0,
    PaperSpace = 1
};

//! Special kinds of handles
enum HandleCodes {
    NoHandle = 0
};

//! Shadow mode
enum ShadowMode {
    CastAndReceieveShadows = 0,
    CastShadows = 1,
    ReceiveShadows = 2,
    IgnoreShadows = 3
};

//! Special kinds of materials
enum MaterialCodes {
    MaterialByLayer = 0
};

//! Special kinds of plot styles
enum PlotStyleCodes {
    DefaultPlotStyle = 0
};

//! Special kinds of transparencies
enum TransparencyCodes {
    Opaque = 0,
    Transparent = -1
};

} // namespace DXF

//Handle 3D coordinate point
class DXF_Coord
{
public:
    DXF_Coord():x(0), y(0),z(0) {}
    DXF_Coord(double ix, double iy, double iz): x(ix), y(iy),z(iz){}

    DXF_Coord& operator = (const DXF_Coord& data)
    {
        x = data.x;  y = data.y;  z = data.z;
        return *this;
    }
    // convert to unitary vector
    void unitize()
    {
        double dist;
        dist = hypot(hypot(x, y), z);
        if (dist > 0.0) {
            x= x/dist;
            y= y/dist;
            z= z/dist;
        }
    }

public:
    double x;
    double y;
    double z;
};


//Handle vertex
class DXF_Vertex2D
{
public:
    DXF_Vertex2D(): x(0), y(0), stawidth(0), endwidth(0), bulge(0){}

    DXF_Vertex2D(double sx, double sy, double b): x(sx), y(sy), stawidth(0), endwidth(0), bulge(b) {}

public:
    double x;                 // x coordinate, code 10 */
    double y;                 // y coordinate, code 20 */
    double stawidth;          // Start width, code 40 */
    double endwidth;          // End width, code 41 */
    double bulge;             // bulge, code 42 */
};


//Handle header vars
class DXF_Variant
{
public:
    enum TYPE
    {
        STRING,
        INTEGER,
        DOUBLE,
        COORD,
        INVALID
    };
    //TODO: add INT64 support
    DXF_Variant(): sdata(std::string()), vdata(), content(0), vType(INVALID), vCode(0) {}

    DXF_Variant(int c, dint32 i): sdata(std::string()), vdata(), content(i), vType(INTEGER), vCode(c){}

    DXF_Variant(int c, duint32 i): sdata(std::string()), vdata(), content(static_cast<dint32>(i)), vType(INTEGER), vCode(c) {}

    DXF_Variant(int c, double d): sdata(std::string()), vdata(), content(d), vType(DOUBLE), vCode(c) {}

    DXF_Variant(int c, UTF8STRING s): sdata(s), vdata(), content(&sdata), vType(STRING), vCode(c) {}

    DXF_Variant(int c, DXF_Coord crd): sdata(std::string()), vdata(crd), content(&vdata), vType(COORD), vCode(c) {}

    DXF_Variant(const DXF_Variant& d): sdata(d.sdata), vdata(d.vdata), content(d.content), vType(d.vType), vCode(d.vCode)
    {
        if (d.vType == COORD)
            content.v = &vdata;
        if (d.vType == STRING)
            content.s = &sdata;
    }

    void addString(int c, UTF8STRING s) {vType = STRING; sdata = s; content.s = &sdata; vCode=c;}
    void addInt(int c, int i) {vType = INTEGER; content.i = i; vCode=c;}
    void addDouble(int c, double d) {vType = DOUBLE; content.d = d; vCode=c;}
    void addCoord(int c, DXF_Coord v) {vType = COORD; vdata = v; content.v = &vdata; vCode=c;}
    void setCoordX(double d) { if (vType == COORD) vdata.x = d;}
    void setCoordY(double d) { if (vType == COORD) vdata.y = d;}
    void setCoordZ(double d) { if (vType == COORD) vdata.z = d;}
    enum TYPE type() { return vType;}
    int code() { return vCode;}            // returns dxf code of this value*/

private:
    std::string sdata;
    DXF_Coord vdata;

private:
    union DXF_VarContent
    {
        UTF8STRING *s;
        dint32 i;
        double d;
        DXF_Coord *v;

        DXF_VarContent(UTF8STRING *sd):s(sd){}
        DXF_VarContent(dint32 id):i(id){}
        DXF_VarContent(double dd):d(dd){}
        DXF_VarContent(DXF_Coord *vd):v(vd){}
    };

public:
    DXF_VarContent content;
private:
    enum TYPE vType;
    int vCode;            // dxf code of this value*/

};

// Class to convert between line width and integer
class DXF_LW_Conv
{
public:
    enum lineWidth
    {
        width00 = 0,       // 0.00mm (dxf 0)*/
        width01 = 1,       // 0.05mm (dxf 5)*/
        width02 = 2,       // 0.09mm (dxf 9)*/
        width03 = 3,       // 0.13mm (dxf 13)*/
        width04 = 4,       // 0.15mm (dxf 15)*/
        width05 = 5,       // 0.18mm (dxf 18)*/
        width06 = 6,       // 0.20mm (dxf 20)*/
        width07 = 7,       // 0.25mm (dxf 25)*/
        width08 = 8,       // 0.30mm (dxf 30)*/
        width09 = 9,       // 0.35mm (dxf 35)*/
        width10 = 10,      // 0.40mm (dxf 40)*/
        width11 = 11,      // 0.50mm (dxf 50)*/
        width12 = 12,      // 0.53mm (dxf 53)*/
        width13 = 13,      // 0.60mm (dxf 60)*/
        width14 = 14,      // 0.70mm (dxf 70)*/
        width15 = 15,      // 0.80mm (dxf 80)*/
        width16 = 16,      // 0.90mm (dxf 90)*/
        width17 = 17,      // 1.00mm (dxf 100)*/
        width18 = 18,      // 1.06mm (dxf 106)*/
        width19 = 19,      // 1.20mm (dxf 120)*/
        width20 = 20,      // 1.40mm (dxf 140)*/
        width21 = 21,      // 1.58mm (dxf 158)*/
        width22 = 22,      // 2.00mm (dxf 200)*/
        width23 = 23,      // 2.11mm (dxf 211)*/
        widthByLayer = 29, // by layer (dxf -1) */
        widthByBlock = 30, // by block (dxf -2) */
        widthDefault = 31  // by default (dxf -3) */
    };

    static int lineWidth2dxfInt(enum lineWidth lw)
    {
        switch (lw)
        {
        case widthByLayer:
            return -1;
        case widthByBlock:
            return -2;
        case widthDefault:
            return -3;
        case width00:
            return 0;
        case width01:
            return 5;
        case width02:
            return 9;
        case width03:
            return 13;
        case width04:
            return 15;
        case width05:
            return 18;
        case width06:
            return 20;
        case width07:
            return 25;
        case width08:
            return 30;
        case width09:
            return 35;
        case width10:
            return 40;
        case width11:
            return 50;
        case width12:
            return 53;
        case width13:
            return 60;
        case width14:
            return 70;
        case width15:
            return 80;
        case width16:
            return 90;
        case width17:
            return 100;
        case width18:
            return 106;
        case width19:
            return 120;
        case width20:
            return 140;
        case width21:
            return 158;
        case width22:
            return 200;
        case width23:
            return 211;
        default:
            break;
        }
        return -3;
    }

    static enum lineWidth dxfInt2lineWidth(int i)
    {
        if (i<0)
        {
            if (i==-1)
                return widthByLayer;
            else if (i==-2)
                return widthByBlock;
            else if (i==-3)
                return widthDefault;
        } else if (i<3) {
            return width00;
        } else if (i<7) {
            return width01;
        } else if (i<11) {
            return width02;
        } else if (i<14) {
            return width03;
        } else if (i<16) {
            return width04;
        } else if (i<19) {
            return width05;
        } else if (i<22) {
            return width06;
        } else if (i<27) {
            return width07;
        } else if (i<32) {
            return width08;
        } else if (i<37) {
            return width09;
        } else if (i<45) {
            return width10;
        } else if (i<52) {
            return width11;
        } else if (i<57) {
            return width12;
        } else if (i<65) {
            return width13;
        } else if (i<75) {
            return width14;
        } else if (i<85) {
            return width15;
        } else if (i<95) {
            return width16;
        } else if (i<103) {
            return width17;
        } else if (i<112) {
            return width18;
        } else if (i<130) {
            return width19;
        } else if (i<149) {
            return width20;
        } else if (i<180) {
            return width21;
        } else if (i<205) {
            return width22;
        } else {
            return width23;
        }
        //default by default
        return widthDefault;
    }
};

#endif

// EOF

