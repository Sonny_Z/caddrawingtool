#ifndef DXF_TEXTCODEC_H
#define DXF_TEXTCODEC_H

#include <string>

class DXF_Converter;

class DXF_TextCodec
{
public:
    DXF_TextCodec();
    ~DXF_TextCodec();
    std::string fromUtf8(std::string s);
    std::string toUtf8(std::string s);
    int getVersion(){return version;}
    void setVersion(std::string *v, bool dxfFormat);
    void setVersion(int v, bool dxfFormat);
    void setCodePage(std::string *c, bool dxfFormat);
    void setCodePage(std::string c, bool dxfFormat){setCodePage(&c, dxfFormat);}
    std::string getCodePage(){return cp;}

private:
    std::string correctCodePage(const std::string& s);

private:
    int version;
    std::string cp;
    DXF_Converter *conv;
};

class DXF_Converter
{
public:
    DXF_Converter(const int *t, int l){table = t;
                               cpLenght = l;}
    virtual ~DXF_Converter(){}
    virtual std::string fromUtf8(std::string *s) {return *s;}
    virtual std::string toUtf8(std::string *s);
    std::string encodeText(std::string stmp);
    std::string decodeText(int c);
    std::string encodeNum(int c);
    int decodeNum(std::string s, int *b);
    const int *table;
    int cpLenght;
};

class DXF_ConvUTF16 : public DXF_Converter {
public:
    DXF_ConvUTF16():DXF_Converter(nullptr, 0) {}
    virtual std::string fromUtf8(std::string *s);
    virtual std::string toUtf8(std::string *s);
};

class DXF_ConvTable : public DXF_Converter {
public:
    DXF_ConvTable(const int *t, int l):DXF_Converter(t, l) {}
    virtual std::string fromUtf8(std::string *s);
    virtual std::string toUtf8(std::string *s);
};

class DXF_ConvDBCSTable : public DXF_Converter {
public:
    DXF_ConvDBCSTable(const int *t,  const int *lt, const int dt[][2], int l):DXF_Converter(t, l) {
        leadTable = lt;
        doubleTable = dt;
    }

    virtual std::string fromUtf8(std::string *s);
    virtual std::string toUtf8(std::string *s);
private:
    const int *leadTable;
    const int (*doubleTable)[2];

};

class DXF_Conv932Table : public DXF_Converter {
public:
    DXF_Conv932Table(const int *t,  const int *lt, const int dt[][2], int l):DXF_Converter(t, l) {
        leadTable = lt;
        doubleTable = dt;
    }

    virtual std::string fromUtf8(std::string *s);
    virtual std::string toUtf8(std::string *s);
private:
    const int *leadTable;
    const int (*doubleTable)[2];

};

#endif // DXF_TEXTCODEC_H
