/******************************************************************************
**  LIBDXFParserLIB - Library to read/write DXF files (ascii & binary)              **
******************************************************************************/

#ifndef LIBDXF_PARSER
#define LIBDXF_PARSER

#include <string>
#include "dxf_entities.h"
#include "dxf_objects.h"
#include "dxf_interface.h"


class dxfReader;

class LIBDXFParser
{
public:
    LIBDXFParser(const char* name);
    ~LIBDXFParser();
    // reads the file specified in constructor
    bool read(DXF_Interface *interface_, bool ext);

private:
    /// used by read() to parse the content of the file
    bool processDxf();
    bool processHeader();
    bool processTables();
    bool processBlocks();
    bool processBlock();
    bool processEntities(bool isblock);
    bool processObjects();

    bool processLType();
    bool processLayer();
    bool processDimStyle();
    bool processTextStyle();
    bool processVports();
    bool processAppId();

    bool processPoint();
    bool processLine();
    bool processRay();
    bool processXline();
    bool processCircle();
    bool processArc();
    bool processEllipse();
    bool processTrace();
    bool processSolid();
    bool processInsert();
    bool processLWPolyline();
    bool processPolyline();
    bool processVertex(DXF_Polyline* pl);
    bool processText();
    bool processMText();
    bool processHatch();
    bool processSpline();
    bool process3dface();
    bool processViewport();
    bool processImage();
    bool processImageDef();
    bool processDimension();
    bool processLeader();
    bool processAttdef();
    bool processAttrib();

private:
    std::string fileName;
    bool binFile;
    dxfReader *reader;
    DXF_Interface *iface;
    DXF_Header header;
    std::string nextentity;
    bool applyExt;
};

#endif // LIBDXF_PARSER
