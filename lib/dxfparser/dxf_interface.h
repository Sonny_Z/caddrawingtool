/******************************************************************************
**  FASDXFLIB - Library to read/write DXF files (ascii & binary)              **
******************************************************************************/

#ifndef DXF_INTERFACE_H
#define DXF_INTERFACE_H

#include <cstring>

#include "dxf_entities.h"
#include "dxf_objects.h"

/* Abstract class (interface) for communicate dxfReader with the application.
 * Inherit your class which takes care of the entities in the 
 * processed DXF file from this interface. */
class DXF_Interface {
public:
    DXF_Interface(){}
    virtual ~DXF_Interface() {}

    // Called when header is parsed.
    virtual void addHeader(const DXF_Header* data) = 0;
    // Called for every line Type.
    virtual void addLType(const DXF_LType& data) = 0;
    // Called for every layer.
    virtual void addLayer(const DXF_Layer& data) = 0;
    // Called for every dim style.
    virtual void addDimStyle(const DXF_Dimstyle& data) = 0;
    // Called for every VPORT table.
    virtual void addVport(const DXF_Vport& data) = 0;
    // Called for every text style.
    virtual void addTextStyle(const DXF_Textstyle& data) = 0;
    // Called for every AppId entry.
    virtual void addAppId(const DXF_AppId& data) = 0;

    // Called for every block. Note: all entities added after this command go into this block until endBlock() is called.
    virtual void addBlock(const DXF_Block& data) = 0;
    // Called to end the current block
    virtual void endBlock() = 0;
    // Called for every point
    virtual void addPoint(const DXF_Point& data) = 0;
    // Called for every line
    virtual void addLine(const DXF_Line& data) = 0;
    // Called for every ray
    virtual void addRay(const DXF_Ray& data) = 0;
    // Called for every xline
    virtual void addXline(const DXF_Xline& data) = 0;
    // Called for every arc
    virtual void addArc(const DXF_Arc& data) = 0;
    // Called for every circle
    virtual void addCircle(const DXF_Circle& data) = 0;
    // Called for every ellipse
    virtual void addEllipse(const DXF_Ellipse& data) = 0;
    // Called for every lwpolyline
    virtual void addLWPolyline(const DXF_LWPolyline& data) = 0;
    // Called for every polyline start
    virtual void addPolyline(const DXF_Polyline& data) = 0;
    // Called for every spline
    virtual void addSpline(const DXF_Spline* data) = 0;
    // Called for every spline knot value
    virtual void addKnot(const DXF_Entity& data) = 0;
    // Called for every insert.
    virtual void addInsert(const DXF_Insert& data) = 0;
    // Called for every trace start
    virtual void addTrace(const DXF_Trace& data) = 0;
    // Called for every 3dface start
    virtual void add3dFace(const DXF_3Dface& data) = 0;
    // Called for every solid start
    virtual void addSolid(const DXF_Solid& data) = 0;
    // Called for every Multi Text entity.
    virtual void addMText(const DXF_MText& data) = 0;
    virtual void addAttdef(const DXF_Attdef& data) = 0;
    virtual void addAttrib(const DXF_Attrib& data) = 0;
    // Called for every Text entity.
    virtual void addText(const DXF_Text& data) = 0;
    // Called for every aligned dimension entity.
    virtual void addDimAlign(const DXF_DimAligned *data) = 0;
    // Called for every linear or rotated dimension entity.
    virtual void addDimLinear(const DXF_DimLinear *data) = 0;
    // Called for every radial dimension entity.
    virtual void addDimRadial(const DXF_DimRadial *data) = 0;
    // Called for every diametric dimension entity.
    virtual void addDimDiametric(const DXF_DimDiametric *data) = 0;
    // Called for every angular dimension (2 lines version) entity.
    virtual void addDimAngular(const DXF_DimAngular *data) = 0;
    // Called for every angular dimension (3 points version) entity.
    virtual void addDimAngular3P(const DXF_DimAngular3p *data) = 0;
    // Called for every ordinate dimension entity.
    virtual void addDimOrdinate(const DXF_DimOrdinate *data) = 0;
    // Called for every leader start.
    virtual void addLeader(const DXF_Leader *data) = 0;
    // Called for every hatch entity.
    virtual void addHatch(const DXF_Hatch *data) = 0;
    // Called for every viewport entity.
    virtual void addViewport(const DXF_Viewport& data) = 0;
    // Called for every image entity.
    virtual void addImage(const DXF_Image *data) = 0;
    // Called for every image definition.
    virtual void linkImage(const DXF_ImageDef *data) = 0;
    // Called for every comment in the DXF file (code 999).
    virtual void addComment(const char* comment) = 0;

    virtual void writeHeader(DXF_Header& data) = 0;
    virtual void writeBlocks() = 0;
    virtual void writeBlockRecords() = 0;
    virtual void writeEntities() = 0;
    virtual void writeLTypes() = 0;
    virtual void writeLayers() = 0;
    virtual void writeTextstyles() = 0;
    virtual void writeVports() = 0;
    virtual void writeDimstyles() = 0;
    virtual void writeAppId() = 0;
};

#endif
