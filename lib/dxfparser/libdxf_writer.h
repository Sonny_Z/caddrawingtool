/******************************************************************************
**  LIBDXFWriterLIB - Library to read/write DXF files (ascii & binary)              **
******************************************************************************/

#ifndef FAS_DXF_H
#define FAS_DXF_H

#include <string>
#include "dxf_entities.h"
#include "dxf_objects.h"
#include "dxf_interface.h"

class dxfWriter;

class LIBDXFWriter {
public:
    LIBDXFWriter(const char* name);
    ~LIBDXFWriter();

    bool write(DXF_Interface *interface_, DXF::Version ver, bool bin);

    bool writeLineType(DXF_LType *ent);
    bool writeLayer(DXF_Layer *ent);
    bool writeDimstyle(DXF_Dimstyle *ent);
    bool writeTextstyle(DXF_Textstyle *ent);
    bool writeVport(DXF_Vport *ent);
    bool writeAppId(DXF_AppId *ent);
    bool writePoint(DXF_Point *ent);
    bool writeLine(DXF_Line *ent);
    bool writeRay(DXF_Ray *ent);
    bool writeXline(DXF_Xline *ent);
    bool writeCircle(DXF_Circle *ent);
    bool writeArc(DXF_Arc *ent);
    bool writeEllipse(DXF_Ellipse *ent);
    bool writeTrace(DXF_Trace *ent);
    bool writeSolid(DXF_Solid *ent);
    bool write3dface(DXF_3Dface *ent);
    bool writeLWPolyline(DXF_LWPolyline *ent);
    bool writePolyline(DXF_Polyline *ent);
    bool writeSpline(DXF_Spline *ent);
    bool writeBlockRecord(std::string name);
    bool writeBlock(DXF_Block *ent);
    bool writeInsert(DXF_Insert *ent);
    bool writeAttdef(DXF_Attdef *ent);
    bool writeAttrib(DXF_Attrib *ent);
    bool writeMText(DXF_MText *ent);
    bool writeText(DXF_Text *ent);
    bool writeHatch(DXF_Hatch *ent);
    bool writeViewport(DXF_Viewport *ent);
    DXF_ImageDef *writeImage(DXF_Image *ent, std::string name);
    bool writeLeader(DXF_Leader *ent);
    bool writeDimension(DXF_Dimension *ent);

private:
    bool writeEntity(DXF_Entity *ent);
    bool writeTables();
    bool writeBlocks();
    bool writeObjects();
    bool writeExtData(const std::vector<DXF_Variant*> &ed);
    std::string toHexStr(int n);

private:
    DXF::Version version;
    std::string fileName;
    bool binFile;
    dxfWriter *writer;
    DXF_Interface *iface;
    int entCount;
    bool wlayer0;
    bool dimstyleStd;
    bool writingBlock;
    int elParts;  /*!< parts munber when convert ellipse to polyline */
    std::map<std::string,int> blockMap;
    std::vector<DXF_ImageDef*> imageDef;  /*!< imageDef list */
    int currHandle;

};

#endif // FAS_DXF_H
