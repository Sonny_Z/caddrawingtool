/******************************************************************************
**  LIBDXFWriterLIB - Library to read/write DXF files (ascii & binary)              **
******************************************************************************/


#include "libdxf_writer.h"

#include <fstream>
#include <algorithm>
#include <sstream>
#include "dxfwriter.h"

#define FIRSTHANDLE 48

LIBDXFWriter::LIBDXFWriter(const char* name)
{
    fileName = name;
    writer = nullptr;
    elParts = 128; //parts munber when convert ellipse to polyline
}
LIBDXFWriter::~LIBDXFWriter()
{
    if (writer != nullptr)
        delete writer;
    for (std::vector<DXF_ImageDef*>::iterator it=imageDef.begin(); it!=imageDef.end(); ++it)
        delete *it;

    imageDef.clear();
}

bool LIBDXFWriter::write(DXF_Interface *interface_, DXF::Version ver, bool bin){
    bool isOk = false;
    std::ofstream filestr;
    version = ver;
    binFile = bin;
    iface = interface_;
    if (binFile)
    {
        filestr.open (fileName.c_str(), std::ios_base::out | std::ios::binary | std::ios::trunc);
        //write sentinel
        filestr << "AutoCAD Binary DXF\r\n" << (char)26 << '\0';
        writer = new dxfWriterBinary(&filestr);
    }
    else
    {
        filestr.open (fileName.c_str(), std::ios_base::out | std::ios::trunc);
        writer = new dxfWriterAscii(&filestr);
        std::string comm = std::string("LIBDXFWriter ") + std::string(DXF_VERSION);
        writer->writeString(999, comm);
    }
    DXF_Header header;
    iface->writeHeader(header);
    writer->writeString(0, "SECTION");
    entCount =FIRSTHANDLE;
    header.write(writer, version);
    writer->writeString(0, "ENDSEC");
    if (ver > DXF::AC1009)
    {
        writer->writeString(0, "SECTION");
        writer->writeString(2, "CLASSES");
        writer->writeString(0, "ENDSEC");
    }
    writer->writeString(0, "SECTION");
    writer->writeString(2, "TABLES");
    writeTables();
    writer->writeString(0, "ENDSEC");
    writer->writeString(0, "SECTION");
    writer->writeString(2, "BLOCKS");
    writeBlocks();
    writer->writeString(0, "ENDSEC");

    writer->writeString(0, "SECTION");
    writer->writeString(2, "ENTITIES");
    iface->writeEntities();
    writer->writeString(0, "ENDSEC");

    if (version > DXF::AC1009)
    {
        writer->writeString(0, "SECTION");
        writer->writeString(2, "OBJECTS");
        writeObjects();
        writer->writeString(0, "ENDSEC");
    }
    writer->writeString(0, "EOF");
    filestr.flush();
    filestr.close();
    isOk = true;
    delete writer;
    writer = nullptr;
    return isOk;
}

bool LIBDXFWriter::writeEntity(DXF_Entity *ent)
{
    ent->handle = ++entCount;
    writer->writeString(5, toHexStr(ent->handle));
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbEntity");
    }
    if (ent->space == 1)
        writer->writeInt16(67, 1);
    if (version > DXF::AC1009)
    {
        writer->writeUtf8String(8, ent->layer);
        writer->writeUtf8String(6, ent->lineType);
    }
    else
    {
        writer->writeUtf8Caps(8, ent->layer);
        writer->writeUtf8Caps(6, ent->lineType);
    }
    writer->writeInt16(62, ent->color);
    if (version > DXF::AC1015 && ent->color24 >= 0)
    {
        writer->writeInt32(420, ent->color24);
    }
    if (version > DXF::AC1014)
    {
        writer->writeInt16(370, DXF_LW_Conv::lineWidth2dxfInt(ent->lWeight));
    }
    return true;
}

bool LIBDXFWriter::writeLineType(DXF_LType *ent)
{
    std::string strname = ent->name;

    transform(strname.begin(), strname.end(), strname.begin(),::toupper);
    //do not write linetypes handled by library
    if (strname == "BYLAYER" || strname == "BYBLOCK" || strname == "CONTINUOUS")
    {
        return true;
    }
    writer->writeString(0, "LTYPE");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, toHexStr(++entCount));
        if (version > DXF::AC1012)
        {
            writer->writeString(330, "5");
        }
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbLinetypeTableRecord");
        writer->writeUtf8String(2, ent->name);
    }
    else
        writer->writeUtf8Caps(2, ent->name);
    writer->writeInt16(70, ent->flags);
    writer->writeUtf8String(3, ent->desc);
    ent->update();
    writer->writeInt16(72, 65);
    writer->writeInt16(73, ent->size);
    writer->writeDouble(40, ent->length);

    for (unsigned int i = 0;  i< ent->path.size(); i++)
    {
        writer->writeDouble(49, ent->path.at(i));
        if (version > DXF::AC1009)
        {
            writer->writeInt16(74, 0);
        }
    }
    return true;
}

bool LIBDXFWriter::writeLayer(DXF_Layer *ent)
{
    writer->writeString(0, "LAYER");
    if (!wlayer0 && ent->name == "0")
    {
        wlayer0 = true;
        if (version > DXF::AC1009)
        {
            writer->writeString(5, "10");
        }
    }
    else
    {
        if (version > DXF::AC1009)
        {
            writer->writeString(5, toHexStr(++entCount));
        }
    }
    if (version > DXF::AC1012)
    {
        writer->writeString(330, "2");
    }
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbLayerTableRecord");
        writer->writeUtf8String(2, ent->name);
    }
    else
    {
        writer->writeUtf8Caps(2, ent->name);
    }
    writer->writeInt16(70, ent->flags);
    writer->writeInt16(62, ent->color);
    if (version > DXF::AC1015 && ent->color24 >= 0)
    {
        writer->writeInt32(420, ent->color24);
    }
    if (version > DXF::AC1009)
    {
        writer->writeUtf8String(6, ent->lineType);
        if (! ent->plotF)
            writer->writeBool(290, ent->plotF);
        writer->writeInt16(370, DXF_LW_Conv::lineWidth2dxfInt(ent->lWeight));
        writer->writeString(390, "F");
    }
    else
        writer->writeUtf8Caps(6, ent->lineType);
    if (!ent->extData.empty())
    {
        writeExtData(ent->extData);
    }
    return true;
}

bool LIBDXFWriter::writeTextstyle(DXF_Textstyle *ent)
{
    writer->writeString(0, "STYLE");
    if (!dimstyleStd)
    {
        std::string name=ent->name;
        transform(name.begin(), name.end(), name.begin(), toupper);
        if (name == "STANDARD")
            dimstyleStd = true;
    }
    if (version > DXF::AC1009)
    {
        writer->writeString(5, toHexStr(++entCount));
    }

    if (version > DXF::AC1012)
    {
        writer->writeString(330, "2");
    }
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbTextStyleTableRecord");
        writer->writeUtf8String(2, ent->name);
    }
    else
    {
        writer->writeUtf8Caps(2, ent->name);
    }
    writer->writeInt16(70, ent->flags);
    writer->writeDouble(40, ent->height);
    writer->writeDouble(41, ent->width);
    writer->writeDouble(50, ent->oblique);
    writer->writeInt16(71, ent->genFlag);
    writer->writeDouble(42, ent->lastHeight);
    if (version > DXF::AC1009)
    {
        writer->writeUtf8String(3, ent->font);
        writer->writeUtf8String(4, ent->bigFont);
        if (ent->fontFamily != 0)
            writer->writeInt32(1071, ent->fontFamily);
    }
    else
    {
        writer->writeUtf8Caps(3, ent->font);
        writer->writeUtf8Caps(4, ent->bigFont);
    }
    return true;
}

bool LIBDXFWriter::writeVport(DXF_Vport *ent)
{
    if (!dimstyleStd)
    {
        ent->name = "*ACTIVE";
        dimstyleStd = true;
    }
    writer->writeString(0, "VPORT");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, toHexStr(++entCount));
        if (version > DXF::AC1012)
            writer->writeString(330, "2");
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbViewportTableRecord");
        writer->writeUtf8String(2, ent->name);
    }
    else
        writer->writeUtf8Caps(2, ent->name);
    writer->writeInt16(70, ent->flags);
    writer->writeDouble(10, ent->lowerLeft.x);
    writer->writeDouble(20, ent->lowerLeft.y);
    writer->writeDouble(11, ent->UpperRight.x);
    writer->writeDouble(21, ent->UpperRight.y);
    writer->writeDouble(12, ent->center.x);
    writer->writeDouble(22, ent->center.y);
    writer->writeDouble(13, ent->snapBase.x);
    writer->writeDouble(23, ent->snapBase.y);
    writer->writeDouble(14, ent->snapSpacing.x);
    writer->writeDouble(24, ent->snapSpacing.y);
    writer->writeDouble(15, ent->gridSpacing.x);
    writer->writeDouble(25, ent->gridSpacing.y);
    writer->writeDouble(16, ent->viewDir.x);
    writer->writeDouble(26, ent->viewDir.y);
    writer->writeDouble(36, ent->viewDir.z);
    writer->writeDouble(17, ent->viewTarget.z);
    writer->writeDouble(27, ent->viewTarget.z);
    writer->writeDouble(37, ent->viewTarget.z);
    writer->writeDouble(40, ent->height);
    writer->writeDouble(41, ent->ratio);
    writer->writeDouble(42, ent->lensHeight);
    writer->writeDouble(43, ent->frontClip);
    writer->writeDouble(44, ent->backClip);
    writer->writeDouble(50, ent->snapAngle);
    writer->writeDouble(51, ent->twistAngle);
    writer->writeInt16(71, ent->viewMode);
    writer->writeInt16(72, ent->circleZoom);
    writer->writeInt16(73, ent->fastZoom);
    writer->writeInt16(74, ent->ucsIcon);
    writer->writeInt16(75, ent->snap);
    writer->writeInt16(76, ent->grid);
    writer->writeInt16(77, ent->snapStyle);
    writer->writeInt16(78, ent->snapIsopair);
    if (version > DXF::AC1014)
    {
        writer->writeInt16(281, 0);
        writer->writeInt16(65, 1);
        writer->writeDouble(110, 0.0);
        writer->writeDouble(120, 0.0);
        writer->writeDouble(130, 0.0);
        writer->writeDouble(111, 1.0);
        writer->writeDouble(121, 0.0);
        writer->writeDouble(131, 0.0);
        writer->writeDouble(112, 0.0);
        writer->writeDouble(122, 1.0);
        writer->writeDouble(132, 0.0);
        writer->writeInt16(79, 0);
        writer->writeDouble(146, 0.0);
        if (version > DXF::AC1018)
        {
            writer->writeString(348, "10020");
            writer->writeInt16(60, ent->gridBehavior);//v2007 undocummented see DXF_Vport class
            writer->writeInt16(61, 5);
            writer->writeBool(292, 1);
            writer->writeInt16(282, 1);
            writer->writeDouble(141, 0.0);
            writer->writeDouble(142, 0.0);
            writer->writeInt16(63, 250);
            writer->writeInt32(421, 3358443);
        }
    }
    return true;
}

bool LIBDXFWriter::writeDimstyle(DXF_Dimstyle *ent)
{
    writer->writeString(0, "DIMSTYLE");
    if (!dimstyleStd)
    {
        std::string name = ent->name;
        std::transform(name.begin(), name.end(), name.begin(),::toupper);
        if (name == "STANDARD")
            dimstyleStd = true;
    }
    if (version > DXF::AC1009)
    {
        writer->writeString(105, toHexStr(++entCount));
    }

    if (version > DXF::AC1012)
    {
        writer->writeString(330, "A");
    }
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbDimStyleTableRecord");
        writer->writeUtf8String(2, ent->name);
    }
    else
        writer->writeUtf8Caps(2, ent->name);
    writer->writeInt16(70, ent->flags);
    if ( version == DXF::AC1009 || !(ent->dimpost.empty()) )
        writer->writeUtf8String(3, ent->dimpost);
    if ( version == DXF::AC1009 || !(ent->dimapost.empty()) )
        writer->writeUtf8String(4, ent->dimapost);
    if ( version == DXF::AC1009 || !(ent->dimblk.empty()) )
        writer->writeUtf8String(5, ent->dimblk);
    if ( version == DXF::AC1009 || !(ent->dimblk1.empty()) )
        writer->writeUtf8String(6, ent->dimblk1);
    if ( version == DXF::AC1009 || !(ent->dimblk2.empty()) )
        writer->writeUtf8String(7, ent->dimblk2);
    writer->writeDouble(40, ent->dimscale);
    writer->writeDouble(41, ent->dimasz);
    writer->writeDouble(42, ent->dimexo);
    writer->writeDouble(43, ent->dimdli);
    writer->writeDouble(44, ent->dimexe);
    writer->writeDouble(45, ent->dimrnd);
    writer->writeDouble(46, ent->dimdle);
    writer->writeDouble(47, ent->dimtp);
    writer->writeDouble(48, ent->dimtm);
    if ( version > DXF::AC1018 || ent->dimfxl !=0 )
        writer->writeDouble(49, ent->dimfxl);
    writer->writeDouble(140, ent->dimtxt);
    writer->writeDouble(141, ent->dimcen);
    writer->writeDouble(142, ent->dimtsz);
    writer->writeDouble(143, ent->dimaltf);
    writer->writeDouble(144, ent->dimlfac);
    writer->writeDouble(145, ent->dimtvp);
    writer->writeDouble(146, ent->dimtfac);
    writer->writeDouble(147, ent->dimgap);
    if (version > DXF::AC1014)
    {
        writer->writeDouble(148, ent->dimaltrnd);
    }
    writer->writeInt16(71, ent->dimtol);
    writer->writeInt16(72, ent->dimlim);
    writer->writeInt16(73, ent->dimtih);
    writer->writeInt16(74, ent->dimtoh);
    writer->writeInt16(75, ent->dimse1);
    writer->writeInt16(76, ent->dimse2);
    writer->writeInt16(77, ent->dimtad);
    writer->writeInt16(78, ent->dimzin);
    if (version > DXF::AC1014)
    {
        writer->writeInt16(79, ent->dimazin);
    }
    writer->writeInt16(170, ent->dimalt);
    writer->writeInt16(171, ent->dimaltd);
    writer->writeInt16(172, ent->dimtofl);
    writer->writeInt16(173, ent->dimsah);
    writer->writeInt16(174, ent->dimtix);
    writer->writeInt16(175, ent->dimsoxd);
    writer->writeInt16(176, ent->dimclrd);
    writer->writeInt16(177, ent->dimclre);
    writer->writeInt16(178, ent->dimclrt);
    if (version > DXF::AC1014)
    {
        writer->writeInt16(179, ent->dimadec);
    }
    if (version > DXF::AC1009)
    {
        if (version < DXF::AC1015)
            writer->writeInt16(270, ent->dimunit);
        writer->writeInt16(271, ent->dimdec);
        writer->writeInt16(272, ent->dimtdec);
        writer->writeInt16(273, ent->dimaltu);
        writer->writeInt16(274, ent->dimalttd);
        writer->writeInt16(275, ent->dimaunit);
    }
    if (version > DXF::AC1014)
    {
        writer->writeInt16(276, ent->dimfrac);
        writer->writeInt16(277, ent->dimlunit);
        writer->writeInt16(278, ent->dimdsep);
        writer->writeInt16(279, ent->dimtmove);
    }
    if (version > DXF::AC1009)
    {
        writer->writeInt16(280, ent->dimjust);
        writer->writeInt16(281, ent->dimsd1);
        writer->writeInt16(282, ent->dimsd2);
        writer->writeInt16(283, ent->dimtolj);
        writer->writeInt16(284, ent->dimtzin);
        writer->writeInt16(285, ent->dimaltz);
        writer->writeInt16(286, ent->dimaltttz);
        if (version < DXF::AC1015)
            writer->writeInt16(287, ent->dimfit);
        writer->writeInt16(288, ent->dimupt);
    }
    if (version > DXF::AC1014)
    {
        writer->writeInt16(289, ent->dimatfit);
    }
    if ( version > DXF::AC1018 && ent->dimfxlon !=0 )
        writer->writeInt16(290, ent->dimfxlon);
    if (version > DXF::AC1009)
    {
        writer->writeUtf8String(340, ent->dimtxsty);
    }
    if (version > DXF::AC1014)
    {
        writer->writeUtf8String(341, ent->dimldrblk);
        writer->writeInt16(371, ent->dimlwd);
        writer->writeInt16(372, ent->dimlwe);
    }
    return true;
}

bool LIBDXFWriter::writeAppId(DXF_AppId *ent)
{
    std::string strname = ent->name;
    transform(strname.begin(), strname.end(), strname.begin(),::toupper);
    //do not write mandatory ACAD appId, handled by library
    if (strname == "ACAD")
        return true;
    writer->writeString(0, "APPID");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, toHexStr(++entCount));
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "9");
        }
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbRegAppTableRecord");
        writer->writeUtf8String(2, ent->name);
    }
    else
    {
        writer->writeUtf8Caps(2, ent->name);
    }
    writer->writeInt16(70, ent->flags);
    return true;
}

bool LIBDXFWriter::writePoint(DXF_Point *ent)
{
    writer->writeString(0, "POINT");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbPoint");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    if (ent->basePoint.z != 0.0)
    {
        writer->writeDouble(30, ent->basePoint.z);
    }
    return true;
}

bool LIBDXFWriter::writeLine(DXF_Line *ent)
{
    writer->writeString(0, "LINE");
    writeEntity(ent);
    if (version > DXF::AC1009) {
        writer->writeString(100, "AcDbLine");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    if (ent->basePoint.z != 0.0 || ent->secPoint.z != 0.0)
    {
        writer->writeDouble(30, ent->basePoint.z);
        writer->writeDouble(11, ent->secPoint.x);
        writer->writeDouble(21, ent->secPoint.y);
        writer->writeDouble(31, ent->secPoint.z);
    }
    else
    {
        writer->writeDouble(11, ent->secPoint.x);
        writer->writeDouble(21, ent->secPoint.y);
    }
    return true;
}

bool LIBDXFWriter::writeRay(DXF_Ray *ent)
{
    writer->writeString(0, "RAY");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbRay");
    }
    DXF_Coord crd = ent->secPoint;
    crd.unitize();
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    if (ent->basePoint.z != 0.0 || ent->secPoint.z != 0.0)
    {
        writer->writeDouble(30, ent->basePoint.z);
        writer->writeDouble(11, crd.x);
        writer->writeDouble(21, crd.y);
        writer->writeDouble(31, crd.z);
    }
    else
    {
        writer->writeDouble(11, crd.x);
        writer->writeDouble(21, crd.y);
    }
    return true;
}

bool LIBDXFWriter::writeXline(DXF_Xline *ent)
{
    writer->writeString(0, "XLINE");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbXline");
    }
    DXF_Coord crd = ent->secPoint;
    crd.unitize();
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    if (ent->basePoint.z != 0.0 || ent->secPoint.z != 0.0)
    {
        writer->writeDouble(30, ent->basePoint.z);
        writer->writeDouble(11, crd.x);
        writer->writeDouble(21, crd.y);
        writer->writeDouble(31, crd.z);
    }
    else
    {
        writer->writeDouble(11, crd.x);
        writer->writeDouble(21, crd.y);
    }
    return true;
}

bool LIBDXFWriter::writeCircle(DXF_Circle *ent)
{
    writer->writeString(0, "CIRCLE");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbCircle");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    if (ent->basePoint.z != 0.0)
    {
        writer->writeDouble(30, ent->basePoint.z);
    }
    writer->writeDouble(40, ent->radious);
    return true;
}

bool LIBDXFWriter::writeArc(DXF_Arc *ent)
{
    writer->writeString(0, "ARC");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbCircle");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    if (ent->basePoint.z != 0.0)
    {
        writer->writeDouble(30, ent->basePoint.z);
    }
    writer->writeDouble(40, ent->radious);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbArc");
    }
    writer->writeDouble(50, ent->staangle*ARAD);
    writer->writeDouble(51, ent->endangle*ARAD);
    return true;
}

bool LIBDXFWriter::writeEllipse(DXF_Ellipse *ent)
{
    //verify axis/ratio and params for full ellipse
    ent->correctAxis();
    if (version > DXF::AC1009)
    {
        writer->writeString(0, "ELLIPSE");
        writeEntity(ent);
        if (version > DXF::AC1009)
        {
            writer->writeString(100, "AcDbEllipse");
        }
        writer->writeDouble(10, ent->basePoint.x);
        writer->writeDouble(20, ent->basePoint.y);
        writer->writeDouble(30, ent->basePoint.z);
        writer->writeDouble(11, ent->secPoint.x);
        writer->writeDouble(21, ent->secPoint.y);
        writer->writeDouble(31, ent->secPoint.z);
        writer->writeDouble(40, ent->ratio);
        writer->writeDouble(41, ent->staparam);
        writer->writeDouble(42, ent->endparam);
    }
    else
    {
        DXF_Polyline pol;
        // copy properties
        ent->toPolyline(&pol, elParts);
        writePolyline(&pol);
    }
    return true;
}

bool LIBDXFWriter::writeTrace(DXF_Trace *ent)
{
    writer->writeString(0, "TRACE");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbTrace");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    writer->writeDouble(30, ent->basePoint.z);
    writer->writeDouble(11, ent->secPoint.x);
    writer->writeDouble(21, ent->secPoint.y);
    writer->writeDouble(31, ent->secPoint.z);
    writer->writeDouble(12, ent->thirdPoint.x);
    writer->writeDouble(22, ent->thirdPoint.y);
    writer->writeDouble(32, ent->thirdPoint.z);
    writer->writeDouble(13, ent->fourPoint.x);
    writer->writeDouble(23, ent->fourPoint.y);
    writer->writeDouble(33, ent->fourPoint.z);
    return true;
}

bool LIBDXFWriter::writeSolid(DXF_Solid *ent){
    writer->writeString(0, "SOLID");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbTrace");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    writer->writeDouble(30, ent->basePoint.z);
    writer->writeDouble(11, ent->secPoint.x);
    writer->writeDouble(21, ent->secPoint.y);
    writer->writeDouble(31, ent->secPoint.z);
    writer->writeDouble(12, ent->thirdPoint.x);
    writer->writeDouble(22, ent->thirdPoint.y);
    writer->writeDouble(32, ent->thirdPoint.z);
    writer->writeDouble(13, ent->fourPoint.x);
    writer->writeDouble(23, ent->fourPoint.y);
    writer->writeDouble(33, ent->fourPoint.z);
    return true;
}

bool LIBDXFWriter::write3dface(DXF_3Dface *ent)
{
    writer->writeString(0, "3DFACE");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbFace");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    writer->writeDouble(30, ent->basePoint.z);
    writer->writeDouble(11, ent->secPoint.x);
    writer->writeDouble(21, ent->secPoint.y);
    writer->writeDouble(31, ent->secPoint.z);
    writer->writeDouble(12, ent->thirdPoint.x);
    writer->writeDouble(22, ent->thirdPoint.y);
    writer->writeDouble(32, ent->thirdPoint.z);
    writer->writeDouble(13, ent->fourPoint.x);
    writer->writeDouble(23, ent->fourPoint.y);
    writer->writeDouble(33, ent->fourPoint.z);
    writer->writeInt16(70, ent->invisibleflag);
    return true;
}

bool LIBDXFWriter::writeLWPolyline(DXF_LWPolyline *ent)
{
    if (version > DXF::AC1009)
    {
        writer->writeString(0, "LWPOLYLINE");
        writeEntity(ent);
        if (version > DXF::AC1009)
        {
            writer->writeString(100, "AcDbPolyline");
        }
        ent->vertexnum = ent->vertlist.size();
        writer->writeInt32(90, ent->vertexnum);
        writer->writeInt16(70, ent->flags);
        writer->writeDouble(43, ent->width);
        if (ent->elevation != 0)
            writer->writeDouble(38, ent->elevation);
        if (ent->thickness != 0)
            writer->writeDouble(39, ent->thickness);
        for (int i = 0;  i< ent->vertexnum; i++){
            auto& v = ent->vertlist.at(i);
            writer->writeDouble(10, v->x);
            writer->writeDouble(20, v->y);
            if (v->stawidth != 0)
                writer->writeDouble(40, v->stawidth);
            if (v->endwidth != 0)
                writer->writeDouble(41, v->endwidth);
            if (v->bulge != 0)
                writer->writeDouble(42, v->bulge);
        }
    }
    else
    {
        // TODO convert lwpolyline in polyline (not exist in acad 12)
    }
    return true;
}

bool LIBDXFWriter::writePolyline(DXF_Polyline *ent)
{
    writer->writeString(0, "POLYLINE");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        if (ent->flags & 8 || ent->flags & 16)
            writer->writeString(100, "AcDb2dPolyline");
        else
            writer->writeString(100, "AcDb3dPolyline");
    }
    else
        writer->writeInt16(66, 1);
    writer->writeDouble(10, 0.0);
    writer->writeDouble(20, 0.0);
    writer->writeDouble(30, ent->basePoint.z);
    if (ent->thickness != 0)
    {
        writer->writeDouble(39, ent->thickness);
    }
    writer->writeInt16(70, ent->flags);
    if (ent->defstawidth != 0)
    {
        writer->writeDouble(40, ent->defstawidth);
    }
    if (ent->defendwidth != 0)
    {
        writer->writeDouble(41, ent->defendwidth);
    }
    if (ent->flags & 16 || ent->flags & 32)
    {
        writer->writeInt16(71, ent->vertexcount);
        writer->writeInt16(72, ent->facecount);
    }
    if (ent->smoothM != 0)
    {
        writer->writeInt16(73, ent->smoothM);
    }
    if (ent->smoothN != 0)
    {
        writer->writeInt16(74, ent->smoothN);
    }
    if (ent->curvetype != 0) {
        writer->writeInt16(75, ent->curvetype);
    }
    DXF_Coord crd  = ent->extDirection;
    if (crd.x != 0 || crd.y != 0 || crd.z != 1)
    {
        writer->writeDouble(210, crd.x);
        writer->writeDouble(220, crd.y);
        writer->writeDouble(230, crd.z);
    }

    int vertexnum = ent->vertlist.size();
    for (int i = 0;  i< vertexnum; i++)
    {
        auto& v = ent->vertlist.at(i);
        writer->writeString(0, "VERTEX");
        writeEntity(ent);
        if (version > DXF::AC1009)
            writer->writeString(100, "AcDbVertex");
        if ( (v->flags & 128) && !(v->flags & 64) )
        {
            writer->writeDouble(10, 0);
            writer->writeDouble(20, 0);
            writer->writeDouble(30, 0);
        }
        else
        {
            writer->writeDouble(10, v->basePoint.x);
            writer->writeDouble(20, v->basePoint.y);
            writer->writeDouble(30, v->basePoint.z);
        }
        if (v->stawidth != 0)
            writer->writeDouble(40, v->stawidth);
        if (v->endwidth != 0)
            writer->writeDouble(41, v->endwidth);
        if (v->bulge != 0)
            writer->writeDouble(42, v->bulge);
        if (v->flags != 0)
        {
            writer->writeInt16(70, ent->flags);
        }
        if (v->flags & 2)
        {
            writer->writeDouble(50, v->tgdir);
        }
        if ( v->flags & 128 )
        {
            if (v->vindex1 != 0)
            {
                writer->writeInt16(71, v->vindex1);
            }
            if (v->vindex2 != 0)
            {
                writer->writeInt16(72, v->vindex2);
            }
            if (v->vindex3 != 0)
            {
                writer->writeInt16(73, v->vindex3);
            }
            if (v->vindex4 != 0)
            {
                writer->writeInt16(74, v->vindex4);
            }
            if ( !(v->flags & 64) )
            {
                writer->writeInt32(91, v->identifier);
            }
        }
    }
    writer->writeString(0, "SEQEND");
    writeEntity(ent);
    return true;
}

bool LIBDXFWriter::writeSpline(DXF_Spline *ent)
{
    if (version > DXF::AC1009)
    {
        writer->writeString(0, "SPLINE");
        writeEntity(ent);
        if (version > DXF::AC1009)
        {
            writer->writeString(100, "AcDbSpline");
        }
        writer->writeDouble(210, ent->normalVec.x);
        writer->writeDouble(220, ent->normalVec.y);
        writer->writeDouble(230, ent->normalVec.z);
        writer->writeInt16(70, ent->flags);
        writer->writeInt16(71, ent->degree);
        writer->writeInt16(72, ent->nknots);
        writer->writeInt16(73, ent->ncontrol);
        writer->writeInt16(74, ent->nfit);
        writer->writeDouble(42, ent->tolknot);
        writer->writeDouble(43, ent->tolcontrol);
        // warning check if nknots are correct and ncontrol
        for (int i = 0;  i< ent->nknots; i++)
        {
            writer->writeDouble(40, ent->knotslist.at(i));
        }
        for (auto const& crd: ent->controllist)
        {
            writer->writeDouble(10, crd->x);
            writer->writeDouble(20, crd->y);
            writer->writeDouble(30, crd->z);
        }
    }
    else
    {
        // TODO convert spline in polyline (not exist in acad 12)
    }
    return true;
}

bool LIBDXFWriter::writeHatch(DXF_Hatch *ent)
{
    if (version > DXF::AC1009)
    {
        writer->writeString(0, "HATCH");
        writeEntity(ent);
        writer->writeString(100, "AcDbHatch");
        writer->writeDouble(10, 0.0);
        writer->writeDouble(20, 0.0);
        writer->writeDouble(30, ent->basePoint.z);
        writer->writeDouble(210, ent->extDirection.x);
        writer->writeDouble(220, ent->extDirection.y);
        writer->writeDouble(230, ent->extDirection.z);
        writer->writeString(2, ent->name);
        writer->writeInt16(70, ent->solid);
        writer->writeInt16(71, ent->associative);
        ent->loopsnum = ent->looplist.size();
        writer->writeInt16(91, ent->loopsnum);
        //write paths data
        for (int i = 0;  i< ent->loopsnum; i++)
        {
            auto const& loop = ent->looplist.at(i);
            writer->writeInt16(92, loop->type);
            if ( (loop->type & 2) == 2)
            {
                // polyline boundary writeme
            }
            else
            {
                //boundary path
                loop->update();
                writer->writeInt16(93, loop->numedges);
                for (int j = 0; j<loop->numedges; ++j)
                {
                    switch ( (loop->objlist.at(j))->eType)
                    {
                    case DXF::LINE:
                    {
                        writer->writeInt16(72, 1);
                        DXF_Line* l = (DXF_Line*)loop->objlist.at(j).get();
                        writer->writeDouble(10, l->basePoint.x);
                        writer->writeDouble(20, l->basePoint.y);
                        writer->writeDouble(11, l->secPoint.x);
                        writer->writeDouble(21, l->secPoint.y);
                        break;
                    }
                    case DXF::ARC:
                    {
                        writer->writeInt16(72, 2);
                        DXF_Arc* a = (DXF_Arc*)loop->objlist.at(j).get();
                        writer->writeDouble(10, a->basePoint.x);
                        writer->writeDouble(20, a->basePoint.y);
                        writer->writeDouble(40, a->radious);
                        writer->writeDouble(50, a->staangle*ARAD);
                        writer->writeDouble(51, a->endangle*ARAD);
                        writer->writeInt16(73, a->isccw);
                        break;
                    }
                    case DXF::ELLIPSE:
                    {
                        writer->writeInt16(72, 3);
                        DXF_Ellipse* a = (DXF_Ellipse*)loop->objlist.at(j).get();
                        a->correctAxis();
                        writer->writeDouble(10, a->basePoint.x);
                        writer->writeDouble(20, a->basePoint.y);
                        writer->writeDouble(11, a->secPoint.x);
                        writer->writeDouble(21, a->secPoint.y);
                        writer->writeDouble(40, a->ratio);
                        writer->writeDouble(50, a->staparam*ARAD);
                        writer->writeDouble(51, a->endparam*ARAD);
                        writer->writeInt16(73, a->isccw);
                        break;
                    }
                    case DXF::SPLINE:
                        // spline boundary writeme
                        //                        writer->writeInt16(72, 4);
                        break;
                    default:
                        break;
                    }
                }
                writer->writeInt16(97, 0);
            }
        }
        writer->writeInt16(75, ent->hstyle);
        writer->writeInt16(76, ent->hpattern);
        if (!ent->solid)
        {
            writer->writeDouble(52, ent->angle);
            writer->writeDouble(41, ent->scale);
            writer->writeInt16(77, ent->doubleflag);
            writer->writeInt16(78, ent->deflines);
        }
        writer->writeInt32(98, 0);
    }
    return true;
}

bool LIBDXFWriter::writeLeader(DXF_Leader *ent)
{
    if (version > DXF::AC1009)
    {
        writer->writeString(0, "LEADER");
        writeEntity(ent);
        writer->writeString(100, "AcDbLeader");
        writer->writeUtf8String(3, ent->style);
        writer->writeInt16(71, ent->arrow);
        writer->writeInt16(72, ent->leadertype);
        writer->writeInt16(73, ent->flag);
        writer->writeInt16(74, ent->hookline);
        writer->writeInt16(75, ent->hookflag);
        writer->writeDouble(40, ent->textheight);
        writer->writeDouble(41, ent->textwidth);
        writer->writeDouble(76, ent->vertnum);
        writer->writeDouble(76, ent->vertexlist.size());
        for (auto const& vert: ent->vertexlist)
        {
            writer->writeDouble(10, vert->x);
            writer->writeDouble(20, vert->y);
            writer->writeDouble(30, vert->z);
        }
    }
    else
    {
    }
    return true;
}
bool LIBDXFWriter::writeDimension(DXF_Dimension *ent)
{
    if (version > DXF::AC1009)
    {
        writer->writeString(0, "DIMENSION");
        writeEntity(ent);
        writer->writeString(100, "AcDbDimension");
        if (!ent->getName().empty()){
            writer->writeString(2, ent->getName());
        }
        writer->writeDouble(10, ent->getDefPoint().x);
        writer->writeDouble(20, ent->getDefPoint().y);
        writer->writeDouble(30, ent->getDefPoint().z);
        writer->writeDouble(11, ent->getTextPoint().x);
        writer->writeDouble(21, ent->getTextPoint().y);
        writer->writeDouble(31, ent->getTextPoint().z);
        if ( !(ent->type & 32))
            ent->type = ent->type +32;
        writer->writeInt16(70, ent->type);
        if ( !(ent->getText().empty()) )
            writer->writeUtf8String(1, ent->getText());
        writer->writeInt16(71, ent->getAlign());
        if ( ent->getTextLineStyle() != 1)
            writer->writeInt16(72, ent->getTextLineStyle());
        if ( ent->getTextLineFactor() != 1)
            writer->writeDouble(41, ent->getTextLineFactor());
        writer->writeUtf8String(3, ent->getStyle());
        if ( ent->getTextLineFactor() != 0)
            writer->writeDouble(53, ent->getDir());
        writer->writeDouble(210, ent->getExtrusion().x);
        writer->writeDouble(220, ent->getExtrusion().y);
        writer->writeDouble(230, ent->getExtrusion().z);

        switch (ent->eType)
        {
        case DXF::DIMALIGNED:
        case DXF::DIMLINEAR:
        {
            DXF_DimAligned * dd = (DXF_DimAligned*)ent;
            writer->writeString(100, "AcDbAlignedDimension");
            DXF_Coord crd = dd->getClonepoint();
            if (crd.x != 0 || crd.y != 0 || crd.z != 0)
            {
                writer->writeDouble(12, crd.x);
                writer->writeDouble(22, crd.y);
                writer->writeDouble(32, crd.z);
            }
            writer->writeDouble(13, dd->getDef1Point().x);
            writer->writeDouble(23, dd->getDef1Point().y);
            writer->writeDouble(33, dd->getDef1Point().z);
            writer->writeDouble(14, dd->getDef2Point().x);
            writer->writeDouble(24, dd->getDef2Point().y);
            writer->writeDouble(34, dd->getDef2Point().z);
            if (ent->eType == DXF::DIMLINEAR)
            {
                DXF_DimLinear * dl = (DXF_DimLinear*)ent;
                if (dl->getAngle() != 0)
                    writer->writeDouble(50, dl->getAngle());
                if (dl->getOblique() != 0)
                    writer->writeDouble(52, dl->getOblique());
                writer->writeString(100, "AcDbRotatedDimension");
            }
            break;
        }
        case DXF::DIMRADIAL:
        {
            DXF_DimRadial * dd = (DXF_DimRadial*)ent;
            writer->writeString(100, "AcDbRadialDimension");
            writer->writeDouble(15, dd->getDiameterPoint().x);
            writer->writeDouble(25, dd->getDiameterPoint().y);
            writer->writeDouble(35, dd->getDiameterPoint().z);
            writer->writeDouble(40, dd->getLeaderLength());
            break;
        }
        case DXF::DIMDIAMETRIC:
        {
            DXF_DimDiametric * dd = (DXF_DimDiametric*)ent;
            writer->writeString(100, "AcDbDiametricDimension");
            writer->writeDouble(15, dd->getDiameter1Point().x);
            writer->writeDouble(25, dd->getDiameter1Point().y);
            writer->writeDouble(35, dd->getDiameter1Point().z);
            writer->writeDouble(40, dd->getLeaderLength());
            break;
        }
        case DXF::DIMANGULAR:
        {
            DXF_DimAngular * dd = (DXF_DimAngular*)ent;
            writer->writeString(100, "AcDb2LineAngularDimension");
            writer->writeDouble(13, dd->getFirstLine1().x);
            writer->writeDouble(23, dd->getFirstLine1().y);
            writer->writeDouble(33, dd->getFirstLine1().z);
            writer->writeDouble(14, dd->getFirstLine2().x);
            writer->writeDouble(24, dd->getFirstLine2().y);
            writer->writeDouble(34, dd->getFirstLine2().z);
            writer->writeDouble(15, dd->getSecondLine1().x);
            writer->writeDouble(25, dd->getSecondLine1().y);
            writer->writeDouble(35, dd->getSecondLine1().z);
            writer->writeDouble(16, dd->getDimPoint().x);
            writer->writeDouble(26, dd->getDimPoint().y);
            writer->writeDouble(36, dd->getDimPoint().z);
            break;
        }
        case DXF::DIMANGULAR3P:
        {
            DXF_DimAngular3p * dd = (DXF_DimAngular3p*)ent;
            writer->writeDouble(13, dd->getFirstLine().x);
            writer->writeDouble(23, dd->getFirstLine().y);
            writer->writeDouble(33, dd->getFirstLine().z);
            writer->writeDouble(14, dd->getSecondLine().x);
            writer->writeDouble(24, dd->getSecondLine().y);
            writer->writeDouble(34, dd->getSecondLine().z);
            writer->writeDouble(15, dd->getVertexPoint().x);
            writer->writeDouble(25, dd->getVertexPoint().y);
            writer->writeDouble(35, dd->getVertexPoint().z);
            break;
        }
        case DXF::DIMORDINATE:
        {
            DXF_DimOrdinate * dd = (DXF_DimOrdinate*)ent;
            writer->writeString(100, "AcDbOrdinateDimension");
            writer->writeDouble(13, dd->getFirstLine().x);
            writer->writeDouble(23, dd->getFirstLine().y);
            writer->writeDouble(33, dd->getFirstLine().z);
            writer->writeDouble(14, dd->getSecondLine().x);
            writer->writeDouble(24, dd->getSecondLine().y);
            writer->writeDouble(34, dd->getSecondLine().z);
            break;
        }
        default:
            break;
        }
    }
    return true;
}

bool LIBDXFWriter::writeInsert(DXF_Insert *ent){
    writer->writeString(0, "INSERT");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbBlockReference");
        writer->writeUtf8String(2, ent->name);
    }
    else
        writer->writeUtf8Caps(2, ent->name);
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    writer->writeDouble(30, ent->basePoint.z);
    writer->writeDouble(41, ent->xscale);
    writer->writeDouble(42, ent->yscale);
    writer->writeDouble(43, ent->zscale);
    writer->writeDouble(50, (ent->angle)*ARAD); //in dxf angle is writed in degrees
    writer->writeInt16(70, ent->colcount);
    writer->writeInt16(71, ent->rowcount);
    writer->writeDouble(44, ent->colspace);
    writer->writeDouble(45, ent->rowspace);
    return true;
}

bool LIBDXFWriter::writeText(DXF_Text *ent)
{
    writer->writeString(0, "TEXT");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbText");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    writer->writeDouble(30, ent->basePoint.z);
    writer->writeDouble(40, ent->height);
    writer->writeUtf8String(1, ent->text);
    writer->writeDouble(50, ent->angle);
    writer->writeDouble(41, ent->widthscale);
    writer->writeDouble(51, ent->oblique);
    if (version > DXF::AC1009)
        writer->writeUtf8String(7, ent->style);
    else
        writer->writeUtf8Caps(7, ent->style);
    writer->writeInt16(71, ent->textgen);
    if (ent->alignH != DXF_Text::HLeft)
    {
        writer->writeInt16(72, ent->alignH);
    }
    if (ent->alignH != DXF_Text::HLeft || ent->alignV != DXF_Text::VBaseLine)
    {
        writer->writeDouble(11, ent->secPoint.x);
        writer->writeDouble(21, ent->secPoint.y);
        writer->writeDouble(31, ent->secPoint.z);
    }
    writer->writeDouble(210, ent->extDirection.x);
    writer->writeDouble(220, ent->extDirection.y);
    writer->writeDouble(230, ent->extDirection.z);
    if (version > DXF::AC1009) {
        writer->writeString(100, "AcDbText");
    }
    if (ent->alignV != DXF_Text::VBaseLine)
    {
        writer->writeInt16(73, ent->alignV);
    }
    return true;
}
bool LIBDXFWriter::writeAttdef(DXF_Attdef *ent)
{
    writer->writeString(0, "TEXT");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbText");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    writer->writeDouble(30, ent->basePoint.z);
    writer->writeDouble(40, ent->height);
    writer->writeUtf8String(1, ent->text);
    writer->writeDouble(50, ent->angle);
    writer->writeDouble(41, ent->widthscale);
    writer->writeDouble(51, ent->oblique);
    if (version > DXF::AC1009)
        writer->writeUtf8String(7, ent->style);
    else
        writer->writeUtf8Caps(7, ent->style);
    writer->writeInt16(71, ent->textgen);
    if (ent->alignH != DXF_Text::HLeft)
    {
        writer->writeInt16(72, ent->alignH);
    }
    if (ent->alignH != DXF_Text::HLeft || ent->alignV != DXF_Text::VBaseLine)
    {
        writer->writeDouble(11, ent->secPoint.x);
        writer->writeDouble(21, ent->secPoint.y);
        writer->writeDouble(31, ent->secPoint.z);
    }
    writer->writeDouble(210, ent->extDirection.x);
    writer->writeDouble(220, ent->extDirection.y);
    writer->writeDouble(230, ent->extDirection.z);
    if (version > DXF::AC1009) {
        writer->writeString(100, "AcDbText");
    }
    if (ent->alignV != DXF_Text::VBaseLine)
    {
        writer->writeInt16(73, ent->alignV);
    }
    return true;
}
bool LIBDXFWriter::writeAttrib(DXF_Attrib *ent)
{
    writer->writeString(0, "TEXT");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbText");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    writer->writeDouble(30, ent->basePoint.z);
    writer->writeDouble(40, ent->height);
    writer->writeUtf8String(1, ent->text);
    writer->writeDouble(50, ent->angle);
    writer->writeDouble(41, ent->widthscale);
    writer->writeDouble(51, ent->oblique);
    if (version > DXF::AC1009)
        writer->writeUtf8String(7, ent->style);
    else
        writer->writeUtf8Caps(7, ent->style);
    writer->writeInt16(71, ent->textgen);
    if (ent->alignH != DXF_Text::HLeft)
    {
        writer->writeInt16(72, ent->alignH);
    }
    if (ent->alignH != DXF_Text::HLeft || ent->alignV != DXF_Text::VBaseLine)
    {
        writer->writeDouble(11, ent->secPoint.x);
        writer->writeDouble(21, ent->secPoint.y);
        writer->writeDouble(31, ent->secPoint.z);
    }
    writer->writeDouble(210, ent->extDirection.x);
    writer->writeDouble(220, ent->extDirection.y);
    writer->writeDouble(230, ent->extDirection.z);
    if (version > DXF::AC1009) {
        writer->writeString(100, "AcDbText");
    }
    if (ent->alignV != DXF_Text::VBaseLine)
    {
        writer->writeInt16(73, ent->alignV);
    }
    return true;
}


bool LIBDXFWriter::writeMText(DXF_MText *ent)
{
    if (version > DXF::AC1009)
    {
        writer->writeString(0, "MTEXT");
        writeEntity(ent);
        writer->writeString(100, "AcDbMText");
        writer->writeDouble(10, ent->basePoint.x);
        writer->writeDouble(20, ent->basePoint.y);
        writer->writeDouble(30, ent->basePoint.z);
        writer->writeDouble(40, ent->height);
        writer->writeDouble(41, ent->widthscale);
        writer->writeInt16(71, ent->textgen);
        writer->writeInt16(72, ent->alignH);
        std::string text = writer->fromUtf8String(ent->text);

        int i;
        for(i =0; (text.size()-i) > 250; )
        {
            writer->writeString(3, text.substr(i, 250));
            i +=250;
        }
        writer->writeString(1, text.substr(i));
        writer->writeString(7, ent->style);
        writer->writeDouble(210, ent->extDirection.x);
        writer->writeDouble(220, ent->extDirection.y);
        writer->writeDouble(230, ent->extDirection.z);
        writer->writeDouble(50, ent->angle);
        writer->writeInt16(73, ent->alignV);
        writer->writeDouble(44, ent->interlin);
    }
    else
    {
        // TODO convert mtext in text lines (not exist in acad 12)
    }
    return true;
}

bool LIBDXFWriter::writeViewport(DXF_Viewport *ent)
{
    writer->writeString(0, "VIEWPORT");
    writeEntity(ent);
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbViewport");
    }
    writer->writeDouble(10, ent->basePoint.x);
    writer->writeDouble(20, ent->basePoint.y);
    if (ent->basePoint.z != 0.0)
        writer->writeDouble(30, ent->basePoint.z);
    writer->writeDouble(40, ent->pswidth);
    writer->writeDouble(41, ent->psheight);
    writer->writeInt16(68, ent->vpstatus);
    writer->writeInt16(69, ent->vpID);
    writer->writeDouble(12, ent->centerPX);// verify if exist in V12
    writer->writeDouble(22, ent->centerPY);// verify if exist in V12
    return true;
}

DXF_ImageDef* LIBDXFWriter::writeImage(DXF_Image *ent, std::string name)
{
    if (version > DXF::AC1009)
    {
        //search if exist imagedef with this mane (image inserted more than 1 time)
        DXF_ImageDef *id = nullptr;
        for (unsigned int i=0; i<imageDef.size(); i++)
        {
            if (imageDef.at(i)->name == name )
            {
                id = imageDef.at(i);
                continue;
            }
        }
        if (id == nullptr)
        {
            id = new DXF_ImageDef();
            imageDef.push_back(id);
            id->handle = ++entCount;
        }
        id->name = name;
        std::string idReactor = toHexStr(++entCount);

        writer->writeString(0, "IMAGE");
        writeEntity(ent);
        writer->writeString(100, "AcDbRasterImage");
        writer->writeDouble(10, ent->basePoint.x);
        writer->writeDouble(20, ent->basePoint.y);
        writer->writeDouble(30, ent->basePoint.z);
        writer->writeDouble(11, ent->secPoint.x);
        writer->writeDouble(21, ent->secPoint.y);
        writer->writeDouble(31, ent->secPoint.z);
        writer->writeDouble(12, ent->vVector.x);
        writer->writeDouble(22, ent->vVector.y);
        writer->writeDouble(32, ent->vVector.z);
        writer->writeDouble(13, ent->sizeu);
        writer->writeDouble(23, ent->sizev);
        writer->writeString(340, toHexStr(id->handle));
        writer->writeInt16(70, 1);
        writer->writeInt16(280, ent->clip);
        writer->writeInt16(281, ent->brightness);
        writer->writeInt16(282, ent->contrast);
        writer->writeInt16(283, ent->fade);
        writer->writeString(360, idReactor);
        id->reactors[idReactor] = toHexStr(ent->handle);
        return id;
    }
    return nullptr; //not exist in acad 12
}

bool LIBDXFWriter::writeBlockRecord(std::string name)
{
    if (version > DXF::AC1009)
    {
        writer->writeString(0, "BLOCK_RECORD");
        writer->writeString(5, toHexStr(++entCount));

        blockMap[name] = entCount;
        entCount = 2+entCount;//reserve 2 for BLOCK & ENDBLOCK
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "1");
        }
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbBlockTableRecord");
        writer->writeUtf8String(2, name);
        if (version > DXF::AC1018)
        {
            //    writer->writeInt16(340, 22);
            writer->writeInt16(70, 0);
            writer->writeInt16(280, 1);
            writer->writeInt16(281, 0);
        }
    }
    return true;
}

bool LIBDXFWriter::writeBlock(DXF_Block *bk)
{
    if (writingBlock)
    {
        writer->writeString(0, "ENDBLK");
        if (version > DXF::AC1009)
        {
            writer->writeString(5, toHexStr(currHandle+2));
            if (version > DXF::AC1014)
            {
                writer->writeString(330, toHexStr(currHandle));
            }
            writer->writeString(100, "AcDbEntity");
        }
        writer->writeString(8, "0");
        if (version > DXF::AC1009)
        {
            writer->writeString(100, "AcDbBlockEnd");
        }
    }
    writingBlock = true;
    writer->writeString(0, "BLOCK");
    if (version > DXF::AC1009)
    {
        currHandle = (*(blockMap.find(bk->name))).second;
        writer->writeString(5, toHexStr(currHandle+1));
        if (version > DXF::AC1014)
        {
            writer->writeString(330, toHexStr(currHandle));
        }
        writer->writeString(100, "AcDbEntity");
    }
    writer->writeString(8, "0");
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbBlockBegin");
        writer->writeUtf8String(2, bk->name);
    }
    else
        writer->writeUtf8Caps(2, bk->name);
    writer->writeInt16(70, bk->flags);
    writer->writeDouble(10, bk->basePoint.x);
    writer->writeDouble(20, bk->basePoint.y);
    if (bk->basePoint.z != 0.0)
    {
        writer->writeDouble(30, bk->basePoint.z);
    }
    if (version > DXF::AC1009)
        writer->writeUtf8String(3, bk->name);
    else
        writer->writeUtf8Caps(3, bk->name);
    writer->writeString(1, "");

    return true;
}

bool LIBDXFWriter::writeTables()
{
    writer->writeString(0, "TABLE");
    writer->writeString(2, "VPORT");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "8");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "0");
        }
        writer->writeString(100, "AcDbSymbolTable");
    }
    writer->writeInt16(70, 1); //end table def
    /*** VPORT ***/
    dimstyleStd =false;
    iface->writeVports();
    if (!dimstyleStd)
    {
        DXF_Vport portact;
        portact.name = "*ACTIVE";
        writeVport(&portact);
    }
    writer->writeString(0, "ENDTAB");
    /*** LTYPE ***/
    writer->writeString(0, "TABLE");
    writer->writeString(2, "LTYPE");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "5");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "0");
        }
        writer->writeString(100, "AcDbSymbolTable");
    }
    writer->writeInt16(70, 4); //end table def
    //Mandatory linetypes
    writer->writeString(0, "LTYPE");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "14");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "5");
        }
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbLinetypeTableRecord");
        writer->writeString(2, "ByBlock");
    }
    else
        writer->writeString(2, "BYBLOCK");
    writer->writeInt16(70, 0);
    writer->writeString(3, "");
    writer->writeInt16(72, 65);
    writer->writeInt16(73, 0);
    writer->writeDouble(40, 0.0);

    writer->writeString(0, "LTYPE");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "15");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "5");
        }
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbLinetypeTableRecord");
        writer->writeString(2, "ByLayer");
    } else
        writer->writeString(2, "BYLAYER");
    writer->writeInt16(70, 0);
    writer->writeString(3, "");
    writer->writeInt16(72, 65);
    writer->writeInt16(73, 0);
    writer->writeDouble(40, 0.0);

    writer->writeString(0, "LTYPE");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "16");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "5");
        }
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbLinetypeTableRecord");
        writer->writeString(2, "Continuous");
    }
    else
    {
        writer->writeString(2, "CONTINUOUS");
    }
    writer->writeInt16(70, 0);
    writer->writeString(3, "Solid line");
    writer->writeInt16(72, 65);
    writer->writeInt16(73, 0);
    writer->writeDouble(40, 0.0);
    //Aplication linetypes
    iface->writeLTypes();
    writer->writeString(0, "ENDTAB");
    /*** LAYER ***/
    writer->writeString(0, "TABLE");
    writer->writeString(2, "LAYER");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "2");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "0");
        }
        writer->writeString(100, "AcDbSymbolTable");
    }
    writer->writeInt16(70, 1); //end table def
    wlayer0 =false;
    iface->writeLayers();
    if (!wlayer0)
    {
        DXF_Layer lay0;
        lay0.name = "0";
        writeLayer(&lay0);
    }
    writer->writeString(0, "ENDTAB");
    /*** STYLE ***/
    writer->writeString(0, "TABLE");
    writer->writeString(2, "STYLE");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "3");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "0");
        }
        writer->writeString(100, "AcDbSymbolTable");
    }
    writer->writeInt16(70, 3); //end table def
    dimstyleStd =false;
    iface->writeTextstyles();
    if (!dimstyleStd)
    {
        DXF_Textstyle tsty;
        tsty.name = "Standard";
        writeTextstyle(&tsty);
    }
    writer->writeString(0, "ENDTAB");

    writer->writeString(0, "TABLE");
    writer->writeString(2, "VIEW");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "6");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "0");
        }
        writer->writeString(100, "AcDbSymbolTable");
    }
    writer->writeInt16(70, 0); //end table def
    writer->writeString(0, "ENDTAB");

    writer->writeString(0, "TABLE");
    writer->writeString(2, "UCS");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "7");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "0");
        }
        writer->writeString(100, "AcDbSymbolTable");
    }
    writer->writeInt16(70, 0); //end table def
    writer->writeString(0, "ENDTAB");

    writer->writeString(0, "TABLE");
    writer->writeString(2, "APPID");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "9");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "0");
        }
        writer->writeString(100, "AcDbSymbolTable");
    }
    writer->writeInt16(70, 1); //end table def
    writer->writeString(0, "APPID");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "12");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "9");
        }
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbRegAppTableRecord");
    }
    writer->writeString(2, "ACAD");
    writer->writeInt16(70, 0);
    iface->writeAppId();
    writer->writeString(0, "ENDTAB");

    writer->writeString(0, "TABLE");
    writer->writeString(2, "DIMSTYLE");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "A");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "0");
        }
        writer->writeString(100, "AcDbSymbolTable");
    }
    writer->writeInt16(70, 1); //end table def
    if (version > DXF::AC1014) {
        writer->writeString(100, "AcDbDimStyleTable");
        writer->writeInt16(71, 1); //end table def
    }
    dimstyleStd =false;
    iface->writeDimstyles();
    if (!dimstyleStd)
    {
        DXF_Dimstyle dsty;
        dsty.name = "Standard";
        writeDimstyle(&dsty);
    }
    writer->writeString(0, "ENDTAB");

    if (version > DXF::AC1009)
    {
        writer->writeString(0, "TABLE");
        writer->writeString(2, "BLOCK_RECORD");
        writer->writeString(5, "1");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "0");
        }
        writer->writeString(100, "AcDbSymbolTable");
        writer->writeInt16(70, 2); //end table def
        writer->writeString(0, "BLOCK_RECORD");
        writer->writeString(5, "1F");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "1");
        }
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbBlockTableRecord");
        writer->writeString(2, "*Model_Space");
        if (version > DXF::AC1018)
        {
            writer->writeInt16(70, 0);
            writer->writeInt16(280, 1);
            writer->writeInt16(281, 0);
        }
        writer->writeString(0, "BLOCK_RECORD");
        writer->writeString(5, "1E");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "1");
        }
        writer->writeString(100, "AcDbSymbolTableRecord");
        writer->writeString(100, "AcDbBlockTableRecord");
        writer->writeString(2, "*Paper_Space");
        if (version > DXF::AC1018)
        {
            writer->writeInt16(70, 0);
            writer->writeInt16(280, 1);
            writer->writeInt16(281, 0);
        }
    }
    /* allways call writeBlockRecords to iface for prepare unnamed blocks */
    iface->writeBlockRecords();
    if (version > DXF::AC1009)
    {
        writer->writeString(0, "ENDTAB");
    }
    return true;
}

bool LIBDXFWriter::writeBlocks()
{
    writer->writeString(0, "BLOCK");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "20");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "1F");
        }
        writer->writeString(100, "AcDbEntity");
    }
    writer->writeString(8, "0");
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbBlockBegin");
        writer->writeString(2, "*Model_Space");
    }
    else
        writer->writeString(2, "$MODEL_SPACE");
    writer->writeInt16(70, 0);
    writer->writeDouble(10, 0.0);
    writer->writeDouble(20, 0.0);
    writer->writeDouble(30, 0.0);
    if (version > DXF::AC1009)
        writer->writeString(3, "*Model_Space");
    else
        writer->writeString(3, "$MODEL_SPACE");
    writer->writeString(1, "");
    writer->writeString(0, "ENDBLK");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "21");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "1F");
        }
        writer->writeString(100, "AcDbEntity");
    }
    writer->writeString(8, "0");
    if (version > DXF::AC1009)
        writer->writeString(100, "AcDbBlockEnd");

    writer->writeString(0, "BLOCK");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "1C");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "1B");
        }
        writer->writeString(100, "AcDbEntity");
    }
    writer->writeString(8, "0");
    if (version > DXF::AC1009)
    {
        writer->writeString(100, "AcDbBlockBegin");
        writer->writeString(2, "*Paper_Space");
    }
    else
        writer->writeString(2, "$PAPER_SPACE");
    writer->writeInt16(70, 0);
    writer->writeDouble(10, 0.0);
    writer->writeDouble(20, 0.0);
    writer->writeDouble(30, 0.0);
    if (version > DXF::AC1009)
        writer->writeString(3, "*Paper_Space");
    else
        writer->writeString(3, "$PAPER_SPACE");
    writer->writeString(1, "");
    writer->writeString(0, "ENDBLK");
    if (version > DXF::AC1009)
    {
        writer->writeString(5, "1D");
        if (version > DXF::AC1014)
        {
            writer->writeString(330, "1F");
        }
        writer->writeString(100, "AcDbEntity");
    }
    writer->writeString(8, "0");
    if (version > DXF::AC1009)
        writer->writeString(100, "AcDbBlockEnd");
    writingBlock = false;
    iface->writeBlocks();
    if (writingBlock)
    {
        writingBlock = false;
        writer->writeString(0, "ENDBLK");
        if (version > DXF::AC1009)
        {
            writer->writeString(5, toHexStr(currHandle+2));
            if (version > DXF::AC1014)
            {
                writer->writeString(330, toHexStr(currHandle));
            }
            writer->writeString(100, "AcDbEntity");
        }
        writer->writeString(8, "0");
        if (version > DXF::AC1009)
            writer->writeString(100, "AcDbBlockEnd");
    }
    return true;
}

bool LIBDXFWriter::writeObjects()
{
    writer->writeString(0, "DICTIONARY");
    std::string imgDictH;
    writer->writeString(5, "C");
    if (version > DXF::AC1014)
    {
        writer->writeString(330, "0");
    }
    writer->writeString(100, "AcDbDictionary");
    writer->writeInt16(281, 1);
    writer->writeString(3, "ACAD_GROUP");
    writer->writeString(350, "D");
    if (imageDef.size() != 0)
    {
        writer->writeString(3, "ACAD_IMAGE_DICT");
        imgDictH = toHexStr(++entCount);
        writer->writeString(350, imgDictH);
    }
    writer->writeString(0, "DICTIONARY");
    writer->writeString(5, "D");
    writer->writeString(330, "C");
    writer->writeString(100, "AcDbDictionary");
    writer->writeInt16(281, 1);
    //write IMAGEDEF_REACTOR
    for (unsigned int i=0; i<imageDef.size(); i++)
    {
        DXF_ImageDef *id = imageDef.at(i);
        std::map<std::string, std::string>::iterator it;
        for ( it=id->reactors.begin() ; it != id->reactors.end(); ++it )
        {
            writer->writeString(0, "IMAGEDEF_REACTOR");
            writer->writeString(5, (*it).first);
            writer->writeString(330, (*it).second);
            writer->writeString(100, "AcDbRasterImageDefReactor");
            writer->writeInt16(90, 2); //version 2=R14 to v2010
            writer->writeString(330, (*it).second);
        }
    }
    if (imageDef.size() != 0)
    {
        writer->writeString(0, "DICTIONARY");
        writer->writeString(5, imgDictH);
        writer->writeString(330, "C");
        writer->writeString(100, "AcDbDictionary");
        writer->writeInt16(281, 1);
        for (unsigned int i=0; i<imageDef.size(); i++)
        {
            size_t f1, f2;
            f1 = imageDef.at(i)->name.find_last_of("/\\");
            f2 =imageDef.at(i)->name.find_last_of('.');
            ++f1;
            writer->writeString(3, imageDef.at(i)->name.substr(f1,f2-f1));
            writer->writeString(350, toHexStr(imageDef.at(i)->handle) );
        }
    }
    for (unsigned int i=0; i<imageDef.size(); i++)
    {
        DXF_ImageDef *id = imageDef.at(i);
        writer->writeString(0, "IMAGEDEF");
        writer->writeString(5, toHexStr(id->handle) );
        if (version > DXF::AC1014) {
            //            writer->writeString(330, "0"); handle to DICTIONARY
        }
        writer->writeString(102, "{ACAD_REACTORS");
        std::map<std::string, std::string>::iterator it;
        for ( it=id->reactors.begin() ; it != id->reactors.end(); ++it )
        {
            writer->writeString(330, (*it).first);
        }
        writer->writeString(102, "}");
        writer->writeString(100, "AcDbRasterImageDef");
        writer->writeInt16(90, 0); //version 0=R14 to v2010
        writer->writeUtf8String(1, id->name);
        writer->writeDouble(10, id->u);
        writer->writeDouble(20, id->v);
        writer->writeDouble(11, id->up);
        writer->writeDouble(21, id->vp);
        writer->writeInt16(280, id->loaded);
        writer->writeInt16(281, id->resolution);
    }
    //no more needed imageDef, delete it
    while (!imageDef.empty())
    {
        imageDef.pop_back();
    }

    return true;
}

bool LIBDXFWriter::writeExtData(const std::vector<DXF_Variant*> &ed)
{
    for (std::vector<DXF_Variant*>::const_iterator it=ed.begin(); it!=ed.end(); ++it)
    {
        switch ((*it)->code())
        {
        case 1000:
        case 1001:
        case 1002:
        case 1003:
        case 1004:
        case 1005:
        {
            int cc = (*it)->code();
            if ((*it)->type() == DXF_Variant::STRING)
                writer->writeUtf8String(cc, *(*it)->content.s);
            break;}
        case 1010:
        case 1011:
        case 1012:
        case 1013:
            if ((*it)->type() == DXF_Variant::COORD)
            {
                writer->writeDouble((*it)->code(), (*it)->content.v->x);
                writer->writeDouble((*it)->code()+10 , (*it)->content.v->y);
                writer->writeDouble((*it)->code()+20 , (*it)->content.v->z);
            }
            break;
        case 1040:
        case 1041:
        case 1042:
            if ((*it)->type() == DXF_Variant::DOUBLE)
                writer->writeDouble((*it)->code(), (*it)->content.d);
            break;
        case 1070:
            if ((*it)->type() == DXF_Variant::INTEGER)
                writer->writeInt16((*it)->code(), (*it)->content.i);
            break;
        case 1071:
            if ((*it)->type() == DXF_Variant::INTEGER)
                writer->writeInt32((*it)->code(), (*it)->content.i);
            break;
        default:
            break;
        }
    }
    return true;
}

// convert a int to string in hex
std::string LIBDXFWriter::toHexStr(int n)
{
    std::ostringstream Convert;
    Convert << std::uppercase << std::hex << n;
    return Convert.str();
}
