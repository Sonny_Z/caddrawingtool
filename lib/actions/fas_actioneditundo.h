/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_ACTIONEDITUNDO_H
#define FAS_ACTIONEDITUNDO_H

#include "fas_actioninterface.h"

// This action class can handle user events for undo / redo.
class FAS_ActionEditUndo : public FAS_ActionInterface
{
    Q_OBJECT
public:
    FAS_ActionEditUndo(bool undo,
                       FAS_EntityContainer& container,
                       FAS_GraphicView& graphicView);

    void init(int status=0) override;
    void trigger() override;

protected:
    // Undo (true) or redo (false)
    bool const undo;
};

#endif
