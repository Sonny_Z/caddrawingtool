/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_PREVIEW_H
#define FAS_PREVIEW_H

#include "fas_entitycontainer.h"

/*
 This class supports previewing. The FAS_Snapper class uses
 an instance of FAS_Preview to preview entities, ranges,  lines, arcs,
 on the fly.
 */
class FAS_Preview : public FAS_EntityContainer
{
public:
    FAS_Preview(FAS_EntityContainer* parent=nullptr);
    ~FAS_Preview() {}
    virtual FAS2::EntityType rtti() const
    {
        return FAS2::EntityPreview;
    }
    virtual void addEntity(FAS_Entity* entity);
    void addCloneOf(FAS_Entity* entity);
    virtual void addSelectionFrom(FAS_EntityContainer& container);
    virtual void addAllFrom(FAS_EntityContainer& container);
    virtual void addStretchablesFrom(FAS_EntityContainer& container,
                                     const FAS_Vector& v1, const FAS_Vector& v2);

private:
    int maxEntities;
};

#endif
