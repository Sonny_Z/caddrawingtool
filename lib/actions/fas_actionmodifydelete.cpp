/**************************************************************************
/** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_actionmodifydelete.h"

//sort with alphabetical order
#include "fas_graphicview.h"
#include "fas_modification.h"
#include <QAction>

FAS_ActionModifyDelete::FAS_ActionModifyDelete(FAS_EntityContainer& container,
                                               FAS_GraphicView& graphicView)
    :FAS_ActionInterface("Delete Entities",
                         container, graphicView)
{
    actionType = FAS2::ActionModifyDelete;
}

void FAS_ActionModifyDelete::init(int status)
{
    FAS_ActionInterface::init(status);
    trigger();
}

void FAS_ActionModifyDelete::trigger()
{
    FAS_Modification m(*container, graphicView);
    m.remove();
    finish(false);
}

void FAS_ActionModifyDelete::updateMouseCursor()
{
    graphicView->setMouseCursor(FAS2::DelCursor);
}

// EOF
