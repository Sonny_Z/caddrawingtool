/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/
#include "fas_actiondefault.h"

//sort with alphabetical order
#include "fas_graphicview.h"
#include "fas_insert.h"
#include "fas_modification.h"
#include "fas_overlaybox.h"
#include "fas_preview.h"
#include "fas_selection.h"
#include <QMouseEvent>

FAS_ActionDefault::FAS_ActionDefault(FAS_EntityContainer& container,
                                     FAS_GraphicView& graphicView)
    : FAS_PreviewActionInterface("Default",
                                 container, graphicView)
    , pPoints(new Points{})
    , restrBak(FAS2::RestrictNothing)
{
    actionType=FAS2::ActionDefault;
}

void FAS_ActionDefault::init(int status)
{
    if(status==Neutral){
        deletePreview();
        deleteSnapper();
    }
    FAS_PreviewActionInterface::init(status);
    pPoints->v1 = pPoints->v2 = {};
}

void FAS_ActionDefault::keyPressEvent(QKeyEvent* e)
{
    switch(e->key())
    {
    case Qt::Key_Shift:
        restrBak = snapMode.restriction;
        setSnapRestriction(FAS2::RestrictNothing);
        keyShiftPressed = true;
        e->accept();
        break; //avoid clearing command line at shift key
    case Qt::Key_Escape:
        deletePreview();
        deleteSnapper();
        setStatus(Neutral);
        e->accept();
    default:
        e->ignore();
    }
}

void FAS_ActionDefault::keyReleaseEvent(QKeyEvent* e)
{
    if (e->key()==Qt::Key_Shift)
    {
        setSnapRestriction(restrBak);
        keyShiftPressed = false;
        e->accept();
    }
}

void FAS_ActionDefault::mouseMoveEvent(QMouseEvent* e)
{
    FAS_Vector mouse = graphicView->toGraph(e->x(), e->y());
    switch (getStatus())
    {
    case Dragging:
        pPoints->v2 = mouse;

        if (graphicView->toGuiDX(pPoints->v1.distanceTo(pPoints->v2))>10)
        {
            // look for reference points to drag:
            double dist;
            FAS_Vector ref = container->getNearestSelectedRef(pPoints->v1, &dist);
            if (ref.valid==true && graphicView->toGuiDX(dist)<8)
            {
                setStatus(MovingRef);
                pPoints->v1 = ref;
                graphicView->moveRelativeZero(pPoints->v1);
            }
            else
            {
                // test for an entity to drag:
                FAS_Entity* en = catchEntity(pPoints->v1);
                if (en && en->isSelected())
                {
                    setStatus(Moving);
                    FAS_Vector vp= en->getNearestRef(pPoints->v1);
                    if(vp.valid) pPoints->v1=vp;
                }
                // no entity found. start area selection:
                else
                {
                    setStatus(SetCorner2);
                }
            }
        }
        break;

    case MovingRef:
        pPoints->v2 = snapPoint(e);
        deletePreview();
        preview->addSelectionFrom(*container);
        preview->moveRef(pPoints->v1, pPoints->v2 - pPoints->v1);
        drawPreview();
        break;

    case Moving:
        pPoints->v2 = snapPoint(e);
        deletePreview();
        preview->addSelectionFrom(*container);
        preview->move(pPoints->v2 - pPoints->v1);
        drawPreview();
        break;

    case SetCorner2:
        if (pPoints->v1.valid)
        {
            pPoints->v2 = mouse;
            deletePreview();
            FAS_OverlayBox* ob = new FAS_OverlayBox(preview.get(),
                                                    FAS_OverlayBoxData(pPoints->v1, pPoints->v2));
            preview->addEntity(ob);
            drawPreview();
        }
        break;
    case Panning:
    {
        FAS_Vector const vTarget(e->x(), e->y());
        FAS_Vector const v01=vTarget - pPoints->v1;
        if(v01.squared()>=64.){
            graphicView->zoomPan((int) v01.x, (int) v01.y);
            pPoints->v1=vTarget;
        }
    }
        break;

    default:
        break;
    }
}

void FAS_ActionDefault::mousePressEvent(QMouseEvent* e)
{
    if (e->button()==Qt::LeftButton)
    {
        switch (getStatus())
        {
        case Neutral:
        {
            auto const m=e->modifiers();
            if(m & (Qt::ControlModifier|Qt::MetaModifier))
            {
                pPoints->v1 = FAS_Vector(e->x(), e->y());
                setStatus(Panning);
            }
            else
            {
                pPoints->v1 = graphicView->toGraph(e->x(), e->y());
                setStatus(Dragging);
            }
        }
            break;

        case Moving:
        {
            pPoints->v2 = snapPoint(e);
            deletePreview();
            FAS_Modification m(*container, graphicView);
            FAS_MoveData data;
            data.number = 0;
            data.useCurrentLayer = false;
            data.useCurrentAttributes = false;
            data.offset = pPoints->v2 - pPoints->v1;
            m.move(data);
            setStatus(Neutral);
            deleteSnapper();
        }
            break;

        case MovingRef:
        {
            pPoints->v2 = snapPoint(e);
            deletePreview();
            FAS_Modification m(*container, graphicView);
            FAS_MoveRefData data;
            data.ref = pPoints->v1;
            data.offset = pPoints->v2 - pPoints->v1;
            m.moveRef(data);
            //container->moveSelectedRef(v1, v2-v2);
            setStatus(Neutral);
        }
            break;

        default:
            break;
        }
    }
    else if (e->button()==Qt::RightButton)
    {
        setStatus(Neutral);
        e->accept();
    }
}



void FAS_ActionDefault::mouseReleaseEvent(QMouseEvent* e)
{
    if (e->button()==Qt::LeftButton)
    {
        pPoints->v2 = graphicView->toGraph(e->x(), e->y());
        switch (getStatus()) {
        case Dragging:
        {
            // select single entity:
            FAS_Entity* en = catchEntity(e);
            if (en)
            {
                deletePreview();
                FAS_Selection s(*container, graphicView);
                if (!keyShiftPressed) //set false when shift key pressed
                {
                    s.deselectAll();
                }
                s.selectSingle(en);
                e->accept();
                setStatus(Neutral);
            }
            else
            {
                setStatus(Neutral);
                FAS_Selection s(*container, graphicView);
                s.deselectAll();
            }
        }
            break;

        case SetCorner2:
        {
            pPoints->v2 = graphicView->toGraph(e->x(), e->y());

            deletePreview();

            bool cross = (pPoints->v1.x > pPoints->v2.x);
            FAS_Selection s(*container, graphicView);
            bool select = (e->modifiers() & Qt::ShiftModifier) ? false : true;
            s.selectWindow(pPoints->v1, pPoints->v2, select, cross);
            setStatus(Neutral);
            e->accept();

            //trig to show a dialog
            emit signalMultiSelectDone();
        }
            break;

        case Panning:
            setStatus(Neutral);
            break;

        default:
            break;
        }
    }
    else if (e->button()==Qt::RightButton)
    {
        //cleanup
        setStatus(Neutral);
        e->accept();
    }
}

void FAS_ActionDefault::updateMouseCursor()
{
    switch (getStatus())
    {
    case Neutral:
        graphicView->setMouseCursor(FAS2::ArrowCursor);
        break;
    case Moving:
    case MovingRef:
        graphicView->setMouseCursor(FAS2::SelectCursor);
        break;
    case Panning:
        graphicView->setMouseCursor(FAS2::ClosedHandCursor);
        break;
    default:
        break;
    }
}
// EOF
