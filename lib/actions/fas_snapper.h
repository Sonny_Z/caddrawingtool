/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_SNAPPER_H
#define FAS_SNAPPER_H

#include <memory>
#include "fas.h"

class FAS_Entity;
class FAS_GraphicView;
class FAS_Vector;
class QMouseEvent;
class FAS_EntityContainer;

// This class holds information on how to snap the mouse.
struct FAS_SnapMode
{
    bool snapFree= false;     /// Whether to snap freely
    bool snapEndpoint= false;     /// Whether to snap to endpoints or not.
    bool snapMiddle= false;       /// Whether to snap to midpoints or not.
    bool snapDistance= false;       /// Whether to snap to distance from endpoints or not.
    bool snapCenter= false;       /// Whether to snap to centers or not.
    bool snapIntersection= false; /// Whether to snap to intersections or not.
    bool snapOnEntity= false;     /// Whether to snap to entities or not.
    FAS2::SnapRestriction restriction= FAS2::RestrictNothing; /// The restriction on the free snap.
    double distance=5.; /// The distance to snap before defaulting to free snaping.
    // Disable all snapping.
    FAS_SnapMode const & clear(void);
    bool operator == (FAS_SnapMode const& rhs) const;
};

/**
 * This class is used for snapping functions in a graphic view.
 * Actions are usually derrived from this base class if they need
 * to catch entities or snap to coordinates. Use the methods to
 * retrieve a graphic coordinate from a mouse coordinate.
 *
 * Possible snapping functions are described in FAS_SnapMode.
 */
class FAS_Snapper
{
public:
    FAS_Snapper() = delete;
    FAS_Snapper(FAS_EntityContainer& container, FAS_GraphicView& graphicView);
    virtual ~FAS_Snapper();

    void init();
    // stop using snapper
    void finish();

    /*
     Return Pointer to the entity which was the key entity for the
     last successful snapping action. If the snap mode is "end point"
     the key entity is the entity whos end point was caught.
     If the snap mode didn't require an entity (e.g. free, grid) this
     method will return nullptr.
     */
    FAS_Entity* getKeyEntity() const
    {
        return keyEntity;
    }

    // Sets a new snap mode./
    void setSnapMode(const FAS_SnapMode& snapMode);

    FAS_SnapMode const* getSnapMode() const;
    FAS_SnapMode* getSnapMode();

    /** Sets a new snap restriction. */
    void setSnapRestriction(FAS2::SnapRestriction /*snapRes*/)
    {
        //this->snapRes = snapRes;
    }

    // Sets the snap range in pixels for catchEntity().
    void setSnapRange(int r)
    {
        snapRange = r;
    }

    // manually set snapPoint
    FAS_Vector snapPoint(const FAS_Vector& coord, bool setSpot = false);
    FAS_Vector snapPoint(QMouseEvent* e);
    FAS_Vector snapFree(QMouseEvent* e);

    FAS_Vector snapFree(const FAS_Vector& coord);
    FAS_Vector snapEndpoint(const FAS_Vector& coord);
    FAS_Vector snapOnEntity(const FAS_Vector& coord);
    FAS_Vector snapCenter(const FAS_Vector& coord);
    FAS_Vector snapMiddle(const FAS_Vector& coord);
    FAS_Vector snapDist(const FAS_Vector& coord);
    FAS_Vector snapIntersection(const FAS_Vector& coord);

    FAS_Vector restrictOrthogonal(const FAS_Vector& coord);
    FAS_Vector restrictHorizontal(const FAS_Vector& coord);
    FAS_Vector restrictVertical(const FAS_Vector& coord);


    FAS_Entity* catchEntity(const FAS_Vector& pos,
                            FAS2::ResolveLevel level=FAS2::ResolveNone);
    FAS_Entity* catchEntity(QMouseEvent* e,
                            FAS2::ResolveLevel level=FAS2::ResolveNone);
    FAS_Entity* catchEntity(const FAS_Vector& pos, FAS2::EntityType enType,
                            FAS2::ResolveLevel level=FAS2::ResolveNone);
    FAS_Entity* catchEntity(QMouseEvent* e, FAS2::EntityType enType,
                            FAS2::ResolveLevel level=FAS2::ResolveNone);
    FAS_Entity* catchEntity(QMouseEvent* e, const std::initializer_list<FAS2::EntityType>& enTypeList,
                            FAS2::ResolveLevel level=FAS2::ResolveNone);

    // Suspends this snapper while another action takes place.
    virtual void suspend();

    // Resumes this snapper after it has been suspended.
    virtual void resume() {
        drawSnapper();
    }

    void drawSnapper();
    static unsigned int snapModeToInt(const FAS_SnapMode& s);
    static FAS_SnapMode intToSnapMode(unsigned int);

protected:
    void deleteSnapper();
    double getSnapRange() const;
    FAS_EntityContainer* container;
    FAS_GraphicView* graphicView;
    FAS_Entity* keyEntity;
    FAS_SnapMode snapMode;
    /* Snap distance for snaping to points with a
     * given distance from endpoints.*/
    double m_SnapDistance;
    /* Snap to equidistant middle points
     * default to 1, i.e., equidistant to start/end points*/
    int middlePoints;
    // Snap range for catching entities.
    int snapRange;
    bool finished{false};

private:
    struct ImpData;
    std::unique_ptr<ImpData> pImpData;

    struct Indicator;
    Indicator* snap_indicator{nullptr};
};

#endif
//EOF
