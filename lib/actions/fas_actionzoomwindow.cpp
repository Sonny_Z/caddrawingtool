/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_actionzoomwindow.h"

//sort with alphabetical order
#include <cmath>
#include "fas_graphicview.h"
#include "fas_preview.h"
#include <QAction>
#include <QMouseEvent>

struct FAS_ActionZoomWindow::Points
{
    FAS_Vector v1;
    FAS_Vector v2;
};


FAS_ActionZoomWindow::FAS_ActionZoomWindow(FAS_EntityContainer& container,
                                           FAS_GraphicView& graphicView, bool keepAspectRatio)
    : FAS_PreviewActionInterface("Zoom Window",
                                 container, graphicView)
    , pPoints(new Points{})
    , keepAspectRatio(keepAspectRatio)
{
}

FAS_ActionZoomWindow::~FAS_ActionZoomWindow(){}


void FAS_ActionZoomWindow::init(int status)
{
    FAS_PreviewActionInterface::init(status);
    pPoints.reset(new Points{});
}



void FAS_ActionZoomWindow::trigger()
{
    FAS_PreviewActionInterface::trigger();

    if (pPoints->v1.valid && pPoints->v2.valid)
    {
        deletePreview();
        if (graphicView->toGuiDX(pPoints->v1.distanceTo(pPoints->v2))>5)
        {
            graphicView->zoomWindow(pPoints->v1, pPoints->v2, keepAspectRatio);
            init();
        }
    }
}

void FAS_ActionZoomWindow::mouseMoveEvent(QMouseEvent* e)
{
    snapFree(e);
    drawSnapper();
    if (getStatus()==SetSecondCorner && pPoints->v1.valid)
    {
        pPoints->v2 = snapFree(e);
        deletePreview();
        preview->addRectangle(pPoints->v1, pPoints->v2);
        drawPreview();
    }
}

void FAS_ActionZoomWindow::mousePressEvent(QMouseEvent* e)
{
    if (e->button()==Qt::LeftButton)
    {
        switch (getStatus())
        {
        case SetFirstCorner:
            pPoints->v1 = snapFree(e);
            drawSnapper();
            setStatus(SetSecondCorner);
            break;
        default:
            break;
        }
    }
}



void FAS_ActionZoomWindow::mouseReleaseEvent(QMouseEvent* e)
{
    if (e->button()==Qt::RightButton)
    {
        if (getStatus()==SetSecondCorner)
        {
            deletePreview();
        }
        init(getStatus()-1);
    }
    else if (e->button()==Qt::LeftButton)
    {
        if (getStatus()==SetSecondCorner)
        {
            pPoints->v2 = snapFree(e);
            if( fabs(pPoints->v1.x-pPoints->v2.x) < FAS_TOLERANCE
                    || fabs(pPoints->v1.y-pPoints->v2.y) < FAS_TOLERANCE )
            {
                //invalid zoom window
                deletePreview();
                init(getStatus()-1);
            }
            trigger();
        }
    }
}

void FAS_ActionZoomWindow::updateMouseCursor()
{
    graphicView->setMouseCursor(FAS2::MagnifierCursor);
}

// EOF
