/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_PREVIEWACTIONINTERFACE_H
#define FAS_PREVIEWACTIONINTERFACE_H

#include <memory>
#include "fas_actioninterface.h"
#include "fas_preview.h"

/**
 * This is the interface that must be implemented for all
 * action classes which need a preview.
 */
class FAS_PreviewActionInterface : public FAS_ActionInterface
{
public:
    FAS_PreviewActionInterface(const char* name,
                               FAS_EntityContainer& container,
                               FAS_GraphicView& graphicView);
    ~FAS_PreviewActionInterface() override;

    void init(int status=0) override;
    void finish(bool updateTB=true) override;
    void suspend() override;
    void resume() override;
    void trigger() override;
    void drawPreview();
    void deletePreview();

private:

protected:
    // Preview that holds the entities to be previewed.
    std::unique_ptr<FAS_Preview> preview;
    bool hasPreview;//whether preview is in use
};

#endif
