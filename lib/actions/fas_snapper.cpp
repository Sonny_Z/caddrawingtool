/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_snapper.h"

//sort with alphabetical order
#include <cmath>
#include "fas_circle.h"
#include "fas_entitycontainer.h"
#include "fas_graphicview.h"
#include "fas_grid.h"
#include "fas_line.h"
#include "fas_math.h"
#include "fas_overlayline.h"
#include "fas_pen.h"
#include "fas_point.h"
#include <QMouseEvent>

struct FAS_Snapper::Indicator
{
    bool lines_state;
    QString lines_type;
    FAS_Pen lines_pen;

    bool shape_state;
    QString shape_type;
    FAS_Pen shape_pen;
};

struct FAS_Snapper::ImpData
{
    FAS_Vector snapCoord;
    FAS_Vector snapSpot;
};

FAS_Snapper::FAS_Snapper(FAS_EntityContainer& container, FAS_GraphicView& graphicView)
    :container(&container)
    ,graphicView(&graphicView)
    ,pImpData(new ImpData{})
    ,snap_indicator(new Indicator{})
{}

FAS_Snapper::~FAS_Snapper()
{
    delete snap_indicator;
    pImpData.~unique_ptr();
}

// Initialize (called by all constructors)
void FAS_Snapper::init() 
{
    snapMode = graphicView->getDefaultSnapMode();
    keyEntity = nullptr;
    pImpData->snapSpot = FAS_Vector{false};
    pImpData->snapCoord = FAS_Vector{false};
    m_SnapDistance = 1.0;

    snap_indicator->lines_state = 1;
    snap_indicator->lines_type = "Crosshair";
    snap_indicator->shape_state = true;
    snap_indicator->shape_type = "Circle";
    QString snap_color = "snap_indicator";

    snap_indicator->lines_pen = FAS_Pen(FAS_Color(snap_color), FAS2::Width00, FAS2::DashLine2);
    snap_indicator->shape_pen = FAS_Pen(FAS_Color(snap_color), FAS2::Width00, FAS2::SolidLine);
    snap_indicator->shape_pen.setScreenWidth(1);

    snapRange=getSnapRange();
}

void FAS_Snapper::finish()
{
    finished = true;
    deleteSnapper();
}

// Disable all snapping.
FAS_SnapMode const & FAS_SnapMode::clear()
{
    snapFree  = false;
    snapEndpoint = false;
    snapMiddle = false;
    snapDistance = false;
    snapCenter = false;
    snapOnEntity = false;
    snapIntersection = false;

    restriction = FAS2::RestrictNothing;

    return *this;
}

bool FAS_SnapMode::operator ==(FAS_SnapMode const& rhs) const
{
    if ( snapFree != rhs.snapFree) return false;
    if ( snapEndpoint != rhs.snapEndpoint) return false;
    if ( snapMiddle != rhs.snapMiddle) return false;
    if ( snapDistance != rhs.snapDistance) return false;
    if ( snapCenter != rhs.snapCenter) return false;
    if ( snapOnEntity != rhs.snapOnEntity) return false;
    if ( snapIntersection != rhs.snapIntersection) return false;
    if ( restriction != rhs.restriction) return false;
    return true;
}

void FAS_Snapper::setSnapMode(const FAS_SnapMode& snapMode)
{
    this->snapMode = snapMode;
}


FAS_SnapMode const* FAS_Snapper::getSnapMode() const
{
    return &(this->snapMode);
}

FAS_SnapMode* FAS_Snapper::getSnapMode()
{
    return &(this->snapMode);
}

//get current mouse coordinates
FAS_Vector FAS_Snapper::snapFree(QMouseEvent* e)
{
    if (!e) {
        return FAS_Vector(false);
    }
    pImpData->snapSpot=graphicView->toGraph(e->x(), e->y());
    pImpData->snapCoord=pImpData->snapSpot;
    snap_indicator->lines_state=true;
    return pImpData->snapCoord;
}

// Snap to a coordinate in the drawing using the current snap mode.
FAS_Vector FAS_Snapper::snapPoint(QMouseEvent* e)
{
    pImpData->snapSpot = FAS_Vector(false);
    FAS_Vector t(false);

    if (!e)
    {
        return pImpData->snapSpot;
    }

    FAS_Vector mouseCoord = graphicView->toGraph(e->x(), e->y());
    double ds2Min=FAS_MAXDOUBLE*FAS_MAXDOUBLE;

    if (snapMode.snapEndpoint)
    {
        t = snapEndpoint(mouseCoord);
        double ds2=mouseCoord.squaredTo(t);

        if (ds2 < ds2Min)
        {
            ds2Min=ds2;
            pImpData->snapSpot = t;
        }
    }
    if (snapMode.snapCenter)
    {
        t = snapCenter(mouseCoord);
        double ds2=mouseCoord.squaredTo(t);
        if (ds2 < ds2Min)
        {
            ds2Min=ds2;
            pImpData->snapSpot = t;
        }
    }
    if (snapMode.snapMiddle)
    {
        //this is still brutal force
        //todo: accept value from widget QG_SnapMiddleOptions
        t = snapMiddle(mouseCoord);
        double ds2=mouseCoord.squaredTo(t);
        if (ds2 < ds2Min){
            ds2Min=ds2;
            pImpData->snapSpot = t;
        }
    }
    if (snapMode.snapDistance)
    {
        //this is still brutal force
        //todo: accept value from widget QG_SnapDistOptions
        t = snapDist(mouseCoord);
        double ds2=mouseCoord.squaredTo(t);
        if (ds2 < ds2Min)
        {
            ds2Min=ds2;
            pImpData->snapSpot = t;
        }
    }
    if (snapMode.snapIntersection)
    {
        t = snapIntersection(mouseCoord);
        double ds2=mouseCoord.squaredTo(t);
        if (ds2 < ds2Min)
        {
            ds2Min=ds2;
            pImpData->snapSpot = t;
        }
    }

    if (snapMode.snapOnEntity &&
            pImpData->snapSpot.distanceTo(mouseCoord) > snapMode.distance)
    {
        t = snapOnEntity(mouseCoord);
        double ds2=mouseCoord.squaredTo(t);
        if (ds2 < ds2Min)
        {
            ds2Min=ds2;
            pImpData->snapSpot = t;
        }
    }

    if( !pImpData->snapSpot.valid )
    {
        pImpData->snapSpot=mouseCoord; //default to snapFree
    }
    else
    {
        //retreat to snapFree when distance is more than half grid
        if(snapMode.snapFree)
        {
            FAS_Vector const& ds=mouseCoord - pImpData->snapSpot;
            FAS_Vector const& grid=graphicView->getGrid()->getCellVector()*0.5;
            if( fabs(ds.x) > fabs(grid.x) ||  fabs(ds.y) > fabs(grid.y) ) pImpData->snapSpot = mouseCoord;
        }
    }
    //apply restriction
    FAS_Vector rz = graphicView->getRelativeZero();
    FAS_Vector vpv(rz.x, pImpData->snapSpot.y);
    FAS_Vector vph(pImpData->snapSpot.x,rz.y);
    switch (snapMode.restriction)
    {
    case FAS2::RestrictOrthogonal:
        pImpData->snapCoord= ( mouseCoord.distanceTo(vpv)< mouseCoord.distanceTo(vph))?
                    vpv:vph;
        break;
    case FAS2::RestrictHorizontal:
        pImpData->snapCoord = vph;
        break;
    case FAS2::RestrictVertical:
        pImpData->snapCoord = vpv;
        break;

        //case FAS2::RestrictNothing:
    default:
        pImpData->snapCoord = pImpData->snapSpot;
        break;
    }
    snapPoint(pImpData->snapSpot, false);
    return pImpData->snapCoord;
}

//manually set snapPoint
FAS_Vector FAS_Snapper::snapPoint(const FAS_Vector& coord, bool setSpot)
{
    if(coord.valid
            ){
        pImpData->snapSpot=coord;
        if(setSpot) pImpData->snapCoord = coord;
        drawSnapper();
    }
    return coord;
}
double FAS_Snapper::getSnapRange() const
{
    if(graphicView )
        return (graphicView->getGrid()->getCellVector()).magnitude();
    return 20.;
}

// Snaps to a free coordinate.
FAS_Vector FAS_Snapper::snapFree(const FAS_Vector& coord)
{
    keyEntity = nullptr;
    return coord;
}

// Snaps to the closest endpoint.
FAS_Vector FAS_Snapper::snapEndpoint(const FAS_Vector& coord)
{
    FAS_Vector vec(false);

    vec = container->getNearestEndpoint(coord,
                                        nullptr/*, &keyEntity*/);
    return vec;
}

FAS_Vector FAS_Snapper::snapOnEntity(const FAS_Vector& coord)
{
    FAS_Vector vec{};
    vec = container->getNearestPointOnEntity(coord, true, nullptr, &keyEntity);
    return vec;
}

FAS_Vector FAS_Snapper::snapCenter(const FAS_Vector& coord)
{
    FAS_Vector vec{};
    vec = container->getNearestCenter(coord, nullptr);
    return vec;
}

FAS_Vector FAS_Snapper::snapMiddle(const FAS_Vector& coord)
{
    return container->getNearestMiddle(coord,static_cast<double *>(nullptr),middlePoints);
}
FAS_Vector FAS_Snapper::snapDist(const FAS_Vector& coord)
{
    FAS_Vector vec;
    vec = container->getNearestDist(m_SnapDistance,
                                    coord,
                                    nullptr);
    return vec;
}
FAS_Vector FAS_Snapper::snapIntersection(const FAS_Vector& coord)
{
    FAS_Vector vec{};
    vec = container->getNearestIntersection(coord,
                                            nullptr);
    return vec;
}

FAS_Vector FAS_Snapper::restrictOrthogonal(const FAS_Vector& coord)
{
    FAS_Vector rz = graphicView->getRelativeZero();
    FAS_Vector ret(coord);
    FAS_Vector retx = FAS_Vector(rz.x, ret.y);
    FAS_Vector rety = FAS_Vector(ret.x, rz.y);

    if (retx.distanceTo(ret) > rety.distanceTo(ret))
    {
        ret = rety;
    }
    else
    {
        ret = retx;
    }

    return ret;
}
FAS_Vector FAS_Snapper::restrictHorizontal(const FAS_Vector& coord)
{
    FAS_Vector rz = graphicView->getRelativeZero();
    FAS_Vector ret = FAS_Vector(coord.x, rz.y);
    return ret;
}

FAS_Vector FAS_Snapper::restrictVertical(const FAS_Vector& coord)
{
    FAS_Vector rz = graphicView->getRelativeZero();
    FAS_Vector ret = FAS_Vector(rz.x, coord.y);
    return ret;
}

FAS_Entity* FAS_Snapper::catchEntity(const FAS_Vector& pos,
                                     FAS2::ResolveLevel level)
{
    double dist (0.);
    FAS_Entity* entity = container->getNearestEntity(pos, &dist, level);

    int idx = -1;
    if (entity && entity->getParent())
    {
        idx = entity->getParent()->findEntity(entity);
    }

    if (entity && dist <= getSnapRange())
    {
        // highlight:
        return entity;
    }
    else
    {
        return nullptr;
    }
}

// Catches an entity which is close to the given position 'pos'.
FAS_Entity* FAS_Snapper::catchEntity(const FAS_Vector& pos, FAS2::EntityType enType,
                                     FAS2::ResolveLevel level)
{
    // set default distance for points inside solids
    FAS_EntityContainer ec(nullptr,false);
    bool isContainer{false};
    switch(enType)
    {
    case FAS2::EntityPolyline:
    case FAS2::EntityContainer:
    case FAS2::EntitySpline:
        isContainer=true;
        break;
    default:
        break;
    }

    for(FAS_Entity* en= container->firstEntity(level);en;en=container->nextEntity(level))
    {
        if(en->isVisible()==false) continue;
        if(en->rtti() != enType && isContainer)
        {
            //whether this entity is a member of member of the type enType
            FAS_Entity* parent(en->getParent());
            bool matchFound{false};
            while(parent )
            {
                if(parent->rtti() == enType)
                {
                    matchFound=true;
                    ec.addEntity(en);
                    break;
                }
                parent=parent->getParent();
            }
            if(!matchFound)
                continue;
        }
        if (en->rtti() == enType)
        {
            ec.addEntity(en);
        }
    }
    if (ec.count() == 0 )
        return nullptr;
    double dist(0.);

    FAS_Entity* entity = ec.getNearestEntity(pos, &dist, FAS2::ResolveNone);

    int idx = -1;
    if (entity && entity->getParent())
    {
        idx = entity->getParent()->findEntity(entity);
    }

    if (entity && dist<=getSnapRange())
    {
        // highlight:
        return entity;
    }
    else
    {
        return nullptr;
    }
}

// Catches an entity which is close to the mouse cursor.
FAS_Entity* FAS_Snapper::catchEntity(QMouseEvent* e,
                                     FAS2::ResolveLevel level)
{
    return catchEntity(
                FAS_Vector(graphicView->toGraphX(e->x()),
                           graphicView->toGraphY(e->y())),
                level);
}


// Catches an entity which is close to the mouse cursor.
FAS_Entity* FAS_Snapper::catchEntity(QMouseEvent* e, FAS2::EntityType enType,
                                     FAS2::ResolveLevel level)
{
    return catchEntity(
    {graphicView->toGraphX(e->x()), graphicView->toGraphY(e->y())},
                enType,
                level);
}

FAS_Entity* FAS_Snapper::catchEntity(QMouseEvent* e, const std::initializer_list<FAS2::EntityType>& enTypeList,
                                     FAS2::ResolveLevel level)
{
    FAS_Entity* pten = nullptr;
    FAS_Vector coord{graphicView->toGraphX(e->x()), graphicView->toGraphY(e->y())};
    switch(enTypeList.size())
    {
    case 0:
        return catchEntity(coord, level);
    default:
    {
        FAS_EntityContainer ec(nullptr,false);
        for( auto t0: enTypeList)
        {
            FAS_Entity* en=catchEntity(coord, t0, level);
            if(en)
                ec.addEntity(en);
        }
        if(ec.count()>0){
            ec.getDistanceToPoint(coord, &pten, FAS2::ResolveNone);
            return pten;
        }
    }
    }
    return nullptr;
}

void FAS_Snapper::suspend()
{
    pImpData->snapSpot = pImpData->snapCoord = FAS_Vector{false};
}

// Deletes the snapper from the screen.
void FAS_Snapper::deleteSnapper()
{
    graphicView->getOverlayContainer(FAS2::Snapper)->clear();
    graphicView->redraw(FAS2::RedrawOverlay); // redraw will happen in the mouse movement event
}

// creates the snap indicator
void FAS_Snapper::drawSnapper()
{
    graphicView->getOverlayContainer(FAS2::Snapper)->clear();
    /*
    if (!finished && pImpData->snapSpot.valid)
    {
        FAS_EntityContainer *container=graphicView->getOverlayContainer(FAS2::Snapper);

        if (snap_indicator->lines_state)
        {
            QString type = snap_indicator->lines_type;

            if (type == "Crosshair")
            {
                FAS_OverlayLine *line = new FAS_OverlayLine(nullptr,
                {{0., graphicView->toGuiY(pImpData->snapCoord.y)},
                 {double(graphicView->getWidth()),
                  graphicView->toGuiY(pImpData->snapCoord.y)}});

                line->setPen(snap_indicator->lines_pen);
                container->addEntity(line);

                line = new FAS_OverlayLine(nullptr,
                {{graphicView->toGuiX(pImpData->snapCoord.x),0.},
                 {graphicView->toGuiX(pImpData->snapCoord.x),
                  double(graphicView->getHeight())}});

                line->setPen(snap_indicator->lines_pen);
                container->addEntity(line);
            }

            else if (type == "Isometric")
            {
                //isometric crosshair
                FAS2::CrosshairType chType=graphicView->getCrosshairType();
                FAS_Vector direction1;
                FAS_Vector direction2(0.,1.);
                double l=graphicView->getWidth()+graphicView->getHeight();
                switch(chType){
                case FAS2::RightCrosshair:
                    direction1=FAS_Vector(M_PI*5./6.)*l;
                    direction2*=l;
                    break;
                case FAS2::LeftCrosshair:
                    direction1=FAS_Vector(M_PI*1./6.)*l;
                    direction2*=l;
                    break;
                default:
                    direction1=FAS_Vector(M_PI*1./6.)*l;
                    direction2=FAS_Vector(M_PI*5./6.)*l;
                }
                FAS_Vector center(graphicView->toGui(pImpData->snapCoord));
                FAS_OverlayLine *line=new FAS_OverlayLine(container,
                {center-direction1,center+direction1});
                line->setPen(snap_indicator->lines_pen);
                container->addEntity(line);
                line=new FAS_OverlayLine(nullptr,
                {center-direction2,center+direction2});
                line->setPen(snap_indicator->lines_pen);
                container->addEntity(line);
            }
            else if (type == "Spiderweb")
            {
                FAS_OverlayLine* line;
                FAS_Vector point1;
                FAS_Vector point2;

                point1 = FAS_Vector{0, 0};
                point2 = FAS_Vector{graphicView->toGuiX(pImpData->snapCoord.x),
                        graphicView->toGuiY(pImpData->snapCoord.y)};
                line=new FAS_OverlayLine{nullptr, {point1, point2}};
                line->setPen(snap_indicator->lines_pen);
                container->addEntity(line);

                point1 = FAS_Vector(0, graphicView->getHeight());
                line = new FAS_OverlayLine{nullptr, {point1, point2}};
                line->setPen(snap_indicator->lines_pen);
                container->addEntity(line);

                point1 = FAS_Vector(graphicView->getWidth(), 0);
                line = new FAS_OverlayLine(nullptr, {point1, point2});
                line->setPen(snap_indicator->lines_pen);
                container->addEntity(line);

                point1 = FAS_Vector(graphicView->getWidth(), graphicView->getHeight());
                line = new FAS_OverlayLine(nullptr, {point1, point2});
                line->setPen(snap_indicator->lines_pen);
                container->addEntity(line);
            }
        }
        if (snap_indicator->shape_state)
        {
            QString type = snap_indicator->shape_type;

            if (type == "Circle")
            {
                FAS_Circle *circle=new FAS_Circle(container,
                {pImpData->snapCoord, 4./graphicView->getFactor().x});
                circle->setPen(snap_indicator->shape_pen);
                container->addEntity(circle);
            }
            else if (type == "Point")
            {
                FAS_Point *point=new FAS_Point(container, pImpData->snapCoord);
                point->setPen(snap_indicator->shape_pen);
                container->addEntity(point);
            }
            else if (type == "Square")
            {
                FAS_Vector snap_point{graphicView->toGuiX(pImpData->snapCoord.x),
                            graphicView->toGuiY(pImpData->snapCoord.y)};

                double a = 6.0;
                FAS_Vector p1 = snap_point + FAS_Vector(-a, a);
                FAS_Vector p2 = snap_point + FAS_Vector(a, a);
                FAS_Vector p3 = snap_point + FAS_Vector(a, -a);
                FAS_Vector p4 = snap_point + FAS_Vector(-a, -a);

                FAS_OverlayLine* line;
                line=new FAS_OverlayLine{nullptr, {p1, p2}};
                line->setPen(snap_indicator->shape_pen);
                container->addEntity(line);

                line = new FAS_OverlayLine{nullptr, {p2, p3}};
                line->setPen(snap_indicator->shape_pen);
                container->addEntity(line);

                line = new FAS_OverlayLine(nullptr, {p3, p4});
                line->setPen(snap_indicator->shape_pen);
                container->addEntity(line);

                line = new FAS_OverlayLine(nullptr, {p4, p1});
                line->setPen(snap_indicator->shape_pen);
                container->addEntity(line);
            }
        }
        graphicView->redraw(FAS2::RedrawOverlay); // redraw will happen in the mouse movement event
    }*/
}

// snap mode to a flag integer
unsigned int FAS_Snapper::snapModeToInt(const FAS_SnapMode& s)
{
    unsigned int ret; //initial
    switch (s.restriction) {
    case FAS2::RestrictHorizontal:
        ret=1;
        break;
    case FAS2::RestrictVertical:
        ret=2;
        break;
    case FAS2::RestrictOrthogonal:
        ret=3;
        break;
    default:
        ret=0;
    }
    ret <<=1;ret |= s.snapFree;
    ret <<=1;ret |= s.snapEndpoint;
    ret <<=1;ret |= s.snapMiddle;
    ret <<=1;ret |= s.snapDistance;
    ret <<=1;ret |= s.snapCenter;
    ret <<=1;ret |= s.snapOnEntity;
    ret <<=1;ret |= s.snapIntersection;
    return ret;
}
// integer flag to snapMode
FAS_SnapMode FAS_Snapper::intToSnapMode(unsigned int ret)
{
    FAS_SnapMode s; //initial
    unsigned int binaryOne(0x1);
    s.snapIntersection = ret & binaryOne;
    ret >>= 1;
    s.snapOnEntity = ret & binaryOne;
    ret >>= 1;
    s.snapCenter = ret & binaryOne;
    ret >>= 1;
    s.snapDistance = ret & binaryOne;
    ret >>= 1;
    s.snapMiddle = ret & binaryOne;
    ret >>= 1;
    s.snapEndpoint = ret & binaryOne;
    ret >>= 1;
    s.snapFree = ret & binaryOne;
    ret >>= 1;
    switch (ret) {
    case 1:
        s.restriction=FAS2::RestrictHorizontal;
        break;
    case 2:
        s.restriction=FAS2::RestrictVertical;
        break;
    case 3:
        s.restriction=FAS2::RestrictOrthogonal;
        break;
    default:
        s.restriction=FAS2::RestrictNothing;
    }
    return s;
}
