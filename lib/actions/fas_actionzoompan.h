/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_ACTIONZOOMPAN_H
#define FAS_ACTIONZOOMPAN_H

#include "fas_actioninterface.h"


// This action class can handle user events to zoom in a window.
class FAS_ActionZoomPan : public FAS_ActionInterface {
    Q_OBJECT
public:
    // Action States.
    enum Status {
        SetPanStart,   // Setting Start.
        SetPanning,     // Setting panning.
        SetPanEnd,      // Setting End
    };

public:
    FAS_ActionZoomPan(FAS_EntityContainer& container,
                      FAS_GraphicView& graphicView);

    void init(int status=0) override;
    void trigger() override;
    void mouseMoveEvent(QMouseEvent* e) override;
    void mousePressEvent(QMouseEvent* e) override;
    void mouseReleaseEvent(QMouseEvent* e) override;
    void updateMouseCursor() override;

protected:
    int x1;
    int y1;
    int x2;
    int y2;
};

#endif
