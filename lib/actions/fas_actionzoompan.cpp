/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_actionzoompan.h"

//sort with alphabetical order
#include "fas_graphicview.h"
#include <QAction>
#include <QMouseEvent>

FAS_ActionZoomPan::FAS_ActionZoomPan(FAS_EntityContainer& container,
                                     FAS_GraphicView& graphicView)
    :FAS_ActionInterface("Zoom Panning", container, graphicView) {}

void FAS_ActionZoomPan::init(int status)
{
    FAS_ActionInterface::init(status);
    snapMode.clear();
    snapMode.restriction = FAS2::RestrictNothing;
    x1 = y1 = x2 = y2 = -1;
    setStatus(SetPanStart);
}

void FAS_ActionZoomPan::trigger()
{
    if (getStatus()==SetPanning && (abs(x2-x1)>7 || abs(y2-y1)>7))
    {
        graphicView->zoomPan(x2-x1, y2-y1);
        x1 = x2;
        y1 = y2;
    }
    if(getStatus()==SetPanEnd) finish(false);
}

void FAS_ActionZoomPan::mouseMoveEvent(QMouseEvent* e)
{
    x2 = e->x();
    y2 = e->y();
    if (getStatus() == SetPanning )
    {
        if (abs(x2-x1)>7 || abs(y2-y1)>7)
        {
            trigger();
        }
    }
}

void FAS_ActionZoomPan::mousePressEvent(QMouseEvent* e) {
    if (e->button()==Qt::MiddleButton || e->button()==Qt::LeftButton)
    {
        x1 = e->x();
        y1 = e->y();
        setStatus(SetPanning);
    }
}

void FAS_ActionZoomPan::mouseReleaseEvent(QMouseEvent* /*e*/)
{
    setStatus(SetPanEnd);
    trigger();
}


void FAS_ActionZoomPan::updateMouseCursor()
{
    switch (getStatus()){
    case SetPanStart:
        graphicView->setMouseCursor(FAS2::OpenHandCursor);
        break;
    case SetPanning:
        graphicView->setMouseCursor(FAS2::ClosedHandCursor);
        break;
    default:
        break;
    }
}

// EOF
