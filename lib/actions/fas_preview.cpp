/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_preview.h"

//sort with alphabetical order
#include "fas_entitycontainer.h"
#include "fas_graphicview.h"
#include "fas_information.h"
#include "fas_line.h"

FAS_Preview::FAS_Preview(FAS_EntityContainer* parent)
    : FAS_EntityContainer(parent, true)
{
    maxEntities = 100;
}

/*
 * Adds an entity to this preview and removes any attributes / layer
 * connectsions before that.
 */
void FAS_Preview::addEntity(FAS_Entity* entity) {
    if (!entity || entity->isUndone()) {
        return;
    }

    bool addBorder = false;

    if (entity->rtti()==FAS2::EntityImage || entity->rtti()==FAS2::EntityHatch ||
            entity->rtti()==FAS2::EntityInsert)
    {
        addBorder = true;
    }
    else
    {
        if (entity->isContainer() && entity->rtti()!=FAS2::EntitySpline)
        {
            if (entity->countDeep() > maxEntities-countDeep())
            {
                addBorder = true;
            }
        }
    }

    if (addBorder)
    {
        FAS_Vector min = entity->getMin();
        FAS_Vector max = entity->getMax();

        FAS_Line* l1 =
                new FAS_Line(this, {min.x, min.y}, {max.x, min.y});
        FAS_Line* l2 =
                new FAS_Line(this, {max.x, min.y}, {max.x, max.y});
        FAS_Line* l3 =
                new FAS_Line(this, {max.x, max.y}, {min.x, max.y});
        FAS_Line* l4 =
                new FAS_Line(this, {min.x, max.y}, {min.x, min.y});

        FAS_EntityContainer::addEntity(l1);
        FAS_EntityContainer::addEntity(l2);
        FAS_EntityContainer::addEntity(l3);
        FAS_EntityContainer::addEntity(l4);

        delete entity;
        entity = nullptr;
    }
    else
    {
        entity->setLayer(nullptr);
        entity->setSelected(false);
        entity->reparent(this);
        FAS_EntityContainer::addEntity(entity);
    }
}

// Clones the given entity and adds the clone to the preview.
void FAS_Preview::addCloneOf(FAS_Entity* entity)
{
    if (!entity)
    {
        return;
    }

    FAS_Entity* clone = entity->clone();
    clone->reparent(this);
    addEntity(clone);
}

// Adds all entities from 'container' to the preview (unselected).
void FAS_Preview::addAllFrom(FAS_EntityContainer& container)
{
    int c=0;
    for(auto e: container)
    {
        if (c<maxEntities)
        {
            FAS_Entity* clone = e->clone();
            clone->setSelected(false);
            clone->reparent(this);

            c+=clone->countDeep();
            addEntity(clone);
        }
    }
}

// Adds all selected entities from 'container' to the preview (unselected).
void FAS_Preview::addSelectionFrom(FAS_EntityContainer& container)
{
    int c=0;
    for(auto e: container)
    {
        if (e->isSelected() && c<maxEntities)
        {
            FAS_Entity* clone = e->clone();
            clone->setSelected(false);
            clone->reparent(this);
            c+=clone->countDeep();
            addEntity(clone);
        }
    }
}

/**
 * Adds all entities in the given range and those which have endpoints
 * in the given range to the preview.
 */
void FAS_Preview::addStretchablesFrom(FAS_EntityContainer& container,
                                      const FAS_Vector& v1, const FAS_Vector& v2)
{
    int c=0;

    for(auto e: container)
    {
        if (e->isVisible() && e->rtti()!=FAS2::EntityHatch &&
                ((e->isInWindow(v1, v2)) || e->hasEndpointsWithinWindow(v1, v2)) &&
                c < maxEntities)
        {
            FAS_Entity* clone = e->clone();
            clone->reparent(this);

            c+=clone->countDeep();
            addEntity(clone);
        }
    }
}
