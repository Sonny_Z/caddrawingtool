/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_actioninterface.h"

//sort with alphabetical order
#include "fas_graphicview.h"
#include <QKeyEvent>

/**
 * Constructor.
 * Sets the entity container on which the action class inherited
 * from this interface operates.
 */
FAS_ActionInterface::FAS_ActionInterface(const char* name,
                                         FAS_EntityContainer& container,
                                         FAS_GraphicView& graphicView) :
    FAS_Snapper(container, graphicView)
{
    this->name = name;
    status = 0;
    finished = false;
    graphic = container.getGraphic();
    document = container.getDocument();
    actionType=FAS2::ActionNone;
}

FAS2::ActionType FAS_ActionInterface::rtti() const
{
    return actionType;
}

//return name of this action
QString FAS_ActionInterface::getName()
{
    return name;
}

void FAS_ActionInterface::setName(const char* _name)
{
    this->name=_name;
}

// Called to initiate an action. This funtcion is often overwritten by the implementing action.
void FAS_ActionInterface::init(int status)
{
    setStatus(status);
    if (status>=0)
    {
        FAS_Snapper::init();
        updateMouseCursor();
    }
    else
    {
        deleteSnapper();
    }
}

void FAS_ActionInterface::mouseMoveEvent(QMouseEvent*) {}
void FAS_ActionInterface::mousePressEvent(QMouseEvent*) {}
void FAS_ActionInterface::mouseReleaseEvent(QMouseEvent*) {}
void FAS_ActionInterface::keyPressEvent(QKeyEvent* e)
{
    e->ignore();
}
void FAS_ActionInterface::keyReleaseEvent(QKeyEvent* e)
{
    e->ignore();
}

/*
 * Sets the current status (progress) of this action.
 * The default implementation sets the class variable 'status' to the
 * given value and finishes the action if 'status' is negative.
*/
void FAS_ActionInterface::setStatus(int status)
{
    this->status = status;
    updateMouseCursor();
    if(status<0) finish();
}

//return Current status of this action.
int FAS_ActionInterface::getStatus()
{
    return status;
}
void FAS_ActionInterface::trigger() {}
void FAS_ActionInterface::updateMouseCursor() {}
bool FAS_ActionInterface::isFinished()
{
    return finished;
}

// Forces a termination of the action without any cleanup.
void FAS_ActionInterface::setFinished()
{
    status = -1;
}

// Finishes this action.
void FAS_ActionInterface::finish(bool /*updateTB*/)
{
    //refuse to quit the default action
    if(!(rtti() == FAS2::ActionDefault) )
    {
        status = -1;
        finished = true;
        FAS_Snapper::finish();
    }
}

// Called by the event handler to give this action a chance to communicate with its predecessor.
void FAS_ActionInterface::setPredecessor(FAS_ActionInterface* pre)
{
    predecessor = pre;
}

// Suspends this action while another action takes place.
void FAS_ActionInterface::suspend()
{
    FAS_Snapper::suspend();
}

//Resumes an action after it was suspended.
void FAS_ActionInterface::resume()
{
    updateMouseCursor();
    FAS_Snapper::resume();
}

void FAS_ActionInterface::setActionType(FAS2::ActionType actionType){
    this->actionType=actionType;
}

