/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_ACTIONZOOMWINDOW_H
#define FAS_ACTIONZOOMWINDOW_H

#include "fas_previewactioninterface.h"

// This action class can handle user events to zoom in a window.
class FAS_ActionZoomWindow : public FAS_PreviewActionInterface
{
    Q_OBJECT
public:
    enum Status
    {
        SetFirstCorner,
        SetSecondCorner
    };
    FAS_ActionZoomWindow(FAS_EntityContainer& container,
                         FAS_GraphicView& graphicView,
                         bool keepAspectRatio=true);

    ~FAS_ActionZoomWindow() override;

    void init(int status=0) override;
    void trigger() override;
    void mouseMoveEvent(QMouseEvent* e) override;
    void mousePressEvent(QMouseEvent* e) override;
    void mouseReleaseEvent(QMouseEvent* e) override;
    void updateMouseCursor() override;

protected:
    struct Points;
    std::unique_ptr<Points> pPoints;

    bool keepAspectRatio;
};

#endif
