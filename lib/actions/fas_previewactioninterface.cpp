/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_previewactioninterface.h"

//sort with alphabetical order
#include "fas_graphicview.h"

FAS_PreviewActionInterface::FAS_PreviewActionInterface(const char* name,
                                                       FAS_EntityContainer& container,
                                                       FAS_GraphicView& graphicView) :
    FAS_ActionInterface(name, container, graphicView)
  ,preview(new FAS_Preview(&container))
{
    preview->setLayer(nullptr);
    hasPreview = true;
}


/** Destructor */
FAS_PreviewActionInterface::~FAS_PreviewActionInterface()
{
    deletePreview();
}

void FAS_PreviewActionInterface::init(int status)
{
    deletePreview();
    FAS_ActionInterface::init(status);
}

void FAS_PreviewActionInterface::finish(bool updateTB)
{
    deletePreview();
    FAS_ActionInterface::finish(updateTB);
}

void FAS_PreviewActionInterface::suspend()
{
    FAS_ActionInterface::suspend();
    deletePreview();
}

void FAS_PreviewActionInterface::resume()
{
    FAS_ActionInterface::resume();
    drawPreview();
}

void FAS_PreviewActionInterface::trigger()
{
    FAS_ActionInterface::trigger();
    deletePreview();
}

void FAS_PreviewActionInterface::deletePreview()
{
    if (hasPreview)
    {
        //avoid deletiong nullptr or empty preview
        preview->clear();
        hasPreview=false;
    }
    if(!graphicView->isCleanUp())
    {
        graphicView->getOverlayContainer(FAS2::ActionPreviewEntity)->clear();
        graphicView->redraw(FAS2::RedrawOverlay);
    }
}

// Draws / deletes the current preview.
void FAS_PreviewActionInterface::drawPreview()
{
    FAS_EntityContainer *container=graphicView->getOverlayContainer(FAS2::ActionPreviewEntity);
    container->clear();
    container->setOwner(false); // Little hack for now so we don't delete teh preview twice
    container->addEntity(preview.get());
    graphicView->redraw(FAS2::RedrawOverlay);
    hasPreview=true;
}
