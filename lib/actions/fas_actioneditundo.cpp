/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_actioneditundo.h"

//sort with alphabetical order
#include "fas_graphic.h"
#include "fas_graphicview.h"
#include <QAction>

// Constructor. true for undo and false for redo.
FAS_ActionEditUndo::FAS_ActionEditUndo(bool undo,
                                       FAS_EntityContainer& container,
                                       FAS_GraphicView& graphicView)
    :FAS_ActionInterface("Edit Undo", container, graphicView)
    , undo(undo)
{
}

void FAS_ActionEditUndo::init(int status)
{
    FAS_ActionInterface::init(status);
    trigger();
}

void FAS_ActionEditUndo::trigger()
{
    if (!graphic)
    {
        qWarning("undo: graphic is null");
        return;
    }

    if (undo)
    {
        document->undo();
    }
    else
    {
        document->redo();
    }
    graphic->setModified(true);
    document->updateInserts(0);
    graphicView->redraw(FAS2::RedrawDrawing);
    finish(false);
}
// EOF
