/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/


#ifndef FAS_ACTIONINTERFACE_H
#define FAS_ACTIONINTERFACE_H

#include <QObject>

#include "fas_snapper.h"

class QKeyEvent;
class FAS_Graphic;
class FAS_Document;
class QAction;

/*
 * This is the interface that must be implemented for all
 * action classes. Action classes handle actions such
 * as drawing lines, moving entities or zooming in.
 */
class FAS_ActionInterface : public QObject, public FAS_Snapper
{
    Q_OBJECT
public:
    FAS_ActionInterface(const char* name,
                        FAS_EntityContainer& container,
                        FAS_GraphicView& graphicView);
    virtual ~FAS_ActionInterface() {}

    virtual FAS2::ActionType rtti() const;

    void setName(const char* _name);
    QString getName();

    virtual void init(int status=0);
    virtual void mouseMoveEvent(QMouseEvent*);
    virtual void mousePressEvent(QMouseEvent*);

    virtual void mouseReleaseEvent(QMouseEvent*);
    virtual void keyPressEvent(QKeyEvent* e);
    virtual void keyReleaseEvent(QKeyEvent* e);
    virtual void setStatus(int status);
    virtual int getStatus();
    virtual void trigger();
    virtual void updateMouseCursor();
    virtual bool isFinished();
    virtual void setFinished();
    virtual void finish(bool updateTB = true );
    virtual void setPredecessor(FAS_ActionInterface* pre);
    virtual void suspend();
    virtual void resume();
    virtual void setActionType(FAS2::ActionType actionType);

private:
    // Current status of the action.
    int status;

protected:
    // Action name. Used internally for debugging
    QString name;

    // This flag is set when the action has terminated and can be deleted.
    bool finished;

    // Pointer to the graphic is this container is a graphic.
    FAS_Graphic* graphic;

    // Pointer to the document (graphic or block) or nullptr.
    FAS_Document* document;

    // Predecessor of this action or nullptr.
    FAS_ActionInterface* predecessor;

    // Command for answering no to a question.
    FAS2::ActionType actionType;
};

#endif
