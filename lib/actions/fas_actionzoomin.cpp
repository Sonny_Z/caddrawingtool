/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "fas_actionzoomin.h"

//sort with alphabetical order
#include "fas_graphicview.h"
#include <QAction>

/**
 * param direction In for zooming in, Out for zooming out.
 * param axis Axis that are affected by the zoom (OnlyX, OnlyY or Both)
 */
FAS_ActionZoomIn::FAS_ActionZoomIn(FAS_EntityContainer& container,
                                   FAS_GraphicView& graphicView,
                                   FAS2::ZoomDirection direction,
                                   FAS2::Axis axis,
                                   FAS_Vector const* pCenter,
                                   double factor)
    :FAS_ActionInterface("Zoom in", container, graphicView)
    ,zoom_factor(factor)
    ,direction(direction)
    ,axis(axis)
    ,center(pCenter?new FAS_Vector{*pCenter}:new FAS_Vector{})
{
}

FAS_ActionZoomIn::~FAS_ActionZoomIn() {}

void FAS_ActionZoomIn::init(int status) {
    FAS_ActionInterface::init(status);
    trigger();
}

void FAS_ActionZoomIn::trigger() {
    switch (axis) {
    case FAS2::OnlyX:
        if (direction==FAS2::In) {
            graphicView->zoomInX();
        } else {
            graphicView->zoomOutX();
        }
        break;

    case FAS2::OnlyY:
        if (direction==FAS2::In) {
            graphicView->zoomInY();
        } else {
            graphicView->zoomOutY();
        }
        break;

    case FAS2::Both:
        if (direction==FAS2::In) {
            graphicView->zoomIn(zoom_factor, *center);
        } else {
            graphicView->zoomOut(zoom_factor, *center);
        }
        break;
    }
    finish(false);
}

// EOF
