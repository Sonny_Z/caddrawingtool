/****************************************************************************
**
** This file is part of the FAS Managementt tool project.
**
**********************************************************************/

#ifndef FAS_ACTIONSELECTWINDOW_H
#define FAS_ACTIONSELECTWINDOW_H

#include "fas_previewactioninterface.h"

#include "fas_vector.h"

// This action class can handle user events to select all entities.
class FAS_ActionDefault : public FAS_PreviewActionInterface {
    Q_OBJECT
public:

    enum Status {
        Neutral,        // we don't know what we do yet.
        Dragging,       // dragging (either an entity or the first part of a selection window)
        SetCorner2,     // Setting the 2nd corner of a selection window.
        Moving,         // Moving entities (drag'n'drop)
        MovingRef,      // Moving a reference point of one or more selected entities
        Panning,        // view panning triggered by Ctl- mouse dragging
    };

public:
    FAS_ActionDefault(FAS_EntityContainer& container,
                      FAS_GraphicView& graphicView);
    ~FAS_ActionDefault(){}

    void init(int status=0) override;

    void keyPressEvent(QKeyEvent* e) override;
    void keyReleaseEvent(QKeyEvent* e) override;

    void mouseMoveEvent(QMouseEvent* e) override;
    void mousePressEvent(QMouseEvent* e) override;
    void mouseReleaseEvent(QMouseEvent* e) override;

    void updateMouseCursor() override;

signals:
    void signalMultiSelectDone();

protected:
    struct Points
    {
        FAS_Vector v1;
        FAS_Vector v2;
    };
    std::unique_ptr<Points> pPoints;
    FAS2::SnapRestriction restrBak;

private:
    bool keyShiftPressed{false};
};

#endif
