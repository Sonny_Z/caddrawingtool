/**************************************************************************
/** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_ACTIONMODIFYDELETE_H
#define FAS_ACTIONMODIFYDELETE_H

#include "fas_actioninterface.h"

// This action class can handle user events to delete entities.
class FAS_ActionModifyDelete : public FAS_ActionInterface {
    Q_OBJECT
public:
    // Action States.
    enum Status
    {
        Acknowledge    /**< Acknowledge or cancel. */
    };

public:
    FAS_ActionModifyDelete(FAS_EntityContainer& container, FAS_GraphicView& graphicView);

    void init(int status=0) override;
    void trigger() override;
    void updateMouseCursor() override;
};

#endif
