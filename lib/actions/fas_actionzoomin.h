/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FAS_ACTIONZOOMIN_H
#define FAS_ACTIONZOOMIN_H

#include "fas_actioninterface.h"

// This action triggers zoom increase.
class FAS_ActionZoomIn : public FAS_ActionInterface
{
    Q_OBJECT
public:
    FAS_ActionZoomIn(FAS_EntityContainer& container,
                     FAS_GraphicView& graphicView,
                     FAS2::ZoomDirection direction = FAS2::In,
                     FAS2::Axis axis = FAS2::Both,
                     FAS_Vector const* pCenter = nullptr,
                     double factor = 1.25);
    ~FAS_ActionZoomIn() override;

    void init(int status=0) override;
    void trigger() override;

protected:
    double zoom_factor;
    FAS2::ZoomDirection direction;
    FAS2::Axis axis;
    std::unique_ptr<FAS_Vector> center;
};

#endif
