#include "waitlongtimedialog.h"
#include "ui_waitlongtimedialog.h"
#include<QMovie>
#include <QTime>
#include <QFileDialog>
#include <QMessageBox>
#include "commondef.h"
#include "blockform.h"
WaitLongTimeDialog::WaitLongTimeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WaitLongTimeDialog)
{
    ui->setupUi(this);
    timer=new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(slotTimeOut()));
    timer->setSingleShot(true);
    timer->start(50);


    this->setWindowFlag(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setWindowOpacity(0.8);
    ui->label->setMovie(new QMovie(":/icons/resource/image/busy/busy2.gif"));
    ui->label->movie()->start();

    process.moveToThread(&thread);
    connect(this,SIGNAL(sigWork()),&process,SLOT(slotDowork()));
    connect(&process,SIGNAL(sigFinish()),this,SLOT(slotFinished()));
    connect(&process,SIGNAL(sigReturedNewViewInfo(const QString&)),this,SLOT(slotPopUpMessage(const QString&)));
    v=0;

}

WaitLongTimeDialog::~WaitLongTimeDialog()
{
    thread.quit();
    thread.wait();
    delete ui;
}
void WaitLongTimeDialog::slotTimeOut()
{
    timer->stop();
    thread.start();
    emit sigWork();


}
void WaitLongTimeDialog::setParas1(int v,FASUtils* utils)
{
    this->v=v;
    this->utils=utils;
    process.setParas1(v,utils);
}
void WaitLongTimeDialog::slotFinished()
{
    if(v==1)
    {
        accept();
    }
    else if(v==2)
    {
        if (utils != nullptr)
        {
                BlockForm blockFormDia(this, utils);
                blockFormDia.setModal(false);
                ui->label->movie()->stop();
                ui->label->hide();
                blockFormDia.show();
                blockFormDia.exec();
        }
        accept();
    }
}

void WaitLongTimeDialog::slotPopUpMessage(const QString& popUpMessage){
    emit sigPopUpMessage(popUpMessage);
//    QMessageBox::information(this, tr("Information"), tr(popUpMessage.toUtf8()));
}
