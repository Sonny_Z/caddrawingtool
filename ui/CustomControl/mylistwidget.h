#ifndef MYLISTWIDGET_H
#define MYLISTWIDGET_H
 
#include <QObject>
#include <QListWidget>
 
class MyListWidget : public QListWidget
{
    Q_OBJECT
public:
    MyListWidget(QWidget *parent = 0);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
 
public slots:
    void receiverData(QPoint pos);
 
signals:
    void wantToGetPoint();
 
private:
    bool m_dragFlag;
    int m_currX,m_currY;//当前的控件的宽度和高度
    bool m_leftFlag,m_rightFlag;    //是拖动左边还是右边
    int m_currWinX,m_currWinY;  //当前窗口X坐标与Y坐标
    int m_widgetX,m_widgetY;    //控件所处的坐标
};
 
#endif // MYLISTWIDGET_H