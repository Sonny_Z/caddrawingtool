#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QMouseEvent>
#include <QCursor>
 
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(ui->widget,SIGNAL(wantToGetPoint()),this,SLOT(receiverSIGNAL()));
    connect(this,SIGNAL(sendPoint(QPoint)),ui->widget,SLOT(receiverData(QPoint)));
 
    ui->treeView->setMouseTracking(true);
    for(int i=0;i<5;i++){
        ui->widget->insertItem(0,"*****第"+QString::number(i)+"行*****");
    }
}
 
Widget::~Widget()
{
    delete ui;
}
 
void Widget::receiverSIGNAL()
{
    QPoint point=mapFromGlobal(QCursor::pos());
    //qDebug()<<point;  不要直接把mapFromGlobal(QCursor::pos())传进去，不然有BUG
    emit this->sendPoint(point);
}