#include "mylistwidget.h"
#include <QMouseEvent>
#include <QDebug>
#include <QCursor>
 
MyListWidget::MyListWidget(QWidget *parent):
    QListWidget(parent),
    m_dragFlag(false),
    m_leftFlag(false),
    m_rightFlag(false)
{
 
}
 
void MyListWidget::mousePressEvent(QMouseEvent *event)
{
    if(event->x()<=15){ //左边
        this->setCursor(Qt::SizeHorCursor);
        m_dragFlag=true;
        m_leftFlag=true;
        m_rightFlag=false;
    }
    else if(event->x()>=this->width()-15){  //右边
        this->setCursor(Qt::SizeHorCursor);
        m_dragFlag=true;
        m_leftFlag=false;
        m_rightFlag=true;
    }
    m_currX=this->width();
    m_currY=this->height();
    m_widgetX=this->pos().x();
    m_widgetY=this->pos().y();
    event->ignore();
}
 
void MyListWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(m_dragFlag){
 
        //分左右考虑
        if(m_leftFlag){
            emit wantToGetPoint();
            this->setGeometry(m_currWinX,this->pos().y(),m_currX+(m_widgetX-m_currWinX),m_currY);
        }
        else if(m_rightFlag){
            this->resize(event->x(),m_currY);
        }
        else{
 
        }
    }
    //qDebug()<<"x:"<<event->x();
}
 
void MyListWidget::mouseReleaseEvent(QMouseEvent *event)
{
    this->setCursor(Qt::ArrowCursor);
    m_dragFlag=false;
}
 
void MyListWidget::receiverData(QPoint pos)
{
    this->m_currWinX=pos.x();
    this->m_currWinY=pos.y();
}