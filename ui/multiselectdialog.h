#ifndef MULTISELECTDIALOG_H
#define MULTISELECTDIALOG_H

#include "ui_multiselectdialog.h"
#include <QWidget>
#include <QDialog>
#include "QLabel"
#include <QPainter>
#include "fas_painter.h"
#include "fas_insert.h"
#include <QVector>
#include "entityqggv.h"
#include <QComboBox>
#include <QTextEdit>
#include <QtGlobal>
#include "fasutils.h"


namespace Ui {
class MultiSelectDialog;
}

class MultiSelectDialog : public QDialog
{
    Q_OBJECT

public:
    MultiSelectDialog(QWidget *parent, FASUtils* utils, QList<FAS_Insert*> devices);
    ~MultiSelectDialog();
    void init();

private slots:
    void on_OK_clicked();
    void on_QuickEncode_clicked();
    void on_statistics_clicked();

private:
    Ui::MultiSelectDialog *uiMultiSelectedDialog;

    FASUtils* utils = nullptr;
    QList<FAS_Insert*> allDevices;
    QList<FAS_Insert*> sortedDeviceList;
    QStringList typeLocalStrList;

    QHash<int, FAS_Insert*> idToDeviceMap;
};

#endif // MULTISELECTDIALOG_H
