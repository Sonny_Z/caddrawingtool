/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef Entity_QG_GV_H
#define Entity_QG_GV_H

#include <QPainter>
#include <QMenu>
#include <QGridLayout>
#include <QWidget>

#include "fas_entitycontainer.h"
#include "fas_graphicview.h"

/**
 * This is the Qt implementation of a widget which can view a
 * graphic.
 *
 * Instances of this class can be linked to layer lists using
 * addLayerListListener().
 */
class EntityQGGV:   public FAS_GraphicView
{
    Q_OBJECT

public:
    EntityQGGV(QWidget* parent, Qt::WindowFlags f, FAS_EntityContainer* container);
    ~EntityQGGV() override;
    int getWidth() const override;
    int getHeight() const override;
    void redraw(FAS2::RedrawMethod method=FAS2::RedrawAll) override;
    void adjustOffsetControls() override{}
    void adjustZoomControls() override{}
    void setBackground(const FAS_Color& bg) override;
    virtual void getPixmapForView(std::unique_ptr<QPixmap>& pm);

    /**
     * @brief setOffset
     * @param ox, offset X
     * @param oy, offset Y
     */
    void setOffset(int ox, int oy) override;
    void setAntialiasing(bool state);
    void setCursorHiding(bool state);

    FAS_Vector getMousePosition() const {return FAS_Vector();}
    void updateGridStatusWidget(const QString&) {}
    void setMouseCursor(FAS2::CursorType){}

protected:
    void paintEvent(QPaintEvent *)override;
    void resizeEvent(QResizeEvent* e) override;

protected:
    // Used for buffering different paint layers
    std::unique_ptr<QPixmap> PixmapLayer1;  // Used for grids and absolute 0
    std::unique_ptr<QPixmap> PixmapLayer2;  // Used for the actual CAD drawing

    FAS2::RedrawMethod redrawMethod;
};

#endif
