#ifndef NONBLOCKFORM_H
#define NONBLOCKFORM_H

#include <QComboBox>
#include <QDialog>

#include "fas_painter.h"
#include "fasutils.h"

namespace Ui {
class NonBlockForm;
}

class NonBlockForm : public QDialog
{
    Q_OBJECT

public:
    NonBlockForm(QWidget *parent, FASUtils* utils);
    ~NonBlockForm();
    void init();

signals:
    void signalSendTypeList(QList<DeviceInfo*> typeList, QList<int> countList);

private slots:
    void on_pBtnCancel_clicked();
    void on_pBtnOk_clicked();

private:
    Ui::NonBlockForm *uiNonBlockForm;

    QStringList typeStrList;
    QStringList typeLocalStrList;
    QList<FAS_Block*> showBlockList;
    QList<QComboBox*> comBoxList;
    QMultiHash<QString, FAS_Insert*> nameToEntityHash;

    FASUtils* utils;
};

#endif // NonBlockForm_H
