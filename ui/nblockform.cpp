#include "nblockform.h"
#include "ui_nblockform.h"

//sort with alphabetical order
#include <QFile>
#include <QTextStream>
#include <QCryptographicHash>
#include "commondef.h"
#include "entityqggv.h"
#include "fas_blocklist.h"
#include "fas_block.h"
#include "fas_insert.h"
#include "threadexcelgenerate.h"
#include "qblog4qt.h"
#include "lib/log4qt/log4qt/logmanager.h"
#include <QDebug>
static void WriteToText(DeviceFeature feature, DeviceInfo* type)
{
    QFile textFile(COMMONDEF->textName);
    if(textFile.open(QIODevice::Text | QFile::Append))
    {
        QTextStream stream(&textFile);
        stream << feature.circles << ", ";
        stream << feature.lines << ", ";
        stream << feature.arcs << ", ";
        stream << feature.hatchs << ", ";
        stream << feature.solids << ", ";
        stream << QString::number(feature.size, 'g', 6) << ", ";
        stream << feature.text << ", ";
        stream << type->getName() << endl;
        textFile.close();
    }
}



static bool IsTwoLinesConnected(FAS_Entity* line1, FAS_Entity* line2)
{
    double pTolerance = COMMONDEF->getConnectionTolerance();
    //double entityTolerance = 0.5;
    line2->calculateBorders();
    line1->calculateBorders();
    if(line1->getStartpoint().distanceTo(line2->getStartpoint())<=pTolerance ||
       line1->getStartpoint().distanceTo(line2->getEndpoint())<=pTolerance ||
       line1->getEndpoint().distanceTo(line2->getEndpoint())<=pTolerance ||
       line1->getEndpoint().distanceTo(line2->getStartpoint())<=pTolerance){
        return true;
    }
    return false;
}

static bool IsFreeVectorExist(QHash<QString,int> vectorSequence){
    bool result = false;
    QHash<QString, int>::iterator i;
    for( i=vectorSequence.begin(); i!=vectorSequence.end(); ++i)
    {
        if(i.value() == 1)
        {
            result =true;
            break;
        }
    }
    return result;
}




static QHash<QString,int> MappingEntitiesToVectorSequence(QList<FAS_Entity*> entitiesList)
{
    //Create vector sequence source of current entities list
    QHash<QString,int> result;
    QString kMin,kMax;
    for(FAS_Entity* e:entitiesList){
        kMin = QString::fromUtf8("%1_%2").arg(QString::number(e->getStartpoint().x, 'f',7).replace(".","")).arg(QString::number(e->getStartpoint().y, 'f',7).replace(".",""));
        kMax = QString::fromUtf8("%1_%2").arg(QString::number(e->getEndpoint().x, 'f',7).replace(".","")).arg(QString::number(e->getEndpoint().y, 'f',7).replace(".",""));
        if(result.contains(kMin)){
            result[kMin]++;
        }else{
            result.insert(kMin,1);
        }

        if(result.contains(kMax)){
            result[kMax]++;
        }else{
            result.insert(kMax,1);
        }
    }
    return result;
}

static QHash<QString,QList<FAS_Entity*>> MappingEntitiesToOriginSequence(QList<FAS_Entity*> lines)
{
    QHash<QString,QList<FAS_Entity*>> result;
    for(FAS_Entity* l:lines)
    {
        //auto vector0= new FAS_Vector(l->getMax().x-l->getMin().x,l->getMax().y-l->getMin().y);
        auto sp=l->getStartpoint();
        auto ep=l->getEndpoint();
        FAS_Vector* vector0;
        if(sp.x>ep.x){
            vector0=new FAS_Vector(sp.x-ep.x,sp.y-ep.y);
        }else{
            vector0=new FAS_Vector(ep.x-sp.x,ep.y-sp.y);
        }
        auto key = QString::fromUtf8("%1_%2").arg(QString::number(abs(vector0->x), 'f',7).replace(".","")).arg(QString::number(abs(vector0->y), 'f',7).replace(".",""));

        QList<FAS_Entity*> ll=result[key];
        ll.append(l);
        result[key]=ll;
    }
    return result;
}

//归元清洗方法
static QList<FAS_EntityContainer*> Origin20Filter(FAS_EntityContainer* Origin20ObjsContainer)
{
    QList<FAS_EntityContainer*> result;
    FAS_EntityContainer* filteredEntities = new FAS_EntityContainer();
    FAS_EntityContainer* suspectEntities = new FAS_EntityContainer();
    QList<FAS_Entity*> lineObjs = Origin20ObjsContainer->getEntityList();
    auto EntitiesToOriginSequence=MappingEntitiesToOriginSequence(lineObjs);

    foreach (QString key, EntitiesToOriginSequence.keys())
    {
        if(EntitiesToOriginSequence[key].count()<2)
        {
           foreach (auto e, EntitiesToOriginSequence[key])
           {
               //foreach (auto e, elist)
               //{
                    filteredEntities->addEntity(e->clone());
               //}
           }
        }else {
            foreach (auto e, EntitiesToOriginSequence[key])
            {
                    //doSomething(key, value);
                //foreach (auto e, elist)
                //{
                    suspectEntities->addEntity(e->clone());
                //}
            }
        }
    }
    result.append(suspectEntities);
    result.append(filteredEntities);

    return result;
}

/*
static bool ContainClosedObj(FAS_EntityContainer*& lineSource, FAS_EntityContainer*& filteredFreeEntities){
    bool result=false;
    //trueCloseObjInBlock->clear();//=new FAS_EntityContainer();
    QList<FAS_Entity*> tempEntities = lineSource->getBlock()->getEntityList();
    QHash<QString,int> vectorSequence;
    QString kMin,kMax;
    for(FAS_Entity* e:tempEntities){
        kMin = QString::fromUtf8("%1_%2").arg(QString::number(e->getMin().x, 'f',7).replace(".","")).arg(QString::number(e->getMin().y, 'f',7).replace(".",""));
        kMax = QString::fromUtf8("%1_%2").arg(QString::number(e->getMax().x, 'f',7).replace(".","")).arg(QString::number(e->getMax().y, 'f',7).replace(".",""));
        if(vectorSequence.contains(kMin))
        {
           vectorSequence[kMin]++;
        }else{
         vectorSequence.insert(kMin,1);
        }

        if(vectorSequence.contains(kMax)){
         vectorSequence[kMax]++;
        }else{
         vectorSequence.insert(kMax,1);
        }
    }

    QString ekMin,ekMax;
    while(IsFreeVectorExist(vectorSequence))
    {
        QHash<QString, int>::iterator i;
        for( i=vectorSequence.begin(); i!=vectorSequence.end(); ++i)
        {
            if(i.value() == 1)
            {
                //QStringList sl =i.key().split("_");
                //FAS_Vector vk = FAS_Vector(sl[0].toDouble(),sl[1].toDouble());
                for(FAS_Entity* e:tempEntities)
                {
                    ekMin = QString::fromUtf8("%1_%2").arg(QString::number(e->getMin().x, 'f',7).replace(".","")).arg(QString::number(e->getMin().y, 'f',7).replace(".",""));
                    ekMax = QString::fromUtf8("%1_%2").arg(QString::number(e->getMax().x, 'f',7).replace(".","")).arg(QString::number(e->getMax().y, 'f',7).replace(".",""));
                    if(i.key()==ekMin||i.key()==ekMax)
                    {
                        filteredFreeEntities->appendEntity(e->clone());
                        lineSource->removeEntity(e);
                        break;
                    }
                }

                if(vectorSequence.contains(ekMin))
                {
                    vectorSequence[ekMin]--;
                }

                if(vectorSequence.contains(ekMax))
                {
                    vectorSequence[ekMax]--;
                }
            }else if(i.value()<=0){
                vectorSequence.erase(i);
                i = vectorSequence.begin()+1;
            }
        }
    }
    return result=!lineSource->isEmpty()?true:false;
}



static QList<FAS_EntityContainer*> CloseObjFilter(FAS_EntityContainer* linesContainer)
{
    QList<FAS_EntityContainer*> result;

    FAS_EntityContainer* closeObj = new FAS_EntityContainer();
    FAS_EntityContainer* closeObjResult = new FAS_EntityContainer();//[1]
    FAS_EntityContainer* restFreeEntities = new FAS_EntityContainer();//[2]
    QList<FAS_Entity*> lines = linesContainer->getEntityList();
    for(FAS_Entity* line : lines)
    {
        bool isNewLineFiltered=false;

        FAS_Vector middlePoint=line->getMiddlePoint();
        QString name = QString::fromUtf8("$equip$0000_%1%2%3").arg(line->getLength()).arg(middlePoint.x).arg(middlePoint.y);
        FAS_Vector bp(middlePoint.x, middlePoint.y);
        FAS_Block* block = new FAS_Block(closeObj, FAS_BlockData(name, bp, false ));

        if(closeObj->getEntityList().count()<=0){
            block->addEntity(line);
            closeObj->addEntity(block);
            continue;
        }
        QList<FAS_Entity*> blks = closeObj->getEntityList();
        for(FAS_Entity* blk : blks){
            if(isNewLineFiltered) break;
            QList<FAS_Entity*> b=blk->getBlock()->getEntityList();
            for(FAS_Entity* l:b){
                if(IsTwoLinesConnected(l,line)){
                    blk->getBlock()->addEntity(line);
                    isNewLineFiltered=true;
                    break;
                }
            }
            if(blk==blks.last()&&!isNewLineFiltered){
                block->addEntity(line);
                closeObj->addEntity(block);
                isNewLineFiltered=true;
            }
        }
    }

    QList<FAS_Entity*> blks = closeObj->getEntityList();
    for(FAS_Entity* blk : blks){
        FAS_EntityContainer* trueCloseObjInBlock=blk->getBlock();
        QList<FAS_Entity*> suspectEntities=trueCloseObjInBlock->getBlock()->getEntityList();
        if(suspectEntities.size()<2){
            for(FAS_Entity* e:suspectEntities){
                restFreeEntities->addEntity(e);
            }
        }else if(ContainClosedObj(trueCloseObjInBlock,restFreeEntities)){
            closeObjResult->appendEntity(trueCloseObjInBlock);
        }
    }
    result.append(closeObjResult);
    result.append(restFreeEntities);
    return result;
}*/


//绝对区域融合方法
static QList<FAS_EntityContainer*> AbsoluteRegionMergeFilter(QList<FAS_EntityContainer*> AssociatedSetsParents, FAS_EntityContainer* ScatteredOnes)
{
    QList<FAS_EntityContainer*> result;

    for(FAS_EntityContainer* c:AssociatedSetsParents)
    {
        if(c == ScatteredOnes){
            continue;
        }
        QList<FAS_Entity*> entities = ScatteredOnes->getEntityList();
        for(FAS_Entity* e:entities)
        {
            bool absoluteInRegion = c->getMin().x <= e->getMin().x
                    && c->getMin().y <= e->getMin().y
                    && c->getMax().x >= e->getMax().x
                    && c->getMax().y >= e->getMax().y
                    ?true:false;
            if(absoluteInRegion)
            {
                c->addEntity(e->clone());
                ScatteredOnes->removeEntity(e);
            }
        }


        result.append(c);
    }
    return result;
}


//封闭过滤方法
static QList<FAS_EntityContainer*> AssociatedAtomicEntityFilter(FAS_EntityContainer* lineContainer)
{
    QList<FAS_EntityContainer*> result;
    QList<FAS_Entity*> lines = lineContainer->getEntityList();
    QHash<QString,int> vectorSequence=MappingEntitiesToVectorSequence(lines);


    //QHash<QString,QList<FAS_Entity*>>

    FAS_EntityContainer* AssociatedEntities = new FAS_EntityContainer();
    FAS_EntityContainer* ScatteredAtmoicdEntities  = new FAS_EntityContainer();
    //Filtering and mapping free entities by single sign in vector sequence
    QString ekMin,ekMax;
    QList<FAS_Entity*> tempEntities;
    for(FAS_Entity* e:lines)
    {
        tempEntities.append(e);
    }
    while(IsFreeVectorExist(vectorSequence))
    {
        //QHash<QString, int>::iterator i;
        QHashIterator<QString,int>item(vectorSequence);
        while(item.hasNext()) {
           auto i =  item.next();
           /*qDebug() << item.key();
           if (i.key() == "test1") {
               map.remove(i.key());
               delete i.value();
           }*/

           if(i.value() == 1)
           {
               //QStringList sl =i.key().split("_");
               //FAS_Vector vk = FAS_Vector(sl[0].toDouble(),sl[1].toDouble());
               //for(FAS_Entity* e:tempEntities)
               foreach (auto e, lines)
               {
                   ekMin = QString::fromUtf8("%1_%2").arg(QString::number(e->getStartpoint().x, 'f',7).replace(".","")).arg(QString::number(e->getStartpoint().y, 'f',7).replace(".",""));
                   ekMax = QString::fromUtf8("%1_%2").arg(QString::number(e->getEndpoint().x, 'f',7).replace(".","")).arg(QString::number(e->getEndpoint().y, 'f',7).replace(".",""));
                   if(i.key()==ekMin||i.key()==ekMax)
                   {
                       ScatteredAtmoicdEntities->addEntity(e->clone());
                       lines.removeOne(e);
                       break;
                        /*
                       if(vectorSequence.contains(ekMin))
                       {
                           vectorSequence[ekMin]--;
                           if (0==vectorSequence[ekMin]) {
                               vectorSequence.remove(ekMin);
                               delete i.value();
                           }
                       }

                       if(vectorSequence.contains(ekMax))
                       {
                           vectorSequence[ekMax]--;
                           if (0==vectorSequence[ekMax]) {
                               vectorSequence.remove(ekMax);
                               delete i.value();
                           }
                       }*/
                   }
               }
           }else if(i.value()<=0) {
               //vectorSequence.remove(i.key());
               //delete i.value();
           }
        }
        vectorSequence.clear();
        vectorSequence=MappingEntitiesToVectorSequence(lines);

        /*for( i=vectorSequence.begin(); i!=vectorSequence.end(); ++i)
        {
            if(i.value() == 1)
            {
                //QStringList sl =i.key().split("_");
                //FAS_Vector vk = FAS_Vector(sl[0].toDouble(),sl[1].toDouble());
                for(FAS_Entity* e:tempEntities)
                {
                    ekMin = QString::fromUtf8("%1_%2").arg(QString::number(e->getStartpoint().x, 'f',7).replace(".","")).arg(QString::number(e->getStartpoint().y, 'f',7).replace(".",""));
                    ekMax = QString::fromUtf8("%1_%2").arg(QString::number(e->getEndpoint().x, 'f',7).replace(".","")).arg(QString::number(e->getEndpoint().y, 'f',7).replace(".",""));
                    if(i.key()==ekMin||i.key()==ekMax)
                    {
                        ScatteredAtmoicdEntities->addEntity(e->clone());
                        lines.removeOne(e);
                        break;
                    }
                }

                if(vectorSequence.contains(ekMin))
                {
                    vectorSequence[ekMin]--;
                }

                if(vectorSequence.contains(ekMax))
                {
                    vectorSequence[ekMax]--;
                }
            }else if(i.value()<=0){
                vectorSequence.erase(i);
                i = vectorSequence.begin()+1;
            }
            if(1==vectorSequence.size()  && i.value()<=0 && 0==vectorSequence.first()&& 0==vectorSequence.last())
            {
                break;
            }
        }*/

    }

    for(FAS_Entity* l:lines)
    {
        AssociatedEntities->addEntity(l);
    }
    result.append(AssociatedEntities);
    result.append(ScatteredAtmoicdEntities);//zhan di
    //result.append(AssociatedSetsMappingToList(AssociatedEntities));

    return result;
}


//关联对象集的封闭序列构建提取方法
static QList<FAS_EntityContainer*> AssociatedSetsMappingToList(FAS_EntityContainer* AssociatedSetsContainer)
{
    QList<FAS_EntityContainer*> result,filteredSets;
    FAS_EntityContainer*  ScatteredAtmoicEntities = new FAS_EntityContainer();
    QList<FAS_Entity*> eSet = AssociatedSetsContainer->getEntityList();
    QList<FAS_Entity*> tempSet;
    int sourceDetectCount,subSourceDetection,resultChecking,entityAppend;
    //关联过滤
    sourceDetectCount =eSet.count();
    while(eSet.count()>0)
    {
        tempSet.clear();
        for(FAS_Entity* e:eSet)
        {
            tempSet.append(e);
        }

        bool isNewContainer = true;

        for(FAS_EntityContainer* &c:result)
        {
            resultChecking +=1;
            bool test = c==result.first();
            QList<FAS_Entity*> mappedSet = c->getEntityList();
            for(FAS_Entity* e:eSet)
            {
                subSourceDetection +=1;
                for(FAS_Entity* me:mappedSet)
                {
                    entityAppend +=1;
                    if(IsTwoLinesConnected(e,me))
                    {
                        c->addEntity(e);

                        tempSet.removeOne(e);
                        isNewContainer=false;
                        break;
                    }
                }
                //if(!isNewContainer) break;
            }
            if(0==mappedSet.count() && c==result.last() && isNewContainer)
            {
                c->addEntity(eSet.first());
                tempSet.removeOne(eSet.first());
                eSet.removeFirst();
                isNewContainer=false;
                break;
            }
        }

        if(isNewContainer)
        {
            result.append(new FAS_EntityContainer());
        }

        eSet.clear();
        for(FAS_Entity* e:tempSet)
        {
            eSet.append(e);
        }
    }

    /*封闭过滤排除散乱图元
    for(FAS_EntityContainer* c:result)
    {
        QList<FAS_EntityContainer*> ScatterringFilterResult = AssociatedAtomicEntityFilter(c);
        QList<FAS_Entity*> ScatteredOnes=ScatterringFilterResult.last()->getEntityList();
        for(FAS_Entity* e:ScatteredOnes){
            ScatteredAtmoicEntities->addEntity(e);
        }


        filteredSets.append(ScatterringFilterResult.first());

    }
    result.clear();

    result.append(filteredSets);
    result.append(ScatteredAtmoicEntities);//stack bottom
    */
    Log4Info("关联对象集的封闭序列构建提取方法 sourceDetectCount:",sourceDetectCount);
    Log4Info("关联对象集的封闭序列构建提取方法 subSourceDetection:",subSourceDetection);
    Log4Info("关联对象集的封闭序列构建提取方法 resultChecking:",resultChecking);
    Log4Info("关联对象集的封闭序列构建提取方法 entityAppend:",entityAppend);
    return result;
}


//建立【线ID-线对象】映射表
//建立【线ID-点列表】映射表
//建立【端点-线ID列表】映射表
static void MappingEntity2HashCodeID(QList<FAS_Entity*> el,QHash<QString,FAS_Entity*> &LineNoLinkedLine,QHash<QString,QList<QString>> &LineLinkedPoint,QHash<QString,QList<QString>> &PointLinkedLines){
//    QHash<QString,FAS_Entity*> LineNoLinkedLine;
//    //std::hash<QString> hash_str;
//    QHash<QString,QList<FAS_Vector>> LineLinkedPoint;
//    QHash<QString,QList<QString>> PointLinkedLines;


    for(auto e:el){
        auto str = QString::fromUtf8("%1_%2_%3_%4")
                .arg(QString::number(e->getStartpoint().x, 'f',7))
                .arg(QString::number(e->getStartpoint().y, 'f',7))
                .arg(QString::number(e->getEndpoint().x, 'f',7))
                .arg(QString::number(e->getEndpoint().y, 'f',7));
        auto key = QString(QCryptographicHash::hash(str.toUtf8(),QCryptographicHash::Sha1).toHex());
        //建立【线ID-线对象】映射表
        LineNoLinkedLine.insert(key,e);

        //建立【线ID-点列表】映射表
        QList<QString> value;
        //QList<FAS_Vector> value1 = new QList<FAS_Vector>{e->getStartpoint(),e->getEndpoint()};

        auto kS=QString::fromUtf8("%1_%2").arg(QString::number(e->getStartpoint().x, 'f',7)).arg(QString::number(e->getStartpoint().y, 'f',7));
        auto kE=QString::fromUtf8("%1_%2").arg(QString::number(e->getEndpoint().x, 'f',7)).arg(QString::number(e->getEndpoint().y, 'f',7));

        value.append(kS);
        value.append(kE);
        LineLinkedPoint.insert(key,value);

        //建立【端点-线ID列表】映射表
        auto objID = QString(QCryptographicHash::hash(str.toUtf8(),QCryptographicHash::Sha1).toHex());

        QList<QString> keys=PointLinkedLines.keys();
        QList<QString> startLineIDs;
        QList<QString> endLineIDs;

        //起点判断
        if(keys.contains(kS)){
            startLineIDs=PointLinkedLines.find(kS).value();
        }
        startLineIDs.append(objID);
        PointLinkedLines.insert(kS,startLineIDs);

        //终点判断
        if(keys.contains(kE)){
            endLineIDs=PointLinkedLines.find(kE).value();
        }
         endLineIDs.append(objID);
         PointLinkedLines.insert(kE,endLineIDs);

    }
}

////建立【线ID-点列表】映射表
//static QHash<QString,QList<FAS_Vector>> MappingVectors2HashCodeID(QList<FAS_Entity*> el){
//    QHash<QString,QList<FAS_Vector>> result;
//    //std::hash<QString> hash_str;
//    for(auto e:el){
//        auto str = QString::fromUtf8("%1_%2_%3_%4")
//                .arg(QString::number(e->getStartpoint().x, 'f',7))
//                .arg(QString::number(e->getStartpoint().y, 'f',7))
//                .arg(QString::number(e->getEndpoint().x, 'f',7))
//                .arg(QString::number(e->getEndpoint().y, 'f',7));
//        auto key = QString(QCryptographicHash::hash(str.toUtf8(),QCryptographicHash::Sha1).toHex());
//        QList<FAS_Vector> value;
//        //QList<FAS_Vector> value1 = new QList<FAS_Vector>{e->getStartpoint(),e->getEndpoint()};
//        value.append(e->getStartpoint());
//        value.append(e->getEndpoint());
//        result.insert(key,value);
//    }
//    return result;
//}


//建立【端点-线ID列表】映射表
//static QHash<QString,QList<QString>> MappingHashCodeID2Vector(QList<FAS_Entity*> el){
//    QHash<QString,QList<QString>> result;
//    //std::hash<QString> hash_str;
//    for(auto e:el){
//        auto str = QString::fromUtf8("%1_%2_%3_%4")
//                            .arg(QString::number(e->getStartpoint().x, 'f',7))
//                            .arg(QString::number(e->getStartpoint().y, 'f',7))
//                            .arg(QString::number(e->getEndpoint().x, 'f',7))
//                            .arg(QString::number(e->getEndpoint().y, 'f',7));
//        auto objID = QString(QCryptographicHash::hash(str.toUtf8(),QCryptographicHash::Sha1).toHex());


//        auto kS=QString::fromUtf8("%1_%2").arg(QString::number(e->getStartpoint().x, 'f',7)).arg(QString::number(e->getStartpoint().y, 'f',7));

//        auto kE=QString::fromUtf8("%1_%2").arg(QString::number(e->getEndpoint().x, 'f',7)).arg(QString::number(e->getEndpoint().y, 'f',7));



//        auto idListS=result[kS];
//        auto idListE=result[kE];
//        idListS.append(objID);
//        idListE.append(objID);
//        result[kS]=idListS;
//        result[kE]=idListE;
//    }
//    return result;
//}



//建立【端点-关联端点列表】映射表
static QHash<QString,QList<QString>> MappingVectors2Vector(QHash<QString,QList<QString>> vectorLinesMappingList){
    QHash<QString,QList<QString>> pointLinkedPointResult;
    double pTolerance = COMMONDEF->getConnectionTolerance();
//    foreach (QString key, vectorLinesMappingList.keys())
//    {
//        auto vList=result[key];
//        result[key]=vList;
//    }

    QList<QString> keys=vectorLinesMappingList.keys();
    int keysSize=keys.size();

    QHash<QString,QList<QString>>::iterator itKeys;
    QHash<QString,QList<QString>>::iterator itValues;

    for( itKeys=vectorLinesMappingList.begin(); itKeys!=vectorLinesMappingList.end(); itKeys++){

        FAS_Vector pi;
        pi.x=(itKeys.key()).split("_").first().toDouble();
        pi.y=(itKeys.key()).split("_").last().toDouble();

        for( itValues=vectorLinesMappingList.begin()+1; itValues!=vectorLinesMappingList.end(); itValues++){
          FAS_Vector pj;
          pj.x=(itValues.key()).split("_").first().toDouble();
          pj.y=(itValues.key()).split("_").last().toDouble();


          if(pi.distanceTo(pj)<=pTolerance){

//              if()

//                auto ivList=result[result.keys().at(i)];
//                auto jvList=result[result.keys().at(j)];
//                ivList.append(result.keys().at(j));
//                jvList.append(result.keys().at(i));
//                result[result.keys().at(i)]=ivList;
//                result[result.keys().at(j)]=jvList;
          }


        }

    }

//    for(int i=0;i<keysSize;i++){
//        for(int j=i+1;j<keysSize;j++){
//            FAS_Vector pi,pj;
//            pi.x=keys.at(i).split("_").first().toDouble();
//            pi.y=keys.at(i).split("_").last().toDouble();
//            pj.x=keys.at(j).split("_").first().toDouble();
//            pj.y=keys.at(j).split("_").last().toDouble();


//            if(pi.distanceTo(pj)<=pTolerance){
////                auto ivList=result[result.keys().at(i)];
////                auto jvList=result[result.keys().at(j)];
////                ivList.append(result.keys().at(j));
////                jvList.append(result.keys().at(i));
////                result[result.keys().at(i)]=ivList;
////                result[result.keys().at(j)]=jvList;
//            }
//        }
//    }
    return pointLinkedPointResult;
}

//建立【线ID-线对象】映射表
//QHash<QString,FAS_Entity*> keyLine;
//建立【线ID-点列表】映射表
//QHash<QString,QList<QString>> keyVectors;
//建立【端点-线ID列表】映射表
//QHash<QString,QList<QString>> vectorLines;
//建立【端点-关联端点列表】映射表
//QHash<QString,QList<QString>> vectorVectors;
//疑似设备的容器列表
//QList<FAS_EntityContainer*> suspectDeviceContainer;
//疑似设备的元件列表
QList<QString> subEntityIDs;

QDateTime log_date_time;
int runTimes;

static void createSingleDevice(QHash<QString,FAS_Entity*> keyLine,QHash<QString,QList<QString>> &keyVectors,QHash<QString,QList<QString>> &vectorLines,QString itLineToPoint,QList<FAS_Entity*> &deviceLines){

    //线到点
    QString lineNo=itLineToPoint;
    QList<QString> points= keyVectors[lineNo];
    keyVectors.remove(itLineToPoint);
    deviceLines.append(keyLine.find(lineNo).value());

    for(QString point : points){
//        QHash<QString,QList<QString>>::Iterator lines= vectorLines.find(point);
          QList<QString> Lines=vectorLines[point];
        if(Lines.isEmpty() || Lines.size()==0){
            continue;
        }

        foreach(QString line,Lines){
            createSingleDevice(keyLine,keyVectors,vectorLines,line,deviceLines);
        }
    }

}

static QList<FAS_EntityContainer*> CreateDevcieContainers(QHash<QString,FAS_Entity*> keyLine,QHash<QString,QList<QString>> keyVectors,QHash<QString,QList<QString>> vectorLines){

    QList<FAS_EntityContainer*> suspectDeviceContainer;

    if(keyVectors.isEmpty()){
        return suspectDeviceContainer;
    }
     Log4Info("start create device");
    while(!keyVectors.isEmpty()){
        QHash<QString,QList<QString>>::Iterator itLineToPoint= keyVectors.begin();
        QString lineNo=itLineToPoint.key();
        QList<FAS_Entity*> deviceLines;
        createSingleDevice(keyLine,keyVectors,vectorLines,lineNo,deviceLines);
        FAS_EntityContainer* c=new FAS_EntityContainer();
        foreach (auto entity, deviceLines) {
            c->addEntity(entity->clone());
        }
        suspectDeviceContainer.append(c);
    }
    Log4Info("end create device");
   return suspectDeviceContainer;
}

static void removeSignleLinePoint(QHash<QString,QList<QString>> &keyVectors,QHash<QString,QList<QString>> &vectorLines,QString pointNo, QString lineNo){
    //取得点对应的线列表
//    if(!pointToLine.keys().contains(pointNo)){
//        return;
//    }
//    QHash<QString,QList<QString>>::Iterator listIt=pointToLine.find(pointNo);
//    if(listIt==pointToLine.end()){
//        return;
//    }
    QList<QString> list=vectorLines[pointNo];

    if(list.isEmpty() || list.size()==0){
        return;
    }

    //如果为1，并且在点对点表里没有数据，直接删除 如何判断为空 TODO
//    QHash<QString,QList<QString>>::Iterator it=vectorVectors.find("pointNo");//点点表，后期补充判空逻辑
    if(list.size()==1){
        QHash<QString,QList<QString>>::Iterator it=vectorLines.find(pointNo);
        vectorLines.erase(it);

        lineNo=list.first();


        QList<QString> points=keyVectors[lineNo];
        keyVectors.remove(lineNo);
        QString anotherPoint;
        foreach(QString point,points){
            if(point!=pointNo){
                anotherPoint=point;
                removeSignleLinePoint(keyVectors,vectorLines,anotherPoint, lineNo);
                //TODO
            }
        }
    }else if(list.size()>2){
        //如果当前点对应多条线，只删除确定删除线
        list.removeOne(lineNo);
        vectorLines.insert(pointNo,list);

    }else if(list.size()==2){
        //如果当前点对应多条线，只删除确定删除线
        list.removeOne(lineNo);
        vectorLines.insert(pointNo,list);
        removeSignleLinePoint(keyVectors,vectorLines,pointNo, "null");

    }else{
        //如果找不到数据
        QHash<QString,QList<QString>>::Iterator it=vectorLines.find(pointNo);
        vectorLines.erase(it);
        return;
    }
}

//过滤掉只有一个交点的线
static void FilterNoneBlockedLine(QHash<QString,QList<QString>> &keyVectors,QHash<QString,QList<QString>> &vectorLines){
    QHash<QString,QList<QString>>::iterator itPorintToLine;


     Log4Info("start filter data");
     QList<QString> kyes =vectorLines.keys();
    foreach(QString key,kyes){

        QList<QString> value=vectorLines[key];
        if(value.isEmpty() || value.size()==0){
            continue;
        }
            QString null="null";
            if(value.size()==1){
                removeSignleLinePoint(keyVectors,vectorLines,key,null);
            }
    }

    Log4Info("end filter data");

}

//建表进行实体元件映射处理，得到疑似设备
static QList<FAS_EntityContainer*> CreateDeviceWithMappingTableMethod(FAS_EntityContainer* AssociatedSetsContainer){
    QList<FAS_EntityContainer*> result;
    QList<FAS_Entity*> lines = AssociatedSetsContainer->getEntityList();

    QHash<QString,FAS_Entity*> keyLine;
//    建立【线ID-点列表】映射表
    QHash<QString,QList<QString>> keyVectors;
//    建立【端点-线ID列表】映射表
    QHash<QString,QList<QString>> vectorLines;
//    建立【端点-关联端点列表】映射表
    QHash<QString,QList<QString>> vectorVectors;

    //建立【线ID-线对象】映射表
    MappingEntity2HashCodeID(lines,keyLine,keyVectors,vectorLines);
    //建立【端点-关联端点列表】映射表
    vectorVectors=MappingVectors2Vector(vectorLines);

   //过滤非相交的线
    FilterNoneBlockedLine(keyVectors,vectorLines);

    return CreateDevcieContainers(keyLine,keyVectors,vectorLines);

}

static FAS_EntityContainer* CreatTempEntitiesContainer(QList<FAS_Entity*> entities){
    FAS_EntityContainer* tempContainer = new FAS_EntityContainer();
    for(FAS_Entity* entity:entities){
        tempContainer->addEntity(entity);
    }
    return tempContainer;
}



static FAS_BlockList* RecognizeSuspectNonBlockDeviceList(FAS_EntityContainer* &c,FASUtils* utils,QList<QList<FAS_Entity*>> atomicEntityList)
{
    FAS_BlockList* result=new FAS_BlockList(false);
    QList<FAS_EntityContainer*> SuspectedDeviceList;
    FAS_EntityContainer *circlesContainer, *linesContainer, *arcsContainer, *hatchContainer, *solidsContainer, *textsContainer
            ,*suspectEntities,*closeObjs;
    circlesContainer = CreatTempEntitiesContainer(atomicEntityList.at(0));
    linesContainer = CreatTempEntitiesContainer(atomicEntityList.at(1));
    arcsContainer = CreatTempEntitiesContainer(atomicEntityList.at(2));
    hatchContainer = CreatTempEntitiesContainer(atomicEntityList.at(3));
    solidsContainer = CreatTempEntitiesContainer(atomicEntityList.at(4));
    //result.size = FASUtils::getEntitySize(container);
    textsContainer=CreatTempEntitiesContainer(atomicEntityList.at(5));

    foreach(auto e,atomicEntityList.at(0)){
        //int er = utils->setNewCodeToEntityByID(e->getId(),QString::fromUtf8("De00-%1").arg(e->getId()));
        //qDebug()<<"测试设定原子实体对象"<<e->getId()<<"标号的结果："<<er;
    }
    foreach(auto e,atomicEntityList.at(1)){
        //int er = utils->setNewCodeToEntityByID(e->getId(),QString::fromUtf8("De00-%1").arg(e->getId()));
        //qDebug()<<"测试设定原子实体对象"<<e->getId()<<"标号的结果："<<er;
    }
    foreach(auto e,atomicEntityList.at(2)){
        //int er = utils->setNewCodeToEntityByID(e->getId(),QString::fromUtf8("De00-%1").arg(e->getId()));
        //qDebug()<<"测试设定原子实体对象"<<e->getId()<<"标号的结果："<<er;
    }

    FAS_EntityContainer*  ScatteredAtmoicEntities = new FAS_EntityContainer();
    //归原清洗
    QList<FAS_EntityContainer*> Origin20FilterResult = Origin20Filter(linesContainer);
    QList<FAS_Entity*> filteredOnes=Origin20FilterResult.last()->getEntityList();
    for(FAS_Entity* e:filteredOnes){
        ScatteredAtmoicEntities->addEntity(e);
    }
    suspectEntities=Origin20FilterResult.first();

    //散乱对象过滤，得到关联对象集&零散对象集
    //QList<FAS_EntityContainer*> ScatterringFilterResult = AssociatedAtomicEntityFilter(suspectEntities);
    QList<FAS_EntityContainer*> ScatterringFilterResult = AssociatedAtomicEntityFilter(suspectEntities);
    QList<FAS_Entity*> ScatteredOnes=ScatterringFilterResult.last()->getEntityList();
    for(FAS_Entity* e:ScatteredOnes){
        ScatteredAtmoicEntities->addEntity(e);
    }
    closeObjs=ScatterringFilterResult.first();

    //求解封闭对象集合
//    QList<FAS_EntityContainer*> AssociatedSetsParents = AssociatedSetsMappingToList(closeObjs);
//SuspectedDeviceList = AbsoluteRegionMergeFilter(AssociatedSetsParents,AssociatedSetsParents.last());


    //建表处理疑似设备
    QList<FAS_EntityContainer*> AssociatedSetsParents = CreateDeviceWithMappingTableMethod(linesContainer);
//    Log4Info("CreateDeviceWithMappingTableMethod(closeObjs) run times:",runTimes);

    //绝对融合
    SuspectedDeviceList = AbsoluteRegionMergeFilter(AssociatedSetsParents,ScatteredAtmoicEntities);
    int cnt = SuspectedDeviceList.count();
    //freeEntities = SuspectedDeviceList.at(cnt-1);
    for(int i = 0; i < cnt; i++)
    {
        FAS_BlockList* blockSource = new FAS_BlockList(false);

        FAS_Vector middlePoint=SuspectedDeviceList.at(i)->getMiddlePoint();
        QList<FAS_Entity*> entlist=SuspectedDeviceList.at(i)->getEntityList();
        QString name = QString::fromUtf8("$equip$%1%2").arg(entlist.first()->getId()).arg(entlist.last()->getId());

        FAS_Block* block = new FAS_Block(c, FAS_BlockData(name, FAS_Vector(middlePoint.x, middlePoint.y), false));
        block->initId();

        for(FAS_Entity* e:entlist){
            e->setPen(FAS_Pen(FAS_Color(0,255,0), FAS2::Width01, FAS2::SolidLine));
            block->addEntity(e);
        }
        blockSource->add(block);

        FAS_Vector ip(middlePoint.x, middlePoint.y);
        FAS_Vector sc(1, 1, 1);
        FAS_Vector sp(0, 0);
        //FAS_Insert* insert = new FAS_Insert(c,FAS_InsertData(name,ip, sc, 0,1, 1,sp, blockSource, FAS2::NoUpdate));
        FAS_Insert* insert = new FAS_Insert(c,FAS_InsertData(name,ip, sc, 0,1, 1,sp, blockSource, FAS2::NoUpdate));
        insert->initId();
        insert->setPen(FAS_Pen(FAS_Color(128,0,128), FAS2::Width01, FAS2::SolidLine));
        insert->appendEntity(block);

        bool isAddNewInsert = true;
        QList<FAS_Entity*> existingObjs=c->getEntityList();
        for(FAS_Entity* insObj:existingObjs){
            if(insObj->rtti() == FAS2::EntityInsert
                    &&FASUtils::isValidInsertEntity(insObj->getInsert())
                    && ((FAS_Insert*)insObj)->getName() == name)
            {
                isAddNewInsert = false;
                insert->clear();
                insert = insObj->getInsert();
                break;
            }
        }

        if(isAddNewInsert){
            c->addEntity(insert->getInsert());
        }
        FAS_Block* blk= insert->getBlockForInsert();
        if(!blk->isEmpty())
        {
            result->add(blk->getBlock());
        }
    }
    return result;
}

NBlockForm::NBlockForm(QWidget *parent, FASUtils* utils) :
    QDialog(parent),
    uiNBlockForm(new Ui::NBlockForm)
{
    uiNBlockForm->setupUi(this);
    FASUtils::setFontForUI(this);

    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint &
                                   ~Qt::WindowMinimizeButtonHint &
                                   ~Qt::WindowCloseButtonHint    &
                                   ~Qt::WindowTitleHint);
    this->utils = utils;
    init();

    //Log4Qt::LogManager::setHandleQtMessages(true);
    //qDebug("Hello World!");
    //qWarning("Hello Qt!");
    //qErrnoWarning("Hello Error！");
    Log4Info("Solution client Started!");
    FAS_EntityContainer *container = (FAS_EntityContainer*)utils->getRSDocument();
    QList<QList<FAS_Entity*>> atomicEntityList=utils->getExistAtomicEntityList(container);

    FAS_BlockList* blocklist=RecognizeSuspectNonBlockDeviceList(container,utils,atomicEntityList);
    init();
    //FAS_BlockList* blocklist = utils->getRSDocument()->getBlockList();
    int cnt = blocklist->count();

    FAS_Block* myBlock;
    QList<FAS_Block*> deviceBlocks, UnknownDeviceBlocks, notDeviceBlocks;
    for(int i = 0; i < cnt; i++)
    {
        myBlock = blocklist->at(i);
        //int er = utils->setNewCodeToEntityByID(myBlock->getId(),QString::fromUtf8("De00-%1").arg(myBlock->getId()));
        //qDebug()<<"设定自组合设备对象标号的结果："<<er;
        QString blockName = myBlock->getName();
        int count = nameToEntityHash.values(blockName).size();
        if (count > 0)
        {
        }
        DeviceInfo* type = COMMONDEF->getDefaultType();
        if(!myBlock->is_set())
        {
            DeviceFeature feature = FASUtils::getDeviceFeature(myBlock);
            type = utils->detectDeviceType(feature);
        }
        else
        {
            type = myBlock->getType();
        }
        type->setShowSVG(false);
        FASUtils::setBlcokType(myBlock,type);
        if(type->isValidDevice())
        {
            deviceBlocks.append(myBlock);
        }
        else if(type->isUnknownDevice())
        {
            UnknownDeviceBlocks.append(myBlock);
        }
        else if(type->isNotDevice())
        {
            notDeviceBlocks.append(myBlock);
        }
    }
    showBlockList = deviceBlocks + UnknownDeviceBlocks + notDeviceBlocks;
    QTableWidget* tableWidget = uiNBlockForm->tableWidget;
    tableWidget->setRowCount(showBlockList.count() + 1);
    int totalCount = 0;

    for(int ii = 0; ii < showBlockList.count(); ii++)
    {
        FAS_Block* oneBlock = showBlockList.at(ii);
        QString blockName = oneBlock->getName();
        QList<FAS_Insert*> devicesOfName = nameToEntityHash.values(blockName);
        int count = devicesOfName.size();
        totalCount += count;
        //**********************Device***************************************/
        FAS_Insert* oneDevice = devicesOfName.takeFirst();
        if(nullptr == oneDevice)
            continue;
        FAS_Insert* newInsert = oneDevice->clone()->getInsert();
        newInsert->getDeviceInfo()->setShowSVG(false);
        double angel=newInsert->getAngle();
        newInsert->setAngle(0.0);
        newInsert->update();
        newInsert->setSelected(false);
        FAS_EntityContainer* container = new FAS_EntityContainer();
        container->addEntity(newInsert);
        EntityQGGV* tgv = new EntityQGGV(this, 0, container);
        tgv->setPrintPreview(true);//set to true to diable absolute zero drawing
        tableWidget->setCellWidget(ii, 0, tgv);
        tgv->zoomAuto();
//        utils->doConvertDeviceToImage(oneBlock->getName(),"jpg",container,QSize(92,112));

         //**********************Device Type***************************************/
        QComboBox* tcomBox = new QComboBox(this);
        tcomBox->addItems(typeLocalStrList);
        DeviceInfo* type = oneBlock->getType();
        int index = typeStrList.indexOf(type->getName());
        tcomBox->setCurrentIndex(index >= 0? index : 0);
        tableWidget->setCellWidget(ii, 1, tcomBox);
        comBoxList.append(tcomBox);

        auto systemTypeStr = COMMONDEF->getSystem(true);
        connect(tcomBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, [=]( int ix ) {
            QString deviceName = typeStrList.at(comBoxList.at(ii)->currentIndex());
            DeviceInfo* tmpType = COMMONDEF->getDeviceInfoFromName(deviceName);
            if(tmpType->isValidDevice())
            {
                tmpType->setSystemType(systemTypeStr);
                FASUtils::setBlcokType(showBlockList.at(ii),tmpType);
            }else{
                tmpType->setSystemType();
                FASUtils::setBlcokType(showBlockList.at(ii),tmpType);
            }
            DeviceFeature feature = FASUtils::getDeviceFeature(showBlockList.at(ii));
            utils->addDetectRule(feature, tmpType->getName(),systemTypeStr);
        });

        //**********************Device Count***************************************/
        QTableWidgetItem* countItem = new QTableWidgetItem();
        countItem->setText(QString::number(count));
        countItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(ii, 2, countItem);
    }
    // show total count
    QTableWidgetItem* totalItem = new QTableWidgetItem();
    totalItem->setText(tr("Total count"));
    totalItem->setFlags(Qt::ItemIsEditable);
    tableWidget->setItem(showBlockList.count(), 1, totalItem);
    QTableWidgetItem* totalCountItem = new QTableWidgetItem();
    totalCountItem->setText(QString::number(totalCount));
    totalCountItem->setFlags(Qt::ItemIsEditable);
    tableWidget->setItem(showBlockList.count(), 2, totalCountItem);

    //resize width
    tableWidget->resizeColumnsToContents();
}

NBlockForm::~NBlockForm()
{
    delete uiNBlockForm;
}

void NBlockForm::init()
{
    showBlockList.clear();
    typeStrList = COMMONDEF->getDeviceTypeList();
    typeLocalStrList = COMMONDEF->getLocalDeviceTypeList();
    nameToEntityHash = utils->getBlockNameToEntityHash();
}

void NBlockForm::on_pBtnCancel_clicked()
{
    close();
}

void NBlockForm::on_pBtnOk_clicked()
{
    ThreadExcelGenerate* threadExcel = new ThreadExcelGenerate();
    connect(this, SIGNAL(signalSendTypeList(QList<DeviceInfo*>, QList<int>)), threadExcel, SLOT(updateDetectType(QList<DeviceInfo*>, QList<int>)));

    QList<DeviceInfo*> detectType;
    QList<int> typeCount;
    QTableWidget* tableWidget = uiNBlockForm->tableWidget;
    for(int i = 0; i < showBlockList.size(); i++)
    {
       FAS_Block* myBlock = showBlockList.at(i);

       QString deviceName = typeStrList.at(comBoxList.at(i)->currentIndex());
       DeviceInfo* tmpType = COMMONDEF->getDeviceInfoFromName(deviceName);
       detectType.append(tmpType);
       int count = tableWidget->item(i, 2)->text().toInt();
       typeCount.append(count);
       FASUtils::setBlcokType(myBlock,tmpType);
    }
    FASUtils::setAllDeviceEntityType(showBlockList, nameToEntityHash);

    emit signalSendTypeList(detectType, typeCount); //0707 ltree
    for (int i = 0; i < showBlockList.size(); i++)
    {
        //Update detect rule.
        DeviceFeature feature = FASUtils::getDeviceFeature(showBlockList.at(i));
        utils->addDetectRule(feature, detectType.at(i)->getName(),detectType.at(i)->getSystemType());
        WriteToText(feature, detectType.at(i));

        /*
        if(detectType.at(i)->isValidDevice())
        {
            //insert into database.
            FASUtils::storeDeviceListToDB(nameToEntityHash.values(showBlockList.at(i)->getName()));
        }*/
    }
    threadExcel->start();

    utils->detectDeviceBranch();
    utils->refreshGraphicView();

}


void NBlockForm::on_pBtnClose_clicked()
{
    close();
}
