/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef QG_GRAPHICVIEW_H
#define QG_GRAPHICVIEW_H

#include <QWidget>
#include <QDialog>
#include <QTextEdit>
#include "fas_graphicview.h"
#include "fas_layerlistlistener.h"
#include "qg_scrollbar.h"
#include "fas_actiondefault.h"
#include "zoomwidget.h"
class QGridLayout;
class QLabel;
class QMenu;
class QG_ScrollBar;

/*
 * This is the Qt implementation of a widget which can view a
 * graphic.
 */
class QG_GraphicView:   public FAS_GraphicView,
                        public FAS_LayerListListener
{
    Q_OBJECT

public:
    QG_GraphicView(QWidget* parent = 0, Qt::WindowFlags f = 0, FAS_Document* doc = nullptr);
    ~QG_GraphicView() override;

    void updateViewDocument(FAS_Document* doc);
    int getWidth() const override;
    int getHeight() const override;
    void redraw(FAS2::RedrawMethod method=FAS2::RedrawAll) override;
    void adjustOffsetControls() override;
    void adjustZoomControls() override;
    void setBackground(const FAS_Color& bg) override;
    void setMouseCursor(FAS2::CursorType c) override;
    void updateGridStatusWidget(const QString& text) override;

    virtual void getPixmapForView(std::unique_ptr<QPixmap>& pm);

    // Methods from FAS_LayerListListener Interface:
    void layerEdited(FAS_Layer*) override{
        redraw(FAS2::RedrawDrawing);
    }
    void layerRemoved(FAS_Layer*) override{
        redraw(FAS2::RedrawDrawing);
    }
    void layerToggled(FAS_Layer*) override{
        redraw(FAS2::RedrawDrawing);
    }
    void layerActivated(FAS_Layer *) override;
    void setOffset(int ox, int oy) override;
    // mouse position in widget coordinates
    FAS_Vector getMousePosition() const override;

    void setAntialiasing(bool state);
    void setCursorHiding(bool state);
    void addScrollbars();
    bool hasScrollbars();
    void setCurrentQAction(QAction* q_action);
    void destroyMenu(const QString& activator);
    void setMenu(const QString& activator, QMenu* menu);
    QString device;
    void keyPressEvent(QKeyEvent* e) override;
    void keyReleaseEvent(QKeyEvent* e) override;

    FAS_ActionDefault* getActionDefault()
    {
        return mActionDefault;
    }

protected:
    void wheelEvent(QWheelEvent* e) override;
    void mousePressEvent(QMouseEvent* e) override;
    void mouseReleaseEvent(QMouseEvent* e) override;
    void mouseMoveEvent(QMouseEvent* e) override;
    void mouseDoubleClickEvent(QMouseEvent* e) override;
    void leaveEvent(QEvent*) override;
    void enterEvent(QEvent*) override;
    void focusInEvent(QFocusEvent*) override;
    void focusOutEvent(QFocusEvent*) override;

    bool event(QEvent * e) override;

    void paintEvent(QPaintEvent *)override;
    void resizeEvent(QResizeEvent* e) override;
    QList<QAction*> recent_actions;

protected:
    // Horizontal scrollbar.
    QG_ScrollBar* hScrollBar;
    // Vertical scrollbar.
    QG_ScrollBar* vScrollBar;
    // Layout used to fit in the view and the scrollbars.
    QGridLayout* layout;
    // CAD mouse cursor
    std::unique_ptr<QCursor> curCad;
    // Delete mouse cursor
    std::unique_ptr<QCursor> curDel;
    // Select mouse cursor
    std::unique_ptr<QCursor> curSelect;
    // Magnifying glass mouse cursor
    std::unique_ptr<QCursor> curMagnifier;
    // Hand mouse cursor
    std::unique_ptr<QCursor> curHand;

    // Used for buffering different paint layers
    std::unique_ptr<QPixmap> PixmapLayer1;  // Used for grids and absolute 0
    std::unique_ptr<QPixmap> PixmapLayer2;  // Used for the actual CAD drawing
    std::unique_ptr<QPixmap> PixmapLayer3;  // Used for crosshair and actionitems
    FAS2::RedrawMethod redrawMethod;
    // Keep tracks of if we are currently doing a high-resolution scrolling
    bool isSmoothScrolling;
    QMap<QString, QMenu*> menus;
private:
    bool antialiasing{false};
    bool scrollbars{false};
    bool cursor_hiding{false};

    FAS_EntityContainer* document;
    FAS_ActionDefault* mActionDefault = nullptr;
    bool paintEventDrawFinished;//绘图完成
    bool mWheelEvent;//滚轮事件
    bool drawProgress;//画进度条
    ZoomWidget* zoomBtn;
    double mzoomFactor;

signals:
    void xbutton1_released();
    void gridStatusChanged(const QString&);
    void signalCreateMultiSelectedDialog();
    void signalSnapPointDone(FAS_Vector point);
    void signalDeviceSelect(FAS_Entity* entity);


public slots:
    void slotHScrolled(int value);
    void slotVScrolled(int value);
    void spreadSignalForMultiSelectDialog();
    void slotZoomRedraw();
};

#endif
