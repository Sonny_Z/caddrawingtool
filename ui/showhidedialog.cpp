#include "showhidedialog.h"
#include "ui_showhidedialog.h"

//sort with alphabetical order
#include <QMessageBox>

#include "commondef.h"

ShowHideDialog::ShowHideDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShowHideDialog)
{
    ui->setupUi(this);
    FASUtils::setFontForUI(this);
    init();
}

void ShowHideDialog::init()
{
    this->utils = FASUtils::getInstance();
    this->ui->cb_deviceType->addItems(COMMONDEF->getDeviceTypeList());
    QList<QString> strList;
    FAS_LayerList* layerList = utils->getRSDocument()->getLayerList();
    for(int ii = 0; ii < layerList->count(); ii++)
    {
        strList.append(layerList->at(ii)->getName());
    }
    this->ui->cb_layer->addItems(strList);
}

void ShowHideDialog::on_ShowAllButton_clicked()
{
    if(utils != nullptr)
    {
        utils->showAllDevices(true);
    }
}

void ShowHideDialog::on_HideAllButton_clicked()
{
    if(utils != nullptr)
    {
        utils->showAllDevices(false);
    }
}

void ShowHideDialog::on_ShowByTypeBut_clicked()
{
    QString index = ui->cb_deviceType->currentText();
    if(utils != nullptr)
    {
        utils->showDevicesByType(COMMONDEF->getDeviceInfoFromName(index), true);
    }
}

void ShowHideDialog::on_HideByTypeBut_clicked()
{
    QString index = ui->cb_deviceType->currentText();
    if(utils != nullptr)
    {
        utils->showDevicesByType(COMMONDEF->getDeviceInfoFromName(index), false);
    }
}
void ShowHideDialog::on_ShowByCodeBut_clicked()
{
    QString inputCode = ui->lineEditCode->text();
    if(!FASUtils::isValidDeviceCode(inputCode))
    {
        QMessageBox::information(this, tr("Warning"), tr("Please check input code."));
    }
    if(utils != nullptr)
    {
        utils->showDeviceByCode(inputCode, true);
    }
}
void ShowHideDialog::on_HideByCodeBut_clicked()
{
    QString inputCode = ui->lineEditCode->text();
    if(!FASUtils::isValidDeviceCode(inputCode))
    {
        QMessageBox::information(this, tr("Warning"), tr("Please check input code."));
    }
    if(utils != nullptr)
    {
        utils->showDeviceByCode(inputCode, false);
    }
}


void ShowHideDialog::on_TwinkleBut_clicked()
{
    bool twinkle = true;
    if(ui->butTwinkle->text() == this->twinkleStr)
    {
        twinkle = true;
    }
    else if(ui->butTwinkle->text() == this->unTwinkleStr)
    {
        twinkle = false;
    }

    QString inputCode = ui->lineEditCode->text();
    if(!FASUtils::isValidDeviceCode(inputCode))
    {
        QMessageBox::information(this, tr("Warning"), tr("Please check input code."));
    }
    if(utils != nullptr)
    {
        utils->twinkleEntity(inputCode, twinkle);
        if(twinkle)
        {
            ui->butTwinkle->setText(this->unTwinkleStr);
        }
        else
        {
            ui->butTwinkle->setText(this->twinkleStr);
        }
    }

}

void ShowHideDialog::on_ShowLayerBut_clicked()
{
    QString layerName = ui->cb_layer->currentText();
    if(utils != nullptr)
    {
        utils->showEntitiesByLayer(layerName, true);
    }
}

void ShowHideDialog::on_HideLayerBut_clicked()
{
    QString layerName = ui->cb_layer->currentText();
    if(utils != nullptr)
    {
        utils->showEntitiesByLayer(layerName, false);
    }
}

ShowHideDialog::~ShowHideDialog()
{
    delete ui;
}
