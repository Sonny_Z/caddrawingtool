#ifndef NEWVIEWDIALOG_H
#define NEWVIEWDIALOG_H

#include "ui_newviewdialog.h"
#include <QWidget>
#include <QDialog>
#include "QLabel"
#include <QPainter>
#include "fas_painter.h"
#include "fas_insert.h"
#include <QVector>
#include "entityqggv.h"
#include <QComboBox>
#include <QTextEdit>
#include <QtGlobal>
#include "fasutils.h"



namespace Ui {
class NewViewDialog;
}

class NewViewDialog : public QDialog
{
    Q_OBJECT

public:
    NewViewDialog(QWidget *parent, FASUtils* utils, bool isRename = false);
    ~NewViewDialog();
    void init();

private slots:
    void on_OKButton_clicked();

    void on_CancelButton_clicked();

    void slotPopUpMessage(const QString&);

private:
    Ui::NewViewDialog *uiNewViewDialog;

    bool isRename = false;
    QString curViewName;
    FASUtils* utils = nullptr;
    QList<FAS_Insert*> allDevices;
};

#endif // NEWVIEWDIALOG_H
