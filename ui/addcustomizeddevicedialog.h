#ifndef ADDCUSTOMIZEDDEVICEDIALOG_H
#define ADDCUSTOMIZEDDEVICEDIALOG_H

#include "ui_addcustomizeddevicedialog.h"
#include <QWidget>
#include <QDialog>
#include "QLabel"
#include <QPainter>
#include "fas_painter.h"
#include "fas_insert.h"
#include <QVector>
#include "entityqggv.h"
#include <QComboBox>
#include <QTextEdit>
#include <QtGlobal>
#include "fasutils.h"


namespace Ui {
class AddCustomizedDeviceDialog;
}

class AddCustomizedDeviceDialog : public QDialog
{
    Q_OBJECT

public:
    AddCustomizedDeviceDialog(QWidget *parent, FASUtils* utils/*, QList<FAS_Insert*> devices*/);
    ~AddCustomizedDeviceDialog();
    void init();

private slots:
    void on_OKButton_clicked();

    void on_CancelButton_clicked();

private:
    Ui::AddCustomizedDeviceDialog *uiAddCustomizedDeviceDialog;

    FASUtils* utils = nullptr;
    QList<FAS_Insert*> allDevices;
};

#endif // ADDCUSTOMIZEDDEVICEDIALOG_H
