 /****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "qg_graphicview.h"

//sort with alphabetical order
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QMenu>
#include <QDebug>
#include "zoomwidget.h"
#if QT_VERSION >= 0x050200
#include <QNativeGestureEvent>
#endif

#include "fas_actionzoomin.h"
#include "fas_actionzoompan.h"
#include "fas_document.h"
#include "fas_eventhandler.h"
#include "fas_painterqt.h"
#include "fas_modification.h"
#include "fas_mtext.h"
#include "fasutils.h"
#include "waitdrawprogressdialog.h"
#include <QMessageBox>
#define QG_SCROLLMARGIN 400

#ifdef Q_OS_WIN32
#define CURSOR_SIZE 16
#else
#define CURSOR_SIZE 15
#endif

QG_GraphicView::QG_GraphicView(QWidget* parent, Qt::WindowFlags f, FAS_Document* doc)
    :FAS_GraphicView(parent, f)
    ,device("Mouse")
    ,document(doc)
    ,curCad(new QCursor(QPixmap(":/icons/resource/cursor/cur_cad_bmp.png"), CURSOR_SIZE, CURSOR_SIZE))
    ,curDel(new QCursor(QPixmap(":/icons/resource/cursor/cur_del_bmp.png"), CURSOR_SIZE, CURSOR_SIZE))
    ,curSelect(new QCursor(QPixmap(":/icons/resource/cursor/cur_select_bmp.png"), CURSOR_SIZE, CURSOR_SIZE))
    ,curMagnifier(new QCursor(QPixmap(":/icons/resource/cursor/cur_glass_bmp.png"), CURSOR_SIZE, CURSOR_SIZE))
    ,curHand(new QCursor(QPixmap(":/icons/resource/cursor/cur_hand_bmp.png"), CURSOR_SIZE, CURSOR_SIZE))
    ,redrawMethod(FAS2::RedrawAll)
    ,isSmoothScrolling(false)
{
    FASUtils::setFontForUI(this);
    if (doc)
    {
        setContainer(doc);
        doc->setGraphicView(this);
        mActionDefault = new FAS_ActionDefault(*doc, *this);
        setDefaultAction(mActionDefault);
        connect(mActionDefault, SIGNAL(signalMultiSelectDone()), this, SLOT(spreadSignalForMultiSelectDialog()));
    }
    setFactorX(4.0);
    setFactorY(4.0);
    setBorders(10, 10, 10, 10);
    setMouseTracking(true);
    setFocusPolicy(Qt::NoFocus);
    setAttribute(Qt::WA_NoMousePropagation);
    view_rect = LC_Rect(toGraph(0, 0), toGraph(getWidth(), getHeight()));
    paintEventDrawFinished=false;
    mWheelEvent=false;
    drawProgress=false;
    mzoomFactor=1;
    if(COMMONDEF->wheelZoomShowWidgetNum)
    {
        zoomBtn=new ZoomWidget(this);
        zoomBtn->hide();
        connect(zoomBtn,SIGNAL(pressed()),this,SLOT(slotZoomRedraw()));
    }

}

void QG_GraphicView::updateViewDocument(FAS_Document* doc){
    if (doc)
    {
        doc->setOwner(false);
        if(document){
            document->setOwner(false);
            document->clear();
            document = doc;
            auto entities = doc->getEntityList();
            foreach(auto e, entities){
                document->addEntity(e);
            }
        }
        if(nullptr != mActionDefault){
            mActionDefault->deletePreview();
        }

        setContainer(doc);
        doc->setGraphicView(this);
        mActionDefault = new FAS_ActionDefault(*doc, *this);
        setDefaultAction(mActionDefault);
        connect(mActionDefault, SIGNAL(signalMultiSelectDone()), this, SLOT(spreadSignalForMultiSelectDialog()));
    }
}

QG_GraphicView::~QG_GraphicView()
{
    cleanUp();
}

int QG_GraphicView::getWidth() const
{
    if(COMMONDEF->firstOpen)
    {
        return 1257;

    }
    if (scrollbars)
        return width() - vScrollBar->sizeHint().width();
    else
        return width();
}

int QG_GraphicView::getHeight() const
{
    if(COMMONDEF->firstOpen)
    {
        return 626;

    }
    if (scrollbars)
        return height() - hScrollBar->sizeHint().height();
    else
        return height();
}

void QG_GraphicView::setBackground(const FAS_Color& bg)
{
    FAS_GraphicView::setBackground(bg);

    QPalette palette;
    palette.setColor(backgroundRole(), bg);
    setPalette(palette);
}

void QG_GraphicView::setMouseCursor(FAS2::CursorType c)
{
    switch (c)
    {
    default:
    case FAS2::ArrowCursor:
        setCursor(Qt::ArrowCursor);
        break;
    case FAS2::UpArrowCursor:
        setCursor(Qt::UpArrowCursor);
        break;
    case FAS2::CrossCursor:
        setCursor(Qt::CrossCursor);
        break;
    case FAS2::WaitCursor:
        setCursor(Qt::WaitCursor);
        break;
    case FAS2::IbeamCursor:
        setCursor(Qt::IBeamCursor);
        break;
    case FAS2::SizeVerCursor:
        setCursor(Qt::SizeVerCursor);
        break;
    case FAS2::SizeHorCursor:
        setCursor(Qt::SizeHorCursor);
        break;
    case FAS2::SizeBDiagCursor:
        setCursor(Qt::SizeBDiagCursor);
        break;
    case FAS2::SizeFDiagCursor:
        setCursor(Qt::SizeFDiagCursor);
        break;
    case FAS2::SizeAllCursor:
        setCursor(Qt::SizeAllCursor);
        break;
    case FAS2::BlankCursor:
        setCursor(Qt::BlankCursor);
        break;
    case FAS2::SplitVCursor:
        setCursor(Qt::SplitVCursor);
        break;
    case FAS2::SplitHCursor:
        setCursor(Qt::SplitHCursor);
        break;
    case FAS2::PointingHandCursor:
        setCursor(Qt::PointingHandCursor);
        break;
    case FAS2::ForbiddenCursor:
        setCursor(Qt::ForbiddenCursor);
        break;
    case FAS2::WhatsThisCursor:
        setCursor(Qt::WhatsThisCursor);
        break;
    case FAS2::OpenHandCursor:
        setCursor(Qt::OpenHandCursor);
        break;
    case FAS2::ClosedHandCursor:
        setCursor(Qt::ClosedHandCursor);
        break;
    case FAS2::CadCursor:
        cursor_hiding ? setCursor(Qt::BlankCursor) : setCursor(*curCad);
        break;
    case FAS2::DelCursor:
        setCursor(*curDel);
        break;
    case FAS2::SelectCursor:
        setCursor(*curSelect);
        break;
    case FAS2::MagnifierCursor:
        setCursor(*curMagnifier);
        break;
    case FAS2::MovingHandCursor:
        setCursor(*curHand);
        break;
    }
}

// Sets the text for the grid status widget in the left bottom corner.
void QG_GraphicView::updateGridStatusWidget(const QString& text)
{
   emit gridStatusChanged(text);
}

// Redraws the widget.
void QG_GraphicView::redraw(FAS2::RedrawMethod method)
{
    redrawMethod=(FAS2::RedrawMethod ) (redrawMethod | method);
    update(); // Paint when reeady to pain
    if(paintEventDrawFinished&&COMMONDEF->firstOpen&&(redrawMethod&FAS2::RedrawDrawing))
    {

            //paintEventDrawFinished=false;
            COMMONDEF->firstOpen=false;

    }
    //上次paintEvent画图完成并且滚动鼠标进入此段代码，
    if(paintEventDrawFinished&&(!COMMONDEF->progressShown)&&COMMONDEF->drawWheelEventProgress&&mWheelEvent&&(redrawMethod&FAS2::RedrawDrawing))
    {
            mWheelEvent=false;
            paintEventDrawFinished=false;
            // qDebug() <<"COMMONDEF->drawEntityCount:"<<COMMONDEF->drawEntityCount;
            if(COMMONDEF->drawEntityCount>20000)//
            {
                drawProgress=true;
                //
                QSize const s0(getWidth(), getHeight());
                if(COMMONDEF->pix->size()!=s0)
                {
                    delete COMMONDEF->pix;
                    COMMONDEF->pix=new QPixmap(getWidth(), getHeight());
                }
                //
                COMMONDEF->progressShown=true;
                WaitDrawProgressDialog dlg;
                dlg.setRange(0,100);
                dlg.setParas(this);
                dlg.setModal(false);
                dlg.exec();
                COMMONDEF->progressShown=false;
            }
            else
            {
                drawProgress=false;
            }


    }


}

void QG_GraphicView::resizeEvent(QResizeEvent* /*e*/)
{
    adjustOffsetControls();
    adjustZoomControls();
    getOverlayContainer(FAS2::Snapper)->clear();
    redraw();
}
void QG_GraphicView::mousePressEvent(QMouseEvent* event)
{
    // pan zoom with middle mouse button
    if (event->button()==Qt::MiddleButton)
    {
        setCurrentAction(new FAS_ActionZoomPan(*container, *this));
    }
    eventHandler->mousePressEvent(event);
     if(COMMONDEF->wheelZoomShowWidgetNum)
     {
         COMMONDEF->isWheelEvent=false;
         mzoomFactor=1;
         zoomBtn->hide();
     }

}

void QG_GraphicView::mouseDoubleClickEvent(QMouseEvent* e)
{
    switch(e->button())
    {
        default:
            break;
        case Qt::LeftButton:
            if (menus.contains("Double-Click"))
            {
                killAllActions();
                menus["Double-Click"]->popup(mapToGlobal(e->pos()));
            }
        case Qt::MiddleButton:
            FAS_GraphicView::zoomAuto();
            break;
    }
    e->accept();
}


void QG_GraphicView::mouseReleaseEvent(QMouseEvent* event)
{
    event->accept();
    switch (event->button())
    {
    case Qt::RightButton:
        eventHandler->mouseReleaseEvent(event);
//        if(eventHandler->en != nullptr && eventHandler->en->rtti() == FAS2::EntityInsert)
//        {
//            emit signalDeviceSelect(eventHandler->en);
//        }
        if(eventHandler->en != nullptr)
        {
            emit signalDeviceSelect(eventHandler->en);
        }
        if (event->modifiers()==Qt::ControlModifier)
        {
            if (menus.contains("Ctrl+Right-Click"))
            {
                menus["Ctrl+Right-Click"]->popup(mapToGlobal(event->pos()));
                break;
            }
        }
        if (event->modifiers()==Qt::ShiftModifier)
        {
            if (menus.contains("Shift+Right-Click"))
            {
                menus["Shift+Right-Click"]->popup(mapToGlobal(event->pos()));
                break;
            }
        }
        if (!eventHandler->hasAction())
        {
            if (menus.contains("Right-Click"))
            {
                menus["Right-Click"]->popup(mapToGlobal(event->pos()));
            }
            else if (!recent_actions.isEmpty())
            {
                QMenu* context_menu = new QMenu(this);
                context_menu->setAttribute(Qt::WA_DeleteOnClose);
                context_menu->addActions(recent_actions);
                context_menu->exec(mapToGlobal(event->pos()));
            }
        }
        else back();
        break;

    case Qt::XButton1:
        enter();
        emit xbutton1_released();
        break;
    default:
        emit signalSnapPointDone(toGraph(FAS_Vector(event->x(), event->y())));
        eventHandler->mouseReleaseEvent(event);
        break;
    }
}

void QG_GraphicView::mouseMoveEvent(QMouseEvent* event)
{
    event->accept();
    eventHandler->mouseMoveEvent(event);
}

bool QG_GraphicView::event(QEvent *event)
{
#if QT_VERSION >= 0x050200
    if (event->type() == QEvent::NativeGesture) {
        QNativeGestureEvent *nge = static_cast<QNativeGestureEvent *>(event);

        if (nge->gestureType() == Qt::ZoomNativeGesture) {
            double v = nge->value();
            FAS2::ZoomDirection direction;
            double factor;

            if (v < 0) {
                direction = FAS2::Out;
                factor = 1-v;
            } else {
                direction = FAS2::In;
                factor = 1+v;
            }

            // It seems the NativeGestureEvent::pos() incorrectly reports global coordinates
            QPoint g = mapFromGlobal(nge->globalPos());
            FAS_Vector mouse = toGraph(g.x(), g.y());
            setCurrentAction(new FAS_ActionZoomIn(*container, *this, direction,
                                                 FAS2::Both, &mouse, factor));
        }

        return true;
    }
#endif
    return QWidget::event(event);
}

void QG_GraphicView::leaveEvent(QEvent* e)
{
    eventHandler->mouseLeaveEvent();
    QWidget::leaveEvent(e);
}


void QG_GraphicView::enterEvent(QEvent* e)
{
    eventHandler->mouseEnterEvent();
    QWidget::enterEvent(e);
}


void QG_GraphicView::focusOutEvent(QFocusEvent* e)
{
    QWidget::focusOutEvent(e);
}


void QG_GraphicView::focusInEvent(QFocusEvent* e)
{
    eventHandler->mouseEnterEvent();
    QWidget::focusInEvent(e);
}

void QG_GraphicView::wheelEvent(QWheelEvent *e)
{

    if (container==nullptr)
    {
        return;
    }
    if(COMMONDEF->wheelZoomShowWidgetNum)
    {
        COMMONDEF->isWheelEvent=true;
    }

    FAS_Vector mouse = toGraph(e->x(), e->y());
    if (e->delta() == 0)
    {
        // A zero delta event occurs when smooth scrolling is ended. Ignore this
        e->accept();
        return;
    }

    bool scroll = false;
    FAS2::Direction direction = FAS2::Up;

    // scroll up / down:
    if (e->modifiers()==Qt::ControlModifier)
    {
        scroll = true;
        switch(e->orientation())
        {
        case Qt::Horizontal:
            direction=(e->delta()>0)?FAS2::Left:FAS2::Right;
            break;
        default:
        case Qt::Vertical:
            direction=(e->delta()>0)?FAS2::Up:FAS2::Down;
        }
    }

    // scroll left / right:
    else if	(e->modifiers()==Qt::ShiftModifier)
    {
        scroll = true;
        switch(e->orientation())
        {
        case Qt::Horizontal:
            direction=(e->delta()>0)?FAS2::Up:FAS2::Down;
            break;
        default:
        case Qt::Vertical:
            direction=(e->delta()>0)?FAS2::Left:FAS2::Right;
        }
    }

    double mfactor=1;
    if (scroll && scrollbars)
    {
        switch(direction)
        {
        case FAS2::Left:
        case FAS2::Right:
            hScrollBar->setValue(hScrollBar->value()+e->delta());
            break;
        default:
            vScrollBar->setValue(vScrollBar->value()+e->delta());
        }
        redraw();
    }

    // zoom in / out:
    else if (e->modifiers()==0)
    {
        /*
         * The zoomFactor effects how quickly the scroll wheel will zoom in & out.
         *
         * Benchmarks:
         * 1.250 - the original; fast & usable, but seems a choppy & a bit 'jarring'
         * 1.175 - still a bit choppy
         * 1.150 - smoother than the original, but still 'quick' enough for good navigation.
         * 1.137 - seems to work well for me
         * 1.125 - about the lowest that would be acceptable and useful, a tad on the slow side for me
         * 1.100 - a very slow & deliberate zooming, but feels very "cautious", "controlled", "safe", and "precise".
         * 1.000 - goes nowhere. :)
         */
        const double zoomFactor = 1.50;
        FAS_Vector mainViewCenter = toGraph(getWidth()/2, getHeight()/2);
        double zooScaleFactor = 0.0;
        FAS2::ZoomDirection zoomType = FAS2::In;
        if (e->delta()>0)
        {
            zooScaleFactor = 1.20;
            zoomType = FAS2::In;
            mfactor=zoomFactor;
        }
        else
        {
            zooScaleFactor = 0.30;
            zoomType = FAS2::Out;
            mfactor=(1.0/zoomFactor);
        }

        FAS_Vector effect{mouse};
        {
            effect-=mainViewCenter;
            effect.scale(zooScaleFactor);
            effect+=mainViewCenter;
        }

        if(COMMONDEF->wheelZoomShowWidgetNum)
        {

            if(mzoomFactor*mfactor>10)
            {
                return;
            }
        }
        setCurrentAction(new FAS_ActionZoomIn(*container, *this,
                                             zoomType, FAS2::Both,
                                             &effect,
                                             zoomFactor
                                            ));
        if(COMMONDEF->wheelZoomShowWidgetNum)
        {

            zoomBtn->setGeometry(e->x(), e->y(),zoomBtn->width(),zoomBtn->height());
            zoomBtn->show();
            mzoomFactor=mzoomFactor*mfactor;
            QString s=QString::number(qRound(mzoomFactor*100))+"%";
            zoomBtn->setText(s);


        }
        else
        {
            redraw();
        }
    }

    mWheelEvent=true;

    QMouseEvent* event = new QMouseEvent(QEvent::MouseMove,
                                         QPoint(e->x(), e->y()),
                                         Qt::NoButton, Qt::NoButton,
                                         Qt::NoModifier);
    eventHandler->mouseMoveEvent(event);
    delete event;
    e->accept();
}

// Called whenever the graphic view has changed.
void QG_GraphicView::adjustOffsetControls()
{
    if (scrollbars)
    {
        static bool running = false;
        if (running)
        {
                return;
        }
        running = true;

        if (container==nullptr || hScrollBar==nullptr || vScrollBar==nullptr)
        {
            return;
        }
        int ox = getOffsetX();
        int oy = getOffsetY();
        FAS_Vector min = container->getMin();
        FAS_Vector max = container->getMax();
        // no drawing yet - still allow to scroll
        if (max.x < min.x+1.0e-6 ||
                max.y < min.y+1.0e-6 ||
                    max.x > FAS_MAXDOUBLE ||
                    max.x < FAS_MINDOUBLE ||
                    min.x > FAS_MAXDOUBLE ||
                    min.x < FAS_MINDOUBLE ||
                    max.y > FAS_MAXDOUBLE ||
                    max.y < FAS_MINDOUBLE ||
                    min.y > FAS_MAXDOUBLE ||
                    min.y < FAS_MINDOUBLE )
        {
            min = FAS_Vector(-10,-10);
            max = FAS_Vector(100,100);
        }
        int minVal = (int)(-ox-getWidth()*0.5
                           - QG_SCROLLMARGIN - getBorderLeft());
        int maxVal = (int)(-ox+getWidth()*0.5
                           + QG_SCROLLMARGIN + getBorderRight());

        if (minVal<=maxVal)
        {
            hScrollBar->setRange(minVal, maxVal);
        }

        minVal = (int)(oy-getHeight()*0.5
                       - QG_SCROLLMARGIN - getBorderTop());
        maxVal = (int)(oy+getHeight()*0.5
                       +QG_SCROLLMARGIN + getBorderBottom());

        if (minVal<=maxVal)
        {
            vScrollBar->setRange(minVal, maxVal);
        }

        hScrollBar->setPageStep(getWidth());
        vScrollBar->setPageStep(getHeight());
        hScrollBar->setValue(-ox);
        vScrollBar->setValue(oy);
        slotHScrolled(-ox);
        slotVScrolled(oy);
        running = false;
    }
}

void QG_GraphicView::adjustZoomControls() {}

/*
 * Slot for horizontal scroll events.
 */
void QG_GraphicView::slotHScrolled(int value)
{
    if (hScrollBar->maximum()==hScrollBar->minimum())
    {
        centerOffsetX();
    }
    else
    {
        setOffsetX(-value);
    }
    if(COMMONDEF->wheelZoomShowWidgetNum&&COMMONDEF->isWheelEvent)
    {

    }
    else
    {
        redraw();
    }

}


/**
 * Slot for vertical scroll events.
 */
void QG_GraphicView::slotVScrolled(int value)
{
    if (vScrollBar->maximum()==vScrollBar->minimum())
    {
        centerOffsetY();
    }
    else
    {
        setOffsetY(value);
    }
    if(COMMONDEF->wheelZoomShowWidgetNum&&COMMONDEF->isWheelEvent)
    {

    }
    else
    {
        redraw();
    }
}

void QG_GraphicView::setOffset(int ox, int oy)
{
    FAS_GraphicView::setOffset(ox, oy);
    adjustOffsetControls();
}

FAS_Vector QG_GraphicView::getMousePosition() const
{
    //find mouse position
    QPoint vp=mapFromGlobal(QCursor::pos());
    //if cursor is not on widget, return the widget center position
    if(!rect().contains(vp))
        vp=QPoint(width()/2, height()/2);
    return toGraph(vp.x(), vp.y());
}

void QG_GraphicView::getPixmapForView(std::unique_ptr<QPixmap>& pm)
{
    QSize const s0(getWidth(), getHeight());
    if(pm && pm->size()==s0)
        return;
    pm.reset(new QPixmap(getWidth(), getHeight()));
}

void QG_GraphicView::layerActivated(FAS_Layer *layer)
{
    FAS_EntityContainer *container = this->getContainer();

    //allow undo cycle for layer change of selected
    FAS_AttributesData data;
    data.pen = FAS_Pen();
    data.layer = layer->getName();
    data.changeColor = false;
    data.changeLineType = false;
    data.changeWidth = false;
    data.changeLayer = true;
    FAS_Modification m(*container, this);
    m.changeAttributes(data);

    container->setSelected(false);
    redraw(FAS2::RedrawDrawing);
}

/**
 * Handles paint events by redrawing the graphic in this view.
 * usually that's very fast since we only paint the buffer we
 * have from the last call..
 */
void QG_GraphicView::paintEvent(QPaintEvent *)
{
    // Re-Create or get the layering pixmaps
    /*QLabel label;
    if(COMMONDEF->firstOpen)
    {
        COMMONDEF->firstOpen=false;
        label.setText(tr("CAD图块正在生成中，请等待..."));
        label.setGeometry(COMMONDEF->mainwindowCenterx-150,COMMONDEF->mainwindowCentery-150,300,200);
        label.setAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        label.show();
        QApplication::processEvents();



    }*/
    /*QMessageBox box;
    if(COMMONDEF->firstOpen)
    {
        COMMONDEF->firstOpen=false;
        box.setText(tr("CAD图块正在生成中，请等待..."));
        box.setGeometry(COMMONDEF->mainwindowCenterx-150,COMMONDEF->mainwindowCentery-150,300,200);
        box.show();
        QApplication::processEvents();
    }*/
    QTime uiPaintingStartTime = QTime::currentTime();
//    qDebug() << "UI painting start time:::" << uiPaintStartTime;
    QString appPerformanceLog = COMMONDEF->getAppPerformanceLog();
    QString uiPaintingStartTimeLogContent = QString::fromUtf8("UI painting start time:::%1\r\n").arg(uiPaintingStartTime.toString());
    COMMONDEF->saveToFile(uiPaintingStartTimeLogContent,appPerformanceLog,true);
//    qDebug() << uiPaintingStartTimeLogContent;
    if(!COMMONDEF->drawFinished)
    {
        return;
    }


    getPixmapForView(PixmapLayer1);
    getPixmapForView(PixmapLayer2);
    getPixmapForView(PixmapLayer3);

    // Draw Layer 1
    if (redrawMethod & FAS2::RedrawGrid)
    {
        PixmapLayer1->fill(background);
        FAS_PainterQt painter1(PixmapLayer1.get());
        drawLayer1((FAS_Painter*)&painter1);
        painter1.end();
    }

    if (redrawMethod & FAS2::RedrawDrawing)
    {
        view_rect = LC_Rect(toGraph(0, 0),
                            toGraph(getWidth(), getHeight()));
        // DRaw layer 2
        PixmapLayer2->fill(Qt::transparent);
        FAS_PainterQt painter2(PixmapLayer2.get());
        if (antialiasing)
        {
            painter2.setRenderHint(QPainter::Antialiasing);
        }
        painter2.setDrawingMode(drawingMode);
        painter2.setDrawSelectedOnly(false);
        if(COMMONDEF->drawWheelEventProgress&&drawProgress)
        {

              paintEventDrawFinished=true;


        }
        else
        {
            if(!COMMONDEF->firstOpen)
            {
                drawLayer2((FAS_Painter*)&painter2);
            }
            paintEventDrawFinished=true;

        }

        //painter2.setDrawSelectedOnly(true);
        //drawLayer2((FAS_Painter*)&painter2);
        painter2.end();
    }

    if (redrawMethod & FAS2::RedrawOverlay)
    {
        PixmapLayer3->fill(Qt::transparent);
        FAS_PainterQt painter3(PixmapLayer3.get());
        if (antialiasing)
        {
            painter3.setRenderHint(QPainter::Antialiasing);
        }

        drawLayer3((FAS_Painter*)&painter3);
        painter3.end();
    }

    // Finally paint the layers back on the screen, bitblk to the rescue!
    FAS_PainterQt wPainter(this);
    wPainter.drawPixmap(0,0,*PixmapLayer1);
    if(COMMONDEF->drawWheelEventProgress&&drawProgress)
    {
        QPixmap pix=*COMMONDEF->pix;
        wPainter.drawPixmap(0,0,pix);
    }
    else
    {
        if(COMMONDEF->firstOpen)
        {
            QPixmap pix=*COMMONDEF->pix;
            wPainter.drawPixmap(0,0,pix);


        }
        else
        {
            wPainter.drawPixmap(0,0,*PixmapLayer2);
        }
    }




    wPainter.drawPixmap(0,0,*PixmapLayer3);
    wPainter.end();

    redrawMethod=FAS2::RedrawNone;
    QTime uiPaintingEndTime = QTime::currentTime();
//    qDebug() << "UI painting end time:::" << uiPaintEndTime;
    QString uiPaintingEndTimeLogContent = QString::fromUtf8("UI painting end time:::%1\r\n").arg(uiPaintingEndTime.toString());
    COMMONDEF->saveToFile(uiPaintingEndTimeLogContent,appPerformanceLog,true);
    QString drawEntityCount = QString::fromUtf8("Draw entity count:::%1\r\n").arg(COMMONDEF->drawEntityCount);
    COMMONDEF->saveToFile(drawEntityCount,appPerformanceLog,true);
    QString uiPaintingPerfLogContent = QString::fromUtf8("UI painting performance time:::%1ms\r\n\r\n")
            .arg(uiPaintingStartTime.msecsTo(uiPaintingEndTime));
    COMMONDEF->saveToFile(uiPaintingPerfLogContent,appPerformanceLog,true);
//    qDebug() << uiPaintingEndTimeLogContent;
}

void QG_GraphicView::setAntialiasing(bool state)
{
    antialiasing = state;
}

void QG_GraphicView::addScrollbars()
{
    scrollbars = true;

    hScrollBar = new QG_ScrollBar(Qt::Horizontal, this);
    vScrollBar = new QG_ScrollBar(Qt::Vertical, this);
    layout = new QGridLayout(this);

    setOffset(50, 50);

    layout->setMargin(0);
    layout->setSpacing(0);
    layout->setColumnStretch(0, 1);
    layout->setColumnStretch(1, 0);
    layout->setColumnStretch(2, 0);
    layout->setRowStretch(0, 1);
    layout->setRowStretch(1, 0);

    hScrollBar->setSingleStep(50);
    hScrollBar->setCursor(Qt::ArrowCursor);
    layout->addWidget(hScrollBar, 1, 0);
    connect(hScrollBar, SIGNAL(valueChanged(int)),
            this, SLOT(slotHScrolled(int)));

    vScrollBar->setSingleStep(50);
    vScrollBar->setCursor(Qt::ArrowCursor);
    layout->addWidget(vScrollBar, 0, 1);
    connect(vScrollBar, SIGNAL(valueChanged(int)),
            this, SLOT(slotVScrolled(int)));
}

bool QG_GraphicView::hasScrollbars()
{
    return scrollbars;
}

void QG_GraphicView::setCursorHiding(bool state)
{
    cursor_hiding = state;
}

void QG_GraphicView::setCurrentQAction(QAction* q_action)
{
    eventHandler->setQAction(q_action);

    if (recent_actions.contains(q_action))
    {
        recent_actions.removeOne(q_action);
    }
    recent_actions.prepend(q_action);
}

void QG_GraphicView::destroyMenu(const QString& activator)
{
    if (menus.contains(activator))
    {
        auto menu = menus.take(activator);
        delete menu;
    }
}

void QG_GraphicView::setMenu(const QString& activator, QMenu* menu)
{
    destroyMenu(activator);
    menus[activator] = menu;
}

void QG_GraphicView::keyPressEvent(QKeyEvent* e)
{
    if (container==nullptr)
    {
        return;
    }
    eventHandler->keyPressEvent(e);
}

void QG_GraphicView::keyReleaseEvent(QKeyEvent* e)
{
    eventHandler->keyReleaseEvent(e);
}

void QG_GraphicView::spreadSignalForMultiSelectDialog()
{
    emit signalCreateMultiSelectedDialog();
}
void QG_GraphicView::slotZoomRedraw()
{
    zoomBtn->hide();
    this->redraw();
    mzoomFactor=1;

}
