#include "logindialog.h"

//sort with alphabetical order
#include <QMessageBox>
#include <QProcess>

#include "cadsqlite.h"
#include "commondef.h"
#include "fas_block.h"
#include "fasutils.h"

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    uiLoginDialog(new Ui::LoginDialog)
{
    uiLoginDialog->setupUi(this);
    FASUtils::setFontForUI(this);

    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint &
                                   ~Qt::WindowMinimizeButtonHint &
                                   ~Qt::WindowCloseButtonHint    &
                                   ~Qt::WindowTitleHint          &
                                   ~Qt::WindowContextHelpButtonHint);
    init();

}

LoginDialog::~LoginDialog()
{
    delete uiLoginDialog;
}

void LoginDialog::init()
{
    QString keyStr = "userName";
    auto localUserName = COMMONDEF->getAppSetting(keyStr);
    bool isNotValidUserName = localUserName.isNull() || localUserName.isEmpty();
    if(!isNotValidUserName){
        this->uiLoginDialog->lineEdit_UserName->setText(localUserName);
    }
}
//获取用户名
QString getUserName()
{
#if 1
    QStringList envVariables;
    envVariables << "USERNAME.*" << "USER.*" << "USERDOMAIN.*"
                 << "HOSTNAME.*" << "DOMAINNAME.*";
    QStringList environment = QProcess::systemEnvironment();
    foreach (QString string, envVariables) {
        int index = environment.indexOf(QRegExp(string));
        if (index != -1) {
            QStringList stringList = environment.at(index).split('=');
            if (stringList.size() == 2) {
                return stringList.at(1);
                break;
            }
        }
    }
    return "unknown";
#else
    QString userName = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    userName = userName.section("/", -1, -1);
    return userName;
#endif
}
void LoginDialog::on_OKButton_clicked()
{

//    QString domainName = info.localDomainName();
//    auto localHostName = info.localHostName();

//    auto localUserName = getUserName();

    auto userName = this->uiLoginDialog->lineEdit_UserName->text();
    userName = userName.replace("\\","/");
    /*if(!COMMONDEF->isValidDomain(domainName)){
        QMessageBox::information(this, tr("Warning"), tr("Invalid Domain Name!"));
        this->uiLoginDialog->lineEdit_UserName->setText("");
    }else */
    auto userNameChecking = userName;
    if(userNameChecking.replace("carcgl/","").isNull() || userNameChecking.replace("carcgl/","").isEmpty()){
        QMessageBox::information(this, tr("Warning"), tr("Invalid Account!"));
    }
    else{
//        if(COMMONDEF->userNameValidation(userName.toUtf8())){
            COMMONDEF->setAppSetting("userName",userName.toUtf8());
//            COMMONDEF->initializationValidation();
            this->accept();
            close();
//        }
//        else{
////            QMessageBox::information(this, tr("Warning"), tr("User name is incorrect. It must be exactly the same as the model selection system!"));
//            QMessageBox::information(this, tr("Warning"), tr("用户名不正确，系统要求识图工具用户名必须和选型系统完全一致！"));
//        }
    }

}

void LoginDialog::on_CancelButton_clicked()
{
    exit(-1);
}
