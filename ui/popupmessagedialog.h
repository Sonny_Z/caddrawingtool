#ifndef POPUPMESSAGEDIALOG_H
#define POPUPMESSAGEDIALOG_H

#include <QDialog>

namespace Ui {
class PopUpMessageDialog;
}

class PopUpMessageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PopUpMessageDialog(QWidget *parent = nullptr);
    ~PopUpMessageDialog();

    void setMessage(QString message);

private:
    Ui::PopUpMessageDialog *ui;
};

#endif // POPUPMESSAGEDIALOG_H
