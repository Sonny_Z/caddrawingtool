#include "popupmessagedialog.h"
#include "ui_popupmessagedialog.h"

PopUpMessageDialog::PopUpMessageDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PopUpMessageDialog)
{
    ui->setupUi(this);
    ui->label_message->adjustSize();
}

void PopUpMessageDialog::setMessage(QString message){
    auto formatMess = QString::fromUtf8("<html><head/><body><p><span style=\" font-size:16pt;\">%1</span></p></body></html>")
            .arg(message);
    ui->label_message->setText(formatMess);
    ui->label_message->adjustSize();
}

PopUpMessageDialog::~PopUpMessageDialog()
{
    delete ui;
}
