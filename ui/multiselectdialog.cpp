#include "multiselectdialog.h"

//sort with alphabetical order
#include <QMessageBox>

#include "cadsqlite.h"
#include "commondef.h"
#include "fas_block.h"
#include "fasutils.h"

MultiSelectDialog::MultiSelectDialog(QWidget *parent, FASUtils* utils, QList<FAS_Insert*> devices) :
    QDialog(parent),
    utils(utils),
    allDevices(devices),
    uiMultiSelectedDialog(new Ui::MultiSelectDialog)
{
    uiMultiSelectedDialog->setupUi(this);
    FASUtils::setFontForUI(this);

    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint &
                                   ~Qt::WindowMinimizeButtonHint &
                                   ~Qt::WindowCloseButtonHint    &
                                   ~Qt::WindowTitleHint);
    init();

    int deviceCount = sortedDeviceList.count();
    QTableWidget* tableWidget = uiMultiSelectedDialog->tableWidget;
    tableWidget->setRowCount(deviceCount);

    QList<FAS_Entity*> noteEntityList = utils->getAllNoteEntityHash().values();
    QHash<QString, FAS_Entity*> codeEntityHash = utils->getAllCodeEntityHash();
    for(int i = 0; i < deviceCount; i++)
    {
         FAS_Insert* device = sortedDeviceList.at(i);
        idToDeviceMap.insert(i+1, device);
        /*** Id ******************************************/
        QTableWidgetItem* idItem = new QTableWidgetItem();
        idItem->setText(QString::number(i+1));
        idItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(i, 0, idItem);

        /*** Device ******************************************/
        FAS_EntityContainer* container = new FAS_EntityContainer();
        FAS_Insert* newInsert = device->clone()->getInsert();
        newInsert->setSelected(false);
        newInsert->setAngle(0.0);
        newInsert->update();
        container->addEntity(newInsert);
        EntityQGGV* entityView = new EntityQGGV(this, 0, container);
        entityView->setPrintPreview(true);//set to true to diable absolute zero drawing

        tableWidget->setCellWidget(i, 1, entityView);
        entityView->zoomAuto();

        // Code ******************************************
        QString deviceCode = device->getInsert()->getDeviceInfo()->getCodeInfo();
        if(codeEntityHash.find(deviceCode) == codeEntityHash.end())
            deviceCode = "";
        QTableWidgetItem* codeItem = new QTableWidgetItem();
        codeItem->setText(deviceCode);
        tableWidget->setItem(i, 2, codeItem);

        // Type ******************************************
        QString strType = device->getInsert()->getDeviceInfo()->getName();

        QTableWidgetItem* typeItem = new QTableWidgetItem();
        typeItem->setText(strType);
        typeItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(i, 3, typeItem);

        // Coordniate ******************************************
        FAS_Vector xy = device->getInsert()->getMiddlePoint();
        QString coordStr = "(" + QString::number(xy.x) + "," + QString::number(xy.y) + ")";

        QTableWidgetItem* coordItem = new QTableWidgetItem();
        coordItem->setText(coordStr);
        coordItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(i, 4, coordItem);

        //*** Description*********************************
        QString note = device->getDeviceInfo()->getNoteText();
        if(0 == note.size())
        {
            utils->findNoteEntityForDevice(device, noteEntityList, note);
        }

        QTableWidgetItem* noteItem = new QTableWidgetItem();
        noteItem->setText(note);
        tableWidget->setItem(i, 5, noteItem);
    }
    tableWidget->resizeColumnsToContents();
}

MultiSelectDialog::~MultiSelectDialog()
{
    delete uiMultiSelectedDialog;
}

void MultiSelectDialog::init()
{
    sortedDeviceList = utils->sortDeviceListByCoordinate(allDevices);
    typeLocalStrList = COMMONDEF->getLocalDeviceTypeList();
}

void MultiSelectDialog::on_OK_clicked()
{
    QTableWidget* tableWidget = uiMultiSelectedDialog->tableWidget;
    for(int ii = 0; ii < sortedDeviceList.count(); ii++)
    {
        QString idStr = tableWidget->item(ii,0)->text();
        QString codeStr = tableWidget->item(ii,2)->text();
        QString noteStr = tableWidget->item(ii,5)->text();
        int id = idStr.toInt();
        if(id > 0)
        {
            FAS_Insert* device = idToDeviceMap.find(id).value();
            if(device != nullptr && utils != nullptr)
            {
                int error = utils->setNewCodeToDevice(device, codeStr, noteStr);
                if(1 == error)
                {
                    QMessageBox::information(this, tr("Warning"), tr("Unexpected error occurs."));
                    return;
                }
                else if(2 == error)
                {
                    QMessageBox::information(this, tr("Warning"), tr("Input code does not match the rule."));
                    return;
                }
                else if(3 == error)
                {
                    QMessageBox::information(this, tr("Warning"), tr("There is an existing code with inut code. Please input another."));
                    return;
                }
                else if(4 == error)
                {
                    QMessageBox::information(this, tr("Warning"), tr("Please set layer for code text in 'Tool->Setting'."));
                    return;
                }
            }
        }
    }
    close();
}


void MultiSelectDialog::on_QuickEncode_clicked()
{
    QString initialCodeStr = uiMultiSelectedDialog->lineEdit_initialCode->text();
    if(!FASUtils::isValidDeviceCode(initialCodeStr))
    {
        QMessageBox::information(this, tr("Warning"), tr("Initial code does not match the rule."));
    }
    QString loopStr;
    int codeNum = 0;
    int codeSize = 0;
    if(initialCodeStr.contains("-"))
    {
        QList<QString> strList = initialCodeStr.split("-");
        if(2 == strList.size())
        {
            loopStr = strList.at(0) + "-";
            QString codeStr = strList.at(1);
            for(auto str : codeStr)
            {
                if(!str.isNumber())
                {
                    QMessageBox::information(this, tr("Warning"), tr("Initial code does not match the rule."));
                    return;
                }
            }
            codeNum = codeStr.toInt();
            codeSize = codeStr.size();
        }
    }
    else
    {
        for(auto str : initialCodeStr)
        {
            if(!str.isNumber())
            {
                QMessageBox::information(this, tr("Warning"), tr("Initial code does not match the rule."));
                return;
            }
        }
        codeNum = initialCodeStr.toInt();
        codeSize = initialCodeStr.size();
    }

    QTableWidget* tableWidget = uiMultiSelectedDialog->tableWidget;
    for(int ii = 0; ii < tableWidget->rowCount(); ii++)
    {
        QString newCode = loopStr + QString("%1").arg(codeNum++, codeSize, 10, QChar('0'));
        tableWidget->item(ii, 2)->setText(newCode);
    }
}

void MultiSelectDialog::on_statistics_clicked()
{
    QHash<int, QList<FAS_Insert*>> typeEntitiesHash = utils->classifyDevicesByType(sortedDeviceList);
    QList<int> typeList = typeEntitiesHash.keys();
    qSort(typeList.begin(), typeList.end());
    QString messageText = tr("Statistics is below:") + "\n";
    int totalCount = 0;
    for(int type : typeList)
    {
        QString deviceName = COMMONDEF->getDeviceInfoFromID(type)->getName();
        deviceName = COMMONDEF->translateToLocal(deviceName);
        int count = typeEntitiesHash.find(type).value().count();
        messageText = messageText + deviceName + " : " + QString::number(count) + "\n";
        if(type != 9999 && type != 9998)
            totalCount += count;
    }
    messageText += tr("Total count of device: ") + QString::number(totalCount);
    QMessageBox::information(this, tr("Statistics"), messageText);
}
