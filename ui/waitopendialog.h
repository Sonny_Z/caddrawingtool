#ifndef WAITOPENDIALOG_H
#define WAITOPENDIALOG_H

#include <QDialog>
#include "fasutils.h"
#include "fas_units.h"
#include "threaddxfprocess.h"
namespace Ui {
class WaitOpenDialog;
}

class WaitOpenDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WaitOpenDialog(QWidget *parent = nullptr);
    ~WaitOpenDialog();
    void setParas(QString title,FASUtils* utils);
public slots:
    void slotTimeOut();
private:
    Ui::WaitOpenDialog *ui;
    QString file;
    FASUtils* utils;
    QTimer* timer;
    ThreadDxfProcess* thread;
};

#endif // WAITOPENDIALOG_H
