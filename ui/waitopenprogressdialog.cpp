#include "waitopenprogressdialog.h"
#include <QTimer>
WaitOpenProgressDialog::WaitOpenProgressDialog(QWidget *parent) :
    MyProgressDialog(parent)
{
    timer=new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(slotTimeOut()));
    timer->setSingleShot(true);
    timer->start(50);


    this->setWindowFlag(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setWindowOpacity(0.8);

    thread=new ThreadDxfProcess(this);
    connect(thread,SIGNAL(dxfOpenSuccess()),this,SLOT(soltOpenSuccess()));
    connect(thread,SIGNAL(dxfOpenFailed()),this,SLOT(slotOpenFailed()));

    timer1=new QTimer(this);
    connect(timer1,SIGNAL(timeout()),this,SLOT(ontimer1()));
    lines=0;
}

WaitOpenProgressDialog::~WaitOpenProgressDialog()
{

}
void WaitOpenProgressDialog::slotTimeOut()
{
    timer->stop();
    QFile data(file);
    int mcount=0;
     if (data.open(QFile::ReadOnly))
     {
         QTextStream out(&data);
         while(!out.atEnd())
         {
              out.readLine();
              mcount++;
         }
     }
     lines=mcount;
    thread->setParas(file,utils);
    thread->start();
    timer1->start(50);

}

void WaitOpenProgressDialog::setParas(QString file,FASUtils* utils)
{
    this->file=file;
    this->utils=utils;
}

void WaitOpenProgressDialog::ontimer1()
{
    int v=(100*COMMONDEF->readLineNumber)/lines;
    this->setValue(v);
}
void WaitOpenProgressDialog::soltOpenSuccess()
{
    timer1->stop();
    this->setValue(100);
    this->accept();
}
void WaitOpenProgressDialog::slotOpenFailed()
{
    timer1->stop();
    this->setValue(100);
    this->reject();
}
