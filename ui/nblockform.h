#ifndef NBLOCKFORM_H
#define NBLOCKFORM_H


#include <QComboBox>
#include <QDialog>

#include "fas_painter.h"
#include "fasutils.h"

namespace Ui {
class NBlockForm;
}

class NBlockForm : public QDialog
{
    Q_OBJECT

public:
    NBlockForm(QWidget *parent, FASUtils* utils);
    ~NBlockForm();
    void init();
signals:
    void signalSendTypeList(QList<DeviceInfo*> typeList, QList<int> countList);

private slots:
    void on_pBtnCancel_clicked();
    void on_pBtnOk_clicked();

    void on_pBtnClose_clicked();

private:

    Ui::NBlockForm *uiNBlockForm;

    QStringList typeStrList;
    QStringList typeLocalStrList;
    QList<FAS_Block*> showBlockList;
    QList<QComboBox*> comBoxList;
    QMultiHash<QString, FAS_Insert*> nameToEntityHash;

    FASUtils* utils;
};
#endif // NBLOCKFORM_H
