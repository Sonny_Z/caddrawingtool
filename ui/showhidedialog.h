#ifndef SHOWHIDEDIALOG_H
#define SHOWHIDEDIALOG_H

#include <QDialog>
#include "fasutils.h"

namespace Ui {
class ShowHideDialog;
}

class ShowHideDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ShowHideDialog(QWidget *parent = 0);
    ~ShowHideDialog();

    void init();

private slots:
    void on_ShowAllButton_clicked();
    void on_HideAllButton_clicked();
    void on_ShowByTypeBut_clicked();
    void on_HideByTypeBut_clicked();
    void on_ShowByCodeBut_clicked();
    void on_HideByCodeBut_clicked();
    void on_ShowLayerBut_clicked();
    void on_HideLayerBut_clicked();
    void on_TwinkleBut_clicked();

private:
    Ui::ShowHideDialog *ui;
    FASUtils* utils;

    QString twinkleStr = tr("Twinkle");
    QString unTwinkleStr = tr("Untwinkle");
};

#endif // SHOWHIDEDIALOG_H
