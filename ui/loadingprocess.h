#ifndef LOADINGPROCESS_H
#define LOADINGPROCESS_H

#include <QDialog>

namespace Ui {
class loadingProcess;
}

class loadingProcess : public QDialog
{
    Q_OBJECT

public:
    explicit loadingProcess(QWidget *parent = nullptr);
    ~loadingProcess();

private:
    Ui::loadingProcess *ui;
};

#endif // LOADINGPROCESS_H
