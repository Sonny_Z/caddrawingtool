#include "adddevicetypedialog.h"
#include "ui_adddevicetypedialog.h"

//sort with alphabetical order
#include <QMessageBox>
#include <QFileDialog>

#include "fasutils.h"

AddDeviceTypeDialog::AddDeviceTypeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDeviceTypeDialog)
{
    ui->setupUi(this);
    FASUtils::setFontForUI(this);
}

AddDeviceTypeDialog::~AddDeviceTypeDialog()
{
    delete ui;
}

void AddDeviceTypeDialog::on_explorer_clicked()
{
    QString filter = "SVG file(*.svg)";
    QString file = QFileDialog::getOpenFileName(this, "Choose SVG file", ".", filter);

    this->ui->lineEdit_SVGPath->setText(file);
}

void AddDeviceTypeDialog::on_OK_clicked()
{
    QString deviceName = this->ui->lineEdit_DeviceName->text();
    QString svgPath = this->ui->lineEdit_SVGPath->text();
    if(deviceName.size() == 0  || svgPath.size() == 0)
    {
        QMessageBox::information(this, tr("Warning"), tr("The information is not complete"));
    }

    int error = FASUtils::addNewDeviceType(deviceName, svgPath);
    if(1 == error)
    {
        QMessageBox::information(this, tr("Warning"), tr("The input device name already exists."));
    }
    else if (2 == error)
    {
        QMessageBox::information(this, tr("Warning"), tr("The SVG file cannot be copied."));
        return;
    }
    else
    {
        QMessageBox::information(this, tr("Information"), tr("New device type is added successfully."));
        return;
    }
    this->close();
}


