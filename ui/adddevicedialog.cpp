#include "adddevicedialog.h"
#include "ui_adddevicedialog.h"

//sort with alphabetical order
#include <QMessageBox>

#include "commondef.h"
#include "fas_insert.h"
#include "fas_mtext.h"
#include "fas_vector.h"

AddDeviceDialog::AddDeviceDialog(QWidget *parent, FASUtils* utils) :
    QDialog(parent),
    ui(new Ui::AddDeviceDialog)
{
    ui->setupUi(this);
    FASUtils::setFontForUI(this);
    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint &
                                   ~Qt::WindowMinimizeButtonHint &
                                   ~Qt::WindowCloseButtonHint);
    this->utils = utils;
    initUI();
}

void AddDeviceDialog::initUI()
{
    QHash<QString, QString> deviceNameToSvgFileMap = COMMONDEF->getNameToSvgFilePathHash();

    FAS_BlockList* blocklist = utils->getRSDocument()->getBlockList();
    for(int ii = 0; ii < blocklist->count(); ii++)
    {
        FAS_Block* oneBlock = blocklist->at(ii);
        int count = utils->getBlockNameToEntityHash().values(oneBlock->getName()).size();
        if(0 == count)
            continue;
        DeviceInfo* type = COMMONDEF->getDefaultType();
        if(!oneBlock->is_set())
        {
            type = utils->detectDeviceType(FASUtils::getDeviceFeature(oneBlock));
        }
        else
        {
            type = oneBlock->getType();
        }
        if(type->isSvgEntity())
        {
            QString deviceName = type->getName();
            QPixmap pixmap(deviceNameToSvgFileMap.value(deviceName));
            QIcon icon(pixmap);
            ui->cb_deviceType->addItem(icon, deviceName);
            nameToBlockMap.insert(deviceName, oneBlock);
        }
    }

    ui->lineEdit_coordX->setText("0.0");
    ui->lineEdit_coordY->setText("0.0");
}

AddDeviceDialog::~AddDeviceDialog()
{
    delete ui;
}

void AddDeviceDialog::on_OK_clicked()
{
    if(nullptr == utils)
    {
        QMessageBox::information(this, tr("Warning"), tr("Failed!"));
        return;
    }
    QString name = ui->cb_deviceType->currentText();
    QString codeText = ui->lineEdit_code->text();
    double coordX = ui->lineEdit_coordX->text().toDouble();
    double coordY = ui->lineEdit_coordY->text().toDouble();

    if(!FASUtils::isValidDeviceCode(codeText))
    {
        QMessageBox::information(this, tr("Warning"), tr("The code text does not match rule."));
        return;
    }
    if(coordX ==0 || coordY == 0)
    {
        QMessageBox::information(this, tr("Warning"), tr("The position does not match rule."));
        return;
    }

    FAS_Block* block = nameToBlockMap.find(name).value();

    int error = utils->addOneDeviceInGraphic(block, FAS_Vector(coordX, coordY), codeText);
    disconnect(utils->getGraphicView(), SIGNAL(signalSnapPointDone(FAS_Vector)), this, SLOT(snapPointDone(FAS_Vector)));
    if(2 == error)
    {
        QMessageBox::information(this, tr("Warning"), tr("Input code does not match the rule."));
        return;
    }
    else if(3 == error)
    {
        QMessageBox::information(this, tr("Warning"), tr("Input code is already assigned to another device."));
        return;
    }
    else if(4 == error)
    {
        QMessageBox::information(this, tr("Warning"), tr("Please set layer for code in Tools->Setting firstly."));
        return;
    }
    else if(1 == error)
    {
        QMessageBox::information(this, tr("Error"), tr("Unexpected error occurs."));
        return;
    }
    close();
}

void AddDeviceDialog::on_Cancel_clicked()
{
    disconnect(utils->getGraphicView(), SIGNAL(signalSnapPointDone(FAS_Vector)), this, SLOT(snapPointDone(FAS_Vector)));
    close();
}


void AddDeviceDialog::on_snapPoint_clicked()
{
    this->hide();
}

void AddDeviceDialog::snapPointDone(FAS_Vector point)
{
    this->show();
    this->ui->lineEdit_coordX->setText(QString::number(point.x));
    this->ui->lineEdit_coordY->setText(QString::number(point.y));
}


