#ifndef DRAWNOTES_H
#define DRAWNOTES_H

#include <QDialog>
#include "fasutils.h"

namespace Ui {
class DrawNotes;
}

class DrawNotes : public QDialog
{
    Q_OBJECT

public:
    DrawNotes(QWidget *parent, FASUtils* utils);
    ~DrawNotes();

private slots:
    void on_btnOk_clicked();
    void on_btnCancel_clicked();
    void on_snapPoint_clicked();

    void snapPointDone(FAS_Vector point);

private:
    Ui::DrawNotes *uiDrawNotes;

    FASUtils* utils = nullptr;

};

#endif // DRAWNOTES_H
