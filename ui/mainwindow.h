#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QWidget>
#include <QListWidget>
#include <QTranslator>
#include <QtWidgets>
#include "fasutils.h"
#include "fas_graphicview.h"
#include "newviewdialog.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    static MainWindow* getAppWindow()
    {
        return appWindow;
    }

    void setUndoEnable(bool enbale);
    void setRedoEnable(bool enable);

private slots:
    void closeMainWindow();
    void on_actionSave_triggered();
    void on_actionSave_As_triggered();
    void on_actionExport_as_image_triggered();
    void on_actionExport_as_PDF_triggered();
    void on_actionClose_File_triggered();
    void on_actionSave_Devices_to_Database_triggered();

    void on_actionUndo_triggered();
    void on_actionRedo_triggered();
    void on_actionDelete_triggered();
    void on_actionCheckLayer_triggered();
    void on_actionDraw_Note_triggered();
    void on_actionAdd_One_Device_triggered();
    void on_actionConnectTwoDevices_triggered();
    void on_actionAuto_Zoom_triggered();

    void on_actionSwitchSVGorCAD_triggered();
    void on_actionFindDevice_triggered();
    void on_actionShowHide_triggered();
    void on_actionAutoCode_triggered();
    void on_actionAdd_New_Device_Type_triggered();
    void on_actionCalculateWireLength_triggered();
    void on_actionSetting_triggered();

    void on_actionChinese_triggered();
    void on_actionEnglish_triggered();

//    void on_activeDrawing_triggered(QAction* action);

    void createMultiSelectedDialog();
    void createInformationDialog(FAS_Entity* );
    void doCloseFile();
    void fileAutoSave();
    void connectGraphicViewSlot();

    void on_actionShow_nBlockList_triggered();
    void on_actionto_LocalDB_triggered();

    void on_actionto_Cloud_triggered();

    void on_actionOpen_triggered();

    void on_actionIES_Device_Block_triggered();

//    void on_actionIES_Device_nBlock_triggered();

    void on_actionD_A_Device_Block_triggered();

//    void on_actionD_A_Device_nBlock_triggered();

    void on_actionSplit_Show_triggered();

    void on_subViewSelected_triggered(QString subViewName);

//    void receiverMouseControlSizeSIGNAL();

    void on_floorNameEdit_triggered(QListWidgetItem *item);
    void slotZoomAuto();

    void on_actionDelete_View_triggered();

    void on_actionAuto_Split_View_triggered();

    void on_action0_0_9_triggered();

    void on_action0_0_10_triggered();

    void on_action0_0_11_triggered();

    void on_action0_0_12_triggered();

    void on_actionUpload_All_Result_triggered();

    void on_actionUpload_Single_Result_triggered();


    void on_action0_0_13_triggered();

    void updateStatusBar(QString statusContent);

    void on_action0_0_14_triggered();

    void on_action0_0_15_triggered();

    void on_actionCheck_Single_Result_triggered();

    void on_actionCheck_All_Result_triggered();

    void on_action0_0_16_triggered();

signals:
    void sendMouseControlSizePoint(QPoint pos);
    void sendSplit_ShowSingal();
private:
    Ui::MainWindow *uiMainWindow;
    FASUtils* utils = nullptr;

    const QString title = "Solution Client - " + COMMONDEF->getAppVersion();

    static MainWindow* appWindow;
    bool initMax = true;
    QGroupBox* leftGroup = nullptr;
    QGroupBox* rightGroup = nullptr;
    QC_MDIWindow* mdiWindow = nullptr;
    QSplitter * mainSplitter = nullptr;
    LC_CentralWidget* centralViewObj = nullptr;
    QTabWidget* leftTabNavigate = nullptr;

    QMenu *popMenu_In_ListWidget_;/*弹出菜单*/
    QAction *action_DetectDrawing_In_ListWidget_;/*声明菜单:菜单上的Action*/
    QAction *action_ShowDetectResult_In_ListWidget_;/*声明菜单:菜单上的Action*/
    QAction *action_ShowHidenDevice_In_ListWidget_;/*声明菜单:菜单上的Action*/
    QAction *action_uploadResult_In_ListWidget_;/*声明菜单:菜单上的Action*/
    QAction *action_Rename_In_ListWidget_;/*声明菜单:菜单上的Action*/
    QAction *action_Delete_In_ListWidget_;/*声明动作:菜单上的Action*/

    QListWidget* listSideNavigate = nullptr;
    QTableWidget* detecedResultTable = nullptr;
    QList<CadDeviceDataDetect*> recognitionBlockList;
    QWidget* viewObjWithNavi = nullptr;
    double usedMemorySizeLimitFactor = 600;

    QTimer *autosaveTimer = nullptr;
    void createGraphicArea();
    void enableWindowActions(bool enable);
    bool openCADFile();
    void initMainwindow();
    void showLoadingGif();
    QWidget* createSideNavigate();
    void loadDetectedResult();
    void loadCustomizedDevices();
    void updateSideNavigate();
    void onRestart();
    double getUsedMemory();
    void openChangeHistoryLog(QString logName);
    void hideRecognizedDeviceFromView();


    QTranslator* translator = nullptr;
    bool showSVG = true;

    int tmpFileCode = 0;

    bool popMenuEnabled = true;
protected:
    void keyPressEvent(QKeyEvent* e) override;
    void keyReleaseEvent(QKeyEvent* e) override;
    void resizeEvent(QResizeEvent* e)override;
    void moveEvent(QMoveEvent *ev)override;

};

#endif // MAINWINDOW_H
