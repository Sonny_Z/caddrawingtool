#ifndef MYPROGRESSDIALOG_H
#define MYPROGRESSDIALOG_H

#include <QDialog>

namespace Ui {
    class MyProgressDialog;
}

class MyProgressDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MyProgressDialog(QWidget *parent = 0);
    ~MyProgressDialog();
    void setRange(int min,int max);
    void setValue(int v);
    void setStyle(QString color);


private:
    Ui::MyProgressDialog *ui;
};

#endif // MYPROGRESSDIALOG_H
