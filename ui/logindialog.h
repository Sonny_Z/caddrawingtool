#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include "ui_logindialog.h"
#include <QWidget>
#include <QDialog>
#include "QLabel"
#include <QPainter>
#include "fas_painter.h"
#include "fas_insert.h"
#include <QVector>
#include "entityqggv.h"
#include <QComboBox>
#include <QTextEdit>
#include <QtGlobal>
#include "fasutils.h"
#include <QHostInfo>

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    LoginDialog(QWidget *parent = nullptr);
    ~LoginDialog();
    void init();

private slots:
    void on_OKButton_clicked();

    void on_CancelButton_clicked();

signals:
//    void singalCloseMainWindowSlot();

private:
    Ui::LoginDialog *uiLoginDialog;
    QHostInfo info;
};

#endif // LOGINDIALOG_H
