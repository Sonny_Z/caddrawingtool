#ifndef AUTOCODEDIALOG_H
#define AUTOCODEDIALOG_H

#include <QDialog>
#include "fasutils.h"

namespace Ui {
class AutoCodeDialog;
}

class AutoCodeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AutoCodeDialog(QWidget *parent = 0);
    ~AutoCodeDialog();
    void init();

private slots:
    void on_DetectLoop_Button_clicked();
    void on_AutoCode_Button_clicked();

private:
    Ui::AutoCodeDialog *ui;
    FASUtils* utils = nullptr;
    QList<FAS_Entity*> wireEntities;
    QList<FAS_Entity*> deviceEntities;
    QList<QList<FAS_Entity*>> loopsToEncode;
};

#endif // AUTOCODEDIALOG_H
