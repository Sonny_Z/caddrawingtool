#ifndef DEVICERECORDFORM_H
#define DEVICERECORDFORM_H

#include <QComboBox>
#include <QDialog>
#include <QTableWidgetItem>
#include "fas_painter.h"
#include "fasutils.h"

namespace Ui {
class DeviceRecordForm;
}

class DeviceRecordForm : public QDialog
{
    Q_OBJECT

public:
    DeviceRecordForm(QWidget *parent, FASUtils* utils, bool isShowAll = false);
    ~DeviceRecordForm();
    void init();


signals:
    void signalSendTypeList(QList<DeviceInfo*> typeList, QList<int> countList);

private slots:
    void on_pBtnCancel_clicked();
    void on_pBtnOk_clicked();
    void on_pBtnShowBranch_clicked();

    void on_pBtnUpload_clicked();
    void slotTotalCount(int index);

    void on_checkHideRD_stateChanged(int arg1);

    void on_pBtnAdd_clicked();

private:
    Ui::DeviceRecordForm *uiDeviceRecordForm;
    bool isShowAll = false;

    QList<FAS_Insert*> UIMemoryInserts;
    QStringList typeStrList;
    QStringList typeLocalStrList;
    QStringList nameStrList;
    QStringList nameLocalStrList;
    QList<FAS_Block*> showBlockList;
    QMap<int,QString> showImageIdNameMap;
    QMap<QString,int> blockNameToImgIdMap;
    QList<QComboBox*> comBoxList;
    QMultiHash<QString, FAS_Insert*> nameToEntityHash;

    void showBranchResult(QHash<QString,QString> branches);
    void uploadNewImageToBaseAI();
    FASUtils* utils;
    QTableWidgetItem* totalCountItem;
    QList<QTableWidgetItem*> countList;
    QMap<QString,QTableWidgetItem*> uploadStatusList;

    QString popUpMessage = "";

    QList<CadDeviceDataDetect*> recognitionBlockList;
    void loadCustomizedDevices();
    QMap<QString,int> mergeCustomizedDevices(QMap<QString,int> recognitionDevices);
    void mergeRecognizedResultData();
};

#endif // DEVICERECORDFORM_H
