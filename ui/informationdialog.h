#ifndef INFORMATIONDIALOG_H
#define INFORMATIONDIALOG_H

#include <QDialog>
#include <QTextEdit>
#include "ui_informationdialog.h"
#include "fasutils.h"

namespace Ui {
class InformationDialog;
}

class InformationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InformationDialog(QWidget *parent, FAS_Entity* entity, FASUtils* utils);
    ~InformationDialog();

private slots:
    void on_pushButton_clicked();
    bool updateRecord();
    void on_cancelPushButton_clicked();
    void on_radioBtnHighlight_toggled(bool checked);

private:
    Ui::InformationDialog *ui;
    QTextEdit *mTextEdit;

    QString oriCodeText;
    QString oriNoteText;

    QString strDbFile;
    FAS_Entity* clickedEntity = nullptr;
    FASUtils* utils = nullptr;
};

#endif // INFORMATIONDIALOG_H
