#include "calculatewirelengthdialog.h"
#include "ui_calculatewirelengthdialog.h"

CalculateWireLengthDialog::CalculateWireLengthDialog(QWidget *parent, FASUtils* utils) :
    QDialog(parent),
    utils(utils),
    ui(new Ui::CalculateWireLengthDialog)
{
    ui->setupUi(this);
    FASUtils::setFontForUI(this);

    QStringList layerNameList;
    FAS_LayerList* layserList = utils->getRSDocument()->getLayerList();
    for(int ii = 0; ii < layserList->count(); ii++)
    {
        layerNameList.append(layserList->at(ii)->getName());
    }
    ui->layerComboBox->addItems(layerNameList);
}

CalculateWireLengthDialog::~CalculateWireLengthDialog()
{
    delete ui;
}

void CalculateWireLengthDialog::calculateLength()
{
    if(utils != nullptr)
    {
        QString layerName = ui->layerComboBox->currentText();
        FAS_Layer* layer = ((FAS_Graphic*)utils->getRSDocument())->findLayer(layerName);
        double length = utils->calculateWireLength(layer);
        ui->resultLable->setText(QString::number(length / 1000.0) + " m");
    }
}
