#include "settingdialog.h"
#include "ui_settingdialog.h"

//sort with alphabetical order
#include <QColorDialog>
#include <QTranslator>

SettingDialog::SettingDialog(QWidget *parent, FASUtils* utils) :
    QDialog(parent),
    utils(utils),
    ui(new Ui::SettingDialog)
{
    ui->setupUi(this);
    FASUtils::setFontForUI(this);

    QStringList layerNames;
    if(utils != nullptr)
    {
        FAS_LayerList* layerList = utils->getRSDocument()->getLayerList();
        for(int ii = 0; ii < layerList->count(); ii++)
        {
            layerNames.append(layerList->at(ii)->getName());
        }
    }
    ui->cb_layerList_Line->addItems(layerNames);
    layerNames.append(tr("Add New Layer"));
    ui->cb_layerList_Code->addItems(layerNames);
    ui->lineEdit_layerName->setDisabled(true);
    QList<QString> codeTypeList;
    codeTypeList.append("XXXXXXXX(All Number)");
    codeTypeList.append("XX-XXX(Loop NO. + Code Number)");
    ui->cb_codeType->addItems(codeTypeList);

    QString colorText = "R: " + QString::number(bgColor.red()) + " G: " + QString::number(bgColor.green()) + " B: " + QString::number(bgColor.blue());
    ui->colorLabel->setText(colorText);

    connect(ui->setColorButton, SIGNAL(clicked(bool)), this, SLOT(on_setColorButton_clicked()), Qt::UniqueConnection);
}

SettingDialog::~SettingDialog()
{
    delete ui;
}

void SettingDialog::enableLineEdit()
{
    if(ui->cb_layerList_Code->currentIndex() == ui->cb_layerList_Code->count() - 1)
    {
        ui->lineEdit_layerName->setEnabled(true);
    }
    else
    {
        ui->lineEdit_layerName->setEnabled(false);
    }
}

void SettingDialog::on_OK_clicked()
{
    bool createNewLayer = (ui->cb_layerList_Code->currentIndex() == ui->cb_layerList_Code->count() - 1);
    if(createNewLayer)
    {
        if(utils != nullptr)
        {
            FAS_Layer* codeLayer = utils->createNewLayer(ui->lineEdit_layerName->text());
            utils->setCodeLayer(codeLayer);
        }
    }
    else
    {
        if(utils != nullptr)
        {
            QString layerName = ui->cb_layerList_Code->currentText();
            FAS_Layer* codeLayer = ((FAS_Graphic*)utils->getRSDocument())->findLayer(layerName);
            utils->setCodeLayer(codeLayer);
        }
    }
    QString layerName = ui->cb_layerList_Line->currentText();
    FAS_Layer* lineLayer = ((FAS_Graphic*)utils->getRSDocument())->findLayer(layerName);
    utils->setWireLineLayer(lineLayer);
    utils->setBackgroudColor(bgColor);
    utils->setMaxNumberInLoop(ui->spinBox->value());
    close();
}

void SettingDialog::on_Cancel_clicked()
{
    close();
}

void SettingDialog::on_setColorButton_clicked()
{
    bgColor = QColorDialog::getColor(Qt::black, this, tr("Select background color"));
    QString colorText = "R: " + QString::number(bgColor.red()) + " G: " + QString::number(bgColor.green()) + " B: " + QString::number(bgColor.blue());
    ui->colorLabel->setText(colorText);
}

