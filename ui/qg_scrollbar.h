/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef QG_SCROLLBAR_H
#define QG_SCROLLBAR_H

#include <QScrollBar>
#include <QWheelEvent>

/**
 * A small wrapper for the Qt scrollbar. This class offers a slot
 * for scroll events.
 */
class QG_ScrollBar: public QScrollBar {
    Q_OBJECT

public:
    QG_ScrollBar(QWidget* parent=0)
            : QScrollBar(parent) {}
    QG_ScrollBar(Qt::Orientation orientation,
                 QWidget* parent=0)
            : QScrollBar(orientation, parent) {}

public slots:
    void slotWheelEvent(QWheelEvent* e) {
        wheelEvent(e);
    }

};

#endif

