#ifndef FINDDEVICE_H
#define FINDDEVICE_H

#include <QDialog>
#include "fasutils.h"

namespace Ui {
class FindDevice;
}

class FindDevice : public QDialog
{
    Q_OBJECT

public:
    FindDevice(QWidget *parent, FASUtils* utils);
    ~FindDevice();

private slots:
    void on_okPushButton_clicked();

    void on_cancelPushButton_clicked();

private:
    Ui::FindDevice *uiFindDevice;
    FASUtils* utils = nullptr;
};

#endif // FINDDEVICE_H
