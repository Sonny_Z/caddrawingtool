#include "newviewdialog.h"

//sort with alphabetical order
#include <QMessageBox>

#include "cadsqlite.h"
#include "commondef.h"
#include "fas_block.h"
#include "fasutils.h"
#include "waitlongtimedialog.h"

NewViewDialog::NewViewDialog(QWidget *parent, FASUtils* utils, bool isRename) :
    QDialog(parent),
    utils(utils),
    isRename(isRename),
//    allDevices(devices),
    uiNewViewDialog(new Ui::NewViewDialog)
{
    uiNewViewDialog->setupUi(this);
    if(isRename){
        this->setWindowTitle(tr("Rename"));
    }

    FASUtils::setFontForUI(this);

    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint &
                                   ~Qt::WindowMinimizeButtonHint &
                                   ~Qt::WindowCloseButtonHint    &
                                   ~Qt::WindowTitleHint          &
                                   ~Qt::WindowContextHelpButtonHint);
    init();
}

NewViewDialog::~NewViewDialog()
{
    delete uiNewViewDialog;
}

void NewViewDialog::init()
{
    curViewName = utils->getCurrentViewName();
    auto nextViewIndex = utils->getNextViewIndex();
    if(isRename){
        this->uiNewViewDialog->lineEdit_ViewName->setText(curViewName.section("_",1,1));
        this->uiNewViewDialog->comboBox_NewViewNameValue->setCurrentIndex(nextViewIndex - 1);
        this->uiNewViewDialog->label_OprDenied->setVisible(false);
    }else{

//    if(curViewName.contains("OriginalView")){
        this->uiNewViewDialog->lineEdit_ViewName->setText("newView");
        this->uiNewViewDialog->comboBox_NewViewNameValue->setCurrentIndex(nextViewIndex - 1);
        this->uiNewViewDialog->label_OprDenied->setVisible(false);
//    }
//    else
//    {
//        auto viewNameItems = curViewName.split("_");
//        this->uiNewViewDialog->label_NewViewIDValue->setNum(viewNameItems.first().toInt());
//        this->uiNewViewDialog->comboBox_NewViewNameValue->setCurrentText(viewNameItems.last());
//        this->uiNewViewDialog->comboBox_NewViewNameValue->setDisabled(true);
//        this->uiNewViewDialog->OKButton->setDisabled(true);
//        this->uiNewViewDialog->label_OprDenied->setVisible(true);
//    }
    }
}

void NewViewDialog::on_OKButton_clicked()
{
    QString newViewNameStr = this->uiNewViewDialog->lineEdit_ViewName->text()
            + "_" +this->uiNewViewDialog->comboBox_NewViewNameValue->currentText();
    if(isRename){
        newViewNameStr = curViewName.section("_",0,0) + "_" + newViewNameStr;
        auto ok = utils->updateCurrentViewName(curViewName,newViewNameStr);
        if(ok){
            accept();
        }else{
            QMessageBox::information(this, tr("Result"), tr("Duplicate! Check it and try again."));
            return;
        }
    }else{
        if(utils != nullptr){
            utils->setNewViewNameStr(newViewNameStr);
            WaitLongTimeDialog dlg(this);
            connect(&dlg,SIGNAL(sigPopUpMessage(const QString&)),this,SLOT(slotPopUpMessage(const QString&)));            dlg.setParas1(1,utils);
            if(dlg.exec()==QDialog::Accepted){
                accept();
            }
        }
    }
    close();
}

void NewViewDialog::on_CancelButton_clicked()
{
    FAS_Selection s(*utils->getRSDocument(), utils->getGraphicView());
    s.deselectAll();
    close();
}

void NewViewDialog::slotPopUpMessage(const QString& popUpMessage){
    QMessageBox::information(this, tr("Information"), tr(popUpMessage.toUtf8()));
}
