#include "blockform.h"
#include "ui_blockform.h"


//sort with alphabetical order
#include <QFile>
#include <QFileInfo>
#include <QTextStream>
#include <QDir>
#include <QMessageBox>
#include "commondef.h"
#include "entityqggv.h"
#include "fas_blocklist.h"
#include "fas_block.h"
#include "fas_insert.h"
#include "threadexcelgenerate.h"
#include <QDebug>
#include "popupmessagedialog.h"
#include "grabthread.h"
#include "fas_painterqt.h"
#include "addcustomizeddevicedialog.h"
#include "QToolTip"
#include "CadDeviceDataDetect.h"
#include "fas_attdef.h"

static void WriteToText(DeviceFeature feature, DeviceInfo* type)
{
    QFile textFile(COMMONDEF->textName);
    if(textFile.open(QIODevice::Text | QFile::Append))
    {
        QTextStream stream(&textFile);
        stream << feature.circles << ", ";
        stream << feature.lines << ", ";
        stream << feature.arcs << ", ";
        stream << feature.hatchs << ", ";
        stream << feature.solids << ", ";
        stream << QString::number(feature.size, 'g', 6) << ", ";
        stream << feature.text << ", ";
        stream << type->getName() << endl;
        textFile.close();
    }
}

qint64 hash(const QString & str)
{
  QByteArray hash = QCryptographicHash::hash(
    QByteArray::fromRawData((const char*)str.utf16(), str.length()*2),
    QCryptographicHash::Md5
  );
  Q_ASSERT(hash.size() == 16);
  QDataStream stream(&hash, QIODevice::ReadOnly);
  qint64 a, b;
  stream >> a >> b;
  return a ^ b;
}

QString generateDeviceFeatureCode(DeviceFeature feature){
    QString blockFeatureCode = QString::fromUtf8("%1%2%3%4%5%6@%7")
            .arg(QString::fromUtf8("%1").arg(feature.lines<10?QString::fromUtf8("0%1").arg(feature.lines):QString::fromUtf8("%1").arg(feature.lines)))
            .arg(QString::fromUtf8("%1").arg(feature.circles<10?QString::fromUtf8("0%1").arg(feature.circles):QString::fromUtf8("%1").arg(feature.circles)))
            .arg(QString::fromUtf8("%1").arg(feature.arcs<10?QString::fromUtf8("0%1").arg(feature.arcs):QString::fromUtf8("%1").arg(feature.arcs)))
            .arg(QString::fromUtf8("%1").arg(feature.hatchs<10?QString::fromUtf8("0%1").arg(feature.hatchs):QString::fromUtf8("%1").arg(feature.hatchs)))
            .arg(QString::fromUtf8("%1").arg(feature.solids<10?QString::fromUtf8("0%1").arg(feature.solids):QString::fromUtf8("%1").arg(feature.solids)))
            .arg(QString::fromUtf8("%1").arg(feature.text.length()<10?QString::fromUtf8("0%1").arg(feature.text.length()):QString::fromUtf8("%1").arg(feature.text.length())))
            .arg(QString::fromUtf8("%1").arg(hash(feature.text)));
    return blockFeatureCode;
}

//bool BlockForm::eventFilter(QObject *obj, QEvent *event)
//{
//    if(event->type() == QEvent::MouseButtonPress)
//    {
//        if(obj == ui->comboBox)
//        {
//            //处理鼠标点击事件
//            QComboBox * combobox = qobject_cast<QComboBox *>(obj);

//            combobox->clear();
//            combobox.addItem("list1");
//        }
//    }

//    return QWidget::eventFilter(obj, event);
//}
BlockForm::BlockForm(QWidget *parent, FASUtils* utils) :
    QDialog(parent),
    uiBlockForm(new Ui::BlockForm)
{
//    this->setAttribute(Qt::WA_DeleteOnClose);
    uiBlockForm->setupUi(this);
    FASUtils::setFontForUI(this);
    setLayout(this->uiBlockForm->verticalLayout_main);
    this->setContentsMargins(6,6,6,6);

    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint &
                                   ~Qt::WindowMinimizeButtonHint &
//                                   ~Qt::WindowCloseButtonHint    &
                                   ~Qt::WindowTitleHint          &
                                   ~Qt::WindowContextHelpButtonHint);
//    Qt::WindowFlags flags=Qt::Dialog;
//    flags |=Qt::WindowMinMaxButtonsHint;
//    flags |=Qt::WindowCloseButtonHint;
//    setWindowFlags(flags);
//    uiBlockForm->tableWidget_Added->setVisible(false);

    this->utils = utils;
//    qDebug() <<"ini form begin";
    init();
//    qDebug() <<"ini form end";
    COMMONDEF->setReadOnly(true);
    FAS_BlockList* blocklist = utils->getRSDocument()->getBlockList();
//    int cnt = blocklist->count();
    QHash<QString,FAS_Block*> clonedBlocksHash=blocklist->getClonedHashBlocks();
//    int cnt = blocklist->count();
    FAS_Block* myBlock;
    QList<FAS_Block*> deviceBlocks, UnknownDeviceBlocks, notDeviceBlocks;
    auto systemTypeStr = COMMONDEF->getSystem(true);

//    qDebug() <<"Cloned block size" <<clonedBlocksHash.size();
    QHashIterator<QString,FAS_Block*> iter(clonedBlocksHash);

    DeviceInfo* type = nullptr;
    while(iter.hasNext())
    {
        iter.next();
        myBlock = iter.value();
        QString blockName = myBlock->getName();
        QList<FAS_Insert*> devicesOfName = nameToEntityHash.values(blockName);
        int count = devicesOfName.size();
        if (count>0)
        {
//            //If this block is a part of another block then ignore it.
//            if(FAS2::EntityInsert  == devicesOfName.first()->getParent()->rtti())
//                continue;

//            int entitiesSize= utils->getEntityCountWithBigCondition(myBlock,500);
////            qDebug() <<"block name size" <<blockName <<entities.size();
//            if(entitiesSize>=500){
//                continue;
//            }

//            double blockSizeX=myBlock->getLengthX();
//            double blockSizeY=myBlock->getLengthY();

//            if(blockSizeX+blockSizeY>10000 || blockSizeX+blockSizeY<10){
//                continue;
//            }



//            auto insertOfmyBlock = blocksOfTheName.first();

//            if(!myBlock->is_set())
            {
//                if(blockName.contains("WWG")){
//                    qDebug() <<"block name count entitysize" <<blockName;
//                }
//                qDebug() <<"DeviceFeature begin";
                DeviceFeature feature = FASUtils::getDeviceFeature(myBlock);
//                 qDebug() <<"DeviceFeature end";
                auto typeTemplate = utils->detectDeviceType(feature);
                type = new DeviceInfo(typeTemplate->getID(),
                                      typeTemplate->getName(),
                                      typeTemplate->getTypeCode(), "");
                auto blockFeatureCode = generateDeviceFeatureCode(feature);
                type->setPerfectMatchFlag(blockFeatureCode);
                type->setSystemType(systemTypeStr);
                FASUtils::setBlcokType(myBlock,type);
//                blockFeatureCode.clear();
            }



//            containers.append(container);
            notDeviceBlocks.append(myBlock->clone()->getBlock());
        }
//        deleteLater();
    }



    //showBlockList
    auto recognitionBlockList = deviceBlocks + UnknownDeviceBlocks + notDeviceBlocks;
    QMap<int,QString> imageIdFeatureCodeMap;
    QTableWidget* tableWidget = uiBlockForm->tableWidget;
////    tableWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
//    tableWidget->verticalScrollBar()->setDisabled(true); // 禁用滚动

    tableWidget->setRowCount(recognitionBlockList.count() + 1);
    tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);//只能单选

    int drawNum=notDeviceBlocks.size();
//     qDebug() <<"draw block size" <<drawNum;


     QFileInfo fileInfo(utils->getORSDocument()->getFilename());
     auto fileName = fileInfo.baseName();

     QString absoluteFilePath = COMMONDEF->getDxfOfDevicePathStr();
     auto desDir = QString::fromUtf8("%1/%2/%3").arg(absoluteFilePath).arg(fileName).arg(utils->getCurrentViewName());
     QDir dir(desDir);
     if(dir.exists()){
         dir.removeRecursively();
     }

     QFileInfo svgFileInfo("./svgStore");
     QString svgFilePath = svgFileInfo.absoluteFilePath();
     auto svgDir = QString::fromUtf8("%1/%2/%3").arg(svgFilePath).arg(fileName).arg(utils->getCurrentViewName());
     QDir dirSVG(svgDir);
     if(dirSVG.exists()){
         dirSVG.removeRecursively();
     }


    for(int ii=0;ii<drawNum;ii++)
    {

        FAS_Insert* newInsert=nullptr;
        FAS_EntityContainer* container=nullptr;
        FAS_Block* oneBlock=nullptr;
//        FAS_Block* newBlock=nullptr;
        QList<FAS_EntityContainer*> containers;
        oneBlock = notDeviceBlocks.at(ii);
        QString blockName = oneBlock->getName();
        QList<FAS_Insert*> devicesOfName = nameToEntityHash.values(blockName);
        FAS_Insert* oneDevice = devicesOfName.first();
//         qDebug() <<"block name count entitysize" <<blockName <<devicesOfName.count() << devicesOfName.first()->getEntityList().size() << oneDevice->getInsertList().size();

        if(nullptr == oneDevice){
             continue;
        }

        //A insert with a attribute entity text value different from default value shoule be checked;
        auto blockEntities = oneDevice->getBlockForInsert()->getEntityList();
        double height;
        QString defaultText, text;
        bool isDrawImageInAtt = false;
        foreach(auto e, blockEntities){
            if(e->rtti() == FAS2::EntityAttdef){
                auto defEntity = dynamic_cast<FAS_ATTDEF*>(e);
                height = defEntity->data.textdata.height;
                if(fabs(height) < 0.1){
                    continue;
                }
                auto label = defEntity->data.label;
                text = oneDevice->data.mmap.value(label).text;
                defaultText = defEntity->getText();
                if(!text.contains(defaultText, Qt::CaseInsensitive)){
                    isDrawImageInAtt = true;
                    break;
                }
//                auto isVisiable = defEntity->isVisible();
            }
        }


        if(oneDevice->data.mmap.size()>0 && isDrawImageInAtt)
        {
                           newInsert=oneDevice;
                           container = new FAS_EntityContainer();
                           QString blockFeatureCode=oneBlock->getType()->getPerfectMatchFlag();

                           long ID=newInsert->getId();
                           imageIdFeatureCodeMap.insert(ID,blockFeatureCode);
                           auto imageName = QString::fromUtf8("%1_%2_%3_%4")
                                   .arg(ID)
                                   .arg(COMMONDEF->getSystem(true))
                                   .arg(hash(oneBlock->getName()))
                                   .arg(blockFeatureCode);
                           container->addEntity(newInsert);
                           container->adjustBorders(newInsert);
                           COMMONDEF->convertDeviceToImage=true;
                           utils->doConvertDeviceToImage(desDir,imageName,"jpg",container,QSize(92,112));
                           COMMONDEF->convertDeviceToImage=false;
                           utils->svgGenerator(svgDir,imageName,container,QSize(92,112));
                           //updater SVG file path
//                           oneDevice->getDeviceInfo()->getSvgRender()->load()
                           blockNameToImgIdMap.insert(oneBlock->getName(),ID);
                           showImageIdNameMap.insert(ID,QString::fromUtf8("%1.jpg").arg(imageName));
                           container->setOwner(false);
                           container->clear();
                           delete container;
        }
        else
        {
        newInsert = oneDevice->clone()->getInsert();

       container = new FAS_EntityContainer();

//       newInsert->setScale(FAS_Vector(1,1));
        newInsert->setSelected(false);
        newInsert->setVisible(true);
        newInsert->setOwner(false);
        boolean isDiffcultBlock=false;
                QList<FAS_Entity*> entities=newInsert->getEntityList();
                FAS_Block* blockInInsert;
                for(FAS_Entity* entity:entities){
                    if(FAS2::EntityInsert==entity->rtti()){
                                isDiffcultBlock=true;
                                break;
                    }
                }
                if(!isDiffcultBlock){
                    newInsert->clear();
                    newInsert->addEntity(oneBlock);
                    newInsert->calculateBlockBorders();
                }else{
                    newInsert->setAngle(0.0);
                    newInsert->update();
                }

        QString blockFeatureCode=oneBlock->getType()->getPerfectMatchFlag();
        long ID=newInsert->getId();
        imageIdFeatureCodeMap.insert(ID,blockFeatureCode);
        auto imageName = QString::fromUtf8("%1_%2_%3_%4")
                .arg(ID)
                .arg(COMMONDEF->getSystem(true))
                .arg(hash(oneBlock->getName()))
                .arg(blockFeatureCode);
        container->addEntity(newInsert);
        container->adjustBorders(newInsert);
        COMMONDEF->convertDeviceToImage=true;
        utils->doConvertDeviceToImage(desDir,imageName,"jpg",container,QSize(92,112));
        COMMONDEF->convertDeviceToImage=false;
        utils->svgGenerator(svgDir,imageName,container,QSize(92,112));
        //updater SVG file path
//        oneDevice->getDeviceInfo()->getSvgRender()->load()

//        qDebug() <<"block ID name count entitysize"<<ID <<blockName <<devicesOfName.count() << devicesOfName.first()->getEntityList().size() << oneDevice->getInsertList().size();

        blockNameToImgIdMap.insert(oneBlock->getName(),ID);
        showImageIdNameMap.insert(ID,QString::fromUtf8("%1.jpg").arg(imageName));
        }
    }

    auto dxfFileName = fileInfo.fileName();
    GrabThread *sd = new GrabThread();
    sd->uploadFile(desDir);
    QTime startTime2 = QTime::currentTime();
//    qDebug() << "Uploading zip file successfully!" <<startTime2;
    QTime startTime3 = QTime::currentTime();
//    qDebug() << "Start AI ..." <<startTime3;
    sd->setImageIdFeatureCodeRequest(imageIdFeatureCodeMap);
//    sd->startClearBaseImageCachePostQuery();
    sd->startRecognitionPostQuery(showImageIdNameMap,systemTypeStr);
    QTime startTime4 = QTime::currentTime();
//    qDebug() << "AI responsed."<<startTime4;;
    auto curRecognitionResponseData = sd->getRecognitionResponseData();
    auto imageIdCodeOfAIResponseMap = curRecognitionResponseData.value("types");
    auto imageIdCode2MatchRatioMap = curRecognitionResponseData.value("matchRatio");
    auto imageIdCodeToBeUpdate4AIMap = curRecognitionResponseData.value("itemsToBeUpdate");
    curImgIdToRemoteImgIdMap = curRecognitionResponseData.value("curImgIdToRemoteImgId");
    auto popUpError = sd->getHttpErrorMessage();
    if(!popUpError.isNull() && !popUpError.isEmpty()){
        QMessageBox::information(this, tr("Result"), tr(popUpError.toUtf8()));
    }
    sd->deleteLater();

    //调整已识别和未识别的设备显示顺序
    typedef  QPair<int,double> matchRatio;
//    QVector<matchRatio> matchRatioVec;
//    for(auto iter = imageIdCode2MatchRatioMap.begin(); iter != imageIdCode2MatchRatioMap.end(); iter++){
//        matchRatio id2Ratio(iter.key(),iter.value().toDouble());
//        matchRatioVec.append(id2Ratio);
//    }
//    qSort(matchRatioVec.begin(),matchRatioVec.end(),[](matchRatio image1, matchRatio image2){
//        return image1.second > image2.second;
//    });

    notDeviceBlocks.clear();
    QList<QPair<int, double>> sortedImageIdRatioPairList;
    QString sortedDevicesInfo;
    QString sortedUnknownDevicesInfo;
    QString sortedNotDevicesInfo;
    QDateTime current_date_time =QDateTime::currentDateTime();
    QString current_date =current_date_time.toString("yyyy.MM.dd hh:mm:ss.zzz ddd");
//    qDebug() << "Start sorting blocks:" << current_date;
    foreach(auto block, recognitionBlockList){
        QString blockName = block->getName();
        int imgID = blockNameToImgIdMap.value(blockName);
        auto deviceCode = imageIdCodeOfAIResponseMap.value(imgID);
        int index = typeStrList.indexOf(deviceCode);
        auto deviceType = nameLocalStrList.value(index);
        if(index>=0){
            deviceBlocks.append(block);
            sortedDevicesInfo = QString::fromUtf8("%1Device: blockName->%2 imgID->%3  DeviceCode->%4  DeviceType->%5 \r\n")
                    .arg(sortedDevicesInfo)
                    .arg(blockName)
                    .arg(imgID)
                    .arg(deviceCode)
                    .arg(deviceType);
        }else if(imageIdCode2MatchRatioMap.find(imgID) != imageIdCode2MatchRatioMap.end()){
            auto curObj = imageIdCode2MatchRatioMap.find(imgID);
            int size = sortedImageIdRatioPairList.size();
            int insertIndex = size;
            for(int index = size - 1 ; index >= 0; index--){
                if(sortedImageIdRatioPairList.at(index).second <= curObj.value().toDouble()){
                    insertIndex = index;
                }else break;
            }
            matchRatio id2Ratio(curObj.key(),curObj.value().toDouble());
            sortedImageIdRatioPairList.insert(insertIndex,id2Ratio);
            UnknownDeviceBlocks.insert(insertIndex, block);
        }else{
            notDeviceBlocks.append(block);
            sortedNotDevicesInfo = QString::fromUtf8("%1notDevice: blockName->%2 imgID->%3 \r\n")
                    .arg(sortedNotDevicesInfo)
                    .arg(blockName)
                    .arg(imgID);
        }
    }

    QDateTime current_date_time_e =QDateTime::currentDateTime();
    QString current_date_e =current_date_time_e.toString("yyyy.MM.dd hh:mm:ss.zzz ddd");
//    qDebug() << "End sorting blocks:" << current_date_e;

    auto recognizedCount = deviceBlocks.size();
    foreach(auto ImageIdRatioPair, sortedImageIdRatioPairList){
        auto index = sortedImageIdRatioPairList.indexOf(ImageIdRatioPair);
        QString blockName = UnknownDeviceBlocks.value(index)->getName();
        int imgID = blockNameToImgIdMap.value(blockName);
        sortedUnknownDevicesInfo = QString::fromUtf8("%1unknownDevice->%2: blockName->%3 imgID->%4  MatchRatio->%5 \r\n")
                .arg(sortedUnknownDevicesInfo)
                .arg(index+recognizedCount+1)
                .arg(blockName)
                .arg(imgID).arg(ImageIdRatioPair.second);
    }
//    sortedImageIdRatioPairList.clear();

//    QString sortedInfo = "sortedInfo";
//    COMMONDEF->saveToFile(sortedDevicesInfo, sortedInfo);
//    COMMONDEF->saveToFile(sortedUnknownDevicesInfo, sortedInfo, true);
//    COMMONDEF->saveToFile(sortedNotDevicesInfo, sortedInfo, true);

    showBlockList = deviceBlocks + UnknownDeviceBlocks + notDeviceBlocks;
    int totalCount = 0;
    int ii = 0;
    int loopCount=showBlockList.count();
    bool isRecognized;
    QLabel *l1=nullptr;
    QWidget *widgetChild=nullptr;
    QVBoxLayout *vLayout=nullptr;
    QComboBox* tcomBox=nullptr;
    QTableWidgetItem* countItem=nullptr;
    QTableWidgetItem* uploadStatusItem=nullptr;
    QString uploadStatus = QString(tr("-"));
    for(int ii=0;ii<loopCount;ii++)
    {
        FAS_Block* oneBlock = showBlockList.value(ii);

        tableWidget->setRowHeight(ii,100);
        tableWidget->verticalHeader()->setDefaultSectionSize(100);

//        FAS_Block* oneBlock = showBlockList.at(ii);
        QString blockName = oneBlock->getName();
        QList<FAS_Insert*> devicesOfName = nameToEntityHash.values(blockName);
        int count = devicesOfName.size();
        if(deviceBlocks.contains(oneBlock)){
            totalCount += count;
            isRecognized = true;
        }else{
            isRecognized = false;
        }

        int imgID=blockNameToImgIdMap.value(blockName);
        auto imgName = showImageIdNameMap.value(imgID);
        auto blkSavePath=QString::fromUtf8("%1/%2").arg(desDir).arg(imgName);
        indexImageTempMap.insert(ii,blkSavePath);
        l1 = new QLabel();     //创建lable

        l1->setAlignment(Qt::AlignHCenter);
        l1->setScaledContents(true);
        l1->setMaximumWidth(92);
        l1->setMaximumHeight(112);//设置居中
        l1->setPixmap(QPixmap(blkSavePath).scaled(l1->size(),Qt::KeepAspectRatio));    //加载图片

        widgetChild = new QWidget;
        vLayout = new QVBoxLayout();
        vLayout->addWidget(l1);
        vLayout->setAlignment(l1,Qt::AlignCenter);
        widgetChild->setLayout(vLayout);
        widgetChild->setStyleSheet("background-color:black;");
        QString toolTipContent = tr("Block Description") + "\r\n"  + tr("Block Name") + " : " + blockName;
//        if(UnknownDeviceBlocks.indexOf(oneBlock)>0){
//            toolTipContent = QString::fromUtf8("%1\r\nMatchRatio:%2")
//                    .arg(toolTipContent)
//                    .arg(sortedImageIdRatioPairList.value(UnknownDeviceBlocks.indexOf(oneBlock)).second);
//        }
        widgetChild->setToolTip(toolTipContent);

        tableWidget->setCellWidget(ii, 0, widgetChild);
        int img=blockNameToImgIdMap.value(blockName);
        auto deviceCode = imageIdCodeOfAIResponseMap.value(img);
        auto deviceFeatureFlagStr = imageIdFeatureCodeMap.value(imgID);
        auto perfectMatchFlag = oneBlock->getType()->getPerfectMatchFlag();
        auto imageName = showImageIdNameMap.value(imgID);

        //**********************Device Type***************************************/
        tcomBox = new MyComboBox(this);
        tcomBox->addItems(nameLocalStrList);
        int indexCheck = typeStrList.indexOf(deviceCode);
        int index = indexCheck >= 2 ? indexCheck : (notDeviceBlocks.contains(oneBlock) ? 1 : 0);
        tcomBox->blockSignals(true);
        tcomBox->setCurrentIndex(index);

        tcomBox->blockSignals(false);
        tableWidget->setCellWidget(ii, 1, tcomBox);
        comBoxList.append(tcomBox);

        if(imageIdCodeToBeUpdate4AIMap.find(imgID)!=imageIdCodeToBeUpdate4AIMap.end()
                && index >= 2
                && nameStrList.at(index).compare("UnknownDevice",Qt::CaseInsensitive)
                && deviceCode.compare("UnknownDevice",Qt::CaseInsensitive)){
            oneBlock->getType()->setName(nameStrList.at(index));
            oneBlock->getType()->setTypeCode(deviceCode);
            utils->addDeviceImagesToBeUpdate4AIList(imageName,oneBlock);
        }

        auto systemTypeStr = COMMONDEF->getSystem(true);
        connect(tcomBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),this, [=]( int ix ) {
            QString deviceName = nameStrList.at(comBoxList.at(ii)->currentIndex());
            auto tmpType = COMMONDEF->getDeviceInfoFromName(deviceName);
            if(tmpType->isValidDevice())
            {
                auto type = oneBlock->getType();
                auto preName = type->getName();
                oneBlock->getType()->setSystemType(systemTypeStr);
                if(nameLocalStrList.value(index).compare(deviceName,Qt::CaseInsensitive)){
                    type->setName(deviceName);
                    type->setTypeCode(typeStrList.at(nameStrList.indexOf(deviceName)));
                    type->setPerfectMatchFlag(perfectMatchFlag);
                    utils->setBlcokType(oneBlock,type);
//                    utils->addDeviceImagesToBeUpdate4AIList(imageName,oneBlock);
                    if(nameStrList.contains(preName) && (preName.compare("UnknownDevice",Qt::CaseInsensitive)
                            && preName.compare("NotDevice",Qt::CaseInsensitive))
                        && preName.compare(deviceName,Qt::CaseInsensitive)){
                        imgNames2ImgIdToBeUpdate.insert(imageName, imgID);
                    }
                }else{
                    utils->setBlcokType(oneBlock,tmpType);
                    imgNames2ImgIdToBeUpdate.insert(imageName, imgID);
                }
            }else{
                auto preType = oneBlock->getType();
                auto preName = preType->getName();
                tmpType->setSystemType();
                FASUtils::setBlcokType(showBlockList.at(ii),tmpType);
//                utils->addDeviceImagesToBeUpdate4AIList(imageName,oneBlock);
                if(deviceName.contains("UnknownDevice",Qt::CaseInsensitive)
                        || deviceName.contains("NotDevice",Qt::CaseInsensitive)){
                    imgNames2ImgIdToBeUpdate.insert(imageName, imgID);
                }

//                utils->eraseCacheDeviceImageAfterUpdate(imageName);
            }
            utils->addDeviceImagesToBeUpdate4AIList(imageName,oneBlock);

            slotTotalCount(ix);
        });

        //**********************Device Count***************************************/
        countItem = new QTableWidgetItem();
        countList.append(countItem);
        countItem->setText(QString::number(count));
        countItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(ii, 2, countItem);

        //**********************Upload Status***************************************/
        uploadStatusItem = new QTableWidgetItem();
        uploadStatusItem->setText(uploadStatus);
        uploadStatusItem->setTextColor(FAS_Color(0,255,0).toQColor());
//        uploadStatusItem->setBackground(FAS_Color(0,255,0).toQColor());
        uploadStatusItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
        uploadStatusList.insert(imageName,uploadStatusItem);
        uploadStatusItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(ii, 3, uploadStatusItem);

//        deleteLater();
    }

//    qDeleteAll(inserts);
//    inserts.clear();
    // show total count
    QTableWidgetItem* totalItem = new QTableWidgetItem();
    totalItem->setText(tr("Total count"));
    totalItem->setFlags(Qt::ItemIsEditable);
    tableWidget->setItem(showBlockList.count(), 1, totalItem);
    //QTableWidgetItem* totalCountItem = new QTableWidgetItem();
    totalCountItem = new QTableWidgetItem();
    totalCountItem->setText(QString::number(totalCount));
    totalCountItem->setFlags(Qt::ItemIsEditable);
    tableWidget->setItem(showBlockList.count(), 2, totalCountItem);

    //resize width
    tableWidget->resizeColumnsToContents();
    tableWidget->deleteLater();
    systemTypeStr.clear();

//    deleteLater();
}
BlockForm::~BlockForm()
{
//    COMMONDEF->setReadOnly(true);
    typeStrList.clear();
    typeLocalStrList.clear();
    nameStrList.clear();
    nameLocalStrList.clear();
    showBlockList.clear();
    showImageIdNameMap.clear();
    blockNameToImgIdMap.clear();
    qDeleteAll(comBoxList);
    comBoxList.clear();
    nameToEntityHash.clear();
    imgNames2ImgIdToBeUpdate.clear();
    curImgIdToRemoteImgIdMap.clear();
    if(!UIMemoryInserts.isEmpty()){
//        qDeleteAll(UIMemoryInserts);
        UIMemoryInserts.clear();
    }
    utils->clearCacheDeviceImagesAfterUpdate();
    delete uiBlockForm;
    deleteLater();

}

void BlockForm::init()
{
//    COMMONDEF->setReadOnly(false);
    indexImageTempMap.clear();
    showBlockList.clear();
    showImageIdNameMap.clear();
    blockNameToImgIdMap.clear();
    typeStrList = COMMONDEF->getDeviceTypeList();
    nameStrList = COMMONDEF->getDeviceNameList();
    nameLocalStrList = COMMONDEF->getDeviceNameList();
//    typeStrList = COMMONDEF->getDeviceNameCodeDict().values();
    typeLocalStrList = COMMONDEF->getLocalDeviceTypeList();
//    qDebug() << "name to entity hash begin";
    nameToEntityHash = utils->getBlockNameToEntityHash();
//     qDebug() << "name to entity hash end";
//    uiBlockForm->textEdit_showResult->setDisabled(true);
    {
        QString absoluteFilePath = COMMONDEF->getDxfOfDevicePathStr();
        QFileInfo fileInfo(utils->getORSDocument()->getFilename());
        auto fileName = fileInfo.baseName();
        auto desDir = QString::fromUtf8("%1/%2/%3").arg(absoluteFilePath).arg(fileName).arg(utils->getCurrentViewName());
        utils->clearDir(desDir);
    }
    loadCustomizedDevices();
//    this->uiBlockForm->checkHideRD->setChecked(COMMONDEF->isHideRecognizedDevice());
}

void BlockForm::on_pBtnCancel_clicked()
{
    close();
}


void BlockForm::showBranchResult(QHash<QString,QString> branches){
    if(branches.keys().size()>0){
//        int textEditWidth = uiBlockForm->textEdit_showResult->width();
//        uiBlockForm->textEdit_showResult->setDisabled(false);
//        int width = uiBlockForm->layoutWidget->width();
//        uiBlockForm->layoutWidget->resize(width - textEditWidth,uiBlockForm->layoutWidget->height());
        QString showContent = "";
        for(int i = 1; i<=branches.keys().size();i++){
            auto key  = branches.keys().at(i-1);
            auto values = branches.values(branches.keys().at(i-1));
            QString value = "";
            if(values.size()>1){
                foreach(auto v, values){
                    value = QString::fromUtf8("%1&%2")
                            .arg(value).arg(v);
                }
            }else{
                value = branches.value(branches.keys().at(i-1));
            }
            showContent = QString::fromUtf8("%1%2: %3-->%4;\r\n")
                    .arg(showContent)
                    .arg(i)
                    .arg(key)
                    .arg(value);
        }
//        uiBlockForm->textEdit_showResult->setText(showContent);
    }
//    else{
//        int textEditWidth = uiBlockForm->textEdit_showResult->width();
//        uiBlockForm->textEdit_showResult->hide();
//        int width = uiBlockForm->layoutWidget->width();
//        uiBlockForm->layoutWidget->resize(width - textEditWidth,uiBlockForm->layoutWidget->height());
//    }
}

void BlockForm::uploadNewImageToBaseAI(){
    auto updateImages = utils->getDeviceImagesToBeUpdate4AIList();
    GrabThread *httpRequest = new GrabThread();
    QString systemCode = COMMONDEF->getSystem(true);
    QString name, typeCode, deviceFeatureFlag, file;
    QFileInfo fileInfo(utils->getORSDocument()->getFilename());
    auto dxfFileName = fileInfo.baseName();
    auto absoluteFilePath = QString::fromUtf8("%1/%2/%3/")
            .arg(COMMONDEF->getDxfOfDevicePathStr())
            .arg(dxfFileName)
            .arg(utils->getCurrentViewName());
    bool isRemoteImgUpdate = false;
    foreach(auto image, updateImages.keys()){
        auto device = updateImages.value(image);
        auto type = device->getType();
        name = type->getName();
        typeCode = type->getTypeCode();
        deviceFeatureFlag = type->getPerfectMatchFlag();
        auto file = QString::fromUtf8("%1/%2").arg(absoluteFilePath).arg(image);

        //update remote image prop.
        if(imgNames2ImgIdToBeUpdate.contains(image)){
            auto imgId = imgNames2ImgIdToBeUpdate.value(image);
            auto newCode = (typeCode.contains("unKnownDevice",Qt::CaseInsensitive)
                    || typeCode.contains("notDevice",Qt::CaseInsensitive)) ? "" : typeCode;
            httpRequest->addimageIdCodeToBeUpdateMap(imgId, newCode);
            isRemoteImgUpdate = true;
            httpRequest->updateRemoteImages(curImgIdToRemoteImgIdMap);//update remote image prop.(api:v1/busiimage/modifyImageInfo)
        }else{
            httpRequest->uploadSignalBaseImage(systemCode,typeCode,name,deviceFeatureFlag,file);// upload new base image info.(api:v1/busiimage/file)
        }


        auto uploadStatusItem = uploadStatusList.value(image);
        if(httpRequest->getHttpResponseStatus()){
            utils->eraseCacheDeviceImageAfterUpdate(image);
            uploadStatusItem->setText(tr("ok"));
            uploadStatusItem->setForeground(QBrush(FAS_Color(0,255,0).toQColor()));
        }else{
            uploadStatusItem->setText(tr("Failed"));
            uploadStatusItem->setForeground(QBrush(FAS_Color(255,0,0).toQColor()));
        }
    }

    if(isRemoteImgUpdate){

    }
    auto popUpError = httpRequest->getHttpErrorMessage();
    if(!popUpError.isNull() && !popUpError.isEmpty()){
        QMessageBox::information(this, tr("Result"), tr(popUpError.toUtf8()));
    }
    httpRequest->deleteLater();
//    utils->clearCacheDeviceImagesAfterUpdate();
}

void BlockForm::on_pBtnOk_clicked()
{
    this->uiBlockForm->pBtnOk->setDisabled(true);
    uploadNewImageToBaseAI();
//    ThreadExcelGenerate* threadExcel = new ThreadExcelGenerate();
//    connect(this, SIGNAL(signalSendTypeList(QList<DeviceInfo*>, QList<int>)), threadExcel, SLOT(updateDetectType(QList<DeviceInfo*>, QList<int>)));

    auto curFloor = utils->getCurrentViewName();
    utils->addBlocksOfView(curFloor,showBlockList);
    QMap<QString,int> curRecognitionResult;
    QList<CadDeviceDataDetect*> viewDeviceList;
    QList<DeviceInfo*> detectType;
    QList<int> typeCount;
    QTableWidget* tableWidget = uiBlockForm->tableWidget;
    int resultCount=showBlockList.size();
    for(int i = 0; i < resultCount; i++)
    {
       FAS_Block* myBlock = showBlockList.at(i);
       QString deviceType = typeLocalStrList.at(comBoxList.at(i)->currentIndex());
       DeviceInfo* tmpType = myBlock->getType();

       int count = tableWidget->item(i, 2)->text().toInt();
       typeCount.append(count);
       if(!deviceType.isEmpty() && deviceType.compare("UnknownDevice", Qt::CaseInsensitive)
               && deviceType.compare("NotDevice", Qt::CaseInsensitive)
               && deviceType.compare("待标定设备", Qt::CaseInsensitive)
               && deviceType.compare("疑似设备", Qt::CaseInsensitive)){
           if(curRecognitionResult.find(deviceType) == curRecognitionResult.end()){
              curRecognitionResult.insert(deviceType,count);
           }else{
               curRecognitionResult.find(deviceType).value() = curRecognitionResult.value(deviceType) + count;
           }
           if(COMMONDEF->isHideRecognizedDevice()){
               tmpType->setValidRecognition(true);
           }else{
               tmpType->setValidRecognition(false);
           }
           auto recognitionDevice = new CadDeviceDataDetect(myBlock,indexImageTempMap.value(i), deviceType, count);
           viewDeviceList.append(recognitionDevice);

           QList<FAS_Insert*> devicesOfName = nameToEntityHash.values(myBlock->getName());
           FAS_Insert* oneDevice = devicesOfName.first();
           oneDevice->setSelected(true);
       }

       tmpType->setShowSVG(false);
       tmpType->setTypeCode(deviceType);
       FASUtils::setBlcokType(myBlock,tmpType);
       detectType.append(tmpType);
    }

    //Merge recognized devices and customized devices
    curRecognitionResult = mergeCustomizedDevices(curRecognitionResult);

    FASUtils::setAllDeviceEntityType(showBlockList, nameToEntityHash);

//    emit signalSendTypeList(detectType, typeCount); //0707 ltree
    //utils->addRecognizedDevicesData("","","",typeCount);

//    for (int i = 0; i < showBlockList.size(); i++)
//    {
//        //Update detect rule.
//        DeviceFeature feature = FASUtils::getDeviceFeature(showBlockList.at(i));
//        utils->addDetectRule(feature, detectType.at(i)->getName(),detectType.at(i)->getSystemType());
//        WriteToText(feature, detectType.at(i));

//        /*
//        if(detectType.at(i)->isValidDevice())
//        {
//            //insert into database.
//            FASUtils::storeDeviceListToDB(nameToEntityHash.values(showBlockList.at(i)->getName()));
//        }*/
//    }
    QString messageContent;

    auto savedFloors = utils->getRecognitionResult().keys();
    auto numOfDevicesToBeSaved = curRecognitionResult.size();
    if(!curFloor.contains("图例层")){
        if(numOfDevicesToBeSaved>0){
            utils->setRecognitionResult(curFloor,curRecognitionResult);
            utils->addRecognizedDevices(curFloor,viewDeviceList);
            messageContent = QString::fromUtf8("[%1] %2 %3 %4 %5.\n%6:\n")
                    .arg(curFloor)
                    .arg(tr("saved"))
                    .arg(curRecognitionResult.size())
                    .arg(numOfDevicesToBeSaved>1 ? tr("devices") : tr("device"))
                    .arg(tr("successfully"))
                    .arg(tr("saved floors"));
        }else{
            messageContent = QString::fromUtf8("[%1] %2 0 %3.\n%4:\n")
                    .arg(curFloor)
                    .arg(tr("saved"))
                    .arg(tr("device"))
                    .arg(tr("Saved floors"));
        }
    //    threadExcel->start();
        savedFloors.clear();
        savedFloors = utils->getRecognitionResult().keys();
    }else{
        messageContent = QString::fromUtf8("%1 %2 of [%1] are uploaded.\nSaved floors:\n")
                .arg(curRecognitionResult.size())
                .arg(numOfDevicesToBeSaved > 1 ? tr("devices") : tr("device"))
                .arg(curFloor);
    }

    foreach(auto floor, savedFloors){
        messageContent = QString::fromUtf8("%1[%2]\n").arg(messageContent).arg(floor);
    }
    utils->refreshGraphicView();
    QMessageBox *messageBox = new QMessageBox(tr("Result"),
                                             messageContent,
                                             QMessageBox::Information,
                                             QMessageBox::Ok | QMessageBox::Default,
                                             QMessageBox::Cancel | QMessageBox::Escape,
                                             0);
//    messageBox->setTextInteractionFlags(Qt::TextSelectableByMouse);
//    messageBox->exec();
    utils->getRSDocument()->setDetectedStatus(true);
//    QMessageBox::information(this, tr("Result"), tr("Completed."));
    this->uiBlockForm->pBtnOk->setEnabled(true);
//    utils->refreshGraphicView();
//    this->close();
}

void BlockForm::on_pBtnShowBranch_clicked()
{
//    utils->buidMatrixToEntitiesMap();
//    qDebug()<<"1.utils->buidMatrixToEntitiesMap() is Done.";
//    utils->buidMatrixToBlocksMapping();
//    qDebug()<<"2.utils->buidMatrixToBlocksMapping() is Done.";
//    utils->setIESBlockParent();
//    qDebug()<<"3.utils->setIESBlockParent() is Done.";
    showBranchResult(utils->detectDeviceBranch());
//    qDebug()<<"4.showBranchResult(utils->detectDeviceBranch()) is Done.";
//    utils->refreshGraphicView();
//    qDebug()<<"5.utils->refreshGraphicView() is Done.";
}

void BlockForm::on_pBtnUpload_clicked()
{
    this->uiBlockForm->pBtnOk->setDisabled(true);
    QMap<QString,int> curRecognitionResult;
    QMap<QString,QMap<QString,int>> finalSingleResultMap;
    QList<DeviceInfo*> detectType;
    QList<int> typeCount;
    QTableWidget* tableWidget = uiBlockForm->tableWidget;
    auto curViewName = utils->getCurrentViewName();
    for(int i = 0; i < showBlockList.size(); i++)
    {
       FAS_Block* myBlock = showBlockList.at(i);
//       QString deviceName = nameLocalStrList.at(comBoxList.at(i)->currentIndex());
       QString deviceType = typeLocalStrList.at(comBoxList.at(i)->currentIndex());
//       DeviceInfo* tmpType = COMMONDEF->getDeviceInfoFromName(deviceName);
       DeviceInfo* tmpType = myBlock->getType();
       tmpType->setShowSVG(false);
       FASUtils::setBlcokType(myBlock,tmpType);
       detectType.append(tmpType);
       int count = tableWidget->item(i, 2)->text().toInt();
       typeCount.append(count);

       if(!deviceType.isEmpty() && deviceType.compare("UnknownDevice", Qt::CaseInsensitive)
               && deviceType.compare("NotDevice", Qt::CaseInsensitive)
               && deviceType.compare("待标定设备", Qt::CaseInsensitive)
               && deviceType.compare("疑似设备", Qt::CaseInsensitive)){
           if(curRecognitionResult.find(deviceType) == curRecognitionResult.end()){
              curRecognitionResult.insert(deviceType,count);
           }else{
              curRecognitionResult.find(deviceType).value() = curRecognitionResult.value(deviceType) + count;
           }
       }
    }

    curRecognitionResult = mergeCustomizedDevices(curRecognitionResult);

    finalSingleResultMap.clear();
    QString responseCode;
    QString popUpError;
    QString messageContent;
    if(!curViewName.contains("图例层")){
        finalSingleResultMap.insert(curViewName,curRecognitionResult);

//    utils->setRecognitionResult(curViewName,curRecognitionResult);

        GrabThread *httpRequest = new GrabThread();
        QTime startTime = QTime::currentTime();
//        qDebug() << "Start uploading single floor recognition result ..." <<startTime;
        httpRequest->syncResultPostQuery(finalSingleResultMap,"onefloor");
        responseCode = httpRequest->getResultSyncResponseCode();
        QTime endTimeE = QTime::currentTime();
//        qDebug() << "Uploaded with code:"<<responseCode<<endTimeE;
        popUpError = httpRequest->getHttpErrorMessage();
        delete httpRequest;
    }
    messageContent = QString::fromUtf8("Floors[%1] action result. \nWith code:%2")
            .arg(curViewName)
            .arg(responseCode);
    if(responseCode.isEmpty()){
        messageContent = QString(tr("Network is invalid."));
    }
    if(!popUpError.isNull() && !popUpError.isEmpty()){
        messageContent = QString::fromUtf8("%1\r\n%2:%3").arg(messageContent).arg(tr("Exception")).arg(popUpError);
    }

    QMessageBox *messageBox = new QMessageBox(tr("Result"),
                                             messageContent,
                                             QMessageBox::Information,
                                             QMessageBox::Ok | QMessageBox::Default,
                                             QMessageBox::Cancel | QMessageBox::Escape,
                                             0);
    messageBox->setTextInteractionFlags(Qt::TextSelectableByMouse);
    messageBox->exec();
    this->uiBlockForm->pBtnOk->setEnabled(true);
}

void BlockForm::slotTotalCount(int index)
{
    int mcount=0;
    for(int i=0;i<comBoxList.size();i++)
    {
        QString s1="UnknownDevice";
        QString s2="NotDevice";
        QString s3="待标定设备";
        QString s4="疑似设备";
        QString s=comBoxList[i]->currentText();

        if((s!=s1)&&(s!=s2)&&(s!=s3)&&(s!=s4))
        {
            mcount+=countList[i]->text().toInt();
        }
    }

    totalCountItem->setText(QString::number(mcount));
}

void BlockForm::on_checkHideRD_stateChanged(int arg1)
{
    COMMONDEF->setHideRecognizedDevice(arg1/*this->uiBlockForm->checkHideRD->isChecked()*/);
}

void BlockForm::on_pBtnAdd_clicked()
{
    if(!uiBlockForm->tableWidget_Added->isVisible()){
        uiBlockForm->tableWidget_Added->setVisible(true);
    }
    AddCustomizedDeviceDialog newCustomizedDeviceDialog(this, utils);
    if(newCustomizedDeviceDialog.exec() == QDialog::Accepted){
        loadCustomizedDevices();
    }
}

void BlockForm::loadCustomizedDevices(){
    QComboBox* tcomBox = nullptr;
    QPushButton* tpushButton = nullptr;
    QTableWidget* tableWidget = uiBlockForm->tableWidget_Added;
    tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableWidget->setRowCount(0);
    tableWidget->clearContents();
    tableWidget->verticalHeader();
    int rowCount = tableWidget->rowCount();
    for(int rowIndex = 0;rowIndex < rowCount;rowIndex++){
        tableWidget->removeRow(rowIndex);
    }
    rowCount = tableWidget->rowCount();
    auto customizedResult = utils->getCustomizedResult();
    auto curViewName = utils->getCurrentViewName();
    QMap<QString, int> newAddedDevices;
    if(!customizedResult.isEmpty() && customizedResult.find(curViewName) != customizedResult.end())
        newAddedDevices = customizedResult.find(curViewName).value();

    QString newAddedDeviceName;
    int newAddedDeviceNumber;
    int newAddedDevicesNumber = newAddedDevices.size();
    for(int insertRowNumber=0;insertRowNumber<newAddedDevicesNumber;insertRowNumber++){
        newAddedDeviceName = newAddedDevices.keys().at(insertRowNumber);
        newAddedDeviceNumber = newAddedDevices.value(newAddedDeviceName);

        tableWidget->insertRow(insertRowNumber);
        //**********************Device Head***************************************/
        QTableWidgetItem* titleItem = new QTableWidgetItem();
        titleItem->setText(tr("New Device"));
        titleItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(insertRowNumber, 0, titleItem);

        //**********************Device Type***************************************/
        tcomBox = new QComboBox(this);
        tcomBox->addItems(nameLocalStrList);
        tcomBox->setCurrentIndex(nameLocalStrList.indexOf(newAddedDeviceName));
        tcomBox->setEnabled(false);
        tableWidget->setCellWidget(insertRowNumber, 1, tcomBox);

        //**********************Device Count***************************************/
        QTableWidgetItem* totalCountItem = new QTableWidgetItem();
    //    totalCountItem = new QTableWidgetItem();
        totalCountItem->setText(QString::number(newAddedDeviceNumber));
        totalCountItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(insertRowNumber, 2, totalCountItem);

        //**********************Device Erease***************************************/
        tpushButton = new QPushButton(this);
        tpushButton->setText(tr("Delete"));
        tableWidget->setCellWidget(insertRowNumber, 3, tpushButton);
        connect(tpushButton, static_cast<void (QPushButton::*)(bool)>(&QPushButton::clicked),this, [=]( bool isDel ) {
            auto curViewCustomizedDevices = utils->getCustomizedResult().find(curViewName).value();
            QMapIterator<QString,int>i(curViewCustomizedDevices);
            while(i.hasNext()) {
               auto item =  i.next();
               auto key  = item.key();
               if (key.contains(newAddedDeviceName,Qt::CaseInsensitive)) {
    //               curViewCustomizedDevices.remove(key);
                   curViewCustomizedDevices.take(key);
                   break;
               }
            }

            utils->setCustomizedResult(curViewName,curViewCustomizedDevices);
            loadCustomizedDevices();
        });

    }
    //resize width
    tableWidget->resizeColumnsToContents();
    tableWidget->resizeRowsToContents();
//    tableWidget->deleteLater();
    setLayout(this->uiBlockForm->verticalLayout_main);
}

QMap<QString,int> BlockForm::mergeCustomizedDevices(QMap<QString,int> recognitionDevices){
    //Merge recognized devices and customized devices
    QMap<QString,int> curRecognitionResult = recognitionDevices;
    auto customizedDevices = utils->getCustomizedResult().value(utils->getCurrentViewName());
    customizedDevices.remove("NotDevice");
    customizedDevices.remove("UnknownDevice");
    customizedDevices.remove("疑似设备");
    customizedDevices.remove("待标定设备");
    auto customizedDeviceNames = customizedDevices.keys();
    foreach(auto customizedDeviceName, customizedDeviceNames){
        auto customizedDeviceTypeCode = typeStrList.at(nameLocalStrList.indexOf(customizedDeviceName));
        int customizedDeviceNumber = customizedDevices.value(customizedDeviceName);
        if(curRecognitionResult.find(customizedDeviceTypeCode) != curRecognitionResult.end()){
            curRecognitionResult.find(customizedDeviceTypeCode).value() += customizedDeviceNumber;
        }else{
            curRecognitionResult.insert(customizedDeviceTypeCode, customizedDeviceNumber);
        }
    }
    return curRecognitionResult;
}
