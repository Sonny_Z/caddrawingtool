#include "entitypropdialog.h"
#include "ui_entitypropdialog.h"
//sort with alphabetical order
#include <QMessageBox>

#include "commondef.h"
#include "fas_insert.h"
#include "fasutils.h"


QString getEntityTypeNameStr(FAS2::EntityType type){
    QString result = "null";
    switch (type) {
    case FAS2::EntityUnknown:
        result = "EntityUnknown";
        break;
    case FAS2::EntityContainer:
        result = "EntityContainer";
        break;
    case FAS2::EntityBlock:
        result = "EntityBlock";
        break;
    case FAS2::EntityFontChar:
        result = "EntityFontChar";
        break;
    case FAS2::EntityInsert:
        result = "EntityInsert";
        break;
    case FAS2::EntityGraphic:
        result = "EntityGraphic";
        break;
    case FAS2::EntityPoint:
        result = "EntityPoint";
        break;
    case FAS2::EntityLine:
        result = "EntityLine";
        break;
    case FAS2::EntityPolyline:
        result = "EntityPolyline";
        break;
    case FAS2::EntityVertex:
        result = "EntityVertex";
        break;
    case FAS2::EntityArc:
        result = "EntityArc";
        break;
    case FAS2::EntityCircle:
        result = "EntityCircle";
        break;
    case FAS2::EntityEllipse:
        result = "EntityEllipse";
        break;
    case FAS2::EntitySolid:
        result = "EntitySolid";
        break;
    case FAS2::EntityConstructionLine:
        result = "EntityConstructionLine";
        break;
    case FAS2::EntityMText:
        result = "EntityMText";
        break;
    case FAS2::EntityText:
        result = "EntityText";
        break;
    case FAS2::EntityDimAligned:
        result = "EntityDimAligned";
        break;
    case FAS2::EntityDimLinear:
        result = "EntityDimLinear";
        break;
    case FAS2::EntityDimRadial:
        result = "EntityDimRadial";
        break;
    case FAS2::EntityDimDiametric:
        result = "EntityDimDiametric";
        break;
    case FAS2::EntityDimAngular:
        result = "EntityDimAngular";
        break;
    case FAS2::EntityDimLeader:
        result = "EntityDimLeader";
        break;
    case FAS2::EntityHatch:
        result = "EntityHatch";
        break;
    case FAS2::EntityImage:
        result = "EntityImage";
        break;
    case FAS2::EntitySpline:
        result = "EntitySpline";
        break;
    case FAS2::EntitySplinePoints:
        result = "EntitySplinePoints";
        break;
    case FAS2::EntityOverlayBox:
        result = "EntityOverlayBox";
        break;
    case FAS2::EntityPreview:
        result = "EntityPreview";
        break;
    case FAS2::EntityPattern:
        result = "EntityPattern";
        break;
    case FAS2::EntityOverlayLine:
        result = "EntityOverlayLine";
        break;
    default:
        break;
    }
    return result;
}

Entitypropdialog::Entitypropdialog(QWidget *parent, FAS_Entity* entity, FASUtils* utils) :
    QDialog(parent),
    clickedEntity(entity),
    utils(utils),
    ui(new Ui::Entitypropdialog)
{
    ui->setupUi(this);
    FASUtils::setFontForUI(this);
    disableControllers();
    if (FASUtils::getHighlight(clickedEntity)){}
    auto entityType = clickedEntity->rtti();

    FAS_Vector minV = clickedEntity->getMin();
    FAS_Vector maxV = clickedEntity->getMax();
    FAS_Vector posV = clickedEntity->getMiddlePoint();

    if(entityType == FAS2::EntityLine || entityType == FAS2::EntitySpline || entityType == FAS2::EntityPolyline){
        minV = clickedEntity->getStartpoint();
        ui->label_minpoint->setText(tr("Start Point Coordinate"));
        maxV = clickedEntity->getEndpoint();
        ui->label_maxpoint->setText(tr("End Point Coordinate"));
    }

    auto relatingPoints = clickedEntity->getConnectedPoint();
    ui->lineEdit_entityId->setText(QString::fromUtf8("%1").arg(clickedEntity->getId()));
    ui->lineEdit_entityType->setText(QString::fromUtf8("%1").arg(getEntityTypeNameStr(clickedEntity->rtti())));
    ui->lineEdit_deviceType->setText(QString::fromUtf8("-"));
    ui->lineEdit_minPointX->setText(QString::number(minV.x, 'f',0));
    ui->lineEdit_minPointY->setText(QString::number(minV.y, 'f',0));
    ui->lineEdit_maxPointX->setText(QString::number(maxV.x, 'f',0));
    ui->lineEdit_maxPointY->setText(QString::number(maxV.y, 'f',0));
    ui->lineEdit_middlePointX->setText(QString::number(posV.x, 'f',0));
    ui->lineEdit_middlePointY->setText(QString::number(posV.y, 'f',0));
    int i = 0;
    QString relatingPointText = QString::fromUtf8("points are listing below:");
    foreach(auto s,relatingPoints){
        relatingPointText = QString::fromUtf8("%1\r\n%2;").arg(relatingPointText).arg(s);
        i++;
    }
    relatingPointText = QString::fromUtf8("%1 %2").arg(i).arg(relatingPointText);
    ui->textEdit_relatingPoints->setText(relatingPointText);


    FAS_Insert* device = entity->getInsert();
    if(device != nullptr)//&& device->getDeviceInfo()->isValidDevice()
    {
        FAS_Block* blk= device->getBlockForInsert();
        if(blk->is_set()){
            FAS_Vector minDV = device->getMin();
            FAS_Vector maxDV = device->getMax();
            FAS_Vector posDV = device->getMiddlePoint();
            auto parentEntity = device->getCalParent();
            auto childEntites = device->getCalChilds();
            auto relatingPoints = device->getConnectedPoint();
            ui->lineEdit_entityId->setText(QString::fromUtf8("%1").arg(clickedEntity->getId()));
            ui->lineEdit_entityType->setText(QString::fromUtf8("%1").arg(getEntityTypeNameStr(clickedEntity->rtti())));
            ui->lineEdit_minPointX->setText(QString::number(minDV.x, 'f',0));
            ui->lineEdit_minPointY->setText(QString::number(minDV.y, 'f',0));
            ui->lineEdit_maxPointX->setText(QString::number(maxDV.x, 'f',0));
            ui->lineEdit_maxPointY->setText(QString::number(maxDV.y, 'f',0));
            ui->lineEdit_middlePointX->setText(QString::number(posDV.x, 'f',0));
            ui->lineEdit_middlePointY->setText(QString::number(posDV.y, 'f',0));
            bool isSetParent = false;
            QString deviceTypeAndSystemType;

            if(!blk->isEmpty() && blk->getType())
            {
                deviceTypeAndSystemType = QString::fromUtf8("%1-%2")
                                                .arg(blk->getType()->getSystemType())
                                                .arg(COMMONDEF->translateToLocal(device->getDeviceInfo()->getName()));
            }else{
                deviceTypeAndSystemType = QString::fromUtf8("%1").arg(COMMONDEF->translateToLocal(device->getDeviceInfo()->getName()));
            }
            ui->lineEdit_deviceType->setText(deviceTypeAndSystemType);
    //        ui->lineEdit_deviceType->setEnabled(false);

    //        QString code = device->getDeviceInfo()->getCodeInfo();
    //        QHash<QString, FAS_Entity*> codeEntityHash = utils->getAllCodeEntityHash();
    //        if(codeEntityHash.find(code) != codeEntityHash.end())
    //        {
    //            ui->CodeTextEdit->setText(device->getDeviceInfo()->getCodeInfo());
    //            oriCodeText = device->getDeviceInfo()->getCodeInfo();
    //        }

            relatingPointText = QString::fromUtf8("%1\r\n>>>>>>[PARENT ID->%2]<<<<<<")
                    .arg(relatingPointText).arg(parentEntity==nullptr?0:parentEntity->getId());


            int j = 0;
            QString childEntitiesStr = QString::fromUtf8("children are listing below:");
            foreach(auto e,childEntites){
                childEntitiesStr = QString::fromUtf8("%1\r\n%2;").arg(childEntitiesStr).arg(e->getId());
                j++;
            }
            childEntitiesStr = QString::fromUtf8("%1 %2").arg(j).arg(childEntitiesStr);

            relatingPointText = QString::fromUtf8("%1\r\n%2").arg(relatingPointText).arg(childEntitiesStr);

            ui->textEdit_relatingPoints->setText(relatingPointText);
            ui->lineEdit_setParent->setText(QString::fromUtf8("%1").arg(parentEntity?parentEntity->getId():0));

            auto items = COMMONDEF->getDeviceNameIdDict()
                    .value(blk->getType()->getSystemType())
                    .value(COMMONDEF->translateToLocal(device->getDeviceInfo()->getName())).keys();
            if(items.size()>0){
                ui->comboBox_detailDeviceType->addItems(items);
                ui->comboBox_detailDeviceType->setCurrentIndex(0);
            }else{
                ui->lineEdit_deviceType->setEnabled(false);
                ui->comboBox_detailDeviceType->setEnabled(false);
            }


        }
        FASUtils::markDeviceChildrenBranch(entity);
        utils->refreshGraphicView();
    }
}

Entitypropdialog::~Entitypropdialog()
{
    delete ui;
}


//bool Entitypropdialog::updateRecord()
//{
//    bool result = false;

//    QString codeText = ui->CodeTextEdit->toPlainText();
//    QString noteText = ui->noteTextEdit->toPlainText();
//    if(oriCodeText == codeText && oriNoteText == noteText)
//        return true;

//    if(nullptr == utils)
//    {
//        QMessageBox::information(this, tr("Warning"), tr("Unexpected error occurs."));
//        return true;
//    }
//    int error = utils->setNewCodeToDevice(clickedEntity->getInsert(), codeText, noteText);
//    if(1 == error)
//    {
//        QMessageBox::information(this, tr("Warning"), tr("Unexpected error occurs."));
//    }
//    else if(2 == error)
//    {
//        QMessageBox::information(this, tr("Warning"), tr("Input code does not match the rule."));
//    }
//    else if(3 == error)
//    {
//        QMessageBox::information(this, tr("Warning"), tr("There is an existing code with inut code. Please input another."));
//    }
//    else if(4 == error)
//    {
//        QMessageBox::information(this, tr("Warning"), tr("Please set layer for code text in 'Tool->Setting'."));
//    }
//    else if(0 == error)
//    {
//        result = true;
//    }
//    return result;
//}
void Entitypropdialog::disableControllers()
{
    ui->lineEdit_entityId->setEnabled(false);
    ui->lineEdit_entityType->setEnabled(false);
    ui->lineEdit_minPointX->setEnabled(false);
    ui->lineEdit_minPointY->setEnabled(false);
    ui->lineEdit_maxPointX->setEnabled(false);
    ui->lineEdit_maxPointY->setEnabled(false);
    ui->lineEdit_middlePointX->setEnabled(false);
    ui->lineEdit_middlePointY->setEnabled(false);
}
void Entitypropdialog::on_buttonBox_accepted()
{

    auto isSetParent = utils->setDeviceParent(ui->lineEdit_entityId->text().toInt()
                                              ,ui->lineEdit_setParent->text().toInt());

    close();
}


void Entitypropdialog::on_buttonBox_clicked(QAbstractButton *button)
{
    close();
}
