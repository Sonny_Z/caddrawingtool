#include "loadingprocess.h"
#include "ui_loadingprocess.h"

#include <QMovie>
#include <QDesktopWidget>

loadingProcess::loadingProcess(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::loadingProcess)
{
    ui->setupUi(this);

    /*** loading gif ***/
    QMovie* movie = new QMovie(":/icons/resource/image/gif/loading.gif");
    ui->label4LoadingGif->setMovie(movie);
    movie->start();
    ui->label4LoadingGif->show();
}

loadingProcess::~loadingProcess()
{
    delete ui;
}
