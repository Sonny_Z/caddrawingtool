#ifndef WAITLONGTIMEDIALOG_H
#define WAITLONGTIMEDIALOG_H

#include <QDialog>
#include <QThread>
#include "longtimeprocess.h"
#include "fasutils.h"
namespace Ui {
class WaitLongTimeDialog;
}

class WaitLongTimeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WaitLongTimeDialog(QWidget *parent = nullptr);
    ~WaitLongTimeDialog();
    void setParas1(int v,FASUtils* utils);
private:
    Ui::WaitLongTimeDialog *ui;
    QTimer* timer;
    QThread thread;
    LongTimeProcess process;
    int v;
    FASUtils* utils;



public slots:
    void slotTimeOut();
    void slotFinished();
    void slotPopUpMessage(const QString&);
signals:
    void sigWork();
    void sigPopUpMessage(const QString&);

};

#endif // WAITLONGTIMEDIALOG_H
