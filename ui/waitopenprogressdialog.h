#ifndef WAITOPENPROGRESSDIALOG_H
#define WAITOPENPROGRESSDIALOG_H
#include "myprogressdialog.h"
#include "fasutils.h"
#include "fas_units.h"
#include "threaddxfprocess.h"


class WaitOpenProgressDialog :public MyProgressDialog
{
    Q_OBJECT

public:
    explicit WaitOpenProgressDialog(QWidget *parent = nullptr);
    ~WaitOpenProgressDialog();
    void setParas(QString title,FASUtils* utils);

private slots:
    void slotTimeOut();
    void ontimer1();
    void soltOpenSuccess();
    void slotOpenFailed();
private:
    QTimer* timer;
    QTimer* timer1;
    QString file;
    FASUtils* utils;
    ThreadDxfProcess* thread;
    int lines;
};

#endif // WAITOPENPROGRESSDIALOG_H
