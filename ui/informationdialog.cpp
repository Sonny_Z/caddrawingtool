#include "informationdialog.h"

//sort with alphabetical order
#include <QMessageBox>

#include "commondef.h"
#include "fas_insert.h"
#include "fasutils.h"

InformationDialog::InformationDialog(QWidget *parent, FAS_Entity* entity, FASUtils* utils) :
    QDialog(parent),
    clickedEntity(entity),
    utils(utils),
    ui(new Ui::InformationDialog)
{
    ui->setupUi(this);
    FASUtils::setFontForUI(this);
    if (FASUtils::getHighlight(clickedEntity))
        ui->radioBtnHighlight->setChecked(true);

    FAS_Insert* device = entity->getInsert();
    if(device != nullptr && device->getDeviceInfo()->isValidDevice())
    {
        FAS_Vector pos = device->getMiddlePoint();

        ui->XTextEdit->setText(QString::number(pos.x));
        ui->XTextEdit->setEnabled(false);

        ui->YTextEdit->setText(QString::number(pos.y));
        ui->YTextEdit->setEnabled(false);

        QString code = device->getDeviceInfo()->getCodeInfo();
        QHash<QString, FAS_Entity*> codeEntityHash = utils->getAllCodeEntityHash();
        if(codeEntityHash.find(code) != codeEntityHash.end())
        {
            ui->CodeTextEdit->setText(device->getDeviceInfo()->getCodeInfo());
            oriCodeText = device->getDeviceInfo()->getCodeInfo();
        }


        ui->TypeTextEdit->setText(COMMONDEF->translateToLocal(device->getDeviceInfo()->getName()));
        ui->TypeTextEdit->setEnabled(false);

        oriNoteText = device->getDeviceInfo()->getNoteText();
        if(0 == oriNoteText.size())
        {
            QList<FAS_Entity*> noteEntityList = utils->getAllNoteEntityHash().values();
            utils->findNoteEntityForDevice(clickedEntity->getInsert(), noteEntityList, oriNoteText);
        }
        ui->noteTextEdit->setText(oriNoteText);
        ui->noteTextEdit->setEnabled(true);
    }
    else
    {
        QMessageBox::information(this, tr("Error"), tr("Cannot find related information!"));
        on_cancelPushButton_clicked();
    }
}

InformationDialog::~InformationDialog()
{
    delete ui;
}

void InformationDialog::on_pushButton_clicked()
{
    bool result = updateRecord();
    if(result)
    {
        close();
    }
}

bool InformationDialog::updateRecord()
{
    bool result = false;

    QString codeText = ui->CodeTextEdit->toPlainText();
    QString noteText = ui->noteTextEdit->toPlainText();
    if(oriCodeText == codeText && oriNoteText == noteText)
        return true;

    if(nullptr == utils)
    {
        QMessageBox::information(this, tr("Warning"), tr("Unexpected error occurs."));
        return true;
    }
    int error = utils->setNewCodeToDevice(clickedEntity->getInsert(), codeText, noteText);
    if(1 == error)
    {
        QMessageBox::information(this, tr("Warning"), tr("Unexpected error occurs."));
    }
    else if(2 == error)
    {
        QMessageBox::information(this, tr("Warning"), tr("Input code does not match the rule."));
    }
    else if(3 == error)
    {
        QMessageBox::information(this, tr("Warning"), tr("There is an existing code with inut code. Please input another."));
    }
    else if(4 == error)
    {
        QMessageBox::information(this, tr("Warning"), tr("Please set layer for code text in 'Tool->Setting'."));
    }
    else if(0 == error)
    {
        result = true;
    }
    return result;
}

void InformationDialog::on_cancelPushButton_clicked()
{
    close();
}

void InformationDialog::on_radioBtnHighlight_toggled(bool checked)
{
    utils->setHighlight(clickedEntity, checked);
}
