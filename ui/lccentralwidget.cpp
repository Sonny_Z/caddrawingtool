/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "lc_centralwidget.h"

//sort with alphabetical order
#include <QMdiArea>


LC_CentralWidget::LC_CentralWidget(QWidget* parent)
    : QFrame(parent)
    , mdi_area(new QMdiArea(this))
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);

    layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(mdi_area);

    mdi_area->setObjectName("mdi_area");
    mdi_area->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mdi_area->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mdi_area->setFocusPolicy(Qt::ClickFocus);
    mdi_area->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mdi_area->setActivationOrder(QMdiArea::ActivationHistoryOrder);

    #if QT_VERSION >= 0x040800
        mdi_area->setTabsMovable(true);
        mdi_area->setTabsClosable(true);
    #endif

    setLayout(layout);
}

LC_CentralWidget::~LC_CentralWidget()
{
    delete layout;
}

QMdiArea* LC_CentralWidget::getMdiArea()
{
    return mdi_area;
}

void LC_CentralWidget::setLayoutMargins(int left, int top, int right, int bottom)
{
    layout->setContentsMargins(left, top, right, bottom);
}
