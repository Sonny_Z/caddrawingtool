#ifndef LC_CENTRALWIDGET_H
#define LC_CENTRALWIDGET_H

#include <QFrame>
#include <QVBoxLayout>

class QMdiArea;

/**
 * a QMdiArea in a QFrame (for QMainWindow.setCentralWidget)
 */
class LC_CentralWidget : public QFrame
{
    Q_OBJECT

public:

    LC_CentralWidget(QWidget* parent);
    ~LC_CentralWidget();
    QMdiArea* getMdiArea();
    void setLayoutMargins(int left=0, int top=0, int right=0, int bottom=0);

protected:

    QMdiArea* mdi_area;

private:
    QVBoxLayout* layout;

};

#endif // LC_CENTRALWIDGET_H
