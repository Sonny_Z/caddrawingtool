#ifndef BLOCKFORM_H
#define BLOCKFORM_H

#include <QComboBox>
#include <QDialog>
#include <QTableWidgetItem>
#include "fas_painter.h"
#include "fasutils.h"

namespace Ui {
class BlockForm;
}
class MyComboBox : public QComboBox
{
    Q_OBJECT    //只有加入Q_OBJECT，才能使用信号signal和槽slot的机制
public:
    explicit MyComboBox(QWidget *parent = 0){};
    ~MyComboBox(){};
protected:
//    virtual void mousePressEvent(QMouseEvent *e);  //添加鼠标点击事件
    void wheelEvent(QWheelEvent *e){
        return;
    }
signals:
//    void clicked();  //自定义点击信号，在mousePressEvent事件发生时触发
};
class BlockForm : public QDialog
{
    Q_OBJECT
public:
    BlockForm(QWidget *parent, FASUtils* utils);
    ~BlockForm();
    void init();

signals:
    void signalSendTypeList(QList<DeviceInfo*> typeList, QList<int> countList);

private slots:
    void on_pBtnCancel_clicked();
    void on_pBtnOk_clicked();
    void on_pBtnShowBranch_clicked();

    void on_pBtnUpload_clicked();
    void slotTotalCount(int index);

    void on_checkHideRD_stateChanged(int arg1);

    void on_pBtnAdd_clicked();

//    bool eventFilter(QObject *, QEvent *);

private:
    Ui::BlockForm *uiBlockForm;

    QList<FAS_Insert*> UIMemoryInserts;
    QStringList typeStrList;
    QStringList typeLocalStrList;
    QStringList nameStrList;
    QStringList nameLocalStrList;
    QList<FAS_Block*> showBlockList;
    QMap<int,QString> showImageIdNameMap;
    QMap<QString,int> blockNameToImgIdMap;
    QList<QComboBox*> comBoxList;
    QMultiHash<QString, FAS_Insert*> nameToEntityHash;

    QMap<int,QString> indexImageTempMap;
    QMap<QString, int> imgNames2ImgIdToBeUpdate;

    QMap<int,QString> curImgIdToRemoteImgIdMap;

    void showBranchResult(QHash<QString,QString> branches);
    void uploadNewImageToBaseAI();
    FASUtils* utils;
    QTableWidgetItem* totalCountItem;
    QList<QTableWidgetItem*> countList;
    QMap<QString,QTableWidgetItem*> uploadStatusList;

    QString popUpMessage = "";

    void loadCustomizedDevices();
    QMap<QString,int> mergeCustomizedDevices(QMap<QString,int> recognitionDevices);
};

#endif // BLOCKFORM_H
