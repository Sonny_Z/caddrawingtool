#ifndef ADDDEVICEDIALOG_H
#define ADDDEVICEDIALOG_H

#include <QDialog>

#include "fasutils.h"
#include "fas_block.h"

namespace Ui {
class AddDeviceDialog;
}

class AddDeviceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDeviceDialog(QWidget *parent = 0, FASUtils* utils = nullptr);
    ~AddDeviceDialog();

    void initUI();

public slots:
    void on_OK_clicked();
    void on_Cancel_clicked();
    void on_snapPoint_clicked();

    void snapPointDone(FAS_Vector point);

private:
    Ui::AddDeviceDialog *ui;
    FASUtils* utils = nullptr;
    QHash<QString, FAS_Block*> nameToBlockMap;
};

#endif // ADDDEVICEDIALOG_H
