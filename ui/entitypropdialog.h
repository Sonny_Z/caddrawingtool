#ifndef ENTITYPROPDIALOG_H
#define ENTITYPROPDIALOG_H

#include <QDialog>
#include <QTextEdit>
#include "ui_entitypropdialog.h"
#include "fasutils.h"

namespace Ui {
class Entitypropdialog;
}

class Entitypropdialog : public QDialog
{
    Q_OBJECT

public:
    explicit Entitypropdialog(QWidget *parent, FAS_Entity* entity, FASUtils* utils);
    ~Entitypropdialog();
private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_clicked(QAbstractButton *button);
    void disableControllers();

private:
    Ui::Entitypropdialog *ui;
    QString entityIDText;
    QString entityTypeText;
    QString deviceTypeText;
    QString minPointVectorXText;
    QString minPointVectorYText;
    QString maxPointVectorXText;
    QString maxPointVectorYText;
    QString middlePointVectorXText;
    QString middlePointVectorYText;
    QTextEdit *relatingPoint;

    FAS_Entity* clickedEntity = nullptr;
    FASUtils* utils = nullptr;
};

#endif // ENTITYPROPDIALOG_H
