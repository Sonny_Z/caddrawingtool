#ifndef ADDDEVICETYPEDIALOG_H
#define ADDDEVICETYPEDIALOG_H

#include <QDialog>

namespace Ui {
class AddDeviceTypeDialog;
}

class AddDeviceTypeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDeviceTypeDialog(QWidget *parent = 0);
    ~AddDeviceTypeDialog();

private slots:
    void on_explorer_clicked();
    void on_OK_clicked();

private:
    Ui::AddDeviceTypeDialog *ui;
};

#endif // ADDDEVICETYPEDIALOG_H
