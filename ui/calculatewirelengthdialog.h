#ifndef CALCULATEWIRELENGTHDIALOG_H
#define CALCULATEWIRELENGTHDIALOG_H

#include <QDialog>
#include "fasutils.h"

namespace Ui {
class CalculateWireLengthDialog;
}

class CalculateWireLengthDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CalculateWireLengthDialog(QWidget *parent = 0, FASUtils* utils = nullptr);
    ~CalculateWireLengthDialog();

public slots:
    void calculateLength();

private:
    Ui::CalculateWireLengthDialog *ui;

    FASUtils* utils = nullptr;
};

#endif // CALCULATEWIRELENGTHDIALOG_H
