#include "addcustomizeddevicedialog.h"

//sort with alphabetical order
#include <QMessageBox>

#include "cadsqlite.h"
#include "commondef.h"
#include "fas_block.h"
#include "fasutils.h"
#include "waitlongtimedialog.h"

AddCustomizedDeviceDialog::AddCustomizedDeviceDialog(QWidget *parent, FASUtils* utils) :
    QDialog(parent),
    utils(utils),
//    allDevices(devices),
    uiAddCustomizedDeviceDialog(new Ui::AddCustomizedDeviceDialog)
{
    uiAddCustomizedDeviceDialog->setupUi(this);
    FASUtils::setFontForUI(this);

    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint &
                                   ~Qt::WindowMinimizeButtonHint &
                                   ~Qt::WindowCloseButtonHint    &
                                   ~Qt::WindowTitleHint          &
                                   ~Qt::WindowContextHelpButtonHint);
    init();
}

AddCustomizedDeviceDialog::~AddCustomizedDeviceDialog()
{
    delete uiAddCustomizedDeviceDialog;
}

void AddCustomizedDeviceDialog::init()
{
    auto nameLocalStrList = COMMONDEF->getDeviceNameList();
    this->uiAddCustomizedDeviceDialog->comboBox_NewDeviceNameList->addItems(nameLocalStrList);
    this->uiAddCustomizedDeviceDialog->comboBox_NewDeviceNameList->setCurrentIndex(0);
    QRegExp rx("^[1-9][0-9]{0,5}$");
    QRegExpValidator *intValidator = new QRegExpValidator(rx, this);
    this->uiAddCustomizedDeviceDialog->lineEdit_NewDeviceNum->setValidator(intValidator);
    this->uiAddCustomizedDeviceDialog->lineEdit_NewDeviceNum->setText("1");
}

void AddCustomizedDeviceDialog::on_OKButton_clicked()
{
    auto curViewName = utils->getCurrentViewName();
    QMap<QString,int> newDevice;
    auto newDeviceName = this->uiAddCustomizedDeviceDialog->comboBox_NewDeviceNameList->currentText();
    auto newDeviceNumber = this->uiAddCustomizedDeviceDialog->lineEdit_NewDeviceNum->text().toInt();

    auto existCustomizedDevices = utils->getCustomizedResult();
    if(existCustomizedDevices.find(curViewName) != existCustomizedDevices.end()){
        auto existDevices = existCustomizedDevices.find(curViewName).value();
        if(existDevices.find(newDeviceName) != existDevices.end()){
            newDeviceNumber = existDevices.value(newDeviceName) + newDeviceNumber;
        }
        existDevices.insert(newDeviceName, newDeviceNumber);
        utils->setCustomizedResult(curViewName,existDevices);
    }else{
        newDevice.insert(newDeviceName,newDeviceNumber);
        utils->setCustomizedResult(curViewName,newDevice);
    }
    accept();
    close();
}

void AddCustomizedDeviceDialog::on_CancelButton_clicked()
{
    close();
}
