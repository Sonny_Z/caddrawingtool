#ifndef WAITDRAWPROGRESSDIALOG_H
#define WAITDRAWPROGRESSDIALOG_H
#include "myprogressdialog.h"
#include "fasutils.h"
#include "fas_units.h"
#include "threaddraw.h"

class WaitDrawProgressDialog:public MyProgressDialog
{
    Q_OBJECT
public:
    WaitDrawProgressDialog(QWidget *parent = nullptr);
    ~WaitDrawProgressDialog();
    void setParas(QG_GraphicView* view);

private slots:
    void slotTimeOut();
    void ontimer1();
    void soltDrawFinished();
private:
    QTimer* timer;
    QTimer* timer1;
    QG_GraphicView* view;
};

#endif // WAITDRAWPROGRESSDIALOG_H
