/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "entityqggv.h"

//sort with alphabetical order
#if QT_VERSION >= 0x050200
#include <QNativeGestureEvent>
#endif

#include "fas_entitycontainer.h"
#include "fas_painterqt.h"

EntityQGGV::EntityQGGV(QWidget* parent, Qt::WindowFlags f, FAS_EntityContainer* container)
    :FAS_GraphicView(parent, f)
    ,redrawMethod(FAS2::RedrawAll)
{
    if (container)
    {
        setContainer(container);
    }
    setFactorX(4.0);
    setFactorY(4.0);
    setBorders(10, 10, 10, 10);

    setMouseTracking(true);
    setFocusPolicy(Qt::NoFocus);
    setAttribute(Qt::WA_NoMousePropagation);

    view_rect = LC_Rect(toGraph(0, 0), toGraph(getWidth(), getHeight()));
    setFixedSize(50, 50);
}

EntityQGGV::~EntityQGGV() {
    delete container;
}

/**
 * @return width of widget.
 */
int EntityQGGV::getWidth() const
{
    return width();
}

/**
 * @return height of widget.
 */
int EntityQGGV::getHeight() const
{
    return height();
}

/**
 * Changes the current background color of this view.
 */
void EntityQGGV::setBackground(const FAS_Color& bg) {
    FAS_GraphicView::setBackground(bg);

    QPalette palette;
    palette.setColor(backgroundRole(), bg);
    setPalette(palette);
}

/**
 * Redraws the widget.
 */
void EntityQGGV::redraw(FAS2::RedrawMethod method)
{
    redrawMethod=(FAS2::RedrawMethod ) (redrawMethod | method);
    update(); // Paint when ready to pain
}


void EntityQGGV::resizeEvent(QResizeEvent* /*e*/) {
    adjustOffsetControls();
    adjustZoomControls();
    getOverlayContainer(FAS2::Snapper)->clear();
    redraw();
}

/**
 * @brief setOffset
 * @param ox, offset X
 * @param oy, offset Y
 */
void EntityQGGV::setOffset(int ox, int oy) {
    FAS_GraphicView::setOffset(ox, oy);
}

void EntityQGGV::getPixmapForView(std::unique_ptr<QPixmap>& pm)
{
    QSize const s0(getWidth(), getHeight());
    if(pm && pm->size()==s0)
        return;
    pm.reset(new QPixmap(getWidth(), getHeight()));
}

/**
 * Handles paint events by redrawing the graphic in this view.
 * usually that's very fast since we only paint the buffer we
 * have from the last call..
 */
void EntityQGGV::paintEvent(QPaintEvent *)
{
    // Re-Create or get the layering pixmaps
    getPixmapForView(PixmapLayer1);
    getPixmapForView(PixmapLayer2);

    // Draw Layer 1
    if (redrawMethod & FAS2::RedrawGrid)
    {
        PixmapLayer1->fill(background);
    }

    if (redrawMethod & FAS2::RedrawDrawing)
    {
        // DRaw layer 2
        PixmapLayer2->fill(Qt::transparent);
        FAS_PainterQt painter2(PixmapLayer2.get());
        painter2.setDrawingMode(drawingMode);
        painter2.setDrawSelectedOnly(false);
        drawLayer2((FAS_Painter*)&painter2);
        //painter2.setDrawSelectedOnly(true);
        //drawLayer2((FAS_Painter*)&painter2);
        painter2.end();
    }

    // Finally paint the layers back on the screen, bitblk to the rescue!

    FAS_PainterQt wPainter(this);
    wPainter.drawPixmap(0,0,*PixmapLayer1);
    wPainter.drawPixmap(0,0,*PixmapLayer2);
    wPainter.end();

    redrawMethod = FAS2::RedrawNone;
}
