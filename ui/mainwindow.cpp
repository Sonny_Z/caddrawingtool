#include "mainwindow.h"
#include "ui_mainwindow.h"

//sort with alphabetical order
#include <QFileDialog>
#include <QMessageBox>
#include <QMovie>
#include <QDesktopWidget>
#include <QLabel>
#include <QProcess>

#include "adddevicedialog.h"
#include "adddevicetypedialog.h"
#include "autocodedialog.h"
#include "blockform.h"
#include "nblockform.h"
#include "calculatewirelengthdialog.h"
#include "commondef.h"
#include "drawnotes.h"
#include "fas_selection.h"
#include "finddevice.h"
#include "informationdialog.h"
#include "entitypropdialog.h"
#include "multiselectdialog.h"
#include "qc_mdiwindow.h"
#include "showhidedialog.h"
#include "settingdialog.h"
#include "loadingprocess.h"
#include "grabthread.h"
#include "qdebug.h"
#include"waitopendialog.h"
#include "waitopenprogressdialog.h"
#include "waitlongtimedialog.h"
#include "threaddraw.h"
#include "waitdrawprogressdialog.h"
#include "deviceRecordform.h"
MainWindow* MainWindow::appWindow = nullptr;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    uiMainWindow(new Ui::MainWindow)
{
    uiMainWindow->setupUi(this);
    FASUtils::setFontForUI(this);
    FAS2::Language language = COMMONDEF->getLanguage();
    switch (language) {
    case FAS2::Chinese:
        on_actionChinese_triggered();
        break;
    default:
        on_actionEnglish_triggered();
        break;
    }

    enableWindowActions(false);
    uiMainWindow->actionUndo->setDisabled(true);
    uiMainWindow->actionRedo->setDisabled(true);
    this->setWindowTitle(this->title);



    appWindow = this;

    COMMONDEF->init();
    FASUtils::initDataBase();
//    if(utils != nullptr){
//        delete utils;
//        utils = nullptr;
//    }
    utils = FASUtils::getInstance();
    utils->clear();
    initMainwindow();
    this->initMax = false;

    /*** create central area ***/
/*    LC_CentralWidget* central = new LC_CentralWidget(this);
    setCentralWidget(central);


    COMMONDEF->init();
    FASUtils::initDataBase();
    utils = FASUtils::getInstance();
    utils->clear();
    utils->setCentralWidget(central);
*/
    utils->initDetectionRules();
//    if(utils != nullptr){
//        delete utils;
//        utils = nullptr;
//    }

    connect(utils, SIGNAL(singalConnectGraphicViewSlot()), this, SLOT(connectGraphicViewSlot()));
    connect(this, SIGNAL(sendSplit_ShowSingal()), this, SLOT(on_actionSplit_Show_triggered()));
/*
    autosaveTimer = new QTimer(this);
    autosaveTimer->setObjectName("autosave");
    connect(autosaveTimer, SIGNAL(timeout()), this, SLOT(fileAutoSave()));
    int ms = 60000 * 5;
    autosaveTimer->start(ms);
*/
//    this->setStatusTip("窗口初始化");
//    statusBar()->setFixedHeight(500);
//    updateStatusBar(tr("Initialized"));
    // 保存
    QSettings settings("Software Inc.", "Icon Editor");
    settings.beginGroup("mainWindow");
    settings.setValue("geometry", saveGeometry());
    settings.setValue("state", saveState());
    settings.endGroup();
}

void MainWindow::updateStatusBar(QString statusContent){
    QDateTime current_date_time =QDateTime::currentDateTime();
    QString current_date =current_date_time.toString("yyyy.MM.dd hh:mm:ss.zzz ddd");
    QLabel *per1 = new QLabel(current_date, this);
    statusBar()->addPermanentWidget(per1);

    statusBar()->showMessage(tr("Status:") + statusContent, 10000);
//    statusBar()->setStyleSheet("color:green");
    statusBar()->setObjectName("QCustomSatusBar");
    statusBar()->setStyleSheet("#QCustomSatusBar{background-color:green;}");
}

void MainWindow::closeMainWindow(){
    this->close();
}

void MainWindow::slotZoomAuto()
{
    //utils->getGraphicView()->zoomAuto();
}
void MainWindow::initMainwindow(){
    if(viewObjWithNavi != nullptr){
        delete viewObjWithNavi;
        viewObjWithNavi = nullptr;
    }

    /*** create central area ***/
    centralViewObj = new LC_CentralWidget(appWindow);
    utils->setCentralWidget(centralViewObj);
    setCentralWidget(centralViewObj);
//    appWindow->setWindowState(Qt::WindowMaximized);  //  最大化

    if(initMax){
        move(QPoint(0, 0));
        appWindow->setGeometry(QApplication::desktop()->availableGeometry());
        int frmX = width();
        int frmY = height();
        appWindow->resize(frmX*0.96, frmY*0.96);
        appWindow->showMaximized();
    }else{
        // 恢复
        QSettings settings("Software Inc.", "Icon Editor");
        settings.beginGroup("mainWindow");
        restoreGeometry(settings.value("geometry").toByteArray());
        restoreState(settings.value("state").toByteArray());
        settings.endGroup();
    }

    //屏幕居中显示
//    int frmX = width();
//    int frmY = height();
//    appWindow->resize(frmX*0.9, frmY*0.9);
//    QDesktopWidget w;
//    int deskWidth = w.width();
//    int deskHeight = w.height();
//    QPoint movePoint(deskWidth / 2 - frmX / 2, deskHeight / 2 - frmY / 2);
//    QPoint movePoint(0, 0);

//    move(movePoint);
//    appWindow->showFullScreen();
//    appWindow->show();
}

MainWindow::~MainWindow()
{
    if(utils != nullptr)
    {
        delete utils;
        utils = nullptr;
    }
    if(translator != nullptr)
    {
        delete translator;
        translator = nullptr;
    }
    recognitionBlockList.clear();
    if(centralViewObj != nullptr){
        delete centralViewObj;
        centralViewObj = nullptr;
    }
    if(viewObjWithNavi != nullptr){
        viewObjWithNavi->close();
        viewObjWithNavi->deleteLater();
        viewObjWithNavi = nullptr;
    }
    if(detecedResultTable != nullptr){
        detecedResultTable->close();
        detecedResultTable->deleteLater();
        detecedResultTable = nullptr;
    }
    if(leftTabNavigate != nullptr){
        leftTabNavigate->close();
        leftTabNavigate->deleteLater();
        leftTabNavigate = nullptr;
    }
////    if(mdiWindow != nullptr){
////        delete mdiWindow;
////        mdiWindow = nullptr;
////    }
}

void MainWindow::showLoadingGif(){
    /*** loading gif ***/
    QLabel* lable = new QLabel(tr("loading..."));
    //QMovie* movie = new QMovie(":/icons/resource/image/gif/loading.gif");
    lable->setAlignment(Qt::AlignCenter);
    QFont font ( "Microsoft YaHei", 22, 75);
    lable->setFont(font);

    //loadingProcess* lp = new loadingProcess();
    //lp->exec();

    /*** create central area ***/
    setCentralWidget(lable);
}

bool MainWindow::openCADFile(){
    bool result = false;
    QString key = "LastFilePath";
    QString lastPath = COMMONDEF->getAppSetting(key);
    lastPath = (lastPath.isNull() || lastPath.isEmpty()) ? "../" : lastPath;
    key.clear();

    QString filter = "Drawing Exchange(*.dxf)";

//    QFileDialog *fileDlg = new QFileDialog(this);
//    fileDlg->setAttribute(Qt::WA_DeleteOnClose);
//    fileDlg->setWindowTitle("/CARRIER/GST");
//    QStringList qstrFilters;
////    qstrFilters<<"Image files(*.bmp *.jpg *.pbm *.pgm *.png *.ppm *.xbm *.xpm)";
////    qstrFilters<<"Any files (*)";
//    qstrFilters<<filter;
//    fileDlg->setNameFilters(qstrFilters);//设置文件过滤器
//    fileDlg->setFileMode(QFileDialog::ExistingFile);
//    fileDlg->setDirectory(lastPath);
    QString file = QFileDialog::getOpenFileName(this, "/CARRIER/GST", lastPath, filter);
//    QString file;
//    if(fileDlg->exec() == QDialog::Accepted)
//    {
//        QStringList strPathList = fileDlg->selectedFiles();
//        file = strPathList.first();
//    }
    QString winTitle = title + " - " + file;
    this->setWindowTitle(winTitle);

    QString fileStr = winTitle + "\r\n";
    auto appPerfLog = COMMONDEF->getAppPerformanceLog();
    COMMONDEF->saveToFile(fileStr,appPerfLog,true);

    QTime startTime = QTime::currentTime();
    if (!file.isEmpty())
    {
        // qDebug() << "Begin Init Main Window ";
        initMainwindow();
        // qDebug() << "End Init Main Window ";
         //bool opened = utils->openFile(file);
         bool opened=false;
         //WaitOpenDialog dlg(this);
         COMMONDEF->readLineNumber=0;

         QFileInfo fileInfo(file);
         QString theKey = "LastFilePath";
         QString theAbsolutePath = fileInfo.absolutePath();
         COMMONDEF->setAppSetting(theKey,theAbsolutePath);//记录路径到QSetting中保存
         theKey.clear();
         theAbsolutePath.clear();

         auto tmpDir = COMMONDEF->tempDirPath();
         auto temName = QString::fromUtf8("temp000%1").arg(tmpFileCode);
         utils->clearDir(tmpDir);/**< 如果目标目录存在，则进行清理 */
         if(!utils->isDirExist(tmpDir) || !utils->isDirExist(tmpDir + "/" + temName)){    /**< 如果目标目录不存在，则进行创建 */
             return false;
         }
         QString tmpFileName = QDir(QApplication::applicationDirPath())
                 .absoluteFilePath(QString::fromUtf8("%1/%2.dxf").arg(tmpDir).arg(temName));
         if(!QFile::copy(file, tmpFileName))
         {
             return false;
         }


         //状态栏
//         QStatusBar * sBar = this->statusBar();//创建状态栏
//         QLabel *lable = new QLabel(this);
//         lable->setText(tr("Parsing file:"));
//         sBar->addWidget(lable);//状态栏添加组件
//         this->setStatusTip("打开文件");

         file = tmpFileName;
         WaitOpenProgressDialog dlg(this);
         dlg.setRange(0,100);
         dlg.setParas(file, utils);
         // qDebug() << "Start open file with WaitOpenProgressDialog: " << file;

//        statusBar()->showMessage("Start open file with WaitOpenProgressDialog: ");

         if(dlg.exec()==QDialog::Accepted)
         {
             opened=true;
             COMMONDEF->firstOpen=true;
         }
         else
         {
            opened=false;
         }
        this->tmpFileCode ++;
        if(opened)
        {
            QTime openFileTime = QTime::currentTime();
            // qDebug() << "It takes " << startTime.secsTo(openFileTime) << " seconds to open file.";

            // qDebug() << "file is opened successfully." ;
            enableWindowActions(true);
            uiMainWindow->actionUpload_All_Result->setDisabled(true);
            uiMainWindow->actionOpen->setDisabled(true);
            result = true;
        }
        else
        {
            QMessageBox::information(this, tr("Warning"), tr("Open file failed.Chinese characters in path can not be supported."));
        }
        // qDebug() << "End  open file with WaitOpenProgressDialog: " << file;

        /*** create graphicview ***/
        createGraphicArea();
        if(utils != nullptr)
        {
            /*** update central area ***/
//            auto central = utils->getCentralWidget();
//            utils->zoomAuto();
//            central->repaint();
//            central->update();
//            setCentralWidget(central);
//            utils->setCentralWidget(central);
        }
//        on_actionSplit_Show_triggered();
        // qDebug() << "GraphicArea is created successfully." ;

        QTime graphicTime = QTime::currentTime();
        // qDebug() << "It takes " << startTime.secsTo(graphicTime) << " seconds to create graphic.";

        /*
        // show blocklist
        BlockForm blockFormDia(this, utils);
        blockFormDia.exec();
        // qDebug() << "BlockForm is executed successfully." ;
        */
        // qDebug() << "Start  open file with WaitDrawProgressDialog dlgProcesser";
        COMMONDEF->pix->fill(Qt::transparent);
        WaitDrawProgressDialog dlgProcesser;
        dlgProcesser.setRange(0,100);
        dlgProcesser.setParas(utils->getGraphicView());
        dlgProcesser.exec();
        // qDebug() << "Start  open file with WaitDrawProgressDialog dlgProcesser";
    }else if(nullptr == utils->getRSDocument() || nullptr == utils->getORSDocument()){
        initMainwindow();
    }
    //COMMONDEF->firstOpen=false;
    QTime endTime = QTime::currentTime();
    // qDebug() << "It takes " << startTime.secsTo(endTime) << " seconds.";
    return result;
}

void MainWindow::on_actionOpen_triggered()
{
    auto usedMemorySizeByApp = getUsedMemory();
    auto isOpened = openCADFile();
    if(isOpened){
        QString key = "systemMemoryLimitFactor";
        auto settingValue = COMMONDEF->getAppSetting(key);
        if(!settingValue.isEmpty()){
            usedMemorySizeLimitFactor = settingValue.toDouble();
        }else{
            COMMONDEF->setAppSetting(key,QString::fromUtf8("%1").arg(usedMemorySizeLimitFactor));
        }
        auto usedMemorySizeByFile = getUsedMemory() - usedMemorySizeByApp;
        if(usedMemorySizeLimitFactor < usedMemorySizeByFile){
            uiMainWindow->actionAuto_Split_View->setDisabled(true);
        }
//        on_actionSplit_Show_triggered();
        emit sendSplit_ShowSingal();
    }
}

void MainWindow::on_actionSwitchSVGorCAD_triggered()
{
    this->showSVG = !this->showSVG;
    if(nullptr == utils)
        return;

    utils->showSvgIoconForAllDevices(showSVG);
    utils->refreshGraphicView();
}

void MainWindow::on_actionFindDevice_triggered()
{
    if (utils != nullptr)
    {
         FindDevice mFindDevice(this, utils);;
         mFindDevice.exec();
    }
}

void MainWindow::createGraphicArea()
{
    mdiWindow = new QC_MDIWindow(this);
    utils->createGraphicView(mdiWindow);
    connect(mdiWindow,SIGNAL(signalShow()),this,SLOT(slotZoomAuto()));
}

void MainWindow::connectGraphicViewSlot()
{
    connect(utils->getCurMdiWindow(), SIGNAL(signalClosing(QC_MDIWindow*)), this, SLOT(doCloseFile()));
    connect(utils->getGraphicView(), SIGNAL(signalCreateMultiSelectedDialog()), this, SLOT(createMultiSelectedDialog()));
    connect(utils->getGraphicView(), SIGNAL(signalDeviceSelect(FAS_Entity*)), this, SLOT(createInformationDialog(FAS_Entity*)));
}

void MainWindow::on_actionShowHide_triggered()
{
    ShowHideDialog showHideDia(this);
    showHideDia.exec();
}

void MainWindow::on_actionAutoCode_triggered()
{
    AutoCodeDialog codeDialog(this);
    codeDialog.exec();
}

void MainWindow::on_actionSave_triggered()
{

    bool ret = false;
    if (utils->getORSDocument())
    {
        QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );
        ret = utils->saveFile(utils->getORSDocument()->getFilename());
        QApplication::restoreOverrideCursor();
    }
    if(ret)
    {
        QMessageBox::information(this, tr("Information"), tr("Saved"));
    }
    else
    {
        QMessageBox::information(this, tr("Information"), tr("Not Saved"));
    }
}

void MainWindow::on_actionSave_As_triggered()
{
    bool ret = false;
    if(utils->getORSDocument())
    {
        QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );
        FAS2::FormatType format = FAS2::FormatDXF;

        QString filter = "Drawing Exchange(*.dxf)";
        QString fileName = QFileDialog::getSaveFileName(this, "/CARRIER/GST", ".", filter);
        ret = utils->saveAsFile(fileName, format);
        QApplication::restoreOverrideCursor();
    }
    if(ret)
    {
        QMessageBox::information(this, tr("Information"), tr("Saved"));
    }
    else
    {
        QMessageBox::information(this, tr("Information"), tr("Not Saved"));
    }
}

void MainWindow::on_actionClose_File_triggered()
{
    utils->getCurMdiWindow()->close();
    doCloseFile();
    initMainwindow();
//    uiMainWindow->actionClose_File->setEnabled(false);
//    uiMainWindow->actionOpen->setEnabled(true);
}

void MainWindow::on_actionAdd_New_Device_Type_triggered()
{
    AddDeviceTypeDialog addDevice;
    addDevice.exec();
}
/*
void MainWindow::on_activeDrawing_triggered(QAction* action)
{
    if(nullptr == action)
    {
        return;
    }
    QString file = action->text();
    utils->activeExistingFile(file);
    QString winTitle = title + file;
    this->setWindowTitle(winTitle);
    enableWindowActions(true);
}
*/
void MainWindow::createMultiSelectedDialog()
{
//    if(utils->getCurrentViewName().contains("Auto-Subview")){
//        return;
//    }

    // qDebug() << "Start  creating new view";
    FAS_Selection s(*utils->getRSDocument(), utils->getGraphicView());
    QList<FAS_Entity*> selectedEntityList = s.getSelectedEntityList();
//    QList<FAS_Insert*> allDevices = FASUtils::getAllInsertEntities(selectedEntityList);
//    if(allDevices.empty())
//    {
//        return;
//    }
//    MultiSelectDialog mMultiSelectDialog(this, utils, allDevices);
//    mMultiSelectDialog.exec();

    if(!selectedEntityList.empty()){
        NewViewDialog newViewBuildingDialog(this, utils);
        if(newViewBuildingDialog.exec() == QDialog::Accepted){
            if(viewObjWithNavi != nullptr){
                updateSideNavigate();
//                on_subViewSelected_triggered(utils->getCurrentViewName());
            }
        }
    }else{
        QMessageBox::information(this, tr("Result"), tr("Invalid Selection."));
    }
    // qDebug() << "End  creating new view";
}

void MainWindow::createInformationDialog(FAS_Entity* selectedEntity)
{
    if(nullptr == selectedEntity)
        return;
//    if(selectedEntity->getInsert()->getDeviceInfo()->isValidDevice())
//    {
//        InformationDialog infoDialog(this, selectedEntity, utils);
//        infoDialog.exec();
//    }

//      Entitypropdialog infoDialog(this, selectedEntity, utils);
//      infoDialog.exec();

    auto entityInfo = selectedEntity->rtti() == FAS2::EntityInsert ? selectedEntity->getInsert()->getDeviceInfo() : NULL;
    auto entityID = selectedEntity->getId();
    auto nameStrList = COMMONDEF->getDeviceNameList();
    auto typeStrList = COMMONDEF->getDeviceTypeList();
    QString messageContent = "Null";
    if(entityInfo){
        auto nameIndex = typeStrList.indexOf(entityInfo->getTypeCode());
        auto deviceName = nameIndex>=0 ? nameStrList.at(nameIndex) : NULL;
        if(!deviceName.isNull() && !deviceName.isEmpty()){
            messageContent = deviceName + "   " + utils->getEntityTypeNameStr(selectedEntity->rtti());
        }
    }else{
        messageContent = utils->getEntityTypeNameStr(selectedEntity->rtti());
    }
    statusBar()->showMessage(QString::fromUtf8("%1%2 ID:%3")
                             .arg(tr("Device information:"))
                             .arg(messageContent)
                             .arg(entityID), 10000);
}

void MainWindow::doCloseFile()
{
    // qDebug() << "Start  MainWindow::doCloseFile()";
    if(utils != nullptr)
    {
        QString file;
        if(utils->getORSDocument() != nullptr){
            file = utils->getORSDocument()->getFilename();
        }
        utils->closeFile(file);

        this->setWindowTitle(this->title);
        enableWindowActions(false);
        uiMainWindow->actionUndo->setDisabled(true);
        uiMainWindow->actionRedo->setDisabled(true);
        uiMainWindow->actionDelete_View->setDisabled(true);
        uiMainWindow->actionOpen->setEnabled(true);
        if(viewObjWithNavi != nullptr){
            delete viewObjWithNavi;
            viewObjWithNavi = nullptr;
        }
        if(detecedResultTable != nullptr){
            detecedResultTable = nullptr;
        }
        if(leftTabNavigate != nullptr){
            leftTabNavigate = nullptr;
        }
    }
    auto tmpDir = COMMONDEF->tempDirPath();
    utils->clearDir(tmpDir);/**< 如果目标目录存在，则进行清理 */

    auto usedMemorySizeByApp = getUsedMemory();
    if(usedMemorySizeByApp >= 1350){
        QMessageBox::information(this,tr("information"),tr("System resource is limited, restart the applicaiton please."));
        exit(0);
    }
    // qDebug() << "End  MainWindow::doCloseFile()";
}

void MainWindow::setUndoEnable(bool enable)
{
    this->uiMainWindow->actionUndo->setEnabled(enable);
}

void MainWindow::setRedoEnable(bool enable)
{
    this->uiMainWindow->actionRedo->setEnabled(enable);
}

void MainWindow::on_actionUndo_triggered()
{
    if(utils != nullptr)
    {
        utils->actionUndo(true);
    }
}

void MainWindow::on_actionRedo_triggered()
{
    if(utils != nullptr)
    {
        utils->actionUndo(false);
    }
}

void MainWindow::on_actionDelete_triggered()
{
    if(utils != nullptr)
    {
        utils->actionModifyDelete();
    }
}

void MainWindow::on_actionAuto_Zoom_triggered()
{
    if(utils != nullptr)
    {
        utils->zoomAuto();
    }
}

void MainWindow::on_actionAdd_One_Device_triggered()
{
    if(utils != nullptr)
    {
        AddDeviceDialog* addDevice = new AddDeviceDialog(this, utils);
        connect(utils->getGraphicView(), SIGNAL(signalSnapPointDone(FAS_Vector)), addDevice, SLOT(snapPointDone(FAS_Vector)));
        addDevice->exec();
    }
}

void MainWindow::on_actionDraw_Note_triggered()
{
    if(utils != nullptr)
    {
        DrawNotes* drawNotesDialog = new DrawNotes(this, utils);
        connect(utils->getGraphicView(), SIGNAL(signalSnapPointDone(FAS_Vector)), drawNotesDialog, SLOT(snapPointDone(FAS_Vector)));
        drawNotesDialog->exec();
    }
}

void MainWindow::on_actionCalculateWireLength_triggered()
{
    CalculateWireLengthDialog dialog(this, utils);
    dialog.exec();
}

void MainWindow::on_actionSetting_triggered()
{
    SettingDialog settingDia(this, utils);
    settingDia.exec();
}

void MainWindow::on_actionExport_as_image_triggered()
{
    if(utils != nullptr && utils->getRSDocument() != nullptr)
    {
        utils->exportFileAsImage(utils->getRSDocument()->getFilename());
    }
}

void MainWindow::on_actionExport_as_PDF_triggered()
{
    if(utils != nullptr && utils->getRSDocument() != nullptr)
    {
        utils->exportFileAsPDF(utils->getRSDocument()->getFilename());
    }
}

void MainWindow::keyPressEvent(QKeyEvent* e)
{
    if(utils != nullptr && utils->getGraphicView() != nullptr)
    {
        utils->getGraphicView()->keyPressEvent(e);
    }
    QMainWindow::keyPressEvent(e);
}

void MainWindow::keyReleaseEvent(QKeyEvent* e)
{
    if(utils != nullptr && utils->getGraphicView() != nullptr)
    {
        utils->getGraphicView()->keyReleaseEvent(e);
    }
    QMainWindow::keyReleaseEvent(e);
}
void MainWindow::resizeEvent(QResizeEvent* e)
{
    COMMONDEF->mainwindowCenterx=this->pos().x()+this->width()/2;
    COMMONDEF->mainwindowCentery=this->pos().y()+this->height()/2;

}
void MainWindow::moveEvent(QMoveEvent *ev)
{
    COMMONDEF->mainwindowCenterx=this->pos().x()+this->width()/2;
    COMMONDEF->mainwindowCentery=this->pos().y()+this->height()/2;
}
void MainWindow::enableWindowActions(bool enable)
{
    uiMainWindow->actionFindDevice->setEnabled(enable);
    //uiMainWindow->actionShow_nBlockList->setEnabled(enable);
    uiMainWindow->actionShowHide->setEnabled(enable);
    uiMainWindow->actionAutoCode->setEnabled(enable);
    uiMainWindow->actionSave->setEnabled(enable);
    uiMainWindow->actionSave_As->setEnabled(enable);
    uiMainWindow->actionClose_File->setEnabled(enable);
    uiMainWindow->actionAuto_Zoom->setEnabled(enable);
    uiMainWindow->actionDraw_Note->setEnabled(enable);
    uiMainWindow->actionAdd_One_Device->setEnabled(enable);
    uiMainWindow->actionAdd_New_Device_Type->setEnabled(enable);
    uiMainWindow->actionIES_Device_Block->setEnabled(enable);
    uiMainWindow->actionD_A_Device_Block->setEnabled(enable);
    uiMainWindow->actionSetting->setEnabled(enable);
    uiMainWindow->actionDelete->setEnabled(enable);
    uiMainWindow->actionDelete_View->setEnabled(enable);
    uiMainWindow->actionSave_Devices_to_Database->setEnabled(enable);
    uiMainWindow->actionto_Cloud->setEnabled(enable);
    uiMainWindow->actionto_LocalDB->setEnabled(enable);
    uiMainWindow->actionSwitchSVGorCAD->setEnabled(enable);
    uiMainWindow->actionCalculateWireLength->setEnabled(enable);
    uiMainWindow->actionCheckLayer->setEnabled(enable);
    uiMainWindow->actionConnectTwoDevices->setEnabled(enable);
//    uiMainWindow->actionSplit_Show->setEnabled(enable);
    uiMainWindow->actionUpload_Single_Result->setEnabled(enable);
    uiMainWindow->actionUpload_All_Result->setEnabled(enable);
    uiMainWindow->actionAuto_Split_View->setEnabled(enable);
    uiMainWindow->actionCheck_Single_Result->setEnabled(enable);
    uiMainWindow->actionCheck_All_Result->setEnabled(enable);
}

void MainWindow::fileAutoSave()
{
    if(utils != nullptr && utils->getORSDocument() != nullptr)
    {
        utils->saveFile(utils->getORSDocument()->getFilename(), true);
    }
}

void MainWindow::on_actionChinese_triggered()
{
    if(nullptr == translator)
    {
        translator = new QTranslator();
    }
    translator->load(":/font/resource/Language_zh.qm");
    QApplication::installTranslator(translator);

    this->uiMainWindow->retranslateUi(this);
    COMMONDEF->setLanguage(FAS2::Chinese);
}

void MainWindow::on_actionEnglish_triggered()
{
    if(translator != nullptr)
    {
        QApplication::removeTranslator(translator);
    }
    this->uiMainWindow->retranslateUi(this);
    COMMONDEF->setLanguage(FAS2::English);
}

void MainWindow::on_actionSave_Devices_to_Database_triggered()
{
    utils->storeDetectionRulesToDB();
    utils->storeDeviceListToDB(utils->getAllDevicesFromRS());

    QMessageBox::information(this, tr("Result"), tr("Devices are saved to Database."));
}

void MainWindow::on_actionCheckLayer_triggered()
{
    FAS_Selection s(*utils->getRSDocument(), utils->getGraphicView());
    QList<FAS_Entity*> selectedEntityList = s.getSelectedEntityList();
    QSet<QString> nameSet;
    for (FAS_Entity* oneEntity : selectedEntityList)
    {
        if(oneEntity->getLayer() != nullptr)
            nameSet.insert(oneEntity->getLayer()->getName());
        else
            nameSet.insert("");
    }
    QString message = "";
    if(nameSet.empty())
        message = tr("No entity selected.");
    else if(nameSet.count() > 1)
        message = tr("Slected entities are located in difference layers.");
    else
        message = tr("Selected entity(s) located in layer: ") + nameSet.values().at(0);
    QMessageBox::information(this, tr("Information"), message);
}

void MainWindow::on_actionConnectTwoDevices_triggered()
{
    FAS_Selection s(*utils->getRSDocument(), utils->getGraphicView());
    QList<FAS_Entity*> selectedEntityList = s.getSelectedEntityList();
    QList<FAS_Insert*> selectedDevices;
    for (FAS_Entity* oneEntity : selectedEntityList)
    {
        if(oneEntity->rtti() == FAS2::EntityInsert)
        {
            FAS_Insert* device = oneEntity->getInsert();
            if(device != nullptr && device->getDeviceInfo()->isValidDevice())
            {
                selectedDevices.append(device);
            }
        }
    }
    if(selectedDevices.count() != 2)
    {
        QMessageBox::information(this, tr("Alert"), tr("Please make sure two valid devices are selected."));
        return;
    }
    if(utils != nullptr)
    {
        if(utils->getWireLineLayer() == nullptr){
            QMessageBox::information(this, tr("Alert"), tr("Please select layer for new created wire in Tools->Setting."));
            return;
        }
        utils->createLineInGraphic(selectedDevices.at(0)->getMiddlePoint(), selectedDevices.at(1)->getMiddlePoint());
    }
}

void MainWindow::on_actionShow_nBlockList_triggered()
{
    if (utils != nullptr)
    {
        NBlockForm nBlockFormDia(this, utils);
        nBlockFormDia.exec();
    }
}

void MainWindow::on_actionto_LocalDB_triggered()
{
    // qDebug() << "Save new features to loacl DB.";
    utils->addDetectionRuleToLocalDB();
    utils->clearNewRules4Local();
    QMessageBox::information(this, tr("Result"), tr("Rules are saved to local Database."));
}

void MainWindow::on_actionto_Cloud_triggered()
{
    // qDebug() << "Save new features to cloud.";
    utils->addDetectionRuleToCloud();
    utils->clearNewRules4Cloud();
    QMessageBox::information(this, tr("Result"), tr("Rules are saved to Cloud."));
}

void MainWindow::on_actionIES_Device_Block_triggered()
{
    uiMainWindow->actionUpload_All_Result->setEnabled(true);
    this->enableWindowActions(false);
    COMMONDEF->setSystem(FAS2::IESsystem);
    COMMONDEF->buildDeviceInfoList();
    if(!COMMONDEF->isNetWorkOnline()){
        QMessageBox::information(this,tr("Warning"),tr("Network exception! Check the connection please."));
        this->enableWindowActions(true);
        return;
    }
    if(1)
    {
        if (utils != nullptr)
        {
            WaitLongTimeDialog dlg(this);
            dlg.setModal(false);
            dlg.setParas1(2,utils);
            dlg.show();
            dlg.exec();
        }
    }
    else
    {
        if (utils != nullptr)
        {
            BlockForm blockFormDia(this, utils);
            blockFormDia.setModal(false);
            blockFormDia.show();
            blockFormDia.exec();
        }
    }
    updateSideNavigate();
    this->enableWindowActions(true);
//    uiMainWindow->actionUpload_Single_Result->setEnabled(true);
//    uiMainWindow->actionUpload_All_Result->setEnabled(true);
}

//void MainWindow::on_actionIES_Device_nBlock_triggered()
//{
//    COMMONDEF->setSystem(FAS2::IESsystem);
//    COMMONDEF->buildDeviceInfoList();
//    if (utils != nullptr)
//    {
//        NBlockForm nBlockFormDia(this, utils);
//        nBlockFormDia.exec();
//    }
//}

void MainWindow::on_actionD_A_Device_Block_triggered()
{

//    COMMONDEF->setSystem(FAS2::D_And_Asystem);
//    COMMONDEF->buildDeviceInfoList();
//    if (utils != nullptr)
//    {
//        BlockForm blockFormDia(this, utils);
//        blockFormDia.exec();
//    }
    uiMainWindow->actionUpload_All_Result->setEnabled(true);
    this->enableWindowActions(false);
    COMMONDEF->setSystem(FAS2::D_And_Asystem);
    COMMONDEF->buildDeviceInfoList();
    if(!COMMONDEF->isNetWorkOnline()){
        QMessageBox::information(this,tr("Warning"),tr("Network exception! Check the connection please."));
        this->enableWindowActions(true);
        return;
    }
    if(1)
    {
        if (utils != nullptr)
        {
            WaitLongTimeDialog dlg(this);
            dlg.setModal(false);
            dlg.setParas1(2,utils);
            dlg.show();
            dlg.exec();
        }
    }
    else
    {
        if (utils != nullptr)
        {
            BlockForm blockFormDia(this, utils);
            blockFormDia.setModal(false);
            blockFormDia.show();
            blockFormDia.exec();
        }
    }
    updateSideNavigate();
    this->enableWindowActions(true);
}

//void MainWindow::on_actionD_A_Device_nBlock_triggered()
//{
//    COMMONDEF->setSystem(FAS2::D_And_Asystem);
//    COMMONDEF->buildDeviceInfoList();
//    if (utils != nullptr)
//    {
//        NBlockForm nBlockFormDia(this, utils);
////        nBlockFormDia.setAttribute(Qt::WA_DeleteOnClose);
//        nBlockFormDia.exec();
//    }
//}


void MainWindow::loadDetectedResult(){
    auto curViewName = utils->getCurrentViewName();
    recognitionBlockList = utils->getRecognizedDevices().value(curViewName);
    int loopCount=recognitionBlockList.count();
    auto nameLocalStrList = COMMONDEF->getDeviceNameList();
    auto typeStrList = COMMONDEF->getDeviceTypeList();

    if(nullptr == detecedResultTable){
        detecedResultTable = new QTableWidget();

    }
    detecedResultTable->clearContents();
    detecedResultTable->verticalHeader();
    int rowCount = detecedResultTable->rowCount();
    for(int rowIndex = 0;rowIndex < rowCount;rowIndex++){
        detecedResultTable->removeRow(rowIndex);
    }
    detecedResultTable->setColumnCount(2);
    detecedResultTable->setSelectionMode(QAbstractItemView::SingleSelection);//只能单选
    QStringList m_Header;
    m_Header/*<<QString(tr("Device"))*/<<QString(tr("Type"))<<QString(tr("Count"))/*<<QString(tr("Status"))*/;
    detecedResultTable->setHorizontalHeaderLabels(m_Header);//添加横向表头
    detecedResultTable->verticalHeader()->setVisible(true);//纵向表头可视化
    detecedResultTable->horizontalHeader()->setVisible(true);//横向表头可视化
    //ui->tableWidget->setShowGrid(false);//隐藏栅格
    detecedResultTable->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置编辑方式：禁止编辑表格
    detecedResultTable->setSelectionBehavior(QAbstractItemView::SelectRows);//设置表格选择方式：设置表格为整行选中
    //ui->qTableWidget->setSelectionBehavior(QAbstractItemView::SelectColumns);//设置表格选择方式：设置表格为整列选中
    detecedResultTable->setSelectionMode(QAbstractItemView::SingleSelection);//选择目标方式
    detecedResultTable->setStyleSheet("selection-background-color:pink");//设置选中颜色：粉色

    detecedResultTable->setRowCount(loopCount + 1);
    int totalCount = 0;
    int ii = 0;
    QLabel *l1=nullptr;
    QWidget *widgetChild=nullptr;
    QVBoxLayout *vLayout=nullptr;
    QComboBox* tcomBox=nullptr;
    QTableWidgetItem* countItem=nullptr;
    QTableWidgetItem* uploadStatusItem=nullptr;
    QString uploadStatus = QString(tr("-"));
    for(int ii=0;ii<loopCount;ii++)
    {
//        detecedResultTable->setRowHeight(ii,100);
//        detecedResultTable->verticalHeader()->setDefaultSectionSize(100);

        auto deviceData = recognitionBlockList.at(ii);

/*        auto blkSavePath = deviceData->getImageSrc();
        l1 = new QLabel();     //创建lable

        l1->setAlignment(Qt::AlignHCenter);
        l1->setScaledContents(true);
        l1->setMaximumWidth(46);
        l1->setMaximumHeight(56);//设置居中
        l1->setPixmap(QPixmap(blkSavePath).scaled(l1->size(),Qt::KeepAspectRatio));    //加载图片

        widgetChild = new QWidget;
        vLayout = new QVBoxLayout();
        vLayout->addWidget(l1);
        vLayout->setAlignment(l1,Qt::AlignCenter);
        widgetChild->setLayout(vLayout);
        widgetChild->setStyleSheet("background-color:black;");
        detecedResultTable->setCellWidget(ii, 0, widgetChild);
*/
        //**********************Device Type***************************************/
        tcomBox = new QComboBox(this);
        tcomBox->addItems(nameLocalStrList);
        auto defaultIndex = 0;
        int indexCheck = typeStrList.indexOf(deviceData->getType());
        int index = indexCheck >= 2 ? indexCheck : defaultIndex;
        tcomBox->setCurrentIndex(index);
        tcomBox->setEnabled(false);
        detecedResultTable->setCellWidget(ii, 0, tcomBox);
//        comBoxList.append(tcomBox);

        //**********************Device Count***************************************/
        countItem = new QTableWidgetItem();
//        countList.append(countItem);
        countItem->setText(QString::number(deviceData->getCount()));
        countItem->setFlags(Qt::ItemIsEditable);
        countItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
        detecedResultTable->setItem(ii, 1, countItem);


        //**********************Upload Status***************************************/
/*        uploadStatusItem = new QTableWidgetItem();
        uploadStatusItem->setText(uploadStatus);
        uploadStatusItem->setTextColor(FAS_Color(0,255,0).toQColor());
//        uploadStatusItem->setBackground(FAS_Color(0,255,0).toQColor());
        uploadStatusItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
        uploadStatusItem->setFlags(Qt::ItemIsEditable);
        detecedResultTable->setItem(ii, 3, uploadStatusItem);
*/

        if(deviceData->getRecognizedStatus()){
            totalCount += deviceData->getCount();
        }
//        qDebug() << "Navigate Result Device:"
//               << "  Type Name->" << tcomBox->currentText()
//               << "  Count->" <<deviceData->getCount()
//               << "  TotalCount->" << totalCount
//               << "  RecogStatus->" << deviceData->getRecognizedStatus();
//        deleteLater();
    }
    loadCustomizedDevices();
//    qDeleteAll(inserts);
//    inserts.clear();
    // show total count
    QTableWidgetItem* totalItem = new QTableWidgetItem();
    totalItem->setText(tr("Total count"));
    totalItem->setFlags(Qt::ItemIsEditable);
    detecedResultTable->setItem(loopCount, 0, totalItem);
    QTableWidgetItem* totalCountItem = new QTableWidgetItem();
    totalCountItem = new QTableWidgetItem();
    totalCountItem->setText(QString::number(totalCount));
    totalCountItem->setFlags(Qt::ItemIsEditable);
    totalCountItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
    detecedResultTable->setItem(loopCount, 1, totalCountItem);

    //resize width
    detecedResultTable->setRowHeight(loopCount,10);
    detecedResultTable->resizeColumnsToContents();
//    tableWidget->deleteLater();
//    systemTypeStr.clear();

//    deleteLater();
    this->leftTabNavigate->setTabText(1,curViewName);
}

void MainWindow::loadCustomizedDevices(){
    auto customizedResult = utils->getCustomizedResult();
    auto curViewName = utils->getCurrentViewName();
    int customizedRowCount;
    QMap<QString, int> newAddedDevices;
    if(!customizedResult.isEmpty() && customizedResult.find(curViewName) != customizedResult.end()){
        newAddedDevices = customizedResult.find(curViewName).value();
        customizedRowCount = newAddedDevices.count();
        if(newAddedDevices.isEmpty()) return;
    }else{
        return;
    }

    QComboBox* tcomBox = nullptr;
//    QPushButton* tpushButton = nullptr;
    detecedResultTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    int rowCount = detecedResultTable->rowCount();
    auto updateRowCount = rowCount + customizedRowCount;
    detecedResultTable->setRowCount(updateRowCount);

    auto nameLocalStrList = COMMONDEF->getDeviceNameList();
    auto typeStrList = COMMONDEF->getDeviceTypeList();
    QString newAddedDeviceName;
    int newAddedDeviceNumber;
    for(int insertRowNumber=rowCount;insertRowNumber<updateRowCount;insertRowNumber++){
        newAddedDeviceName = newAddedDevices.keys().at(insertRowNumber - rowCount);
        newAddedDeviceNumber = newAddedDevices.value(newAddedDeviceName);

//        detecedResultTable->insertRow(insertRowNumber);
        //**********************Device Head***************************************/
/*        QTableWidgetItem* titleItem = new QTableWidgetItem();
        titleItem->setText(tr("Customized Device"));
        titleItem->setFlags(Qt::ItemIsEditable);
        detecedResultTable->setItem(insertRowNumber, 0, titleItem);
*/
        //**********************Device Type***************************************/
        tcomBox = new QComboBox(this);
        tcomBox->addItems(nameLocalStrList);
        tcomBox->setCurrentIndex(nameLocalStrList.indexOf(newAddedDeviceName));
        tcomBox->setEnabled(false);
        detecedResultTable->setCellWidget(insertRowNumber, 0, tcomBox);

        //**********************Device Count***************************************/
        QTableWidgetItem* totalCountItem = new QTableWidgetItem();
    //    totalCountItem = new QTableWidgetItem();
        totalCountItem->setText(QString::number(newAddedDeviceNumber));
        totalCountItem->setFlags(Qt::ItemIsEditable);
        totalCountItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
        detecedResultTable->setItem(insertRowNumber, 1, totalCountItem);


        //**********************Device Erease***************************************/
/*        tpushButton = new QPushButton(this);
        tpushButton->setText(tr("Delete"));
        detecedResultTable->setCellWidget(insertRowNumber, 3, tpushButton);
        connect(tpushButton, static_cast<void (QPushButton::*)(bool)>(&QPushButton::clicked),this, [=]( bool isDel ) {
            auto curViewCustomizedDevices = utils->getCustomizedResult().find(curViewName).value();
            QMapIterator<QString,int>i(curViewCustomizedDevices);
            while(i.hasNext()) {
               auto item =  i.next();
               auto key  = item.key();
               if (key.contains(newAddedDeviceName,Qt::CaseInsensitive)) {
    //               curViewCustomizedDevices.remove(key);
                   curViewCustomizedDevices.take(key);
                   break;
               }
            }

            utils->setCustomizedResult(curViewName,curViewCustomizedDevices);
            loadCustomizedDevices();
        });*/

    }
    //resize width
    detecedResultTable->resizeColumnsToContents();
    detecedResultTable->resizeRowsToContents();
//    tableWidget->deleteLater();
}

void MainWindow::updateSideNavigate(){
    // qDebug() << "Start  updateSideNavigate()";
    auto indexRow = listSideNavigate->currentRow();
    listSideNavigate->setEditTriggers(QAbstractItemView::NoEditTriggers);
    if(nullptr != viewObjWithNavi && nullptr != listSideNavigate){
        // qDebug() << "Start  listSideNavigate->clear()";
        disconnect(listSideNavigate,SIGNAL(currentTextChanged(QString)),this,
                SLOT(on_subViewSelected_triggered(QString)));
        disconnect(listSideNavigate,SIGNAL(itemChanged(QListWidgetItem*)),this,
                SLOT(on_floorNameEdit_triggered(QListWidgetItem*)));
        listSideNavigate->clear();
        // qDebug() << "End  listSideNavigate->clear()";

        // qDebug() << "Start  this->listSideNavigate->addItem";
        auto subViews = utils->getSubViews();
        std::sort(subViews.begin(), subViews.end());
        foreach(auto subView,subViews){
            bool isViewDetected = utils->isViewDetected(subView);
            auto item = new QListWidgetItem(QIcon(isViewDetected
                                                  ? ":/icons/resource/image/cadCoolPoint.png"
                                                  : ":/icons/resource/image/cadHotPoint.png"),subView);
            if(subView.compare("OriginalView", Qt::CaseInsensitive)){
                item->setFlags(item->flags() | Qt::ItemIsEditable);
                item->setToolTip(tr("Click to display(double click to edit) it."));
            }
            this->listSideNavigate->addItem(item);
        }
        subViews.clear();
        // qDebug() << "End  this->listSideNavigate->addItem";

        connect(listSideNavigate,SIGNAL(currentTextChanged(QString)),this,
                SLOT(on_subViewSelected_triggered(QString)));
        connect(listSideNavigate,SIGNAL(itemChanged(QListWidgetItem*)),this,
                SLOT(on_floorNameEdit_triggered(QListWidgetItem*)));
//        auto listCount = listSideNavigate->count();
//        auto mainViewIndex = (0 <= indexRow && indexRow <= listCount - 1) ? indexRow : listCount - 1;
//        listSideNavigate->item(mainViewIndex)->setSelected(true);
//        listSideNavigate->setCurrentRow(mainViewIndex);
    }
    loadDetectedResult();
    // qDebug() << "End  updateSideNavigate()";
}


QWidget* MainWindow::createSideNavigate(){
    // qDebug() << "Start  createSideNavigate()";
    QWidget* result= new QWidget;

    leftGroup = new QGroupBox(this);
    rightGroup = new QGroupBox(this);
    mainSplitter = new QSplitter(Qt::Horizontal);
//    mainSplitter->resize(600,300);//如果把分割器放置在layout中，那么就无需设置size
    QString style = QString("QSplitter::handle { background-color: rgb(179, 179, 179); }") //分割线的颜色
                        + QString("QSplitter {border: 1px solid grey}");
    mainSplitter->setStyleSheet(style);
    mainSplitter->setHandleWidth(2);//分割线的宽度
    mainSplitter->setChildrenCollapsible(false);//不允许把分割出的子窗口拖小到0，最小值被限定为sizeHint或maxSize/minSize

    this->leftTabNavigate = new QTabWidget(this);
    leftTabNavigate->clear();
    leftTabNavigate->setTabPosition(QTabWidget::North);//
    leftTabNavigate->setTabShape(QTabWidget::Triangular);//设置选项卡的形状
    leftTabNavigate->setMovable(true);
    leftTabNavigate->setMinimumWidth(centralViewObj->width()/6);


    listSideNavigate = new QListWidget(this);
    //List 初始化动作；
    action_DetectDrawing_In_ListWidget_ = new QAction(QIcon(":/icons/resource/image/png/truth.png"),tr("Detect Drawing"), this);
    //子菜单
    QMenu *childMenu = new QMenu();
    //子菜单的 子项
    QAction *detectIES = new QAction(QIcon(":/icons/resource/image/png/system.png"),tr("IES SYSTEM"),childMenu);
    QAction *detectDandA = new QAction(QIcon(":/icons/resource/image/png/system.png"),tr("DA System"), childMenu);
    QList<QAction *> childActionList;
    childActionList<<detectIES\
                  <<detectDandA;
    childMenu->addActions(childActionList);
    action_DetectDrawing_In_ListWidget_->setMenu(childMenu);//设置子菜单 归属
    action_ShowDetectResult_In_ListWidget_ = new QAction(QIcon(":/icons/resource/image/png/Show.png"),tr("Show Detect Result..."), this);
    action_ShowHidenDevice_In_ListWidget_ = new QAction(tr("Hide Recognized Device"), this);
    action_uploadResult_In_ListWidget_ = new QAction(QIcon(":/icons/resource/image/png/upload.png"),tr("Upload Detect Result"), this);
    action_Rename_In_ListWidget_ = new QAction(QIcon(":/icons/resource/image/png/edit.png"),tr("Rename"), this);
    action_Delete_In_ListWidget_ = new QAction(QIcon(":/icons/resource/image/png/delete.png"),tr("Delete view"), this);


    //List 初始化菜单；
    popMenu_In_ListWidget_ = new QMenu(listSideNavigate);
//    popMenu_In_ListWidget_->setStyleSheet("QMenu{background-color:rgb(255,255,255);color:rgb(0, 0, 0);font:10pt ""宋体"";}"
//                                          "QMenu::item:selected{background--color:rgb(51,51,51);color:rgb(0, 0, 0);font:10pt ""宋体"";}"
//                                          "QMenu::item:hover{background-color:#409CE1;}"
//                                          "QMenu::item{padding:11px 32px;color:rgba(51,51,51,1);font-size:12px;}");

    //List 给动作设置信号槽；
    connect(detectIES, &QAction::triggered, [=]()
    {
        on_actionIES_Device_Block_triggered();
        popMenuEnabled = false;
    });
    connect(detectDandA, &QAction::triggered, [=]()
    {
        on_actionD_A_Device_Block_triggered();
        popMenuEnabled = false;
    });

    connect(action_ShowDetectResult_In_ListWidget_, &QAction::triggered, [=]()
    {
//        auto savedRecognizedDevices = utils->getRecognizedDevices();
//        auto currentFloor = utils->getCurrentViewName();
//        if(savedRecognizedDevices.find(currentFloor) != savedRecognizedDevices.end()){
//            DeviceRecordForm dceviceRecordFormDia(this, utils);
//            dceviceRecordFormDia.setModal(false);
//            dceviceRecordFormDia.show();
//            dceviceRecordFormDia.exec();
//        }else{
//            QMessageBox::information(this, tr("Warning"), tr("No view data has been saved."));
//        }
        on_actionCheck_Single_Result_triggered();
        popMenuEnabled = false;
    });
    connect(action_ShowHidenDevice_In_ListWidget_, &QAction::triggered, [=]()
    {
        bool hasRecognizedDevice = utils->getRecognizedDevices().value(utils->getCurrentViewName()).size() > 0;
        if(hasRecognizedDevice){
            auto curDoc = utils->getRSDocument();
            bool isRecognizedDeviceHiden = curDoc->getRecognizedDeviceHiden();
            curDoc->setRecognizedDeviceHiden(!isRecognizedDeviceHiden);
            //Hide recognized devices
            hideRecognizedDeviceFromView();;
        }
    });
    connect(action_uploadResult_In_ListWidget_, &QAction::triggered, [=]()
    {
        on_actionUpload_Single_Result_triggered();
    });
    connect(action_Rename_In_ListWidget_, &QAction::triggered, [=]()
    {
//        int row = listSideNavigate->currentIndex().row();
//        QString text = listSideNavigate->currentItem()->text();
//        QString selectedInfo = QString("Line index is %1, line content is %2").arg(row).arg(text);
//        // qDebug() <<selectedInfo;
//        // qDebug() <<"Add Item is clicked";
//        item->setFlags(Qt::ItemIsEnabled|Qt::ItemIsEditable);
//        listSideNavigate->setEditTriggers(QListWidget::AnyKeyPressed);
//        listSideNavigate->editItem(listSideNavigate->currentItem());
        NewViewDialog newViewBuildingDialog(this, utils, true);
        if(newViewBuildingDialog.exec() == QDialog::Accepted){
            if(viewObjWithNavi != nullptr){
                updateSideNavigate();
//                on_subViewSelected_triggered(utils->getCurrentViewName());
            }
        }

        popMenuEnabled = false;
    });
    connect( action_Delete_In_ListWidget_, &QAction::triggered, [=]()
    {
//        int row = listSideNavigate->currentIndex().row();
//        QString text = listSideNavigate->currentItem()->text();
//        QString selectedInfo = QString("Line index is %1, line content is %2").arg(row).arg(text);
//        // qDebug() <<selectedInfo;
//        // qDebug() <<"Del Item is clicked";
        on_actionDelete_View_triggered();
        popMenuEnabled = false;
    });

    listSideNavigate->setContextMenuPolicy(Qt::CustomContextMenu);
    //List 动作添加到菜单；

    popMenu_In_ListWidget_->addAction(action_DetectDrawing_In_ListWidget_);
    popMenu_In_ListWidget_->addAction(action_ShowDetectResult_In_ListWidget_);
    connect(listSideNavigate,&QListWidget::customContextMenuRequested,[=](const QPoint &pos)
    {
        if(!popMenuEnabled){
            popMenuEnabled = !popMenuEnabled;
            return ;
        }
        // qDebug() <<pos;
//        auto itm = listSideNavigate->itemAt(mapFromGlobal(QCursor::pos()));
        auto itm = listSideNavigate->itemAt(pos);
        if(NULL != itm) //如果有item则*
        {
            if(itm->text().contains("OriginalView",Qt::CaseInsensitive)){
                popMenu_In_ListWidget_->removeAction(action_Rename_In_ListWidget_);
                popMenu_In_ListWidget_->removeAction(action_Delete_In_ListWidget_);
                popMenu_In_ListWidget_->removeAction(action_ShowHidenDevice_In_ListWidget_);

            }else{
                popMenu_In_ListWidget_->removeAction(action_ShowHidenDevice_In_ListWidget_);
                bool isRecognizedDeviceHiden = utils->getRSDocument()->getRecognizedDeviceHiden();
                if(isRecognizedDeviceHiden){
                    action_ShowHidenDevice_In_ListWidget_->setIcon(QIcon(":/icons/resource/image/png/check.png"));
                }else{
                    action_ShowHidenDevice_In_ListWidget_->setIcon(QIcon(""));
                }
                popMenu_In_ListWidget_->addAction(action_ShowHidenDevice_In_ListWidget_);
                popMenu_In_ListWidget_->addAction(action_Rename_In_ListWidget_);
                popMenu_In_ListWidget_->addAction(action_Delete_In_ListWidget_);
            }
//            popMenu_In_ListWidget_->popup(mapFromGlobal(pos));
            popMenu_In_ListWidget_->exec(QCursor::pos());
//            popMenu_In_ListWidget_->exec(pos);

        }
    });


    this->leftTabNavigate->addTab(listSideNavigate,tr("View"));//在后面添加选项卡
    loadDetectedResult();
    this->leftTabNavigate->addTab(detecedResultTable,tr("Device"));//在后面添加选项卡
//    listSideNavigate->setContextMenuPolicy(Qt::CustomContextMenu);
//    listSideNavigate->setProperty("contextMenuPolicy", Qt::CustomContextMenu);
    centralViewObj = utils->getCentralWidget();


    auto vBoxLeft = new QVBoxLayout;
    vBoxLeft->addWidget(leftTabNavigate);
    vBoxLeft->setMargin(0);
    leftGroup->setLayout(vBoxLeft);

    auto vBoxRight = new QVBoxLayout;
    vBoxRight->addWidget(centralViewObj);
    vBoxRight->setMargin(0);
    rightGroup->setLayout(vBoxRight);

    mainSplitter->addWidget(leftGroup);//把ui中拖出的各个控件拿走，放到分割器里面
    mainSplitter->addWidget(rightGroup);
//    mainSplitter->addWidget(new QLabel("QLabel"));//也可以在分割器里面新建控件

    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->addWidget(mainSplitter);
    hbox->setMargin(0);
    result->setLayout(hbox);

//    connect(listSideNavigate,SIGNAL(currentTextChanged(QString)),this,
//            SLOT(on_subViewSelected_triggered(QString)));
//    connect(listSideNavigate,SIGNAL(itemChanged(QListWidgetItem*)),this,
//            SLOT(on_floorNameEdit_triggered(QListWidgetItem*)));
    // qDebug() << "End  createSideNavigate()";
    return result;
}


void MainWindow::hideRecognizedDeviceFromView()
{
    bool isHiden = utils->getRSDocument()->getRecognizedDeviceHiden();
    auto curViewName = utils->getCurrentViewName();
    auto nameToEntityHash = utils->getBlockNameToEntityHash();
    auto allRecognizedDevies = utils->getRecognizedDevices();
    QList<CadDeviceDataDetect*> recognizedDeviceList = allRecognizedDevies.value(curViewName);
    int resultCount = recognizedDeviceList.size();
    QList<FAS_Block*> blockList;
    if(isHiden){
        for(int i = 0; i < resultCount; i++)
        {
           FAS_Block* myBlock = recognizedDeviceList.at(i)->getBlock();
           DeviceInfo* tmpType = myBlock->getType();
           tmpType->setValidRecognition(true);

           QList<FAS_Insert*> devicesOfName = nameToEntityHash.values(myBlock->getName());
           FAS_Insert* oneDevice = devicesOfName.first();
           oneDevice->setSelected(true);
           tmpType->setShowSVG(false);
           FASUtils::setBlcokType(myBlock,tmpType);
           blockList.append(myBlock);
        }
    }else{
        auto inserts = nameToEntityHash.values();
        foreach(auto insert, inserts){
            insert->setVisible(true);
        }
    }
    FASUtils::setAllDeviceEntityType(blockList, nameToEntityHash);
    utils->refreshGraphicView();
}

void MainWindow::on_actionSplit_Show_triggered()
{
    // qDebug() << "Start  on_actionSplit_Show_triggered()";
    uiMainWindow->actionDelete_View->setEnabled(true);
    /*** update central area ***/
    if(viewObjWithNavi == nullptr){
        viewObjWithNavi = createSideNavigate();
    }
    updateSideNavigate();

    setCentralWidget(viewObjWithNavi);
//    on_subViewSelected_triggered(listSideNavigate->item(0)->text());
    // qDebug() << "End  on_actionSplit_Show_triggered()";
}

void MainWindow::on_actionDelete_View_triggered()
{
    if(this->listSideNavigate->currentItem() != Q_NULLPTR)
    {
        auto isOK = QMessageBox::information(this, tr("Infomation"), tr("The view will be deleted."),QMessageBox::Yes|QMessageBox::No);
        if(QMessageBox::No == isOK){
            return;
        }
        auto curViewName = this->listSideNavigate->currentItem()->text()/*item->text()*/;
        if(!curViewName.contains("OriginalView")){
            utils->deleteCurrentView(curViewName);
            QListWidgetItem * item = this->listSideNavigate->takeItem(this->listSideNavigate->currentRow());
            delete item;
            updateSideNavigate();
        }else{
            QMessageBox::information(this, tr("Warning"), tr("The original view can not be deleted."));
        }
    }
    else{
        QMessageBox::information(this, tr("Warning"), tr("A view should be selected."));
    }
}

void MainWindow::on_subViewSelected_triggered(QString subViewName){

    if(!subViewName.isEmpty() && !subViewName.isNull()){

        if(subViewName.contains(utils->getCurrentViewName())){
            if(utils != nullptr)
            {
                utils->zoomAuto();
                this->leftTabNavigate->setTabText(1,subViewName);
            }
            return;
        }
        mdiWindow->close();
        mdiWindow = new QC_MDIWindow(this);
        utils->createSubGraphicView(mdiWindow,subViewName);
        utils->getRSDocument()->setRecognizedDeviceHiden(false);
        hideRecognizedDeviceFromView();
        loadDetectedResult();
    }

//    utils->buildCodeToDeviceMap();
//    utils->buidMatrixToEntitiesMap();
//    utils->buidMatrixToBlocksMapping();
}

void MainWindow::on_floorNameEdit_triggered(QListWidgetItem *item){
    auto previousText = utils->getCurrentViewName();
    // qDebug() << "Old view name: "<<previousText;
    auto curText = item->text().trimmed();
    // qDebug() << "New view name: "<<curText;

    if(curText.isNull() || curText.isEmpty()){
        item->setText(previousText);
    }else{
        auto ok = utils->updateCurrentViewName(previousText,curText);
        if(ok){
            on_subViewSelected_triggered(curText);
        }else{
            QMessageBox::information(this, tr("Result"), tr("Duplicate! Check it and try again."));
            item->setText(previousText);
        }
    }
//    item->setFlags(item->flags() & ~Qt::ItemIsEnabled & ~Qt::ItemIsSelectable);
}

////TODO 后期实现 鼠标控制导航栏尺寸
//void MainWindow::receiverMouseControlSizeSIGNAL(){

//}

void MainWindow::on_actionUpload_Single_Result_triggered()
{
    auto finalResutlMap = utils->getRecognitionResult();
    auto curViewName = utils->getCurrentViewName();
    if(!COMMONDEF->floorValidation(curViewName)){
        QMessageBox::information(this, tr("Information"), tr("No floor code."));
    }
    if(this->listSideNavigate->currentItem()!=Q_NULLPTR)
    {
//        auto isOK = QMessageBox::information(this, tr("Infomation"), tr("The view will be deleted."),QMessageBox::Yes|QMessageBox::No);
//        if(QMessageBox::No == isOK){
//            return;
//        }
//        QListWidgetItem * item = this->listSideNavigate->takeItem(this->listSideNavigate->currentRow());
//        auto curViewNameSelected = item->text();
    }
    else{
        QMessageBox::information(this, tr("Warning"), tr("A view should be selected."));
        return;
    }
    QMap<QString,int> curRecognitionResult = finalResutlMap.value(curViewName);
    if(curRecognitionResult.size() <= 0){
        QMessageBox::information(this, tr("Warning"), tr("No valid result data in the view."));
        return;
    }
    QMap<QString,QMap<QString,int>> finalSingleResultMap;

    finalSingleResultMap.clear();
    QString responseCode;
    QString popUpError;
    QString messageContent;
    if(!curViewName.contains("图例层")){
        finalSingleResultMap.insert(curViewName,curRecognitionResult);

//    utils->setRecognitionResult(curViewName,curRecognitionResult);

        GrabThread *httpRequest = new GrabThread();
        QTime startTime = QTime::currentTime();
        // qDebug() << "Start uploading single floor recognition result ..." <<startTime;
        httpRequest->syncResultPostQuery(finalSingleResultMap,"onefloor");
        responseCode = httpRequest->getResultSyncResponseCode();
        QTime endTimeE = QTime::currentTime();
        // qDebug() << "Uploaded with code:"<<responseCode<<endTimeE;
        popUpError = httpRequest->getHttpErrorMessage();
        delete httpRequest;
    }
    messageContent = QString::fromUtf8("Floors[%1] action result. \nWith code:%2")
            .arg(curViewName)
            .arg(responseCode);
    if(responseCode.isEmpty()){
        messageContent = QString(tr("Network is invalid."));
    }
    if(!popUpError.isNull() && !popUpError.isEmpty()){
        messageContent = QString::fromUtf8("%1\r\n%2:%3").arg(messageContent).arg(tr("Exception")).arg(popUpError);
    }

    QMessageBox *messageBox = new QMessageBox(tr("Result"),
                                             messageContent,
                                             QMessageBox::Information,
                                             QMessageBox::Ok | QMessageBox::Default,
                                             QMessageBox::Cancel | QMessageBox::Escape,
                                             0);
    messageBox->setTextInteractionFlags(Qt::TextSelectableByMouse);
    messageBox->exec();
}

void MainWindow::on_actionUpload_All_Result_triggered()
{
    auto finalResutlMap = utils->getRecognitionResult();
     if(finalResutlMap.size()>0){
        GrabThread *httpRequest = new GrabThread();
        QTime startTime = QTime::currentTime();
        // qDebug() << "Start uploading recognition result ..." <<startTime;
//        //set request json data
//        // TODO:使用Json
        QString feedbackMessageOfFloorKeys;
        QString noFloorCode;
//        QVariantList requestData;
         foreach(auto floorKey, finalResutlMap.keys()){
            feedbackMessageOfFloorKeys = QString::fromUtf8("%1%2%3")
                    .arg(floorKey)
                    .arg(!feedbackMessageOfFloorKeys.isNull()&&!feedbackMessageOfFloorKeys.isEmpty()?",":"")
                    .arg(feedbackMessageOfFloorKeys);
            if(!COMMONDEF->floorValidation(floorKey)){
                noFloorCode = QString::fromUtf8("%1\r\n%2")
                        .arg(noFloorCode)
                        .arg(floorKey);
            }
         }
         if(!noFloorCode.isNull() && !noFloorCode.isEmpty()){
             QMessageBox::information(this, tr("Information"), tr("No floor code:\r\n") + noFloorCode);
         }
//            QVariantMap floor;
//            floor.clear();
//            floor.insert("floor", floorKey);
//            QVariantList productList;
//            productList.clear();
//            auto products = finalResutlMap.value(floorKey);
//            foreach(auto productKey, products.keys()){

//                QVariantMap product;
//                product.clear();
//                auto productCount = products.value(productKey);
//                auto firstIndex = productKey.lastIndexOf ("-");
//                auto clazz = productKey.left(firstIndex);
//                auto label = productKey.split("-").last();
//                product.insert("clazz",clazz);
//                    QVariantList productKindList;
//                    productKindList.clear();
//                    QVariantMap productKindMap;
//                    productKindMap.clear();
//                        productKindMap.insert("count",productCount);
//                        productKindMap.insert("label",label);
//                    productKindList<<productKindMap;
//                product.insert("kinds",productKindList);

//                productList<<product;
//            }
//            floor.insert("products",productList);
//            requestData<<floor;
//        }
    //    requestData.insert("imgURLs",QFloorList);
        httpRequest->syncResultPostQuery(finalResutlMap);
        auto responseCode = httpRequest->getResultSyncResponseCode();
        QTime startTimeE = QTime::currentTime();
        // qDebug() << "Uploaded with code:"<<responseCode<<startTimeE;

    //    qsrand(QTime(0, 0, 0).secsTo(QTime::currentTime()));
    //    auto randomCode = qrand() % (9999 - 1000) + 1000;

        auto messageContent = QString::fromUtf8("Floors[%1] action result. \nWith code:%2").arg(feedbackMessageOfFloorKeys).arg(responseCode);
        if(responseCode.isEmpty()){
            messageContent = QString::fromUtf8("Network is invalid.");
        }
        auto popUpError = httpRequest->getHttpErrorMessage();
        if(!popUpError.isNull() && !popUpError.isEmpty()){
            messageContent = QString::fromUtf8("%1\r\nException:%2").arg(messageContent).arg(popUpError);
        }

        QMessageBox *messageBox = new QMessageBox("Result",
                                                 messageContent,
                                                 QMessageBox::Information,
                                                 QMessageBox::Ok | QMessageBox::Default,
                                                 QMessageBox::Cancel | QMessageBox::Escape,
                                                 0);
        messageBox->setTextInteractionFlags(Qt::TextSelectableByMouse);
        messageBox->exec();
        httpRequest->deleteLater();
    }
    else{
        QMessageBox::information(this, tr("Result"), tr("Failed:No result."));
    }
//    QMessageBox::information(this, tr("Result"), tr(messageContent.toLatin1()));

//    QString programAdress = QString::fromUtf8("%1").arg("C:/Program Files (x86)/Google/Chrome/Application/chrome.exe");
//    QString m_strWebUrl = "http://www.baidu.com";
//    QStringList arguments;
//    arguments << "--chrome-frame" << m_strWebUrl;
//    QProcess *chromeProcess = new QProcess();
//    chromeProcess ->start(programAdress ,arguments );
}



void MainWindow::onRestart(){
    qApp->exit(666666);
}

/**
  * @author Sonny
  * @date 2020-12-02
  * 查询程序占用内存。
  * 思路：通过调用外部命令'tasklist /FI "PID EQ pid"'。
  * 将返回的字符串首先替换掉','，
  * 然后用正则表达式匹配以KB为单位表示内存的字符串，
  * 最后换算为MB为单位返回。
  * 添加头文件#include <windows.h>
  * 使用的时候，getUsedMemory(GetCurrentProcessId())；
  * GetCurrentProcessId()是windows的api。
  */
double MainWindow::getUsedMemory()
{
    char pidChar[25];
    DWORD pid = GetCurrentProcessId();
    //将DWORD类型转换为10进制的char*类型
    _ultoa(pid,pidChar,10);

    //调用外部命令
    QProcess p;
    p.start("tasklist /FI \"PID EQ " + QString(pidChar) + " \"");
    p.waitForFinished();
    //得到返回结果
    QString result = QString::fromLocal8Bit(p.readAllStandardOutput());
    //关闭外部命令
    p.close();

    //替换掉","
    result = result.replace(",","");
    //匹配 '数字+空格+K'部分。
    QRegExp rx("(\\d+)(\\s)(K)");
    //初始化结果
    QString usedMem("");
    if(rx.indexIn(result) != -1){
        //匹配成功
        usedMem = rx.cap(0);
    }
    //截取K前面的字符串，转换为数字，供换算单位使用。
    usedMem = usedMem.left(usedMem.length() - 1);
    //换算为MB的单位
//    auto resultMid = QString::number(usedMem.toDouble()/1024,'g',4) + "M";
//    QString::number(usedMem.toDouble() / 1024) + " MB"
    return usedMem.toDouble()/1024;
}

void MainWindow::on_actionAuto_Split_View_triggered()
{
    QSet<QString> newViewSet;
    if(nullptr != utils){
        auto existingViews = utils->getSubViews();
        utils->buildSubViews();
        auto curViews = utils->getSubViews();
        newViewSet = curViews.toSet().subtract(existingViews.toSet());//.intersect(curViews.toSet());
    }
    auto newViewList = newViewSet.values();
    if(!newViewList.isEmpty() && newViewList.size()>0){
        std::sort(newViewList.begin(),newViewList.end());
//        updateSideNavigate();
        auto messages = QString::fromUtf8("%1 %2: ")
                .arg(tr("Created new"))
                .arg(newViewList.size()>1 ? tr("views") : tr("view"));
        QString newViews;
        foreach(auto m, newViewList){
            newViews = QString::fromUtf8("%1%2%3")
                    .arg(newViews)
                    .arg(newViews.isEmpty() || newViews.isNull() ? "" : ",\n\r")
                    .arg(m);
        }
        messages = QString::fromUtf8("%1 \n\r%2")
                .arg(messages)
                .arg(newViews);
        QMessageBox::information(this,tr("information"),messages.toUtf8());
//        on_actionSplit_Show_triggered();
        emit sendSplit_ShowSingal();
    }else{
        QMessageBox::information(this,tr("information"),tr("No new view is automatically created."));
    }
}

void MainWindow::openChangeHistoryLog(QString logName){
    QFileInfo fileInfo(COMMONDEF->getChangeHistoryLogPath());
    QString absoluteFilePath = fileInfo.absoluteFilePath();
    if(COMMONDEF->isDirExist(absoluteFilePath)){
        auto sourceDir = QString::fromUtf8("%1/%2").arg(absoluteFilePath).arg(logName);
        if(QFile::exists(sourceDir)){
            auto filePath = QString::fromUtf8("./%1").arg(logName);
            if(!QFile::exists(filePath)){
                QFile::copy(sourceDir, filePath);
            }
            QString path = QString::fromUtf8("notepad.exe %1")
                    .arg(filePath);
            QProcess *proc = new QProcess();
            proc->start(path);
        }
    }
}


void MainWindow::on_actionCheck_Single_Result_triggered()
{
    auto savedRecognizedDevices = utils->getRecognizedDevices();
    auto currentFloor = utils->getCurrentViewName();
    if(savedRecognizedDevices.find(currentFloor) != savedRecognizedDevices.end()){
        DeviceRecordForm dceviceRecordFormDia(this, utils);
        dceviceRecordFormDia.setModal(false);
        dceviceRecordFormDia.show();
        dceviceRecordFormDia.exec();
    }else{
        QMessageBox::information(this, tr("Warning"), tr("No view data has been saved."));
    }
}

void MainWindow::on_actionCheck_All_Result_triggered()
{
    auto savedRecognizedDevices = utils->getRecognizedDevices();
    if(savedRecognizedDevices.size() > 0){
        DeviceRecordForm dceviceRecordFormDia(this, utils, true);
        dceviceRecordFormDia.setModal(false);
        dceviceRecordFormDia.show();
        dceviceRecordFormDia.exec();
    }else{
        QMessageBox::information(this, tr("Warning"), tr("No view data has been saved."));
    }
}

void MainWindow::on_action0_0_9_triggered()
{
    openChangeHistoryLog("0.0.9.txt");
}

void MainWindow::on_action0_0_10_triggered()
{
    openChangeHistoryLog("0.0.10.txt");
}

void MainWindow::on_action0_0_11_triggered()
{
    openChangeHistoryLog("0.0.11.txt");
}

void MainWindow::on_action0_0_12_triggered()
{
    openChangeHistoryLog("0.0.12.txt");
}

void MainWindow::on_action0_0_13_triggered()
{
    openChangeHistoryLog("0.0.13.txt");
}

void MainWindow::on_action0_0_14_triggered()
{
    openChangeHistoryLog("0.0.14.txt");
}

void MainWindow::on_action0_0_15_triggered()
{
    openChangeHistoryLog("0.0.15.txt");
}


void MainWindow::on_action0_0_16_triggered()
{
    openChangeHistoryLog("0.0.16.txt");
}
