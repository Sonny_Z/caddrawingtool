#include "waitdrawprogressdialog.h"
#include <QTimer>
WaitDrawProgressDialog::WaitDrawProgressDialog(QWidget *parent):
        MyProgressDialog(parent)
{

    this->setWindowFlag(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setWindowOpacity(0.8);

    timer=new QTimer(this);
    timer->setSingleShot(true);
    timer->start(50);
    timer1=new QTimer(this);

    connect(timer,SIGNAL(timeout()),this,SLOT(slotTimeOut()));
    connect(mthreaddraw,SIGNAL(signalDrawFinished()),this,SLOT(soltDrawFinished()));
    connect(timer1,SIGNAL(timeout()),this,SLOT(ontimer1()));
    this->setStyle("green");
}

WaitDrawProgressDialog::~WaitDrawProgressDialog()
{

}
void WaitDrawProgressDialog::slotTimeOut()
{
    timer->stop();
    COMMONDEF->drawFinished=false;
    COMMONDEF->drawProgressValue=0;
    mthreaddraw->updateElements(view);
    timer1->start(50);

}

void WaitDrawProgressDialog::setParas(QG_GraphicView* view)
{
    this->view=view;
}

void WaitDrawProgressDialog::ontimer1()
{
    int value=COMMONDEF->drawProgressValue;
    setValue(value);
}
void WaitDrawProgressDialog::soltDrawFinished()
{
    COMMONDEF->drawFinished=true;
    //view->redraw();
    timer1->stop();
    this->setValue(100);
    this->accept();
}


