#include "autocodedialog.h"
#include "ui_autocodedialog.h"

//sort with alphabetical order
#include <QMessageBox>

AutoCodeDialog::AutoCodeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AutoCodeDialog)
{
    ui->setupUi(this);
    FASUtils::setFontForUI(this);
    init();
}

AutoCodeDialog::~AutoCodeDialog()
{
    delete ui;
}

void AutoCodeDialog::init()
{
    this->utils = FASUtils::getInstance();

    ui->button_DetectLoop->setDisabled(true);
    ui->button_AutoCode->setDisabled(true);
    QList<QString> layerNames;
    if(utils != nullptr)
    {
        FAS_LayerList* layerList = utils->getRSDocument()->getLayerList();
        for(int ii = 0; ii < layerList->count(); ii++)
        {
            layerNames.append(layerList->at(ii)->getName());
        }
    }
    if(!layerNames.empty())
    {
        ui->cB_DeviceLayer->addItems(layerNames);
        ui->cB_DeviceLayer->setCurrentIndex(0);
        ui->cB_WireLayer->addItems(layerNames);
        ui->cB_WireLayer->setCurrentIndex(0);
        ui->cB_TextLayer->addItems(layerNames);
        if(utils->getCodeLayer() != nullptr)
        {
            int index = layerNames.indexOf(utils->getCodeLayer()->getName());
            ui->cB_TextLayer->setCurrentIndex(index >= 0? index : 0);
        }
        else
        {
            ui->cB_TextLayer->setCurrentIndex(0);
        }
        ui->button_DetectLoop->setEnabled(true);
    }
}

void AutoCodeDialog::on_DetectLoop_Button_clicked()
{
    loopsToEncode.clear();
    QList<QList<FAS_Entity*>> allLoops;
    QString deviceLayerName = ui->cB_DeviceLayer->currentText();
    QString wireLayerName = ui->cB_WireLayer->currentText();
    bool singleDeviceInLoop = false;
    if(utils != nullptr)
    {
        QMultiHash<QString, FAS_Entity*> hashMap; //layer name to entity map;
        QHash<int, FAS_Entity*> IDMap;            //entityID to entity map;
        utils->clarifyLayerEntities(hashMap, IDMap);
        QList<FAS_Entity*> wireEntities = utils->getEntityListOnLayer(wireLayerName, hashMap);
        QList<FAS_Entity*> deviceEntities = utils->getEntityListOnLayer(deviceLayerName, hashMap);
        allLoops = utils->detectLoopList(wireEntities, deviceEntities, IDMap);
        for(QList<FAS_Entity*> oneLoop : allLoops)
        {
            QList<FAS_Insert*> devicesInLoop = utils->getDeviceListInLoop(oneLoop);
            if(devicesInLoop.count() > 1)
                loopsToEncode.append(oneLoop);
            else
                singleDeviceInLoop = true;
        }
    }
    QString result = tr("Find") + QString::number(loopsToEncode.size()) + tr("loops!") + "\n";
    if(singleDeviceInLoop)
    {
        QString temp = "\n" + QString::number(allLoops.count() - loopsToEncode.count()) + tr(" devices are disconnected to loops") + "\n" + "\n";
        result += temp;
    }

    FAS_GraphicView* view= utils->getGraphicView();
    for(int ii = 0; ii < loopsToEncode.size(); ii++)
    {
        int numDevices = 0, numWires = 0;
        for(FAS_Entity* entity : loopsToEncode.at(ii))
        {
            QString layerName = entity->getLayer()->getName();
            if(layerName == deviceLayerName)
                numDevices++;
            else if(layerName == wireLayerName)
                numWires++;
            entity->setSelected(true);
            view->deleteEntity(entity);
            view->drawEntity(entity);
        }
        QString temp = "Loop " + QString::number(ii+1) + tr(" has ") + QString::number(numDevices) + tr(" devices and ") + QString::number(numWires) + tr(" wires.");
        if(numDevices > utils->getMaxNumberInLoop())
        {
            temp += tr(" Over maximum limit!") + "\n";
        }
        else
        {
            temp += "\n";
        }
        result = result + temp;
    }
    QMessageBox::information(this, tr("Information"), result);
    if(!loopsToEncode.empty())
    {
        ui->button_AutoCode->setEnabled(true);
    }
}

void AutoCodeDialog::on_AutoCode_Button_clicked()
{
    if(utils != nullptr)
    {
        if(utils->getCodeLayer() == nullptr)
        {
            QMessageBox::information(this, tr("Warning"), tr("Please set layer for code text in 'Tool->Set Layer For Device Code'."));
            return;
        }

        QString textLayerName = ui->cB_TextLayer->currentText();
        FAS_Layer* textLayer = ((FAS_Graphic*)utils->getRSDocument())->findLayer(textLayerName);
        QHash<QString, FAS_Entity*> codeHash = utils->generateCodeForLoopDeviceList(loopsToEncode);
        utils->addCodeTextInGraphic(codeHash, textLayer);
        QMessageBox::information(this, tr("Information"), QString::number(codeHash.size()) + tr(" code(s) are added in this document."));
    }
}

