#include "zoomwidget.h"
#include "ui_zoomwidget.h"

ZoomWidget::ZoomWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ZoomWidget)
{
    ui->setupUi(this);
    //以下画窗口圆角
    QPainterPath path;
    QRectF rect = QRectF(0,0,this->width(),this->height());
    path.addRoundedRect(rect,10,10);
    QPolygon polygon= path.toFillPolygon().toPolygon();//获得这个路径上的所有的点
    QRegion region(polygon);//根据这些点构造这个区域
    setMask(region);
}

ZoomWidget::~ZoomWidget()
{
    delete ui;
}
void ZoomWidget::setText(QString text)
{
    ui->label2->setText(text);
}
