#include "drawnotes.h"
#include "ui_drawnotes.h"

//sort with alphabetical order
#include <QMessageBox>

DrawNotes::DrawNotes(QWidget *parent, FASUtils* utils) :
    QDialog(parent),
    utils(utils),
    uiDrawNotes(new Ui::DrawNotes)
{
    uiDrawNotes->setupUi(this);
    FASUtils::setFontForUI(this);

    uiDrawNotes->lineEdit_coordX->setText("0.0");
    uiDrawNotes->lineEdit_coordY->setText("0.0");
}

DrawNotes::~DrawNotes()
{
    delete uiDrawNotes;
}

void DrawNotes::on_btnOk_clicked()
{
    QString inputNotes = uiDrawNotes->pTEdit_note->toPlainText();
    int height = uiDrawNotes->spinBox_height->value();

    double x = uiDrawNotes->lineEdit_coordX->text().toDouble();
    double y = uiDrawNotes->lineEdit_coordY->text().toDouble();
    if(x == 0.0 && y == 0.0)
    {
        QMessageBox::information(this, tr("Warning"), tr("Position is invalid."));
        return;
    }
    if(nullptr == utils)
    {
        QMessageBox::information(this, tr("Warning"), tr("Unexpected error occurs."));
        return;
    }
    utils->drawNotes(inputNotes, height, FAS_Vector(x,y));

    disconnect(utils->getGraphicView(), SIGNAL(signalSnapPointDone(FAS_Vector)), this, SLOT(snapPointDone(FAS_Vector)));

    close();
}

void DrawNotes::on_btnCancel_clicked()
{
    disconnect(utils->getGraphicView(), SIGNAL(signalSnapPointDone(FAS_Vector)), this, SLOT(snapPointDone(FAS_Vector)));
    close();
}

void DrawNotes::on_snapPoint_clicked()
{
    this->hide();
}

void DrawNotes::snapPointDone(FAS_Vector point)
{
    this->show();
    uiDrawNotes->lineEdit_coordX->setText(QString::number(point.x));
    uiDrawNotes->lineEdit_coordY->setText(QString::number(point.y));
}

