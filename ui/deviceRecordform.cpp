#include "deviceRecordform.h"
#include "ui_deviceRecordform.h"


//sort with alphabetical order
#include <QFile>
#include <QFileInfo>
#include <QTextStream>
#include <QDir>
#include <QMessageBox>
#include "commondef.h"
#include "entityqggv.h"
#include "fas_blocklist.h"
#include "fas_block.h"
#include "fas_insert.h"
#include "threadexcelgenerate.h"
#include <QDebug>
#include "popupmessagedialog.h"
#include "grabthread.h"
#include "fas_painterqt.h"
#include "addcustomizeddevicedialog.h"
#include "QToolTip"
#include "CadDeviceDataDetect.h"

static void WriteToText(DeviceFeature feature, DeviceInfo* type)
{
    QFile textFile(COMMONDEF->textName);
    if(textFile.open(QIODevice::Text | QFile::Append))
    {
        QTextStream stream(&textFile);
        stream << feature.circles << ", ";
        stream << feature.lines << ", ";
        stream << feature.arcs << ", ";
        stream << feature.hatchs << ", ";
        stream << feature.solids << ", ";
        stream << QString::number(feature.size, 'g', 6) << ", ";
        stream << feature.text << ", ";
        stream << type->getName() << endl;
        textFile.close();
    }
}

DeviceRecordForm::DeviceRecordForm(QWidget *parent, FASUtils* utils, bool isShowAll) :
    QDialog(parent),
    uiDeviceRecordForm(new Ui::DeviceRecordForm)
{
//    this->setAttribute(Qt::WA_DeleteOnClose);
    uiDeviceRecordForm->setupUi(this);
    FASUtils::setFontForUI(this);
    setLayout(this->uiDeviceRecordForm->verticalLayout_main);
    this->setContentsMargins(6,6,6,6);

    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint &
                                   ~Qt::WindowMinimizeButtonHint &
//                                   ~Qt::WindowCloseButtonHint    &
                                   ~Qt::WindowTitleHint          &
                                   ~Qt::WindowContextHelpButtonHint);
//    Qt::WindowFlags flags=Qt::Dialog;
//    flags |=Qt::WindowMinMaxButtonsHint;
//    flags |=Qt::WindowCloseButtonHint;
//    setWindowFlags(flags);
//    uiDeviceRecordForm->tableWidget_Added->setVisible(false);

    this->utils = utils;
    COMMONDEF->setReadOnly(true);
    auto systemTypeStr = COMMONDEF->getSystem(true);


    auto curViewName = utils->getCurrentViewName();
    auto recognitionTmpBlockList = utils->getRecognizedDevices().value(curViewName);
    foreach(auto deviceResultData, recognitionTmpBlockList){
        auto b = deviceResultData->getBlock();
        auto tmpResultData = new CadDeviceDataDetect(b, deviceResultData->getImageSrc(), deviceResultData->getType(), deviceResultData->getCount());
        recognitionBlockList.append(tmpResultData);
    }
    if(isShowAll){
        this->isShowAll = isShowAll;
        mergeRecognizedResultData();
    }
    init(); //load initial data

    QTableWidget* tableWidget = uiDeviceRecordForm->tableWidget;
    tableWidget->setRowCount(recognitionBlockList.count() + 1);
    tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);//只能单选



    int totalCount = 0;
    int ii = 0;
    int loopCount=recognitionBlockList.count();
    QLabel *l1=nullptr;
    QWidget *widgetChild=nullptr;
    QVBoxLayout *vLayout=nullptr;
    QComboBox* tcomBox=nullptr;
    QTableWidgetItem* countItem=nullptr;
    QTableWidgetItem* uploadStatusItem=nullptr;
    QString uploadStatus = QString(tr("-"));
    for(int ii=0;ii<loopCount;ii++)
    {
//        tableWidget->setRowHeight(ii,100);
//        tableWidget->verticalHeader()->setDefaultSectionSize(100);

        /*
        auto blkSavePath = recognitionBlockList.at(ii)->getImageSrc();
        l1 = new QLabel();     //创建lable

        l1->setAlignment(Qt::AlignHCenter);
        l1->setScaledContents(true);
        l1->setMaximumWidth(92);
        l1->setMaximumHeight(112);//设置居中
        l1->setPixmap(QPixmap(blkSavePath).scaled(l1->size(),Qt::KeepAspectRatio));    //加载图片

        widgetChild = new QWidget;
        vLayout = new QVBoxLayout();
        vLayout->addWidget(l1);
        vLayout->setAlignment(l1,Qt::AlignCenter);
        widgetChild->setLayout(vLayout);
        widgetChild->setStyleSheet("background-color:black;");
        tableWidget->setCellWidget(ii, 0, widgetChild);

        */
        //**********************Device Type***************************************/
        tcomBox = new QComboBox(this);
        tcomBox->addItems(nameLocalStrList);
        auto defaultIndex = 0;
        int indexCheck = typeStrList.indexOf(recognitionBlockList.at(ii)->getType());
        int index = indexCheck >= 2 ? indexCheck : defaultIndex;
        tcomBox->setCurrentIndex(index);
        tcomBox->setEnabled(false);
        tableWidget->setCellWidget(ii, 0, tcomBox);
        comBoxList.append(tcomBox);

        //**********************Device Count***************************************/
        countItem = new QTableWidgetItem();
        countList.append(countItem);
        countItem->setText(QString::number(recognitionBlockList.at(ii)->getCount()));
        countItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(ii, 1, countItem);


        //**********************Upload Status***************************************/
/*        uploadStatusItem = new QTableWidgetItem();
        uploadStatusItem->setText(uploadStatus);
        uploadStatusItem->setTextColor(FAS_Color(0,255,0).toQColor());
//        uploadStatusItem->setBackground(FAS_Color(0,255,0).toQColor());
        uploadStatusItem->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter|Qt::AlignCenter);
        uploadStatusItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(ii, 3, uploadStatusItem);
*/
        if(recognitionBlockList.at(ii)->getRecognizedStatus()){
            totalCount += recognitionBlockList.at(ii)->getCount();
        }

//        deleteLater();
    }

//    qDeleteAll(inserts);
//    inserts.clear();
    // show total count
    QTableWidgetItem* totalItem = new QTableWidgetItem();
    totalItem->setText(tr("Total count"));
    totalItem->setFlags(Qt::ItemIsEditable);
    tableWidget->setItem(loopCount, 0, totalItem);
    //QTableWidgetItem* totalCountItem = new QTableWidgetItem();
    totalCountItem = new QTableWidgetItem();
    totalCountItem->setText(QString::number(totalCount));
    totalCountItem->setFlags(Qt::ItemIsEditable);
    tableWidget->setItem(loopCount, 1, totalCountItem);

    //resize width
    tableWidget->resizeColumnsToContents();
    tableWidget->deleteLater();
    systemTypeStr.clear();

//    deleteLater();
}

DeviceRecordForm::~DeviceRecordForm()
{
//    COMMONDEF->setReadOnly(true);
    typeStrList.clear();
    typeLocalStrList.clear();
    nameStrList.clear();
    nameLocalStrList.clear();
    showBlockList.clear();
    showImageIdNameMap.clear();
    blockNameToImgIdMap.clear();
    qDeleteAll(comBoxList);
    comBoxList.clear();
    nameToEntityHash.clear();
    if(!UIMemoryInserts.isEmpty()){
//        qDeleteAll(UIMemoryInserts);
        UIMemoryInserts.clear();
    }
    qDeleteAll(recognitionBlockList);
    recognitionBlockList.clear();
    utils->clearCacheDeviceImagesAfterUpdate();
    delete uiDeviceRecordForm;
    deleteLater();

}

void DeviceRecordForm::init()
{
//    COMMONDEF->setReadOnly(false);
    showBlockList.clear();
    showImageIdNameMap.clear();
    blockNameToImgIdMap.clear();
    typeStrList = COMMONDEF->getDeviceTypeList();
    nameStrList = COMMONDEF->getDeviceNameList();
    nameLocalStrList = COMMONDEF->getDeviceNameList();
//    typeStrList = COMMONDEF->getDeviceNameCodeDict().values();
    typeLocalStrList = COMMONDEF->getLocalDeviceTypeList();
//    qDebug() << "name to entity hash begin";
    nameToEntityHash = utils->getBlockNameToEntityHash();
//     qDebug() << "name to entity hash end";
//    uiDeviceRecordForm->textEdit_showResult->setDisabled(true);
    {
        QString absoluteFilePath = COMMONDEF->getDxfOfDevicePathStr();
        QFileInfo fileInfo(utils->getORSDocument()->getFilename());
        auto fileName = fileInfo.baseName();
        auto desDir = QString::fromUtf8("%1/%2/%3").arg(absoluteFilePath).arg(fileName).arg(utils->getCurrentViewName());
        utils->clearDir(desDir);
    }
//    this->uiDeviceRecordForm->checkHideRD->setChecked(COMMONDEF->isHideRecognizedDevice());
    loadCustomizedDevices();
}

void DeviceRecordForm::mergeRecognizedResultData(){
        int loopCur = recognitionBlockList.size();
        auto curViewName = utils->getCurrentViewName();
        auto allRecognizedDevies = utils->getRecognizedDevices();
        auto floors = allRecognizedDevies.keys();
        foreach(auto floor, floors){
            if(!floor.compare(curViewName, Qt::CaseInsensitive)/* || !floor.compare("OriginalView", Qt::CaseInsensitive)*/){
                continue;
            }
            auto devices = allRecognizedDevies.value(floor);
            int loopTmp = devices.size();
            for(int i = 0; i < loopTmp; i++){
                bool isNewAdd = true;
                auto tmpDevice = devices.at(i);
                for(int j = 0; j < loopCur; j++){
                    auto curDevice = recognitionBlockList.at(j);
                    if(tmpDevice->getType() == curDevice->getType()){
                        auto mergeCount = curDevice->getCount() + tmpDevice->getCount();
                        curDevice->setCount(mergeCount);
                        isNewAdd = false;
                        break;
                    }
                }
                if(isNewAdd){
                    auto b = tmpDevice->getBlock();
                    auto tmpResultData = new CadDeviceDataDetect(b, tmpDevice->getImageSrc(), tmpDevice->getType(), tmpDevice->getCount());
                    recognitionBlockList.append(tmpResultData);
                }
            }
        }
}



void DeviceRecordForm::on_pBtnCancel_clicked()
{
    close();
}


void DeviceRecordForm::showBranchResult(QHash<QString,QString> branches){
    if(branches.keys().size()>0){
//        int textEditWidth = uiDeviceRecordForm->textEdit_showResult->width();
//        uiDeviceRecordForm->textEdit_showResult->setDisabled(false);
//        int width = uiDeviceRecordForm->layoutWidget->width();
//        uiDeviceRecordForm->layoutWidget->resize(width - textEditWidth,uiDeviceRecordForm->layoutWidget->height());
        QString showContent = "";
        for(int i = 1; i<=branches.keys().size();i++){
            auto key  = branches.keys().at(i-1);
            auto values = branches.values(branches.keys().at(i-1));
            QString value = "";
            if(values.size()>1){
                foreach(auto v, values){
                    value = QString::fromUtf8("%1&%2")
                            .arg(value).arg(v);
                }
            }else{
                value = branches.value(branches.keys().at(i-1));
            }
            showContent = QString::fromUtf8("%1%2: %3-->%4;\r\n")
                    .arg(showContent)
                    .arg(i)
                    .arg(key)
                    .arg(value);
        }
//        uiDeviceRecordForm->textEdit_showResult->setText(showContent);
    }
//    else{
//        int textEditWidth = uiDeviceRecordForm->textEdit_showResult->width();
//        uiDeviceRecordForm->textEdit_showResult->hide();
//        int width = uiDeviceRecordForm->layoutWidget->width();
//        uiDeviceRecordForm->layoutWidget->resize(width - textEditWidth,uiDeviceRecordForm->layoutWidget->height());
//    }
}

void DeviceRecordForm::on_pBtnOk_clicked()
{
    utils->refreshGraphicView();
    this->close();
}


void DeviceRecordForm::on_pBtnShowBranch_clicked()
{
//    utils->buidMatrixToEntitiesMap();
//    qDebug()<<"1.utils->buidMatrixToEntitiesMap() is Done.";
//    utils->buidMatrixToBlocksMapping();
//    qDebug()<<"2.utils->buidMatrixToBlocksMapping() is Done.";
//    utils->setIESBlockParent();
//    qDebug()<<"3.utils->setIESBlockParent() is Done.";
    showBranchResult(utils->detectDeviceBranch());
//    qDebug()<<"4.showBranchResult(utils->detectDeviceBranch()) is Done.";
//    utils->refreshGraphicView();
//    qDebug()<<"5.utils->refreshGraphicView() is Done.";
}

void DeviceRecordForm::on_pBtnUpload_clicked()
{
    this->uiDeviceRecordForm->pBtnOk->setDisabled(true);
    QMap<QString,int> curRecognitionResult;
    QMap<QString,QMap<QString,int>> finalSingleResultMap;
    QList<DeviceInfo*> detectType;
    QList<int> typeCount;
    QTableWidget* tableWidget = uiDeviceRecordForm->tableWidget;
    auto curViewName = utils->getCurrentViewName();
    for(int i = 0; i < showBlockList.size(); i++)
    {
       FAS_Block* myBlock = showBlockList.at(i);
//       QString deviceName = nameLocalStrList.at(comBoxList.at(i)->currentIndex());
       QString deviceType = typeLocalStrList.at(comBoxList.at(i)->currentIndex());
//       DeviceInfo* tmpType = COMMONDEF->getDeviceInfoFromName(deviceName);
       DeviceInfo* tmpType = myBlock->getType();
       tmpType->setShowSVG(false);
       FASUtils::setBlcokType(myBlock,tmpType);
       detectType.append(tmpType);
       int count = tableWidget->item(i, 2)->text().toInt();
       typeCount.append(count);

       if(!deviceType.isEmpty() && deviceType.compare("UnknownDevice", Qt::CaseInsensitive)
               && deviceType.compare("NotDevice", Qt::CaseInsensitive)
               && deviceType.compare("待标定设备", Qt::CaseInsensitive)
               && deviceType.compare("疑似设备", Qt::CaseInsensitive)){
           if(curRecognitionResult.find(deviceType) == curRecognitionResult.end()){
              curRecognitionResult.insert(deviceType,count);
           }else{
              curRecognitionResult.find(deviceType).value() = curRecognitionResult.value(deviceType) + count;
           }
       }
    }

    curRecognitionResult = mergeCustomizedDevices(curRecognitionResult);

    finalSingleResultMap.clear();
    QString responseCode;
    QString popUpError;
    QString messageContent;
    if(!curViewName.contains("图例层")){
        finalSingleResultMap.insert(curViewName,curRecognitionResult);

//    utils->setRecognitionResult(curViewName,curRecognitionResult);

        GrabThread *httpRequest = new GrabThread();
        QTime startTime = QTime::currentTime();
//        qDebug() << "Start uploading single floor recognition result ..." <<startTime;
        httpRequest->syncResultPostQuery(finalSingleResultMap,"onefloor");
        responseCode = httpRequest->getResultSyncResponseCode();
        QTime endTimeE = QTime::currentTime();
//        qDebug() << "Uploaded with code:"<<responseCode<<endTimeE;
        popUpError = httpRequest->getHttpErrorMessage();
        delete httpRequest;
    }
    messageContent = QString::fromUtf8("Floors[%1] action result. \nWith code:%2")
            .arg(curViewName)
            .arg(responseCode);
    if(responseCode.isEmpty()){
        messageContent = QString(tr("Network is invalid."));
    }
    if(!popUpError.isNull() && !popUpError.isEmpty()){
        messageContent = QString::fromUtf8("%1\r\n%2:%3").arg(messageContent).arg(tr("Exception")).arg(popUpError);
    }

    QMessageBox *messageBox = new QMessageBox(tr("Result"),
                                             messageContent,
                                             QMessageBox::Information,
                                             QMessageBox::Ok | QMessageBox::Default,
                                             QMessageBox::Cancel | QMessageBox::Escape,
                                             0);
    messageBox->setTextInteractionFlags(Qt::TextSelectableByMouse);
    messageBox->exec();
    this->uiDeviceRecordForm->pBtnOk->setEnabled(true);
}

void DeviceRecordForm::slotTotalCount(int index)
{
    int mcount=0;
    for(int i=0;i<comBoxList.size();i++)
    {
        QString s1="UnknownDevice";
        QString s2="NotDevice";
        QString s3="待标定设备";
        QString s4="疑似设备";
        QString s=comBoxList[i]->currentText();

        if((s!=s1)&&(s!=s2)&&(s!=s3)&&(s!=s4))
        {
            mcount+=countList[i]->text().toInt();
        }
    }

    totalCountItem->setText(QString::number(mcount));
}

void DeviceRecordForm::on_checkHideRD_stateChanged(int arg1)
{
    COMMONDEF->setHideRecognizedDevice(arg1/*this->uiDeviceRecordForm->checkHideRD->isChecked()*/);
    showBlockList = utils->getBlocksOfView(utils->getCurrentViewName());
    int resultCount=comBoxList.size();
    for(int i = 0; i < resultCount; i++)
    {
       FAS_Block* myBlock = showBlockList.at(i);
       QString deviceType = typeLocalStrList.at(comBoxList.at(i)->currentIndex());
       DeviceInfo* tmpType = myBlock->getType();
       if(!deviceType.isEmpty() && deviceType.compare("UnknownDevice", Qt::CaseInsensitive)
               && deviceType.compare("NotDevice", Qt::CaseInsensitive)
               && deviceType.compare("待标定设备", Qt::CaseInsensitive)
               && deviceType.compare("疑似设备", Qt::CaseInsensitive)){
           if(COMMONDEF->isHideRecognizedDevice()){
               tmpType->setValidRecognition(true);
           }else{
               tmpType->setValidRecognition(false);
           }
           QList<FAS_Insert*> devicesOfName = nameToEntityHash.values(myBlock->getName());
           FAS_Insert* oneDevice = devicesOfName.first();
           oneDevice->setSelected(true);
       }

       tmpType->setShowSVG(false);
       tmpType->setTypeCode(deviceType);
       FASUtils::setBlcokType(myBlock,tmpType);
    }
    FASUtils::setAllDeviceEntityType(showBlockList, nameToEntityHash);
    utils->refreshGraphicView();
}

void DeviceRecordForm::on_pBtnAdd_clicked()
{
    if(!uiDeviceRecordForm->tableWidget_Added->isVisible()){
        uiDeviceRecordForm->tableWidget_Added->setVisible(true);
    }
    AddCustomizedDeviceDialog newCustomizedDeviceDialog(this, utils);
    if(newCustomizedDeviceDialog.exec() == QDialog::Accepted){
        loadCustomizedDevices();
    }
}

void DeviceRecordForm::loadCustomizedDevices(){
    QComboBox* tcomBox = nullptr;
    QPushButton* tpushButton = nullptr;
    QTableWidget* tableWidget = uiDeviceRecordForm->tableWidget_Added;
    tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableWidget->setRowCount(0);
    tableWidget->clearContents();
    tableWidget->verticalHeader();
    int rowCount = tableWidget->rowCount();
    for(int rowIndex = 0;rowIndex < rowCount;rowIndex++){
        tableWidget->removeRow(rowIndex);
    }
    rowCount = tableWidget->rowCount();
    auto customizedResult = utils->getCustomizedResult();
    auto curViewName = utils->getCurrentViewName();
    QMap<QString, int> newAddedDevices;
    if(this->isShowAll){
        auto floors = customizedResult.keys();
        foreach(auto floor, floors){
            auto tmpDevices = customizedResult.value(floor);
            auto deviceTypes = tmpDevices.keys();
            foreach(auto deviceType, deviceTypes){
                auto tmpDeviceCount = tmpDevices.value(deviceType);
                if(newAddedDevices.find(deviceType) != newAddedDevices.end()){
                    newAddedDevices.find(deviceType).value() += tmpDeviceCount;
                }else{
                    newAddedDevices.insert(deviceType, tmpDeviceCount);
                }
            }
        }
    }else if(!customizedResult.isEmpty() && customizedResult.find(curViewName) != customizedResult.end()){
        newAddedDevices = customizedResult.find(curViewName).value();
    }
    QString newAddedDeviceName;
    int newAddedDeviceNumber;
    int newAddedDevicesNumber = newAddedDevices.size();
    for(int insertRowNumber=0;insertRowNumber<newAddedDevicesNumber;insertRowNumber++){
        newAddedDeviceName = newAddedDevices.keys().at(insertRowNumber);
        newAddedDeviceNumber = newAddedDevices.value(newAddedDeviceName);

        tableWidget->insertRow(insertRowNumber);
        //**********************Device Head***************************************/
/*        QTableWidgetItem* titleItem = new QTableWidgetItem();
        titleItem->setText(tr("New Device"));
        titleItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(insertRowNumber, 0, titleItem);
*/
        //**********************Device Type***************************************/
        tcomBox = new QComboBox(this);
        tcomBox->addItems(nameLocalStrList);
        tcomBox->setCurrentIndex(nameLocalStrList.indexOf(newAddedDeviceName));
        tcomBox->setEnabled(false);
        tableWidget->setCellWidget(insertRowNumber, 0, tcomBox);

        //**********************Device Count***************************************/
        QTableWidgetItem* totalCountItem = new QTableWidgetItem();
    //    totalCountItem = new QTableWidgetItem();
        totalCountItem->setText(QString::number(newAddedDeviceNumber));
        totalCountItem->setFlags(Qt::ItemIsEditable);
        tableWidget->setItem(insertRowNumber, 1, totalCountItem);

        //**********************Device Erease***************************************/
/*        tpushButton = new QPushButton(this);
        tpushButton->setText(tr("Delete"));
        tpushButton->setEnabled(false);
        tableWidget->setCellWidget(insertRowNumber, 3, tpushButton);
        connect(tpushButton, static_cast<void (QPushButton::*)(bool)>(&QPushButton::clicked),this, [=]( bool isDel ) {
            auto curViewCustomizedDevices = utils->getCustomizedResult().find(curViewName).value();
            QMapIterator<QString,int>i(curViewCustomizedDevices);
            while(i.hasNext()) {
               auto item =  i.next();
               auto key  = item.key();
               if (key.contains(newAddedDeviceName,Qt::CaseInsensitive)) {
    //               curViewCustomizedDevices.remove(key);
                   curViewCustomizedDevices.take(key);
                   break;
               }
            }

            utils->setCustomizedResult(curViewName,curViewCustomizedDevices);
            loadCustomizedDevices();
        });
*/
    }
    //resize width
    tableWidget->resizeColumnsToContents();
    tableWidget->resizeRowsToContents();
//    tableWidget->deleteLater();
    setLayout(this->uiDeviceRecordForm->verticalLayout_main);
}

QMap<QString,int> DeviceRecordForm::mergeCustomizedDevices(QMap<QString,int> recognitionDevices){
    //Merge recognized devices and customized devices
    QMap<QString,int> curRecognitionResult = recognitionDevices;
    auto customizedDevices = utils->getCustomizedResult().value(utils->getCurrentViewName());
    customizedDevices.remove("NotDevice");
    customizedDevices.remove("UnknownDevice");
    customizedDevices.remove("疑似设备");
    customizedDevices.remove("待标定设备");
    auto customizedDeviceNames = customizedDevices.keys();
    foreach(auto customizedDeviceName, customizedDeviceNames){
        auto customizedDeviceTypeCode = typeStrList.at(nameLocalStrList.indexOf(customizedDeviceName));
        int customizedDeviceNumber = customizedDevices.value(customizedDeviceName);
        if(curRecognitionResult.find(customizedDeviceTypeCode) != curRecognitionResult.end()){
            curRecognitionResult.find(customizedDeviceTypeCode).value() += customizedDeviceNumber;
        }else{
            curRecognitionResult.insert(customizedDeviceTypeCode, customizedDeviceNumber);
        }
    }
    return curRecognitionResult;
}
