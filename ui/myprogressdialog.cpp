#include "myprogressdialog.h"
#include "ui_myprogressdialog.h"

MyProgressDialog::MyProgressDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MyProgressDialog)
{
    ui->setupUi(this);
    //this->setWindowFlags(Qt::FramelessWindowHint);

    QString s="QProgressBar{text-align: center;}QProgressBar::chunk{background-color:blue;}";
    ui->progressBar->setStyleSheet(s);
//    ui->progressBar->setVisible(false);
}

MyProgressDialog::~MyProgressDialog()
{
    delete ui;
}
void MyProgressDialog::setRange(int min,int max)
{
    ui->progressBar->setRange(min,max);
}

void MyProgressDialog::setValue(int v)
{
    ui->progressBar->setValue(v);
}
void MyProgressDialog::setStyle(QString color)
{
    QString s=QString("QProgressBar{text-align: center;}QProgressBar::chunk{background-color:%1;}").arg(color);
    ui->progressBar->setStyleSheet(s);
}


