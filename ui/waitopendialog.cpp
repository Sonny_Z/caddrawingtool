#include "waitopendialog.h"
#include "ui_waitopendialog.h"
#include<QMovie>
#include <QTime>
#include <QFileDialog>
#include <QMessageBox>
#include "commondef.h"
WaitOpenDialog::WaitOpenDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WaitOpenDialog)
{
    ui->setupUi(this);

    timer=new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(slotTimeOut()));
    timer->setSingleShot(true);
    timer->start(50);


    this->setWindowFlag(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setWindowOpacity(0.8);
    //ui->label->setStyleSheet("QLabel{ background: yellow }");
    //ui->label->setAttribute(Qt::WA_TranslucentBackground);
    ui->label->setMovie(new QMovie(":/icons/resource/image/busy/busy2.gif"));
    ui->label->movie()->start();

    thread=new ThreadDxfProcess(this);
    connect(thread,SIGNAL(dxfOpenSuccess()),this,SLOT(accept()));
    connect(thread,SIGNAL(dxfOpenFailed()),this,SLOT(reject()));

}

WaitOpenDialog::~WaitOpenDialog()
{
    delete ui;
}
void WaitOpenDialog::slotTimeOut()
{
    timer->stop();//start thread openCADFile
    thread->setParas(file,utils);
    thread->start();

}

void WaitOpenDialog::setParas(QString file,FASUtils* utils)
{
    this->file=file;
    this->utils=utils;
}


