#ifndef SETTINGDIALOG_H
#define SETTINGDIALOG_H

#include <QDialog>
#include "fasutils.h"

namespace Ui {
class SettingDialog;
}

class SettingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingDialog(QWidget *parent = 0, FASUtils* utils = nullptr);
    ~SettingDialog();
public slots:
    void on_OK_clicked();
    void on_Cancel_clicked();
    void enableLineEdit();
    void on_setColorButton_clicked();

private:
    Ui::SettingDialog *ui;
    FASUtils* utils = nullptr;
    QColor bgColor = QColor(Qt::black);
};

#endif // SETTINGDIALOG_H
