#include "finddevice.h"
#include "ui_finddevice.h"

//sort with alphabetical order
#include <QMessageBox>

#include "commondef.h"
#include "fasutils.h"

FindDevice::FindDevice(QWidget *parent, FASUtils* utils):
    QDialog(parent),
    uiFindDevice(new Ui::FindDevice)
{
    uiFindDevice->setupUi(this);
    FASUtils::setFontForUI(this);
    this->utils = utils;
}

FindDevice::~FindDevice()
{
    delete uiFindDevice;
}

void FindDevice::on_okPushButton_clicked()
{
    int error = 0;
    QString codeInfo = uiFindDevice->findCodelineEdit->text();

    if(nullptr == utils)
    {
        QMessageBox::information(this, tr("Error"), tr("Critical error happens, please restart software."));
        return;
    }
    if(!utils->isValidDeviceCode(codeInfo))
    {
        QMessageBox::information(this, tr("Error"), tr("Please check input!"));
        return;
    }
    FAS_Entity* destEntity = utils->findDeviceByCode(codeInfo);
    if(nullptr == destEntity)
    {
        QMessageBox::information(this, tr("Error"), tr("Cannot find the code."));
        return;
    }
    FAS_Vector position = destEntity->getMiddlePoint();
    destEntity->setSelected(true);
    auto view = utils->getGraphicView();
    view->zoomAuto();
    FAS_Vector currectFactor = utils->getGraphicView()->getFactor();
    double f = (currectFactor.x + currectFactor.y) * 0.5;
    view->zoomIn(0.1/f, position);

    close();
}

void FindDevice::on_cancelPushButton_clicked()
{
    close();
}
