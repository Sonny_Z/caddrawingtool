#include "threaddraw.h"
#include <QDebug>
#include <QPainter>
#include "commondef.h"
#include "fas_painterqt.h"
ThreadDraw* ThreadDraw::uniqueInstance = NULL;
ThreadDraw::ThreadDraw()
{
 stopped = false;
}
ThreadDraw::~ThreadDraw()
{
}
ThreadDraw* ThreadDraw::instance()
{
    if (uniqueInstance==NULL)
    {
        uniqueInstance = new ThreadDraw();
    }
    return uniqueInstance;
}
void ThreadDraw::run()
{
        //QMutexLocker locker(&mutex);
        draw();
}
void ThreadDraw::updateElements(FAS_GraphicView* view)
{

        this->stopped=false;
        this->view=view;
        start();
}
void ThreadDraw::draw()
{
    COMMONDEF->pix->fill(Qt::transparent);
    FAS_PainterQt painter2(COMMONDEF->pix);
    painter2.setDrawingMode(FAS2::ModeFull);
    painter2.setDrawSelectedOnly(false);
    view->drawLayer2((FAS_Painter*)&painter2);
    emit signalDrawFinished();
}

void ThreadDraw::stop()
{
    stopped = true;
}
