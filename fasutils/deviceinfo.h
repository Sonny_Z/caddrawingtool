#ifndef DEVICEINFO_H
#define DEVICEINFO_H
#include <QtSvg/QSvgRenderer>

class DeviceInfo
{
public:
    DeviceInfo(int id, QString name,QString typeCode, QString iconPath);
    DeviceInfo(DeviceInfo* ori);
    ~DeviceInfo();

    int getID();
    void setID(int id);
    QString getName();
    void setName(QString name);
    QString getTypeCode();
    void setTypeCode(QString typeCode);
    QSvgRenderer* getSvgRender();
    void setSvgRender(QSvgRenderer* render);

    bool isUnknownDevice();
    bool isNotDevice();
    bool isValidDevice();
    bool isIES();
    bool isD_AND_A();

    bool isRecognized();
    void setValidRecognition(bool isRecognized);

    bool isSvgEntity();
    bool getShowSVG();
    void setShowSVG(bool show);

    QString getCodeInfo();
    void setCodeInfo(QString code);

    QString getNoteText();
    void setNoteText(QString note);

    QString getSystemType();
    void setSystemType(QString systemType = "UnknownSystem");

    QString getPerfectMatchFlag();
    void setPerfectMatchFlag(QString perfectMatchFlag = "null");
private:
    DeviceInfo();

    int deviceID = 0;
    QSvgRenderer* svgRender = nullptr;
    QString name;
    QString typeCode;
    bool showSVG;
    bool validRecognition = false;
    QString codeInfo;
    QString noteText;
    QString systemType = "UnknownSystem";
    QString perfectMatchFlag = "";
};

#endif // DEVICEINFO_H
