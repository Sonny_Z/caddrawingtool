#include "longtimeprocess.h"
#include "blockform.h"

LongTimeProcess::LongTimeProcess()
{
    value=0;
}
 void LongTimeProcess::setParas1(int v,FASUtils* utils)
 {
     value=v;
     this->utils=utils;
 }
void LongTimeProcess::slotDowork()
{

    if(value==1)
    {
        work1();
    }
    else if(value==2)
    {
        work2();
    }
    emit sigFinish();

}
void LongTimeProcess::work1()
{
//    utils->buildSubViews();
//    utils->zoomAuto();
    auto newView = utils->buildSubView();
//    QString newViewCreatedMessage = QString::fromUtf8("Confirm the selected area as a new view:%1").arg(newView);
//    QMessageBox::information(this, tr("Confirmation"), tr(newViewCreatedMessage.toLatin1()));
    auto popUpKey = QString(tr("Info"));
    if(newView.contains(popUpKey, Qt::CaseInsensitive)){
        emit sigReturedNewViewInfo(newView);
    }
}
void LongTimeProcess::work2()
{

//    COMMONDEF->setSystem(FAS2::IESsystem);
//    COMMONDEF->buildDeviceInfoList();
}
