﻿#ifndef COMMONDEF_H
#define COMMONDEF_H

#include "deviceinfo.h"
#include <fas.h>
#include "fas_entity.h"
#include <QHash>
#include <QStringList>
#include <QSettings>
#include <QApplication>
#include "grabthread.h"
#include <QNetworkConfigurationManager>

#define COMMONDEF CommonDef::instance()

class CommonDef : public QObject
{
    Q_OBJECT

public:
    static CommonDef* instance();
    ~CommonDef();

    void init();
    void clear();
    qint64 hash(const QString & str);
    bool isDirExist(QString fullPath);
    QStringList getLocalDeviceTypeList();
    QStringList getDeviceTypeList();
    QStringList getDeviceNameList();
    QString getDatabaseName();
    QString getExcelName();
    QList<DeviceInfo*> getDeviceInfoList();
    void buildDeviceInfoList();
    DeviceInfo* getDefaultType();
    DeviceInfo* getNewNotDeviceInfo();
    DeviceInfo* getDeviceInfoFromName(QString name);
    DeviceInfo* getDeviceInfoFromID(int id);
    QHash<QString, QString> getNameToSvgFilePathHash();
    QString getSvgFolder();
    double getTolerance();
    double getConnectionTolerance();
    QString getDxfOfDevicePathStr();
    QString getSystemTypeIESStr();
    QString getSystemTypeD_AND_AStr();
    QString getNotFoundStr();
    QString textName;
    QString translateToLocal(QString engStr);
    FAS2::Language getLanguage();
    void setLanguage(FAS2::Language language);
    FAS2::SystemType getSystem();
    QString getSystem(bool isGetSystemNameString);
    QString getSystemByCode(QString systemCode);
    void setSystem(FAS2::SystemType systemName);
    int getSubViewSplitterFactor();
    int getSubViewOutlineCollectionFactor();
    bool getAdmin();
    void setAdmin(bool isAdmin);
    QHash<QString,QHash<QString,QHash<QString, QString>>> getDeviceNameIdDict();
    QHash<QString,QString> getDeviceNameCodeDict();

    void getAppSettings();
    QString getAppSetting(QString &key);
    void setAppSetting(QString key, QString value);
    QString getAppPerformanceLog();

    void addProductName2TypeCode(QString productName2TypeCode);
    QList<QString> getProductName2TypeCode();

    void clearDir(const QString& temp_path);
    void setReadOnly(bool isReadOnly);
    bool isReadOnlyMode();

    bool isValidDomain(QString localDomainName);

    float getMinMatchRate();
    bool isConbinedWithLocalData();

    bool isNetWorkOnline();
    bool isLogEnabled();

    int timeoutMillisecond();
    bool isDigitString(const QString& src);

    QString getChangeHistoryLogPath();

    void setHideRecognizedDevice(bool v);
    bool isHideRecognizedDevice();

    QString getLatestOnlineAppVersion();

    QString tempDirPath();

    void saveToFile(QString& content, QString& fileName, bool isAppend = false, QString fileType = "log", QString dir = "./runLog");

    QString getAppVersion();
    bool userNameValidation(QString userName);
    bool initializationValidation();

    bool floorValidation(QString floorName);

    bool getSvgUiMode();
    void setSvgUiMode(bool svgUiEnabled);
private:
    CommonDef();
    bool buildDeviceNameIdDict();
    void loadLocalProductTypeList(QString systemType);
    static CommonDef* uniqueInstance;
    QString appVersion = "V0.0.16";
    /*= QList<QString>() << "carcgl"*/
    QList<QString> domainCheckList{"carcgl","apfs","utccgl","apcarrier","EMEAFS"};
    QList<QString> floorCheckingList{"-3F","-2F","-1F","1F","2F"
                                     ,"3F","4F","5F","6F","7F","8F"
                                     ,"9F","10F","11F","12F","13F","14F"
                                     ,"15F","16F","17F","18F","19F","20F"
                                     ,"21F","22F","23F","24F","25F","26F"
                                     ,"27F","28F","29F","30F","31F","32F"
                                     ,"33F","34F","35F","36F","37F","38F"
                                     ,"39F","40F","避难层","地下层","消防控制室"
                                     ,"夹层","残疾人通道","图例层"};

    QList<QString> serverProductName2TypeCode;
    QList<DeviceInfo*> deviceInfoList;
    QHash<QString, DeviceInfo*> nameToDeviceInfoHash;
    QHash<int, DeviceInfo*> idToDeviceInfoHash;
    QHash<QString, QString> nameToSvgFileHash;
    QHash<QString, QString> typeNameDict;
    QHash<QString,QHash<QString,QHash<QString, QString>>> deviceNameIDDict;
//    QHash<QString,QString> deviceNameCodeHash;
    QString databaseName;
    QString excelName;
    QString svgFolderName;
    QString tempFolderName;
    QString settingFileName;
    QSettings* appSetting = nullptr;
    const QString systemTypeIES = "IES";
    const QString systemTypeD_AND_A = "D_AND_A";
    const double tolerance = 0.00001;
    const double connectiontolerance = 50;
    const int subViewSplitterFactor = 999;
    const int subViewOutlineCollectionFactor = 3;
    const QString dxfOfDevicePath = "./deviceImageStore";
    const QString changeHistoryLogPath = "./changeHistoryLog";
    const QString appPerformanceLog = "appPerformanceLog";
    bool isReadOnly = true;
    const float minMatchRate=0.9;
    const bool conbinedWithLocalData=true;
    bool hideRecognizedDevice = false;
    bool isSvgUiModeEnabled = false;


    QString latestOnlineAppVersion;

public:
    bool firstOpen;
    int mainwindowCenterx;
    int mainwindowCentery;
    bool convertDeviceToImage;
    int readLineNumber;

    bool drawWheelEventProgress;//是否绘制进度条,false:不使用画图显示进度条功能
    QPixmap* pix;//线程绘图传出的pix
    bool drawFinished;//线程绘图完成
    int drawProgressValue;//绘图进度条值
    int drawEntityCount;//上次所绘制的项的数量
    bool progressShown;//进度条已经被显示
    bool wheelZoomShowWidgetNum;//滚轮缩放显示数字控件
    bool isWheelEvent;//滚轮缩放显示数字控件功能为true时，这个变量用来判断是否鼠标滚动

    //Dispaly factors, to speed up display performance;
    int bigEntityThd;       //When the entities count in one insert is bigger than bigEntityThd, we will skip this insert to speed up display;
    int displayThd;         //When (screen size)/(entity size) is smaller than displayThd, then we will just draw a rectangle to speed up display;
    int maxDisplayEntities; //Maximal displayed entities on the GUI;
    int minDisplayEntities; //Minimal displayed entities on the GUI; Finially, one optical display number will be calcuated based on min and max;
    bool fastDisplay; //Added by Jie, April 2021; Open this switch, we will only show maxDisplayEntities on GUI;
};

#endif // COMMONDEF_H
