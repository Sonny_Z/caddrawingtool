﻿#include "fasutils.h"

//sort with alphabetical order
#include <QApplication>
#include <QMdiArea>
#include <QFileDialog>
#include <QFontDatabase>
#include <QSvgGenerator>
#include <QPrinter>
#include <QImageWriter>
#include <QtCore/qmath.h>
#include <QTextStream>
#include <QSettings>
#include <QGraphicsScene>

#include "cadsqlite.h"
#include "fas_actioneditundo.h"
#include "fas_actionmodifydelete.h"
#include "fas_block.h"
#include "fas_insert.h"
#include "fas_line.h"

#include "fas_selection.h"

#include "fas_text.h"
#include "fas_Information.h"
#include <QDebug>
FASUtils* FASUtils::singletonUtils = nullptr;
FASUtils:: FASUtils()
{
    codeToDeviceMap.clear();
    twinkleTimer = new QTimer(this);
    twinkleTimer->setObjectName("twinkle");
    maxNumberInLoop = 10000000;
}

FASUtils* FASUtils::getInstance()
{
    if(nullptr == singletonUtils)
    {
        singletonUtils = new FASUtils();
    }
    return singletonUtils;
}

FASUtils::~FASUtils()
{
    clear();
    if(twinkleTimer != nullptr)
    {
        delete twinkleTimer;
        twinkleTimer = nullptr;
    }
    if(subViews4CurDocument.size()>0){
        qDeleteAll(subViews4CurDocument);
        subViews4CurDocument.clear();
    }

//    qDeleteAll(mdiWindowHash);
//    mdiWindowHash.clear();
//    qDeleteAll(subViews4CurDocument);
//    subViews4CurDocument.clear();
//    qDeleteAll(codeToDeviceMap);
//    codeToDeviceMap.clear();
//    qDeleteAll(codeToEntityMap);
//    codeToEntityMap.clear();
}

void FASUtils::clear()
{
    if(qg_graphicView != nullptr)
    {
        delete qg_graphicView;
        qg_graphicView = nullptr;
    }
//    central->close();
    clearCacheDeviceImagesAfterUpdate();
    finalCustomizedResult.clear();
    finalRecognitionResult.clear();
    recognizedDevices.clear();
    subViews4CurDocument.clear();
    codeToDeviceMap.clear();
    codeToEntityMap.clear();

    twinkleEntityList.clear();
    filePath.clear();
    codeLayer = nullptr;
    wireLineLayer = nullptr;
    maxNumberInLoop = 10000000;
//    deleteLater();
}


bool FASUtils::closeFile(QString filePath)
{
    // qDebug() << "Start  utils->closeFile(file)";
    bool result = false;
    if(nullptr == curDocument || filePath.isEmpty())
        return true;

#ifdef Q_OS_WIN
    // store detection rule and devicelist  before close file.
    storeDetectionRulesToDB();
    storeDeviceListToDB(this->getAllDevicesFromRS());
#endif

    QString currentFile = originDocument->getFilename();
    QString currentView = curDocument->getFilename();
    if(filePath == currentFile)
    {

//        delete originDocument;

        // qDebug() << "Start  clear subViews4CurDocument";
        if(subViews4CurDocument.size()>0){
//            qDeleteAll(subViews4CurDocument);
//            foreach(auto subView, subViews4CurDocument.values()){
//                delete subView->getBlockList();
//            }
            subViews4CurDocument.clear();
            viewNo =1;
        }
        // qDebug() << "End  clear subViews4CurDocument";

        // qDebug() << "Start  clear documentHash";
        if(documentHash.find(filePath) != documentHash.end())
        {
//            delete documentHash.find(filePath).value();
            documentHash.remove(filePath);
        }
        // qDebug() << "End  clear documentHash";

        // qDebug() << "Start  utils->clear()";
        this->clear();
        // qDebug() << "End  utils->clear()";

        // qDebug() << "Start  clear mdiWindowHash";
        if(mdiWindowHash.find(currentView) != mdiWindowHash.end())
        {
            delete mdiWindowHash.find(currentView).value();
            mdiWindowHash.remove(currentView);
        }
        // qDebug() << "End  clear mdiWindowHash";

//        deleteLater();
        curDocument = nullptr;
        originDocument = nullptr;

        result = true;
    }
    // qDebug() << "End  utils->closeFile(file)";
    return result;
}


void FASUtils::initDataBase()
{
    /*** create and init database ***/
    CadSqlite mySql;
    mySql.initDatabase();
}


bool FASUtils::openFile(QString filePath)
{
    QString currentFile;
    auto orsDoc = getORSDocument() ;
    if(orsDoc && nullptr != orsDoc){
        currentFile = orsDoc->getFilename();
        if(filePath == currentFile)
        {
            return true;
        }else{
            closeFile(currentFile);
        }
    }
//    clear();
    //initDetectionRule();
    bool result = false;
    if(documentHash.find(filePath) != documentHash.end())
    {
        //return activeExistingFile(filePath);
    }
    QFileInfo fileInfo(filePath);
//    auto fileSizeMB = fileInfo.size()/1024.00/1024.00;
//    if(fileSizeMB>5){
//        return false;
//    }

    if(fileInfo.isFile())
    {
        FAS2::FormatType type = FAS2::FormatDXF;
//        if(curDocument != nullptr){
//            delete curDocument;
//            curDocument = nullptr;
//        }
        QTime parsingStartTime = QTime::currentTime();
        QString fileParsingLogContent = QString::fromUtf8("\r\nFile parsing start time:::%1\r\n").arg(parsingStartTime.toString());
//        qDebug() << fileParsingLogContent;
        QString appPerformanceLog = COMMONDEF->getAppPerformanceLog();;
        COMMONDEF->saveToFile(filePath,appPerformanceLog,true);
        COMMONDEF->saveToFile(fileParsingLogContent,appPerformanceLog,true);
        curDocument = new FAS_Graphic();
        result = curDocument->open(filePath, type);
        if(result)
        {
            setORSDocument(curDocument);
            documentHash.insert(filePath, curDocument);
            //buildCodeToDeviceMap();
//            buidMatrixToEntitiesMap();
//            buidMatrixToBlocksMapping();
//            buildSubViews();
            result = true;
        }
        QTime parsingEndTime = QTime::currentTime();
        fileParsingLogContent = QString::fromUtf8("File parsing end time:::%1\r\n").arg(parsingEndTime.toString());
        COMMONDEF->saveToFile(fileParsingLogContent,appPerformanceLog,true);

        QString fileParsingPerfLogContent = QString::fromUtf8("File parse performance time:::%1ms\r\n\r\n")
                .arg(parsingStartTime.msecsTo(parsingEndTime));
        COMMONDEF->saveToFile(fileParsingPerfLogContent,appPerformanceLog,true);
//        qDebug() << fileParsingLogContent;
//        qDebug() << "File parsing total time:::" << parsingTotalTime;
    }
    else
    {
        result = false;
    }
    return result;
}

bool FASUtils::saveFile(QString filePath, bool isAutoSave)
{
    bool result = false;
    if(nullptr == curDocument)
        return result;
    QString currentFile = curDocument->getFilename();
    if(currentFile == filePath)
    {
        curDocument->setGraphicView(qg_graphicView);
        result = curDocument->save(isAutoSave);
    }
    return result;
}

bool FASUtils::saveAsFile(QString filePath, FAS2::FormatType type)
{
    bool result = false;
    if(nullptr == curDocument || 0 == filePath.size())
        return result;

    QString oriFile = curDocument->getFilename();
    curDocument->setGraphicView(qg_graphicView);
    result = curDocument->saveAs(filePath, type, true);
    if(result)
    {
        documentHash.remove(oriFile);
        documentHash.insert(filePath, curDocument);
    }
    return result;
}

bool FASUtils::activeExistingFile(QString file)
{
    bool result = false;
    if(curDocument != nullptr && file == curDocument->getFilename())
        return true;

    clear();
    if(documentHash.find(file) != documentHash.end() &&
       mdiWindowHash.find(file) != mdiWindowHash.end())
    {
        curDocument = documentHash.find(file).value();

        curMdiWindow =  mdiWindowHash.find(file).value();
        curMdiWindow->hide();
        createGraphicView(curMdiWindow);
        buildCodeToDeviceMap();
        result = true;
    }
    else
    {
        result = false;
    }
    return result;
}

void FASUtils::createGraphicView(QC_MDIWindow* mdiWindow)
{
    if(qg_graphicView != nullptr)
    {
        delete qg_graphicView;;
        qg_graphicView = nullptr;
    }
    qg_graphicView = new QG_GraphicView(mdiWindow, 0, curDocument);

    qg_graphicView->setObjectName("graphicview");
    qg_graphicView->addScrollbars();

    QMdiArea* mdiAreaCAD = central->getMdiArea();
    mdiWindow->setWidget(qg_graphicView);
    QMdiSubWindow* mainArea = mdiAreaCAD->addSubWindow(mdiWindow);
    mainArea->showMaximized();
    mainArea->setFocus();
    mdiWindow->show();
    curMdiWindow = mdiWindow;
    mdiWindowHash.insert(curDocument->getFilename(), mdiWindow);
    qg_graphicView->zoomAuto();

    emit singalConnectGraphicViewSlot();
}

void FASUtils::createSubGraphicView(QC_MDIWindow* mdiWindow,const QString& subViewName){
    setCurrentViewName(subViewName);
    setRSDocument(subViews4CurDocument.find(curSubViewName).value());
    if(qg_graphicView != nullptr)
    {
        delete qg_graphicView;
        qg_graphicView = nullptr;
    }
    qg_graphicView = new QG_GraphicView(mdiWindow, 0, curDocument);

    qg_graphicView->setObjectName("graphicview");
    qg_graphicView->addScrollbars();

    QMdiArea* mdiAreaCAD = central->getMdiArea();
    mdiWindow->setWidget(qg_graphicView);
    QMdiSubWindow* mainArea = mdiAreaCAD->addSubWindow(mdiWindow);
    mainArea->showMaximized();
    mainArea->setFocus();
    mainArea->show();
    mdiWindow->repaint();
    curMdiWindow = mdiWindow;

    mdiWindowHash.insert(curDocument->getFilename(), mdiWindow);
    qg_graphicView->zoomAuto();

    emit singalConnectGraphicViewSlot();
}

QList<QString> FASUtils::getSubViews(){
    return subViews4CurDocument.keys();
}


FAS_Document* FASUtils::getRSDocument()
{
    return curDocument;
}

FAS_Document* FASUtils::getORSDocument()
{
    return originDocument;
}


void FASUtils::setRSDocument(FAS_Document* document)
{
    //April, 2021 to speed up display;
    if (COMMONDEF->fastDisplay)
    {
        document->sortEntities();
    }

    this->curDocument = document;
}
void FASUtils::setORSDocument(FAS_Document* document)
{
    //April, 2021 to speed up display;
    if (COMMONDEF->fastDisplay)
    {
        document->sortEntities();
    }

    this->originDocument = document;
    subViews4CurDocument.insert("OriginalView",originDocument);
    setCurrentViewName("OriginalView");
}


void FASUtils::setCentralWidget(LC_CentralWidget* central)
{
    this->central = central;
}

LC_CentralWidget* FASUtils::getCentralWidget(){
    return this->central;
}

QC_MDIWindow* FASUtils::getCurMdiWindow()
{
    return curMdiWindow;
}


QG_GraphicView* FASUtils::getGraphicView()
{
    return qg_graphicView;
}

void FASUtils::setGraphicView(QG_GraphicView* view)
{
    this->qg_graphicView = view;
}

QG_GraphicView* FASUtils::getSubGraphicView()
{
    return qg_subGraphicView;
}

void FASUtils::setSubGraphicView(QG_GraphicView* view)
{
    this->qg_subGraphicView = view;
}

FAS_Entity* FASUtils::findDeviceByCode(QString codeInfo)
{
    FAS_Entity* result = nullptr;

    if(0 == codeToDeviceMap.size())
    {
        buildCodeToDeviceMap();
    }
    if(codeToDeviceMap.find(codeInfo) != codeToDeviceMap.end())
    {
        result = codeToDeviceMap.find(codeInfo).value();
    }
    return result;
}

QMultiHash<QString, FAS_Entity*>* FASUtils::getCodeToDeviceMapForMutation()
{
    return &codeToDeviceMap;
}

//get code entity hash.QHash<QString, FAS_Entity*> key is the code text.
QHash<QString, FAS_Entity*> FASUtils::getAllCodeEntityHash()
{
    QHash<QString, FAS_Entity *> codeEntityHash;
    if(curDocument != nullptr)
    {
        codeEntityHash.clear();
        QList<FAS_Entity*> entities = curDocument->getEntityList();
        for (auto oneEntity : entities)
        {
            if(!oneEntity->isVisible())
                continue;
            QString code;
            if(FASUtils::isValidCodeEntity(oneEntity, code))
            {
                codeEntityHash.insert(code, oneEntity);
            }
        }
    }
    return codeEntityHash;
}

//get note text entity hash.QHash<QString, FAS_Entity*> key is the note text.
QMultiHash<QString, FAS_Entity*> FASUtils::getAllNoteEntityHash()
{
    QMultiHash<QString, FAS_Entity *> noteEntityHash;
    if(curDocument != nullptr)
    {
        noteEntityHash.clear();
        QList<FAS_Entity*> entities = curDocument->getEntityList();
        for (auto oneEntity : entities)
        {
            if(!oneEntity->isVisible())
                continue;
            // if the entity is code entity, it should not be note text entity.
            QString temp;
            if(FASUtils::isValidCodeEntity(oneEntity, temp))
                continue;
            QString note;
            if(FAS2::EntityText == oneEntity->rtti())
            {
                note = ((FAS_Text*)oneEntity)->getText();
            }
            else if(FAS2::EntityMText == oneEntity->rtti())
            {
                note = ((FAS_MText*)oneEntity)->getText();
            }
            if(note.size() > 0)
            {
                noteEntityHash.insert(note, oneEntity);
            }
        }
    }
    return noteEntityHash;
}


//get all insert entities from graphic view.
QList<FAS_Insert*> FASUtils::getAllInsertEntities(FAS_EntityContainer* container)
{
    QList<FAS_Insert*> result;
    if(container != nullptr)
    {
        QList<FAS_Entity*> allEntities = container->getEntityList();
        result = getAllInsertEntities(allEntities);
    }
    return result;
}

QList<FAS_Insert*> FASUtils::getAllInsertEntities(QList<FAS_Entity*> container)
{
    QList<FAS_Insert*> result;
    for (FAS_Entity* entity : container)
    {
        if (FASUtils::isValidInsertEntity(entity))
        {
            result.append((FAS_Insert*)entity);
        }
        if(entity->isContainer())
        {
            result.append(getAllInsertEntities((FAS_EntityContainer*)entity));
        }
    }
    return result;
}
//get all insert entities from graphic view.
QList<FAS_Entity*> FASUtils::getAllValidEntities(FAS_EntityContainer* container)
{
    QList<FAS_Entity*> result;
    if(container != nullptr)
    {
        QList<FAS_Entity*> allEntities = container->getEntityList();
        if(allEntities.size()>500){
            return allEntities;
        }
        result = getAllValidEntities(allEntities);
    }
    return result;
}

//get all insert entities from graphic view.
int FASUtils::getEntityCountWithBigCondition(FAS_EntityContainer* container,int bigCondition)
{
    if(container != nullptr)
    {
        QList<FAS_Entity*> allEntities = container->getEntityList();
        int count = allEntities.size();
        for (FAS_Entity* entity : allEntities)
        {


            if(entity->isContainer()){
               int tempResult=getEntityCountWithBigCondition((FAS_EntityContainer*)entity,bigCondition);
               count=count+tempResult;
               if(count>=bigCondition){
                   break;
               }
            }


        }
        return count;

    }
}

QList<FAS_Entity*> FASUtils::getAllValidEntities(QList<FAS_Entity*> container)
{
    QList<FAS_Entity*> result;
    if(container.isEmpty()){
       return result;
    }

    for (FAS_Entity* entity : container)
    {

        if(result.size()>500){
            break;
        }

        if(entity->isContainer())
        {
            if(entity->rtti()==FAS2::EntityInsert || entity->rtti()==FAS2::EntityBlock){
              result.append(getAllValidEntities((FAS_EntityContainer*)entity));
            }else{                
              result.append(entity);
            }
        }else{
            result.append(entity);
        }

    }
    return result;
}

//build point matrix for line start point and end point
void FASUtils::buidMatrixToEntitiesMap(){

    if(nullptr == curDocument)
        return;
    matrixToEntitiesMap.clear();
    // get All Line entities.
    QList<FAS_Entity*> entities = curDocument->getEntityList();
    for (auto oneEntity : entities)
    {
        if(!oneEntity->isVisible())
            continue;

        if(FAS2::EntityLine==oneEntity->rtti()||FAS2::EntityPolyline==oneEntity->rtti()){
            FAS_Vector startPoint=oneEntity->getStartpoint();
            FAS_Vector endPoint=oneEntity->getEndpoint();

            if(abs(endPoint.distanceTo(startPoint))<20){
                continue;
            }


            setEntityToMatrixToEntitiesMap(startPoint,oneEntity);
            setEntityToMatrixToEntitiesMap(endPoint,oneEntity);
        }

    }

//    // qDebug() <<"init point matrix value "<< matrixToEntitiesMap;

}

//connect block with nearby point & entity
void FASUtils::buidMatrixToBlocksMapping(){
    if(nullptr == curDocument)
        return;

    // get All block entities.
    QList<FAS_Entity*> entities = curDocument->getEntityList();
    for (FAS_Entity* oneEntity : entities)
    {


        if(!oneEntity->isVisible())
            continue;

        if(FAS2::EntityInsert==oneEntity->rtti()){

            if(oneEntity->getMax().distanceTo(oneEntity->getMin())>3000){
                continue;
            }
//            // qDebug() <<"init blocks "<< oneEntity->getInsert()->getId();
            QSet<QString> matrixIDs= getBlocksMatrixs(oneEntity->getInsert(),300,300);
//            // qDebug() <<"init matrixIDs "<< matrixIDs;
            QList<QString> relatedPoints=getPointsFromMatrixs(matrixIDs);

//            // qDebug() <<"init related points "<< relatedPoints;

            //init point to block list
            QList<FAS_Entity*> relatedEntities=getPointsEntitiesFromMatrixs(matrixIDs);

            if(relatedEntities.size()>=2){
                //过滤掉只属于一个块内部的线
                for (int i = 0; i < relatedEntities.size(); i++){
                    for (int k = i + 1; k < relatedEntities.size(); k++){
                        if ( relatedEntities.at(i)->getId() ==  relatedEntities.at(k)->getId()){
                            FAS_Entity* newEnt=relatedEntities.at(k);
//                            // qDebug() <<"removed line number:"<< newEnt->getId();
                            if(newEnt->getStartpoint().distanceTo(newEnt->getEndpoint())<30){
                                removeLineFromPointToLineEntitiesMap(newEnt->getStartpoint().getPointID(),newEnt);
                                removeLineFromPointToLineEntitiesMap(newEnt->getEndpoint().getPointID(),newEnt);
                                relatedPoints.removeAll(newEnt->getStartpoint().getPointID());
                                relatedPoints.removeAll(newEnt->getEndpoint().getPointID());
                            }else{
                                double distance1=oneEntity->getDistanceToPoint(newEnt->getStartpoint(),nullptr,FAS2::ResolveNone,0);
                                double distance2=oneEntity->getDistanceToPoint(newEnt->getEndpoint(),nullptr,FAS2::ResolveNone,0);

                                if(distance1>distance2){
                                    relatedPoints.removeAll(newEnt->getStartpoint().getPointID());
                                }else{
                                    relatedPoints.removeAll(newEnt->getEndpoint().getPointID());
                                }
                            }
                            break;
                        }
                    }
                }
            }


            FAS_Entity** oneEntityNew=&oneEntity;
            int relatedPointsNumber=relatedPoints.size();
            for(int i=0;i<relatedPointsNumber;i++){
                QString point=relatedPoints.value(i);
                double distance=oneEntity->getDistanceToPoint(getVectorFromPoint(point),nullptr,FAS2::ResolveNone,0);
//                // qDebug() <<"Distance is "<< distance;
                if(distance<1e+1){
                    oneEntity->addConnectedPoints(point);
//                    // qDebug() <<"init block connected points "<< point;
                }

                if(distance<300){
                    pointToBlockMap.insert(point,oneEntity);
                    oneEntity->addMayRelatedPoints(point);
//                    // qDebug() <<"init block related points "<< point;
                }

            }

            if(oneEntity->getConnectedPointsSize()==0){
                oneEntity->setConnectedPoints(relatedPoints);
            }

//            // qDebug() <<"init related entities "<< relatedEntities;
            oneEntity->setCalPath(relatedEntities);
//            // qDebug() <<"Entity Value: "<< oneEntity;
//            // qDebug() <<"Update Entities Value: "<< entities;
//            // qDebug() <<"Entities Value: "<< curDocument->getEntityList();

            IESController.append(oneEntity);
        }

        }

}

//QMultiHash<QString, QMultiHash<QString, QList<FAS_Entity*>>>
//set one entity into map
bool FASUtils::setEntityToMatrixToEntitiesMap(FAS_Vector point,FAS_Entity* entity){

    QString matrixID=point.getPointMatrixID();
    QString pointID=point.getPointID();

    auto matrixResult = matrixToEntitiesMap.find(matrixID);
    QHash<QString, QList<FAS_Entity*>> pointMap;
    QList<FAS_Entity*> list;
    if(matrixResult != matrixToEntitiesMap.end()){
        pointMap=matrixResult.value();
        //get point map
        auto pointResult = matrixResult.value().find(pointID);
        if(pointResult != matrixResult.value().end()){
            list=pointResult.value();
            list.append(entity);
        }else{
             //if pointid no data in map
            list.append(entity);
            pointMap.insert(pointID,list);
        }

        pointMap.insert(pointID,list);
        matrixToEntitiesMap.insert(matrixID,pointMap);

    }else{

        //if there is no matrix id data in Map
            list.append(entity);
            pointMap.insert(pointID,list);
            matrixToEntitiesMap.insert(matrixID,pointMap);
    }
//    // qDebug() <<"init point matrix matrixID "<< matrixID;

    //init point to line mapping
    QList<FAS_Entity*> lines;
    auto pointResult = pointToLineEntitiesMap.find(pointID);
    if(pointResult != pointToLineEntitiesMap.end()){
        lines=pointResult.value();
        lines.append(entity);
        pointToLineEntitiesMap.remove(pointID);
        pointToLineEntitiesMap.insert(pointID,lines);

    }else{
        lines.append(entity);
        pointToLineEntitiesMap.insert(pointID,lines);
    }

    return true;
}

//找到潜在的起点，终点都没有和其他线有交点的线
//然后再找出此线附件的线和点，检查此线是不是疑似桥接线，如果是则加入内存管理
//判断桥接点的依据 1. 点到此线的距离小于50，2.有多点存在。 3.点所在线对应的另一点分别位于线的两侧  4.一侧点的数量大于2，一侧等于1
QHash<QString,QString> FASUtils::buildBridgePointToLineEntityMap(){

    QHash<QString,QString> bridgePointToLineMap;
    if(pointToLineEntitiesMap.size()==0){
        return bridgePointToLineMap;
    }

    //找到所有的满独立的线
//    // qDebug() <<"find single line begin";
    QSet<FAS_Entity*> bridgeLines;
    QHash<QString,QList<FAS_Entity*>>::iterator it;
    int runNum=0;
    for(it=pointToLineEntitiesMap.begin();it!=pointToLineEntitiesMap.end();++it)
    {
        // qDebug() << runNum++;
        QList<FAS_Entity*> values=it.value();
        QString key=it.key();
        if(values.size()==1){
            FAS_Entity* line=values.value(0);
            if(values.value(0)->getSize().magnitude()>5000){
                continue;
            }
            QString anotherKey=getAnotherPointFromPointToLineEntitiesMap(key,line);
            auto pointResult = pointToLineEntitiesMap.find(anotherKey);
            if(pointResult != pointToLineEntitiesMap.end()){
                QList<FAS_Entity*> anotherValues=pointResult.value();
                if(anotherValues.size()==1){
                    bridgeLines.insert(line);
                }
            }

        }
    }
//    // qDebug() <<"find single line end";


//    // qDebug() <<"validate line begin line numbers:" << bridgeLines.size();
    //找到交界线附件的点，并且判断他们的关系
    foreach(FAS_Entity* line,bridgeLines){
        if(line->getSize().magnitude()>5000){
            continue;
        }
        QSet<QString> lineMatrix=getLineMatrixs(line,0,0);
        QList<QString> nearbyPoints=getPointsFromMatrixs(lineMatrix);
//        // qDebug() << "find point IDs:" <<nearbyPoints;
        nearbyPoints.removeOne(line->getStartpoint().getPointID());
        nearbyPoints.removeOne(line->getEndpoint().getPointID());
        if(nearbyPoints.size()<4){
            continue;
        }

        QList<QString> oneSidePoints;
        QList<QString> anotherSidePoints;
        foreach(QString pointId,nearbyPoints){
            if(!line->isPointOnEntity(getVectorFromPoint(pointId),30)){
                nearbyPoints.removeOne(pointId);
            }
        }
        if(nearbyPoints.size()<4){
            continue;
        }

        foreach(QString pointId,nearbyPoints){
            FAS_Entity* anotherLine=pointToLineEntitiesMap.find(pointId).value().value(0);
            FAS_Vector anotherVector=getAnotherVecorFromPointToLineEntitiesMap(pointId,anotherLine);
            double pointInLineSide=getPointandLinePosition(line,anotherVector);
            if(pointInLineSide==0){
                continue;
            }else if(pointInLineSide>0){
                oneSidePoints.append(pointId);
            }else{
                anotherSidePoints.append(pointId);
            }
        }

        if(oneSidePoints.size()!=1 && anotherSidePoints.size()!=1){
            continue;
        }

        if(oneSidePoints.size()==1){
            foreach(QString point, anotherSidePoints){
                bridgePointToLineMap.insert(point,oneSidePoints.value(0));
//                // qDebug() <<"set bridge line/point data" << point << oneSidePoints.value(0);
            }
        }else if(anotherSidePoints.size()==1){
            foreach(QString point, oneSidePoints){
                bridgePointToLineMap.insert(point,anotherSidePoints.value(0));
//                // qDebug() <<"set bridge line/point data" << point << anotherSidePoints.value(0);
            }
        }
    }


    return bridgePointToLineMap;
}
//>0 one side   ==0 in line    <0 another side
double FASUtils::getPointandLinePosition(FAS_Entity* line,FAS_Vector point){
    double x1=line->getStartpoint().x;
    double y1=line->getStartpoint().y;
    double x2=line->getEndpoint().x;
    double y2=line->getEndpoint().y;
    double x3=point.x;
    double y3=point.y;

    return (x1-x3)*(y2-y3)-(y1-y3)*(x2-x3);

}
QList<FAS_Entity*> FASUtils::getLineFromPointToLineEntitiesMap(QString pointID){
    auto result = pointToLineEntitiesMap.find(pointID);
    QList<FAS_Entity*> ret=result.value();

    return ret;
}

void FASUtils::removeLineFromPointToLineEntitiesMap(QString pointID,FAS_Entity* line){

    if(line==nullptr){
        return;
    }
    auto result = pointToLineEntitiesMap.find(pointID);
    QList<FAS_Entity*> ret;

    if(result != pointToLineEntitiesMap.end()){
        ret=result.value();
        ret.removeOne(line);
    }

    pointToLineEntitiesMap.remove(pointID);
    pointToLineEntitiesMap.insert(pointID,ret);
//    // qDebug() <<"Remove Line from Map: PointID:"<<pointID<<" Line:"<<line->getId();
}

QString FASUtils::getAnotherPointFromPointToLineEntitiesMap(QString pointID,FAS_Entity* line){
    QString startPoint=line->getStartpoint().getPointID();
    if(pointID==startPoint){
        return line->getEndpoint().getPointID();
    }

    return startPoint;
}

FAS_Vector FASUtils::getAnotherVecorFromPointToLineEntitiesMap(QString pointID,FAS_Entity* line){
    QString startPoint=line->getStartpoint().getPointID();     
    if(pointID==startPoint){
        return line->getEndpoint();
    }

    return line->getStartpoint();
}

int FASUtils::getLineNumberFromPointToLineEntitiesMap(QString pointID){
   auto result = pointToLineEntitiesMap.find(pointID);

   if(result != pointToLineEntitiesMap.end()){
       return result->size();
   }

   return 0;
}

QHash<QString, QHash<QString, QList<FAS_Entity*>>> FASUtils::getMatrixToEntitiesMap(){
    return matrixToEntitiesMap;
}

QHash<QString, QList<FAS_Entity*>> FASUtils::getPointToEntities(QString matrixId){

    QHash<QString, QList<FAS_Entity*>> ret;
    auto matrixResult = matrixToEntitiesMap.find(matrixId);
    if(matrixResult != matrixToEntitiesMap.end()){
        ret=matrixResult.value();
    }

    return ret;

}
QSet<QString> getMatrixNeighbours(QString matrixId);

//get the matrix id which connected to block
QSet<QString> FASUtils::getBlocksMatrixs(FAS_Insert* block,double xExtends,double yExtends){
    QSet<QString> set;
    int minPointX=floor((block->getMin().x-xExtends)/200);
    int minPointY=floor((block->getMin().y-yExtends)/200);
    int maxPointX=floor((block->getMax().x+xExtends)/200);
    int maxPointY=floor((block->getMax().y+yExtends)/200);
    int temx=0;
    if(minPointX>maxPointX){
        temx=minPointX;
        minPointX=maxPointX;
        maxPointX=temx;
    }

    int temy=0;
    if(minPointY>maxPointY){
        temy=minPointY;
        minPointY=maxPointY;
        maxPointY=temy;
    }

    for(int i=minPointX;i<=maxPointX;i++){

       for(int j=minPointY;j<=maxPointY;j++){
          set.insert(QString("%1_%2").arg(QString::number(i)).arg(QString::number(j)));

       }
   }

    return set;

}

//get the matrix id which biside of point
QSet<QString> FASUtils::getPointMatrixs(QString pointID,int xExpansionFactor,int yExpansionFactor){
    QSet<QString> set;
    QStringList points=pointID.split("_");
    int pointX=floor((points.value(0).toDouble(nullptr))/200);
    int pointY=floor((points.value(1).toDouble(nullptr))/200);

    for(int i=pointX-1-xExpansionFactor;i<=pointX+1+xExpansionFactor;i++){

       for(int j=pointY-1-yExpansionFactor;j<=pointY+1+yExpansionFactor;j++){

           if(i==pointX && j==pointY){
               continue;
           }
          set.insert(QString("%1_%2").arg(QString::number(i)).arg(QString::number(j)));

       }
   }

    return set;

}

//get the matrix id which biside of point
QSet<QString> FASUtils::getLineMatrixs(FAS_Entity* line,int xExpansionFactor,int yExpansionFactor){
    QSet<QString> set;
    FAS_Vector startPoint=line->getMin();
    FAS_Vector endPoint=line->getMax();

    int pointX1=floor(startPoint.x/200);
    int pointY1=floor(startPoint.y/200);
    int pointX2=floor(endPoint.x/200);
    int pointY2=floor(endPoint.y/200);

    for(int i=pointX1-xExpansionFactor;i<=pointX2+xExpansionFactor;i++){

       for(int j=pointY1-yExpansionFactor;j<=pointY2+yExpansionFactor;j++){

          set.insert(QString("%1_%2").arg(QString::number(i)).arg(QString::number(j)));

       }
   }

    return set;

}

QList<QString> FASUtils::getPointsFromMatrixs(QSet<QString> matrixIDs){
//    // qDebug() <<"getPointsFromMatrixs start";
    QList<QString>  vets;
    if(matrixIDs.size()==0){
        return vets;
    }

    for(QString matrixID:matrixIDs){
        if(getPointSizeInMatrix(matrixID)==0){
            continue;
        }
        auto pointToEntitiesResult=getPointToEntities(matrixID);
        QHash<QString, QList<FAS_Entity*>>::const_iterator it;
        if(it!=pointToEntitiesResult.end()){
        for (it = pointToEntitiesResult.constBegin(); it != pointToEntitiesResult.constEnd(); ++it) {
            int lineNum=getLineNumberFromPointToLineEntitiesMap(it.key());
//            // qDebug() <<" Line number "<< lineNum;
            for(int i=0;i<lineNum;i++){
                vets.append(it.key());
//                // qDebug() <<"Add points: "<< it.key();
            }
        }
        }
    }
//    // qDebug() <<"getPointsFromMatrixs end";
    return vets;
}

QList<FAS_Entity*> FASUtils::getPointsEntitiesFromMatrixs(QSet<QString> matrixIDs){
    QList<FAS_Entity*>  vets;
    if(matrixIDs.size()==0){
        return vets;
    }

    for(QString matrixID:matrixIDs){
        if(getPointSizeInMatrix(matrixID)==0){
            continue;
        }
        auto pointToEntitiesResult=getPointToEntities(matrixID);
        QHash<QString, QList<FAS_Entity*>>::const_iterator it;
        for (it = pointToEntitiesResult.constBegin(); it != pointToEntitiesResult.constEnd(); ++it) {
            if(getEntitySizeByPointInMatrix(matrixID,it.key())==0){
                continue;
            }

            QList<FAS_Entity*> entities=getEntitesFromMatrixandPoint(matrixID,it.key());
            QList<FAS_Entity*>::const_iterator ie;
            for (ie = entities.constBegin(); ie != entities.constEnd(); ++ie) {
                vets.append(*ie);
//                // qDebug() <<"Related Line ID: "<< (*ie)->getId();
//                // qDebug() <<"StartPoint & Endpoint: "<< (*ie)->getStartpoint().getPointID()  << (*ie)->getEndpoint().getPointID();
            }
        }
    }

    return vets;
}

//get point size in matrix table
int FASUtils::getPointSizeInMatrix(QString matrixId){
    auto matrixResult = matrixToEntitiesMap.find(matrixId);
    if(matrixResult != matrixToEntitiesMap.end()){
        return matrixResult->size();
    }

    return 0;
}
QList<FAS_Entity*> FASUtils::getEntitesFromMatrixandPoint(QString matrixID,QString pointID){
    auto matrixResult = matrixToEntitiesMap.find(matrixID);
    QHash<QString, QList<FAS_Entity*>> pointMap;
    QList<FAS_Entity*> list;
    if(matrixResult != matrixToEntitiesMap.end()){
        pointMap=matrixResult.value();
        //get point map
        auto pointResult = pointMap.find(pointID);
        if(pointResult != pointMap.end()){
            list=pointResult.value();
        }
      }
     return list;
}

//get entity size for one point in metrix table
    int FASUtils::getEntitySizeByPointInMatrix(QString matrixID,QString pointID)
    {
        auto matrixResult = matrixToEntitiesMap.find(matrixID);
        QHash<QString, QList<FAS_Entity*>> pointMap;
        QList<FAS_Entity*> list;
        if(matrixResult != matrixToEntitiesMap.end()){
            pointMap=matrixResult.value();
            //get point map
            auto pointResult = pointMap.find(pointID);
            if(pointResult != pointMap.end()){
                list=pointResult.value();
                return list.size();
            }
        }
        return 0;
}
    //build block trees for IES devices
    void FASUtils::setIESBlockParent(){

        //1. loop the blocks, find related points
//        // qDebug() <<"Start IES Line Connection";
//        // qDebug() <<"Start block Numbers: "<< IESController.size();

//        // qDebug() <<"reset bridge line/point begin";
        bridgePointMap=buildBridgePointToLineEntityMap();
        // qDebug() <<"reset bridge line/point end";

        for(int runtime=0;runtime<3;runtime++){
        for (FAS_Entity* oneblock : IESController) {

            if(oneblock->hasCalParent()){
                continue;
            }
            setBlockParentLoop(&oneblock);
        }
        }
    }

    void FASUtils::setBlockParentLoop(FAS_Entity** oneBlock){

        QList<FAS_Entity*> path;

        bool isSpecialLine=false;

        if((*oneBlock)->hasCalParent()){
            return;
        }

        // qDebug() <<"Start block ID: "<< (*oneBlock)->getInsert()->getId();

        QList<QString> relatedPoints=(*oneBlock)->getConnectedPoint();
        int relatedPointNumber=relatedPoints.size();

//        for(QString point:relatedPoints){

//            double distance=(*oneBlock)->getDistanceToPoint(getVectorFromPoint(point),nullptr,FAS2::ResolveNone,0);
//            if(distance>1e+1){
////                relatedPointNumber=relatedPointNumber-1;
//            }else{
//                 pointID=point;
//            }
//        }
        // qDebug() <<"block related points: "<< relatedPoints;
        //if only one point related to this block, this block is leaf in the tree.
        if((1==relatedPointNumber) ){
            QString pointID=relatedPoints.first();
            FAS_Vector point=getVectorFromPoint(pointID);

            // qDebug() <<"Begin set parent to leaf block ID: "<< (*oneBlock)->getInsert()->getId();

            //如果同一个点关联两个以上的块，则将此点从其他块的关联点中删除
            auto pointsRelatedBlockesIt=pointToBlockMap.find(pointID);
            if(pointsRelatedBlockesIt!=pointToBlockMap.end()){
                QList<FAS_Entity*> pointsRelatedBlockes=pointToBlockMap.values(pointID);
                if(pointsRelatedBlockes.size()>1){
                    foreach(FAS_Entity* block,pointsRelatedBlockes){
                        if(block->getInsert()->getId()==(*oneBlock)->getInsert()->getId()){
                            continue;
                        }
                        block->removeMayRelatedPoints(pointID);
                        // qDebug() <<"revoce non-related point for block ID: "<< block->getInsert()->getId() << pointID;
                    }

                }
            }


            //find the point related line then get another point
            QList<FAS_Entity*> pointRelatedLines=getLineFromPointToLineEntitiesMap(pointID);
            // qDebug() <<"Main function received related line number:" << pointRelatedLines.size();
            if(pointRelatedLines.isEmpty()){
                // qDebug() <<"No Line related this block,Ignore this block";
            }

            if(pointRelatedLines.size()>1){
                // qDebug() <<"Many lines linked to this block. program couldn't handle";
            }

            if(pointRelatedLines.size()==1){
                FAS_Entity* line=pointRelatedLines.value(0);
                isGroupTransaction=false;
                findNextPoint(oneBlock,oneBlock,point,line);
                QString newPointID=(*oneBlock)->getConnectedPoint().value(0);
                // qDebug() <<"Connected point ID:" << newPointID;
                FAS_Entity* anotherBlock=findConnectedBlock(newPointID);

                int positionRating=getPointAndBlockPositionRating(oneBlock,&anotherBlock,getVectorFromPoint(newPointID));
                // qDebug() <<"Rating Result:" << positionRating;


                if(positionRating>=6){

                    path.append(line);
                    (*oneBlock)->setCalParent(anotherBlock);
                    (*oneBlock)->setCalPath(lineTransactionMap);
                    // qDebug() <<"Block parent & related points before delete"<< anotherBlock->getInsert()->getId() << anotherBlock->getRelatedPoints();
                    // qDebug() <<"Block parent & connected points before delete"<< anotherBlock->getInsert()->getId() << anotherBlock->getConnectedPoint();


                    anotherBlock->setCalChilds((*oneBlock));
                    // qDebug() <<"Block--Block Parent--Line "<< (*oneBlock)->getInsert()->getId() << "-" << anotherBlock->getInsert()->getId() << "-" << path.value(1);
                    // qDebug() <<"Connected path: "<<lineTransactionMap;
                    int signId = (*oneBlock)->getInsert()->getId();
                    setNewCodeToEntityByID(signId,QString::fromUtf8("De01i-%1").arg(signId));
                    if(isGroupTransaction){
                        rollbackTransaction();
                        // qDebug() <<"Rollback if it is group transaction";
                        // qDebug() <<"Block parent & related points after delete"<< anotherBlock->getInsert()->getId() << anotherBlock->getRelatedPoints();
                        // qDebug() <<"Block parent & connected points after delete"<< anotherBlock->getInsert()->getId() << anotherBlock->getConnectedPoint();
                    }else{
                        anotherBlock->removeMayRelatedPoints(newPointID);
                        anotherBlock->removeConnectedPoints(newPointID);
                        // qDebug() <<"Block parent & related points after delete"<< anotherBlock->getInsert()->getId() << anotherBlock->getRelatedPoints();
                        // qDebug() <<"Block parent & connected points after delete"<< anotherBlock->getInsert()->getId() << anotherBlock->getConnectedPoint();
                        submitTransaction();
                    }

                    setBlockParentLoop((&anotherBlock));
                    return;

                }

                if(positionRating<6){
                    // qDebug() <<"Reset block related point blockID"<<(*oneBlock)->getInsert()->getId();
                    // qDebug() <<"Reset block related point remove pointID"<<pointID;
                    // qDebug() <<"Reset block related point add pointID"<<newPointID;
                    // qDebug() <<"Ignore this block & point";
                    rollbackTransaction();
                    return;
                }
            }
        }else{

            // qDebug() <<"Ignore block ID: "<< (*oneBlock)->getInsert()->getId();
        }

    }

//点和块的关系，返回数字：10一定是，0一定不是，根据可能性的大小，会返回一个可能性比例
    int FASUtils::getPointAndBlockPositionRating(FAS_Entity** firstBlock,FAS_Entity** oneBlock,FAS_Vector point){
        if((*oneBlock)==nullptr){
            return 0;
        }

        // qDebug() <<"Trying to compare block %1 point %2  connection Rating: "<< (*oneBlock)->getInsert()->getId() << point.getPointID();
        FAS_Entity* block = (*oneBlock);
        QString pointID=point.getPointID();
        auto blockFromAnotherPointResult= pointToBlockMap.find(pointID);
        //如果另一点也挨着块
        if(blockFromAnotherPointResult != pointToBlockMap.end()){

            if((*firstBlock)!=nullptr){
                if((*oneBlock)->getInsert()->getId()==(*firstBlock)->getInsert()->getId()){
                    return 0;
                }
            }

            if(block->hasCalParent()){
                return 5;
            }


            //case1 如果点和块是挨着的
            double distance=block->getDistanceToPoint(point,nullptr,FAS2::ResolveNone,0);
            // qDebug() <<"Compare the point with related block distance "<<distance;
            if(distance<15){
                return 10;
            }

            //case2 如果不挨着，但是从子节点找上来时，当前块只有一个节点
            QList<QString> linkedPoints=block->getRelatedPoints();
            if(linkedPoints.size()==1){
                return 10;
            }

            //case3 如果当前块只有两个节点，判定为高疑似相交点
            if(linkedPoints.size()==2){

                if(block->isLeaf()){
                    return 9;
                }
                return 8;
            }

            return 5;


        }

        return 0;

    }

    int FASUtils::getPointAndNearBlockPositionRating(FAS_Vector point){

         // qDebug() <<"Compare the point with related block PointID"<<point.getPointID();
        auto blockFromAnotherPointResult= pointToBlockMap.find(point.getPointID());
        //如果另一点不挨着块
        if(blockFromAnotherPointResult == pointToBlockMap.end()){
            return 0;
        }
        FAS_Entity * firstBlock=nullptr;
        FAS_Entity * block=findConnectedBlock(point.getPointID());
        // qDebug() <<"Compare the point with related block BlockID"<<block->getInsert()->getId();
        return getPointAndBlockPositionRating(&firstBlock,&(block),point);

    }

//找到点和它相关的疑似点
    FAS_Vector FASUtils::findNextPoint(FAS_Entity** beginningBlock,FAS_Entity** oneBlock,FAS_Vector point,FAS_Entity* oneLine){
        // qDebug() <<"begin run find next point function";
        FAS_Entity* nullBlock=nullptr;

        QString pointID=point.getPointID();
        FAS_Vector returnPoint;
        QList<FAS_Entity*> pointRelatedLines=getLineFromPointToLineEntitiesMap(pointID);
        // qDebug() <<"found the nearby Point & line number" <<pointID<<"&"<< pointRelatedLines.size();

        if(lineTransactionMap.size()>=20){
            return returnPoint;
        }

        int rating=0;
        //如果当前点只对应一条直线
        if(1==pointRelatedLines.size()){
            // qDebug() <<"Begin find point when line number is 1";
            for(FAS_Entity* anotherLine:pointRelatedLines){
                // qDebug() <<"One point ID"<<pointID;
                FAS_Vector anotherVector=getAnotherVecorFromPointToLineEntitiesMap(pointID,anotherLine);
                // qDebug() <<"Another point ID"<<anotherVector.getPointID();
                rating=getPointAndNearBlockPositionRating(anotherVector);
                // qDebug() <<"Connection rating is:"<<rating;

                removeLineFromPointToLineEntitiesMap(anotherVector.getPointID(),anotherLine);
                //如果确定是挨着的
                if(rating>8){
                    // qDebug() <<"Final return point is:"<<anotherVector.getPointID();
                    (*beginningBlock)->removeAllConnectedPoints();
                    (*beginningBlock)->addConnectedPoints(anotherVector.getPointID());

                    return returnPoint;
                }


                pointToLineTransactionMap.insert(anotherVector.getPointID(),anotherLine);
                lineTransactionMap.append(anotherLine);

                //如果疑似挨着，但是又不是很确定,逻辑肯定不对，需要扩展，应该是疑似要是找不到还可以退回来操作
                if(5<rating && rating<=8){
                    // qDebug() <<"Not comfirmed point:"<<anotherVector.getPointID();
                    FAS_Entity* anotherBlock = findConnectedBlock(anotherVector.getPointID());
                    anotherBlock->beginRelatedPointsTransaction(anotherVector.getPointID());
                    blockTransactionMap.append(anotherBlock);
                    findNextPoint(beginningBlock,&nullBlock,anotherVector,nullptr);
                }

                if(rating==5){
                    // qDebug() <<"Calculate when rating is 5";


                    FAS_Entity* anotherBlock = findConnectedBlock(anotherVector.getPointID());

                    // qDebug() <<"Begin calculate when block is not null";
                    //如果点是属于块的一部分，而且点和块没有关系，很大可能是线需要跨过块
                    //最有可能是另一点所在的线和这条线的角度接近360度
                    if(anotherBlock!=nullptr){
                        // qDebug() <<"Connect line beside the block";
                        QList<QString> blockRelatedPoints=anotherBlock->getRelatedPoints();
                        // qDebug() <<"Block related points:"<< blockRelatedPoints;
                        //此时的块应该至少有一个点是连子节点的，可能有一个点连父节点，再加上成对的两个跨块点

                        double templineAngle1=FAS_MAXDOUBLE;
                        FAS_Vector tempRelatedVector;
                        bool isFound=false;

                        if(blockRelatedPoints.size()>=3){

                            //如果是两点相连的情况
                            int pointNum=0;
                            for(QString blockRelatedPoint:blockRelatedPoints){
                                if(anotherVector.getPointID()==blockRelatedPoint){
                                    pointNum++;
                                }
                            }
                            if(pointNum>=2){
                                anotherBlock->beginRelatedPointsTransaction(anotherVector.getPointID());
                                blockTransactionMap.append(anotherBlock);
                                findNextPoint(beginningBlock,&nullBlock,anotherVector,nullptr);

                            }
                            if(pointNum==1){
                                for(QString blockRelatedPoint:blockRelatedPoints){
                                    // qDebug() <<"Process the point "<<blockRelatedPoint;
                                    if(anotherVector.getPointID()==blockRelatedPoint){
                                        // qDebug() <<"Ignore self point ";
                                        continue;
                                    }
                                    //距离不能是挨着
                                    FAS_Vector blockRelatedVector=getVectorFromPoint(blockRelatedPoint);
                                    double distance=anotherBlock->getDistanceToPoint(blockRelatedVector,nullptr,FAS2::ResolveNone,0);
                                    // qDebug() <<"point to block distance is "<<distance;
                                    if(distance>1e+1){
                                        double newlineAngle1=blockRelatedVector.angleBetween(getVectorFromPoint(anotherLine->getStartpoint().getPointID()),getVectorFromPoint(anotherLine->getEndpoint().getPointID()));
                                        // qDebug() <<"point to line angle is "<<newlineAngle1;
                                        //如果两个线的角度相同，暂定假设他们会连续
                                        if(abs(newlineAngle1)-6.1>0){
                                            if(templineAngle1>6.28-abs(newlineAngle1)){
                                                templineAngle1=6.28-abs(newlineAngle1);
                                                tempRelatedVector=blockRelatedVector;
                                                isFound=true;
                                            }
                                        }else if(0.18-abs(newlineAngle1)>0){
                                            if(templineAngle1>abs(newlineAngle1)){
                                                templineAngle1=abs(newlineAngle1);
                                                tempRelatedVector=blockRelatedVector;
                                                isFound=true;
                                            }
                                        }
                                    }

                                }

                                if(isFound){
                                    // qDebug() <<"find point at the another side of block "<< tempRelatedVector.getPointID();
                                    anotherBlock->beginRelatedPointsTransaction(anotherVector.getPointID());
                                    anotherBlock->beginRelatedPointsTransaction(tempRelatedVector.getPointID());
                                    blockTransactionMap.append(anotherBlock);
                                    findNextPoint(beginningBlock,&nullBlock,tempRelatedVector,nullptr);
                                }
                            }
                        }
                        // qDebug() <<"End connect line beside the block";
                    }
                }else{
                    // qDebug() <<"Reset when line number is 1. related PointID-AnotherPointID"<<point.getPointID()<<"-"<<anotherVector.getPointID();
                    findNextPoint(beginningBlock,&nullBlock,anotherVector,anotherLine);
                    // qDebug() <<"End find point when line number is 1";
                }
            }
        }
        //如果是两条直线相连， 找到另一点
        if(2==pointRelatedLines.size()){
            // qDebug() <<"Begin find point when line number is 2";
            for(FAS_Entity* anotherLine:pointRelatedLines){

                //                    FAS_Vector anotherVector=getAnotherVecorFromPointToLineEntitiesMap(pointID,anotherLine);
                //                    if(anotherLine->getId()==oneLine->getId()){
                //                        removeLineFromPointToLineEntitiesMap(pointID,anotherLine);
                //                        return anotherVector;
                ////                        findNextPoint(beginningBlock,&block,point,anotherLine);
                //                        break;
                //                    }

                //                    rating=getPointAndNearBlockPositionRating(anotherVector);

                //                    if(rating>5){
                //                        return anotherVector;
                //                        break;
                //                    }

                //                    removeLineFromPointToLineEntitiesMap(anotherVector.getPointID(),anotherLine);
                //                    findNextPoint(&block,anotherVector,anotherLine);

            }
            // qDebug() <<"End find point when line number is 2";
            //正常情况一定是有一条线，如果是有两条，则很大的可能是其中有一条线不需要

        }

        //如果没有相连的点，也不是块的点，就找出此点附近所有的点，找出来一个和此点最疑似相连的点
        //1. 如果找出来的点和原来的线在一条直线上，则为最疑似的点
        if(0==pointRelatedLines.size()){
            //判断是否是桥接点
            auto bridgePointIt=bridgePointMap.find(pointID);
            if(bridgePointIt!=bridgePointMap.end()){
                // qDebug() <<"Begin recal when related line number is 0, and point is bridge point";
                isGroupTransaction=true;
                // qDebug() <<"Group Transaction,Will rollback all removed line data";
                findNextPoint(beginningBlock,&nullBlock,getVectorFromPoint(bridgePointIt.value()),nullptr);
            }else{
                // qDebug() <<"Begin when related line number is 0, find the point by matrix";
                QSet<QString> matrixs=getPointMatrixs(pointID,1,1);
                if(oneLine){
                    // qDebug() <<"connected line number is :" << oneLine->getId();
                    FAS_Vector startVector=oneLine->getStartpoint();
                    FAS_Vector endVector=oneLine->getEndpoint();
                    int startX=startVector.x;
                    int startY=startVector.y;
                    int endX=endVector.x;
                    int endY=endVector.y;
                    if(abs(startX-endX)<100){
                        // qDebug() <<"get matrix from Y";
                        matrixs=getPointMatrixs(pointID,0,10);
                    }else if(abs(startY-endY)<100){
                        // qDebug() <<"get matrix from X";
                        matrixs=getPointMatrixs(pointID,10,0);
                    }
                }
                // qDebug() <<"nearby matrix IDs "<<matrixs;
                QList<QString> nearbyPoints;
                nearbyPoints=getPointsFromMatrixs(matrixs);
                if(oneLine!=nullptr){
                    nearbyPoints.removeOne(oneLine->getStartpoint().getPointID());
                    nearbyPoints.removeOne(oneLine->getEndpoint().getPointID());
                }

                // qDebug() <<"nearby matrix points "<<nearbyPoints;

                //如果附件没有任何点，说明这是一条无效线,返回空对象
                if(nearbyPoints.isEmpty()){
                    // qDebug() <<"didn't find any nearby point for this line, remove point from block";
                    //返回空数据
                    return returnPoint;
                }else if(nearbyPoints.size()==1){
                    QString nearbyPoint=nearbyPoints.value(0);
                    // qDebug() <<"re-calculate point from nearbyPoints number is 1 " << nearbyPoint;
                    findNextPoint(beginningBlock,&nullBlock,getVectorFromPoint(nearbyPoint),nullptr);
                    //return getVectorFromPoint(nearbyPoints.value(0));
                }else{

                    // qDebug() <<"find nearbyPoints number is great than 1 ";

                    if(oneLine==nullptr){
                        // qDebug() <<"line is null";
                        return returnPoint;
                    }
                    bool isAngleVector=false;


                    //如果角度没有相同的，假定离它最近的点为相交点
                    double comparedDistance=FAS_MAXDOUBLE;
                    double nearbylineAngle=FAS_MINDOUBLE;
                    FAS_Vector angleVector;
                    FAS_Vector distanceVector;
                    for(QString nearbyPoint:nearbyPoints){
                        FAS_Vector tempNearbyPointVector=getVectorFromPoint(nearbyPoint);
                        double nearbylineDistance=tempNearbyPointVector.distanceTo(getVectorFromPoint(pointID));

                        double tempNearbylineAngle=tempNearbyPointVector.angleBetween(getVectorFromPoint(oneLine->getStartpoint().getPointID()),getVectorFromPoint(oneLine->getEndpoint().getPointID()));
                        //排除在同一侧的点
                        // qDebug() <<"nearbyPointVector and nearbylineAngle1:"<<tempNearbyPointVector.getPointID() <<oneLine->getId() << tempNearbylineAngle;

                        //如果两个线的角度相同，暂定假设他们会连续
                        //if(abs(nearbylineAngle)>6.1||abs(nearbylineAngle)<0.18){
                        if(abs(tempNearbylineAngle)>6.1){
                            // qDebug() <<"Reset point from nearbylineAngle)>6.1 ";

                            if(abs(6.28-tempNearbylineAngle)<abs(nearbylineAngle)){
                                nearbylineAngle=6.28-tempNearbylineAngle;
                                comparedDistance=nearbylineDistance;
                                angleVector=tempNearbyPointVector;
                            }else if(abs(6.28-tempNearbylineAngle)==abs(nearbylineAngle)){
                                if(comparedDistance>nearbylineDistance){
                                    angleVector=tempNearbyPointVector;
                                    // qDebug() <<"nearbyPointVector and distance:"<<tempNearbyPointVector.getPointID() << comparedDistance;

                                }

                            }


                            isAngleVector=true;

                        }else if(abs(tempNearbylineAngle)<0.18){
                            // qDebug() <<"Reset point from nearbylineAngle)<0.18 ";

                            if(abs(tempNearbylineAngle)<abs(nearbylineAngle)){
                                nearbylineAngle=tempNearbylineAngle;
                                comparedDistance=nearbylineDistance;
                                angleVector=tempNearbyPointVector;
                            }else if(abs(tempNearbylineAngle)==abs(nearbylineAngle)){
                                if(comparedDistance>nearbylineDistance){
                                    angleVector=tempNearbyPointVector;
                                    // qDebug() <<"nearbyPointVector and distance:"<<tempNearbyPointVector.getPointID() << comparedDistance;

                                }

                            }


                            isAngleVector=true;
                        }

                        //如果两个线的角度相同，暂定假设他们会连续
                        if(comparedDistance>nearbylineDistance){
                            comparedDistance=nearbylineDistance;
                            distanceVector=tempNearbyPointVector;
                            // qDebug() <<"nearbyPointVector and distance:"<<tempNearbyPointVector.getPointID() << comparedDistance;

                        }
                    }

                    if(isAngleVector){
                        returnPoint=angleVector;
                    }else{

                        if(comparedDistance>100){
                            return returnPoint;
                        }
                        returnPoint=distanceVector;
                    }

                    // qDebug() <<"re-calculate point from nearbyPoint angle/distance condition";
                    findNextPoint(beginningBlock,&nullBlock,returnPoint,nullptr);


                }

                // qDebug() <<"End when related line number is 0, find the point by matrix";
            }
        }
    }

    bool FASUtils::submitTransaction(){
        if(blockTransactionMap.size()>0){
            foreach(FAS_Entity* enty,blockTransactionMap){
                enty->submitRelatedPointsTransaction();
            }

        }

        lineTransactionMap.clear();
        pointToLineTransactionMap.clear();
        return true;
    }
    bool FASUtils::rollbackTransaction(){
        if(blockTransactionMap.size()>0){
            foreach(FAS_Entity* enty,blockTransactionMap){
                enty->rollbackRelatedPointsTransaction();
            }

        }

        if(!pointToLineTransactionMap.isEmpty()){
            QList<QString> keys=pointToLineTransactionMap.keys();
            for(QString key:keys){

                QList<FAS_Entity*> values=pointToLineTransactionMap.values(key);
                QList<FAS_Entity*> lines = pointToLineEntitiesMap.value(key);
                for(FAS_Entity* value: values){

                    if(lines.size()>0 ){
                        foreach(FAS_Entity* line,lines){
                            if(line->getId()==value->getId()){
                                break;
                            }
                        }
                        lines.append(value);
                        pointToLineEntitiesMap.remove(key);
                        pointToLineEntitiesMap.insert(key,lines);

                    }else{
                        lines.append(value);
                        pointToLineEntitiesMap.insert(key,lines);
                    }

                }
            }
        }
        lineTransactionMap.clear();
        pointToLineTransactionMap.clear();
        return true;
    }

    FAS_Entity* FASUtils::findConnectedBlock(QString pointID){
        // qDebug() <<"Find point related block start";
        auto anotherBlockIt = pointToBlockMap.find(pointID);

        FAS_Entity* anotherBlock = nullptr;
        if(anotherBlockIt == pointToBlockMap.end()){
            // qDebug() <<"Find point related block end" << " no block";
            return anotherBlock;
        }

        QList<FAS_Entity*> blocks=pointToBlockMap.values(pointID);
        // qDebug() <<"related blocks is" <<  blocks;
        if(blocks.size()==1){
        return blocks.value(0);
        }else if(blocks.size()==2){
            FAS_Entity* block1=blocks.value(0);
            FAS_Entity* block2=blocks.value(1);
            if(block1->getInsert()->getId()==block2->getInsert()->getId()){
                return block1;
            }
            double distanceBlock1=block1->getDistanceToPoint(getVectorFromPoint(pointID),nullptr,FAS2::ResolveNone,0);
            // qDebug() <<"block1 distance is" << block1->getInsert()->getId() << distanceBlock1;
            double distanceBlock2=block2->getDistanceToPoint(getVectorFromPoint(pointID),nullptr,FAS2::ResolveNone,0);
            // qDebug() <<"block2 distance is" << block2->getInsert()->getId() << distanceBlock2;
            //如果有一个块离点更近，则返回这个块
            if(distanceBlock1>1e+1&&distanceBlock2<1e+1){
                block1->removeMayRelatedPoints(pointID);
                anotherBlock=block2;
            }

            else if(distanceBlock2>1e+1&&distanceBlock1<1e+1){
                block2->removeMayRelatedPoints(pointID);
                anotherBlock= block1;
            }


            //如果两个块都很近,最有可能的是当前块只有两个相连点
            else if(distanceBlock2<1e+1&&distanceBlock1<1e+1){

                int sizeBlock1=block1->getRelatedPoints().size();
                // qDebug() <<"block1 related points size" <<  sizeBlock1;
                int sizeBlock2=block2->getRelatedPoints().size();
                // qDebug() <<"block2 related points size" <<  sizeBlock1;

                if(sizeBlock1==2){
                    block2->removeMayRelatedPoints(pointID);
                    anotherBlock= block1;
                }else if(sizeBlock2==2){
                    block1->removeMayRelatedPoints(pointID);
                    anotherBlock= block2;
                }else{
                    if(distanceBlock1>distanceBlock2){
                        block1->removeMayRelatedPoints(pointID);
                        anotherBlock= block2;
                    }else if(distanceBlock1<distanceBlock2){
                        block2->removeMayRelatedPoints(pointID);
                        anotherBlock= block1;
                    }else{
                        // qDebug() <<"Same distance for 2 bolocks,need to be updated";
                        block2->removeMayRelatedPoints(pointID);
                        anotherBlock= block1;
                    }
                }
            }

            //如果两个块都很远,选一个比较近的
            else if(distanceBlock2>1e+1&&distanceBlock1>1e+1){

                if(distanceBlock1>=distanceBlock2){
                    block1->removeMayRelatedPoints(pointID);
                    anotherBlock= block2;
                }

                else if(distanceBlock1<distanceBlock2){
                    block2->removeMayRelatedPoints(pointID);
                    anotherBlock= block1;
                }
            }



        }else{
            double distanceBlock=FAS_MAXDOUBLE;
            for(FAS_Entity* block : blocks){
                double tempDistanceBlock=block->getDistanceToPoint(getVectorFromPoint(pointID),nullptr,FAS2::ResolveNone,0);
                // qDebug() <<"block distance is" << block->getInsert()->getId() << tempDistanceBlock;
                if(tempDistanceBlock<distanceBlock){
                    distanceBlock=tempDistanceBlock;
                    anotherBlock=block;
                }
            }

            // qDebug() <<"Find block is" << anotherBlock->getInsert()->getId();


        }

        // qDebug() <<"Find point related block end" << anotherBlock->getInsert()->getId();
        return anotherBlock;


    }
    FAS_Vector FASUtils::getVectorFromPoint(QString point){
        QList<QString> pointXY=point.split("_");
        FAS_Vector vector=FAS_Vector(pointXY.value(0).toDouble(nullptr),pointXY.value(1).toDouble(nullptr));
        return vector;
    }

    void FASUtils::markDeviceChildrenBranch(FAS_Entity* block){
        if(!block->isContainer()){
            return;
        }
        block->setSelected(true);
        if(block->isLeaf()){
            return;
        }
        QList<FAS_Entity*> childBlocks=block->getCalChilds();
        foreach(FAS_Entity* childBlock,childBlocks){
            markDeviceChildrenBranch(childBlock);
        }
    }


// Build the map between code and entity.
void FASUtils::buildCodeToDeviceMap()
{
    if(nullptr == curDocument)
        return;
    codeToDeviceMap.clear();
    // 1. traverse all device, find nearest code entity.
    QList<FAS_Insert*> allInsertEntities = FASUtils::getAllInsertEntities(curDocument);
    QHash<QString, FAS_Entity *> codeEntityMap = this->getAllCodeEntityHash();
    QList<FAS_Entity *> codeEntList = codeEntityMap.values();
    for(FAS_Insert* entity : allInsertEntities)
    {
        QString code = entity->getDeviceInfo()->getCodeInfo();
        bool entityExist = (codeEntityMap.find(code) != codeEntityMap.end());
        if(!entityExist)
        {
            findCodeForDevice(entity, codeEntList, code);
        }
        codeToDeviceMap.insert(code, entity);
    }
    //2. Filter: If one code is assigned to several device,
    //           then found the nearest device with the code and remove other maps.
    QSet<QString> allCodeStr = codeToDeviceMap.keys().toSet();
    for(QString oneCode : allCodeStr)
    {
        //If the code is not found, ignore this.
        if(oneCode == COMMONDEF->getNotFoundStr())
            continue;
        FAS_Entity* codeEntity = codeEntityMap.value(oneCode);
        if(nullptr == codeEntity)//no code entity, should not happen.
            continue;
        FAS_Vector codeEntityPoint = codeEntity->getMiddlePoint();
        QList<FAS_Entity*> devicesToFilter = codeToDeviceMap.values(oneCode);
        if(devicesToFilter.size() < 2)//Just one device to oneCode, no need to filter.
            continue;
        double minDist = FAS_MAXDOUBLE;
        FAS_Entity* resultDevice = nullptr;
        for(FAS_Entity* oneDevice : devicesToFilter)
        {
            FAS_Vector devicePoint = oneDevice->getMiddlePoint();
            double tempDist = devicePoint.distanceTo(codeEntityPoint);
            if(tempDist < minDist)
            {
                resultDevice = oneDevice;
                minDist = tempDist;
            }
        }
        if(nullptr == resultDevice)
            continue;
        codeToDeviceMap.remove(oneCode);
        //insert map between the code and nearest device.
        codeToDeviceMap.insert(oneCode, resultDevice);
        for(FAS_Entity* oneDevice : devicesToFilter)
        {
            if(oneDevice != resultDevice)
            {
                //For other device, assign "Not Found" code.
                codeToDeviceMap.insert(COMMONDEF->getNotFoundStr(), oneDevice);
            }
        }
    }
    //3. Set the attribute of deviceInfo->codeInfo.
    for(auto oneMap = codeToDeviceMap.begin(); oneMap != codeToDeviceMap.end(); ++oneMap)
    {
        FAS_Entity* device = oneMap.value();
        device->getInsert()->getDeviceInfo()->setCodeInfo(oneMap.key());
    }
}

QList<QString> FASUtils::getAllDevicesFromDB()
{
    QList<QString> allDeviceCodes;
    CadSqlite* mySql = new CadSqlite();
    QString strDbFile = COMMONDEF->getDatabaseName();
    mySql->openDataBase(strDbFile);

    QSqlDatabase datebase;
    QSqlQuery query(datebase);
    QString str = QString("select * from device");
    query.exec(str);
    while(query.next())
    {
        allDeviceCodes.append(query.value(3).toString());
    }
    mySql->closeDataBase();
    return allDeviceCodes;
}

QList<FAS_Insert*> FASUtils::getAllDevicesFromRS()
{
    QList<FAS_Insert*> allDeviceEntities;
    if(nullptr == curDocument)
        return allDeviceEntities;
    QList<FAS_Insert*> insertEntities = FASUtils::getAllInsertEntities(curDocument);
    for(FAS_Insert* oneEntity : insertEntities)
    {
        if(oneEntity->getDeviceInfo()->isValidDevice())
        {
            allDeviceEntities.append(oneEntity);
        }
    }
    return allDeviceEntities;
}

QList<QString> FASUtils::getDevicesByTypeFromDB(DeviceInfo* detectType)
{
    QList<QString> result;
    CadSqlite* mySql = new CadSqlite();
    QString strDbFile = COMMONDEF->getDatabaseName();
    mySql->openDataBase(strDbFile);

    QSqlDatabase datebase;
    QSqlQuery query(datebase);
    QString typeStr = detectType->getName();
    QString str = QString("select * from device where deviceInfo='%1'").arg(typeStr);
    query.exec(str);
    while(query.next())
    {
        result.append(query.value(3).toString());
    }
    mySql->closeDataBase();
    return result;
}

void FASUtils::showEntityList(QList<FAS_Insert*>& input, bool show)
{
    if(input.empty() || nullptr == qg_graphicView)
    {
        return;
    }
    for(FAS_Insert* entity : input)
    {
        entity->setVisible(show);
    }
    qg_graphicView->redraw(FAS2::RedrawDrawing);
}

QList<FAS_Insert*> FASUtils::getDevicesByTypeFromRS(DeviceInfo* detectType)
{
    QList<FAS_Insert*> resultEntities;

    if(nullptr == curDocument)
        return resultEntities;
    QList<FAS_Insert*> allDeviceEntities = FASUtils::getAllInsertEntities(curDocument);
    for(FAS_Insert* entity : allDeviceEntities)
    {
        if(detectType->getID() == entity->getDeviceInfo()->getID())
        {
            resultEntities.append(entity);
        }
    }
    return resultEntities;
}

int FASUtils::showAllDevices(bool show)
{
    int error = 0;
    QList<FAS_Insert*> allDevices = getAllDevicesFromRS();
    showEntityList(allDevices, show);
    return error;
}

int FASUtils::showDeviceByCode(QString codeInfo, bool show)
{
    int error = 0;
    auto result = codeToDeviceMap.find(codeInfo);
    if(result != codeToDeviceMap.end())
    {
        FAS_Entity* entity = result.value();
        entity->setVisible(show);
        qg_graphicView->redraw(FAS2::RedrawDrawing);
    }
    return error;
}

int FASUtils::showDeviceByCodeList(QList<QString>& devicesToShow, bool show)
{
    int error = 0;
    QList<FAS_Insert*> entities;
    for(QString oneDevice : devicesToShow)
    {
        auto result = codeToDeviceMap.find(oneDevice);
        if(result != codeToDeviceMap.end())
        {
            FAS_Entity* entity = result.value();\
            entities.append((FAS_Insert*)entity);
        }
    }
    showEntityList(entities, show);
    return error;
}

int FASUtils::showDevicesByType(DeviceInfo* deviceInfo, bool show)
{
    int error = 0;
    // The following code work more robustness.
    QList<FAS_Insert*> entities = getDevicesByTypeFromRS(deviceInfo);
    showEntityList(entities, show);
    return error;
}

void FASUtils::showEntitiesByLayer(QString layerName, bool show)
{
    if(nullptr == curDocument)
        return;

    QList<FAS_Entity*> entityList = curDocument->getEntityList();
    for(FAS_Entity* oneEntity : entityList)
    {
        if(oneEntity->getLayer() && oneEntity->getLayer()->getName() == layerName)
        {
            oneEntity->setVisible(show);
        }
    }
    qg_graphicView->redraw(FAS2::RedrawDrawing);
}

void FASUtils::twinkleEntity(QString code, bool twinkle)
{
    if(twinkle)
    {
        int ms = 500;
        twinkleTimer->start(ms);

        connect(twinkleTimer, SIGNAL(timeout()), this, SLOT(slotTwinkleEntityList()));
        twinkleEntityList.clear();
        twinkleEntityList.append(code);
    }
    else
    {
        twinkleTimer->stop();
        disconnect(twinkleTimer, SIGNAL(timeout()), this, SLOT(slotTwinkleEntityList()));
        showDeviceByCodeList(twinkleEntityList, true);
        twinkleEntityList.clear();
    }
}

void FASUtils::slotTwinkleEntityList()
{
    if(twinkleEntityList.empty())
        return;
    static bool show = true;
    show = !show;
    showDeviceByCodeList(twinkleEntityList, show);
}

bool FASUtils::findCodeForDevice(FAS_Insert* device, QList<FAS_Entity*>& codeEntityList, QString& codeText)
{
    bool found = false;

    if(nullptr == device || codeEntityList.empty())
    {
        found = true;
        codeText = COMMONDEF->getNotFoundStr();
        return found;
    }

    double deviceSize = device->getMax().distanceTo(device->getMin());
    double rangeSize = deviceSize * 1.5;

    FAS_Vector coord = device->getMiddlePoint();
    double minDist = FAS_MAXDOUBLE;
    for (FAS_Entity* codeEnt : codeEntityList)
    {
        FAS_Vector textPoint = codeEnt->getMiddlePoint();
        double temp = textPoint.distanceTo(coord);
        if (temp < rangeSize && temp < minDist)
        {
            minDist = temp;
            found = true;
            if(FAS2::EntityText == codeEnt->rtti())
            {
                codeText = ((FAS_Text*)codeEnt)->getText();
            }
            else if(FAS2::EntityMText == codeEnt->rtti())
            {
                codeText = ((FAS_MText*)codeEnt)->getText();
            }
        }
    }
    if(!found)
    {
        codeText = COMMONDEF->getNotFoundStr();
        found = true;
    }
    return found;
}

//Get entities in blick, including circle, line, arc, hatch, solid.
QList<QList<FAS_Entity*>> FASUtils::getNeededEntityList(FAS_EntityContainer *container)
{
    QList<QList<FAS_Entity*>> result;
    QList<FAS_Entity*> circles, lines, arcs, hatch, solids, texts, points;
    QList<FAS_Entity *> enList = container->getEntityList();
    double tolerance = COMMONDEF->getTolerance();
    for(FAS_Entity* oneEntity : enList)
    {
        if(!oneEntity->isVisible())
            continue;
        QList<QList<FAS_Entity*>> tempResult;
        FAS_Vector startPoint, endPoint;
        switch (oneEntity->rtti())
        {
        case FAS2::EntityEllipse:
        case FAS2::EntityCircle:
            circles.append(oneEntity);
            break;
        //case FAS2::EntitySpline:
        case FAS2::EntityLine:
            startPoint = oneEntity->getStartpoint();
            endPoint = oneEntity->getEndpoint();
            if(startPoint.distanceTo(endPoint) > tolerance)
            {
                lines.append(oneEntity);
            }
            break;
        case FAS2::EntityArc:
            arcs.append(oneEntity);
            break;
        case FAS2::EntityHatch:
            hatch.append(oneEntity);
            break;
        case FAS2::EntitySolid:
            solids.append(oneEntity);
            break;
        case FAS2::EntityPolyline:
        case FAS2::EntityInsert:
            tempResult = getNeededEntityList((FAS_EntityContainer*)oneEntity);
//            circles.append(tempResult.at(0));
//            lines.append(tempResult.at(1));
//            arcs.append(tempResult.at(2));
//            hatch.append(tempResult.at(3));
//            solids.append(tempResult.at(4));
//            texts.append(tempResult.at(5));
//            points.append(tempResult.at(6));
            break;
        case FAS2::EntityText:
        case FAS2::EntityMText:
            texts.append(oneEntity);
            break;
        case FAS2::EntityPoint:
        //case FAS2::EntitySplinePoints:
            points.append(oneEntity);
            break;
        default:
            break;
        }
    }
    result.append(circles);
    result.append(lines);
    result.append(arcs);
    result.append(hatch);
    result.append(solids);
    result.append(texts);
    result.append(points);
    return result;
}

//Get entities in block, including circle, line, arc, hatch, solid.
QList<QList<FAS_Entity*>> FASUtils::getEntityListForImage(FAS_EntityContainer* &container)
{
    QList<QList<FAS_Entity*>> result;
    QList<FAS_Entity*> circles, lines, arcs, hatch, solids, texts, points;
    QList<FAS_Entity *> enList = container->getEntityList();

    double tolerance = COMMONDEF->getTolerance();
    for(FAS_Entity* &oneEntity : enList)
    {
        if(!oneEntity->isVisible())
            continue;
        QList<QList<FAS_Entity*>> tempResult;
        FAS_Vector startPoint, endPoint;
        switch (oneEntity->rtti())
        {
        case FAS2::EntityEllipse:
        case FAS2::EntityCircle:
            circles.append(oneEntity);
            break;
        //case FAS2::EntitySpline:
        case FAS2::EntityLine:
            startPoint = oneEntity->getStartpoint();
            endPoint = oneEntity->getEndpoint();
            if(startPoint.distanceTo(endPoint) > tolerance)
            {
                lines.append(oneEntity);
            }
            break;
        case FAS2::EntityArc:
            arcs.append(oneEntity);
            break;
        case FAS2::EntityHatch:
            hatch.append(oneEntity);
            break;
        case FAS2::EntitySolid:
            solids.append(oneEntity);
            break;
        case FAS2::EntityPolyline:
        case FAS2::EntityInsert:
            tempResult = getNeededEntityList((FAS_EntityContainer*)oneEntity);
            circles.append(tempResult.at(0));
            lines.append(tempResult.at(1));
            arcs.append(tempResult.at(2));
            hatch.append(tempResult.at(3));
            solids.append(tempResult.at(4));
            texts.append(tempResult.at(5));
            points.append(tempResult.at(6));
            break;
        case FAS2::EntityText:
        case FAS2::EntityMText:
            texts.append(oneEntity);
            break;
        case FAS2::EntityPoint:
        //case FAS2::EntitySplinePoints:
            points.append(oneEntity);
            break;
        default:
            break;
        }
    }
    result.append(circles);
    result.append(lines);
    result.append(arcs);
    result.append(hatch);
    result.append(solids);
    result.append(texts);
    result.append(points);
    return result;
}

//Get atomic entities out of blocks in document, including circle, line, arc, hatch, point, text and others
QList<QList<FAS_Entity*>> FASUtils::getExistAtomicEntityList(FAS_EntityContainer *container)
{
    QList<QList<FAS_Entity*>> result;
    QList<FAS_Entity*> circles, lines, arcs, hatch, solids, texts;
    QList<FAS_Entity *> enList = container->getEntityList();
    double tolerance = COMMONDEF->getTolerance();
    for(FAS_Entity* oneEntity : enList)
    {
        if(!oneEntity->isVisible())
            continue;
        QList<QList<FAS_Entity*>> tempResult;
        FAS_Vector startPoint, endPoint;
        switch (oneEntity->rtti())
        {
        case FAS2::EntityCircle:
            circles.append(oneEntity);
            break;
        case FAS2::EntityLine:
            startPoint = oneEntity->getStartpoint();
            endPoint = oneEntity->getEndpoint();
            if(startPoint.distanceTo(endPoint) > tolerance)
            {
                lines.append(oneEntity);
            }
            break;
        case FAS2::EntityArc:
            arcs.append(oneEntity);
            break;
        case FAS2::EntityHatch:
            hatch.append(oneEntity);
            break;
        case FAS2::EntitySolid:
            solids.append(oneEntity);
            break;
        case FAS2::EntityPolyline:
            startPoint = oneEntity->getStartpoint();
            endPoint = oneEntity->getEndpoint();
            if(startPoint.distanceTo(endPoint) > tolerance)
            {
                lines.append(oneEntity);
            }
            break;
        case FAS2::EntitySpline:
            startPoint = oneEntity->getStartpoint();
            endPoint = oneEntity->getEndpoint();
            if(startPoint.distanceTo(endPoint) > tolerance)
            {
                lines.append(oneEntity);
            }
            break;
        case FAS2::EntityInsert:
        case FAS2::EntityText:
            texts.append(oneEntity);
            break;
        case FAS2::EntityMText:
            texts.append(oneEntity);
            break;
        default:
            break;
        }
    }
    result.append(circles);
    result.append(lines);
    result.append(arcs);
    result.append(hatch);
    result.append(solids);
    result.append(texts);

    return result;
}




//Get counts of entities in block, including circle, line, arc, hatch, point and others.
DeviceFeature FASUtils::getDeviceFeature(FAS_EntityContainer* container)
{
    DeviceFeature result;
    QList<QList<FAS_Entity*>> entities = getNeededEntityList(container);
    result.circles = entities.at(0).size();
    result.lines = entities.at(1).size();
    result.arcs = entities.at(2).size();
    result.hatchs = entities.at(3).size();
    result.solids = entities.at(4).size();
    result.size = FASUtils::getEntitySize(container);
    for(FAS_Entity* entity : entities.at(5))
    {
        QString tempText = "";
        if(entity->rtti() == FAS2::EntityText)
            tempText = ((FAS_Text*)entity)->getText();
        else if(entity->rtti() == FAS2::EntityMText)
            tempText = ((FAS_MText*)entity)->getText();
        result.text += tempText;
    }

    return result;
}

// Calculate the size of block.
// Which is defined as length of all lines, arcs and circles.
// Here, the length is calculated as relative length.
double FASUtils::getEntitySize(FAS_EntityContainer* block)
{
    double result = 0;
    QList<QList<FAS_Entity*>> entities = getNeededEntityList(block);
    QList<FAS_Entity*> entitiesNeeded;
    QList<double> lengthList;
    double maxLength = FAS_MINDOUBLE;
    //Get all entities with type Circle, Line and Arc.
    for(int ii = 0; ii < 3; ii++)
    {
        entitiesNeeded.append(entities.at(ii));
    }
    for(FAS_Entity* oneEntity : entitiesNeeded)
    {
        double length = oneEntity->getLength();
        lengthList.append(length);
        if(maxLength < length)
        {
            maxLength = length;
        }
    }
    for(double length : lengthList)
    {
        result += length/maxLength;
    }
    return result;
}

// Calculate the valid scope of block.
// Which is defined as length & height of atomic entities.

// Detect the type of input device, detectList can be obtained from getNeededEntityCount()
// and entitySize can be obtained from getEntitySize().
DeviceInfo* FASUtils::detectDeviceType(DeviceFeature inputFeature)
{
    DeviceInfo* resultType = COMMONDEF->getDefaultType();
    for(DetectionRule rule : this->detectionRule)
    {
        if(rule.systemType.compare(COMMONDEF->getSystem(true),Qt::CaseInsensitive)) continue;
        if(rule.feature.circles == inputFeature.circles &&
                rule.feature.lines == inputFeature.lines &&
                rule.feature.arcs == inputFeature.arcs &&
                rule.feature.hatchs == inputFeature.hatchs &&
                rule.feature.solids == inputFeature.solids &&
                rule.feature.text == inputFeature.text)
        {
            if(abs(rule.feature.size - inputFeature.size) < COMMONDEF->getTolerance() ||
                    abs(rule.feature.size - 0.0) < COMMONDEF->getTolerance())
            {
                resultType = COMMONDEF->getDeviceInfoFromName(rule.deviceType);
            }
        }
    }
    return resultType;
}

//Clarify all entities by layer.
void FASUtils::clarifyLayerEntities(QMultiHash<QString, FAS_Entity*>& layerEntityHash, QHash<int, FAS_Entity*>& IDMap)
{
    QList<FAS_Entity*> allEntities = qg_graphicView->getContainer()->getEntityList();
    for(FAS_Entity* oneEntity : allEntities)
    {
        if(!oneEntity->isVisible())
            continue;
        FAS_Layer* layer = oneEntity->getLayer();
        if(layer != nullptr)
        {
            QString layerName = layer->getName();
            layerEntityHash.insert(layerName, oneEntity);
            IDMap.insert(oneEntity->getId(), oneEntity);
            /*oneEntity->setSelected(true);
            FAS_GraphicView* view=oneEntity->getDocument()->getGraphicView();
            view->deleteEntity(oneEntity);
            view->drawEntity(oneEntity);
            if (oneEntity->rtti()==FAS2::EntityBlock)
            {
                oneEntity->getBlock()->setActivePen(FAS_Pen(FAS_Color(0,0,255),FAS2::LineWidth::Width01,FAS2::LineType::SolidLine));
                view->deleteEntity(oneEntity);
                view->drawEntity(oneEntity);
            }
            else
            {
                oneEntity->setPen(FAS_Pen(FAS_Color(0,0,255),FAS2::LineWidth::Width01,FAS2::LineType::SolidLine));
                view->deleteEntity(oneEntity);
                view->drawEntity(oneEntity);
            }*/
        }
    }
}

// Get all entities on given layer.
QList<FAS_Entity*> FASUtils::getEntityListOnLayer(QString layerName, QMultiHash<QString, FAS_Entity*>& layerEntityHash)
{
    QList<FAS_Entity*> result;

    if(layerEntityHash.empty())
    {
        QHash<int, FAS_Entity*> temp;
        clarifyLayerEntities(layerEntityHash, temp);
    }
    result = layerEntityHash.values(layerName);
    return result;
}

static bool IsPointInRectangle(FAS_Vector point, FAS_Vector minV, FAS_Vector maxV)
{
    double tol = COMMONDEF->getTolerance();
    if(point.x >= minV.x - tol && point.x <= maxV.x + tol &&
       point.y >= minV.y - tol && point.y <= maxV.y + tol)
    {
        return true;
    }
    else
    {
        return false;
    }
}

static QList<FAS_Entity*> GetAdjWireEntities(FAS_Entity* entity, QList<FAS_Entity*>& wireList)
{
    FAS_Vector minV = entity->getMin(), maxV = entity->getMax();
    QList<FAS_Entity*> adjWires;
    for(FAS_Entity* oneWire : wireList)
    {
        bool inRectangle = false;
        FAS_Vector wStartPoint = oneWire->getStartpoint();
        FAS_Vector wEndPoint = oneWire->getEndpoint();

        if(IsPointInRectangle(wStartPoint, minV, maxV))
            inRectangle = true;
        else if(IsPointInRectangle(wEndPoint, minV, maxV))
            inRectangle = true;

        if(inRectangle)
        {
            adjWires.append(oneWire);
        }
    }
    return adjWires;
}

static QMultiHash<int, int> BuildAdjacentRelation(QList<FAS_Entity*>& wireList, QList<FAS_Entity*>& devices)
{
    QMultiHash<int, int> relation;
    for(FAS_Entity* oneDevice : devices)
    {
        QList<FAS_Entity*> adjWires = GetAdjWireEntities(oneDevice, wireList);
        for(FAS_Entity* wire : adjWires)
        {
            unsigned long int wireID = wire->getId();
            unsigned long int deviceID = oneDevice->getId();
            relation.insert(wireID, deviceID);
            relation.insert(deviceID, wireID);
        }
    }
    return relation;
}

static void FindAdjacents
(
        FAS_Entity* entity,
        const QMultiHash<int, int>& adjacentRelation,
        const  QHash<int,FAS_Entity*>& IDMap,
        QSet<int>& traversed,
        QList<FAS_Entity*>& loop
)
{
    traversed.insert(entity->getId());
    loop.append(entity);
    QList<int> adjacents = adjacentRelation.values(entity->getId());
    for(int oneAdj : adjacents)
    {
        FAS_Entity* entityAdj = IDMap.value(oneAdj);
        if(traversed.find(oneAdj) == traversed.end())
        {
            FindAdjacents(entityAdj, adjacentRelation, IDMap, traversed, loop);
        }
    }
}

// get all devices from input loop(loop contains devices and wires)
QList<FAS_Insert*> FASUtils:: getDeviceListInLoop(QList<FAS_Entity*> loop)
{
    QList<FAS_Insert*> result;
    if(loop.empty())
        return result;
    for(FAS_Entity* oneEntity : loop)
    {
        if(oneEntity->rtti() == FAS2::EntityInsert && oneEntity->getInsert() != nullptr)
            result.append(oneEntity->getInsert());
    }
    return result;
}

// Get loops. One loop is defined as entities are connected with wire directly.
QList<QList<FAS_Entity*>> FASUtils::detectLoopList(QList<FAS_Entity*>& wireList, QList<FAS_Entity*>& devices, QHash<int, FAS_Entity*>& IDMap)
{
    QList<FAS_Entity*> loop;
    QList<QList<FAS_Entity*>> loops;
    QMultiHash<int, int> adjacentRelation = BuildAdjacentRelation(wireList, devices);

    QSet<int> traversed;
    QList<FAS_Entity*> dNoAdjacent, dOneAdjacent, dMoreAdjacent;
    for(FAS_Entity* oneDevice : devices)
    {
        QList<int> adjacents = adjacentRelation.values(oneDevice->getId());

        if(0 == adjacents.size())
            dNoAdjacent.append(oneDevice);
        else if(1 == adjacents.size())
            dOneAdjacent.append(oneDevice);
        else
            dMoreAdjacent.append(oneDevice);
    }
    //Detect loop from the device in the end.
    for(FAS_Entity* oneDevice : dOneAdjacent)
    {
        // If the device has been added to one loop, no need to detect it again.
        if(traversed.find(oneDevice->getId()) != traversed.end())
            continue;

        loop.clear();
        FindAdjacents(oneDevice, adjacentRelation, IDMap, traversed, loop);
        loops.append(loop);
    }
    for(FAS_Entity* oneDevice : dMoreAdjacent)
    {
        // If the device has been added to one loop, no need to detect it again.
        if(traversed.find(oneDevice->getId()) != traversed.end())
            continue;

        loop.clear();
        FindAdjacents(oneDevice, adjacentRelation, IDMap, traversed, loop);
        loops.append(loop);
    }
    for(FAS_Entity* oneDevice : dNoAdjacent)
    {
        // If the device has been added to one loop, no need to detect it again.
        if(traversed.find(oneDevice->getId()) != traversed.end())
            continue;

        loop.clear();
        FindAdjacents(oneDevice, adjacentRelation, IDMap, traversed, loop);
        loops.append(loop);
    }

    return loops;
}

//Get the upper left point of Insert Block as the insert point of svg/mtext.
FAS_Vector FASUtils::getUpperLeftVector(const FAS_Entity* inputEntity)
{
    FAS_Vector result;
    result.x = inputEntity->getMin().x;
    result.y = inputEntity->getMax().y;
    return result;
}

// Generate code text for devices in loops automatically (sort by type in one loop if sortByType == true).
QHash<QString, FAS_Entity*> FASUtils::generateCodeForLoopDeviceList(QList<QList<FAS_Entity*>>& loops, bool sortByType)
{
    QHash<QString, FAS_Entity*> resultHash;
    QHash<QString, FAS_Entity*> codeEntityHash = getAllCodeEntityHash();
    int loopNo = 0;
    for(QList<FAS_Entity*> oneLoop : loops)
    {
        loopNo++;
        int code = 1;
        QList<FAS_Insert*> devicesToEncode;
        if(sortByType)
        {
            devicesToEncode.append(sortDeviceListByType(oneLoop));
        }
        else
        {
            devicesToEncode = getDeviceListInLoop(oneLoop);
        }
        for(FAS_Insert* oneEntity : devicesToEncode)
        {
            QString oriCode = oneEntity->getDeviceInfo()->getCodeInfo();

            if(!isValidDeviceCode(oriCode) || codeEntityHash.find(oriCode) == codeEntityHash.end())
            {
                QString text = QString("%1-%2").arg(loopNo, 3, 10, QChar('0')).arg(code++, 4, 10, QChar('0'));
                resultHash.insert(text, oneEntity);
                oneEntity->getInsert()->getDeviceInfo()->setCodeInfo(text);
                codeToDeviceMap.insert(text, oneEntity);
                codeToDeviceMap.remove(oriCode, oneEntity);
            }
        }
    }

    //updateDeviceDataBase(codeToDeviceMap);
    return resultHash;
}

// Add code text for devices in graphic view.
void FASUtils::addCodeTextInGraphic(QHash<QString, FAS_Entity*>& codeHash, FAS_Layer* layerToAdd)
{
    double angle = 0.0;
    double lineSpacingFactor = 1.0;
    QString style = "standard";

    QSet<FAS_MText*> rsTextSet;
    for(auto oneMap  = codeHash.begin(); oneMap != codeHash.end(); ++oneMap)
    {
        double height = (oneMap.value()->getMax().y - oneMap.value()->getMin().y) * 0.25;
        double textLength = height * 6;
        double deviceLength = oneMap.value()->getMax().x - oneMap.value()->getMin().x;
        double offsetX = (textLength - deviceLength) * 0.5;
        FAS_Vector insertPoint = getUpperLeftVector((FAS_EntityContainer*)oneMap.value());
        insertPoint.x = insertPoint.x - offsetX;

        double width = height * 8;
        QString text = oneMap.key();
        FAS_MTextData textData(insertPoint, height, width,
                              FAS_MTextData::VABottom, FAS_MTextData::HALeft,
                              FAS_MTextData::LeftToRight, FAS_MTextData::AtLeast,
                              lineSpacingFactor, text, style, angle, FAS2::NoUpdate);

        FAS_MText* rsText = new FAS_MText(qg_graphicView->getContainer(), textData);
        if(layerToAdd != nullptr)
        {
            rsText->setLayer(layerToAdd);
        }
        rsText->update();
        rsTextSet.insert(rsText);
        qg_graphicView->getContainer()->addEntity(rsText);
    }
    // We can not enable undo for this operation. Because we have built the map between device and code.
    if (curDocument)
    {
        curDocument->startUndoCycle();
        for(auto rsText : rsTextSet)
        {
            curDocument->addUndoable(rsText);
        }
        curDocument->endUndoCycle();
    }
    qg_graphicView->redraw(FAS2::RedrawDrawing);
}

QHash<int, QList<FAS_Insert*>> FASUtils::classifyDevicesByType(QList<FAS_Insert*> deviceList)
{
    QHash<int, QList<FAS_Insert*>> typeEntitiesHash;
    for(FAS_Insert* oneDevice : deviceList)
    {
        DeviceInfo* deviceInfo = oneDevice->getDeviceInfo();
        if(typeEntitiesHash.find(deviceInfo->getID()) != typeEntitiesHash.end())
        {
            typeEntitiesHash.find(deviceInfo->getID()).value().append(oneDevice);
        }
        else
        {
            QList<FAS_Insert*> tempList;
            tempList.append(oneDevice);
            typeEntitiesHash.insert(deviceInfo->getID(), tempList);
        }
    }
    return typeEntitiesHash;
}

QList<FAS_Insert*> FASUtils::sortDeviceListByType(QList<FAS_Entity*> entityList)
{
    QList<FAS_Insert*> resultList;
    QList<FAS_Insert*> allDevices;
    for(FAS_Entity* oneEntity : entityList)
    {
        if(oneEntity->rtti() != FAS2::EntityInsert)
            continue;
        FAS_Insert* insert = oneEntity->getInsert();
        allDevices.append(insert);
    }
    QHash<int, QList<FAS_Insert*>> typeEntitiesHash = classifyDevicesByType(allDevices);
    QList<int> typeList = typeEntitiesHash.keys();
    qSort(typeList.begin(), typeList.end());
    for(int type : typeList)
    {
        resultList.append(typeEntitiesHash.value(type));
    }
    return resultList;
}

static FAS_Insert* findLeftTopDevice(QList<FAS_Insert*> deviceList)
{
    FAS_Insert* result;
    FAS_Vector resultPoint(FAS_MAXDOUBLE, FAS_MINDOUBLE);
    if(deviceList.empty())
        return result;

    for(FAS_Insert* oneDevice : deviceList)
    {
        FAS_Vector midPoint = oneDevice->getMiddlePoint();
        if(midPoint.x < resultPoint.x)
        {
            result = oneDevice;
            resultPoint = midPoint;
        }
        else if (abs(midPoint.x - resultPoint.x) < FAS_TOLERANCE)
        {
            if(midPoint.y > resultPoint.y)
            {
                result = oneDevice;
                resultPoint = midPoint;
            }
        }
    }
    return result;
}

//sort entities by coordinate, from left to right, from up to down
QList<FAS_Insert*> FASUtils::sortDeviceListByCoordinate(QList<FAS_Insert*> deviceList)
{
    QList<FAS_Insert*> result;
    while(!deviceList.empty())
    {
        FAS_Insert* leftTop = findLeftTopDevice(deviceList);
        result.append(leftTop);
        deviceList.removeOne(leftTop);
    }
    return result;
}

// Add one new device type to COMMONDEF.
// return 1 if the input name is already exist. return 2 if svg file cannot be copied.
int FASUtils::addNewDeviceType(QString deviceName, QString svgPath)
{
    QList<QString> stringList = COMMONDEF->getDeviceTypeList();
    if(stringList.indexOf(deviceName) > 0)
    {
        return 1; // Error: input device name is same with one of the existing devices.
    }
    QList<DeviceInfo*> deviceInfoList = COMMONDEF->getDeviceInfoList();
    int num = deviceInfoList.size() - 2;
    QString rename = QString::number(num) + "_" + deviceName + ".svg";
    QString newPath = COMMONDEF->getSvgFolder() + "/" + rename;

    if(!QFile::copy(svgPath, newPath))
    {
        return 2; // Error: svg file cannot be copied.
    }
    COMMONDEF->clear();
    COMMONDEF->init();
    return 0; // No error.
}
/*
//Update the device database after generate new code.
void FASUtils::updateDeviceDataBase(QMultiHash<QString, FAS_Entity*>& codeToDeviceMap)
{
    CadSqlite mySql;
    QString strDbFile = COMMONDEF->getDatabaseName();
    mySql.openDataBase(strDbFile);
    mySql.dropDevice();
    mySql.createDeviceTable();

    QString strType;
    FAS_Vector point;
    QString deviceCode;
    QString noteText;
    int insertId = 1;
    for(auto iter = codeToDeviceMap.begin(); iter != codeToDeviceMap.end(); ++iter)
    {
        strType = iter.value()->getInsert()->getDeviceInfo()->getName();
        point = iter.value()->getMiddlePoint();
        deviceCode = iter.key();
        noteText = iter.value()->getInsert()->getDeviceInfo()->getNoteText();
        mySql.addDevice(QString::number(insertId++), QString::number(point.x), QString::number(point.y), deviceCode, strType, noteText);
    }
    mySql.closeDataBase();
}


//Update the device's position after move.
bool FASUtils::updateDevicePostion(FAS_Entity* oriEntity, FAS_Entity* newEntity)
{
    bool result = true;
    if(nullptr == oriEntity || nullptr == newEntity)
        return false;

    FAS_Vector oriPos = oriEntity->getMiddlePoint();
    FAS_Vector newPos = newEntity->getMiddlePoint();
    CadSqlite mySql;
    QString strDbFile = COMMONDEF->getDatabaseName();
    mySql.openDataBase(strDbFile);

    QSqlQuery query(mySql.m_db);

    QString str = QString("SELECT * FROM device WHERE X='%1' and Y='%2'" ).arg(QString::number(oriPos.x)).arg(QString::number(oriPos.y));
    query.exec(str);
    if(query.next())
    {
        int recordId = query.value(0).toInt();
        QString update = QString("update device set X='%1',Y='%2' where Id='%3'" ).arg(QString::number(newPos.x)).arg(QString::number(newPos.y)).arg(QString::number(recordId));
        result = query.exec(update);
    }
    mySql.closeDataBase();
    return result;
}*/

int FASUtils::setEntitySelected(FAS_Entity* en)
{
    int error = 0;
    if (curDocument == nullptr || qg_graphicView == nullptr)
    {
        error = 1;
        return error;
    }

    FAS_Selection s(*curDocument, qg_graphicView);
    s.selectSingle(en);

    return error;
}

bool FASUtils::isValidDeviceCode(QString codeInfo)
{
    bool result = false;
    if(0 == codeInfo.size() || codeInfo == COMMONDEF->getNotFoundStr())
        result = false;
    else if(8 == codeInfo.size() && codeInfo.toInt() != 0)
    {
        result = true;
    }
    else if(codeInfo.contains("-"))
    {
        QList<QString> strList = codeInfo.split("-");
        if(2 == strList.size())
        {
            if((strList.at(0).size() == 2 || strList.at(0).size() == 3) &&
               (strList.at(1).size() == 3 || strList.at(1).size() == 4))
            {
                result = true;
            }
        }
    }
    return result;
}

bool FASUtils::isValidInsertEntity(FAS_Entity* entity)
{
    if(nullptr == entity)
        return false;

    if(entity->rtti() != FAS2::EntityInsert)
        return false;
    else if(entity->getInsert()->getEntityList().empty())
        return false;
    else
        return true;
}

//return the validity of input text entity. It should has valid text and the entity list should not be empty.
// entity list is empty while it is added to undo list.
bool FASUtils::isValidCodeEntity(FAS_Entity* entity, QString& code)
{
    if(nullptr == entity)
        return false;
    if(!entity->isVisible())
        return false;

    QString str;
    if(FAS2::EntityText == entity->rtti())
    {
        str = ((FAS_Text*)entity)->getText();
    }
    else if(FAS2::EntityMText == entity->rtti())
    {
        str = ((FAS_MText*)entity)->getText();
    }
    bool result = FASUtils::isValidDeviceCode(str);
    if(result)
    {
        code = str;
    }
    return result;
}

bool FASUtils::isDuplicateEntityToBeFiltered(FAS_Insert* objToBeChecked, QMultiHash<QString, FAS_Insert*> statisticsHash){
    bool result = false;
    QString blockName = objToBeChecked->getInsert()->getName();
    QList<FAS_Insert*> devicesOfName = statisticsHash.values(blockName);
    foreach(auto b, devicesOfName){
        if(b->getMin()==objToBeChecked->getMin()
                &&b->getMax() == objToBeChecked->getMax()
                &&b->getMiddlePoint() == objToBeChecked->getMiddlePoint()){
            result = true;
            break;
        }
    }
    return result;
}

// Get entity count of given block name in given document list.
QMultiHash<QString, FAS_Insert*> FASUtils::getBlockNameToEntityHash()
{
    QMultiHash<QString, FAS_Insert*> resultHash;
    if(curDocument == nullptr)
    {
        return resultHash;
    }
    // qDebug() << "getAllInsertEntities start";
    QList<FAS_Insert*> entityList = curDocument->getInsertList();
//    QList<FAS_Insert*> entityList = FASUtils::getAllValidInsertEntities(curDocument);
     // qDebug() << "getAllInsertEntities end";
    int i=0;
    int count=entityList.size();
    for(FAS_Insert* entity : entityList)
    {
//        // qDebug() << "Entity Loop start" << i << count;
        if(!entity->isVisible() || isDuplicateEntityToBeFiltered(entity,resultHash)){
            if(!entity->getDeviceInfo()->isRecognized())
                continue;
        }
        QString name = entity->getInsert()->getName();
        resultHash.insert(name, entity);
//        // qDebug() << "Entity Loop" << i << count;
        i++;
    }
    return resultHash;
}

// Get entity count of given block name in given document list.
QList<FAS_Entity*> FASUtils::getEntitiesInBlock(FAS_EntityContainer* myBlock)
{
    QList<FAS_Entity*> result;
    if(myBlock == nullptr)
    {
        return result;
    }

    if(!myBlock->isContainer()){
        return result;
    }

    QList<FAS_Entity*> entities=((FAS_EntityContainer*)myBlock)->getEntityList();

    for(FAS_Entity* e:entities){
        if(e->isContainer()){
            result.append(FASUtils::getEntitiesInBlock((FAS_EntityContainer*)e->getBlockOrInsert()));
        }else{
            result.append(e);
        }
    }
    return result;
}


static FAS_Insert* findOneInsertOfBlock(QString blockName, FAS_Document* document)
{
    FAS_Insert* result = nullptr;
    QList<FAS_Insert*> insertList = FASUtils::getAllInsertEntities(document);
    for(FAS_Insert* insert : insertList)
    {
        if(insert->getName() == blockName)
        {
            result = insert;
            break;
        }
    }
    return result;
}

// Add one device with given block and add its code.
// return 0 if no error. 1 if unexpected error. 2 if code is invalid. 3 if code is already exist. 4 if code layer is not set.
int FASUtils::addOneDeviceInGraphic(FAS_Block* refBlock, FAS_Vector pos, QString code)
{
    int error = 0;
    if(nullptr == refBlock)
        return 1;
    if(!isValidDeviceCode(code))
        return 2;

    QString blockName = refBlock->getName();
    FAS_Insert* newDevice = nullptr;
    FAS_Insert* sameDevice = findOneInsertOfBlock(blockName, getRSDocument());
    if(sameDevice != nullptr)
    {
        newDevice = (FAS_Insert*)sameDevice->clone();
    }
    if(nullptr == newDevice)
        return 1;

    FAS_EntityContainer* container = getGraphicView()->getContainer();
    newDevice->setInsertionPoint(pos);
    newDevice->setLayer(sameDevice->getLayer());
    newDevice->getDeviceInfo()->setCodeInfo("");
    newDevice->update();
    container->addEntity(newDevice);

    error = setNewCodeToDevice(newDevice, code, "");
    if(error != 0)
    {
        container->removeEntity(newDevice);
        return error;
    }

    if(curDocument != nullptr)
    {
        curDocument->startUndoCycle();
        curDocument->addUndoable(newDevice);
        curDocument->endUndoCycle();
    }
    return error;
}

void FASUtils::setHighlight(FAS_Entity* e, bool isHighlight)
{
    if (e)
    {
        e->setHighlighted(isHighlight);
        qg_graphicView->redraw(FAS2::RedrawDrawing);
    }
}

bool FASUtils::getHighlight(FAS_Entity* e)
{
    if (e)
    {
        return e->isHighlighted();
    }
    return false;
}

void FASUtils::drawNotes(QString notes, int height, FAS_Vector pos)
{
    QString inputNotes = notes;
    double width = height * notes.size() * 0.8;
    QString text = inputNotes;
    QString style = "standard";
    double angle = 0;
    double lineSpacingFactor = 1.0;
    FAS_MTextData noteTextData(pos, height, width,
                              FAS_MTextData::VATop, FAS_MTextData::HALeft,
                              FAS_MTextData::LeftToRight, FAS_MTextData::AtLeast,
                              lineSpacingFactor, text, style, angle);
    FAS_MText* noteEntityText = new FAS_MText(curDocument, noteTextData);
    noteEntityText->update();
    curDocument->addEntity(noteEntityText);
    getGraphicView()->redraw(FAS2::RedrawDrawing);
    curDocument->startUndoCycle();
    curDocument->addUndoable(noteEntityText);
    curDocument->endUndoCycle();
}

// set new code to device entity, if there is no code entity now, create one. And store the information to database.
// return o if no error. 1 if unexpected error. 2 if code is invalid. 3 if code is already exist. 4 if code layer is not set.
int FASUtils::setNewCodeToDevice(FAS_Insert* device, QString newCode, QString noteText)
{
    int error = 0;
    if(nullptr == curDocument || nullptr == device)
    {
        error = 1;
        return error;
    }
    if(!isValidDeviceCode(newCode))
    {
        error = 2;
        return error;
    }

    bool codeChanged = false, noteChanged = false;
    QString oriCodeText = device->getDeviceInfo()->getCodeInfo();
    QString oriNoteText = device->getDeviceInfo()->getNoteText();

    QHash<QString, FAS_Entity*> codeEntityHash = this->getAllCodeEntityHash();
    QHash<QString, FAS_Entity*> noteEntityHash = this->getAllCodeEntityHash();

    if(oriCodeText != newCode || codeEntityHash.find(oriCodeText) == codeEntityHash.end())
        codeChanged = true;
    if(oriNoteText != noteText || noteEntityHash.find(oriNoteText) == noteEntityHash.end())
        noteChanged = true;

    curDocument->startUndoCycle();

    if(codeChanged)
    {
        if(codeEntityHash.find(newCode) != codeEntityHash.end())
        {
            error = 3;
            return error;
        }
        //1. change text of code entity. If there is no code entity, create one.
        if(codeEntityHash.find(oriCodeText) != codeEntityHash.end())
        {
            FAS_Entity* codeEntity = codeEntityHash.find(oriCodeText).value();
            if(FAS2::EntityText == codeEntity->rtti())
            {
                ((FAS_Text*)codeEntity)->setText(newCode);
            }
            else if (FAS2::EntityMText == codeEntity->rtti())
            {
                ((FAS_MText*)codeEntity)->setText(newCode);
            }
            codeEntity->update();
        }
        else if(device->isContainer())
        {
            if(this->getCodeLayer() == nullptr)
            {
                error = 4;
                return error;
            }
            double height = (device->getMax().y - device->getMin().y) * 0.25;
            double textLength = height * newCode.size() * 0.6;
            double deviceLength = device->getMax().x - device->getMin().x;
            double offsetX = (textLength - deviceLength) * 0.5;
            FAS_Vector insertPoint = FASUtils::getUpperLeftVector(device);
            insertPoint.x = insertPoint.x - offsetX;
            double widthRel = height * 8;
            double angle = 0.0;
            double lineSpacingFactor = 1.0;
            QString style = "standard";
            FAS_MTextData textData(insertPoint, height, widthRel,
                                  FAS_MTextData::VABottom, FAS_MTextData::HALeft,
                                  FAS_MTextData::LeftToRight, FAS_MTextData::AtLeast,
                                  lineSpacingFactor, newCode, style, angle, FAS2::NoUpdate);
            FAS_MText* rsText = new FAS_MText(getGraphicView()->getContainer(), textData);
            rsText->setLayer(this->getCodeLayer());
            rsText->update();
            getGraphicView()->getContainer()->addEntity(rsText);

            curDocument->addUndoable(rsText);
        }

        //2. Set code string to device entity.
        device->getDeviceInfo()->setCodeInfo(newCode);

        //3. update codeToDeviceMap.
        codeToDeviceMap.insert(newCode, device);
        codeToDeviceMap.remove(oriCodeText, device);
    }

    if(noteChanged && noteText.size() > 0)
    {
        if(this->getCodeLayer() == nullptr)
        {
            error = 4;
            return error;
        }
        //1. change text of note entity. If there is no note entity, create one.
        QString oriNoteText;
        QList<FAS_Entity*> noteEntityList = noteEntityHash.values();
        FAS_Entity* oriNoteEntity = findNoteEntityForDevice(device, noteEntityList, oriNoteText);
        if(oriNoteEntity != nullptr)
        {
            if(FAS2::EntityText == oriNoteEntity->rtti())
            {
                ((FAS_Text*)oriNoteEntity)->setText(noteText);
            }
            else if(FAS2::EntityMText == oriNoteEntity->rtti())
            {
                ((FAS_MText*)oriNoteEntity)->setText(noteText);
            }
            oriNoteEntity->update();
        }
        else
        {
            //Add note text for device
            double height = (device->getMax().y - device->getMin().y) * 0.25;
            double textLength = height * noteText.size() * 0.6;
            double deviceLength = device->getMax().x - device->getMin().x;
            double offsetX = (textLength - deviceLength) * 0.5;
            FAS_Vector insertPoint = device->getMin();
            insertPoint.x = insertPoint.x - offsetX;
            double widthRel = height * noteText.size();
            double angle = 0.0;
            double lineSpacingFactor = 1.0;
            QString style = "standard";
            FAS_MTextData textData(insertPoint, height, widthRel,
                                  FAS_MTextData::VATop, FAS_MTextData::HALeft,
                                  FAS_MTextData::LeftToRight, FAS_MTextData::AtLeast,
                                  lineSpacingFactor, noteText, style, angle, FAS2::NoUpdate);
            FAS_MText* rsText = new FAS_MText(getGraphicView()->getContainer(), textData);
            rsText->setLayer(this->getCodeLayer());
            rsText->update();
            getGraphicView()->getContainer()->addEntity(rsText);

            curDocument->addUndoable(rsText);
        }
        //2. Set note string to device entity.
        device->getDeviceInfo()->setNoteText(noteText);
    }

    curDocument->endUndoCycle();

    if(codeChanged || noteChanged)
        getGraphicView()->redraw();

    return error;
}

bool isTwoEntitiesComparedAsOne(FAS_Entity* entity1, FAS_Entity* entity2){
    bool result = false;
    if(entity1->rtti()==entity2->rtti()
            &&entity1->getStartpoint()==entity2->getStartpoint()
            &&entity1->getMiddlePoint()==entity2->getMiddlePoint()
            &&entity1->getEndpoint()==entity2->getEndpoint()
            &&entity1->getMin()==entity2->getMin()
            &&entity1->getMax()==entity2->getMax()
            &&entity1->getLength()==entity2->getLength()){
        result = true;
    }
    return result;
}



// set new unique code to non-device entity, if there is no code entity now, create one. And store the information to database.
// return o if no error. 1 if unexpected error. 2 if code is already exist.
int FASUtils::setNewCodeToEntity(FAS_Entity* entity, QString newCode)
{
    int error = 0;
    if(nullptr == curDocument || nullptr == entity)
    {
        error = 1;
        return error;
    }

    bool codeChanged = false;
    QString oriCodeText = newCode;


    //0. Get the code text of coded entity.
    for(auto oneMap = codeToEntityMap.begin(); oneMap != codeToEntityMap.end(); ++oneMap)
    {
        if(isTwoEntitiesComparedAsOne(entity, oneMap.value())){
            oriCodeText = oneMap.key();
            break;
        }
    }

    QHash<QString, FAS_Entity*> codeEntityHash = this->getAllCodeEntityHash();

    if(oriCodeText != newCode || codeToEntityMap.find(newCode) == codeToEntityMap.end())
        codeChanged = true;

    curDocument->startUndoCycle();

    if(codeChanged)
    {
        if(codeEntityHash.find(newCode) != codeEntityHash.end())
        {
            error = 2;
            return error;
        }
        //1. change text of code entity. If there is no code entity, create one.
        if(codeEntityHash.find(oriCodeText) != codeEntityHash.end())
        {
            FAS_Entity* codeEntity = codeEntityHash.find(oriCodeText).value();
            if(FAS2::EntityText == codeEntity->rtti())
            {
                ((FAS_Text*)codeEntity)->setText(newCode);
            }
            else if (FAS2::EntityMText == codeEntity->rtti())
            {
                ((FAS_MText*)codeEntity)->setText(newCode);
            }
            codeEntity->update();
        }
        else
        {
            auto pen = entity->getPen();
            pen.setColor(FAS_Color(0,0,255));
            if(this->getCodeLayer() == nullptr)
            {
                //error = 4;
                //return error;
                auto newLayer = createNewLayer("CodeLayer");
                newLayer->setPen(pen);
                this->setCodeLayer(newLayer);
            }
            double eHeight;
            FAS_Vector insertPoint = FAS_Vector(0,0,0);
            if(!isValidInsertEntity(entity) && entity->isContainer()){
                FAS_Insert* theInsert;
                QList<FAS_Insert*> insertList = getAllInsertEntities(curDocument);
                for(FAS_Insert* insert : insertList)
                {
                    if(insert->getName() == entity->getBlock()->getName() && entity->getId() == insert->getId())
                    {
                        theInsert = insert;
                        break;
                    }
                }
                eHeight = theInsert->getMax().y - theInsert->getMin().y;
                insertPoint = theInsert->getInsertionPoint();
            }else{
                eHeight = entity->getMax().y - entity->getMin().y;
                insertPoint = entity->getMiddlePoint();
            }
            double height = eHeight>0 ? eHeight * 0.25 : 6;
            double textLength = height * newCode.size() * 0.6;
            double eLength = entity->getMax().x - entity->getMin().x;
            double offsetX = (textLength - eLength) * 0.5;
            //FAS_Vector insertPoint = FASUtils::getUpperLeftVector(entity);
            insertPoint.x = insertPoint.x - offsetX;
            double widthRel = height * 8;
            double angle = 0.0;
            double lineSpacingFactor = 1.0;
            QString style = "standard";
            FAS_MTextData textData(insertPoint, height, widthRel,
                                  FAS_MTextData::VABottom, FAS_MTextData::HALeft,
                                  FAS_MTextData::LeftToRight, FAS_MTextData::AtLeast,
                                  lineSpacingFactor, newCode, style, angle, FAS2::NoUpdate);
            FAS_MText* rsText = new FAS_MText(getGraphicView()->getContainer(), textData);
            rsText->setLayer(this->getCodeLayer());
            rsText->setPen(pen);
            rsText->update();
            getGraphicView()->getContainer()->addEntity(rsText);

            curDocument->addUndoable(rsText);
        }

        //2. update codeToEntityMap.
        codeToEntityMap.remove(oriCodeText, entity);
        codeToEntityMap.insert(newCode, entity);
    }

    curDocument->endUndoCycle();

    if(codeChanged)
        getGraphicView()->redraw();

    return error;
}

bool isPointToEntityByID(FAS_Entity* entity,int entityID){
    if(nullptr != entity && 0==entityID - entity->getId()){
        return true;
    }else if(entity->isContainer()){
        if(FASUtils::isValidInsertEntity(entity)){
            auto blk = ((FAS_Insert*)entity)->getBlockForInsert();
            if(nullptr != blk && 0==entityID - (blk->getId())){
                return true;
            }else if(nullptr == blk){
                auto eList = blk->getEntityList();
                foreach(auto e,eList){
                    if(isPointToEntityByID(e,entityID)){
                        return true;
                    }
                }
            }else
            {
                return false;
            }
        }
        auto eList = ((FAS_EntityContainer*)entity)->getEntityList();
        foreach(auto e,eList){
            if(isPointToEntityByID(e,entityID)){
                return true;
            }
        }
    }else{
        return false;
    }
}

QList<FAS_Entity*> FASUtils::getEntitiesByID(int entityID)
{
    QList<FAS_Entity*> result;
    QList<FAS_Entity*> eList = curDocument->getEntityList();
    for(FAS_Entity* e : eList)
    {
        if(isPointToEntityByID(e,entityID))
        {
            if(FASUtils::isValidInsertEntity(e)){
                result.append((FAS_Insert*)e);
            }
            result.append(e);
        }
    }
    return result;
}

int FASUtils::setNewCodeToEntityByID(int entityID, QString newCode)
{
    int error = 0;
    int reDrawCount = 0;
    if(nullptr == curDocument)
    {
        error = 1;
        return error;
    }

    QList<FAS_Entity*> entityList = getEntitiesByID(entityID);

    if(0 == entityList.size()){
        error = 2;
        return error;
    }

    auto exCode = newCode;
    foreach(FAS_Entity* entity, entityList){
        if(nullptr == entity)
        {
            error += 2;
            continue;
        }
        if(exCode.split("-")[1].compare(QString::fromUtf8("%1").arg(entity->getId()),Qt::CaseInsensitive)){
            newCode = QString::fromUtf8("@%1_%2").arg(exCode).arg(entity->getId());
        }

        bool codeChanged = false;
        QString oriCodeText = newCode;

        //0. Get the code text of coded entity.
        for(auto oneMap = codeToEntityMap.begin(); oneMap != codeToEntityMap.end(); ++oneMap)
        {
            if(isTwoEntitiesComparedAsOne(entity, oneMap.value())){
                oriCodeText = oneMap.key();
                break;
            }
        }

        QHash<QString, FAS_Entity*> codeEntityHash = this->getAllCodeEntityHash();

        if(oriCodeText != newCode || codeToEntityMap.find(newCode) == codeToEntityMap.end())
        {
            codeChanged = true;
            reDrawCount += 1;
        }
        curDocument->startUndoCycle();

        if(codeChanged)
        {
            if(codeEntityHash.find(newCode) != codeEntityHash.end())
            {
                error += 3;
                continue;
            }
            //1. change text of code entity. If there is no code entity, create one.
            if(codeEntityHash.find(oriCodeText) != codeEntityHash.end())
            {
                FAS_Entity* codeEntity = codeEntityHash.find(oriCodeText).value();
                if(FAS2::EntityText == codeEntity->rtti())
                {
                    ((FAS_Text*)codeEntity)->setText(newCode);
                }
                else if (FAS2::EntityMText == codeEntity->rtti())
                {
                    ((FAS_MText*)codeEntity)->setText(newCode);
                }
                codeEntity->update();
            }
            else
            {
                auto pen = entity->getPen();
                pen.setColor(FAS_Color(255,255,255));
                if(this->getCodeLayer() == nullptr)
                {
                    auto newLayer = createNewLayer("CodeLayer");
                    newLayer->setPen(pen);
                    this->setCodeLayer(newLayer);
                }
                double eHeight = entity->getMax().y - entity->getMin().y;
                double eLength = entity->getMax().x - entity->getMin().x;
                double height = eHeight * 0.25;
                if(eHeight<10){
                    height = eLength * 0.1 * 0.25;
                }
                FAS_Vector insertPoint = FASUtils::getUpperLeftVector(entity);
                if(!entity->isContainer()){
                    double textLength = height * newCode.size() * 0.6;
                    double offsetX = (textLength - eLength) * 0.5;
                    double offsetY = (height - eHeight) * 0.5;
                    insertPoint.x = insertPoint.x - offsetX;
                    insertPoint.y = insertPoint.y + offsetY;
                }

                double widthRel = height * 8;
                double angle = 0.0;
                double lineSpacingFactor = 1.0;
                QString style = "standard";
                FAS_MTextData textData(insertPoint, height, widthRel,
                                      FAS_MTextData::VABottom, FAS_MTextData::HALeft,
                                      FAS_MTextData::LeftToRight, FAS_MTextData::AtLeast,
                                      lineSpacingFactor, newCode, style, angle, FAS2::NoUpdate);
                FAS_MText* rsText = new FAS_MText(getGraphicView()->getContainer(), textData);
                rsText->setLayer(this->getCodeLayer());
                rsText->setPen(pen);
                rsText->update();
                getGraphicView()->getContainer()->addEntity(rsText);

                curDocument->addUndoable(rsText);
            }

            //2. update codeToEntityMap.
            codeToEntityMap.remove(oriCodeText, entity);
            codeToEntityMap.insert(newCode, entity);
        }

        curDocument->endUndoCycle();
    }
    if(reDrawCount>0)
        getGraphicView()->redraw();

    return error;
}


//undo is true when Undo, false when Redo
void FASUtils::actionUndo(bool undo)
{
    if(qg_graphicView != nullptr && curDocument != nullptr)
    {
        qg_graphicView->killAllActions();
        FAS_ActionEditUndo* undoAction = new FAS_ActionEditUndo(undo, *curDocument, *qg_graphicView);
        qg_graphicView->setCurrentAction(undoAction);
    }
}

//delete selected entityies.
void FASUtils::actionModifyDelete()
{
    if(qg_graphicView != nullptr && curDocument != nullptr)
    {
        qg_graphicView->killAllActions();
        FAS_ActionModifyDelete* actionDelete = new FAS_ActionModifyDelete(*curDocument, *qg_graphicView);
        qg_graphicView->setCurrentAction(actionDelete);
    }
}

void FASUtils::zoomAuto()
{
    if(qg_graphicView != nullptr)
    {
        qg_graphicView->zoomAuto();
    }
}

//refresh the graphic view, called when rs entity is added or modified.
void FASUtils::refreshGraphicView()
{
    if(qg_graphicView != nullptr)
    {
        qg_graphicView->redraw();
    }
}

//set device info type of blcok.
void FASUtils::setBlcokType(FAS_Block* block, DeviceInfo* type)
{
    if(nullptr == block || type == nullptr)
        return;
    block->setType(type);
}

// set device type that have same name with given block.
// Note: Before call this, should set deviceinfo object of all Blcok entities by detecting type with detectDeviceType() and
// set block type with setBlcokType().  nameToEntityHash can be obtained from getBlockNameToEntityHash().
void FASUtils::setAllDeviceEntityType(QList<FAS_Block*> blcokList, QMultiHash<QString, FAS_Insert*> nameToEntityHash)
{
    for(auto oneBlock : blcokList)
    {
        DeviceInfo* deviceInfo = oneBlock->getType();
        if(nullptr == deviceInfo)
            continue;
        QList<FAS_Insert*> deviceList = nameToEntityHash.values(oneBlock->getName());
        for(FAS_Insert* oneDevice : deviceList)
        {
            oneDevice->getDeviceInfo()->setID(deviceInfo->getID());
            oneDevice->getDeviceInfo()->setName(deviceInfo->getName());
            oneDevice->getDeviceInfo()->setShowSVG(deviceInfo->getShowSVG());
            oneDevice->getDeviceInfo()->setSvgRender(deviceInfo->getSvgRender());

            oneDevice->getDeviceInfo()->setTypeCode(deviceInfo->getTypeCode());
            oneDevice->getDeviceInfo()->setPerfectMatchFlag(deviceInfo->getPerfectMatchFlag());
            oneDevice->getDeviceInfo()->setSystemType(deviceInfo->getSystemType());

            oneDevice->getDeviceInfo()->setValidRecognition(deviceInfo->isRecognized());
            oneDevice->setVisible(!deviceInfo->isRecognized());
        }
    }
}

//get the layer of codes are placed.
FAS_Layer* FASUtils::getCodeLayer(){
    return this->codeLayer;
}
void FASUtils::setCodeLayer(FAS_Layer* layer)
{
    this->codeLayer = layer;
}

// create a new layer.
FAS_Layer* FASUtils::createNewLayer(QString name)
{
    FAS_Layer* layer = new FAS_Layer(name);
    ((FAS_Graphic*)curDocument)->addLayer(layer);
    return layer;
}

bool FASUtils::exportFileAsImage(QString filePath)
{
    if(filePath != curDocument->getFilename())
        return false;

    bool result = false;
    QStringList filters;
    QList<QByteArray> supportedImageFormats = QImageWriter::supportedImageFormats();
    supportedImageFormats.push_back("svg"); // add svg

    for (QString format: supportedImageFormats)
    {
        format = format.toLower();
        QString st;
        if (format=="jpeg" || format=="tiff")
        {
            // Don't add the aliases
        } else
        {
            st = QString("%1 (%2)(*.%2)")
                    .arg(format)
                    .arg(format);
        }
        if (st.length()>0)
            filters.push_back(st);
    }
    // revise list of filters
    filters.removeDuplicates();
    filters.sort();

    // set dialog options: filters, mode, accept, directory, filename
    QFileDialog fileDlg(nullptr, "Export as");

    fileDlg.setNameFilters(filters);
    fileDlg.setFileMode(QFileDialog::AnyFile);
    fileDlg.setAcceptMode(QFileDialog::AcceptSave);
    QString fileName = "unnamed";
    fileDlg.selectFile(fileName);

    bool cancel = false;
    if (fileDlg.exec() == QDialog::Accepted)
    {
        QStringList files = fileDlg.selectedFiles();
        if (!files.isEmpty())
            fileName = files[0];
        cancel = false;
    }
    else
    {
        cancel = true;
    }

    // store new default settings:
    if (!cancel)
    {
        // find out extension:
        QString filter = fileDlg.selectedNameFilter();
        QString format = "";
        int i = filter.indexOf("(*.");
        if (i != -1)
        {
            int i2 = filter.indexOf(QRegExp("[) ]"), i);
            format = filter.mid(i+3, i2-(i+3));
            format = format.toUpper();
        }

        // append extension to file:
        if (!QFileInfo(fileName).fileName().contains("."))
        {
            fileName.push_back("." + format.toLower());
        }
        FAS_Vector dist = curDocument->getMax() - curDocument->getMin();
        double x = 1280;
        double y = x * dist.y / dist.x;
        result = doExportFileAsImage(fileName, format, QSize(x, y), QSize(25,25), false);
    }
    return result;
}

bool FASUtils::doExportFileAsImage(const QString& name, const QString& format, QSize size, QSize borders, bool black)
{
    bool ret = false;
    // set vars for normal pictures and vectors (svg)
    QPixmap* picture = new QPixmap(size);
    QSvgGenerator* vector = new QSvgGenerator();

    // set buffer var
    QPaintDevice* buffer = nullptr;
    if(format.toLower() != "svg")
    {
        buffer = picture;
    }
    else
    {
        vector->setSize(size);
        vector->setViewBox(QRectF(QPointF(0,0),size));
        vector->setFileName(name);
        buffer = vector;
    }

    // set painter with buffer
    FAS_PainterQt painter(buffer);
    painter.setBackground(qg_graphicView->getBackground().toQColor());
    painter.setDrawingMode(qg_graphicView->getDrawingMode());

    FAS_StaticGraphicView gv(size.width(), size.height(), &painter, &borders);
    gv.setBackground(gv.getBackground());
    gv.setContainer(qg_graphicView->getContainer());
    gv.zoomAuto(false);
    gv.drawEntity(&painter, gv.getContainer());

    // end the picture output
    auto blkSavePath=QString::fromUtf8("%1.%2").arg(name).arg(format);
    if(format.toLower() != "svg")
    {
        QImageWriter iio;
        QImage img = picture->toImage();
        iio.setFileName(name);
        iio.setFormat(format.toLatin1());
        if (iio.write(img))
        {
            ret = true;
        }
    }
    painter.end();
    // delete vars
    delete picture;
    delete vector;

    return ret;
}



QImage toGray( QImage *image )
{
    int height = image->height();
    int width = image->width();
    QImage ret(width, height, QImage::Format_Indexed8);
    ret.setColorCount(256);
    for(int i = 0; i < 256; i++)
    {
        ret.setColor(i, qRgb(i, i, i));
    }
    switch(image->format())
    {
    case QImage::Format_Indexed8:
        for(int i = 0; i < height; i ++)
        {
            const uchar *pSrc = (uchar *)image->constScanLine(i);
            uchar *pDest = (uchar *)ret.scanLine(i);
            memcpy(pDest, pSrc, width);
        }
        break;
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32:
    case QImage::Format_ARGB32_Premultiplied:
        for(int i = 0; i < height; i ++)
        {
            const QRgb *pSrc = (QRgb *)image->constScanLine(i);
            uchar *pDest = (uchar *)ret.scanLine(i);

            for( int j = 0; j < width; j ++)
            {
                 pDest[j] = qGray(pSrc[j]);
            }
        }
        break;
    }
    return ret;
}

QPixmap ChangeImageColor(QPixmap sourcePixmap, QColor origColor, QColor destColor)
{
    QImage image = sourcePixmap.toImage();
    uchar * imagebits_32;
    for(int i =0; i <image.height(); ++i)
    {
        imagebits_32 = image.scanLine(i);
        for(int j =0; j < image.width(); ++j)
        {
            int r_32 = imagebits_32[j * 4 + 2];
            int g_32 = imagebits_32[j * 4 + 1];
            int b_32 = imagebits_32[j * 4];
            if(r_32 == origColor.red()
            && g_32 == origColor.green()
            && b_32 == origColor.blue())
            {
                imagebits_32[j * 4 + 2] = (uchar)destColor.red();
                imagebits_32[j * 4 + 1] = (uchar)destColor.green();
                imagebits_32[j * 4] = (uchar)destColor.blue();
            }
        }
    }
    return QPixmap::fromImage(image);
}

bool FASUtils::isDirExist(QString fullPath)
{
    QDir dir(fullPath);
    if(dir.exists())
    {
      return true;
    }
    else
    {
       bool ok = dir.mkpath(fullPath);//创建多级目录
       return ok;
    }
}

int generateRandomNumbers(int bitsNumberNeed){
    QTime time;
    time=QTime::currentTime();
    int min = qPow(10,bitsNumberNeed-1);
    int max = qPow(10,bitsNumberNeed)-1;
    auto factor = qPow(10,bitsNumberNeed+2);
    auto seed = time.msec()+time.second()*factor;
    qsrand(seed);

    //double result=0.01*n;//0到1之间两位小数的随机数
    return (qrand()%(max-min))+min;
}

 bool FASUtils::doConvertDeviceToImage(const QString& desDir, const QString& imageName, const QString& format, FAS_EntityContainer* container, QSize size, bool black)
{
    //Clear the items' count that recorded in last image to solve the issue of incomlete device graphic.
    COMMONDEF->drawEntityCount = 0;
    bool ret = false;
    QImage *picture = new QImage(size,QImage::Format_ARGB32_Premultiplied);
    picture->fill("lightGray");
    QSvgGenerator* vector = new QSvgGenerator();

    // set buffer var
    QPaintDevice* buffer = nullptr;
    if(format.toLower() != "svg")
    {
        buffer = picture;
    }
    else
    {
        vector->setSize(size);
        vector->setViewBox(QRectF(QPointF(0,0),size));
        auto svgSavePath=QString::fromUtf8("%1/%2.%3").arg(desDir).arg(imageName).arg(format);
        vector->setFileName(svgSavePath);
        buffer = vector;
    }
    // set painter with buffer
    FAS_PainterQt painter(buffer);
    //painter.setBackground(qg_graphicView->getBackground().toQColor());
    auto drawingMode = qg_graphicView->getDrawingMode();//ModeFull
    painter.setDrawingMode(qg_graphicView->getDrawingMode());

    painter.setBackground(FAS_Color(255,255,255).toQColor());
    painter.setPen(FAS_Color(6,6,6));


    FAS_StaticGraphicView gv(size.width(), size.height(), &painter);
    gv.setBackground(gv.getBackground());
    gv.setContainer(container);
    auto viewContainer = gv.getContainer();
    gv.zoomAutoBlock(true);

    FAS_InsertData d =((FAS_Insert*)container->getEntityList().first())->getData();
//    auto temPex = painter.getPen();
//    qDebug() << "JPG painter PenCr: " << temPex.getColor().toQColor().toRgb();
    gv.drawEntity(&painter, viewContainer);
//    auto temPCr = painter.getPen();
//    qDebug() << "JPG painter PenCr: " << temPCr.getColor().toQColor().toRgb();

    // end the picture output

    auto blkSavePath=QString::fromUtf8("%1/%2.%3").arg(desDir).arg(imageName).arg(format);
    if(this->isDirExist(desDir))
    {
        if(format.toLower() != "svg"){
            QImageWriter iio;
            QImage img = toGray(picture);
            iio.setFileName(blkSavePath);
            iio.setFormat(format.toLatin1());
            if (iio.write(img))
            {
                ret = true;
            }
        }
    }
    painter.end();
    // delete vars
    delete picture;
    picture = nullptr;
    delete vector;
    vector = nullptr;
    return ret;
}

 bool FASUtils::svgGenerator(const QString& desDir,const QString& imageName,
                   FAS_EntityContainer* container, QSize size, const QString& format, bool black){
     //Clear the items' count that recorded in last image to solve the issue of incomlete device graphic.
     COMMONDEF->drawEntityCount = 0;
     bool ret = false;
     QImage *picture = new QImage(size,QImage::Format_ARGB32_Premultiplied);
     picture->fill("lightGray");
     QSvgGenerator* vector = new QSvgGenerator();

     // set buffer var
     QPaintDevice* buffer = nullptr;
     if(format.toLower() != "svg")
     {
         buffer = picture;
     }
     else
     {
         vector->setSize(size);
         vector->setViewBox(QRectF(QPointF(0,0),size));
         auto svgSavePath=QString::fromUtf8("%1/%2.%3").arg(desDir).arg(imageName).arg(format);
         vector->setFileName(svgSavePath);
         buffer = vector;
     }
     // set painter with buffer
     FAS_PainterQt painter(buffer);
     //painter.setBackground(qg_graphicView->getBackground().toQColor());
//     auto drawingMode = qg_graphicView->getDrawingMode();//ModeFull
     painter.setDrawingMode(FAS2::ModeFull);

//     painter.setBackground(FAS_Color(255,255,255).toQColor());
//     painter.setPen(FAS_Color(255,255,255));


     FAS_StaticGraphicView gv(size.width(), size.height(), &painter);
//     gv.setBackground(gv.getBackground());
//     gv.setBackground(FAS_Color(255,255,255));
//     auto temP = painter.getPen();
//     qDebug() << "SVG painter PenEx: " << temP.getColor().toQColor().toRgb();
     gv.setContainer(container);
     auto viewContainer = gv.getContainer();
     gv.zoomAutoBlock(true);

     FAS_InsertData d =((FAS_Insert*)container->getEntityList().first())->getData();
//     auto temP = painter.getPen();
//     qDebug() << "SVG painter PenEx: " << temP.getColor().toQColor().toRgb();
     gv.drawEntity(&painter, viewContainer);
//     auto temPCr = painter.getPen();
//     qDebug() << "SVG painter PenCr: " << temPCr.getColor().toQColor().toRgb();

     // end the picture output

     auto blkSavePath=QString::fromUtf8("%1/%2.%3").arg(desDir).arg(imageName).arg(format);
     if(COMMONDEF->isDirExist(desDir))
     {
         if(format.toLower() != "svg"){
             QImageWriter iio;
             QImage img = toGray(picture);
             iio.setFileName(blkSavePath);
             iio.setFormat(format.toLatin1());
             if (iio.write(img))
             {
                 ret = true;
             }
         }
     }
     painter.end();
     // delete vars
     delete picture;
     picture = nullptr;
     delete vector;
     vector = nullptr;
     return ret;


 }

bool FASUtils::exportFileAsPDF(QString filePath)
{
    bool result = false;
    if(filePath != curDocument->getFilename())
    {
        return result;
    }

    QPrinter printer(QPrinter::HighResolution);

    FAS_Graphic* graphic = curDocument->getGraphic();
    bool landscape = false;
    FAS2::PaperFormat pf = graphic->getPaperFormat(&landscape);
    QPrinter::PageSize paperSize = QPrinter::Custom;
    printer.setPaperSize(paperSize);
    if (landscape)
    {
        printer.setOrientation(QPrinter::Landscape);
    }
    else
    {
        printer.setOrientation(QPrinter::Portrait);
    }
    // printer setup:
    bool bStartPrinting = false;

    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setColorMode(QPrinter::Color);
    QFileDialog fileDlg(nullptr, "Export as PDF");
    QString     defFilter("PDF files (*.pdf)");
    QStringList filters;

    filters << defFilter
            << "Any files (*)";

    fileDlg.setNameFilters(filters);
    fileDlg.setFileMode(QFileDialog::AnyFile);
    fileDlg.selectNameFilter(defFilter);
    fileDlg.setAcceptMode(QFileDialog::AcceptSave);
    fileDlg.setDefaultSuffix("pdf");

    if( QDialog::Accepted == fileDlg.exec())
    {
        QStringList files = fileDlg.selectedFiles();
        if (!files.isEmpty())
        {
            if(!files[0].endsWith(R"(.pdf)",Qt::CaseInsensitive))
                files[0] = files[0]+".pdf";
            printer.setOutputFileName(files[0]);
            bStartPrinting = true;
        }
    }
    else
    {
        bStartPrinting = false;
    }

    if (bStartPrinting)
    {
        printer.setResolution(1200);
        printer.setFullPage(true);

        FAS_PainterQt painter(&printer);
        painter.setDrawingMode(qg_graphicView->getDrawingMode());

        QSize border(25,25);
        FAS_StaticGraphicView gv(printer.width(), printer.height(), &painter, &border);
        gv.setPrinting(true);
        gv.setContainer(graphic);
        gv.zoomAuto(false);
        gv.drawEntity(&painter, graphic);
        gv.setBackground(gv.getBackground());

        // GraphicView deletes painter
        painter.end();
        result = true;
    }
    else
    {
        result = false;
    }
    return result;
}

// find nearest valid note text entity for device.
FAS_Entity* FASUtils::findNoteEntityForDevice(FAS_Insert* device, QList<FAS_Entity *>& noteEntityList, QString& noteText)
{
    FAS_Entity* resultEntity = nullptr;
    if(nullptr == device || noteEntityList.empty())
    {
        return resultEntity;
    }

    double deviceSize = device->getMax().distanceTo(device->getMin());
    double rangeSize = deviceSize * 1.5;

    FAS_Vector coord = device->getMiddlePoint();
    double minDist = FAS_MAXDOUBLE;
    for (FAS_Entity* noteEnt : noteEntityList)
    {
        FAS_Vector textPoint = noteEnt->getMiddlePoint();
        double temp = textPoint.distanceTo(coord);
        if (temp < rangeSize && temp < minDist)
        {
            minDist = temp;
            if(FAS2::EntityText == noteEnt->rtti())
            {
                noteText = ((FAS_Text*)noteEnt)->getText();
                resultEntity = noteEnt;
            }
            else if(FAS2::EntityMText == noteEnt->rtti())
            {
                noteText = ((FAS_MText*)noteEnt)->getText();
                resultEntity = noteEnt;
            }
        }
    }
    return resultEntity;
}

//Change backgroud color.
void FASUtils::setBackgroudColor(QColor bg)
{
    if(qg_graphicView != nullptr)
    {
        FAS_Color bgColor(bg);
        qg_graphicView->setBackground(bgColor);
        qg_graphicView->redraw();
    }
}

void FASUtils::setFontForUI(QWidget* widget)
{
    QFontDatabase fontDatabase;
    auto curFontFamilies = fontDatabase.families();
    auto widgetFamily = widget->fontInfo().family();
    int id = QFontDatabase::addApplicationFont(":/font/resource/font/DroidSansFallback.ttf");
    QString msyh = QFontDatabase::applicationFontFamilies(id).at(0);
    auto fontExist = curFontFamilies.contains(msyh);
    if(fontExist){
        QFontDatabase::removeApplicationFont(id);
    }
    if(widgetFamily.compare(msyh,Qt::CaseInsensitive))
    {
        QFont font(msyh,10);
        font.setPointSize(10);
        widget->setFont(font);
    }
}

void FASUtils::showSvgIoconForAllDevices(bool showSVG)
{
    QList<FAS_Insert*> allDevices = getAllDevicesFromRS();
    for(FAS_Insert* device : allDevices)
    {
        device->getDeviceInfo()->setShowSVG(showSVG);
    }
}

//calculate length of given wire in layer
double FASUtils::calculateWireLength(FAS_Layer* layer)
{
    double result = 0;
    if(layer == nullptr)
        return result;

    QList<FAS_Entity*> allEntities = getRSDocument()->getEntityList();
    for(FAS_Entity* oneEntity : allEntities)
    {
        if(oneEntity->getLayer() == layer)
        {
            double length = oneEntity->getLength();
            if(length > 0)
                result += length;
        }
    }
    return result;
}

//create one line
void FASUtils::createLineInGraphic(FAS_Vector start, FAS_Vector end)
{
    FAS_Document* doc = getRSDocument();
    if(doc != nullptr){
        FAS_Line* line = new FAS_Line(doc, {start, end});
        line->setLayer(this->getWireLineLayer());
        line->update();
        doc->addEntity(line);

        doc->startUndoCycle();
        doc->addUndoable(line);
        doc->endUndoCycle();
        getGraphicView()->drawEntity(line);
    }
}


//add new detected block of device
void FASUtils::addNewDetectdInsertOfDevice(int key, FAS_Insert* Block){
    this->codeToTempInsertOfDeviceMap.insert(key,Block);
}

//clear new detected blocks of devices
void FASUtils::clearNewDetectdInsertOfDevice(){
    this->codeToTempInsertOfDeviceMap.clear();
}

QMultiHash<int, FAS_Insert*> FASUtils::getTempInsertOfDeviceMap()
{
    return codeToTempInsertOfDeviceMap;
}

FAS_Insert* FASUtils::findTempInsertOfDeviceMapByKey(int key)
{
    return codeToTempInsertOfDeviceMap.find(key).value();
}


void FASUtils::initDetectionRules()
{
    this->detectionRule.clear();
    //Init from database firstly, if there is no rule in DB, init rules.
    CadSqlite mysql;
    mysql.openDataBase(COMMONDEF->getDatabaseName());
    int count=0;    
    if(mysql.isExistingTable("DETECT"))
    {
        QString strSql;
        QSqlQuery query(mysql.m_db);

        strSql.clear();
        strSql.append("select * from DETECT");
        query.prepare(strSql);
        query.exec();
        while(query.next())
        {
            int circles = query.value(1).toInt();
            int lines = query.value(2).toInt();
            int arcs = query.value(3).toInt();
            int hatch = query.value(4).toInt();
            int solids = query.value(5).toInt();
            double size = query.value(6).toDouble();
            QString text = query.value(7).toString();
            QString name = query.value(8).toString();
            QString system = query.value(9).toString();
            addDetectRule(DeviceFeature(circles, lines, arcs, hatch, solids, size, text), name,system,true);
            count++;
        }
    }
//    auto systemType = COMMONDEF->getSystem(true);
    if(count < 22)
    {
        DeviceInfo* type = COMMONDEF->getDefaultType();
        // *** Smoke Detect ***************
        type = COMMONDEF->getDeviceInfoFromID(100 * 0);
        addDetectRule(DeviceFeature(0, 8, 0, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(0, 7, 0, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(2, 8, 0, 0, 12, 0.0, ""), type->getName(),"",true);//

        // *** Thermal Detect ****************
        type = COMMONDEF->getDeviceInfoFromID(100 * 1);
        addDetectRule(DeviceFeature(1, 5, 0, 0, 6, 0.0, ""), type->getName(),"",true);//

        // *** Manul Alarm ****************
        type = COMMONDEF->getDeviceInfoFromID(100 * 2);
        addDetectRule(DeviceFeature(2, 5, 1, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(0, 5, 1, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(0, 5, 4, 0, 0, 0.0, ""), type->getName(),"",true);//

        // *** Fire Alarm **************
        type = COMMONDEF->getDeviceInfoFromID(100 * 3);
        addDetectRule(DeviceFeature(0, 3, 1, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(0, 11, 0, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(2, 17, 0, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(1, 15, 0, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(0, 21, 0, 0, 0, 0.0, ""), type->getName(),"",true);//

        // *** Hydrant ****************
        type = COMMONDEF->getDeviceInfoFromID(100 * 4);
        addDetectRule(DeviceFeature(0, 11, 1, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(0, 6, 1, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(1, 13, 0, 1, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(1, 4, 0, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(0, 6, 16, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(1, 2, 2, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(0, 4, 8, 0, 0, 0.0, ""), type->getName(),"",true);//

        // *** Fire Phone ****************
        type = COMMONDEF->getDeviceInfoFromID(100 * 7);
        addDetectRule(DeviceFeature(0, 10, 1, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(0, 10, 4, 0, 0, 0.0, ""), type->getName(),"",true);//

        // *** Flow Indicater ****************
        type = COMMONDEF->getDeviceInfoFromID(100 * 13);
        addDetectRule(DeviceFeature(0, 5, 0, 0, 1, 0.0, ""), type->getName(),"",true);//

        // *** Emergence Light ****************
        type = COMMONDEF->getDeviceInfoFromID(100 * 17);
        addDetectRule(DeviceFeature(0, 6, 0, 0, 0, 0.0, ""), type->getName(),"",true);

        // *** SmokeThermal detecter ************
        type = COMMONDEF->getDeviceInfoFromID(100 * 21);
        addDetectRule(DeviceFeature(1, 10, 0, 0, 6, 0.0, ""), type->getName(),"",true);//

        // *** Signal Valve ***************
        type = COMMONDEF->getDeviceInfoFromID(100 * 41);
        addDetectRule(DeviceFeature(0, 5, 0, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(1, 5, 0, 0, 7, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(1, 5, 0, 0, 0, 0.0, ""), type->getName(),"",true);//
        addDetectRule(DeviceFeature(0, 7, 8, 0, 0, 0.0, ""), type->getName(),"",true);//
    }
}
/*
//add detect rule to current memory.
void FASUtils::addDetectRule(DeviceFeature deviceFeature, QString typeStr, QString sytempTypeStr)
{
    //The input rule is covered by rule list.
    if(detectDeviceType(deviceFeature)->getName() == typeStr)
        return;

    DetectionRule newRule;
    newRule.feature = deviceFeature;
    newRule.deviceType = typeStr;
    if(!sytempTypeStr.isEmpty()){
        newRule.systemType = sytempTypeStr;
    }


    this->detectionRule.append(newRule);
}*/

//delete the old existing rule and return the value of its property 'isStored'.
bool deleteDetectRuleFromMemory(QList<DetectionRule> &storedRulesInMemory, DeviceFeature deviceFeatureToBeDelete){
    bool result = false;
    QList<DetectionRule>::iterator it = storedRulesInMemory.begin();
    DetectionRule newRule;
    while (it!= storedRulesInMemory.end()) {
        auto itSysType = it->systemType;
        auto curSysType = COMMONDEF->getSystem(true);
        auto isNotCurSys = curSysType.compare(itSysType,Qt::CaseInsensitive);
        auto isNotUnknownSys = itSysType.compare(newRule.systemType,Qt::CaseInsensitive);
        if(isNotUnknownSys && isNotCurSys){
            it++;
            continue;
        }

        if(it->feature.circles == deviceFeatureToBeDelete.circles &&
                it->feature.lines == deviceFeatureToBeDelete.lines &&
                it->feature.arcs == deviceFeatureToBeDelete.arcs &&
                it->feature.hatchs == deviceFeatureToBeDelete.hatchs &&
                it->feature.solids == deviceFeatureToBeDelete.solids &&
                it->feature.text == deviceFeatureToBeDelete.text)
        {
            if(abs(it->feature.size - deviceFeatureToBeDelete.size) < COMMONDEF->getTolerance() ||
                    abs(it->feature.size - 0.0) < COMMONDEF->getTolerance())
            {
                result = it->isStored;
                it = storedRulesInMemory.erase(it);
            }
            else
            ++it;
        }
        else
        ++it;
    }
    return result;
}


//bool isRuleExist(QHash<QString,bool> &manageExistRules,
//                     DeviceFeature deviceFeature,
//                     QString sytempTypeStr){
//    bool isAdmin = COMMONDEF->getAdmin();
//    bool  canUpdate=false;
////    if(!result)
////        return result;
//    auto ruleKey = QString::fromUtf8("%1_%2_%3%4%5%6%7%8")
//            .arg(sytempTypeStr)
//            .arg(deviceFeature.arcs)
//            .arg(deviceFeature.size)
//            .arg(deviceFeature.text)
//            .arg(deviceFeature.lines)
//            .arg(deviceFeature.hatchs)
//            .arg(deviceFeature.solids)
//            .arg(deviceFeature.circles);
//    if(manageExistRules.values(ruleKey).size()>0){
//        if(isAdmin){
//           canUpdate = true;
//        }
//    }
//    else{
//        manageExistRules.insert(ruleKey,false);
//        canUpdate = true;
//    }
//    return canUpdate;
//}

//add detect rule to memory.
void FASUtils::addDetectRule(DeviceFeature deviceFeature, QString typeStr, QString sytempTypeStr, bool isInit)
{
    //Only administrator has the access to update the existing rules' property.
    bool isAdmin = COMMONDEF->getAdmin();
    //The input rule is covered by rule list.
    auto localTypeName = detectDeviceType(deviceFeature)->getName();
    if(!localTypeName.compare(typeStr,Qt::CaseInsensitive)
            && (sytempTypeStr.isEmpty() || sytempTypeStr.compare("UnknownSystem",Qt::CaseInsensitive))){
        if(!isAdmin){
            return;
        }
    }
    else if(typeStr.compare("NotDevice",Qt::CaseInsensitive)==0
            || typeStr.compare("UnknownDevice",Qt::CaseInsensitive)==0){
        deleteDetectRuleFromMemory(this->detectionRule,deviceFeature);
        deleteDetectRuleFromMemory(this->newDetectionRules4Local,deviceFeature);
        deleteDetectRuleFromMemory(this->newDetectionRules4Cloud,deviceFeature);
        return;
    }

    DetectionRule newRule;
    newRule.feature = deviceFeature;
    newRule.deviceType = typeStr;
    if(!sytempTypeStr.isEmpty()){
        newRule.systemType = sytempTypeStr;
    }

    if(!isInit){
        newRule.isStored = deleteDetectRuleFromMemory(this->newDetectionRules4Local, deviceFeature);
        this->newDetectionRules4Local.append(newRule);
        newRule.isStored = deleteDetectRuleFromMemory(this->newDetectionRules4Cloud, deviceFeature);
        this->newDetectionRules4Cloud.append(newRule);
    }
    newRule.isStored = deleteDetectRuleFromMemory(this->detectionRule, deviceFeature);
    this->detectionRule.append(newRule);
}


// add device list to database.
void FASUtils::storeDeviceListToDB(QList<FAS_Insert*> deviceList)
{
    CadSqlite mySql;
    mySql.openDataBase(COMMONDEF->getDatabaseName());
    mySql.dropDevice();
    mySql.createDeviceTable();

    int insertId = 1;
    for (FAS_Insert* oneDevice : deviceList)
    {
        DeviceInfo* deviceInfo = oneDevice->getDeviceInfo();
        FAS_Vector xy = oneDevice->getMiddlePoint();
        QString deviceCode = deviceInfo->getCodeInfo();
        QString strType = deviceInfo->getName();
        QString strNote = deviceInfo->getNoteText();
        QString strSysType = deviceInfo->getSystemType();
        mySql.addDevice(QString::number(insertId), QString::number(xy.x), QString::number(xy.y), deviceCode, strType, strNote, strSysType);
        ++insertId;
    }
    mySql.closeDataBase();
}

//Store detection rule to databse
void FASUtils::storeDetectionRulesToDB()
{
    CadSqlite mySql;
    mySql.openDataBase(COMMONDEF->getDatabaseName());
    int rulesNumber = mySql.readDetectNum();
    mySql.dropDetect();
    mySql.createDetectTable();
    int id = 0;
    for(DetectionRule rule : this->detectionRule)
    {
        id++;
        mySql.addDetectRule(id, rule);
        rulesNumber = mySql.readDetectNum();
//        // qDebug() << "Rules covered the local DB, total stored number=" << rulesNumber;
    }
    mySql.closeDataBase();
    addDetectionRuleToCloud();
    clearNewRules4Cloud();
    clearNewRules4Local();
}

//add new detect rule to database.
void FASUtils::addDetectionRuleToLocalDB()
{
    CadSqlite mysql;
    mysql.openDataBase(COMMONDEF->getDatabaseName());
    int rulesNumber = mysql.readDetectNum();
    int i = 0;
    for(DetectionRule &r : this->newDetectionRules4Local)
    {
        //The input rule is covered by rule list.
        if(r.deviceType.compare("NotDevice",Qt::CaseInsensitive) !=0
                && r.deviceType.compare("UnknownDevice",Qt::CaseInsensitive)!=0
                && !r.isStored){
            mysql.addDetectRule(++rulesNumber,r);
            r.isStored = true;
            i++;
        }
    }
    rulesNumber = mysql.readDetectNum();
    // qDebug() << i << "new features added in DB, total stored number=" << rulesNumber;
    i=0;
}


//download detect rule to memory.
QList<DetectionRule> FASUtils::getDetectionRulesFromCloud(){
    QList<DetectionRule> result;
    result = this->detectionRule;
    return result;
}


//get new detect rule to database.
void FASUtils::updateLocalDetectionRuleFromCloud()
{
    //connect to cloud
    //update new rule for this->detectionRule from cloud
    //update local dababase
    //disconnect to cloud
    // qDebug() << "download 0 features from cloud";
}

//add new detect rule to database.
void FASUtils::addDetectionRuleToCloud()
{
    //connect to cloud
    //update new rule in this->newDetectionRules4Cloud into cloud
    //clear this->newDetectionRules4Cloud
    //disconnect to cloud
    // qDebug() << 0 << "new features added to cloud";
    clearNewRules4Cloud();
}



void FASUtils::clearNewRules4Cloud()
{
    this->newDetectionRules4Cloud.clear();
}

void FASUtils::clearNewRules4Local()
{
    this->newDetectionRules4Local.clear();
}

void filterBorderLines(QMultiMap<double,FAS_Entity*> allEntities){

    FAS_Entity* tempentity=nullptr;
    QMultiMap<double,FAS_Entity*>::Iterator   MapItor;
    for(MapItor = --allEntities.end();MapItor != --allEntities.begin();MapItor--)
        {
        foreach (FAS_Entity* value, allEntities.values(MapItor.key())) {

              }
        }
}

QList<QPair<QString,double>> createSubViewSplitter(double start, double end){
    QList<QPair<QString,double>> result;
    start -= 1;
    end += 1;
    int step = (int)(end - start)/COMMONDEF->getSubViewSplitterFactor();
    for(int p=start;p<end;p+=step){
        int p1=p;
        int p2=(p+step)<=end ? p+step : end;
//        QString xKey = QString::fromUtf8("%1_%2").arg(QString::number(p1,'f',7)).arg(QString::number(p2,'f',7));
        QString xKey = QString::fromUtf8("%1_%2").arg(QString::number(p1)).arg(QString::number(p2));
        QPair<QString, double> qPair(xKey, true);
        result.append(qPair);
    }
    return result;
}

QList<QPair<double,double>> createSubViewAxis(QList<QPair<QString,double>> subViewSplitterTemplate){
    QList<QPair<double,double>> result;
    double startValue;
    double endValue;
    bool isScan4NewView = true;
    for(auto checkPoint = subViewSplitterTemplate.begin(); checkPoint != subViewSplitterTemplate.end(); ++checkPoint)
    {
        QStringList CheckPoints =checkPoint->first.split("_");
        startValue =  isScan4NewView ? CheckPoints[0].toDouble() : startValue;
        endValue = CheckPoints[1].toDouble();
        bool isStoreAsNewView = checkPoint->second && !isScan4NewView;
        bool isContinuousSplitter = checkPoint->second && isScan4NewView;
        bool isTheValidVeryEnd = !isScan4NewView && checkPoint->first.compare(subViewSplitterTemplate.last().first,Qt::CaseInsensitive) == 0;
        if(isStoreAsNewView || isTheValidVeryEnd){
            QPair<double, double> qPair(startValue, endValue);
            result.append(qPair);
            isScan4NewView = true;
            // qDebug() <<"subViewRangeStart = "<< startValue<<",subViewRangeEnd = "<< endValue;
        }else if(isContinuousSplitter){
            continue;
        }
        else {
            isScan4NewView = false;
        }
    }
    return result;
}

QList<QPair<FAS_Vector,FAS_Vector>> createSubViewAreas(QList<QPair<double,double>> xAxis, QList<QPair<double,double>> yAxis){
    QList<QPair<FAS_Vector,FAS_Vector>> result;

    QMultiMap<double,FAS_Entity*> allEntities;
    FAS_Entity* tempentity=nullptr;
    QMultiMap<double,FAS_Entity*>::Iterator   MapItor;
    for(MapItor = --allEntities.end();MapItor != --allEntities.begin();MapItor--){
        foreach (FAS_Entity* value, allEntities.values(MapItor.key())) {
            QPair<FAS_Vector,FAS_Vector> boarder;


              }
        }

    for(int y=yAxis.size() -1 ;y>=0 ;y--){
        for(int x=0;x<xAxis.size();x++){
            auto vMin = FAS_Vector(xAxis.at(x).first,yAxis.at(y).first);
            auto vMax = FAS_Vector(xAxis.at(x).second,yAxis.at(y).second);
            QPair<FAS_Vector,FAS_Vector> area(vMin,vMax);
            result.append(area);
        }
    }
    return result;
}




bool isEntityInArea(FAS_Entity* entity, QPair<FAS_Vector,FAS_Vector> area){
    bool result = false;
    if(entity->getMin().x >= area.first.x
        && entity->getMin().y >= area.first.y
        && entity->getMax().x <= area.second.x
        && entity->getMax().y <= area.second.y)
    {
        result = true;//is splitter
    }
    return result;
}

bool isEntityInOrCoveredByEntity(FAS_Entity* thisEntity, FAS_Entity* e){
    bool result = false;
    auto minEP = e->getMin();
    auto maxEP = e->getMax();
    auto minE = thisEntity->getMin();
    auto maxE = thisEntity->getMax();
    if((minEP.x <= minE.x
             && minEP.y <= minE.y)
             &&( maxEP.x >= maxE.x
             && maxEP.y >= maxE.y)){
            result = true;//is splitter
    }
    return result;
}

bool isEntityInEntity(FAS_Entity* entity, FAS_Entity* entityParent){
    bool result = false;
//     // qDebug() <<"parent max x" << entityParent->getMax().x;
//     // qDebug() <<"parent max y" << entityParent->getMax().y;
//     // qDebug() <<"parent min x" << entityParent->getMin().x;
//     // qDebug() <<"parent min y" << entityParent->getMin().y;


    auto minEP = entityParent->getMin();
    auto maxEP = entityParent->getMax();
    auto minE = entity->getMin();
    auto midE = entity->getMiddlePoint();
    auto maxE = entity->getMax();
    if(entity->rtti() == FAS2::EntityInsert){
        if((minEP.x <= midE.x
                && minEP.y <= midE.y)&&(
                 maxEP.x >= midE.x
                && maxEP.y >= midE.y))
        {
            result = true;//is splitter
//            // qDebug() <<"child max x" << entity->getMiddlePoint().x;
//            // qDebug() <<"child max y" << entity->getMiddlePoint().y;
//            // qDebug() <<"child min x" << entity->getMiddlePoint().x;
//            // qDebug() <<"child min y" << entity->getMiddlePoint().y;
        }

    }else{
        if((minEP.x <= minE.x
                && minEP.y <= minE.y)
                &&( maxEP.x >= maxE.x
                && maxEP.y >= maxE.y))
        {
            result = true;//is splitter
//            // qDebug() <<"child max x" << entity->getMax().x;
//            // qDebug() <<"child max y" << entity->getMax().y;
//            // qDebug() <<"child min x" << entity->getMin().x;
//            // qDebug() <<"child min y" << entity->getMin().y;
        }

    }
    return result;
}

FAS_Entity* getTheMaxEntity(FAS_Entity* e1, FAS_Entity* e2){
    if(e1 == nullptr){
        return e2;
    }else if(e2 == nullptr){
        return e1;
    }
    FAS_Entity* result = e2;
    if((e1->rtti() == FAS2::EntityText || e1->rtti() == FAS2::EntityMText)
            && (e2->rtti() == FAS2::EntityText || e2->rtti() == FAS2::EntityMText)){
        auto th01 = e1->rtti() == FAS2::EntityText
                ? ((FAS_Text*)e1)->getHeight()
                : ((FAS_MText*)e1)->getHeight();
        auto th02 = e2->rtti() == FAS2::EntityText
                ? ((FAS_Text*)e2)->getHeight()
                : ((FAS_MText*)e2)->getHeight();
        if(th01 >= th02){
            result = e1;
        }
    }
    else if(e1->getSize().magnitude() >= e2->getSize().magnitude()){
        result = e1;
    }
    return  result;
}

bool isBlockWithValidOutlines(FAS_Entity* e){

    if(e->rtti() == FAS2::EntityInsert){
        FAS_Vector blockLengthVector=e->getSize();
        if(blockLengthVector.magnitude()<1000){
            return false;
        }

        // qDebug() <<"Begin check block ID " << e->getInsert()->getId();
        QList<QList<FAS_Entity*>> lists= FASUtils::getExistAtomicEntityList((FAS_EntityContainer*)e);
        auto lines  = lists.at(1);

        if(lines.size()>=1){
            return true;
        }
//        // qDebug() <<"Line numbers in block" << lines.size();
//        if(lines.size()<4){
//            return false;
//        }

//        int lineNumber=0;
//        double blockMinX=e->getMin().x;
//        double blockMinY=e->getMin().y;
//        double blockMaxX=e->getMax().x;
//        double blockMaxY=e->getMax().y;

//        // qDebug() <<"Block X " <<blockX;
//        // qDebug() <<"Block Y " <<blockY;

//        foreach(auto l, lines){
//            double lineMinX=l->getMin().x;
//            double lineMinY=l->getMin().y;
//            double lineMaxX=l->getMax().x;
//            double lineMaxY=l->getMax().y;
////            // qDebug() <<"line X " <<lineX;
////            // qDebug() <<"line Y " <<lineY;
////            FAS_Vector lineLengthVector=l->getSize();

////            // qDebug() <<"lineX-blockX " <<abs(lineX-blockX);
////            // qDebug() <<"blockY-lineY " <<abs(blockY-lineY);

//            if((abs(blockMinX-lineMinX)<=100||abs(blockMinY-lineMinY)<=100)
//                    ||(abs(blockMaxX-lineMaxX)<=100||abs(blockMaxY-lineMaxY)<=100)){

//                if(l->rtti()==FAS2::EntityPolyline){
//                     // qDebug() <<"Valid EntityPolyline block ID " << e->getInsert()->getId();
//                    return true;
//                }
//                lineNumber++;
//            }

//        }

//        if(lineNumber==2){
//            // qDebug() <<"Valid Line block ID " << e->getInsert()->getId();
//            return true;
//        }
    }
    return false;
}

QList<FAS_Entity*> getTopSizeEntities(QMultiMap<double,FAS_Entity*> entityMap, int selectTopGroupsNum){
    QList<FAS_Entity*> result;
    int stopSign = 0;
    double keySign = 0;
    auto keys  = entityMap.keys();
    for(int i = keys.size() - 1; i >= 0 ; i--){
        if(keySign != keys.at(i)){
            keySign = keys.at(i);
            stopSign++;
            if(stopSign > selectTopGroupsNum)
                break;
        }
        result.append(entityMap.values(keys.at(i)));
    }
    return result;
}

QMultiMap<double,FAS_Entity*> FASUtils::figureOutTheViewOutlines(QList<FAS_Entity*> allEntities){
    QList<FAS_Entity*> result;

    QMultiMap<double,FAS_Entity*> entities;
    FAS_Entity* maxBlock = nullptr;

    //collect the top 64 maximum entities of lines, polylines and blocks.
    auto figureFactor = 64/*(int)(allEntities.size() * 0.38)*/;
    foreach(auto e, allEntities){
        bool isAlreadyCoverd = false;
        foreach(auto ee, entities){
            if(isEntityInOrCoveredByEntity(e,ee)/*e->isInOrCoveredByEntity(ee)*/){
                isAlreadyCoverd = true;
                break;
            }
        }
        if(isAlreadyCoverd) continue;
        auto eSizeMagnitude = QString::number(e->getSize().magnitude(), 'f',3).toDouble();
        if(figureFactor > 0){
            if(!entities.keys().contains(eSizeMagnitude)){
                figureFactor--;
            }
        }else break;

        if(e->rtti() == FAS2::EntityLine){
//                entities.insert(eSize.magnitude(),e);
        }else if(e->rtti() == FAS2::EntityPolyline){
                entities.insert(eSizeMagnitude,e);
        }else if(e->rtti() == FAS2::EntityInsert){
            bool isValid = isBlockWithValidOutlines(e);
            if(isValid){
                entities.insert(eSizeMagnitude,e);
            }
        }
    }

    //remove noise
    /*QMultiMap<double,FAS_Entity*>::Iterator it;
    for(it=Lines.begin(); it != Lines.end();){
        if(Lines.values(it.key()).count() == 1){
            Lines.erase(it++);
            continue;
        }
        it++;
    }*/
//    QMutableMapIterator<double,FAS_Entity*> it(entities);
//    while (it.hasNext()) {
//        if(entities.values(it.next().key()).count() == 1){
//            it.remove();
//        }
//    }


//    //Pick out the top N level maximum entity groups.
//    int outlineFactorN = COMMONDEF->getSubViewOutlineCollectionFactor();
//    if(Lines.size() > 0){
//        result.append(getTopSizeEntities(Lines,outlineFactorN));
//    }
//    if(PolyLines.size() > 0){
//        result.append(getTopSizeEntities(PolyLines,outlineFactorN));
//    }
//    if(maxBlocks.size() > 0){
//        result.append(getTopSizeEntities(maxBlocks,outlineFactorN));
//    }
    return entities;
}

int FASUtils::getNextViewIndex(){
    return viewNo;
}
void FASUtils::setNewViewNameStr(QString newViewNameStr){
    this->newViewNameStr = newViewNameStr;
}

QString FASUtils::buildSubView(){
    QString newViewName;
//    subViews4CurDocument.insert("OriginalView",originDocument);

    FAS_BlockList* curBlockList = curDocument->getBlockList();
    QHash<QString,FAS_Block*> clonedCurBlockHash=curBlockList->getClonedHashBlocks();
    QList<QString> validationList;
    auto nameToDeviceInsertHash  = getBlockNameToEntityHash();
    QList<QPair<FAS_Vector,FAS_Vector>> subViewAreas;

    FAS_Selection s(*this->getRSDocument(), this->getGraphicView());
    QList<FAS_Entity*> allEntities = s.getSelectedEntityList();

    QHash<int, FAS_Entity*> tempIDToEntityMap;
    foreach (FAS_Entity* e, allEntities)
    {
        e->setSelected(false);
         if(e->rtti() == FAS2::EntityInsert){
             tempIDToEntityMap.insert(e->getInsert()->getId(),e);
             IDToEntityMap.insert(e->getInsert()->getId(),e);
         }else{
             tempIDToEntityMap.insert(e->getId(),e);
             IDToEntityMap.insert(e->getId(),e);
         }
    }

    QString viewName = "";
    if(tempIDToEntityMap.size() == 0){
        // qDebug() <<"break look Map data ";
        return "Invalid View";
    }
    auto subDocument = new FAS_Graphic();
    bool hasValue = false;
    FAS_Entity* maxText = nullptr;
    foreach(FAS_Entity* entity,allEntities){
        if(tempIDToEntityMap.size() == 0){
            // qDebug() <<"View is created.";
            break;
        }
        FAS_Entity* child = nullptr;
        if(entity->rtti() == FAS2::EntityInsert){
            child=tempIDToEntityMap.value(entity->getInsert()->getId());
            if(!child || child == nullptr){
                continue;
            }
            subDocument->addEntity(tempIDToEntityMap.take(entity->getInsert()->getId()));
//            child->update();
            auto b= child->getInsert()->getBlockForInsert();
            if(nullptr != b){
                QString name = b->getName();
                if(nullptr == curBlockList->find(name)){
                    continue;
                }
                if(!validationList.contains(name)){
                    validationList.append(name);
                    FAS_Block* currBlock=curBlockList->find(name)->clone()->getBlock();
                    currBlock->setParent(subDocument);
                    FAS_Block* clonedBlock=clonedCurBlockHash.value(name);
                    clonedBlock->setParent(subDocument);
                    subDocument->addBlock(currBlock);
                    subDocument->getBlockList()->addClone(clonedBlock);
                }
            }
            hasValue=true;
        }else{
            child=tempIDToEntityMap.value(entity->getId());
            if(!child || child==nullptr){
                continue;
            }
            subDocument->addEntity(tempIDToEntityMap.take(child->getId()));
            hasValue=true;
        }
    }

    if(subDocument->getEntityList().size()>20){
//        if(this->curSubViewName.contains("OriginalView")){
            viewName = QString::fromUtf8("%1_%2")
                    .arg(QString("%1").arg(viewNo, 2, 10, QLatin1Char('0')))
                    .arg(newViewNameStr);
                    /*.arg(maxText != nullptr ? maxText->rtti() == FAS2::EntityText
                                              ? ((FAS_Text*)maxText)->getText()
                                              : ((FAS_MText*)maxText)->getText()
                                              : "InvalidView")*/;
            QString subView = QString::fromUtf8("%1[View-%2]")
                    .arg(originDocument->getFilename())
                    .arg(viewName);
            subDocument->setFilename(subView);
            subViews4CurDocument.insert(viewName,subDocument);

            newViewName = viewName;
            viewNo++;
            newViewNameStr.clear();
//        }else{
//            subViews4CurDocument.insert(this->curSubViewName,subDocument);

//        }

        // qDebug() <<"Set parent block view no " << viewNo;
    }else{
        return QString(tr("Info: Selecting Less Than 20 Entities Caused Invalid Operation."));
    }
    refreshGraphicView();
    return newViewName;
}

int FASUtils::countExistingAtomicInserts(FAS_Insert* insert){
    int count = 1;
    auto innerInterts = insert->getInsertList();
    foreach(auto insert, innerInterts){
        auto innerBlocksCount = countExistingAtomicInserts(insert);
        count += innerBlocksCount;
    }
    return count;
}

void FASUtils::buildSubViews(){
    // qDebug() <<"Auto buildSubViews start";
    int clonedBlockCount = 0;

        FAS_BlockList* curBlockList = originDocument->getBlockList();
        subViews4CurDocument.insert("OriginalView",originDocument);
        QHash<QString,FAS_Block*> clonedCurBlockHash=curBlockList->getClonedHashBlocks();
        QList<QString> validationList;
        auto nameToDeviceInsertHash  = getBlockNameToEntityHash();
        QList<QPair<FAS_Vector,FAS_Vector>> subViewAreas;

        originDocument->sortEntities();
        QList<FAS_Entity*> allEntities = originDocument->getEntityList();
        QHash<int, FAS_Entity*> tempIDToEntityMap;
        foreach (FAS_Entity* e, allEntities)
        {
             if(e->rtti() == FAS2::EntityInsert){
                 tempIDToEntityMap.insert(e->getInsert()->getId(),e);
                 IDToEntityMap.insert(e->getInsert()->getId(),e);
             }else{
                 tempIDToEntityMap.insert(e->getId(),e);
                 IDToEntityMap.insert(e->getId(),e);
             }
        }

        auto viewOutline = figureOutTheViewOutlines(allEntities);        

        QString viewName = "Auto-Subview";

        QMultiMap<double,FAS_Entity*>::Iterator   MapItor;
        QList<double> countedViewOutLines;
        for(MapItor = --viewOutline.end();MapItor != --viewOutline.begin();MapItor--){
            auto key = MapItor.key();
            auto isCounted = countedViewOutLines.contains(key);
            if(countedViewOutLines.size()>=16){
                break;
            }else if(isCounted){
                continue;
            }else{
                countedViewOutLines.append(key);
            }

            if(tempIDToEntityMap.size()==0){
                // qDebug() <<"break look Map data ";
                break;
            }
            QList<FAS_Entity*> values=viewOutline.values(key);
            int valuesSize=values.size();
            foreach (FAS_Entity* value, values) {
                // qDebug() <<"Set parent block size " << MapItor.key();
                // qDebug() <<"Set parent block " << value->getId();
                QPair<FAS_Vector,FAS_Vector> boarder;
                QHash<int, FAS_Entity*>::Iterator   entityItor;
                auto subDocument = new FAS_Graphic();
                subDocument->addEntity(value);
                bool hasValue=false;
                FAS_Entity* maxText = nullptr;
                foreach(FAS_Entity* entity,allEntities){

                    if(tempIDToEntityMap.size()==0){
                        // qDebug() <<"break look Map data ";
                        break;
                    }

                    FAS_Entity* child=nullptr;
                    if(entity->rtti() == FAS2::EntityInsert){
                        child=tempIDToEntityMap.value(entity->getInsert()->getId());
                        if(!child || child==nullptr){
                            continue;
                        }
                        if(!isEntityInEntity(child,value)){
                            continue;
                        }
                        subDocument->addEntity(tempIDToEntityMap.take(entity->getInsert()->getId()));
//                        subDocument->addBlock((FAS_Block*)(child));
                        auto b= child->getInsert()->getBlockForInsert();
                        if(nullptr != b){
                            QString name = b->getName();
                            if(nullptr == curBlockList->find(name)){
                                continue;
                            }
                            if(!validationList.contains(name)){
                                validationList.append(name);
                                FAS_Block* currBlock=curBlockList->find(name)->clone()->getBlock();

                                currBlock->setParent(subDocument);
                                FAS_Block* clonedBlock=clonedCurBlockHash.value(name);
                                clonedBlock->setParent(subDocument);
                                subDocument->addBlock(currBlock);
                                subDocument->getBlockList()->addClone(clonedBlock);
                            }

                        }
                        hasValue=true;
                    }else{
                        child=tempIDToEntityMap.value(entity->getId());
                        if(!child || child==nullptr){
                            continue;
                        }
                        if(!isEntityInEntity(child,value)){
                            continue;
                        }

                        subDocument->addEntity(tempIDToEntityMap.take(child->getId()));
                        hasValue=true;
                    }
                }



                auto entityCountInView = subDocument->getEntityList().size();
                if(subDocument->getEntityList().size()>20){
                    viewName = QString::fromUtf8("%1_%2")
                            .arg(QString("%1").arg(viewNo, 2, 10, QLatin1Char('0')))
                            .arg(maxText != nullptr ? maxText->rtti() == FAS2::EntityText
                                                      ? ((FAS_Text*)maxText)->getText()
                                                      : ((FAS_MText*)maxText)->getText()
                                                      : viewName);
                    QString subView = QString::fromUtf8("%1[View-%2]")
                            .arg(originDocument->getFilename())
                            .arg(viewName);
                    subDocument->setFilename(subView);
                    subViews4CurDocument.insert(viewName,subDocument);
                    validationList.clear();
                    viewName.clear();
                    subView.clear();

                    viewName = "Auto-Subview";
                    viewNo++;

                    // qDebug() <<"Set parent block view no " << viewNo;
                }



            }
            values.clear();
        }
        countedViewOutLines.clear();
}

QMap<QString,QMap<QString,QMap<QString,int>>> FASUtils::getRecognizedDevicesData()
{
    return this->recognizedDevicesMap;
}

void FASUtils::addRecognizedDevicesData(QString storeyName, QString loopName, QString deviceName, int deviceNumber){
    QMap<QString,int> newDevice;
    QMap<QString,QMap<QString,int>> newLoop;
    //QMap<QString,QMap<QString,QMap<QString,int>>>
    auto storeyIt = this->recognizedDevicesMap.find(storeyName);
    if(storeyIt != this->recognizedDevicesMap.end())
    {
        auto loopIt = storeyIt.value().find(loopName);
        if(loopIt != storeyIt.value().end())
        {
            auto deviceIt = loopIt.value().find(deviceName);
            if(deviceIt != loopIt.value().end()){
                deviceIt.value() += deviceNumber;
            }else{
                loopIt.value().insert(deviceName,deviceNumber);
            }
        }else{
            newDevice.insert(deviceName,deviceNumber);
            storeyIt.value().insert(loopName,newDevice);
        }
    }else{
        newDevice.insert(deviceName,deviceNumber);
        newLoop.insert(loopName,newDevice);
        this->recognizedDevicesMap.insert(storeyName,newLoop);
    }
}

bool FASUtils::updateStorey4RecognizedDevices(QString storeyName, QString newStoreyName){
    bool result = false;
    auto storeyIt = this->recognizedDevicesMap.find(storeyName);
    if(storeyIt != this->recognizedDevicesMap.end())
    {
        auto value = storeyIt.value();
        this->recognizedDevicesMap.erase(storeyIt);
        this->recognizedDevicesMap.insert(newStoreyName,value);
    }
    return result;
}

//TODO 手动设置设备父节点属性
bool FASUtils::setDeviceParent(int childID, int parentID){
    auto entityIt=this->IDToEntityMap.find(childID);
    auto entityParentIt=this->IDToEntityMap.find(parentID);

    if(entityIt!=IDToEntityMap.end() && entityParentIt!=IDToEntityMap.end()){
        if(!entityIt.value()->hasCalParent()){
            entityIt.value()->setCalParent(entityParentIt.value());
            entityParentIt.value()->setCalChilds(entityIt.value());
            int signId = entityIt.value()->getInsert()->getId();
            setNewCodeToEntityByID(signId,QString::fromUtf8("De01i-%1").arg(signId));
        }
    }



    return true;
}

//TODO 设备回路识别
QHash<QString,QString> FASUtils::detectDeviceBranch(){
//    buidMatrixToEntitiesMap();
//    buidMatrixToBlocksMapping();
    QHash<QString,QString> result;
    result.insert("test01","test010");
    return result;
}

QString FASUtils::getCurrentViewName(){
    return curSubViewName;
}

void FASUtils::setCurrentViewName(QString viewName){
    this->curSubViewName = viewName;
}
QMap<QString,QMap<QString,int>> FASUtils::getRecognitionResult(){
    return finalRecognitionResult;
}
void FASUtils::setRecognitionResult(QString viewName, QMap<QString,int> viewDectionResult){
    finalRecognitionResult.insert(viewName,viewDectionResult);
}

void FASUtils::deleteRecognitionResult4View(QString viewName){
    if(finalRecognitionResult.find(viewName) != finalRecognitionResult.end()){
        finalRecognitionResult.remove(viewName);
    }
}

QMap<QString,QMap<QString,int>> FASUtils::getCustomizedResult(){
    return finalCustomizedResult;
}
void FASUtils::deleteCustomizedResult4View(QString viewName){
    if(finalCustomizedResult.find(viewName) != finalCustomizedResult.end()){
        finalCustomizedResult.remove(viewName);
    }
}
void FASUtils::setCustomizedResult(QString viewName, QMap<QString,int> viewCustomizedResult){
    if(finalCustomizedResult.find(viewName) != finalCustomizedResult.end()){
        if(viewCustomizedResult.size() == 0){
            finalCustomizedResult.take(viewName);
        }else{
            finalCustomizedResult.insert(viewName,viewCustomizedResult);
        }

    }else{
        finalCustomizedResult.insert(viewName, viewCustomizedResult);
    }
}

bool FASUtils::isViewDetected(QString viewName){
    bool result = false;
    auto existViewNames = getSubViews();
    foreach(auto viewName, existViewNames){
        if(!viewName.contains(viewName,Qt::CaseInsensitive))
            return result;
    }
    if(subViews4CurDocument.find(viewName) != subViews4CurDocument.end()){
        //Update subViews map data.
        auto curViewData = subViews4CurDocument.value(viewName);
        result = curViewData->getDetectedStatus();
    }
    return result;
}

bool FASUtils::updateCurrentViewName(QString previousViewName,QString newViewName){
    bool result = false;
    auto existViewNames = getSubViews();
    foreach(auto viewName, existViewNames){
        if(viewName.contains(newViewName,Qt::CaseInsensitive))
            return result;
    }
    if(subViews4CurDocument.find(previousViewName) != subViews4CurDocument.end()){
        //Update subViews map data.
        auto curViewData = subViews4CurDocument.value(previousViewName);
        subViews4CurDocument.remove(previousViewName);
        subViews4CurDocument.insert(newViewName,curViewData);
        setCurrentViewName(newViewName);

        if(finalRecognitionResult.find(previousViewName) != finalRecognitionResult.end()){
            //Update final recognition result map data
            auto curViewRecognitionData = finalRecognitionResult.value(previousViewName);
            finalRecognitionResult.remove(previousViewName);
            finalRecognitionResult.insert(newViewName,curViewRecognitionData);
        }
        if(recognizedDevices.find(previousViewName) != recognizedDevices.end()){
            //Update final recognition result map data
            auto curViewRecognitionData = recognizedDevices.value(previousViewName);
            recognizedDevices.remove(previousViewName);
            recognizedDevices.insert(newViewName,curViewRecognitionData);
        }
        result = true;
    }
    return result;
}

void FASUtils::deleteCurrentView(QString viewName){
    if(!subViews4CurDocument.contains(viewName)){
        return;
    }else{
        subViews4CurDocument.remove(viewName);
        deleteCustomizedResult4View(viewName);//delete data of view for final uploading
        deleteRecognitionResult4View(viewName);//delete data of view for final uploading
        deleteRecognizedDevices4View(viewName);//delete data of view for local display
    }
}

void FASUtils::addDeviceImagesToBeUpdate4AIList(QString imageName, FAS_Block* device)
{
    if(this->deviceImagesToBeUpdate4AIList.find(imageName) != this->deviceImagesToBeUpdate4AIList.end()){
        this->deviceImagesToBeUpdate4AIList.remove(imageName);
    }
    this->deviceImagesToBeUpdate4AIList.insert(imageName, device);
}
QMap<QString,FAS_Block*> FASUtils::getDeviceImagesToBeUpdate4AIList()
{
    return this->deviceImagesToBeUpdate4AIList;
}

void FASUtils::clearCacheDeviceImagesAfterUpdate(){
//    qDeleteAll(deviceImagesToBeUpdate4AIList);
    this->deviceImagesToBeUpdate4AIList.clear();
}

void FASUtils::eraseCacheDeviceImageAfterUpdate(QString keyName){
    this->deviceImagesToBeUpdate4AIList.remove(keyName);
}

void FASUtils::clearDir(const QString& temp_path)
{
    QDir Dir(temp_path);
    if(Dir.isEmpty())
    {
//        std::cout << "临时文件文件夹" << path << "为空";
        return;
    }
    Dir.removeRecursively();


//    //获取所选文件类型过滤器
//    QStringList filters;
//    filters<<QString("*.jpeg")<<QString("*.jpg")<<QString("*.png")<<QString("*.tiff")<<QString("*.gif")<<QString("*.bmp");
//    // 第三个参数是QDir的过滤参数，这三个表示收集所有文件和目录，且不包含"."和".."目录。
//    // 因为只需要遍历第一层即可，所以第四个参数填QDirIterator::NoIteratorFlags
//    QDirIterator dirsIterator(temp_path, QDir::Files | QDir::AllDirs | QDir::NoDotAndDotDot, QDirIterator::NoIteratorFlags);
//    while(dirsIterator.hasNext())
//    {
//        if (!Dir.remove(dirsIterator.next())) // 删除文件操作如果返回否，那它就是目录
//        {
//            QDir(dirsIterator.filePath()).removeRecursively(); // 删除目录本身以及它下属所有的文件及目录
//        }
//    }
}

void FASUtils::addBlocksOfView(QString viewName,QList<FAS_Block*> blockList){
    if(this->blocksOfViews.find(viewName) == this->blocksOfViews.end()){
        this->blocksOfViews.insert(viewName, blockList);
    }
}

QList<FAS_Block*> FASUtils::getBlocksOfView(QString viewName){
    return this->blocksOfViews.value(viewName);
}


void FASUtils::addRecognizedDevices(QString viewName, QList<CadDeviceDataDetect*> viewDeviceList){

    if(this->recognizedDevices.find(viewName) != this->recognizedDevices.end()){
        this->recognizedDevices.find(viewName).value().clear();
        this->recognizedDevices.find(viewName).value().append(viewDeviceList);
    }else{
        this->recognizedDevices.insert(viewName, viewDeviceList);
    }
}
QMap<QString,QList<CadDeviceDataDetect*>> FASUtils::getRecognizedDevices(){
    return this->recognizedDevices;
}

void FASUtils::deleteRecognizedDevices4View(QString viewName){
    if(this->recognizedDevices.find(viewName) != this->recognizedDevices.end()){
        this->recognizedDevices.remove(viewName);
    }
}

QString FASUtils::getEntityTypeNameStr(FAS2::EntityType type){
    QString result = "null";
    switch (type) {
    case FAS2::EntityUnknown:
        result = tr("EntityUnknown");
        break;
    case FAS2::EntityContainer:
        result = tr("EntityContainer");
        break;
    case FAS2::EntityBlock:
        result = tr("EntityBlock");
        break;
    case FAS2::EntityFontChar:
        result = tr("EntityFontChar");
        break;
    case FAS2::EntityInsert:
        result = tr("EntityInsert");
        break;
    case FAS2::EntityGraphic:
        result = tr("EntityGraphic");
        break;
    case FAS2::EntityPoint:
        result = tr("EntityPoint");
        break;
    case FAS2::EntityLine:
        result = tr("EntityLine");
        break;
    case FAS2::EntityPolyline:
        result = tr("EntityPolyline");
        break;
    case FAS2::EntityVertex:
        result = tr("EntityVertex");
        break;
    case FAS2::EntityArc:
        result = tr("EntityArc");
        break;
    case FAS2::EntityCircle:
        result = tr("EntityCircle");
        break;
    case FAS2::EntityEllipse:
        result = tr("EntityEllipse");
        break;
    case FAS2::EntitySolid:
        result = tr("EntitySolid");
        break;
    case FAS2::EntityConstructionLine:
        result = tr("EntityConstructionLine");
        break;
    case FAS2::EntityMText:
        result = tr("EntityMText");
        break;
    case FAS2::EntityText:
        result = tr("EntityText");
        break;
    case FAS2::EntityDimAligned:
        result = tr("EntityDimAligned");
        break;
    case FAS2::EntityDimLinear:
        result = tr("EntityDimLinear");
        break;
    case FAS2::EntityDimRadial:
        result = tr("EntityDimRadial");
        break;
    case FAS2::EntityDimDiametric:
        result = tr("EntityDimDiametric");
        break;
    case FAS2::EntityDimAngular:
        result = tr("EntityDimAngular");
        break;
    case FAS2::EntityDimLeader:
        result = tr("EntityDimLeader");
        break;
    case FAS2::EntityHatch:
        result = tr("EntityHatch");
        break;
    case FAS2::EntityImage:
        result = tr("EntityImage");
        break;
    case FAS2::EntitySpline:
        result = tr("EntitySpline");
        break;
    case FAS2::EntitySplinePoints:
        result = tr("EntitySplinePoints");
        break;
    case FAS2::EntityOverlayBox:
        result = tr("EntityOverlayBox");
        break;
    case FAS2::EntityPreview:
        result = tr("EntityPreview");
        break;
    case FAS2::EntityPattern:
        result = tr("EntityPattern");
        break;
    case FAS2::EntityOverlayLine:
        result = tr("EntityOverlayLine");
        break;
    default:
        break;
    }
    return result;
}
