/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#include "Ifilter.h"

//sort with alphabetical order
#include <cmath>
#include <iostream>

#include "commondef.h"
#include "fas.h"
#include "fas_arc.h"
#include "fas_blocklist.h"
#include "fas_circle.h"
#include "fas_ellipse.h"
#include "fas_graphic.h"
#include "fas_graphicview.h"
#include "fas_layer.h"
#include "fas_math.h"
#include "fas_painterqt.h"
#include "fasutils.h"

FilterFactorData::FilterFactorData(const QString& _name,
                               FAS_Vector _insertionPoint,
                               FAS_Vector _scaleFactor,
                               double _angle,
                               int _cols, int _rows, FAS_Vector _spacing,
                               FAS_BlockList* _blockSource ,
                               FAS2::UpdateMode _updateMode ):
    name(_name)
  ,insertionPoint(_insertionPoint)
  ,scaleFactor(_scaleFactor)
  ,angle(_angle)
  ,cols(_cols)
  ,rows(_rows)
  ,spacing(_spacing)
  ,blockSource(_blockSource)
  ,updateMode(_updateMode)
  ,deviceInfo(COMMONDEF->getNewNotDeviceInfo())
{
}

std::ostream& operator << (std::ostream& os, const FilterFactorData& d)
{
    os << "(" << d.name.toLatin1().data() << ")";
    return os;
}

IFilter::IFilter()
{
}

IFilter::~IFilter()
{
    if(data.deviceInfo != nullptr)
    {
        delete data.deviceInfo;
        data.deviceInfo = nullptr;
    }
}

FAS_Entity* IFilter::clone() const
{
    return NULL;
}
/*
// Updates the entity buffer of this insert entity.
void Filter::update()
{

}

// return Pointer to the block associated with this Insert
FAS_Block* Filter::getBlockForInsert() const
{

}




*/

std::ostream& operator << (std::ostream& os, const IFilter& i)
{
    os << " Insert: " << i.getData() << std::endl;
    return os;
}

void IFilter::setDeviceInfo(DeviceInfo* info)
{
    data.deviceInfo = info;
}

DeviceInfo* IFilter::getDeviceInfo() const
{
    return data.deviceInfo;
}

