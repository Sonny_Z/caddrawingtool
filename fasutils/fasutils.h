﻿#ifndef FASUTILS_H
#define FASUTILS_H

#include <QTimer>
#include <QColor>
#include "commondef.h"
#include "deviceinfo.h"
#include "fas_document.h"
#include "fas_graphic.h"
#include "lc_centralwidget.h"
#include "qc_mdiwindow.h"
#include "qg_graphicview.h"
#include "fas_mtext.h"
#include "fas_staticgraphicview.h"
#include "fas_painterqt.h"
#include "fas_selection.h"
#include "caddevicedatadetect.h"

struct DeviceFeature
{
    DeviceFeature(){}
    DeviceFeature(int c, int l, int a, int h, int p, double s, QString text)
        :circles(c), lines(l), arcs(a), hatchs(h), solids(p), size(s), text(text)
    {}

    int circles = 0;
    int lines = 0;
    int arcs = 0;
    int hatchs = 0;
    int solids = 0;
    double size = 0.0;
    QString text = "";
};

struct DetectionRule
{
    DeviceFeature feature;
    QString deviceType = "NotDevice";
    QString systemType = "UnknownSystem";
    bool isStored = false;
};

struct ViewData
{
    QMap<QString,FAS_Document*> subViewDocs;
    FAS_Entity* maxSizeEntity;
    FAS_Entity* maxText;
    QString curViewName = "OriginalView";

};

class FASUtils : public QObject
{
    Q_OBJECT
public:
    static FASUtils* getInstance();
    ~FASUtils();

    //APIs for IES Recognition start
    //put all lines point in 200*200 matrix square.

    //from vecoter to matrix point id
    static QString getPointMatrixId(FAS_Vector point);

    //APIs for IES Recognition end
    void clear();
    static void initDataBase();
    void initDetectionRules();
    bool isDirExist(QString fullPath);
    bool openFile(QString filePath);
    bool activeExistingFile(QString file);
    bool saveFile(QString filePath,  bool isAutoSave = false);
    bool saveAsFile(QString filePath, FAS2::FormatType type);
    bool exportFileAsImage(QString filePath);
    bool exportFileAsPDF(QString filePath);
    bool doExportFileAsImage(const QString& name, const QString& format,
                      QSize size, QSize borders, bool black);
    bool doConvertDeviceToImage(const QString& baseDir,const QString& name, const QString& format,
                                FAS_EntityContainer* c, QSize size, bool black = true);
    static bool svgGenerator(const QString& baseDir,const QString& name,
                      FAS_EntityContainer* c, QSize size, const QString& format = "svg", bool black = true);
    bool closeFile(QString filePath);
    void createGraphicView(QC_MDIWindow* mdiWindow);
    void createSubGraphicView(QC_MDIWindow* mdiWindow,const QString& subViewName);

    // This API recieves a code information(QString), and return the entity.
    FAS_Entity* findDeviceByCode(QString code);

    //Get Device from DB/RS document.
    // From RS document is recommanded, this is much more robutsness.
    QList<QString> getAllDevicesFromDB();
    QList<FAS_Insert*> getAllDevicesFromRS();

    QList<QString> getDevicesByTypeFromDB(DeviceInfo* type);
    QList<FAS_Insert*> getDevicesByTypeFromRS(DeviceInfo* type);

    // The show/hide APIs.
    int showAllDevices(bool show);
    int showDeviceByCode(QString code, bool show);
    int showDeviceByCodeList(QList<QString>& codeList, bool show);
    int showDevicesByType(DeviceInfo* info, bool show);
    void showEntityList(QList<FAS_Insert*>& input, bool show);
    void showEntitiesByLayer(QString layer, bool show);
    void twinkleEntity(QString code, bool twinkle);

    //Getter/Setter of RS document and qg_graphicView
    FAS_Document* getRSDocument();
    FAS_Document* getORSDocument();
    void setRSDocument(FAS_Document* document);
    void setORSDocument(FAS_Document* document);
    QG_GraphicView* getGraphicView();
    void setGraphicView(QG_GraphicView* view);
    QG_GraphicView* getSubGraphicView();
    void setSubGraphicView(QG_GraphicView* view);

    QMultiMap<double,FAS_Entity*> figureOutTheViewOutlines(QList<FAS_Entity*> allEntities);
    void buildSubViews();
    int countExistingAtomicInserts(FAS_Insert* insert);
    QString buildSubView();
    void setNewViewNameStr(QString newViewNameStr);
    QString newViewNameStr;
    int getNextViewIndex();
    void addSubView(FAS_Document* view);
    QList<QString> getSubViews();
    QMultiHash<QString, FAS_Document*> getSubDocuments();


    //get all insert entities from graphic view.
    static QList<FAS_Insert*> getAllInsertEntities(FAS_EntityContainer* container);
    static QList<FAS_Insert*> getAllInsertEntities(QList<FAS_Entity*> container);

    //IES CAD Recognition Start
    //QMultiHash<QString, QMultiHash<QString, QList<FAS_Entity*>>> matrixToEntitiesMap;
    void buidMatrixToEntitiesMap();
    void buidMatrixToBlocksMapping();
    QHash<QString,QString> buildBridgePointToLineEntityMap();
    QHash<QString, QHash<QString, QList<FAS_Entity*>>> getMatrixToEntitiesMap();
    QHash<QString, QList<FAS_Entity*>> getPointToEntities(QString matrixId);
    QSet<QString> getMatrixNeighbours(QString matrixId);
    //extends the block max size to retrieve matrix ids
    QSet<QString> getBlocksMatrixs(FAS_Insert* block,double xExtends,double yExtends);
    QSet<QString> getPointMatrixs(QString pointID,int xExpansionFactor,int yExpansionFactor);
    QSet<QString> getLineMatrixs(FAS_Entity* line,int xExpansionFactor,int yExpansionFactor);
    int getPointSizeInMatrix(QString matrixId);
    QList<FAS_Entity*> getEntitesFromMatrixandPoint(QString matrixId,QString pointId);
    int getEntitySizeByPointInMatrix(QString matrixId,QString pointId);
    bool setEntityToMatrixToEntitiesMap(FAS_Vector point,FAS_Entity*);
    QList<QString> getPointsFromMatrixs(QSet<QString> matrixIDs);
    QList<FAS_Entity*> getPointsEntitiesFromMatrixs(QSet<QString> matrixIDs);

    QList<FAS_Entity*> getLineFromPointToLineEntitiesMap(QString pointID);
    int getLineNumberFromPointToLineEntitiesMap(QString pointID);

    void removeLineFromPointToLineEntitiesMap(QString pointID,FAS_Entity* line);
    //one block must have one parent. if no parent, it is the root block
    void setIESBlockParent();
    void setBlockParentLoop(FAS_Entity** oneBlock);
    //点和块的关系，返回数字：10一定是，0一定不是，根据可能性的大小，会返回一个可能性比例
    int getPointAndBlockPositionRating(FAS_Entity** firstBlock,FAS_Entity** oneBlock,FAS_Vector point);
    int getPointAndNearBlockPositionRating(FAS_Vector point);
    FAS_Vector findNextPoint(FAS_Entity** beginningBlock,FAS_Entity** oneBlock,FAS_Vector point,FAS_Entity* oneLine);
    FAS_Vector getVectorFromPoint(QString point);

    FAS_Entity* findConnectedBlock(QString point);

    bool submitTransaction();
    bool rollbackTransaction();

    QString getAnotherPointFromPointToLineEntitiesMap(QString pointID,FAS_Entity* line);
    FAS_Vector getAnotherVecorFromPointToLineEntitiesMap(QString pointID,FAS_Entity* line);
    double getPointandLinePosition(FAS_Entity* line,FAS_Vector vector);
    static void markDeviceChildrenBranch(FAS_Entity* block);
    //IES CAD Recognition End


    // Build the map between code and entity.
    void buildCodeToDeviceMap();

    QMultiHash<QString, FAS_Entity*>* getCodeToDeviceMapForMutation();

    //get code entity hash.QHash<QString, FAS_Entity*> key is the code text.
    QHash<QString, FAS_Entity*> getAllCodeEntityHash();

    //get note text entity hash.QHash<QString, FAS_Entity*> key is the note text.
    //same text may exist, so should be multihash.
    QMultiHash<QString, FAS_Entity*> getAllNoteEntityHash();

    // find nearest valid code entity for device.
    bool findCodeForDevice(FAS_Insert* device, QList<FAS_Entity *>& codeEntityList, QString& code);

    // find nearest valid note text entity for device.
    FAS_Entity* findNoteEntityForDevice(FAS_Insert* device, QList<FAS_Entity *>& noteEntityList, QString& note);

    //Get entities in block, including circle, line, arc, hatch, point and others.
    static QList<QList<FAS_Entity*>> getNeededEntityList(FAS_EntityContainer *container);
    //Get entities in block, including circle, line, arc, hatch, point and others for image recognition.
    static QList<QList<FAS_Entity*>> getEntityListForImage(FAS_EntityContainer* &container);
    //Get atomic entities out of blocks in document, including circle, line, arc, hatch, point, text and others.
    static QList<QList<FAS_Entity*>> getExistAtomicEntityList(FAS_EntityContainer *container);

    //Get counts of entities in block, including circle, line, arc, hatch, point and others.
    static DeviceFeature getDeviceFeature(FAS_EntityContainer *container);

    // Calculate the size of block, which is defined as length of all lines, arcs and circles.
    static double getEntitySize(FAS_EntityContainer* block);

    // Detect the type of input device, detectList can be obtained from getNeededEntityCount() and
    // entitySize can be obtained from getEntitySize().
    DeviceInfo* detectDeviceType(DeviceFeature deviceFeature);

    //Clarify all entities by layer.
    void clarifyLayerEntities(QMultiHash<QString, FAS_Entity*>& hashMap, QHash<int, FAS_Entity*>& IDMap);

    // Get all entities on given layer.
    QList<FAS_Entity*> getEntityListOnLayer(QString layerName, QMultiHash<QString, FAS_Entity*>& hash);

    // get all devices from input loop(loop contains devices and wires)
    QList<FAS_Insert*> getDeviceListInLoop(QList<FAS_Entity*> loop);

    // Get loops. One loop is defined as entities are connected with wire directly.
    QList<QList<FAS_Entity*>> detectLoopList(QList<FAS_Entity*>& wire, QList<FAS_Entity*>& device, QHash<int, FAS_Entity*>& IDMap);

    //Get the upper left point of Insert Block as the insert point of svg/mtext.
    static FAS_Vector getUpperLeftVector(const FAS_Entity* inpuIntity);

    // Generate code text for devices, sort them by type in one loop.
    // allEncoded is return value, presenting whether all devices are encoded
    QHash<QString, FAS_Entity*> generateCodeForLoopDeviceList(QList<QList<FAS_Entity*>>& loops, bool sortByType = true);

    // Show code text for devices in graphic view.
    void addCodeTextInGraphic(QHash<QString, FAS_Entity*>& codeHash, FAS_Layer* layerToAdd);

    //classify all devices by type.
    //returned type is "Device ID" to device map.
    QHash<int, QList<FAS_Insert*>> classifyDevicesByType(QList<FAS_Insert*> deviceList);

    // Sort entities by the device type.
    QList<FAS_Insert*> sortDeviceListByType(QList<FAS_Entity*> entityList);

    //sort entities by coordinate, from left to right, from up to down
    QList<FAS_Insert*> sortDeviceListByCoordinate(QList<FAS_Insert*> entityList);

    // Add one new device type to COMMONDEF.
    // return 1 if the input name is already exist. return 2 if svg file cannot be copied.
    static int addNewDeviceType(QString deviceName, QString svgPath);
    //Update the device database after generate new code.
    //void updateDeviceDataBase(QMultiHash<QString, FAS_Entity*>& codeToDeviceMap);

    //Update the device's position after move.
    //static bool updateDevicePostion(FAS_Entity* oriEntity, FAS_Entity* newEntity);

    int setEntitySelected(FAS_Entity* en);

    static bool isValidDeviceCode(QString codeInfo);
    static bool isValidInsertEntity(FAS_Entity* entity);
    static bool isValidCodeEntity(FAS_Entity* entity, QString& code);

    QMultiHash<QString, FAS_Insert*> getBlockNameToEntityHash();// Get block name to entities hash map.
    bool isDuplicateEntityToBeFiltered(FAS_Insert* objToBeChecked, QMultiHash<QString, FAS_Insert*> statisticsHash); //To fillter the duplicate entity that covered at the same position in the drawing.

    // Add one device with given block and add its code.
    // return o if no error. 1 if unexpected error. 2 if code is invalid. 3 if code is already exist. 4 if code layer is not set.
    int addOneDeviceInGraphic(FAS_Block* refBlock, FAS_Vector pos, QString code);

    // set new code to device entity, if there is no code entity now, create one. And store the information to database.
    // return o if no error. 1 if unexpected error. 2 if code is invalid. 3 if code is already exist. 4 if code layer is not set.
    int setNewCodeToDevice(FAS_Insert* device, QString newCode, QString noteText);
    // set new code to non-device entity, if there is no code entity now, create one. And store the information to database.
    // return o if no error. 1 if unexpected error. 2 if code is invalid. 3 if code is already exist. 4 if code layer is not set.
    int setNewCodeToEntity(FAS_Entity* entity, QString newCode);
    int setNewCodeToEntityByID(int entityID, QString newCode);
    QList<FAS_Entity*> getEntitiesByID(int entityID);
     QList<FAS_Entity*> getEntitiesInBlock(FAS_EntityContainer* myBlock);

    void drawNotes(QString notes, int height, FAS_Vector pos);

    //set Entity highlight
    void setHighlight(FAS_Entity* e, bool isHighlight);

    //get Entity highlight value
    static bool getHighlight(FAS_Entity* e);

    //undo is true when Undo, false when Redo
    void actionUndo(bool undo);

    //delete selected entityies.
    void actionModifyDelete();

    //zoom auto
    void zoomAuto();

    //refresh the graphic view, called when rs entity is added or modified.
    void refreshGraphicView();

    //add detect rule to database.
    //void addDetectRule(DeviceFeature deviceFeature, QString typeStr, QString sytempTypeStr="");

    void addDetectRule(DeviceFeature deviceFeature, QString typeStr,QString sytempTypeStr="", bool isInit = false);
    void addDetectionRuleToLocalDB();
    void clearNewRules4Local();
    void addDetectionRuleToCloud();
    void clearNewRules4Cloud();
    void updateLocalDetectionRuleFromCloud();
    QList<DetectionRule> getDetectionRulesFromCloud();

    //set device info type of blcok.
    static void setBlcokType(FAS_Block* block, DeviceInfo* type);
    // set device type that have same name with given block.
    // Note: Before call this, should set deviceinfo object of all Blcok entities by detecting type with detectDeviceType() and
    // set block type with setBlcokType(). nameToEntityHash can be obtained from getBlockNameToEntityHash().
    static void setAllDeviceEntityType(QList<FAS_Block*> blcokList, QMultiHash<QString, FAS_Insert*> nameToEntityHash);

    //get/set the layer of codes are placed.
    FAS_Layer* getCodeLayer();
    void setCodeLayer(FAS_Layer* layer);

    // create a new layer.
    FAS_Layer* createNewLayer(QString name);

    //get/set wireLineLayer
    FAS_Layer* getWireLineLayer(){return wireLineLayer;}
    void setWireLineLayer(FAS_Layer* layer){
        this->wireLineLayer = layer;
    }

    // set the central widget, which can  be created in mainwindow by new one.
    void setCentralWidget(LC_CentralWidget* central);
    LC_CentralWidget* getCentralWidget();
    // get current mdiwindow.
    QC_MDIWindow* getCurMdiWindow();

    //Change backgroud color.
    void setBackgroudColor(QColor);

    // add device list to database.
    void storeDeviceListToDB(QList<FAS_Insert*> deviceList);

    //Store detection rule to databse
    void storeDetectionRulesToDB();

    static void setFontForUI(QWidget* widget);

    //show svg icon or cad entities of devices
    void showSvgIoconForAllDevices(bool showSVG);

    //calculate length of given wire in layer
    double calculateWireLength(FAS_Layer* layer);

    //set maxium number for devices in loop
    void setMaxNumberInLoop(int max){
        this->maxNumberInLoop = max;
    }
    int getMaxNumberInLoop(){
        return maxNumberInLoop;
    }

    //create one line
    void createLineInGraphic(FAS_Vector start, FAS_Vector end);

    //add new detected block of device
    void addNewDetectdInsertOfDevice(int key, FAS_Insert* Block);

    //clear new detected blocks of devices
    void clearNewDetectdInsertOfDevice();

    QMultiHash<int, FAS_Insert*> getTempInsertOfDeviceMap();

    FAS_Insert* findTempInsertOfDeviceMapByKey(int key);

    QMap<QString,QMap<QString,QMap<QString,int>>> getRecognizedDevicesData();
    void addRecognizedDevicesData(QString storeyName, QString loopName, QString deviceName, int deviceNumber);
    bool updateStorey4RecognizedDevices(QString storeyName, QString newStoreyName);
    bool setDeviceParent(int childID, int parentID);
    QHash<QString,QString> detectDeviceBranch();
    QString getCurrentViewName();
    void setCurrentViewName(QString newViewName);
    bool updateCurrentViewName(QString oldViewName,QString newViewName);
    bool isViewDetected(QString newViewName);

    void deleteCurrentView(QString viewName);
    QMap<QString,QMap<QString,int>> getRecognitionResult();
    void setRecognitionResult(QString viewName, QMap<QString,int> viewDectionResult);
    void deleteRecognitionResult4View(QString viewName);

    QMap<QString,QMap<QString,int>> getCustomizedResult();
    void setCustomizedResult(QString viewName, QMap<QString,int> viewCustomizedResult);
    void deleteCustomizedResult4View(QString viewName);

    void addDeviceImagesToBeUpdate4AIList(QString imageName, FAS_Block* device);
    QMap<QString,FAS_Block*> getDeviceImagesToBeUpdate4AIList();
    void clearCacheDeviceImagesAfterUpdate();
    void eraseCacheDeviceImageAfterUpdate(QString keyName);
    void clearDir(const QString& temp_path);

    static QString getEntityTypeNameStr(FAS2::EntityType type);
    static QList<FAS_Entity*> getAllValidEntities(QList<FAS_Entity*> entities);
    static QList<FAS_Entity*> getAllValidEntities(FAS_EntityContainer* container);
    static int getEntityCountWithBigCondition(FAS_EntityContainer* container,int bigCondition);

    void addRecognizedDevices(QString viewName, QList<CadDeviceDataDetect*> deviceData);
    void deleteRecognizedDevices4View(QString viewName);
    QMap<QString,QList<CadDeviceDataDetect*>> getRecognizedDevices();
    void addBlocksOfView(QString viewName,QList<FAS_Block*> blockList);
    QList<FAS_Block*> getBlocksOfView(QString viewName);

private:
    static FASUtils* singletonUtils;
    FASUtils();

    LC_CentralWidget* central = nullptr;
    FAS_Document* originDocument = nullptr;
    FAS_Document* curDocument = nullptr;
    QMap<QString, FAS_Document*> subViews4CurDocument;
    QString curSubViewName="999_temp";
    ViewData subViewsData;

//    QMdiArea* curMdiAreaCAD = nullptr;
//    QMdiSubWindow* curMainArea = nullptr;
    QC_MDIWindow* curMdiWindow = nullptr;
    QG_GraphicView* qg_graphicView = nullptr;
    QG_GraphicView* qg_subGraphicView = nullptr;
    QMultiHash<QString, FAS_Entity*> codeToDeviceMap;
    QMultiHash<QString, FAS_Entity*> codeToEntityMap;
    QHash<int, FAS_Entity*> IDToEntityMap;

    //for CAD recognition, QMultiHash<MatrixId, QMultiHash<PointId, QList<Entities>>
    QHash<QString, QHash<QString, QList<FAS_Entity*>>> matrixToEntitiesMap;
    //Lind ID to Line entities
    QHash<QString,QList<FAS_Entity*>> pointToLineEntitiesMap;
    //Point ID to Block
    QMultiHash<QString,FAS_Entity*> pointToBlockMap;
    QList<FAS_Entity*> IESController;
    QList<FAS_Entity*> blockTransactionMap;
    QList<FAS_Entity*> lineTransactionMap;
    QMultiHash<QString,FAS_Entity*> pointToLineTransactionMap; 
    QHash<QString,QString> bridgePointMap;
    bool isGroupTransaction=false;

    QMultiHash<int, FAS_Insert*> codeToTempInsertOfDeviceMap;
    QString filePath;
    QHash<QString, FAS_Document*> documentHash;
    QHash<QString, QC_MDIWindow*> mdiWindowHash;

    FAS_Layer* codeLayer = nullptr;
    FAS_Layer* wireLineLayer = nullptr;
    int maxNumberInLoop = 0;
    int viewNo = 1;

    QTimer* twinkleTimer = nullptr;
    QList<QString> twinkleEntityList;

    QList<DetectionRule> detectionRule;
    QList<DetectionRule> newDetectionRules4Local;
    QList<DetectionRule> newDetectionRules4Cloud;
    QHash<QString,bool> manageExistRules;
    QMap<QString,QMap<QString,int>> finalRecognitionResult;//restore recognized data of views
    QMap<QString,QMap<QString,int>> finalCustomizedResult;//restore recognized data of views

    QMap<QString,QMap<QString,QMap<QString,int>>> recognizedDevicesMap;
    QMap<QString,QList<CadDeviceDataDetect*>> recognizedDevices;//for local display
    QMap<QString,QList<FAS_Block*>> blocksOfViews;
    QMap<QString,FAS_Block*> deviceImagesToBeUpdate4AIList;
private slots:
    void slotTwinkleEntityList();

signals:
    void singalConnectGraphicViewSlot();
};

#endif // FASUTILS_H
