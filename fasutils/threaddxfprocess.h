#ifndef THREADDXFPROCESS_H
#define THREADDXFPROCESS_H
#include <QThread>
#include "fasutils.h"
#include "fas_units.h"




class ThreadDxfProcess: public QThread
{
    Q_OBJECT
public:
    ThreadDxfProcess(QObject *parent = nullptr);
    void setParas(QString title,FASUtils* utils);
protected:
    void run();
private:
    QString file;
    FASUtils* utils;
signals:
    void dxfOpenFailed();
    void dxfOpenSuccess();
};

#endif // THREADDXFPROCESS_H
