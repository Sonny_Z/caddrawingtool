#ifndef GRABTHREAD_H
#define GRABTHREAD_H

#include <QThread>
class QNetworkAccessManager;
#include <QNetworkReply>
#include "commondef.h"

#include <QObject>
#include <QTimer>
#include <QNetworkReply>

class QReplyTimeout : public QObject {

    Q_OBJECT

public:
    QReplyTimeout(QNetworkReply *reply, const int timeout) : QObject(reply) {
        Q_ASSERT(reply);
        if (reply && reply->isRunning()) {  // 启动单次定时器
            QTimer::singleShot(timeout, this, SLOT(onTimeout()));
        }
    }

signals:
    void timeout();  // 超时信号 - 供进一步处理

private slots:
    void onTimeout() {  // 处理超时
        QNetworkReply *reply = static_cast<QNetworkReply*>(parent());
        if (reply->isRunning()) {
            reply->abort();
            reply->deleteLater();
            emit timeout();
        }
    }
};

class GrabThread : public QThread
{
        Q_OBJECT
    public:
        explicit GrabThread(QObject *parent = 0);
        ~GrabThread();
        void runRequest();
        void uploadFile(QString filePath);
        void uploadSignalBaseImage(QString& systemCode, QString& code, QString& name, QString& perfectMatchFlag, QString& file);
        void saveToFile(QString& content, QString& fileName, bool isAppend = false, QString fileType = "log", QString dir = "./log");
        void startGetQuery(QString redirect_url);
        void startRecognitionPostQuery(QMap<int,QString> images, QString systemTypeStr = "IES");
        void startClearBaseImageCachePostQuery(QString requestUrl);
        void productKindsGetQuery(QString sysQueryCode = "120204");
        void syncResultPostQuery(QMap<QString,QMap<QString,int>> finalResultMap, QString syncType = "allfloors");
        QNetworkReply* getReply();
        bool runrun;
        bool isHttpFinished();
        QMap<QString,QMap<int,QString>> getRecognitionResponseData();
        void setImageIdFeatureCodeRequest(QMap<int,QString> imageIdFeatureCodeMap);
        QMap<int,QString> getImageIdFeatureCodeMap();
        void setResultSyncResponseCode(QString httpResoponseCode);
        QString getResultSyncResponseCode();
        bool getHttpResponseStatus();
        void setHttpResponseStatus(bool isSuccess);
        void startClearBaseImageCachePostQuery();
        void encodeURI(QString str, QByteArray &outArr);
        QString getHttpErrorMessage();

        QString getLatestOnlineAppVersion();
        void latestOnlineAppVersionGetHttpQuery();
        void getLatestOnlineAppVersionHttpResponse();

        void uploadLatestLocalAppVersion2Server(QString computerName, QString appVersion, QString userName);

        void userNameValidationPutHttpQuery(QString userName);
        bool getInitializationValidationHttpResponse();

        void addimageIdCodeToBeUpdateMap(int imgId,QString code = "");
        void updateRemoteImages(QMap<int,QString> imgIdToRemoteImgId);

    signals:

    public slots:
        void replyFinished(QNetworkReply *);
        void slot_httpError(QNetworkReply::NetworkError);
        void slot_httpFinished();
        void slot_productKindsGetQueryHttpFinished(QString systemCode);
        void slot_uploadFileHttpFinished();

        void slot_httpPostFinished();

private:
    QMap<int,QPair<QString,double>> getRecognitionData(QJsonArray recognitionResult, bool isLocalMethod = true);
    void parseRecognitionFinalDataFromResponse(QJsonArray responseResult);
    void parseErrorMessage(QByteArray responseData, int http_status = 200);
    void initHttpCode2Info();
    QString getInfoByHttpCode(int httpCode);
    void replyProcess(QNetworkReply* reply = nullptr);

    QNetworkAccessManager* qnam;
    QNetworkRequest request;
    QString queryUrl;
    QNetworkReply* reply;
    QString query_word;
    QMap<int,QString> imageIdFeatureCodeMap;
    QMap<int,QString> imageIdCodeResultMap;
    QMap<int,QString> imageIdCodeMatchRatioMap;
    QMap<int,QString> imageIdCodeToBeUpdateMap;
    QMap<int,QString> imgIdToRemoteImgIdMap;
    QString resultSyncResponseCode = "";
    QString uploadFileResponseCode = "";
//    QString serverAddressAI = "http://81.68.151.87:8110";
//    QString serverAddressBusiness = "http://81.68.151.87:8001";
    QString serverAddressAI = "https://agt.sparkletest.gst.com.cn";
    QString serverAddressBusiness = "https://agt.sparkletest.gst.com.cn";
    QString serverAppId = "415327502909408fa9634faec2b187f7";
    QString serverAppSecurity = "2561135a7f9643a8a1f7eb3ea8165631";
//    QString serverAuthorization = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdXBlckFkbWluIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNjAzOTU2MTYxNjA1LCJleHAiOjE2MDQyMTUzNjF9.zJ0kIeCyZ8TOnQ93eLplTcKTlhuSbt0srMYWvKqL2CRmEO7uUPugvZ-pMsIKmMHhlltMIT6GZ-YRQ3UcUYLZhQ";
    bool is_init;
    bool is_HttpFinished = false;
    bool isSuccess = false;
    double recognitionFactor;
    QString popUpErrorMessage = "";
    QHash<int,QString> errorHttpCode2Message;
    QString latestOnlineAppVersion;

    QTimer timer;
};


#endif // GRABTHREAD_H
