#ifndef THREADDRAW_H
#define THREADDRAW_H

#include <QMutex>
#include <QQueue>
#include <QThread>
#include <QWaitCondition>
#include <QPixmap>
#include <QPair>
#include "fas_entitycontainer.h"
#include "fas_graphicview.h"
#define mthreaddraw ThreadDraw::instance()
class ThreadDraw: public QThread
{
public:
    Q_OBJECT
public:
    ThreadDraw();
    ~ThreadDraw();
    void updateElements(FAS_GraphicView* view);
    void stop();
    static ThreadDraw* instance();
public:
    static ThreadDraw* uniqueInstance;
private:
    void draw();
protected:
    void run();
private:
    volatile bool stopped;
private:
    QMutex mutex;
    FAS_GraphicView* view;
signals:
    void signalDrawFinished();
};

#endif // THREADDRAW_H
