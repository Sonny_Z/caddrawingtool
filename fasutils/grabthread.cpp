#include "grabthread.h"
//////////////grabthread.cpp////////////////////
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QSslConfiguration>
#include <QtNetwork/QSslSocket>
#include <QEventLoop>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QVariant>
#include <QFileDialog>
#include <QHttpPart>
#include "JlCompress.h"
#include <QMessageBox>
#include "popupmessagedialog.h"
#include "qdebug.h"
GrabThread::GrabThread(QObject *parent) :
    QThread(parent),
    is_init(true)
{
    initHttpCode2Info();
    QString key = "recognitionFactor";
    recognitionFactor = COMMONDEF->getAppSetting(key).toDouble();

    key = "serverAddressAI";
    auto settingValue = COMMONDEF->getAppSetting(key);
    if(!settingValue.isEmpty()){
        serverAddressAI = settingValue;
    }else{
        COMMONDEF->setAppSetting("serverAddressAI",serverAddressAI);
    }

    key = "serverAddressBusiness";
    settingValue = COMMONDEF->getAppSetting(key);
    if(!settingValue.isEmpty()){
        serverAddressBusiness = settingValue;
    }else{
        COMMONDEF->setAppSetting("serverAddressBusiness",serverAddressBusiness);
    }

    key = "serverAppId";
    settingValue = COMMONDEF->getAppSetting(key);
    if(!settingValue.isEmpty()){
        serverAppId = settingValue;
    }else{
        COMMONDEF->setAppSetting("serverAppId",serverAppId);
    }
    key = "serverAppSecurity";
    settingValue = COMMONDEF->getAppSetting(key);
    if(!settingValue.isEmpty()){
        serverAppSecurity = settingValue;
    }else{
        COMMONDEF->setAppSetting("serverAppSecurity",serverAppSecurity);
    }
//    key = "serverAuthorization";
//    settingValue = COMMONDEF->getAppSetting(key);
//    if(!settingValue.isEmpty()){
//        serverAuthorization = settingValue;
//    }else{
//        COMMONDEF->setAppSetting("serverAuthorization",serverAuthorization);
//    }

    settingValue.clear();
    key.clear();

    this->timer.setInterval(COMMONDEF->timeoutMillisecond());  // 设置超时时间 20 秒
    this->timer.setSingleShot(true);

}

GrabThread::~GrabThread(){
    imageIdFeatureCodeMap.clear();
    imageIdCodeResultMap.clear();
    imageIdCodeToBeUpdateMap.clear();
    imageIdCodeMatchRatioMap.clear();
    imgIdToRemoteImgIdMap.clear();
}

void GrabThread::replyProcess(QNetworkReply* runningReply){
    QNetworkReply* reply = this->reply;
    if(runningReply != nullptr) reply = runningReply;
    /***************如果需要同步********************/
    qRegisterMetaType<QNetworkReply::NetworkError>
                            ("QNetworkReply::NetworkError");
    QEventLoop loop; // 使用事件循环使得网络通讯同步进行
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this,
            SLOT(slot_httpError(QNetworkReply::NetworkError)));
    timer.start();
    loop.exec(); // 进入事件循环， 直到reply的finished()信号发出， 这个语句才能退出
    if (timer.isActive()) {  // 处理响应
        timer.stop();
    }
    disconnect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    reply->abort();
    reply->deleteLater();
    /****************如果需要异步**********************/
    /*
        connect(reply, SIGNAL(finished()), this, SLOT(slot_httpFinished()));
        qRegisterMetaType<QNetworkReply::NetworkError>("QNetworkReply::NetworkError");
        connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this,
                SLOT(slot_httpError(QNetworkReply::NetworkError)));
    */
    /**************************************/
}

void GrabThread::runRequest()
{
    if (is_init)
    {
        qnam = new QNetworkAccessManager();
        is_init = false;
//        startPostQuery();
    }
    while(runrun)
    {
        startGetQuery("");
    }
}

void GrabThread::startGetQuery(QString redirect_url)
{
    QNetworkRequest request;
    QString url;
    // 如果是重定向请求， 则直接指向位置， 否则拼字符串
    if (redirect_url.length() != 0)
    {
       url = redirect_url;
    }else
    {
       url = "http://www.nuihq.com/" + query_word;
    }
    // 设定url
    request.setUrl(QUrl(url));
    // 设定请求头
    request.setRawHeader("Host", "www.baidu.com");
    request.setRawHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:7.0.1) Gecko/20100101 Firefox/7.0.1");
    request.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    request.setRawHeader("Accept-Language", "zh-cn,zh;q=0.5");
    // TODO:使用gzip
    request.setRawHeader("Accept-Encoding", "deflate");
    request.setRawHeader("Accept-Charset", "utf-8;q=0.7,*;q=0.7");
    request.setRawHeader("Connection", "keep-alive");

    // 使用get方式发起请求
    reply = qnam->get(request);
    replyProcess();
    slot_httpFinished();
}

void GrabThread::uploadSignalBaseImage(QString& systemCode, QString& code, QString& name, QString& perfectMatchFlag, QString& fileFullPath)
{
    auto time = QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
    if(perfectMatchFlag.isEmpty()||systemCode.isEmpty()||code.isEmpty()||name.isEmpty()||fileFullPath.isEmpty()){
//        QMessageBox::warning(nullptr, tr("Warning"), tr("Image uploading failed:"+fileFullPath.toLatin1()));
        auto queryLog = QString("imageUploadError");
        auto recordContent = QString::fromUtf8("%1：Image uploading failed:%2\r\n").arg(time).arg(fileFullPath);
        saveToFile(recordContent,queryLog,true);
        return;
    }else if(!code.compare("UnknownDevice",Qt::CaseInsensitive)
            || !name.compare("UnknownDevice",Qt::CaseInsensitive)){
//        QMessageBox::warning(nullptr, tr("Warning"), tr("Image uploading failed:"+fileFullPath.toLatin1()));
        auto queryLog = QString("imageUploadError");
        auto recordCon = QString::fromUtf8("%1：Image uploading failed:%2\r\n").arg(time).arg(fileFullPath);
        auto recordContent = QString::fromUtf8("%1Unkown TYPE CODE:%2\rUnkown NAME:%3\n").arg(recordCon).arg(code).arg(name);
        saveToFile(recordContent,queryLog,true);
        return;
    }
    else
    {
        auto queryLog = QString("imageUploadedRecord");
        auto recordContent = QString::fromUtf8("%1：Image uploaded:%2\r\n").arg(time).arg(fileFullPath);
        saveToFile(recordContent,queryLog,true);
    }
    //http://140.143.15.235:8110/imagematch/v1/busiimage/file
    //https://vikings.bdease.com:9443/imagematch/v1/busiimage/file
    QString baseUrl = QString::fromUtf8("%1/imagematch/v1/busiimage/file").arg(serverAddressAI);
    //中文请求内容处理
//    QTextCodec * codecGB2312 = QTextCodec::codecForName("GB2312");
//    QByteArray byteArrayGB2312 = codecGB2312->fromUnicode(name);
//    QByteArray byteArrayPercentEncodedName = byteArrayGB2312.toPercentEncoding();
    auto nameCov = QUrl::toPercentEncoding(name);


    // 设定请求头
    QNetworkRequest request;
    //配置SSL
    QSslConfiguration config;
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    conf.setProtocol(QSsl::TlsV1SslV3);
    request.setSslConfiguration(conf);
    QString keyStr = "userName";
    auto localUserName = COMMONDEF->getAppSetting(keyStr);

    QString requestUrl = QString::fromUtf8("%1?code=%2&name=%3&perfectMatchFlag=%4&systemId=%5&deleteFlag=%6&imageStatus=%7&userName=%8")
            .arg(baseUrl)
            .arg(code)
            .arg(name)
            .arg(perfectMatchFlag)
            .arg(systemCode)
            .arg("false")
            .arg("DISABLED")
            .arg(localUserName);
    QByteArray outArr;
    encodeURI(requestUrl, outArr);
    request.setUrl(QUrl(outArr));
    auto queryLog = QString("imageUploadRequest");
    saveToFile(requestUrl,queryLog);

    // TODO:上传文件
    QFileInfo fileInfo(fileFullPath);
    QString absoluteFilePath = fileInfo.absoluteFilePath();
    QString file = QString::fromUtf8("%1.jpg").arg(fileInfo.baseName());

    //TODO:multi文件容器
    QHttpMultiPart* multi_part = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    QHttpPart image_part;
    QString requestFormat = QString::fromUtf8("form-data;name=%1;filename=%2;type=multipart/form-data").arg("file").arg(file);
    image_part.setHeader(QNetworkRequest::ContentDispositionHeader,QVariant(requestFormat));
    QFile *imageFile = new QFile(absoluteFilePath);
    if (!imageFile->open(QIODevice::ReadOnly)||imageFile->size()==0)
    {
        imageFile->close();
        auto queryLog = QString("InvalidImageRecord");
        auto recordContent = QString::fromUtf8("%1：Invalid image:%2\r\n").arg(time).arg(fileFullPath);
        saveToFile(recordContent,queryLog,true);
        QMessageBox::warning(nullptr, tr("Warning"), tr("Invalid image."));
        return ;
    }
    image_part.setBodyDevice(imageFile);
    imageFile->setParent(multi_part);
    multi_part->append(image_part);

    // 使用post方式发起请求
    QNetworkAccessManager *restclient; //in class
    restclient = new QNetworkAccessManager(this); //constructor
    this->reply = restclient->post(request,multi_part);
    multi_part->setParent(reply);
    replyProcess();
    imageFile->close();
    slot_httpPostFinished();
}
void GrabThread::uploadFile(QString filePath){
    is_HttpFinished = false;
    // 设定urlQString&
/*    QUrl myurl;
    myurl.setScheme("http"); //https also applicable
    myurl.setHost("3p325241r1.wicp.vip");
    myurl.setPath("/imagematch/v1/image/compare/upload/matchfiles");
    // qDebug() << myurl.toString();
    request.setUrl(myurl);
*/
//    QString filter = "Compressed File(*.zip)";
//    QString file = QFileDialog::getOpenFileName(nullptr, "*", ".", filter);
    //http://140.143.15.235:8110/imagematch/v1/image/compare/upload/matchfiles
    //https://vikings.bdease.com:9443/imagematch/v1/image/compare/upload/matchfiles
    QString requestUrl = QString::fromUtf8("%1/imagematch/v1/image/compare/upload/matchfiles").arg(serverAddressAI);

    QString file = QString::fromUtf8("%1.zip").arg(filePath);
    QFile fileTemp(file);
    if(fileTemp.exists()){
        fileTemp.remove();
    }
    if(false==JlCompress::compressDir(QString::fromUtf8("%1.zip").arg(filePath),filePath))
             qDebug() <<"compress failed";
        else
             qDebug() <<"compress successed";

//    QNetworkRequest request;
    //配置SSL
    QSslConfiguration config;
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    conf.setProtocol(QSsl::AnyProtocol);
    request.setSslConfiguration(conf);

    // 设定请求头

    request.setUrl(QUrl(requestUrl));
    QString requestFormat = QString::fromUtf8("form-data;name=%1;filename=%2;type=application/x-zip-compressed").arg("file").arg(file);


    //TODO:multi文件容器
    QHttpPart file_part;
    QHttpMultiPart* multi_part = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    file_part.setHeader(QNetworkRequest::ContentDispositionHeader,QVariant(requestFormat));
    // TODO:上传文件
    QFile *compressedFile = new QFile(file);
    compressedFile->open(QIODevice::ReadOnly);
    compressedFile->setParent(multi_part);
    file_part.setBodyDevice(compressedFile);
    multi_part->append(file_part);

    // 使用post方式发起请求
    QNetworkAccessManager *restclient; //in class
    restclient = new QNetworkAccessManager(this); //constructor

    this->reply = restclient->post(request,multi_part);
    multi_part->setParent(reply);
    replyProcess();
    slot_uploadFileHttpFinished();
    requestUrl.clear();
    delete compressedFile;
}

void GrabThread::startClearBaseImageCachePostQuery()
{
    QNetworkRequest request;
    //配置SSL
//    QSslConfiguration config;
//    QSslConfiguration conf = request.sslConfiguration();
//    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
//    conf.setProtocol(QSsl::TlsV1SslV3);
//    request.setSslConfiguration(conf);
    // 设定url
    //http://140.143.15.235:8110/imagematch/v1/image/compare/clear
    //"https://vikings.bdease.com:9443/imagematch/v1/image/compare/clear"
    QString requestUrl = QString::fromUtf8("%1/imagematch/v1/image/compare/clear").arg(serverAddressAI);
    request.setUrl(QUrl(requestUrl));
    // 设定请求头
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");

    // 使用post方式发起请求
    QNetworkAccessManager *restclient; //in class
    restclient = new QNetworkAccessManager(this); //constructor
    this->reply = restclient->post(request,"");
    replyProcess();
    slot_httpPostFinished();
}

void GrabThread::startRecognitionPostQuery(QMap<int,QString> images, QString systemTypeStr)
{
    is_HttpFinished = false;
    QNetworkRequest request;
    //配置SSL
    QSslConfiguration config;
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    conf.setProtocol(QSsl::TlsV1SslV3);
    request.setSslConfiguration(conf);
    // 设定url http://140.143.15.235:8110/imagematch/v1/image/compare/like
    // "https://vikings.bdease.com:9443/imagematch/v1/image/compare/like"

    QString requestUrl = QString::fromUtf8("%1/imagematch/v1/image/compare/like").arg(serverAddressAI);
    request.setUrl(QUrl(requestUrl));
    // 设定请求头
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");

    QString keyStr = "userName";
    auto localUserName = COMMONDEF->getAppSetting(keyStr);

    //set request json data
    // TODO:使用Json
        QVariantMap requestData;
        requestData.insert("requestId",uploadFileResponseCode);
        requestData.insert("version",1);
        requestData.insert("subSytem","IES");
        requestData.insert("requestUser",localUserName);
        requestData.insert("minMatchRate",COMMONDEF->getMinMatchRate());
        requestData.insert("combinedWithLocal",COMMONDEF->isConbinedWithLocalData());
        QVariantList QVarList;
        foreach(auto imgkey, images.keys()){
            QVariantMap tmpvm;
            tmpvm.clear();
            auto imageName = images.value(imgkey);
            tmpvm["imageSrc"] = imageName;
            tmpvm["imageId"] = imgkey;
            QVarList<<tmpvm;
         }
    requestData.insert("imgURLs",QVarList);

    //log request json data
    QByteArray payload=QJsonDocument::fromVariant(requestData).toJson();
    auto displayContent = QString::fromUtf8("requestUrl:\r\n%1\r\ndata:\r\n%2").arg(requestUrl).arg(QVariant(payload).toString());
    //qDebug() << displayContent;
    auto queryLog = QString("requestContent");
    saveToFile(displayContent,queryLog);

     // 使用post方式发起请求
    QNetworkAccessManager *restclient; //in class
    restclient = new QNetworkAccessManager(this); //constructor
    auto supportedSchemesContent = restclient->supportedSchemes().join(",");
//    qDebug() << supportedSchemesContent;
//    qDebug() << QSslSocket::sslLibraryBuildVersionString();
//    qDebug() << QSslSocket::supportsSsl();
    auto supportedSchemes = QString("supportedSchemes");
    this->reply->ignoreSslErrors();
    supportedSchemesContent = QString::fromUtf8("%1\r\n%2\r\n%3")
            .arg(supportedSchemesContent)
            .arg(QSslSocket::sslLibraryBuildVersionString())
            .arg(QSslSocket::supportsSsl());
    saveToFile(supportedSchemesContent,supportedSchemes);
    this->reply = restclient->post(request,payload);
    replyProcess();
    requestUrl.clear();
    requestData.clear();
    slot_httpFinished();
}

void GrabThread::syncResultPostQuery(QMap<QString,QMap<QString,int>> finalResultMap, QString syncType)
{
    is_HttpFinished = false;
    bool isAllFloors = syncType.contains("allfloors");
    QString requestUrl = "app/v1/sync";//allfloors or onefloor
    requestUrl = QString::fromUtf8("%1/%2/%3").arg(serverAddressBusiness).arg(requestUrl).arg(syncType);

    QNetworkRequest request;
    //配置SSL
    QSslConfiguration config;
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    conf.setProtocol(QSsl::TlsV1SslV3);
    request.setSslConfiguration(conf);
    // 设定url
    request.setUrl(QUrl(requestUrl));
    // 设定请求头
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setRawHeader("AppId",serverAppId.toLatin1());
    request.setRawHeader("AppSecurity",serverAppSecurity.toLatin1());

    //set request json data
    // TODO:使用Json

    QVariantList requestJsonData;
    QVariantMap floor;
    foreach(auto floorKey, finalResultMap.keys()){
        floor.clear();
        floor.insert("floor", floorKey);
        QVariantList productList;
        productList.clear();
        auto products = finalResultMap.value(floorKey);
        foreach(auto productKey, products.keys()){
            QVariantMap product;
            product.clear();
            auto productCount = products.value(productKey);
            product.insert("productKindCode",productKey);
            product.insert("count",productCount);
            productList<<product;
        }
        floor.insert("products",productList);
        if(isAllFloors){
            requestJsonData<<floor;
        }
    }
//    qDebug() << "request url:" << request.url();
//    qDebug() << "request AppId:" << request.rawHeader("AppId");
//    qDebug() << "request AppSecurity:" << request.rawHeader("AppSecurity");
//    qDebug() << "request ETagHeader：" << request.header(QNetworkRequest::KnownHeaders::ETagHeader);
//    qDebug() << "request CookieHeader：" << request.header(QNetworkRequest::KnownHeaders::CookieHeader);
//    qDebug() << "request ServerHeader：" << request.header(QNetworkRequest::KnownHeaders::ServerHeader);
//    qDebug() << "request IfMatchHeader：" << request.header(QNetworkRequest::KnownHeaders::IfMatchHeader);
//    qDebug() << "request LocationHeader：" << request.header(QNetworkRequest::KnownHeaders::LocationHeader);
//    qDebug() << "request SetCookieHeader：" << request.header(QNetworkRequest::KnownHeaders::SetCookieHeader);
//    qDebug() << "request UserAgentHeader：" << request.header(QNetworkRequest::KnownHeaders::UserAgentHeader);
//    qDebug() << "request ContentTypeHeader：" << request.header(QNetworkRequest::KnownHeaders::ContentTypeHeader);
//    qDebug() << "request IfNoneMatchHeader：" << request.header(QNetworkRequest::KnownHeaders::IfNoneMatchHeader);
//    qDebug() << "request LastModifiedHeader：" << request.header(QNetworkRequest::KnownHeaders::LastModifiedHeader);
//    qDebug() << "request ContentLengthHeader：" << request.header(QNetworkRequest::KnownHeaders::ContentLengthHeader);
//    qDebug() << "request IfModifiedSinceHeader：" << request.header(QNetworkRequest::KnownHeaders::IfModifiedSinceHeader);
//    qDebug() << "request ContentDispositionHeader：" << request.header(QNetworkRequest::KnownHeaders::ContentDispositionHeader);

    //input request json data
    QByteArray payload;
    if(isAllFloors){
        payload =QJsonDocument::fromVariant(requestJsonData).toJson();
    }
    else{
        payload =QJsonDocument::fromVariant(floor).toJson();
    }
    auto displayContent = QString::fromUtf8("requestHeader:url=%1\rAppId=%2\rAppSecurity=%3\rContentTypeHeader=%4\r\n%5")
            .arg(request.url().toString())
            .arg(QString::fromStdString(request.rawHeader("AppId").toStdString()))
            .arg(QString::fromStdString(request.rawHeader("AppSecurity").toStdString()))
            .arg(request.header(QNetworkRequest::KnownHeaders::ContentTypeHeader).toString())
            .arg(QVariant(payload).toString());
    //qDebug() << displayContent;
    auto queryLog = QString("syncResutlPostQueryContent");
    saveToFile(displayContent,queryLog);

    // 使用post方式发起请求
    QNetworkAccessManager *restclient; //in class
    restclient = new QNetworkAccessManager(this); //constructor
    this->reply = restclient->post(request,payload);
    replyProcess();
    slot_httpFinished();
}


void GrabThread::productKindsGetQuery(QString sysQueryCode)
{
    is_HttpFinished = false;
    QString requestUrl = "app/v1/prodKinds/all";
    requestUrl = QString::fromUtf8("%1/%2/%3").arg(serverAddressBusiness).arg(requestUrl).arg(sysQueryCode);

    QNetworkRequest request;
    //配置SSL
    QSslConfiguration config;
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    conf.setProtocol(QSsl::TlsV1SslV3);
    request.setSslConfiguration(conf);
    // 设定url
    request.setUrl(QUrl(requestUrl));
    // 设定请求头
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
//    request.setRawHeader("Authorization",serverAuthorization.toLatin1());
    request.setRawHeader("AppId",serverAppId.toLatin1());
    request.setRawHeader("AppSecurity",serverAppSecurity.toLatin1());


    //set request json data

    //input request json data
    auto displayContent = QString::fromUtf8("curl -X GET --header 'Accept: %1'\t--header 'AppId: %2'\t--header 'AppSecurity: %3'\r\n'%4'")
            .arg(request.header(QNetworkRequest::KnownHeaders::ContentTypeHeader).toString())
//            .arg(QString::fromStdString(request.rawHeader("Authorization").toStdString()))
            .arg(QString::fromStdString(request.rawHeader("AppId").toStdString()))
            .arg(QString::fromStdString(request.rawHeader("AppSecurity").toStdString()))
            .arg(request.url().toString());
    //qDebug() << displayContent;
    auto queryLog = QString("productKindsGetQuery");
    saveToFile(displayContent,queryLog);

    // 使用get方式发起请求
    QNetworkAccessManager *restclient; //in class
    restclient = new QNetworkAccessManager(this); //constructor
    this->reply = restclient->get(request);
    replyProcess();
    slot_productKindsGetQueryHttpFinished(sysQueryCode);
}

bool GrabThread::isHttpFinished(){
    return is_HttpFinished;
}
QNetworkReply* GrabThread::getReply(){
    return reply;
}

void GrabThread::slot_httpPostFinished(){
    int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if(http_status == 200){
        auto dataIn = reply->readAll();
        parseErrorMessage(dataIn);

        QTextCodec *gbk=QTextCodec::codecForName("UTF-8");
        QString g2u=gbk->toUnicode(dataIn);
        QString dataReply(dataIn);
        dataReply=dataReply.toUtf8();
        //qDebug() << dataReply;
        auto responseLog = QString("responseContent4ImageUpload");
        saveToFile(dataReply,responseLog);

        QJsonDocument jsdoc;
        jsdoc = QJsonDocument::fromJson(dataIn);
        auto jsobj = jsdoc.object();
        auto syncResponseCode = jsobj.value("status").toString();
        if(!syncResponseCode.compare("success",Qt::CaseInsensitive)){
            setHttpResponseStatus(true);
        }

    }else{
        auto dataIn = reply->readAll();
        parseErrorMessage(dataIn, http_status);
    }

}

void GrabThread::slot_uploadFileHttpFinished(){
    int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if(http_status == 200){
        auto dataIn = reply->readAll();
        parseErrorMessage(dataIn);

        QTextCodec *gbk=QTextCodec::codecForName("UTF-8");
        QString g2u=gbk->toUnicode(dataIn);
        QString dataReply(dataIn);
        dataReply=dataReply.toUtf8();
        //qDebug() << dataReply;
        auto responseLog = QString("uploadFileResponseContent");
        saveToFile(dataReply,responseLog);

        QJsonDocument jsdoc;
        jsdoc = QJsonDocument::fromJson(dataIn);
        auto jsobj = jsdoc.object();
        if(jsobj.isEmpty())
            return;
        uploadFileResponseCode = jsobj.value("data").toString();

    }else{
        auto dataIn = reply->readAll();
        parseErrorMessage(dataIn, http_status);
    }
}
QMap<int,QPair<QString,double>> GrabThread::getRecognitionData(QJsonArray recognitionResult, bool isLocalMethod){
    QMap<int,QPair<QString,double>> resultMap;
    QString recognitionLogContent;
    foreach (const QJsonValue &value, recognitionResult){
        QJsonObject jsob = value.toObject();
        auto imageIDStr  = jsob.value("imageId").toString();
        auto imageID = imageIDStr.toInt();
        auto sublist = jsob.value("subList").toArray();
        if(sublist.size()==0){
            QPair<QString, double> qPair("0",0);
            resultMap.insert(imageID, qPair);
            continue;
        }
        double likenessResult=0.0;
        auto imageFeatureCodeSrc = imageIdFeatureCodeMap.value(imageID);
        int recognizedMatchingSubImageId = 0;
        QString recognizedMatchingSubImageName;
        foreach(const QJsonValue &v, sublist){
            QJsonObject js = v.toObject();
            auto likeness = js.value("likeness").toDouble();
            auto subImage = js.value("subImage").toObject();
            auto code = subImage.value("code").toString();
            auto deviceFeatureCode = subImage.value("perfectMatchFlag").toString();
            recognizedMatchingSubImageId = subImage.value("id").toInt();
            recognizedMatchingSubImageName = subImage.value("name").toString();
            //filter the different devices has same feature code.
            if(likeness < recognitionFactor){
                if(likeness >= 0.85){
                    if(imageIdCodeMatchRatioMap.find(imageID) != imageIdCodeMatchRatioMap.end()){
                        if(imageIdCodeMatchRatioMap.value(imageID).toDouble() < likeness){
                            imageIdCodeMatchRatioMap.insert(imageID, QString::fromUtf8("%1").arg(likeness));
                        }
                    }else{
                        imageIdCodeMatchRatioMap.insert(imageID, QString::fromUtf8("%1").arg(likeness));
                    }
                }
                continue;
            }

            if(!deviceFeatureCode.compare(imageFeatureCodeSrc,Qt::CaseInsensitive)){
                if(likeness>likenessResult){
                    likenessResult=likeness;
                    QPair<QString, double> qPair(code,likenessResult);
                    resultMap.insert(imageID, qPair);
                    imgIdToRemoteImgIdMap.insert(imageID,QString::fromUtf8("%1").arg(recognizedMatchingSubImageId));
                    if(likeness==1){
                        break;
                    }
                }
            }
        }

        //Recognition record.
        if(recognizedMatchingSubImageId>0){
            // qDebug() << imageIDStr <<"->"<<recognizedMatchingSubImageName<<"["<< recognizedMatchingSubImageId <<":Matching ratio:"<<likenessResult<<"]";
            recognitionLogContent = QString::fromUtf8("%1->%2[%3:Matching Ratio->%4]\r\n%5")
                    .arg(imageIDStr)
                    .arg(recognizedMatchingSubImageName)
                    .arg(recognizedMatchingSubImageId)
                    .arg(likenessResult)
                    .arg(recognitionLogContent);
        }
    }

    auto recognitionLog = QString(isLocalMethod?"RecognitionRecordLocal":"RecognitionRecordRemote");
    saveToFile(recognitionLogContent,recognitionLog);
    return resultMap;
}

void GrabThread::parseRecognitionFinalDataFromResponse(QJsonArray responseResult){
    QList<int> requestImageIdList;
    QJsonArray localRecognitionPart;
    QJsonArray remoteRecognitionPart;
    foreach (const QJsonValue &value, responseResult){
        QJsonObject jsobTempL;
        QJsonObject jsobTempR;
        QJsonObject jsob = value.toObject();
        auto imageIDStr  = jsob.value("imageId").toString();
        auto imageID = imageIDStr.toInt();
        requestImageIdList.append(imageID);
        jsobTempL.insert("imageId",imageIDStr);
        jsobTempR.insert("imageId",imageIDStr);

        QJsonArray subListTempL;
        QJsonArray subListTempR;
        auto subList = jsob.value("subList").toArray();
        if(subList.size()==0){
            continue;
        }
        foreach(const QJsonValue &v, subList){
            QJsonObject jsTemp;
            QJsonObject js = v.toObject();
            auto likeness = js.value("likeness").toDouble();
            auto subImage = js.value("subImage").toObject();
            auto code = subImage.value("imageStatus").toString();

            jsTemp.insert("likeness",likeness);
            jsTemp.insert("subImage",subImage);
            if(code.contains("ENABLED",Qt::CaseInsensitive)){
                subListTempR.append(jsTemp);
            }else{
                subListTempL.append(jsTemp);
            }
        }
        jsobTempL.insert("subList",subListTempL);
        jsobTempR.insert("subList",subListTempR);
        localRecognitionPart.append(jsobTempL);
        remoteRecognitionPart.append(jsobTempR);
    }
    QTime startTime4 = QTime::currentTime();
    qDebug() << "getRecognitionDataStart:"<<startTime4;;
    auto localRecognitionResult = getRecognitionData(localRecognitionPart);
    auto remoteRecognitionResult = getRecognitionData(remoteRecognitionPart,false);
    QTime startTime5 = QTime::currentTime();
    qDebug() << "getRecognitionDataEnd:"<<startTime5;;
    foreach(auto imageId, requestImageIdList){
        auto localResult = localRecognitionResult.value(imageId);
        auto remoteResult = remoteRecognitionResult.value(imageId);
        if(localResult.second >= remoteResult.second){
            if(!localResult.first.isEmpty() && !localResult.first.isNull())
                imageIdCodeResultMap.insert(imageId, localResult.first);
        }else{
            if(!remoteResult.first.isEmpty() && !remoteResult.first.isNull())
                imageIdCodeResultMap.insert(imageId, remoteResult.first);
        }
    }
}

void GrabThread::slot_httpFinished()
{
    int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if(http_status == 200){
        auto dataIn = reply->readAll();
        QTextCodec *gbk=QTextCodec::codecForName("UTF-8");
        QString g2u=gbk->toUnicode(dataIn);
        QString dataReply(dataIn);
        dataReply=dataReply.toUtf8();
        //qDebug() << dataReply;
        auto responseLog = QString("responseContent");
        saveToFile(dataReply,responseLog);

        QJsonDocument jsdoc;
        jsdoc = QJsonDocument::fromJson(dataIn);
        auto jsobj = jsdoc.object();
        auto syncResponseStatus = jsobj.value("status").toString();
        if(!syncResponseStatus.compare("success",Qt::CaseInsensitive)){
            setHttpResponseStatus(true);
        }
        if(jsobj.isEmpty())
            return;
        auto syncResponseCode = jsobj.value("key").toString();
        if(!syncResponseCode.isNull() && !syncResponseCode.isEmpty()){
            setResultSyncResponseCode(syncResponseCode);
        }else{
            setResultSyncResponseCode("Error!Try it again.");
        }
        auto jsdataobj = jsobj.value("data").toObject();
        auto jsarr = jsdataobj["result"].toArray();
        if(!jsarr.isEmpty())
            parseRecognitionFinalDataFromResponse(jsarr);
        parseErrorMessage(dataIn);
    }else{
        auto dataIn = reply->readAll();
        parseErrorMessage(dataIn, http_status);
//        QTextCodec *gbk=QTextCodec::codecForName("GBK");
//        QString g2u=gbk->toUnicode(dataIn);
//        QString dataReply(dataIn);
//        dataReply=dataReply.toUtf8();
//        QJsonDocument jsdoc;
//        jsdoc = QJsonDocument::fromJson(dataIn);
//        auto jsobj = jsdoc.object();
//        if(jsobj.isEmpty())
//            return;
//        auto syncResponseCode = jsobj.value("error").toString();
//        syncResponseCode = QString::fromUtf8("Failed->[%1]\r\n").arg(syncResponseCode);
//        if(!syncResponseCode.isNull() && !syncResponseCode.isEmpty()){
//            setResultSyncResponseCode(syncResponseCode);
//        }else{
//            setResultSyncResponseCode("Error!Try it again.");
//        }
//        auto responseLog = QString("responseErrorContent");
//        saveToFile(dataReply,responseLog);
    }


    reply->deleteLater();

    reply->close();
    reply->deleteLater();
    is_HttpFinished = true;
/*
    // 判断是否是重定向
    if (http_status == 302)
    {
        startGetQuery(reply->rawHeader("Location"));
    }else
    {
        QString reply_content = QString::fromUtf8(reply->readAll());
        if (reply->error() == QNetworkReply::NoError)
        {
            saveToFile(reply_content);
        }else
        {
            qDebug() << "ERROR:" << query_word << " CODE:" << reply->error();
        }
    }
    */
}

void GrabThread::slot_productKindsGetQueryHttpFinished(QString systemCode)
{
    int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if(http_status == 200){
        auto dataIn = reply->readAll();
        parseErrorMessage(dataIn);

        QTextCodec *gbk=QTextCodec::codecForName("UTF-8");
        QString g2u=gbk->toUnicode(dataIn);
        QString dataReply(dataIn);
        dataReply = dataReply.toUtf8();
        //qDebug() << dataReply;
        auto responseLog = QString("productKindsResponseContent");
        saveToFile(dataReply,responseLog);
        auto systemType = COMMONDEF->getSystemByCode(systemCode);

        QJsonDocument jsdoc;
        jsdoc = QJsonDocument::fromJson(g2u.toUtf8());
        if(jsdoc.isEmpty())
            return;
        auto jsarr = jsdoc.array();
        auto productCount = 0;
        QString logContent;
        foreach (const QJsonValue &value, jsarr){
            QJsonObject jsob = value.toObject();
            auto productCode = jsob.value("productKindCode").toString();
            auto productName = jsob.value("productKindName").toString();
            QString productNum = "";
            if(productCount>9){
                productNum = QString::fromUtf8("%1").arg(productCount);
            }else{
                productNum = QString::fromUtf8("0%1").arg(productCount);
            }
            productCount++;
            QString productName2TypeCode = QString::fromUtf8("%1%%2%%3")
                    .arg(productNum).arg(productName).arg(productCode);
            COMMONDEF->addProductName2TypeCode(productName2TypeCode);
            logContent = QString::fromUtf8("%1%2\r\n").arg(logContent).arg(productName2TypeCode);
        }
        QString dirStr = QString::fromUtf8("./svgfiles/%1").arg(systemType);
        saveToFile(logContent,systemType,false,"ini",dirStr);

    }else if(http_status != 0){
        auto dataIn = reply->readAll();
        parseErrorMessage(dataIn, http_status);

//        QTextCodec *gbk=QTextCodec::codecForName("GBK");
//        QString g2u=gbk->toUnicode(dataIn);
//        QString dataReply(dataIn);
//        dataReply=dataReply.toUtf8();
//        //qDebug() << dataReply;

//        QJsonDocument jsdoc;
//        jsdoc = QJsonDocument::fromJson(dataIn);
//        auto jsobj = jsdoc.object();
//        if(jsobj.isEmpty())
//            return;
//        auto responseLog = QString("responseErrorContent");
//        saveToFile(dataReply,responseLog);
    }
//    else{
//        QString message = QString::fromUtf8("Network connection error code: %1").arg(http_status);
//        QMessageBox::warning(nullptr, tr("Warning"), tr(message.toLatin1()));
//    }



    reply->deleteLater();

    reply->close();
    reply->deleteLater();
    is_HttpFinished = true;
/*
    // 判断是否是重定向
    if (http_status == 302)
    {
        startGetQuery(reply->rawHeader("Location"));
    }else
    {
        QString reply_content = QString::fromUtf8(reply->readAll());
        if (reply->error() == QNetworkReply::NoError)
        {
            saveToFile(reply_content);
        }else
        {
            qDebug() << "ERROR:" << query_word << " CODE:" << reply->error();
        }
    }
    */
}

QMap<QString,QMap<int,QString>> GrabThread::getRecognitionResponseData(){
    QMap<QString,QMap<int,QString>> result;
    result.insert("types",imageIdCodeResultMap);
    result.insert("matchRatio",imageIdCodeMatchRatioMap);
    result.insert("itemsToBeUpdate",imageIdCodeToBeUpdateMap);
    result.insert("curImgIdToRemoteImgId",imgIdToRemoteImgIdMap);
    return result;
}

void GrabThread::addimageIdCodeToBeUpdateMap(int imgId,QString code){
    imageIdCodeToBeUpdateMap.insert(imgId,code);
}


void GrabThread::updateRemoteImages(QMap<int,QString> imgIdToRemoteImgId){
    QString requestUrl = QString::fromUtf8("%1/imagematch/v1/busiimage/modifyImageInfo").arg(serverAddressAI);
    //配置SSL
    QSslConfiguration config;
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    conf.setProtocol(QSsl::TlsV1SslV3);
    request.setSslConfiguration(conf);

    // 设定请求头
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    request.setUrl(QUrl(requestUrl));

    auto images = this->imageIdCodeToBeUpdateMap.keys();
    QString code;
    QString keyStr = "userName";
    auto userName = COMMONDEF->getAppSetting(keyStr);
    QVariantList requestJsonData;
    foreach(auto imgId, images){
        QVariantMap img;
        auto id = imgIdToRemoteImgId.value(imgId).toInt();
        auto code = imageIdCodeToBeUpdateMap.value(imgId);
        if(id){
            img.insert("id", id);
            if(!code.isEmpty()) img.insert("code", code);
            img.insert("userName",userName);
            requestJsonData << img;
        }
    }
    imageIdCodeToBeUpdateMap.clear();
    if(requestJsonData.size() == 0) return;
    //input request json data
    QByteArray payload;
    payload =QJsonDocument::fromVariant(requestJsonData).toJson();
    auto displayContent = QString::fromUtf8("requestHeader:url=%1\rContentTypeHeader=%2\r\n%3")
            .arg(request.url().toString())
            .arg(request.header(QNetworkRequest::KnownHeaders::ContentTypeHeader).toString())
            .arg(QVariant(payload).toString());
    //qDebug() << displayContent;
    auto queryLog = QString("updateRemoteImagesPutQueryContent");
    saveToFile(displayContent,queryLog);

    // 使用put方式发起请求
    QNetworkAccessManager *restclient; //in class
    restclient = new QNetworkAccessManager(this); //constructor
    this->reply = restclient->put(request,payload);
    replyProcess();
    slot_httpFinished();
}

void GrabThread::replyFinished(QNetworkReply *)
{

}

void GrabThread::slot_httpError(QNetworkReply::NetworkError)
{
    if (timer.isActive()) {  // 处理响应
        timer.stop();
    }
}

void GrabThread::saveToFile(QString & content, QString & fileName, bool isAppend, QString fileType, QString dir)
{
    if(!COMMONDEF->isLogEnabled()){
        return;
    }
    QFileInfo fileInfo(dir);
    QString absoluteFilePath = fileInfo.absoluteFilePath();
    if(COMMONDEF->isDirExist(absoluteFilePath)){
        auto filePath = QString::fromUtf8("%1/%2.%3").arg(absoluteFilePath).arg(fileName).arg(fileType);
        QFile file(filePath);
        if(isAppend){
            file.open(QIODevice::ReadWrite | QIODevice::Append);
        }else{
            file.open(QIODevice::WriteOnly);
        }
        file.write(content.toUtf8());
        file.close();
    }
}

void GrabThread::setImageIdFeatureCodeRequest(QMap<int,QString> imageIdFeatureCodeMap){
    this->imageIdFeatureCodeMap = imageIdFeatureCodeMap;
}

QMap<int,QString> GrabThread::getImageIdFeatureCodeMap(){
    return this->imageIdFeatureCodeMap;
}

void GrabThread::setResultSyncResponseCode(QString httpResoponseCode){
    this->resultSyncResponseCode = httpResoponseCode;
}
QString GrabThread::getResultSyncResponseCode(){
    return this->resultSyncResponseCode;
}

bool GrabThread::getHttpResponseStatus(){
    return this->isSuccess;
}

void GrabThread::setHttpResponseStatus(bool isSuccess){
    this->isSuccess = isSuccess;
}

void GrabThread::encodeURI(QString str, QByteArray &outArr){
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    if(codec->canEncode(str)) {
        QByteArray tmpArr;
        tmpArr = codec->fromUnicode(str);
        for(int i=0,size = tmpArr.length();i<size;i++){
            char ch = tmpArr.at(i);
            if(ch < 128 && ch > 0){
                outArr.append(ch);
            }else{
            uchar low = ch & 0xff;
                char c[3];
                sprintf(c,"%02X",low);
                outArr.append("%").append(c);
            }
        }
    }
}

void GrabThread::parseErrorMessage(QByteArray responseData, int http_status){
    QString errorMessage;
    QTextCodec *gbk = QTextCodec::codecForName("UTF-8");
    if(http_status == 200){
        QString g2u = gbk->toUnicode(responseData);
        QJsonDocument jsdoc;
        jsdoc = QJsonDocument::fromJson(g2u.toUtf8());
        auto jsobj = jsdoc.object();
        if(jsobj.isEmpty())
            return;
        auto status = jsobj.value("status").toString().contains("FAIL",Qt::CaseInsensitive);
        if(status){
            errorMessage = jsobj.value("message").toString();
        }else{
            auto syncResponseCode = jsobj.value("error").toString();
            if(!syncResponseCode.isNull() && !syncResponseCode.isEmpty()){
                syncResponseCode = QString::fromUtf8("%1->[%2]\r\n")
                        .arg(tr("Failed"))
                        .arg(syncResponseCode);
                setResultSyncResponseCode(syncResponseCode);
            }
        }
    }else{
        errorMessage = QString::fromUtf8("%1： %2")
                .arg(tr("Network Exception"))
                .arg(getInfoByHttpCode(http_status));
    }

    if(!errorMessage.isNull() && !errorMessage.isEmpty()){
        if(!this->popUpErrorMessage.contains(errorMessage)){
            this->popUpErrorMessage = QString::fromUtf8("%1\r\n%2").
                    arg(this->popUpErrorMessage).
                    arg(errorMessage);
            auto responseLog = QString("responseErrorContent");
            saveToFile(this->popUpErrorMessage, responseLog);
        }
    }
}

QString GrabThread::getHttpErrorMessage(){
    return this->popUpErrorMessage;
}

void GrabThread::initHttpCode2Info(){
    this->errorHttpCode2Message.clear();
    this->errorHttpCode2Message.insert(0,tr("No Network."));
    this->errorHttpCode2Message.insert(200,tr("Success."));
    this->errorHttpCode2Message.insert(201,tr("Created."));
    this->errorHttpCode2Message.insert(400,tr("Bad Request."));
    this->errorHttpCode2Message.insert(401,tr("Unauthorized."));
    this->errorHttpCode2Message.insert(402,tr("Payment Required."));
    this->errorHttpCode2Message.insert(403,tr("Forbidden."));
    this->errorHttpCode2Message.insert(404,tr("Not Found."));
    this->errorHttpCode2Message.insert(405,tr("Method Not Allowed."));
    this->errorHttpCode2Message.insert(406,tr("Not Acceptable."));
    this->errorHttpCode2Message.insert(407,tr("Proxy Authentication Required."));
    this->errorHttpCode2Message.insert(408,tr("Request Timeout."));
    this->errorHttpCode2Message.insert(500,tr("Internal Server Error."));
    this->errorHttpCode2Message.insert(501,tr("Not Implemented."));
    this->errorHttpCode2Message.insert(502,tr("Bad Gateway."));
    this->errorHttpCode2Message.insert(503,tr("Service Unavailable."));
    this->errorHttpCode2Message.insert(504,tr("Gateway Timeout."));
    this->errorHttpCode2Message.insert(505,tr("HTTP Version Not Supported."));
}

QString GrabThread::getInfoByHttpCode(int httpCode){
    QString result;
    QTextCodec *gbk = QTextCodec::codecForName("UTF-8");
    if(this->errorHttpCode2Message.contains(httpCode)){
        auto errorMessage = this->errorHttpCode2Message.find(httpCode).value();
        result = gbk->toUnicode(errorMessage.toUtf8());
    }else{
        auto unexpectedError = QString(tr("Unexpected Network Error."));
        result = gbk->toUnicode(unexpectedError.toUtf8());
    }
    return result;
}

void GrabThread::latestOnlineAppVersionGetHttpQuery(){
    QString requestUrl = "app/v1/tools/TOOL";
    requestUrl = QString::fromUtf8("%1/%2").arg(serverAddressBusiness).arg(requestUrl);

    QNetworkRequest request;
    //配置SSL
    QSslConfiguration config;
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    conf.setProtocol(QSsl::TlsV1SslV3);
    request.setSslConfiguration(conf);
    // 设定url
    request.setUrl(QUrl(requestUrl));
    // 设定请求头
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
//    request.setRawHeader("Authorization",serverAuthorization.toLatin1());
    request.setRawHeader("AppId",serverAppId.toLatin1());
    request.setRawHeader("AppSecurity",serverAppSecurity.toLatin1());


    //set request json data

    //input request json data
    auto displayContent = QString::fromUtf8("curl -X GET --header 'Accept: %1'\t--header 'AppId: %2'\t--header 'AppSecurity: %3'\r\n'%4'")
            .arg(request.header(QNetworkRequest::KnownHeaders::ContentTypeHeader).toString())
//            .arg(QString::fromStdString(request.rawHeader("Authorization").toStdString()))
            .arg(QString::fromStdString(request.rawHeader("AppId").toStdString()))
            .arg(QString::fromStdString(request.rawHeader("AppSecurity").toStdString()))
            .arg(request.url().toString());
    //qDebug() << displayContent;
    auto queryLog = QString("productKindsGetQuery");
    saveToFile(displayContent,queryLog);

    // 使用get方式发起请求
    QNetworkAccessManager *restclient; //in class
    restclient = new QNetworkAccessManager(this); //constructor
    this->reply = restclient->get(request);
    replyProcess();
    getLatestOnlineAppVersionHttpResponse();
}

void GrabThread::getLatestOnlineAppVersionHttpResponse(){
    int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if(http_status == 200){
        auto dataIn = reply->readAll();
        parseErrorMessage(dataIn);

        QTextCodec *gbk=QTextCodec::codecForName("UTF-8");
        QString g2u=gbk->toUnicode(dataIn);
        QString dataReply(dataIn);
        dataReply = dataReply.toUtf8();
        //qDebug() << dataReply;
        auto responseLog = QString("latestVersionResponseContent");
        saveToFile(dataReply,responseLog);

        QJsonDocument jsdoc;
        jsdoc = QJsonDocument::fromJson(g2u.toUtf8());
        if(jsdoc.isEmpty())
            return;
        auto jsobj = jsdoc.object();
        if(jsobj.isEmpty())
            return;
        auto version = jsobj.value("version").toString();
        this->latestOnlineAppVersion = version;

    }else if(http_status != 0){
        auto dataIn = reply->readAll();
        parseErrorMessage(dataIn, http_status);
    }
    reply->deleteLater();
    reply->close();
    reply->deleteLater();
    is_HttpFinished = true;
/*
    // 判断是否是重定向
    if (http_status == 302)
    {
        startGetQuery(reply->rawHeader("Location"));
    }else
    {
        QString reply_content = QString::fromUtf8(reply->readAll());
        if (reply->error() == QNetworkReply::NoError)
        {
            saveToFile(reply_content);
        }else
        {
            qDebug() << "ERROR:" << query_word << " CODE:" << reply->error();
        }
    }
    */
}

QString GrabThread::getLatestOnlineAppVersion(){
    latestOnlineAppVersionGetHttpQuery();
    return latestOnlineAppVersion;
}


void GrabThread::userNameValidationPutHttpQuery(QString userName){
    this->timer.setInterval(2000);
    QString requestUrl = "app/v1/user/validate";
    requestUrl = QString::fromUtf8("%1/%2").arg(serverAddressBusiness).arg(requestUrl);
//    requestUrl = QString::fromUtf8("http://81.68.151.87:8001/%1").arg(requestUrl);

    QNetworkRequest request;
    //配置SSL
    QSslConfiguration config;
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    conf.setProtocol(QSsl::TlsV1SslV3);
    request.setSslConfiguration(conf);
    // 设定url
    request.setUrl(QUrl(requestUrl));
    // 设定请求头
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
//    request.setRawHeader("Authorization",serverAuthorization.toLatin1());
    request.setRawHeader("AppId",serverAppId.toLatin1());
    request.setRawHeader("AppSecurity",serverAppSecurity.toLatin1());


    //set request json data
//    QByteArray
    // TODO:使用Json
    QVariantMap requestData;
    requestData.insert("username", userName);
    QByteArray data =QJsonDocument::fromVariant(requestData).toJson();;

    //input request json data
    auto displayContent = QString::fromUtf8("curl -X PUT --header 'Accept: %1'\t--header 'AppId: %2'\t--header 'AppSecurity: %3'\r\n'%4'\r\n'%5'")
            .arg(request.header(QNetworkRequest::KnownHeaders::ContentTypeHeader).toString())
//            .arg(QString::fromStdString(request.rawHeader("Authorization").toStdString()))
            .arg(QString::fromStdString(request.rawHeader("AppId").toStdString()))
            .arg(QString::fromStdString(request.rawHeader("AppSecurity").toStdString()))
            .arg(QVariant(data).toString())
            .arg(request.url().toString());
    //qDebug() << displayContent;
    auto queryLog = QString("userNameValidationPutHttpQuery");
    saveToFile(displayContent,queryLog);

    // 使用get方式发起请求
    QNetworkAccessManager *restclient; //in class
    restclient = new QNetworkAccessManager(this); //constructor
    this->reply = restclient->put(request, data);
    replyProcess();
    getInitializationValidationHttpResponse();
}

bool GrabThread::getInitializationValidationHttpResponse(){
    bool result = false;
    int http_status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if(http_status == 200){
        result =true;

    }else{
        auto dataIn = reply->readAll();
        parseErrorMessage(dataIn, http_status);
    }

    reply->deleteLater();
    reply->close();
    reply->deleteLater();
    is_HttpFinished = true;
    return result;
}

void GrabThread::uploadLatestLocalAppVersion2Server(QString computerName, QString appVersion, QString userName){
    this->timer.setInterval(2000);
    QString requestUrl = "app/v1/tool";
    requestUrl = QString::fromUtf8("%1/%2").arg(serverAddressBusiness).arg(requestUrl);
    QNetworkRequest request;
    //配置SSL
    QSslConfiguration config;
    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    conf.setProtocol(QSsl::TlsV1SslV3);
    request.setSslConfiguration(conf);
    // 设定url
    request.setUrl(QUrl(requestUrl));
    // 设定请求头
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
//    request.setRawHeader("Authorization",serverAuthorization.toLatin1());
    request.setRawHeader("AppId",serverAppId.toLatin1());
    request.setRawHeader("AppSecurity",serverAppSecurity.toLatin1());

    //set request json data
//    QByteArray
    // TODO:使用Json
    QVariantMap requestData;
//    requestData.insert("computerName","790ZR73.carcgl.com");
//    requestData.insert("currentVersion","superadmin");
//    requestData.insert("username","superadmin");
    requestData.insert("computerName",computerName);
    requestData.insert("currentVersion",appVersion);
    requestData.insert("username",userName);
    QByteArray data =QJsonDocument::fromVariant(requestData).toJson();;

    //input request json data
    auto displayContent = QString::fromUtf8("curl -X PUT --header 'Accept: %1'\t--header 'AppId: %2'\t--header 'AppSecurity: %3'\r\n'%4'\r\n'%5'")
            .arg(request.header(QNetworkRequest::KnownHeaders::ContentTypeHeader).toString())
//            .arg(QString::fromStdString(request.rawHeader("Authorization").toStdString()))
            .arg(QString::fromStdString(request.rawHeader("AppId").toStdString()))
            .arg(QString::fromStdString(request.rawHeader("AppSecurity").toStdString()))
            .arg(QVariant(data).toString())
            .arg(request.url().toString());
    //qDebug() << displayContent;
    auto queryLog = QString("userLocalAppVersionHttpPost");
    saveToFile(displayContent,queryLog);

    // 使用get方式发起请求
    QNetworkAccessManager *restclient; //in class
    restclient = new QNetworkAccessManager(this); //constructor
    this->reply = restclient->post(request, data);
    replyProcess();
    getInitializationValidationHttpResponse();
}



