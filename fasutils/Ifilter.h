/****************************************************************************
** This file is part of the FAS Managementt tool project.
**********************************************************************/

#ifndef FILTER_H
#define FILTER_H

#include "deviceinfo.h"
#include "fasutils.h"
#include "fas_entitycontainer.h"

class FAS_BlockList;

// Holds the data that defines an insert.
struct FilterFactorData {
    FilterFactorData() {}
    FilterFactorData(const QString& name,
                   FAS_Vector insertionPoint,
                   FAS_Vector scaleFactor,
                   double angle,
                   int cols, int rows, FAS_Vector spacing,
                   FAS_BlockList* blockSource = nullptr,
                   FAS2::UpdateMode updateMode = FAS2::Update);

    QString name;
    FAS_Vector insertionPoint;
    FAS_Vector scaleFactor;
    double angle;
    int cols, rows;
    FAS_Vector spacing;
    FAS_BlockList* blockSource;
    FAS2::UpdateMode updateMode;
    DeviceInfo* deviceInfo = nullptr;
};

std::ostream& operator << (std::ostream& os, const FilterFactorData& d);

/*
 * An insert inserts a block into the drawing at a certain location
 * with certain attributes (angle, scale, ...).
 * Inserts don't really contain other entities internally. They just
 * refer to a block. However, to the outside world they act exactly
 * like EntityContainer.
 */
class IFilter
{
public:
    IFilter();
    IFilter(QList<QList<FAS_Entity>> suspectEntitiesOfDevices, QList<FAS_Entity> suspectFreeEntities, const FilterFactorData& d);
    virtual ~IFilter();

    virtual FAS_Entity* clone() const;

    // return FAS2::EntityInsert */
    virtual FAS2::EntityType rtti() const
    {
        return FAS2::EntityInsert;
    }

    // return Copy of data that defines the insert.
    FilterFactorData getData() const
    {
        return data;
    }
/*
    virtual bool isVisible() const;

    virtual FAS_VectorSolutions getRefPoints() const;
    virtual FAS_Vector getNearestRef(const FAS_Vector& coord, double* dist = nullptr) const;

    virtual void move(const FAS_Vector& offset);
    virtual void rotate(const FAS_Vector& center, const double& angle);
    virtual void rotate(const FAS_Vector& center, const FAS_Vector& angleVector);
    virtual void scale(const FAS_Vector& center, const FAS_Vector& factor);
    virtual void mirror(const FAS_Vector& axisPoint1, const FAS_Vector& axisPoint2);

*/

    friend std::ostream& operator << (std::ostream& os, const IFilter& i);

    void setDeviceInfo(DeviceInfo* type);
    DeviceInfo* getDeviceInfo() const;
/*
    QList<QList<FAS_Entity>> suspectEntitiesOfDevices;
    QList<FAS_Entity> suspectFreeEntities;
    FAS_EntityContainer* c = new FAS_EntityContainer;
*/
protected:
    FilterFactorData data;
    mutable FAS_Block* block;
};


#endif
