#include "threaddxfprocess.h"

ThreadDxfProcess::ThreadDxfProcess(QObject *parent):
    QThread(parent)
{

}

void ThreadDxfProcess::setParas(QString file,FASUtils* utils)
{
    this->file=file;
    this->utils=utils;
}
void ThreadDxfProcess::run()
{

    bool opened = utils->openFile(file);
    if(opened)
    {
        emit dxfOpenSuccess();
    }
    else
    {
        emit dxfOpenFailed();
    }


}
