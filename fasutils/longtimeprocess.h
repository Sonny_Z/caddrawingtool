#ifndef LONGTIMEPROCESS_H
#define LONGTIMEPROCESS_H

#include <QObject>
#include "fasutils.h"
class LongTimeProcess:public QObject
{
    Q_OBJECT
public:
    LongTimeProcess();
    void setParas1(int v,FASUtils* utils);
private:
    void work1();
    void work2();
public slots:
    void slotDowork();
signals:
    void sigFinish();
    void sigReturedNewViewInfo(const QString&);
private:
    FASUtils* utils;
    int value;

};

#endif // LONGTIMEPROCESS_H
