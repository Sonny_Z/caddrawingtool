﻿#include "commondef.h"

//sort with alphabetical order
#include <QString>
#include <QtNetwork/QHostInfo>
#include "fas_system.h"
#include <QPixmap>
#include "qtextcodec.h"
CommonDef* CommonDef::uniqueInstance = nullptr;

CommonDef::CommonDef()
{
    svgFolderName = "svgfiles";
    databaseName = "./data.db";
    excelName = "./FASDeviceCountResult.xlsx";
    tempFolderName = QDir(QApplication::applicationDirPath())
            .absoluteFilePath("temp");

    textName = "./entity count.txt";
    typeNameDict.clear();
    firstOpen=false;
    mainwindowCenterx=600;
    mainwindowCentery=300;
    convertDeviceToImage=false;
    drawFinished=true;
    drawWheelEventProgress=false;
    progressShown=false;
    readLineNumber=0;
    drawEntityCount=qRound(FAS_MAXDOUBLE);
    this->pix=new QPixmap(1257, 626);
    drawProgressValue=0;
    wheelZoomShowWidgetNum=false;
    isWheelEvent=false;

    bigEntityThd = 8000; //When the entities count in one insert is bigger than bigEntityThd, we will skip this insert to speed up display;
    displayThd = 200;  //When (screen size)/(entity size) is smaller than displayThd, then we will just draw a rectangle to speed up display;
    minDisplayEntities = 8000;
    maxDisplayEntities = 20000;
    fastDisplay = true; //Open this switch, we will only show the optimal number entities between minDisplayEntities and maxDisplayEntities on GUI;
                        //As to how to calculate this optimal number, please go to calOptimalMaxDisplayEntities() function in fas_entitycontainer.cpp;

    settingFileName = QDir(QApplication::applicationDirPath()).absoluteFilePath("appSetting.ini");
    QFile file(settingFileName);
    bool initSettings = true;
    if(file.exists()){
        initSettings = false;
    }
    getAppSettings();
    if(initSettings){
        setAppSetting("AdminProp","false");
        setAppSetting("SvgUiModeEnabled","0");
        setAppSetting("LastFilePath","..");
        setAppSetting("RecognitionFactor","0.99");
        setAppSetting("systemMemoryLimitFactor","600");
        setAppSetting("LogEnabled","1");
        setAppSetting("TimeoutMillisecond","20000");
    }
}

bool CommonDef::getSvgUiMode(){
    QString keyStr = "SvgUiModeEnabled";
    if(getAppSetting(keyStr).toInt()){
        this->isSvgUiModeEnabled = true;
    }else{
        this->isSvgUiModeEnabled = false;
    }
    return this->isSvgUiModeEnabled;
}

void CommonDef::setSvgUiMode(bool svgUiEnabled){
    if(svgUiEnabled){
        setAppSetting("SvgUiModeEnabled","1");
    }else{
        setAppSetting("SvgUiModeEnabled","0");
    }
    this->isSvgUiModeEnabled = svgUiEnabled;
}

CommonDef::~CommonDef()
{
    qDeleteAll(deviceInfoList);
    qDeleteAll(nameToDeviceInfoHash);
    qDeleteAll(idToDeviceInfoHash);
//    qDeleteAll(nameToSvgFileHash);
//    qDeleteAll(typeNameDict);
//    qDeleteAll(deviceNameIDDict);
//    qDeleteAll(deviceNameCodeHash);
    serverProductName2TypeCode.clear();
    deviceInfoList.clear();
    nameToDeviceInfoHash.clear();
    idToDeviceInfoHash.clear();
    nameToSvgFileHash.clear();
    typeNameDict.clear();
    deviceNameIDDict.clear();
//    deviceNameCodeHash.clear();
    databaseName.clear();
    excelName.clear();
    svgFolderName.clear();
    settingFileName.clear();

    delete appSetting;

    delete uniqueInstance;
}

CommonDef* CommonDef::instance()
{
    if (!uniqueInstance)
    {
        uniqueInstance = new CommonDef();
    }
    return uniqueInstance;
}

void CommonDef::init()
{
    QFileInfo fileInfo("./log");
    QString absoluteFilePath = fileInfo.absoluteFilePath();
    clearDir(absoluteFilePath);
    isDirExist(tempFolderName);

    svgFolderName = "svgfiles";
    databaseName = "./data.db";
    excelName = "./FASDeviceCountResult.xlsx";

    textName = "./entity count.txt";

    GrabThread *httpRequest = new GrabThread();
    latestOnlineAppVersion = httpRequest->getLatestOnlineAppVersion();
    httpRequest->productKindsGetQuery("120204");
//    httpRequest->productKindsGetQuery("120201");
    delete httpRequest;

    buildDeviceNameIdDict();

    typeNameDict.clear();
    typeNameDict.insert("SmokeDetecter", tr("SmokeDetecter"));
    typeNameDict.insert("ThermalDetecter", tr("ThermalDetecter"));
    typeNameDict.insert("ManualAlarm", tr("ManualAlarm"));
    typeNameDict.insert("FireAlarm", tr("FireAlarm"));
    typeNameDict.insert("Hydrant", tr("Hydrant"));
    typeNameDict.insert("GasDetecter", tr("GasDetecter"));
    typeNameDict.insert("BuzzerAlarm", tr("BuzzerAlarm"));
    typeNameDict.insert("FirePhone", tr("FirePhone"));
    typeNameDict.insert("StartStopButton", tr("StartStopButton"));
    typeNameDict.insert("Elevator", tr("Elevator"));
    typeNameDict.insert("FanDevice", tr("FanDevice"));
    typeNameDict.insert("RollingDoor", tr("RollingDoor"));
    typeNameDict.insert("AirConditioner", tr("AirConditioner"));
    typeNameDict.insert("FlowIndicator", tr("FlowIndicator"));
    typeNameDict.insert("PressureSwitch", tr("PressureSwitch"));
    typeNameDict.insert("FireproofValve", tr("FireproofValve"));
    typeNameDict.insert("AntiTheftModule", tr("AntiTheftModule"));
    typeNameDict.insert("EmergencyLight", tr("EmergencyLight"));
    typeNameDict.insert("Alternator", tr("Alternator"));
    typeNameDict.insert("SolenoidValve", tr("SolenoidValve"));
    typeNameDict.insert("SprayingIndicator", tr("SprayingIndicator"));
    typeNameDict.insert("SmokeThermalDetecter", tr("SmokeThermalDetecter"));
    typeNameDict.insert("CableThermalDetecter", tr("CableThermalDetecter"));
    typeNameDict.insert("LinkedPower", tr("LinkedPower"));
    typeNameDict.insert("SmokeEmissionValve", tr("SmokeEmissionValve"));
    typeNameDict.insert("FoamPump", tr("FoamPump"));
    typeNameDict.insert("SprayPump", tr("SprayPump"));
    typeNameDict.insert("GasStart", tr("GasStart"));
    typeNameDict.insert("GasStop", tr("GasStop"));
    typeNameDict.insert("StopButton", tr("StopButton"));
    typeNameDict.insert("WaterCurtainPump", tr("WaterCurtainPump"));
    typeNameDict.insert("WaterCurtainPower", tr("WaterCurtainPower"));
    typeNameDict.insert("AirSupplyValve", tr("AirSupplyValve"));
    typeNameDict.insert("PressureStabilizingPump", tr("PressureStabilizingPump"));
    typeNameDict.insert("HydrantPump", tr("HydrantPump"));
    typeNameDict.insert("RainPump", tr("RainPump"));
    typeNameDict.insert("GateValve", tr("GateValve"));
    typeNameDict.insert("FloorIndicator", tr("FloorIndicator"));
    typeNameDict.insert("PumpFaultIndicator", tr("PumpFaultIndicator"));
    typeNameDict.insert("DredgingIndicator", tr("DredgingIndicator"));
    typeNameDict.insert("HighPressureWaterTank", tr("HighPressureWaterTank"));
    typeNameDict.insert("SignalValve", tr("SignalValve"));
    typeNameDict.insert("SuctionSmokeDetector", tr("SuctionSmokeDetector"));
    typeNameDict.insert("AirCompressor", tr("AirCompressor"));
    typeNameDict.insert("ResidualCurrentTransformer", tr("ResidualCurrentTransformer"));
    typeNameDict.insert("SwitchBoard", tr("SwitchBoard"));
    typeNameDict.insert("UnknownDevice", "UnknownDevice");
    typeNameDict.insert("NotDevice", "NotDevice");

    buildDeviceInfoList();
}

bool CommonDef::buildDeviceNameIdDict(){
    bool result = true;
    deviceNameIDDict.clear();
    auto subDir = QString::fromUtf8("%1").arg(svgFolderName);
    QDir dir(subDir);
    QFileInfoList folder_list = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
    for(int i = 0; i != folder_list.size(); i++)
    {
        QString oneSysDirPath = folder_list.at(i).absoluteFilePath();
        QFileInfo fileInfo(oneSysDirPath);
        QString baseName = fileInfo.baseName();
        QString NameIdDictFile = QString::fromUtf8("%1/%2%3.ini").arg(oneSysDirPath).arg(baseName).arg("products");
        QFile file(NameIdDictFile);
        if(file.exists())
        {
            bool opened = file.open(QIODevice::ReadOnly);
            if(opened)
            {
                QHash<QString,QHash<QString, QString>> typeNameIdDict;
                while (!file.atEnd())
                {
                    QHash<QString, QString> nameIdDict;
                    QByteArray byteArray = file.readLine();
                    QString dictLine(byteArray);
                    if(!dictLine.isNull() && !dictLine.isEmpty()){
                        dictLine.remove("\r\n");
                        QStringList typeNameId =dictLine.split("\t");
                        if(typeNameIdDict.contains(typeNameId.at(0))){
                            nameIdDict = typeNameIdDict.find(typeNameId.at(0)).value();
                            nameIdDict.insert(typeNameId.at(1),typeNameId.at(2));
                            typeNameIdDict.remove(typeNameId.at(0));
                            typeNameIdDict.insert(typeNameId.at(0),nameIdDict);
                        }else{
                            nameIdDict.insert(typeNameId.at(1),typeNameId.at(2));
                            typeNameIdDict.insert(typeNameId.at(0),nameIdDict);
                        }
//                        deviceNameCodeHash.insert(typeNameId.at(2),typeNameId.at(1));
                        typeNameId.clear();
                    }
                    dictLine.clear();
                    nameIdDict.clear();
                }
                deviceNameIDDict.insert(baseName,typeNameIdDict);
                typeNameIdDict.clear();
                file.close();
            }
        }
    }
    if(deviceNameIDDict.isEmpty()){
        result = false;
    }
    return result;
}

//QHash<QString,QString> CommonDef::getDeviceNameCodeDict(){
//    return deviceNameCodeHash;
//}

void CommonDef::buildDeviceInfoList()
{
    deviceInfoList.clear();
    idToDeviceInfoHash.clear();
    nameToDeviceInfoHash.clear();
    nameToSvgFileHash.clear();

    auto systemType = getSystem(true);
//    auto subDir = QString::fromUtf8("%1/%2").arg(svgFolderName).arg(systemType);
//    QList<QString> svgFileNames = FAS_SYSTEM->getFileList(subDir, "svg");

    DeviceInfo* deviceInfo = new DeviceInfo(9998,"UnknownDevice", "UnknownDevice", "");
    deviceInfoList.append(deviceInfo);
    idToDeviceInfoHash.insert(9998, deviceInfo);
    nameToDeviceInfoHash.insert("UnknownDevice", deviceInfo);

    deviceInfo = new DeviceInfo(9999, "NotDevice","NotDevice", "");
    deviceInfoList.append(deviceInfo);
    nameToDeviceInfoHash.insert("NotDevice", deviceInfo);
    idToDeviceInfoHash.insert(9999, deviceInfo);

//    QList<QString> productName2TypeCodeList = getProductName2TypeCode();
//    if(productName2TypeCodeList.count()<=0){
        loadLocalProductTypeList(systemType);
//    }
    auto productName2TypeCodeList = getProductName2TypeCode();
    int ii = 0;
    for(QString productName2TypeCode : productName2TypeCodeList)
    {
        QString deviceName;
        QString deviceTypeCode;
//        QFileInfo fileInfo(oneFilePath);
//        QString baseName = fileInfo.baseName();
        QString baseName = productName2TypeCode;
        QStringList tempList1 = baseName.split('%', QString::SkipEmptyParts);
        if(tempList1.size() > 1)
        {
            deviceName = tempList1.at(1);
            deviceTypeCode = tempList1.at(2);
        }
        int id = 100 * (ii++);
        DeviceInfo* deviceInfo = new DeviceInfo(id, deviceName,deviceTypeCode, productName2TypeCode);
        deviceInfo->setSystemType(systemType);
        deviceInfoList.append(deviceInfo);
        nameToDeviceInfoHash.insert(deviceName, deviceInfo);
        idToDeviceInfoHash.insert(id, deviceInfo);
        nameToSvgFileHash.insert(deviceName, productName2TypeCode);
        baseName.clear();
        tempList1.clear();
    }
    ii = 0;
}


bool CommonDef::isValidDomain(QString localDomainName){
    bool result = false;
    if(!localDomainName.isNull() && !localDomainName.isEmpty()){
        foreach(QString validDomain, domainCheckList){
            if(localDomainName.contains(validDomain,Qt::CaseInsensitive)){
                result = true;
                break;
            }
        }
    }

    return result;
}

void CommonDef::clear()
{
    for(auto deviceInfo : deviceInfoList)
    {
        auto svgRender = deviceInfo->getSvgRender();
        if( svgRender != nullptr)
        {
            delete svgRender;
        }
        delete deviceInfo;
        deviceInfo = nullptr;
    }
    deviceInfoList.clear();
    nameToDeviceInfoHash.clear();
    idToDeviceInfoHash.clear();
    nameToSvgFileHash.clear();
    typeNameDict.clear();
}

qint64 CommonDef::hash(const QString & str)
{
  QByteArray hash = QCryptographicHash::hash(
    QByteArray::fromRawData((const char*)str.utf16(), str.length()*2),
    QCryptographicHash::Md5
  );
  Q_ASSERT(hash.size() == 16);
  QDataStream stream(&hash, QIODevice::ReadOnly);
  qint64 a, b;
  stream >> a >> b;
  return a ^ b;
}

bool CommonDef::isDirExist(QString Path){
    QFileInfo fileInfo(Path);
    QString fullPath = fileInfo.absoluteFilePath();
    QDir dir(fullPath);
    if(dir.exists())
    {
      return true;
    }
    else
    {
       bool ok = dir.mkpath(fullPath);//创建多级目录
       return ok;
    }
}

QString CommonDef::translateToLocal(QString engString)
{
    QString result = engString;
    if(engString.size() == 0)
        return result;

    bool useLocalName = (getLanguage() == FAS2::Chinese);
    if(useLocalName && typeNameDict.find(engString) != typeNameDict.end())
    {
        result = typeNameDict.find(engString).value();
    }
    return result;
}

FAS2::Language CommonDef::getLanguage()
{
    FAS2::Language result = FAS2::English;
    QString languageFile = "Language.txt";
    QFile file(languageFile);
    if(file.exists())
    {
        bool opened = file.open(QIODevice::ReadOnly);
        if(opened)
        {
            QByteArray byteArray = file.readLine();
            QString language(byteArray);
            if(language == "Chinese")
                result = FAS2::Chinese;
            file.close();
        }
    }
    return result;
}

void CommonDef::setLanguage(FAS2::Language language)
{
    QString fileName = "Language.txt";
    QFile file(fileName);
    if(file.open(QIODevice::WriteOnly))
    {
        switch (language) {
        case FAS2::Chinese:
            file.write("Chinese");
            break;
        default:
            file.write("English");
            break;
        }
        file.close();
    }
}

void CommonDef::loadLocalProductTypeList(QString systemType){
    this->serverProductName2TypeCode.clear();
    auto subDir = QString::fromUtf8("%1/%2").arg(svgFolderName).arg(systemType);
//        QDir dir(subDir);
//        QFileInfoList folder_list = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
//        for(int i = 0; i != folder_list.size(); i++)
//        {
//        QString oneSysDirPath = folder_list.at(i).absoluteFilePath();
    QFileInfo fileInfo(subDir);
    QString baseName = fileInfo.baseName();
    QString NameIdDictFile = QString::fromUtf8("%1/%2.ini").arg(fileInfo.absoluteFilePath()).arg(baseName);
    QFile file(NameIdDictFile);
    if(file.exists())
    {
        bool opened = file.open(QIODevice::ReadOnly);
        if(opened)
        {
            QHash<QString,QHash<QString, QString>> typeNameIdDict;
            while (!file.atEnd())
            {
                QHash<QString, QString> nameIdDict;
                QByteArray byteArray = file.readLine();
                QString dictLine(byteArray);
                if(!dictLine.isNull() && !dictLine.isEmpty()){
                    dictLine.remove("\r\n");
                    COMMONDEF->addProductName2TypeCode(dictLine);
                }
            }
        }
    }
//        }
}

DeviceInfo* CommonDef::getDefaultType()
{
    return deviceInfoList.at(1/*deviceInfoList.size() - 2*/);
}

DeviceInfo* CommonDef::getNewNotDeviceInfo()
{
    return new DeviceInfo(9999, "NotDevice","NotDevice", "");
}

DeviceInfo* CommonDef::getDeviceInfoFromName(QString name)
{
    if(nameToDeviceInfoHash.find(name) != nameToDeviceInfoHash.end())
    {
        return nameToDeviceInfoHash.find(name).value();
    }
    else
    {
        return getDefaultType();
    }
}

DeviceInfo* CommonDef::getDeviceInfoFromID(int id)
{
    if(idToDeviceInfoHash.find(id) != idToDeviceInfoHash.end())
    {
        return idToDeviceInfoHash.find(id).value();
    }
    else
    {
        return getDefaultType();
    }
}

QStringList CommonDef::getLocalDeviceTypeList()
{
    QStringList typeStrList;
    for(DeviceInfo* type : deviceInfoList)
    {
        typeStrList.append(translateToLocal(type->getTypeCode()));
    }
    return typeStrList;
}

QStringList CommonDef::getDeviceTypeList()
{
    QStringList typeStrList;
    for(DeviceInfo* type : deviceInfoList)
    {
        typeStrList.append(type->getTypeCode());
    }
    return typeStrList;
}

QStringList CommonDef::getDeviceNameList()
{
    QStringList nameStrList;
    for(DeviceInfo* type : deviceInfoList)
    {
        nameStrList.append(type->getName());
    }
    return nameStrList;
}

QString CommonDef::getDatabaseName()
{
    return databaseName;
}

QString CommonDef::getExcelName()
{
    return excelName;
}

double CommonDef::getTolerance()
{
    return this->tolerance;
}

double CommonDef::getConnectionTolerance()
{
    return this->connectiontolerance;
}

QList<DeviceInfo*> CommonDef::getDeviceInfoList()
{
    return deviceInfoList;
}

QHash<QString, QString> CommonDef::getNameToSvgFilePathHash()
{
    return nameToSvgFileHash;
}

QString CommonDef::getSvgFolder()
{
    QString filePath;
    if(!nameToSvgFileHash.empty())
    {
        QFileInfo fileInfo(nameToSvgFileHash.values().first());
        filePath = fileInfo.absolutePath();
    }
    return filePath;
}

QString CommonDef::getDxfOfDevicePathStr()
{
    QFileInfo fileInfo(dxfOfDevicePath);
    QString absoluteFilePath = fileInfo.absoluteFilePath();
    return absoluteFilePath;
}
QString CommonDef::getSystemTypeIESStr(){
    return systemTypeIES;
}
QString CommonDef::getSystemTypeD_AND_AStr(){
    return systemTypeD_AND_A;
}

QString CommonDef::getNotFoundStr()
{
    return "Not Found";
}

FAS2::SystemType CommonDef::getSystem()
{
    FAS2::SystemType result = FAS2::D_And_Asystem;
    QString key = "System";
    auto systemType = getAppSetting(key);
    key.clear();
    if(systemType == "IESsystem")
        result = FAS2::IESsystem;
    return result;
}

QString CommonDef::getSystemByCode(QString systemCode){
    QString result = systemTypeIES;
    if(systemCode == "120204")
        result = systemTypeIES;
    if(systemCode == "120201")
        result = systemTypeD_AND_A;
    systemCode.clear();
    return result;
}

QString CommonDef::getSystem(bool isGetSystemNameString)
{
    QString result = systemTypeIES;
    QString key = "System";
    auto systemType = getAppSetting(key);
    key.clear();
    if(systemType == "IESsystem")
        result = systemTypeIES;
    if(systemType == "D&Asystem")
        result = systemTypeD_AND_A;
    systemType.clear();
    return result;
}

void CommonDef::setSystem(FAS2::SystemType systemType)
{
    QString key = "System";
    QString value = "";
    switch (systemType) {
    case FAS2::IESsystem:
        value = "IESsystem";
        setAppSetting(key,value);
        break;
    default:
        value = "D&Asystem";
        setAppSetting(key,value);
        break;
    }
    key.clear();
    value.clear();
}

bool CommonDef::getAdmin(){
    bool result = false;
    QString key = "AdminProp";
    auto isAdmin = getAppSetting(key);
    key.clear();
    if(isAdmin.compare("false",Qt::CaseInsensitive))
        result = true;
    isAdmin.clear();
    return result;
}
void CommonDef::setAdmin(bool isAdmin){
    if(isAdmin){
        setAppSetting("AdminProp","true");
    }else{
        setAppSetting("AdminProp","false");
    }
}

int CommonDef::getSubViewSplitterFactor()
{
    return subViewSplitterFactor;
}
int CommonDef::getSubViewOutlineCollectionFactor()
{
    return subViewOutlineCollectionFactor;
}

QHash<QString,QHash<QString,QHash<QString, QString>>> CommonDef::getDeviceNameIdDict(){
    return deviceNameIDDict;
}

void CommonDef::getAppSettings(){
    if(this->appSetting != nullptr){
       delete this->appSetting;
       this->appSetting = nullptr;
    }
    this->appSetting = new QSettings(settingFileName, QSettings::IniFormat);
}
QString CommonDef::getAppSetting(QString &key){
    getAppSettings();
    auto result  = this->appSetting->value(key).toString();
    key.clear();
    return result;
}
float CommonDef::getMinMatchRate(){
    return minMatchRate;
}
bool CommonDef::isConbinedWithLocalData(){
    return conbinedWithLocalData;
}
void CommonDef::setAppSetting(QString key, QString value){
    this->appSetting->setValue(key,value);
}

QString CommonDef::getAppPerformanceLog(){
    return this->appPerformanceLog;
}

void CommonDef::addProductName2TypeCode(QString productName2TypeCode){
    this->serverProductName2TypeCode.append(productName2TypeCode);
}


QList<QString> CommonDef::getProductName2TypeCode(){
    return this->serverProductName2TypeCode;
}

void CommonDef::clearDir(const QString& temp_path)
{
    QDir Dir(temp_path);
    if(Dir.isEmpty())
    {
//        std::cout << "临时文件文件夹" << path << "为空";
        return;
    }
    Dir.removeRecursively();


//    //获取所选文件类型过滤器
//    QStringList filters;
//    filters<<QString("*.jpeg")<<QString("*.jpg")<<QString("*.png")<<QString("*.tiff")<<QString("*.gif")<<QString("*.bmp");
//    // 第三个参数是QDir的过滤参数，这三个表示收集所有文件和目录，且不包含"."和".."目录。
//    // 因为只需要遍历第一层即可，所以第四个参数填QDirIterator::NoIteratorFlags
//    QDirIterator dirsIterator(temp_path, QDir::Files | QDir::AllDirs | QDir::NoDotAndDotDot, QDirIterator::NoIteratorFlags);
//    while(dirsIterator.hasNext())
//    {
//        if (!Dir.remove(dirsIterator.next())) // 删除文件操作如果返回否，那它就是目录
//        {
//            QDir(dirsIterator.filePath()).removeRecursively(); // 删除目录本身以及它下属所有的文件及目录
//        }
//    }
}


void CommonDef::setReadOnly(bool isReadOnly){
//    if(isReadOnly){
//        setAppSetting("ReadOnlyMode","true");
//    }else{
//        setAppSetting("ReadOnlyMode","false");
//    }
    this->isReadOnly = isReadOnly;
}
bool CommonDef::isReadOnlyMode(){
    return this->isReadOnly;
}

bool CommonDef::isNetWorkOnline()
{
    QNetworkConfigurationManager mgr;
//    connect(&mgr, &QNetworkConfigurationManager::onlineStateChanged,this, [=](bool isOnline){
//        qDebug()<<"网络已经更改"<<isOnline;
//    });
    return mgr.isOnline();
}

QString CommonDef::getChangeHistoryLogPath(){
    return this->changeHistoryLogPath;
}

bool CommonDef::isLogEnabled(){
    bool result = true;
    QString keyStr = "LogEnabled";
    if("0" == getAppSetting(keyStr)){
        result = false;
    }
    return result;
}

int CommonDef::timeoutMillisecond(){
    int result = 20000;
    QString keyStr = "TimeoutMillisecond";
    auto timeoutValue = getAppSetting(keyStr);
    if(!timeoutValue.isEmpty() && !timeoutValue.isNull() && isDigitString(timeoutValue)){
        result = timeoutValue.toInt();
    }
    return result;
}

void CommonDef::setHideRecognizedDevice(bool v){
    this->hideRecognizedDevice = v;
}

bool CommonDef::isHideRecognizedDevice(){
    return this->hideRecognizedDevice;
}


QString CommonDef::getLatestOnlineAppVersion(){
    return this->latestOnlineAppVersion;
}

bool CommonDef::isDigitString(const QString& src) {
    QByteArray ba = src.toUtf8();//QString 转换为 char*
     const char *s = ba.data();
//    const char *s = src.toUtf8().data();
    while(*s && *s>='0' && *s<='9') s++;
    return !bool(*s);
}

QString CommonDef::tempDirPath(){
    return this->tempFolderName;
}

void CommonDef::saveToFile(QString & content, QString & fileName, bool isAppend, QString fileType, QString dir)
{
    if(!COMMONDEF->isLogEnabled()){
        return;
    }
    QFileInfo fileInfo(dir);
    QString absoluteFilePath = fileInfo.absoluteFilePath();
    if(COMMONDEF->isDirExist(absoluteFilePath)){
        auto filePath = QString::fromUtf8("%1/%2.%3").arg(absoluteFilePath).arg(fileName).arg(fileType);
        QFile file(filePath);
        if(isAppend){
            file.open(QIODevice::ReadWrite | QIODevice::Append);
        }else{
            file.open(QIODevice::WriteOnly);
        }
        file.write(content.toUtf8());
        file.close();
    }
}
QString CommonDef::getAppVersion(){
    return this->appVersion;
}
bool CommonDef::userNameValidation(QString userName){
    bool result = false;
    GrabThread *httpRequest = new GrabThread();
    httpRequest->userNameValidationPutHttpQuery(userName);
    if(httpRequest->getInitializationValidationHttpResponse()){
        result = true;
    }
    delete httpRequest;
    return result;
}
bool CommonDef::initializationValidation(){
    QHostInfo info;
    QString hostName = info.localHostName();
    QString keyStr = "userName";
    auto localUserName = COMMONDEF->getAppSetting(keyStr);
    if(!userNameValidation(localUserName)){
        return false;
    }
    GrabThread *httpRequest = new GrabThread();
    httpRequest->uploadLatestLocalAppVersion2Server(hostName,this->appVersion,localUserName);
    delete httpRequest;

    return true;
}

bool CommonDef::floorValidation(QString floorName){
    bool result = false;
    if(floorCheckingList.contains(floorName.split("_").last())){
        result = true;
    }
    return result;
}
