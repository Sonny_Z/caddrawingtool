#include "deviceinfo.h"
#include "commondef.h"

DeviceInfo::DeviceInfo(int id, QString name,QString typeCode, QString iconPath)
{
    setID(id);
    setName(name);
    setTypeCode(typeCode);
    QSvgRenderer* render = nullptr;
    if(iconPath.size() != 0)
    {
        render = new QSvgRenderer(iconPath);
    }
    svgRender = render;
    setShowSVG(true);
    setCodeInfo("");
    setNoteText("");
}

DeviceInfo::DeviceInfo(DeviceInfo* ori)
{
    this->setID(ori->getID());
    this->setName(ori->getName());
    this->setShowSVG(ori->getShowSVG());
    this->setCodeInfo(ori->getCodeInfo());
    this->setNoteText(ori->getNoteText());
    this->svgRender = ori->getSvgRender();
}


DeviceInfo::~DeviceInfo()
{
}

bool DeviceInfo::isUnknownDevice()
{
    return (9998 == this->getID());
}

bool DeviceInfo::isNotDevice()
{
    return (9999 == this->getID());
}

bool DeviceInfo::isValidDevice()
{
    return (this->getID() != 9998 && this->getID() != 9999);
}

int DeviceInfo::getID()
{
    return this->deviceID;
}

void DeviceInfo::setID(int id)
{
    this->deviceID = id;
}

QString DeviceInfo::getName()
{
    return this->name;
}

void DeviceInfo::setName(QString name)
{
    this->name = name;
}

QString DeviceInfo::getTypeCode()
{
    return this->typeCode;
}

void DeviceInfo::setTypeCode(QString typeCode)
{
    this->typeCode = typeCode;
}

QSvgRenderer* DeviceInfo::getSvgRender()
{
    return this->svgRender;
}

void DeviceInfo::setSvgRender(QSvgRenderer* render)
{
    this->svgRender = render;
}

bool DeviceInfo::getShowSVG()
{
    return this->showSVG;
}

void DeviceInfo::setShowSVG(bool show)
{
    this->showSVG = show;
}

QString DeviceInfo::getCodeInfo()
{
    return this->codeInfo;
}

void DeviceInfo::setCodeInfo(QString code)
{
    this->codeInfo = code;
}

bool DeviceInfo::isSvgEntity()
{
    return this->isValidDevice();
}

QString DeviceInfo::getNoteText()
{
    return noteText;
}

void DeviceInfo::setNoteText(QString note)
{
    this->noteText = note;
}

QString DeviceInfo::getSystemType()
{
    if(systemType.isNull()||systemType.isEmpty()){
        systemType = "UnknownSystem";
    }
    return systemType;
}

void DeviceInfo::setSystemType(QString systemType)
{
    this->systemType = systemType;
}

bool DeviceInfo::isD_AND_A()
{
    return (systemType.compare(COMMONDEF->getSystemTypeD_AND_AStr(),Qt::CaseInsensitive)==0);
}

bool DeviceInfo::isIES()
{
    return (systemType.compare(COMMONDEF->getSystemTypeIESStr(),Qt::CaseInsensitive)==0);
}

QString DeviceInfo::getPerfectMatchFlag()
{
    return this->perfectMatchFlag;
}
void DeviceInfo::setPerfectMatchFlag(QString perfectMatchFlag)
{
    this->perfectMatchFlag = perfectMatchFlag;
}

bool DeviceInfo::isRecognized(){
    return validRecognition;
}

void DeviceInfo::setValidRecognition(bool isRecognized){
    this->validRecognition = isRecognized;
}
