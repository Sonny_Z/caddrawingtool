<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AddCustomizedDeviceDialog</name>
    <message>
        <source>New View</source>
        <translation type="obsolete">新视图</translation>
    </message>
    <message>
        <location filename="../forms/addcustomizeddevicedialog.ui" line="23"/>
        <source>Customize New Device</source>
        <translation>自定义新加设备</translation>
    </message>
    <message>
        <location filename="../forms/addcustomizeddevicedialog.ui" line="43"/>
        <source>New Device Confirmation</source>
        <translation>新设备确认</translation>
    </message>
    <message>
        <location filename="../forms/addcustomizeddevicedialog.ui" line="65"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../forms/addcustomizeddevicedialog.ui" line="84"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../forms/addcustomizeddevicedialog.ui" line="110"/>
        <source>New Device No.:</source>
        <translation>新设备数量：</translation>
    </message>
    <message>
        <location filename="../forms/addcustomizeddevicedialog.ui" line="174"/>
        <source>New Device Name:</source>
        <translation>新设备名称：</translation>
    </message>
</context>
<context>
    <name>AddDeviceDialog</name>
    <message>
        <location filename="../forms/adddevicedialog.ui" line="20"/>
        <source>Add One Device</source>
        <translation>添加设备</translation>
    </message>
    <message>
        <location filename="../forms/adddevicedialog.ui" line="69"/>
        <source>Please select the device&apos;s type:</source>
        <translation>请选择设备类型：</translation>
    </message>
    <message>
        <location filename="../forms/adddevicedialog.ui" line="88"/>
        <source>Please set the code of the device:</source>
        <translation>请输入设备编码：</translation>
    </message>
    <message>
        <location filename="../forms/adddevicedialog.ui" line="107"/>
        <source>Please set the insert point of the device:</source>
        <translation>请点选设备插入点：</translation>
    </message>
    <message>
        <location filename="../forms/adddevicedialog.ui" line="158"/>
        <source>X:</source>
        <translation>X:</translation>
    </message>
    <message>
        <location filename="../forms/adddevicedialog.ui" line="177"/>
        <source>Y:</source>
        <translation>Y:</translation>
    </message>
    <message>
        <location filename="../forms/adddevicedialog.ui" line="196"/>
        <source>Code:</source>
        <translation>编码：</translation>
    </message>
    <message>
        <location filename="../forms/adddevicedialog.ui" line="209"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../forms/adddevicedialog.ui" line="222"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../forms/adddevicedialog.ui" line="241"/>
        <source>Snap Point in Window</source>
        <translation>在窗口中点选</translation>
    </message>
    <message>
        <location filename="../ui/adddevicedialog.cpp" line="68"/>
        <location filename="../ui/adddevicedialog.cpp" line="78"/>
        <location filename="../ui/adddevicedialog.cpp" line="83"/>
        <location filename="../ui/adddevicedialog.cpp" line="93"/>
        <location filename="../ui/adddevicedialog.cpp" line="98"/>
        <location filename="../ui/adddevicedialog.cpp" line="103"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/adddevicedialog.cpp" line="68"/>
        <source>Failed!</source>
        <translation>失败!</translation>
    </message>
    <message>
        <location filename="../ui/adddevicedialog.cpp" line="78"/>
        <source>The code text does not match rule.</source>
        <translation>编码不符合规则。</translation>
    </message>
    <message>
        <location filename="../ui/adddevicedialog.cpp" line="83"/>
        <source>The position does not match rule.</source>
        <translation>位置不符合规则。</translation>
    </message>
    <message>
        <location filename="../ui/adddevicedialog.cpp" line="93"/>
        <source>Input code does not match the rule.</source>
        <translation>初始编码不符合规则。</translation>
    </message>
    <message>
        <location filename="../ui/adddevicedialog.cpp" line="98"/>
        <source>Input code is already assigned to another device.</source>
        <translation>输入编码已被其他设备占用。</translation>
    </message>
    <message>
        <location filename="../ui/adddevicedialog.cpp" line="103"/>
        <source>Please set layer for code in Tools-&gt;Setting firstly.</source>
        <translation>请在“工具-&gt;设置&quot;中指定图层。</translation>
    </message>
    <message>
        <location filename="../ui/adddevicedialog.cpp" line="108"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../ui/adddevicedialog.cpp" line="108"/>
        <source>Unexpected error occurs.</source>
        <translation>发生未知错误。</translation>
    </message>
</context>
<context>
    <name>AddDeviceTypeDialog</name>
    <message>
        <location filename="../forms/adddevicetypedialog.ui" line="20"/>
        <source>Expand Device Type</source>
        <translation>扩展设备类型</translation>
    </message>
    <message>
        <location filename="../forms/adddevicetypedialog.ui" line="50"/>
        <source>Add one new type to list</source>
        <translation>添加新的设备类型</translation>
    </message>
    <message>
        <location filename="../forms/adddevicetypedialog.ui" line="71"/>
        <source>Please input the type&apos;s name:</source>
        <translation>请输入类型名称：</translation>
    </message>
    <message>
        <location filename="../forms/adddevicetypedialog.ui" line="92"/>
        <source>Please select the SVG file of the type:</source>
        <translation>请选择SVG文件：</translation>
    </message>
    <message>
        <location filename="../forms/adddevicetypedialog.ui" line="123"/>
        <source>explorer</source>
        <translation>浏览...</translation>
    </message>
    <message>
        <location filename="../forms/adddevicetypedialog.ui" line="144"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../forms/adddevicetypedialog.ui" line="165"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/adddevicetypedialog.cpp" line="37"/>
        <location filename="../ui/adddevicetypedialog.cpp" line="43"/>
        <location filename="../ui/adddevicetypedialog.cpp" line="47"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/adddevicetypedialog.cpp" line="37"/>
        <source>The information is not complete</source>
        <translation>信息不完整</translation>
    </message>
    <message>
        <location filename="../ui/adddevicetypedialog.cpp" line="43"/>
        <source>The input device name already exists.</source>
        <translation>输入的设备名称已存在。</translation>
    </message>
    <message>
        <location filename="../ui/adddevicetypedialog.cpp" line="47"/>
        <source>The SVG file cannot be copied.</source>
        <translation>无法完成SVG文件的拷贝。</translation>
    </message>
    <message>
        <location filename="../ui/adddevicetypedialog.cpp" line="52"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../ui/adddevicetypedialog.cpp" line="52"/>
        <source>New device type is added successfully.</source>
        <translation>新设备类型添加成功。</translation>
    </message>
</context>
<context>
    <name>AutoCodeDialog</name>
    <message>
        <location filename="../forms/autocodedialog.ui" line="20"/>
        <source>Auto Code Dialog</source>
        <translation>自动编码窗口</translation>
    </message>
    <message>
        <location filename="../forms/autocodedialog.ui" line="40"/>
        <source>Detect Loops</source>
        <translation>识别Loop</translation>
    </message>
    <message>
        <location filename="../forms/autocodedialog.ui" line="61"/>
        <source>Auto Code</source>
        <translation>自动编码</translation>
    </message>
    <message>
        <location filename="../forms/autocodedialog.ui" line="100"/>
        <source>Device&apos;s layer:</source>
        <translation>设备图层：</translation>
    </message>
    <message>
        <location filename="../forms/autocodedialog.ui" line="139"/>
        <source>Wire&apos;s layer:</source>
        <translation>管线图层：</translation>
    </message>
    <message>
        <location filename="../forms/autocodedialog.ui" line="178"/>
        <source>Select layer where the text lie on:</source>
        <translation>请选择文字所放置图层：</translation>
    </message>
    <message>
        <location filename="../forms/autocodedialog.ui" line="199"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="80"/>
        <source>Find</source>
        <translation>找到</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="80"/>
        <source>loops!</source>
        <translation>loops！</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="83"/>
        <source> devices are disconnected to loops</source>
        <translation> 个设备跟loop断开</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="102"/>
        <source> has </source>
        <translation>有</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="102"/>
        <source> devices and </source>
        <translation>设备和</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="102"/>
        <source> wires.</source>
        <translation>总线。</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="105"/>
        <source> Over maximum limit!</source>
        <translation>超过最大限制！</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="113"/>
        <location filename="../ui/autocodedialog.cpp" line="134"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="126"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="126"/>
        <source>Please set layer for code text in &apos;Tool-&gt;Set Layer For Device Code&apos;.</source>
        <translation>请在“工具-&gt;设置&quot;中指定图层。</translation>
    </message>
    <message>
        <location filename="../ui/autocodedialog.cpp" line="134"/>
        <source> code(s) are added in this document.</source>
        <translation>个设备编码被添加。</translation>
    </message>
</context>
<context>
    <name>BlockForm</name>
    <message>
        <location filename="../forms/blockform.ui" line="32"/>
        <source>BlockList</source>
        <translation>设备列表</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../forms/blockform.ui" line="125"/>
        <source>Upload Status</source>
        <translation>上传状态</translation>
    </message>
    <message>
        <location filename="../forms/blockform.ui" line="173"/>
        <source>Added Block</source>
        <translation>新增设备</translation>
    </message>
    <message>
        <location filename="../forms/blockform.ui" line="181"/>
        <source>Added Type</source>
        <translation>新增设备类型</translation>
    </message>
    <message>
        <location filename="../forms/blockform.ui" line="197"/>
        <source>Action</source>
        <translation>操作</translation>
    </message>
    <message>
        <source>Hide Recognized Devices</source>
        <translation type="vanished">隐藏已识别的设备</translation>
    </message>
    <message>
        <location filename="../forms/blockform.ui" line="226"/>
        <source>Add Device</source>
        <translation>添加设备</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation type="vanished">单层上传</translation>
    </message>
    <message>
        <location filename="../forms/blockform.ui" line="261"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Show Branch</source>
        <translation type="vanished">显示支路</translation>
    </message>
    <message>
        <location filename="../forms/blockform.ui" line="101"/>
        <source>Block</source>
        <translation>块</translation>
    </message>
    <message>
        <location filename="../forms/blockform.ui" line="109"/>
        <source>Device Type</source>
        <translation>设备类型</translation>
    </message>
    <message>
        <location filename="../forms/blockform.ui" line="117"/>
        <location filename="../forms/blockform.ui" line="189"/>
        <source>Count</source>
        <translation>数量</translation>
    </message>
    <message>
        <location filename="../forms/blockform.ui" line="51"/>
        <source>Device Information</source>
        <translation>设备信息</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="443"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="480"/>
        <source>Block Description</source>
        <translation>图块说明</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="480"/>
        <source>Block Name</source>
        <translation>图块名称</translation>
    </message>
    <message>
        <source>Layer Name</source>
        <translation type="vanished">图层名称</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="582"/>
        <source>Total count</source>
        <translation>总计</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="727"/>
        <source>ok</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="730"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="829"/>
        <location filename="../ui/blockform.cpp" line="837"/>
        <source>saved</source>
        <translation>已保存</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="831"/>
        <location filename="../ui/blockform.cpp" line="847"/>
        <source>devices</source>
        <translation>类设备</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="831"/>
        <location filename="../ui/blockform.cpp" line="838"/>
        <location filename="../ui/blockform.cpp" line="847"/>
        <source>device</source>
        <translation>类设备</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="832"/>
        <source>successfully</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="833"/>
        <source>saved floors</source>
        <translation>已保存楼层</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="839"/>
        <source>Saved floors</source>
        <translation>已保存楼层</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="943"/>
        <source>Network is invalid.</source>
        <translation>网络异常</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="946"/>
        <source>Exception</source>
        <translation>异常</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="1025"/>
        <source>New Device</source>
        <translation>新增设备</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="1045"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../ui/blockform.cpp" line="352"/>
        <location filename="../ui/blockform.cpp" line="740"/>
        <location filename="../ui/blockform.cpp" line="855"/>
        <location filename="../ui/blockform.cpp" line="949"/>
        <source>Result</source>
        <translation>结果</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Completed.</source>
        <translation type="vanished">已完成。</translation>
    </message>
</context>
<context>
    <name>CalculateWireLengthDialog</name>
    <message>
        <location filename="../forms/calculatewirelengthdialog.ui" line="20"/>
        <source>Calculate Length of Wire</source>
        <translation>计算总线长度</translation>
    </message>
    <message>
        <location filename="../forms/calculatewirelengthdialog.ui" line="40"/>
        <source>Please select layer of the Wire:</source>
        <translation>请选择总线图层：</translation>
    </message>
    <message>
        <location filename="../forms/calculatewirelengthdialog.ui" line="63"/>
        <source>Total Length of Wire is:</source>
        <translation>总线长度是：</translation>
    </message>
    <message>
        <location filename="../forms/calculatewirelengthdialog.ui" line="75"/>
        <source>0m</source>
        <translation>0m</translation>
    </message>
    <message>
        <location filename="../forms/calculatewirelengthdialog.ui" line="109"/>
        <source>Calculate</source>
        <translation>计算</translation>
    </message>
    <message>
        <location filename="../forms/calculatewirelengthdialog.ui" line="129"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>CommonDef</name>
    <message>
        <location filename="../fasutils/commondef.cpp" line="190"/>
        <source>SmokeDetecter</source>
        <translation>烟雾报警器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="191"/>
        <source>ThermalDetecter</source>
        <translation>温度报警器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="192"/>
        <source>ManualAlarm</source>
        <translation>手动报警器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="193"/>
        <source>FireAlarm</source>
        <translation>火灾报警器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="194"/>
        <source>Hydrant</source>
        <translation>消防栓</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="195"/>
        <source>GasDetecter</source>
        <translation>气体探测器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="196"/>
        <source>BuzzerAlarm</source>
        <translation>蜂鸣器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="197"/>
        <source>FirePhone</source>
        <translation>消防电话</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="198"/>
        <source>StartStopButton</source>
        <translation>启停按钮</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="199"/>
        <source>Elevator</source>
        <translation>电梯</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="200"/>
        <source>FanDevice</source>
        <translation>风箱</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="201"/>
        <source>RollingDoor</source>
        <translation>卷帘门</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="202"/>
        <source>AirConditioner</source>
        <translation>空调</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="203"/>
        <source>FlowIndicator</source>
        <translation>水流指示器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="204"/>
        <source>PressureSwitch</source>
        <translation>压力开关</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="205"/>
        <source>FireproofValve</source>
        <translation>防火阀</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="206"/>
        <source>AntiTheftModule</source>
        <translation>防盗模块</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="207"/>
        <source>EmergencyLight</source>
        <translation>应急灯</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="208"/>
        <source>Alternator</source>
        <translation>发电机</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="209"/>
        <source>SolenoidValve</source>
        <translation>电磁阀</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="210"/>
        <source>SprayingIndicator</source>
        <translation>喷洒指示器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="211"/>
        <source>SmokeThermalDetecter</source>
        <translation>烟温复合传感器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="212"/>
        <source>CableThermalDetecter</source>
        <translation>缆式感温传感器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="213"/>
        <source>LinkedPower</source>
        <translation>联动电源</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="214"/>
        <source>SmokeEmissionValve</source>
        <translation>排烟阀</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="215"/>
        <source>FoamPump</source>
        <translation>泡沫泵</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="216"/>
        <source>SprayPump</source>
        <translation>喷淋泵</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="217"/>
        <source>GasStart</source>
        <translation>气体启动</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="218"/>
        <source>GasStop</source>
        <translation>气体停止</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="219"/>
        <source>StopButton</source>
        <translation>停止按钮</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="220"/>
        <source>WaterCurtainPump</source>
        <translation>水幕泵</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="221"/>
        <source>WaterCurtainPower</source>
        <translation>水幕电</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="222"/>
        <source>AirSupplyValve</source>
        <translation>送风阀</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="223"/>
        <source>PressureStabilizingPump</source>
        <translation>稳压泵</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="224"/>
        <source>HydrantPump</source>
        <translation>消防栓泵</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="225"/>
        <source>RainPump</source>
        <translation>雨琳泵</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="226"/>
        <source>GateValve</source>
        <translation>闸阀</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="227"/>
        <source>FloorIndicator</source>
        <translation>层号灯</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="228"/>
        <source>PumpFaultIndicator</source>
        <translation>泵故障</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="229"/>
        <source>DredgingIndicator</source>
        <translation>疏导指示</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="230"/>
        <source>HighPressureWaterTank</source>
        <translation>高压水箱</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="231"/>
        <source>SignalValve</source>
        <translation>信号阀</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="232"/>
        <source>SuctionSmokeDetector</source>
        <translation>吸气感烟传感器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="233"/>
        <source>AirCompressor</source>
        <translation>空压机</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="234"/>
        <source>ResidualCurrentTransformer</source>
        <translation>剩余电流互感器</translation>
    </message>
    <message>
        <location filename="../fasutils/commondef.cpp" line="235"/>
        <source>SwitchBoard</source>
        <translation>配电盘</translation>
    </message>
    <message>
        <source>UnknownDevice</source>
        <translation type="vanished">待标定设备</translation>
    </message>
    <message>
        <source>NotDevice</source>
        <translation type="vanished">疑似设备</translation>
    </message>
</context>
<context>
    <name>DeviceRecordForm</name>
    <message>
        <location filename="../forms/deviceRecordform.ui" line="32"/>
        <source>Device Information</source>
        <translation>设备信息</translation>
    </message>
    <message>
        <source>Block</source>
        <translation type="vanished">块</translation>
    </message>
    <message>
        <location filename="../forms/deviceRecordform.ui" line="86"/>
        <source>Device Type</source>
        <translation>设备类型</translation>
    </message>
    <message>
        <location filename="../forms/deviceRecordform.ui" line="94"/>
        <location filename="../forms/deviceRecordform.ui" line="150"/>
        <source>Count</source>
        <translation>数量</translation>
    </message>
    <message>
        <source>Hide Recognized Devices</source>
        <translation type="vanished">隐藏已识别的设备</translation>
    </message>
    <message>
        <location filename="../forms/deviceRecordform.ui" line="195"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Upload Status</source>
        <translation type="vanished">上传状态</translation>
    </message>
    <message>
        <source>Added Block</source>
        <translation type="vanished">新增设备</translation>
    </message>
    <message>
        <location filename="../forms/deviceRecordform.ui" line="142"/>
        <source>Added Type</source>
        <translation>新增设备类型</translation>
    </message>
    <message>
        <source>Action</source>
        <translation type="vanished">操作</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="../ui/deviceRecordform.cpp" line="97"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../ui/deviceRecordform.cpp" line="161"/>
        <source>Total count</source>
        <translation>总计</translation>
    </message>
    <message>
        <location filename="../ui/deviceRecordform.cpp" line="384"/>
        <source>Network is invalid.</source>
        <translation>网络异常</translation>
    </message>
    <message>
        <location filename="../ui/deviceRecordform.cpp" line="387"/>
        <source>Exception</source>
        <translation>异常</translation>
    </message>
    <message>
        <location filename="../ui/deviceRecordform.cpp" line="390"/>
        <source>Result</source>
        <translation>结果</translation>
    </message>
    <message>
        <source>New Device</source>
        <translation type="vanished">新增设备</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
</context>
<context>
    <name>DrawNotes</name>
    <message>
        <location filename="../forms/drawnotes.ui" line="20"/>
        <source>Draw Note</source>
        <translation>添加文字</translation>
    </message>
    <message>
        <location filename="../forms/drawnotes.ui" line="48"/>
        <source>Please input a note:</source>
        <translation>请输入文字：</translation>
    </message>
    <message>
        <location filename="../forms/drawnotes.ui" line="61"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../forms/drawnotes.ui" line="74"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../forms/drawnotes.ui" line="93"/>
        <source>Please define the height of the notes:</source>
        <translation>请输入文本高度：</translation>
    </message>
    <message>
        <location filename="../forms/drawnotes.ui" line="137"/>
        <source>Snap One Point</source>
        <translation>点选插入点</translation>
    </message>
    <message>
        <location filename="../forms/drawnotes.ui" line="188"/>
        <source>X:</source>
        <translation>X:</translation>
    </message>
    <message>
        <location filename="../forms/drawnotes.ui" line="207"/>
        <source>Please set the insert point of the device:</source>
        <translation>请输入设备插入点：</translation>
    </message>
    <message>
        <location filename="../forms/drawnotes.ui" line="226"/>
        <source>Y:</source>
        <translation>Y:</translation>
    </message>
    <message>
        <location filename="../ui/drawnotes.cpp" line="33"/>
        <location filename="../ui/drawnotes.cpp" line="38"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/drawnotes.cpp" line="33"/>
        <source>Position is invalid.</source>
        <translation>位置不正确</translation>
    </message>
    <message>
        <location filename="../ui/drawnotes.cpp" line="38"/>
        <source>Unexpected error occurs.</source>
        <translation>发生未知错误。</translation>
    </message>
</context>
<context>
    <name>Entitypropdialog</name>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>图面实体属性</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="59"/>
        <source>Entity Property List</source>
        <translation>实体对象属性列表</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="140"/>
        <source>Entity Type:</source>
        <translation>实体类型</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="210"/>
        <source>Device Detail Type:</source>
        <translation>设备精细选型</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="253"/>
        <source>Min Point Coordinate</source>
        <translation>最小点坐标</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="264"/>
        <location filename="../forms/entitypropdialog.ui" line="329"/>
        <location filename="../forms/entitypropdialog.ui" line="394"/>
        <source>X:</source>
        <translation>X:</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="285"/>
        <location filename="../forms/entitypropdialog.ui" line="350"/>
        <location filename="../forms/entitypropdialog.ui" line="415"/>
        <source>Y:</source>
        <translation>Y:</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="318"/>
        <source>Max Point Coordinate</source>
        <translation>最大点坐标</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="383"/>
        <source>Middle Point Coordinate</source>
        <translation>中心点坐标</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="450"/>
        <source>Relating Point List</source>
        <translation>实体关联点清单</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="473"/>
        <source>Parent Device ID:</source>
        <translation>链路上级设备</translation>
    </message>
    <message>
        <source>Parent:</source>
        <translation type="vanished">父节点</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="89"/>
        <source>ID:</source>
        <translation>标识</translation>
    </message>
    <message>
        <source>Entity Type::</source>
        <translation type="vanished">实体类型</translation>
    </message>
    <message>
        <location filename="../forms/entitypropdialog.ui" line="177"/>
        <source>Device Type:</source>
        <translation>设备类型</translation>
    </message>
    <message>
        <location filename="../ui/entitypropdialog.cpp" line="131"/>
        <source>Start Point Coordinate</source>
        <translation>起点坐标</translation>
    </message>
    <message>
        <location filename="../ui/entitypropdialog.cpp" line="133"/>
        <source>End Point Coordinate</source>
        <translation>终点坐标</translation>
    </message>
</context>
<context>
    <name>FASUtils</name>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="4963"/>
        <source>Info: Selecting Less Than 20 Entities Caused Invalid Operation.</source>
        <translation>信息：选择少于20个实体引起无效操作。</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5371"/>
        <source>EntityUnknown</source>
        <translation>未知实体</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5374"/>
        <source>EntityContainer</source>
        <translation>容器实体</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5377"/>
        <source>EntityBlock</source>
        <translation>模块实体</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5380"/>
        <source>EntityFontChar</source>
        <translation>字</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5383"/>
        <source>EntityInsert</source>
        <translation>实体块</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5386"/>
        <source>EntityGraphic</source>
        <translation>图形实体</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5389"/>
        <source>EntityPoint</source>
        <translation>点</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5392"/>
        <source>EntityLine</source>
        <translation>直线</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5395"/>
        <source>EntityPolyline</source>
        <translation>折线</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5398"/>
        <source>EntityVertex</source>
        <translation>顶点</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5401"/>
        <source>EntityArc</source>
        <translation>弧线</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5404"/>
        <source>EntityCircle</source>
        <translation>圆</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5407"/>
        <source>EntityEllipse</source>
        <translation>椭圆</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5410"/>
        <source>EntitySolid</source>
        <translation>实心体</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5413"/>
        <source>EntityConstructionLine</source>
        <translation>构造线</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5416"/>
        <source>EntityMText</source>
        <translation>多行文本</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5419"/>
        <source>EntityText</source>
        <translation>单行文本</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5422"/>
        <source>EntityDimAligned</source>
        <translation>尺寸对齐</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5425"/>
        <source>EntityDimLinear</source>
        <translation>线性尺寸</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5428"/>
        <source>EntityDimRadial</source>
        <translation>径向尺寸</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5431"/>
        <source>EntityDimDiametric</source>
        <translation>直径尺寸</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5434"/>
        <source>EntityDimAngular</source>
        <translation>角度尺寸</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5437"/>
        <source>EntityDimLeader</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5440"/>
        <source>EntityHatch</source>
        <translation>填充</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5443"/>
        <source>EntityImage</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5446"/>
        <source>EntitySpline</source>
        <translation>曲线</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5449"/>
        <source>EntitySplinePoints</source>
        <translation>曲线顶点</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5452"/>
        <source>EntityOverlayBox</source>
        <translation>叠加框</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5455"/>
        <source>EntityPreview</source>
        <translation>预览</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5458"/>
        <source>EntityPattern</source>
        <translation>图案</translation>
    </message>
    <message>
        <location filename="../fasutils/fasutils.cpp" line="5461"/>
        <source>EntityOverlayLine</source>
        <translation>叠加线</translation>
    </message>
</context>
<context>
    <name>FindDevice</name>
    <message>
        <location filename="../forms/finddevice.ui" line="20"/>
        <source>Find Device</source>
        <translation>搜索设备</translation>
    </message>
    <message>
        <location filename="../forms/finddevice.ui" line="32"/>
        <source>Input Device Code：</source>
        <translation>请输入设备编码：</translation>
    </message>
    <message>
        <location filename="../forms/finddevice.ui" line="67"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../forms/finddevice.ui" line="86"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/finddevice.cpp" line="31"/>
        <location filename="../ui/finddevice.cpp" line="36"/>
        <location filename="../ui/finddevice.cpp" line="42"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../ui/finddevice.cpp" line="31"/>
        <source>Critical error happens, please restart software.</source>
        <translation>发生未知错误，请重启软件。</translation>
    </message>
    <message>
        <location filename="../ui/finddevice.cpp" line="36"/>
        <source>Please check input!</source>
        <translation>输入不正确，请检查输入！</translation>
    </message>
    <message>
        <location filename="../ui/finddevice.cpp" line="42"/>
        <source>Cannot find the code.</source>
        <translation>找不到设备编码。</translation>
    </message>
</context>
<context>
    <name>GrabThread</name>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="232"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="232"/>
        <source>Invalid image.</source>
        <translation>无效图片。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1057"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <source>Null Error!</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1064"/>
        <source>Network Exception</source>
        <translation>网络异常</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1085"/>
        <source>No Network.</source>
        <translation>没有网络。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1086"/>
        <source>Success.</source>
        <translation>成功。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1087"/>
        <source>Created.</source>
        <translation>已创建。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1088"/>
        <source>Bad Request.</source>
        <translation>错误请求。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1089"/>
        <source>Unauthorized.</source>
        <translation>未授权。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1090"/>
        <source>Payment Required.</source>
        <translation>需要付款。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1091"/>
        <source>Forbidden.</source>
        <translation>禁止访问。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1092"/>
        <source>Not Found.</source>
        <translation>无法找到文件。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1093"/>
        <source>Method Not Allowed.</source>
        <translation>资源被禁止。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1094"/>
        <source>Not Acceptable.</source>
        <translation>无法接受。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1095"/>
        <source>Proxy Authentication Required.</source>
        <translation>要求代理身份验证。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1096"/>
        <source>Request Timeout.</source>
        <translation>请求超时。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1097"/>
        <source>Internal Server Error.</source>
        <translation>内部服务器错误。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1098"/>
        <source>Not Implemented.</source>
        <translation>未实现。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1099"/>
        <source>Bad Gateway.</source>
        <translation>网关错误。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1100"/>
        <source>Service Unavailable.</source>
        <translation>服务不可用。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1101"/>
        <source>Gateway Timeout.</source>
        <translation>网关超时。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1102"/>
        <source>HTTP Version Not Supported.</source>
        <translation>不支持HTTP版本。</translation>
    </message>
    <message>
        <location filename="../fasutils/grabthread.cpp" line="1112"/>
        <source>Unexpected Network Error.</source>
        <translation>未知的网络错误。</translation>
    </message>
    <message>
        <source>Unknown Network Error.</source>
        <translation type="vanished">未知的网络错误</translation>
    </message>
</context>
<context>
    <name>InformationDialog</name>
    <message>
        <location filename="../forms/informationdialog.ui" line="20"/>
        <source>Device information</source>
        <translation>设备信息</translation>
    </message>
    <message>
        <location filename="../forms/informationdialog.ui" line="39"/>
        <source>Information</source>
        <translation>设备信息</translation>
    </message>
    <message>
        <location filename="../forms/informationdialog.ui" line="130"/>
        <source>X:</source>
        <translation>X:</translation>
    </message>
    <message>
        <location filename="../forms/informationdialog.ui" line="158"/>
        <source>Y:</source>
        <translation>Y:</translation>
    </message>
    <message>
        <location filename="../forms/informationdialog.ui" line="171"/>
        <source>Code:</source>
        <translation>编码：</translation>
    </message>
    <message>
        <location filename="../forms/informationdialog.ui" line="184"/>
        <source>Type:</source>
        <translation>类型：</translation>
    </message>
    <message>
        <location filename="../forms/informationdialog.ui" line="58"/>
        <source>Highlight</source>
        <translation>高亮</translation>
    </message>
    <message>
        <location filename="../forms/informationdialog.ui" line="80"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../forms/informationdialog.ui" line="93"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../forms/informationdialog.ui" line="197"/>
        <source>Note:</source>
        <translation>注释：</translation>
    </message>
    <message>
        <location filename="../ui/informationdialog.cpp" line="55"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../ui/informationdialog.cpp" line="55"/>
        <source>Cannot find related information!</source>
        <translation>找不到给定信息</translation>
    </message>
    <message>
        <location filename="../ui/informationdialog.cpp" line="85"/>
        <location filename="../ui/informationdialog.cpp" line="91"/>
        <location filename="../ui/informationdialog.cpp" line="95"/>
        <location filename="../ui/informationdialog.cpp" line="99"/>
        <location filename="../ui/informationdialog.cpp" line="103"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/informationdialog.cpp" line="85"/>
        <location filename="../ui/informationdialog.cpp" line="91"/>
        <source>Unexpected error occurs.</source>
        <translation>发生未知错误。</translation>
    </message>
    <message>
        <location filename="../ui/informationdialog.cpp" line="95"/>
        <source>Input code does not match the rule.</source>
        <translation>编码不符合规则。</translation>
    </message>
    <message>
        <location filename="../ui/informationdialog.cpp" line="99"/>
        <source>There is an existing code with inut code. Please input another.</source>
        <translation>输入编码已被其他设备占用。</translation>
    </message>
    <message>
        <location filename="../ui/informationdialog.cpp" line="103"/>
        <source>Please set layer for code text in &apos;Tool-&gt;Setting&apos;.</source>
        <translation>请在“工具-&gt;设置&quot;中指定图层。</translation>
    </message>
</context>
<context>
    <name>Log4Qt::AppenderSkeleton</name>
    <message>
        <location filename="../lib/log4qt/log4qt/appenderskeleton.cpp" line="137"/>
        <source>Activation of appender &apos;%1&apos; that requires layout and has no layout set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/appenderskeleton.cpp" line="228"/>
        <source>Use of non activated appender &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/appenderskeleton.cpp" line="236"/>
        <source>Use of closed appender &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/appenderskeleton.cpp" line="244"/>
        <source>Use of appender &apos;%1&apos; that requires layout and has no layout set</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Log4Qt::DailyRollingFileAppender</name>
    <message>
        <location filename="../lib/log4qt/log4qt/dailyrollingfileappender.cpp" line="148"/>
        <source>Use of appender &apos;%1&apos; without having a valid date pattern set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/dailyrollingfileappender.cpp" line="215"/>
        <source>The pattern &apos;%1&apos; does not specify a frequency for appender &apos;%2&apos;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Log4Qt::Factory</name>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/factory.cpp" line="265"/>
        <source>Cannot convert to type &apos;%1&apos; for property &apos;%2&apos; on object of class &apos;%3&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/factory.cpp" line="378"/>
        <source>Unable to set property value on object</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/factory.cpp" line="384"/>
        <source>Invalid null object pointer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/factory.cpp" line="393"/>
        <source>Invalid empty property name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/factory.cpp" line="411"/>
        <source>Property &apos;%1&apos; does not exist in class &apos;%2&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/factory.cpp" line="424"/>
        <source>Property &apos;%1&apos; is not writable in class &apos;%2&apos;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Log4Qt::FileAppender</name>
    <message>
        <location filename="../lib/log4qt/log4qt/fileappender.cpp" line="131"/>
        <source>Activation of Appender &apos;%1&apos; that requires file and has no file set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/fileappender.cpp" line="161"/>
        <source>Use of appender &apos;%1&apos; without open file</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/fileappender.cpp" line="224"/>
        <source>Unable to write to file &apos;%1&apos; for appender &apos;%2&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/fileappender.cpp" line="258"/>
        <source>Unable to open file &apos;%1&apos; for appender &apos;%2&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/fileappender.cpp" line="276"/>
        <source>Unable to remove file &apos;%1&apos; for appender &apos;%2&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/fileappender.cpp" line="292"/>
        <source>Unable to rename file &apos;%1&apos; to &apos;%2&apos; for appender &apos;%3&apos;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Log4Qt::OptionConverter</name>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/optionconverter.cpp" line="103"/>
        <source>Missing closing bracket for opening bracket at %1. Invalid subsitution in value %2.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/optionconverter.cpp" line="151"/>
        <source>Invalid option string &apos;%1&apos; for a boolean</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/optionconverter.cpp" line="207"/>
        <source>Invalid option string &apos;%1&apos; for a file size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/optionconverter.cpp" line="227"/>
        <source>Invalid option string &apos;%1&apos; for an integer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/optionconverter.cpp" line="242"/>
        <source>Invalid option string &apos;%1&apos; for an qint64</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/optionconverter.cpp" line="260"/>
        <source>Invalid option string &apos;%1&apos; for a level</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/optionconverter.cpp" line="299"/>
        <source>Invalid option string &apos;%1&apos; for a target</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Log4Qt::PatternFormatter</name>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/patternformatter.cpp" line="535"/>
        <source>Found character &apos;%1&apos; where digit was expected.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/patternformatter.cpp" line="620"/>
        <source>Option &apos;%1&apos; cannot be converted into an integer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/helpers/patternformatter.cpp" line="628"/>
        <source>Option %1 isn&apos;t a positive integer</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Log4Qt::PropertyConfigurator</name>
    <message>
        <location filename="../lib/log4qt/log4qt/propertyconfigurator.cpp" line="146"/>
        <source>Unable to open property file &apos;%1&apos;</source>
        <translation>文件打开失败</translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/propertyconfigurator.cpp" line="158"/>
        <source>Unable to read property file &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/propertyconfigurator.cpp" line="370"/>
        <source>Missing appender definition for appender named &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/propertyconfigurator.cpp" line="380"/>
        <source>Unable to create appender of class &apos;%1&apos; namd &apos;%2&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/propertyconfigurator.cpp" line="428"/>
        <source>Missing layout definition for appender &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/propertyconfigurator.cpp" line="438"/>
        <source>Unable to create layoput of class &apos;%1&apos; requested by appender &apos;%2&apos;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Log4Qt::WriterAppender</name>
    <message>
        <location filename="../lib/log4qt/log4qt/writerappender.cpp" line="137"/>
        <source>Activation of Appender &apos;%1&apos; that requires writer and has no writer set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../lib/log4qt/log4qt/writerappender.cpp" line="192"/>
        <source>Use of appender &apos;%1&apos; without a writer set</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../forms/logindialog.ui" line="32"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <source>Input Domin Account</source>
        <translation type="vanished">请输入域账号名</translation>
    </message>
    <message>
        <source>Input GST Domin Account</source>
        <translation type="vanished">请输入海湾域账号</translation>
    </message>
    <message>
        <source>请输入海湾域账号名</source>
        <translation type="vanished">Input GST Domin Account</translation>
    </message>
    <message>
        <location filename="../forms/logindialog.ui" line="55"/>
        <source>Input GST BPM Account</source>
        <translation>请输入海湾域账号</translation>
    </message>
    <message>
        <location filename="../forms/logindialog.ui" line="73"/>
        <source>请输入海湾域账号</source>
        <translation>Input GST BPM Account</translation>
    </message>
    <message>
        <location filename="../forms/logindialog.ui" line="111"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;example:carcgl\username&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>example:carcgl\username</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;example:carcgl/username&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">example:carcgl/username</translation>
    </message>
    <message>
        <location filename="../forms/logindialog.ui" line="114"/>
        <source>carcgl\</source>
        <translation>carcgl\</translation>
    </message>
    <message>
        <location filename="../forms/logindialog.ui" line="189"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../forms/logindialog.ui" line="160"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/logindialog.cpp" line="83"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>用户名不正确，系统要求识图工具用户名必须和选型系统完全一致！</source>
        <translation type="vanished">用户名不正确，系统要求识图工具用户名必须和选型系统完全一致！</translation>
    </message>
    <message>
        <source>User name is incorrect. It must be exactly the same as the model selection system!</source>
        <translation type="vanished">用户名不正确，系统要求识图工具用户名必须和选型系统完全一致！</translation>
    </message>
    <message>
        <source>Invalid Domain Name!</source>
        <translation type="vanished">无效用户</translation>
    </message>
    <message>
        <location filename="../ui/logindialog.cpp" line="83"/>
        <source>Invalid Account!</source>
        <translation>无效账户</translation>
    </message>
</context>
<context>
    <name>LongTimeProcess</name>
    <message>
        <location filename="../fasutils/longtimeprocess.cpp" line="34"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/mainwindow.ui" line="20"/>
        <source>MainWindow</source>
        <translation>主窗口</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="47"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="vanished">导出</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="vanished">工具箱</translation>
    </message>
    <message>
        <source>Window</source>
        <translation type="vanished">窗口</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">编辑</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="54"/>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <source>Show_BlockList</source>
        <translation type="vanished">显示设备列表</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="277"/>
        <source>Show_nBlockList</source>
        <translation>显示非块设备列表</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="282"/>
        <source>to LocalDB</source>
        <translation>存储到本地</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="287"/>
        <source>to Cloud</source>
        <translation>云同步</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="292"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="128"/>
        <location filename="../forms/mainwindow.ui" line="131"/>
        <source>FindDevice</source>
        <translation>搜索设备</translation>
    </message>
    <message>
        <source>Save Features</source>
        <translation type="vanished">保存设备特征</translation>
    </message>
    <message>
        <source>Detect Device</source>
        <translation type="vanished">图纸识别</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="61"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="65"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="71"/>
        <source>Change History</source>
        <translation>变更历史</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="88"/>
        <location filename="../ui/mainwindow.cpp" line="1396"/>
        <source>View</source>
        <translation>视图</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="95"/>
        <source>Detect</source>
        <translation>图纸识别</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="102"/>
        <source>Upload Result</source>
        <translation>上传结果</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="109"/>
        <source>Check Result</source>
        <translation>查看结果</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="134"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="143"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="148"/>
        <source>ShowHide Devices</source>
        <translation>显示/隐藏设备</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="153"/>
        <source>Save As...</source>
        <translation>另存为...</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="158"/>
        <source>AutoCode</source>
        <translation>自动编码</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="163"/>
        <source>Close File</source>
        <translation>关闭文件</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="168"/>
        <source>Add New Device Type</source>
        <translation>扩展设备类型</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="173"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="176"/>
        <source>Undo last step.</source>
        <translation>撤销上一步操作</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="179"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="184"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="187"/>
        <source>Ctrl+Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="192"/>
        <source>Auto Zoom</source>
        <translation>自适应窗口</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="197"/>
        <source>Add One Device</source>
        <translation>添加一个设备</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="202"/>
        <source>Draw Note</source>
        <translation>添加注释</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="207"/>
        <source>Setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="212"/>
        <source>Export as image</source>
        <translation>导出为图片</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="217"/>
        <source>Export as PDF</source>
        <translation>导出为PDF</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="222"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="225"/>
        <source>Delete selected entities</source>
        <translation>删除所选实体</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="228"/>
        <source>Del</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="233"/>
        <source>Chinese</source>
        <translation>中文</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="238"/>
        <source>English</source>
        <translation>英文</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="243"/>
        <source>Save Devices to Database</source>
        <translation>保存设备到数据库</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="248"/>
        <source>Switch SVG/CAD</source>
        <translation>切换SVG/CAD图标</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="251"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="256"/>
        <source>Calculate Wire Length</source>
        <translation>计算总线长度</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="261"/>
        <source>Check Layer of Entity</source>
        <translation>检查图层</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="264"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="269"/>
        <source>Connect Two Devices</source>
        <translation>连接两个设备</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="301"/>
        <location filename="../ui/mainwindow.cpp" line="1266"/>
        <source>IES SYSTEM</source>
        <translation>智能疏散系统</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="313"/>
        <location filename="../ui/mainwindow.cpp" line="1267"/>
        <source>DA System</source>
        <translation>通用消防系统</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="321"/>
        <source>Upload All Result</source>
        <translation>全部上传</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="339"/>
        <source>V0.0.16</source>
        <translation>V0.0.16</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="399"/>
        <source>0.0.16</source>
        <translation>0.0.16</translation>
    </message>
    <message>
        <source>V0.0.15</source>
        <translation type="vanished">V0.0.15</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="384"/>
        <source>0.0.15</source>
        <translation>0.0.15</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="389"/>
        <source>Check Single Result</source>
        <translation>单层查看</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="394"/>
        <source>Check All Result</source>
        <translation>全部查看</translation>
    </message>
    <message>
        <source>V.0.0.14</source>
        <translation type="vanished">V.0.0.14</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="379"/>
        <source>0.0.14</source>
        <translation>0.0.14</translation>
    </message>
    <message>
        <source>V.0.0.13</source>
        <translation type="vanished">V.0.0.13</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="369"/>
        <source>Upload Single Result</source>
        <translation>单层上传</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="374"/>
        <source>0.0.13</source>
        <translation>0.0.13</translation>
    </message>
    <message>
        <source>All Result</source>
        <translation type="vanished">全部上传</translation>
    </message>
    <message>
        <source>Single View</source>
        <translation type="vanished">单层上传</translation>
    </message>
    <message>
        <source>Upload Recognition Result</source>
        <translation type="vanished">上传识别结果</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="329"/>
        <source>Delete View</source>
        <translation>删除视图</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="334"/>
        <source>Auto Split View</source>
        <translation>自动分图</translation>
    </message>
    <message>
        <source>V.0.0.12</source>
        <translation type="vanished">V.0.0.12</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="364"/>
        <source>0.0.12</source>
        <translation>0.0.12</translation>
    </message>
    <message>
        <source>V.0.0.11</source>
        <translation type="vanished">V.0.0.11</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="359"/>
        <source>0.0.11</source>
        <translation>0.0.11</translation>
    </message>
    <message>
        <source>V.0.0.10</source>
        <translation type="vanished">V.0.0.10</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="354"/>
        <source>0.0.10</source>
        <translation>0.0.10</translation>
    </message>
    <message>
        <source>V.0.0.9</source>
        <translation type="vanished">V.0.0.9</translation>
    </message>
    <message>
        <source>V0.0.9</source>
        <translation type="vanished">V0.0.9</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="344"/>
        <source>0.0.9</source>
        <translation>0.0.9</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="349"/>
        <source>Initial</source>
        <translation>初始</translation>
    </message>
    <message>
        <source>IES SYSTEM CHART</source>
        <translation type="vanished">智能疏散-系统图</translation>
    </message>
    <message>
        <source>IES LAYOUT CHART</source>
        <translation type="vanished">智能疏散-平面图</translation>
    </message>
    <message>
        <source>Split Show</source>
        <translation type="vanished">图纸分页显示</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="215"/>
        <source>loading...</source>
        <translation>正在加载……</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="327"/>
        <location filename="../ui/mainwindow.cpp" line="886"/>
        <location filename="../ui/mainwindow.cpp" line="943"/>
        <location filename="../ui/mainwindow.cpp" line="1495"/>
        <location filename="../ui/mainwindow.cpp" line="1499"/>
        <location filename="../ui/mainwindow.cpp" line="1570"/>
        <location filename="../ui/mainwindow.cpp" line="1575"/>
        <location filename="../ui/mainwindow.cpp" line="1830"/>
        <location filename="../ui/mainwindow.cpp" line="1843"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>Open file failed.</source>
        <translation type="vanished">打开文件失败。</translation>
    </message>
    <message>
        <source>Open file failed.Chinese characters in path can not be support.</source>
        <translation type="vanished">打开文件失败！仅支持不包含中文字符的文件路径。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="327"/>
        <source>Open file failed.Chinese characters in path can not be supported.</source>
        <translation>打开文件失败！仅支持不包含中文字符的文件路径。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="449"/>
        <location filename="../ui/mainwindow.cpp" line="453"/>
        <location filename="../ui/mainwindow.cpp" line="472"/>
        <location filename="../ui/mainwindow.cpp" line="476"/>
        <location filename="../ui/mainwindow.cpp" line="820"/>
        <location filename="../ui/mainwindow.cpp" line="1558"/>
        <location filename="../ui/mainwindow.cpp" line="1643"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="449"/>
        <location filename="../ui/mainwindow.cpp" line="472"/>
        <source>Saved</source>
        <translation>保存成功</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="453"/>
        <location filename="../ui/mainwindow.cpp" line="476"/>
        <source>Not Saved</source>
        <translation>未保存</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="534"/>
        <location filename="../ui/mainwindow.cpp" line="798"/>
        <location filename="../ui/mainwindow.cpp" line="868"/>
        <location filename="../ui/mainwindow.cpp" line="876"/>
        <location filename="../ui/mainwindow.cpp" line="1541"/>
        <location filename="../ui/mainwindow.cpp" line="1609"/>
        <location filename="../ui/mainwindow.cpp" line="1703"/>
        <source>Result</source>
        <translation>结果</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="534"/>
        <source>Invalid Selection.</source>
        <translation>选择无效</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="567"/>
        <source>Device information:</source>
        <translation>设备信息：</translation>
    </message>
    <message>
        <source>Device information: Null</source>
        <translation type="vanished">设备信息：无</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="605"/>
        <source>System resource is limited, restart the applicaiton please.</source>
        <translation>系统资源受限，请重启应用程序。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="798"/>
        <source>Devices are saved to Database.</source>
        <translation>设备信息已存储在数据库中。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="815"/>
        <source>No entity selected.</source>
        <translation>未选中任何实例。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="817"/>
        <source>Slected entities are located in difference layers.</source>
        <translation>所选图例位于不同的图层。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="819"/>
        <source>Selected entity(s) located in layer: </source>
        <translation>所选图例所在图层为：</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="841"/>
        <location filename="../ui/mainwindow.cpp" line="847"/>
        <source>Alert</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="841"/>
        <source>Please make sure two valid devices are selected.</source>
        <translation>请确保选择了两个设备。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="847"/>
        <source>Please select layer for new created wire in Tools-&gt;Setting.</source>
        <translation>请在&quot;工具-&gt;设置&quot;中选择总线图层。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="868"/>
        <source>Rules are saved to local Database.</source>
        <translation>已存储设备特征至本地数据库</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="876"/>
        <source>Rules are saved to Cloud.</source>
        <translation>已同步设备特征到云</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="886"/>
        <location filename="../ui/mainwindow.cpp" line="943"/>
        <source>Network exception! Check the connection please.</source>
        <translation>网络异常，请检查网络连接是否正常。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1217"/>
        <source>Click to display(double click to edit) it.</source>
        <translation>单击显示图纸,双击编辑标签</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1273"/>
        <source>Show Detect Result...</source>
        <translation>显示识别结果...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1398"/>
        <source>Device</source>
        <translation>设备</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1005"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1005"/>
        <source>Count</source>
        <translation>数量</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="vanished">状态</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1025"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1095"/>
        <source>Total count</source>
        <translation>总计</translation>
    </message>
    <message>
        <source>Customized Device</source>
        <translation type="vanished">定制设备</translation>
    </message>
    <message>
        <source>Check Detect Result</source>
        <translation type="vanished">检查检测结果</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1276"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1277"/>
        <source>Delete view</source>
        <translation>删除视图</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1830"/>
        <location filename="../ui/mainwindow.cpp" line="1843"/>
        <source>No view data has been saved.</source>
        <translation>当前视图数据未保存</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1484"/>
        <source>Infomation</source>
        <translation>消息</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1262"/>
        <source>Detect Drawing</source>
        <translation>图纸识别</translation>
    </message>
    <message>
        <source>Show Detect Result</source>
        <translation type="vanished">显示识别结果...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1274"/>
        <source>Hide Recognized Device</source>
        <translation>隐藏已识别设备</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1275"/>
        <source>Upload Detect Result</source>
        <translation>上传识别结果</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1484"/>
        <source>The view will be deleted.</source>
        <translation>将删除此视图。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1495"/>
        <source>The original view can not be deleted.</source>
        <translation>源图无法删除</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1499"/>
        <location filename="../ui/mainwindow.cpp" line="1570"/>
        <source>A view should be selected.</source>
        <translation>应该选取一个视图</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1541"/>
        <source>Duplicate! Check it and try again.</source>
        <translation>重复项！检查后重试！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1558"/>
        <source>No floor code.</source>
        <translation>无楼层信息</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1575"/>
        <source>No valid result data in the view.</source>
        <translation>当前视图无有效识别结果。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1603"/>
        <source>Network is invalid.</source>
        <translation>网络异常</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1606"/>
        <source>Exception</source>
        <translation>异常</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1643"/>
        <source>No floor code:<byte value="xd"/>
</source>
        <translation>无楼层信息</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1703"/>
        <source>Failed:No result.</source>
        <translation>上传失败：未返回响应编码。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1781"/>
        <source>Created new</source>
        <translation>创建新</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1782"/>
        <source>views</source>
        <translation>视图</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1782"/>
        <source>view</source>
        <translation>视图</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="605"/>
        <location filename="../ui/mainwindow.cpp" line="1793"/>
        <location filename="../ui/mainwindow.cpp" line="1797"/>
        <source>information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="118"/>
        <source>Status:</source>
        <translation>状态：</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1797"/>
        <source>No new view is automatically created.</source>
        <translation>没有自动创建新的视图。</translation>
    </message>
</context>
<context>
    <name>MultiSelectDialog</name>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="20"/>
        <source>Device Information</source>
        <translation>设备信息</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="40"/>
        <source>RangeSelection</source>
        <translation>框选</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="97"/>
        <location filename="../ui/multiselectdialog.cpp" line="215"/>
        <source>Statistics</source>
        <translation>统计</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="147"/>
        <source>ID</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="171"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="179"/>
        <source>Coordinate</source>
        <translation>坐标</translation>
    </message>
    <message>
        <source>NO</source>
        <translation type="vanished">序号</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="155"/>
        <source>Device</source>
        <translation>设备</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="163"/>
        <source>Code</source>
        <translation>编码</translation>
    </message>
    <message>
        <source>Device Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <source>X-Coord</source>
        <translation type="vanished">X坐标</translation>
    </message>
    <message>
        <source>Y-Coord</source>
        <translation type="vanished">Y坐标</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="59"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="78"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>XX devices are selected.</source>
        <translation type="vanished">XX个设备被选中</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="215"/>
        <source>Initial code:</source>
        <translation>初始编码：</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="240"/>
        <source>Quick Encode</source>
        <translation>快速编码</translation>
    </message>
    <message>
        <location filename="../forms/multiselectdialog.ui" line="187"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../ui/multiselectdialog.cpp" line="122"/>
        <location filename="../ui/multiselectdialog.cpp" line="127"/>
        <location filename="../ui/multiselectdialog.cpp" line="132"/>
        <location filename="../ui/multiselectdialog.cpp" line="137"/>
        <location filename="../ui/multiselectdialog.cpp" line="152"/>
        <location filename="../ui/multiselectdialog.cpp" line="168"/>
        <location filename="../ui/multiselectdialog.cpp" line="182"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/multiselectdialog.cpp" line="122"/>
        <source>Unexpected error occurs.</source>
        <translation>发生未知错误。</translation>
    </message>
    <message>
        <location filename="../ui/multiselectdialog.cpp" line="127"/>
        <source>Input code does not match the rule.</source>
        <translation>编码不符合规则。</translation>
    </message>
    <message>
        <location filename="../ui/multiselectdialog.cpp" line="132"/>
        <source>There is an existing code with inut code. Please input another.</source>
        <translation>输入编码已被其他设备占用。</translation>
    </message>
    <message>
        <location filename="../ui/multiselectdialog.cpp" line="137"/>
        <source>Please set layer for code text in &apos;Tool-&gt;Setting&apos;.</source>
        <translation>请在“工具-&gt;设置&quot;中指定图层。</translation>
    </message>
    <message>
        <location filename="../ui/multiselectdialog.cpp" line="152"/>
        <location filename="../ui/multiselectdialog.cpp" line="168"/>
        <location filename="../ui/multiselectdialog.cpp" line="182"/>
        <source>Initial code does not match the rule.</source>
        <translation>初始编码不符合规则。</translation>
    </message>
    <message>
        <location filename="../ui/multiselectdialog.cpp" line="203"/>
        <source>Statistics is below:</source>
        <translation>统计如下：</translation>
    </message>
    <message>
        <location filename="../ui/multiselectdialog.cpp" line="214"/>
        <source>Total count of device: </source>
        <translation>设备总和：</translation>
    </message>
</context>
<context>
    <name>MyProgressDialog</name>
    <message>
        <location filename="../ui/myprogressdialog.ui" line="78"/>
        <source>Dialog</source>
        <translation>图面实体属性</translation>
    </message>
</context>
<context>
    <name>NBlockForm</name>
    <message>
        <location filename="../forms/nblockform.ui" line="26"/>
        <source>BlockList</source>
        <translation>设备列表</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <location filename="../forms/nblockform.ui" line="66"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../forms/nblockform.ui" line="85"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../forms/nblockform.ui" line="98"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../forms/nblockform.ui" line="124"/>
        <source>Block</source>
        <translation>块</translation>
    </message>
    <message>
        <location filename="../forms/nblockform.ui" line="132"/>
        <source>Device Type</source>
        <translation>设备类型</translation>
    </message>
    <message>
        <location filename="../forms/nblockform.ui" line="140"/>
        <source>Count</source>
        <translation>数量</translation>
    </message>
    <message>
        <location filename="../forms/nblockform.ui" line="162"/>
        <source>Device Information</source>
        <translation>设备信息</translation>
    </message>
    <message>
        <location filename="../ui/nblockform.cpp" line="1106"/>
        <source>Total count</source>
        <translation>总计</translation>
    </message>
</context>
<context>
    <name>NewViewDialog</name>
    <message>
        <source>Device Information</source>
        <translation type="vanished">新视图</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="vanished">登录</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="23"/>
        <source>New View</source>
        <translation>新视图</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="43"/>
        <source>New View Selection Confirmation</source>
        <translation>确认选中新视图</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="65"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="84"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>New View ID:</source>
        <translation type="vanished">新视图ID:</translation>
    </message>
    <message>
        <source>New View Name:</source>
        <translation type="vanished">新视图名称：</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="112"/>
        <source>View Name:</source>
        <translation>视图名称</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="134"/>
        <source>Floor Name:</source>
        <translation>楼层名称</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="165"/>
        <source>-3F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="170"/>
        <source>-2F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="175"/>
        <source>-1F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="180"/>
        <source>1F</source>
        <translation>1F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="185"/>
        <source>2F</source>
        <translation>2F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="190"/>
        <source>3F</source>
        <translation>3F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="195"/>
        <source>4F</source>
        <translation>4F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="200"/>
        <source>5F</source>
        <translation>5F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="205"/>
        <source>6F</source>
        <translation>6F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="210"/>
        <source>7F</source>
        <translation>7F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="215"/>
        <source>8F</source>
        <translation>8F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="220"/>
        <source>9F</source>
        <translation>9F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="225"/>
        <source>10F</source>
        <translation>10F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="230"/>
        <source>11F</source>
        <translation>11F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="235"/>
        <source>12F</source>
        <translation>12F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="240"/>
        <source>13F</source>
        <translation>13F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="245"/>
        <source>14F</source>
        <translation>14F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="250"/>
        <source>15F</source>
        <translation>15F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="255"/>
        <source>16F</source>
        <translation>16F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="260"/>
        <source>17F</source>
        <translation>17F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="265"/>
        <source>18F</source>
        <translation>18F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="270"/>
        <source>19F</source>
        <translation>19F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="275"/>
        <source>20F</source>
        <translation>20F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="280"/>
        <source>21F</source>
        <translation>21F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="285"/>
        <source>22F</source>
        <translation>22F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="290"/>
        <source>23F</source>
        <translation>23F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="295"/>
        <source>24F</source>
        <translation>24F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="300"/>
        <source>25F</source>
        <translation>25F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="305"/>
        <source>26F</source>
        <translation>26F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="310"/>
        <source>27F</source>
        <translation>27F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="315"/>
        <source>28F</source>
        <translation>28F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="320"/>
        <source>29F</source>
        <translation>29F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="325"/>
        <source>30F</source>
        <translation>30F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="330"/>
        <source>31F</source>
        <translation>31F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="335"/>
        <source>32F</source>
        <translation>32F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="340"/>
        <source>33F</source>
        <translation>33F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="345"/>
        <source>34F</source>
        <translation>34F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="350"/>
        <source>35F</source>
        <translation>35F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="355"/>
        <source>36F</source>
        <translation>36F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="360"/>
        <source>37F</source>
        <translation>37F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="365"/>
        <source>38F</source>
        <translation>38F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="370"/>
        <source>39F</source>
        <translation>39F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="375"/>
        <source>40F</source>
        <translation>40F</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="380"/>
        <source>避难层</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="385"/>
        <source>地下层</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="390"/>
        <source>消防控制室</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="395"/>
        <source>夹层</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="400"/>
        <source>残疾人通道</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="405"/>
        <source>图例层</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="410"/>
        <source>Auto-Subview</source>
        <translation>自动-子视图</translation>
    </message>
    <message>
        <location filename="../forms/newviewdialog.ui" line="445"/>
        <source>Operation in subviews is denied.</source>
        <translation>子视图不支持此操作！</translation>
    </message>
    <message>
        <location filename="../ui/newviewdialog.cpp" line="21"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../ui/newviewdialog.cpp" line="76"/>
        <source>Result</source>
        <translation>结果</translation>
    </message>
    <message>
        <location filename="../ui/newviewdialog.cpp" line="76"/>
        <source>Duplicate! Check it and try again.</source>
        <translation>重复项！检查后重试！</translation>
    </message>
    <message>
        <location filename="../ui/newviewdialog.cpp" line="100"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
</context>
<context>
    <name>PopUpMessageDialog</name>
    <message>
        <location filename="../forms/popupmessagedialog.ui" line="14"/>
        <source>Message Dialog</source>
        <translation>消息</translation>
    </message>
    <message>
        <location filename="../forms/popupmessagedialog.ui" line="42"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Completed!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;操作已完成!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main/main.cpp" line="28"/>
        <location filename="../main/main.cpp" line="43"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../main/main.cpp" line="29"/>
        <source>Application is running.</source>
        <translation>应用程序正在运行。</translation>
    </message>
    <message>
        <location filename="../main/main.cpp" line="44"/>
        <source>Only Carrier(GST) assets are supported! Application will be closed.</source>
        <translation>软件只能在属于海湾公司资产的电脑上运行！程序将被关闭。</translation>
    </message>
    <message>
        <location filename="../main/main.cpp" line="63"/>
        <source>New version</source>
        <translation>识图工具软件最新版本</translation>
    </message>
    <message>
        <location filename="../main/main.cpp" line="65"/>
        <source>is available! Please download the latest version online.</source>
        <translation>已更新！请登录选型网站下载最新版本。</translation>
    </message>
    <message>
        <source>Domain name is invalid! Application will be closed.</source>
        <translation type="vanished">无效域名，程序将关闭。</translation>
    </message>
    <message>
        <location filename="../main/main.cpp" line="66"/>
        <source>Information</source>
        <translation>消息</translation>
    </message>
    <message>
        <source>New version is enabled! Download latest tool online please.</source>
        <translation type="obsolete">识图工具软件已更新！请登录选型网站下载最新版本。</translation>
    </message>
</context>
<context>
    <name>SettingDialog</name>
    <message>
        <location filename="../forms/settingdialog.ui" line="20"/>
        <source>Setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../forms/settingdialog.ui" line="81"/>
        <source>Device&apos;s code will addded in following layer:</source>
        <translation>请选择设备编码被添加到的图层：</translation>
    </message>
    <message>
        <location filename="../forms/settingdialog.ui" line="38"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../forms/settingdialog.ui" line="57"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../forms/settingdialog.ui" line="108"/>
        <source>New layer&apos;s name:</source>
        <translation>新图层名称：</translation>
    </message>
    <message>
        <location filename="../forms/settingdialog.ui" line="135"/>
        <source>Please select the format of device code:</source>
        <translation>请选择设备编码格式：</translation>
    </message>
    <message>
        <location filename="../forms/settingdialog.ui" line="172"/>
        <source>Layer for new created Line:</source>
        <translation>新添加总线所在图层：</translation>
    </message>
    <message>
        <location filename="../forms/settingdialog.ui" line="234"/>
        <source>R:      G:      B:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/settingdialog.ui" line="253"/>
        <source>Maxium number of Device in one loop:</source>
        <translation>单个Loop的最大设备数量：</translation>
    </message>
    <message>
        <location filename="../forms/settingdialog.ui" line="206"/>
        <source>Set backgroud color</source>
        <translation>设置背景颜色</translation>
    </message>
    <message>
        <location filename="../ui/settingdialog.cpp" line="26"/>
        <source>Add New Layer</source>
        <translation>添加新图层</translation>
    </message>
    <message>
        <location filename="../ui/settingdialog.cpp" line="92"/>
        <source>Select background color</source>
        <translation>请选择背景颜色</translation>
    </message>
</context>
<context>
    <name>ShowHideDialog</name>
    <message>
        <location filename="../forms/showhidedialog.ui" line="20"/>
        <source>Show/Hide dialog</source>
        <translation>显示隐藏窗口</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="23"/>
        <source>Show/Hide the device between groups</source>
        <translation>根据不同组别显示隐藏</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="221"/>
        <source>ShowAll</source>
        <translation>显示所有</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="206"/>
        <source>HideAll</source>
        <translation>隐藏所有</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="119"/>
        <source>ShowByCode</source>
        <translation>显示此编码设备</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="94"/>
        <source>HideByCode</source>
        <translation>隐藏此编码设备</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="134"/>
        <source>ShowByType</source>
        <translation>显示此类型设备</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="149"/>
        <source>HideByType</source>
        <translation>隐藏此类型设备</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="176"/>
        <source>Show Layer</source>
        <translation>显示图层</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="191"/>
        <source>Hide Layer</source>
        <translation>隐藏图层</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="79"/>
        <location filename="../ui/showhidedialog.h" line="36"/>
        <source>Twinkle</source>
        <translation>闪烁</translation>
    </message>
    <message>
        <location filename="../forms/showhidedialog.ui" line="43"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../ui/showhidedialog.cpp" line="69"/>
        <location filename="../ui/showhidedialog.cpp" line="81"/>
        <location filename="../ui/showhidedialog.cpp" line="105"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/showhidedialog.cpp" line="69"/>
        <location filename="../ui/showhidedialog.cpp" line="81"/>
        <location filename="../ui/showhidedialog.cpp" line="105"/>
        <source>Please check input code.</source>
        <translation>输入不正确，请检查输入！</translation>
    </message>
    <message>
        <location filename="../ui/showhidedialog.h" line="37"/>
        <source>Untwinkle</source>
        <translation>取消闪烁</translation>
    </message>
</context>
<context>
    <name>ThreadExcelGenerate</name>
    <message>
        <location filename="../data/threadexcelgenerate.cpp" line="32"/>
        <source>DeviceName</source>
        <translation>设备类型</translation>
    </message>
    <message>
        <location filename="../data/threadexcelgenerate.cpp" line="33"/>
        <source>Count</source>
        <translation>数量</translation>
    </message>
</context>
<context>
    <name>WaitLongTimeDialog</name>
    <message>
        <location filename="../ui/waitlongtimedialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>图面实体属性</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">信息</translation>
    </message>
</context>
<context>
    <name>WaitOpenDialog</name>
    <message>
        <location filename="../ui/waitopendialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>图面实体属性</translation>
    </message>
</context>
<context>
    <name>ZoomWidget</name>
    <message>
        <location filename="../ui/zoomwidget.ui" line="83"/>
        <source>Form</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../ui/zoomwidget.ui" line="107"/>
        <source>缩放比例：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/zoomwidget.ui" line="154"/>
        <source>100%</source>
        <translation>100%</translation>
    </message>
    <message>
        <location filename="../ui/zoomwidget.ui" line="181"/>
        <source>请滚动鼠标滚轮选择缩放比例再单击鼠标进行缩放</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>loadingProcess</name>
    <message>
        <location filename="../forms/loadingprocess.ui" line="14"/>
        <source>Dialog</source>
        <translation>图面实体属性</translation>
    </message>
    <message>
        <location filename="../forms/loadingprocess.ui" line="26"/>
        <source>loading</source>
        <translation>正在加载</translation>
    </message>
</context>
</TS>
